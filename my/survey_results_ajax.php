<?
 session_start();
 include("connect.php");
 include("functions.php");


/***------------------------------------------
 *
 * ����������� �������� ��� �����������
 *
 **/


/***------------------------------------------
 * �������� ���������� �����������
 **/

function vote_results($survey_id){

 $my_survey_issues = sql_row("SELECT * FROM `my_survey_issues` WHERE `survey_id` = '".$survey_id."' LIMIT 1;");

   $reply = unserialize($my_survey_issues['reply']);
   $answers_leader = array();
   $answers = array();

   $otvets = explode("<br />", $my_survey_issues['value']);

   $user_count = 0;
   $total_count = 0;
   $total_cont = array();

     foreach ($otvets as $k => $v) {

     $k_ind = $k+1;
     $plus_cnt = 0;
     $cur_cnt = 0;

      if (!empty($reply[$k_ind])) {
       foreach ($reply[$k_ind] as $ck => $cv) {
        $cur_cnt += $cv;
        $total_cont[$ck] = 1;
       }
      }

      $answers[$k]['answer'] = cp_text($v);   # �����
      $answers[$k]['cnt'] = (int)($cur_cnt + $plus_cnt);   # �������
      $answers_leader[$k]['cnt'] = $answers[$k]['cnt'];   # �������
      $answers_leader[$k]['id'] = $k_ind;   # �������

     # ������� ���������� �������
       $total_count += $cur_cnt;
     }

       $user_count = count($total_cont);

   # ��������� ������� ������ : "1 2 3" �������� ������
     $ids_leader = "";
     $id_leader = 0;
     rsort($answers_leader);
     foreach ($answers_leader as $k => $v) {
      if ($id_leader > $v['cnt']) { break; }
      if ($v['cnt'] > 0) {
        if ($id_leader) { $com = " "; } else { $com = ""; }
        $ids_leader .= $com.$v['id'];

        $id_leader = $v['cnt'];
      }
     }

       $issues = array();
       $issues[0]['label'] = '';
       $issues[0]['answer'] = cp_text($my_survey_issues['title']);      # ��������
       $issues[0]['answers'] = $answers;
       $issues[0]['total'] = $total_count;
       $issues[0]['leaders'] = (string)$ids_leader;   # 1 = �� ������

     return array(
           'issues' => $issues,
           'user_count' => $user_count,  # ���������� ���������������
           'label' => ''
      );
}

function vote_survey($survey_id, $author_id){

     $my_survey = sql_row("SELECT * FROM `my_survey` WHERE `id` = '".$survey_id."' LIMIT 1;");
     $my_survey_issues = sql_row("SELECT * FROM `my_survey_issues` WHERE `survey_id` = '".$survey_id."' LIMIT 1;");

     $group_id = $my_survey['id'];
     $issue_id = $my_survey_issues['id'];

     $survey_groups_issues_id = 0;
     $survey_groups_issues = array();
     $survey_groups_issues[$survey_groups_issues_id]['title'] = cp_text($my_survey_issues['title']);
     $survey_groups_issues[$survey_groups_issues_id]['label'] = cp_text($my_survey_issues['label']);
     $survey_groups_issues[$survey_groups_issues_id]['label_f'] = $my_survey_issues['label_f'];
     $survey_groups_issues[$survey_groups_issues_id]['comment'] = $my_survey_issues['comment'];
     $survey_groups_issues[$survey_groups_issues_id]['answer'] = $my_survey_issues['answer'];
     $survey_groups_issues[$survey_groups_issues_id]['answer_f'] = $my_survey_issues['answer_f'];
     $survey_groups_issues[$survey_groups_issues_id]['type'] = $my_survey_issues['type'];   # checkbox_m  = ��������� �������, radio
     $survey_groups_issues[$survey_groups_issues_id]['html'] = $my_survey_issues['html'];
     $survey_groups_issues[$survey_groups_issues_id]['value'] = cp_text($my_survey_issues['value']); # 'sdf<br />ss'
     $survey_groups_issues[$survey_groups_issues_id]['value_f'] = cp_text($my_survey_issues['value_f']);
     $survey_groups_issues[$survey_groups_issues_id]['default'] = $my_survey_issues['default'];
     $survey_groups_issues[$survey_groups_issues_id]['index'] = $my_survey_issues['index'];
     $survey_groups_issues[$survey_groups_issues_id]['survey_id'] = $survey_id;
     $survey_groups_issues[$survey_groups_issues_id]['width'] = $my_survey_issues['width'];
     $survey_groups_issues[$survey_groups_issues_id]['height'] = $my_survey_issues['height'];
     $survey_groups_issues[$survey_groups_issues_id]['required'] = $my_survey_issues['required'];
     $survey_groups_issues[$survey_groups_issues_id]['horizontal'] = $my_survey_issues['horizontal'];
     $survey_groups_issues[$survey_groups_issues_id]['check_type'] = $my_survey_issues['check_type'];
     $survey_groups_issues[$survey_groups_issues_id]['issue_id'] = $issue_id;


         $survey_groups_id = 0;
         $survey_groups = array();
         $survey_groups[$survey_groups_id]['group_id'] = $group_id;
         $survey_groups[$survey_groups_id]['pos'] = 0;
         $survey_groups[$survey_groups_id]['title'] = cp_text($my_survey['title']);
         $survey_groups[$survey_groups_id]['label'] = cp_text($my_survey['label']);
         $survey_groups[$survey_groups_id]['rd_acc'] = '';  # community
         $survey_groups[$survey_groups_id]['can_public'] = $my_survey['can_public'];
         $survey_groups[$survey_groups_id]['public'] = $my_survey['public'];
         $survey_groups[$survey_groups_id]['can_edit'] = '1';
         $survey_groups[$survey_groups_id]['issues'] = $survey_groups_issues;

         $survey_issues = array();
         $survey_issues[$issue_id]['issue_id'] = $issue_id;
         $survey_issues[$issue_id]['result'] = "0";


     $survey = array();

     $survey['survey_id'] = $survey_id;
     $survey['title'] = cp_text($my_survey['title']);
     $survey['label'] = cp_text($my_survey['label']);
     $survey['rd_acc'] = $my_survey['rd_acc'];      # community
     $survey['can_public'] = $my_survey['can_public'];
     $survey['public'] = $my_survey['public'];
     $survey['date_open'] = date("Y-d-m");  # 31-12-2036
     $survey['date_close'] = '2036-12-31';
     $survey['type'] = $my_survey['type'];
     $survey['groups'] = $survey_groups;
     $survey['changed'] = $my_survey['changed'];

     $survey['issues'] = $survey_issues;


    # �������� ������

     $r_survey_issues = array();
     $r_survey_issues[$issue_id]['issue_id'] = $issue_id;
     $r_survey_issues[$issue_id]['result'] = "0";

     $r_survey_survey = array();
     $r_survey_survey['author_id'] = $author_id;
     $r_survey_survey['can_edit'] = '0';
     $r_survey_survey['survey_id'] = $survey_id;
     $r_survey_survey['author_title'] = cp_text($my_survey['owner_title']);
     $r_survey_survey['rd_acc'] = "all";


     $r_survey_groups = array();
     $r_survey_groups[$group_id]['group_id'] = $group_id;
     $r_survey_groups[$group_id]['rd_acc'] = "all";
     $r_survey_groups[$group_id]['can_change'] = "1";

       $r_survey = array();
       $r_survey['issues'] = $r_survey_issues;
       $r_survey['survey'] = $r_survey_survey;
       $r_survey['groups'] = $r_survey_groups;



        return ((array(
             'survey' => $survey,
             'r_survey' => $r_survey,
             'label' => ''
        )));

}

/***------------------------------------------
 * �������� �����
 **/

if (@$_POST['cmd'] == 'get_survey') {

 $survey_id = @$_POST['survey_id'];
 $author_id = @$_POST['author_id'];

 $my_user = sql_row("SELECT * FROM `my_users` WHERE `current_id` = '".$author_id."' LIMIT 1;");

   /*if (empty($my_user['current_id'])) {
    die("�� �� ��������������");                      # ��������� ������������ ����� �������
   } else {
   }*/




   $answer_type = 0;  # 0=�����, 1=����������, 2=���������� �����������

    if (is_vote($survey_id, $author_id) || empty($my_user['current_id'])) {
     $answer_type = 2;  # 2 = ���������� �����������
    }

     if ($answer_type == 2) {
        die("OK ".$answer_type." ".json_encode(vote_results($survey_id)));
     } else {

        die("OK ".$answer_type." ".json_encode(vote_survey($survey_id, $author_id)));

     }
}

/***------------------------------------------
 * �������� ��������� ������
 **/

if (@$_POST['cmd'] == 'get_vote_results') {

 $survey_id = $_POST['survey_id'];
 $my_survey_issues = sql_row("SELECT * FROM `my_survey_issues` WHERE `survey_id` = '".$survey_id."' LIMIT 1;");

  if (empty($my_survey_issues['id'])) {
    die("����� �� ������");
  } else {
    die("OK ".json_encode(vote_results($survey_id)));
  }
}



/***------------------------------------------
 * �������� �������� ��� ���������� ������
 **/

if (@$_POST['cmd'] == 'get') {

 $groups = array();

 $survey = array();
 $survey['groups'] = array();
 $survey['survey_id'] = 1;
 $survey['author_id'] = 1;

 $survey['comment'] = 1;
 $survey['title'] = 'title';
 $survey['label'] = 'label';
 $survey['write'] = '111';
 $survey['results'] = 1;


 $survey['issues'] = array();
 $survey['groups'][] = $in_groups;
 $groups[0]['issues'] = $in_groups;

 $r_survey = array();
 $r_survey = $survey;

      die("OK ".json_encode(array(
           'survey' => $survey,
           'r_survey' => $r_survey,
           'groups' => $groups,
           'title' => '123',
           'label' => '456',
           'rd_acc' => 1,
           'can_change' => 1,
           'survey_id' => 1,
           'author_id' => 1,
           'write' => 'ddddd',
           'results' => 'ddddd'
      )));
}

/***------------------------------------------
 * �������� survey.rdacc
 **/

if (@$_POST['cmd'] == 'survey.rdacc') {

 $survey_id = $_POST['survey_id'];
 $author_id = $_POST['author_idy_id'];
 $photo = array();
 die("qOK");
}

/***------------------------------------------
 * �������� group.rdacc
 **/

if (@$_POST['cmd'] == 'group.rdacc') {

 $survey_id = $_POST['survey_id'];
 $author_id = $_POST['author_idy_id'];
 $photo = array();
 die("1OK");
}


echo "������ �������";
?>