<?
 session_start();
 include("connect.php");
 include("functions.php");

function paste_script() {
?>
<script>
function ToggleExtendedSearch()
{
  $("extended_search").toggle();
  $("search_extended").value = $("extended_search").visible() ? 1 : 0;
  $("extended_search_title").firstChild.nodeValue = $("extended_search").visible() ? "������� � �������� ������" : "������� � ������������ ������";
}
function UpdateSearchCheckboxes()
{
  if ($("search_space_art").checked || $("search_space_art_title").checked ||
    $("search_space_art_author").checked || $("search_space_art_owner").checked ||
    $("search_space_art_tags").checked)
  {
    $("search_art").checked = true;
  }
  else
  {
    $("search_art").checked = false;
  }
  if ($("search_space_album_body").checked || $("search_space_album_title").checked ||
    $("search_space_album_author").checked || $("search_space_album_owner").checked ||
    $("search_space_album_tags").checked)
  {
    $("search_album").checked = true;
  }
  else
  {
    $("search_album").checked = false;
  }
  if ($("search_space_photo_body").checked || $("search_space_photo_title").checked ||
    $("search_space_photo_author").checked || $("search_space_photo_owner").checked ||
    $("search_space_photo_tags").checked)
  {
    $("search_photo").checked = true;
  }
  else
  {
    $("search_photo").checked = false;
  }
  /*if ($("search_space_audioalbum_body").checked || $("search_space_audioalbum_title").checked ||
    $("search_space_audioalbum_author").checked || $("search_space_audioalbum_owner").checked ||
    $("search_space_audioalbum_tags").checked)
  {
    $("search_audioalbum").checked = true;
  }
  else
  {
    $("search_audioalbum").checked = false;
  }*/
  /*if ($("search_space_track_body").checked || $("search_space_track_title").checked ||
    $("search_space_track_author").checked || $("search_space_track_owner").checked ||
    $("search_space_track_tags").checked)
  {
    $("search_track").checked = true;
  }
  else
  {
    $("search_track").checked = false;
  }*/
  if ($("search_space_comment_body").checked || $("search_space_comment_author").checked ||
    $("search_space_comment_owner").checked)
  {
    $("search_comment").checked = true;
  }
  else
  {
    $("search_comment").checked = false;
  }
  if ($("search_space_user").checked || $("search_space_user_body").checked ||
    $("search_space_user_tags").checked)
  {
    $("search_user").checked = true;
  }
  else
  {
    $("search_user").checked = false;
  }
  /*if ($("search_space_group").checked || $("search_space_group_body").checked ||
    $("search_space_group_tags").checked)
  {
    $("search_group").checked = true;
  }
  else
  {
    $("search_group").checked = false;
  }*/
}
function SearchArt()
{
  var checked = $("search_art").checked;
  $("search_space_art").checked = checked;
  $("search_space_art_title").checked = checked;
  $("search_space_art_author").checked = checked;
  $("search_space_art_owner").checked = checked;
  $("search_space_art_tags").checked = checked;
}
function SearchAlbum()
{
  var checked = $("search_album").checked;
  $("search_space_album_body").checked = checked;
  $("search_space_album_title").checked = checked;
  $("search_space_album_author").checked = checked;
  $("search_space_album_owner").checked = checked;
  $("search_space_album_tags").checked = checked;
}
function SearchPhoto()
{
  var checked = $("search_photo").checked;
  $("search_space_photo_body").checked = checked;
  $("search_space_photo_title").checked = checked;
  $("search_space_photo_author").checked = checked;
  $("search_space_photo_owner").checked = checked;
  $("search_space_photo_tags").checked = checked;
}
/*function SearchAudioAlbum()
{
  var checked = $("search_audioalbum").checked;
  $("search_space_audioalbum_body").checked = checked;
  $("search_space_audioalbum_title").checked = checked;
  $("search_space_audioalbum_author").checked = checked;
  $("search_space_audioalbum_owner").checked = checked;
  $("search_space_audioalbum_tags").checked = checked;
}
function SearchTrack()
{
  var checked = $("search_track").checked;
  $("search_space_track_body").checked = checked;
  $("search_space_track_title").checked = checked;
  $("search_space_track_author").checked = checked;
  $("search_space_track_owner").checked = checked;
  $("search_space_track_tags").checked = checked;
}*/
function SearchComment()
{
  var checked = $("search_comment").checked;
  $("search_space_comment_body").checked = checked;
  $("search_space_comment_author").checked = checked;
  $("search_space_comment_owner").checked = checked;
}
function SearchUser()
{
  var checked = $("search_user").checked;
  $("search_space_user_body").checked = checked;
  $("search_space_user").checked = checked;
  $("search_space_user_tags").checked = checked;
}
/*function SearchGroup()
{
  var checked = $("search_group").checked;
  $("search_space_group_body").checked = checked;
  $("search_space_group").checked = checked;
  $("search_space_group_tags").checked = checked;
}*/
community.init.add(UpdateSearchCheckboxes);
</script>
<?
}

 include("index_header.php");

?>


            <div class="block">
              <h2>�����</h2>
              <div class="content">
                <form>
                  <p class="mb10" style="width:100%;float:left;">
                    <input class="inp_text mr5" type="text" name="q" value="<?= @$_GET['q'] ?>" />
                    <input class="button mr5" type="submit" value="������" />
                    <input type="hidden" name="do_search" value="1" />
                    <input type="hidden" id="search_extended" name="search_extended" value="0" />
                    <a href="#" style="position:relative;top:3px;" onclick="ToggleExtendedSearch();return false;"><small id="extended_search_title">������� � ������������ ������</small></a>
                  </p>

                  <div class="clear mb10" id="extended_search" style="display: none;">
                  <table class="table" style="line-height:20px;">
                    <col style="width:200px;" />
                    <col style="width:200px;" />
                    <col />
                    <tr>
                      <td rowspan="8">������</td><td><input type="checkbox" value="on" id="search_art" onclick="SearchArt();">������</input> ��:</td>
                      <td>
                        <input type="checkbox" name="search_space_art" id="search_space_art" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">������</input>
                        <input type="checkbox" name="search_space_art_title" id="search_space_art_title" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">����������</input>
                        <input type="checkbox" name="search_space_art_author" id="search_space_art_author" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">�������</input>
                        <input type="checkbox" name="search_space_art_owner" id="search_space_art_owner" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">���������� �����</input>
                        <input type="checkbox" name="search_space_art_tags" id="search_space_art_tags" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">������</input>
                      </td>
                    </tr>

                    <tr>
                      <td><input type="checkbox" value="on" id="search_album" onclick="SearchAlbum();">�������</input> ��:</td>
                      <td>
                        <input type="checkbox" name="search_space_album_body" id="search_space_album_body" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">��������</input>
                        <input type="checkbox" name="search_space_album_title" id="search_space_album_title" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">����������</input>
                        <input type="checkbox" name="search_space_album_author" id="search_space_album_author" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">�������</input>
                        <input type="checkbox" name="search_space_album_owner" id="search_space_album_owner" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">���������� �����</input>
                        <input type="checkbox" name="search_space_album_tags" id="search_space_album_tags" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">������</input>
                      </td>
                    </tr>

                    <tr>
                      <td><input type="checkbox" value="on" id="search_photo" onclick="SearchPhoto();">����������</input> ��:</td>
                      <td>
                        <input type="checkbox" name="search_space_photo_body" id="search_space_photo_body" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">��������</input>
                        <input type="checkbox" name="search_space_photo_title" id="search_space_photo_title" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">����������</input>
                        <input type="checkbox" name="search_space_photo_author" id="search_space_photo_author" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">�������</input>
                        <input type="checkbox" name="search_space_photo_owner" id="search_space_photo_owner" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">���������� �����</input>
                        <input type="checkbox" name="search_space_photo_tags" id="search_space_photo_tags" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">������</input>
                      </td>
                    </tr>
                    <!--<tr>
                      <td><input type="checkbox" value="on" id="search_audioalbum" onclick="SearchAudioAlbum();">������������</input> ��:</td>
                      <td>
                        <input type="checkbox" name="search_space_audioalbum_body" id="search_space_audioalbum_body" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">��������</input>
                        <input type="checkbox" name="search_space_audioalbum_title" id="search_space_audioalbum_title" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">����������</input>
                        <input type="checkbox" name="search_space_audioalbum_author" id="search_space_audioalbum_author" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">�������</input>
                        <input type="checkbox" name="search_space_audioalbum_owner" id="search_space_audioalbum_owner" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">���������� �����</input>
                        <input type="checkbox" name="search_space_audioalbum_tags" id="search_space_audioalbum_tags" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">������</input>
                      </td>
                    </tr>-->

                    <!--<tr>
                      <td><input type="checkbox" value="on" id="search_track" onclick="SearchTrack();">�����������</input> ��:</td>
                      <td>
                        <input type="checkbox" name="search_space_track_body" id="search_space_track_body" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">��������</input>
                        <input type="checkbox" name="search_space_track_title" id="search_space_track_title" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">����������</input>
                        <input type="checkbox" name="search_space_track_author" id="search_space_track_author" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">�������</input>
                        <input type="checkbox" name="search_space_track_owner" id="search_space_track_owner" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">���������� �����</input>
                        <input type="checkbox" name="search_space_track_tags" id="search_space_track_tags" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">������</input>
                      </td>
                    </tr>-->
                    <tr>
                      <td><input type="checkbox" value="on" id="search_comment" onclick="SearchComment();">�����������</input> ��:</td>
                      <td>
                        <input type="checkbox" name="search_space_comment_body" id="search_space_comment_body" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">������</input>
                        <input type="checkbox" name="search_space_comment_author" id="search_space_comment_author" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">�������</input>
                        <input type="checkbox" name="search_space_comment_owner" id="search_space_comment_owner" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">���������� �����</input>
                      </td>
                    </tr>

                    <tr>
                      <td><input type="checkbox" value="on" id="search_user" onclick="SearchUser();">����������</input> ��:</td>
                      <td>
                        <input type="checkbox" name="search_space_user" id="search_space_user" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">������</input>
                        <input type="checkbox" name="search_space_user_body" id="search_space_user_body" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">��������</input>
                        <input type="checkbox" name="search_space_user_tags" id="search_space_user_tags" value="on" checked="checked" onclick="UpdateSearchCheckboxes();">���������</input>
                      </td>
                    </tr>

                    <!--<tr>
                      <td><input type="checkbox" value="on" id="search_group" onclick="SearchGroup();">������</input> ��:</td>
                      <td>
                        <input type="checkbox" name="search_space_group" id="search_space_group" value="on" checked="checked">���������</input>
                        <input type="checkbox" name="search_space_group_body" id="search_space_group_body" value="on" checked="checked">��������</input>
                        <input type="checkbox" name="search_space_group_tags" id="search_space_group_tags" value="on" checked="checked">���������</input>
                      </td>
                    </tr>-->

                    <tr id="search_type_tr">
                      <td class="pb5">�����:</td>
                      <td colspan="2">
                        <input style="border-style: none; background-color: c5ced5;" type="radio" name="search_type" value="any" checked="checked">����� ����</input>
                        <input style="border-style: none; background-color: c5ced5;" type="radio" name="search_type" value="all" >���� ����</input>
                        <input style="border-style: none; background-color: c5ced5;" type="radio" name="search_type" value="phrase" >������� ���������� �����</input>
                        <input style="border-style: none; background-color: c5ced5;" type="radio" name="search_type" value="boolean" >� ������� ���������</input> <span onclick="$('search_expression_info').show()" style="cursor: pointer"><b>[?]</b></span>
                      </td>
                    </tr>

                    <tr id="search_sort_tr">
                      <td class="pb5">����������� ��:</td>
                      <td colspan="2">
                        <input style="border-style: none; background-color: c5ced5;" type="radio" name="search_sort" value="relevance" checked="checked">�������������</input>
                        <input style="border-style: none; background-color: c5ced5;" type="radio" name="search_sort" value="datetime" >����</input>
                      </td>
                    </tr>

                    <tr>
                      <td class="pb5">������ �� �����:</td>
                      <td colspan="2">
                        <div class="fl"><input class="inp_text" type="text" name="search_date1" id="search_date1" value="" /><script type="text/javascript">community.init.add(function() { jQuery("#search_date1").datepicker(); });</script></div><span class="fl ml5 mr5"> - </span><div class="fl"><input class="inp_text" type="text" name="search_date2" id="search_date2" value="" /><script type="text/javascript">community.init.add(function() { jQuery("#search_date2").datepicker(); });</script></div>
                      </td>
                    </tr>
                  </table>
                </form>

                <div id="search_expression_info" style="display:none">
                  <p>� ��������� ����� �������������� ���������:</p>
                  <ul class="ml10 mb10">
                    <li><b>&amp;</b> ��� ������ - ���������� "�"</li>
                    <li><b>|</b> - ���������� "���"</li>
                    <li><b>-</b> ��� <b>!</b> - ���������� "��"</li>
                    <li><b>()</b> - ��� �����������</li>
                  </ul>

                  <p><b>�������:</b></p>
                  <ul class="ml10 mb10">
                    <li>hello &amp; world</li>
                    <li>hello | world</li>
                    <li>hello -world</li>
                    <li>hello !world</li>
                    <li>( hello world )</li>
                    <li>( cat -dog ) | ( cat -mouse )</li>
                  </ul>
                  <p><span onclick="$('search_expression_info').hide()" style="cursor: pointer"><b>[������ ���������]</b></span></p>
                </div>
              </div>
              <? if (@$_GET['q']) { ?>
              <p class=" clear mb20">����� ������� <b>0</b> ������� �� <b>0.001</b> ������</p>
              <? } ?>
              <script>
                community.show_pages('0','1');
              </script>


                <script>
                  community.show_pages('0','1');
                </script>

                <div class="clear"><!-- --></div>
              </div>

<? include("index_footer.php"); ?>