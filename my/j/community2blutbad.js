var game_linker = new Object();

game_linker.game_name = function() { return "Blutbad"; };
game_linker.game_domain = function() { return "http://www.blutbad.ru"; };
game_linker.register_link = function() { return 'http://'+this._blutbad_domain()+'.blutbad.ru/reg.php'; };
game_linker.user_info_link = function(user)
  {
    return 'http://'+this._blutbad_domain()+'.blutbad.ru/inf.php?user='+user;
  };
game_linker.user_info_icon = function() { return 'http://img.blutbad.ru/i/inf.gif'; };
game_linker.user_info_icon_male = function() { return 'http://img.blutbad.ru/i/infM.gif'; };
game_linker.user_info_icon_female = function() { return 'http://img.blutbad.ru/i/infF.gif'; };
game_linker.user_info_icon_width = function() { return 12; };
game_linker.user_info_icon_height = function() { return 12; };

game_linker.null_img_url = function() { return "http://img.blutbad.ru/i/null.gif"; };

game_linker.smiles_url = function() { return "http://img.blutbad.ru/i/smilies/"; };

game_linker.smiles =
		["jhi","hi","diler","ok","eusa","beggar","upset","notknow","kz","tooth","neigh","roll","sleep","grimace","kuku",
		 "smash","pray2","nunu","badtease","plak","pank","rupor","kruger","above","acute","alc","alcoholic","amaze","angel",
                 "applause","baby","banghead","basketball","blabla","blind","blush","box","brainy","bye","camomile","campfire","close",
                 "coffee","comp","confeta","crazy","cry2","cry","cymbals","dance3","dance","declare","devil","dirge","doctor","dwarf",
                 "eat","encore","figa","fingal","fu","furious2","furious","gitler","good2","good","hang","hb","horse","itchy","king",
                 "kloun","laugh","love2","mishka","nafik","newy","no","pirate","prayer","preved","regulation","rendezvous","roar","roast","sad",
                 "scare","scream","secret","serenity","sick","smile","smoke","smoked","sos","spartak","stinker","stop","stoped","superstition",
                 "tongue2","tongue","vdv","warrior","whistle","who","writer","yahoo","yawn","polet","tongue3","gazeta","victory","air_kiss","gaz",
                 "igry","grust","flover","juggler",
                 "ass","hiya","agree","friday","friends","punish","kissmy","duel","beer","beer2","kiss2","kiss",
                 "lariat","love","affection","mother","noshbask","kosa","tango","serenada"
		];

game_linker.smiles_x =
		[36,29,30,30,30,35,20,38,20,38,27,30,30,18,30,35,32,25,30,26,28,37,40,30,24,51,45,40,29,32,15,28,
                 36,36,30,37,44,29,30,31,40,46,42,50,30,38,40,34,31,39,31,40,41,41,43,41,43,40,36,23,18,28,18,23,
                 36,36,32,52,74,35,28,43,22,35,44,38,47,18,45,31,50,29,49,31,52,22,40,36,22,38,33,22,40,31,32,61,
                 31,38,38,36,36,26,43,45,28,33,38,44,22,41,18,25,24,26,36,32,18,52,38,36,28,44,54,50,50,63,101,50,
                 60,61,66,78,45,52,41,69,48,68,33
		];

game_linker.smiles_y =
		[26,25,30,26,25,26,23,23,25,27,20,25,24,22,26,29,30,24,25,23,31,20,27,26,23,43,32,30,26,28,26,23,
                 37,24,27,24,27,25,25,29,27,39,26,24,27,24,24,26,30,25,26,27,37,37,25,46,31,34,27,23,25,24,18,23,
                 23,25,42,37,47,25,28,30,24,26,25,31,30,23,35,26,33,29,34,23,25,24,31,22,27,27,25,25,31,27,34,36,
                 26,25,24,26,23,24,29,30,27,30,37,28,25,25,24,31,25,27,25,34,23,32,33,
                 25,30,22,26,27,31,26,32,27,26,29,34,29,33,26,24,28,23,28,55
		];

game_linker.smiles_page =
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2
		];

game_linker._blutbad_domain = function()
  {
   var domains = [ "damask", "alamut" ];
   var index = (Math.round((Math.random()*(domains.length - 1))));
   return domains[index];
  };

function addAllErr(text, tim){
	if(!tim)
		var tim = 12500;
  jQuery(document).ready(function(){
	jQuery('.privacy_err').remove();
	jQuery('body').append('<div class="privacy_err no_display">'+text+'</div>');
	jQuery('.privacy_err').fadeIn('fast');
	setTimeout("jQuery('.privacy_err').fadeOut('fast')", tim);
  });
}