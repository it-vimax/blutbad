var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

//Manages linked areas with headers, only one of them is visible.
//First areas are added via add method, and than all of them are displayed using show method.
Community.LinkedAreas = Class.create({
  initialize: function(container,options)
  {
  if (!options) options = {};
  this.id = Community.LinkedAreas.id++;
  this.container = $(container);
  if (!this.container) throw new Error("����������� ��������� ��� ������� ��������� ��������");
  this.areas = new Array();
  this.selected = 0;
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.container = null;
  },

  stop: function()
  {
  var header_element;
  for (var i=0;i<this.areas.length;i++)
  {
    header_element = $(this.area_header_id(i));
    if (header_element) header_element.stopObserving("click");
  }
  },

  area_header_id: function(index) { return "Community_LinkedAreas_Header_"+index+"_"+this.id; },
  area_content_id: function(index) { return "Community_LinkedAreas_Content_"+index+"_"+this.id; },

  add: function(header,content)
  {
  this.areas.push({header:header,content:content});
  },

  show: function()
  {
  var t_this = this;
  this.stop();
  this.container.update("");
  if (this.areas.length == 0) return;
  this.areas.each(function(value,index)
    {
      new Insertion.Bottom(t_this.container,t_this.html(index));
      $(t_this.area_header_id(index)).observe("click",t_this.select.bindAsEventListener(t_this,index));
    });
  this.selected = 0;
  this.container.show();
  },

  html: function(index)
  {
  var value = '<h2 ';
  if (index == 0) value += 'class="bold" ';
  value += 'style="cursor:pointer;" id="'+this.area_header_id(index)+'"><span>'+this.areas[index].header+'</span></h2>';
  value += '<div class="scroll" id="'+this.area_content_id(index)+'"';
  if (index > 0) value += ' style="display: none;"';
  value += '><div class="scroll_content">'+this.areas[index].content+'</div></div>';
  return value;
  },

  select: function(event,index)
  {
  $(this.area_header_id(this.selected)).removeClassName("bold");
  $(this.area_content_id(this.selected)).hide();
  $(this.area_content_id(index)).show();
  $(this.area_header_id(index)).addClassName("bold");
  this.selected = index;
  }
});

Community.LinkedAreas.id = 0;