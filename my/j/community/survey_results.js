var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Survey.Survey = Class.create({
  initialize: function(element,survey_id,author_id,options)
  {
  if (!options) options = {};
  this.id = Community.Survey.Survey.id++;
  this.survey_id = survey_id;
  this.author_id = author_id;
  this.element = $(element);
  this.options = options;
  if (!this.element) throw new Error("����������� ��������� ��� ������!");
  this.element.update("���� ��������...").show();
  new Ajax.Request(community.url.survey_results_ajax,
       {
        parameters: {cmd:"get_survey",survey_id:survey_id,author_id:author_id},
        onSuccess: this.init_success.bind(this),
        onFailure: community.ajax_failure.bind(community)
       });
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  if (this.survey) this.survey.clean();
  this.element = null;
  },

  init_success: function(transport)
  {  
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { this.element.hide(); return; }
  var answer_type = response[1];
  response.splice(0,2);

  switch(answer_type)
  {
    case "0":this.survey = new Community.Survey.Answer(this.element,0,0,response.join(" "),this.options); return;    // �����
    case "1": this.survey = new Community.Survey.Results(this.element,0,0,response.join(" "),this.options); return;   // ����������
    case "2": this.survey = new Community.Survey.Vote.Results(this.element,0,response.join(" "),this.options); return;// ���������� �����������
  }
  }
});

Community.Survey.Survey.id = 0;

//--------------------------------------------------------------------------------

Community.Survey.Results = Class.create({
  initialize: function(element,survey_id,author_id,results,options)
  {
  if (!options) options = {};
  this.hide_title = options.hide_title || false;
  this.id = Community.Survey.Results.id++;
  this.element = $(element);
  if (!this.element) throw new Error("����������� ��������� ��� ����������� ������!");
  if (!results)
  {
    new Ajax.Request(community.url.survey_results_ajax,
       {
        parameters: {cmd:"get",survey_id:survey_id,author_id:author_id},
        onSuccess: this.init_success.bind(this),
        onFailure: community.ajax_failure.bind(community)
       });
    this.element.update("1���� ��������...").show();
  }
  else
  {
    if (typeof results == "string") this.survey = new Community.Survey(results.evalJSON(true));
    else this.survey = new Community.Survey(results);
    this.unfilter_survey();
    this.display();
  }
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.element = null;
  },

  stop: function()
  {
  var rd_acc_element = $(this.survey_rd_acc_id());
  if (rd_acc_element) rd_acc_element.stopObserving("change");
  var change_element = $(this.change_button_id());
  if (change_element) change_element.stopObserving("click");
  var length = this.survey.get("groups").length;
  for (var i=0;i<length;i++)
  {
    var group_rd_acc_element = $(this.group_rd_acc_id(i));
    if (group_rd_acc_element)
    {
      group_rd_acc_element.stopObserving("change");
    }
  }
  },

  unfilter_survey: function()
  {
   this.survey.set("title",community.unfilter_body(this.survey.get("title")));
   this.survey.set("label",community.unfilter_body(this.survey.get("label")));
   var groups_length = this.survey.get("groups").length;

   for (var group_index=0;group_index < groups_length;group_index++)
   {
    var group = this.survey.get("groups")[group_index];
    group.set("title",community.unfilter_body(group.get("title")));
    group.set("label",community.unfilter_body(group.get("label")));
    var issues_length = group.get("issues").length;
    for (var issue_index=0;issue_index < issues_length;issue_index++)
    {
     var issue = group.get("issues")[issue_index];
     issue.set("title",community.unfilter_body(issue.get("title")));
     issue.set("comment",community.unfilter_body(issue.get("comment")));
     issue.set("value",community.unfilter_body(issue.get("value")));
     issue.set("label",community.unfilter_body(issue.get("label")));
     issue.set("answer",community.unfilter_body(issue.get("answer")));
     issue.set("result",community.unfilter_body(issue.get("result")));
    }
   }
  },

  survey_rd_acc_id: function() { return "Community_SurveyResults_Survey_RdAcc_"+this.id; },
  group_rd_acc_id: function(index) { return "Community_SurveyResults_Group_RdAcc_"+index+"_"+this.id; },
  change_button_id: function() { return "Community_SurveyResults_ChangeButton_"+this.id; },

  init_success: function(transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������ ��������� ����������� ������: "+transport.responseText); return; }
  response.splice(0,1);
  this.survey = new Community.Survey(response.join(" ").evalJSON(true));
  this.unfilter_survey();
  this.display();
  },

  change_survey_rd_acc: function(event)
  {
  new Ajax.Request(community.url.survey_results_ajax,
       {
        parameters: {cmd:"survey.rdacc",author_id:this.survey.get("author_id"),
               survey_id:this.survey.get("survey_id"),
               rd_acc:$F(this.survey_rd_acc_id()),check:community.prop.check
              },
        onSuccess: this.change_rd_acc_success.bind(this),
        onFailure: community.ajax_failure.bind(community)
       });
  },

  change_group_rd_acc: function(event,group_index)
  {
  new Ajax.Request(community.url.survey_results_ajax,
       {
        parameters: {cmd:"group.rdacc",author_id:this.survey.get("author_id"),
               group_id:this.survey.group_get(group_index,"group_id"),
               rd_acc:$F(this.group_rd_acc_id(group_index)),check:community.prop.check
              },
        onSuccess: this.change_rd_acc_success.bind(this),
        onFailure: community.ajax_failure.bind(community)
       });
  },

  change_rd_acc_success: function(transport)
  {
  if (transport.responseText != "OK") { alert("������ ��������� ����: "+transport.responseText); return; }
  },

  change: function(event)
  {
  this.stop();
  new Community.Survey.Answer(this.element,this.survey.get("survey_id"),this.survey.get("author_id"),'',{hide_title:this.hide_title});
  },

  display: function()
  {
  this.element.update(this.html()).show();

  if (this.survey.get("rd_acc"))
  {
    $(this.survey_rd_acc_id()).onchange=this.change_survey_rd_acc.bindAsEventListener(this);
    makeSelectBox("#"+this.survey_rd_acc_id());
  }
  if (this.survey.get("can_change"))
  {
    $(this.change_button_id()).observe("click",this.change.bindAsEventListener(this));
  }
  var length = this.survey.get("groups").length;
  for (var i=0;i < length;i++)
  {
    var group_rd_acc_element = $(this.group_rd_acc_id(i));
    if (group_rd_acc_element)
    {
      group_rd_acc_element.onchange=this.change_group_rd_acc.bindAsEventListener(this,i);
      makeSelectBox("#"+this.group_rd_acc_id(i));
    }
  }
  },

  html: function()
  {
  var value='';

  var rd_acc = this.survey.get("rd_acc");
  if (rd_acc)
  {
    value += '<div class="clear select-wrap" id="visibleLevelWrap"><label for="visibleLevel">����� ��������</label> <span class="por fl"><select class="selectbox" style="width:130px;" id="'+this.survey_rd_acc_id()+'">';
    community.acc_title.each(function(pair)
          {
      value=value+'<option value="'+pair.key+'"';
      if (pair.key == rd_acc) value += ' selected="selected"';
      value += '>'+pair.value+'</option>';
          });
    value += '</select></span></div>';
  }

  if (!this.hide_title)
  {
    value += '<h3 class="mb30 pt5 mt0">'+community.filter_body(this.survey.get("title"))+'</h3>';
  }
  if (this.survey.get("label"))
  {
    value=value+'<p><i>'+community.filter_body(this.survey.get("label"))+'</i></p>';
  }

  value += '<div class="border">';
  var t_this = this;
  this.survey.get("groups").each(function(group,index)
          {
          value=value+t_this.group_html(index);
          });
  value += '</div>';
  if (this.survey.get("can_change"))
  {
    value += '<a class="button" id="'+this.change_button_id()+'">��������</a>';
  }
  return value;
  },

  group_html: function(group_index)
  {
  var value = '';
  if (this.survey.group_get(group_index,"label"))
  {
    value += '<i>'+community.filter_body(this.survey.group_get(group_index,"label"))+'</i>';
  }
  if (this.survey.get("groups").length > 1 && this.survey.group_get(group_index,"rd_acc"))
  {
    value += '<br />����� ��������: <select class="selectbox" style="width:130px;" id="'+this.group_rd_acc_id(group_index)+'">';
    var t_this = this;
    community.acc_title.each(function(pair)
          {
      value += '<option value="'+pair.key+'"';
      if (pair.key == t_this.survey.group_get(group_index,"rd_acc")) value += ' selected="selected"';
      value += '>'+pair.value+'</option>';
          }
         );
    value += '</select>';
  }
  var t_this = this;
  value += '<table class="table" style="padding-top:10px;">';
  this.survey.group_get(group_index,"issues").each(function(group,index)
          {
          value=value+t_this.issue_html(group_index,index);
          });
  value += '</table>';
  return value;
  },

  issue_html: function(group_index,issue_index)
  {
  var value='';
  var answer = community.filter_body(this.survey.issue_get(group_index,issue_index,"answer"));
  var label = community.filter_body(this.survey.issue_get(group_index,issue_index,"label"));

  if (answer || label) value += '<tr><td class="p5">' + (answer ? answer : label) + '</td>';
  value += '<td class="p5">';
  var type = this.survey.issue_get(group_index,issue_index,"type");
  var result = community.filter_body(this.survey.issue_get(group_index,issue_index,"result"));
  var org_result = this.survey.issue_get(group_index,issue_index,"org_result");
  var issue_id = this.survey.issue_get(group_index,issue_index,"issue_id");

  if (this.survey.issue_get(group_index,issue_index,"index") != "0")
  {
    if (type != "check_m" && type != "list" && type != "string_m")
    {
      result = '<a href="'+community.url.survey_search+'?id='+issue_id+'&result='+org_result+'">'+result+'</a>';
    }
    if (type == "check_m" || type == "list")
    {
      var result_array = result.split(", ");
      var org_result_array = org_result.split(",");
      for (var i=0;i < result_array.length;i++)
      {
        result_array[i]='<a href="'+community.url.survey_search+'?id='+issue_id+'&result='+org_result_array[i]+'">'+result_array[i]+'</a>';
      }
      result = result_array.join(", ");
    }

    if (type == "string_m")
    {
      var result_array = result.split(", ");
      for (var i=0;i<result_array.length;i++)
      {
        result_array[i] ='<a href="'+community.url.survey_search+'?id='+issue_id+'&result='+result_array[i]+'">'+result_array[i]+'</a>';
      }
      result = result_array.join(", ");
    }
  }
  value += result;

  value += '</td></tr>'
  return value;
  }

});

Community.Survey.Results.id = 0;