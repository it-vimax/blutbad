var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Comments = Class.create({
  initialize: function(element,root_id,root_comments,can_post,options)
  {
  options = options || {};
  this.id = Community.Comments.id++;
  this.element = $(element);
  this.root_id = root_id;
  this.can_post = (can_post - 0) || false;
  this.lastviewtime = options.lastviewtime || 0;

  if (!this.element) throw new Error("����������� ��������� ��� ������������!");
  new Insertion.Top(this.element,this.root_html());
  if (root_comments)
  {
    this.comments = root_comments;
    this.comments_hash = this.comments.inject(new Hash(),function(acc,value)
                   {
                value.parent = null;
                acc.set(value.id,value);
                return acc;
                   });
    $(this.open_all_comments_id()).observe("click",this.open_all_comments.bindAsEventListener(this));
    $(this.toggle_plain_view_id()).observe("click",this.toggle_plain_view.bindAsEventListener(this));
    this.display_comments(this.root_comments_id(),root_comments,0);
    if (this.can_post) this.display_new_comment(this.root_id);
  }
  else this.init();
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.element = null;
  },

  stop: function()
  {
   var open_all_element = $(this.open_all_comments_id());
   if (open_all_element) open_all_element.stopObserving("click");
   var toggle_plain_view_element = $(this.toggle_plain_view_id());
   if (toggle_plain_view_element) toggle_plain_view_element.stopObserving("click");
   this.stop_comments(this.comments);
   if (this.editor_new) { this.editor_new.stop(); this.editor_new = null; }
   if ($(this.new_comment_id(this.root_id)))
   {
     $(this.new_comment_id(this.root_id)).remove();
   }
  },

  stop_comments: function(comments)
  {
  var t_this = this;
  comments.each(function(value) { t_this.stop_comment(value); });
  },

  stop_comment: function(comment)
  {
  if (comment.childs) this.stop_comments(comment.childs);
  if (comment.editor_new) { comment.editor_new.stop(); comment.editor_new = null; }
  if (comment.editor_edit) { comment.editor_edit.stop(); comment.editor_edit = null; }
  var answers_element = $(this.comment_answers_button_id(comment.id));
  if (answers_element) answers_element.stopObserving("click");
  var answers_all_element = $(this.comment_answers_all_button_id(comment.id));
  if (answers_all_element) answers_all_element.stopObserving("click");
  var answer_element = $(this.comment_answer_button_id(comment.id));
  if (answer_element) answer_element.stopObserving("click");
  var edit_element = $(this.comment_edit_button_id(comment.id));
  if (edit_element) edit_element.stopObserving("click");
  var delete_element = $(this.comment_delete_button_id(comment.id));
  if (delete_element) delete_element.stopObserving("click");
  var hide_element = $(this.comment_hide_button_id(comment.id));
  if (hide_element) hide_element.stopObserving("click");
  var new_element = $(this.new_comment_button_id(comment.id));
  if (new_element) new_element.stopObserving("click");
  var edit_element = $(this.edit_comment_button_id(comment.id));
  if (edit_element) edit_element.stopObserving("click");
  if (comment.censure) { comment.censure.clean(); comment.censure = null; }
  },

  init: function()
  {
    $(this.root_comments_id()).update("���� ��������...");
    new Ajax.Request(community.url.comments_ajax,{
          parameters: { cmd:"get_root",id:this.root_id },
          onSuccess: this.init_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
        });
  },

  init_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������ ��������� ������������: "+transport.responseText); return; }
    response.splice(0,1);
    var result = response.join(" ").evalJSON(true);
    $(this.root_comments_id()).update("");
    this.comments = $A(result.root_comments);
    this.can_post = Number(result.can_post);
    if (!this.lastviewtime)
    {
      this.lastviewtime = result.lastviewtime || 0;
    }

    this.comments_hash = this.comments.inject(new Hash(),function(acc,value)
                   {
                value.parent = null;
                acc.set(value.id,value);
                return acc;
                   });
    $(this.open_all_comments_id()).observe("click",this.open_all_comments.bindAsEventListener(this));
    $(this.toggle_plain_view_id()).observe("click",this.toggle_plain_view.bindAsEventListener(this));
    this.display_comments(this.root_comments_id(),this.comments,0);
    if (this.can_post) this.display_new_comment(this.root_id);
  },

  toggle_plain_view: function()
  {
    this.stop();
    this.element.update("");
    new Community.Comments.Plain(this.element,this.root_id,'','','',
      {lastviewtime:this.lastviewtime});
  },

  open_all_comments_id: function() { return "Community_Comments_OpenAll_"+this.id; },
  toggle_plain_view_id: function() { return "Community_Comments_TogglePlainView_"+this.id; },
  root_comments_id: function() { return "Community_Comments_Root_"+this.id; },
  comment_id: function(id) { return "Community_Comments_Comment_"+id+"_"+this.id; },
  comment_body_id: function(id) { return "Community_Comments_Body_"+id+"_"+this.id; },
  comment_answers_button_id: function(id) { return "Community_Comments_Answers_Button_"+id+"_"+this.id; },
  comment_answers_all_button_id: function(id) { return "Community_Comments_Answers_All_Button_"+id+"_"+this.id; },

  comment_answer_button_id: function(id) { return "Community_Comments_Answer_Button_"+id+"_"+this.id; },
  comment_edit_button_id: function(id) { return "Community_Comments_Edit_Button_"+id+"_"+this.id; },
  comment_delete_button_id: function(id) { return "Community_Comments_Delete_Button_"+id+"_"+this.id; },
  comment_censure_id: function(id) { return "Community_Comments_Censure_"+id+"_"+this.id; },
  comment_hide_button_id: function(id) { return "Community_Comments_Hide_Button_"+id+"_"+this.id; },
  comment_level_id: function(id) { return "Community_Comments_Level_"+id+"_"+this.id; },
  comment_childs_id: function(id) { return "Community_Comments_Childs_"+id+"_"+this.id; },
  new_comment_id: function(root_id) { return "Community_Comments_New_"+root_id+"_"+this.id; },
  new_comment_body_id: function(root_id) { return "Community_Comments_New_Body_"+root_id+"_"+this.id; },
  new_comment_status_id: function(root_id) { return "Community_Comments_New_Status_"+root_id+"_"+this.id; },
  new_comment_button_id: function(root_id) { return "Community_Comments_New_Button_"+root_id+"_"+this.id; },
  edit_comment_id: function(root_id) { return "Community_Comments_Edit_"+root_id+"_"+this.id; },
  edit_comment_body_id: function(root_id) { return "Community_Comments_Edit_Body_"+root_id+"_"+this.id; },
  edit_comment_status_id: function(root_id) { return "Community_Comments_Edit_Status_"+root_id+"_"+this.id; },
  edit_comment_button_id: function(root_id) { return "Community_Comments_Edit_Edit_Button_"+root_id+"_"+this.id; },

  display_comments: function(element,comments,level,posted_id)
  {
  $(element).update(this.html(comments,level,posted_id)).show();
  comments.each(this.observe_comment.bind(this));
  },

  observe_comment: function(comment)
  {
  if (comment.cnt_child>0)
  {
    $(this.comment_answers_button_id(comment.id)).observe("click",this.toggle_answers.bindAsEventListener(this,comment.id));
    $(this.comment_answers_all_button_id(comment.id)).observe("click",this.toggle_answers_all.bindAsEventListener(this,comment.id));
  }
  if (comment.can_post - 0)
  {
    $(this.comment_answer_button_id(comment.id)).observe("click",this.toggle_new_comment.bindAsEventListener(this,comment.id));
  }
  if (comment.can_edit - 0)
  {
    $(this.comment_edit_button_id(comment.id)).observe("click",this.toggle_edit_comment.bindAsEventListener(this,comment.id));
  }
  if (comment.can_delete - 0)
  {
    $(this.comment_delete_button_id(comment.id)).observe("click",this.delete_comment.bindAsEventListener(this,comment.id));
  }
  if (comment.can_hide - 0)
  {
    $(this.comment_hide_button_id(comment.id)).observe("click",this.hide_comment.bindAsEventListener(this,comment.id));
  }
  if (comment.can_censure - 0)
  {
    comment.censure = new Community.Censure(this.comment_censure_id(comment.id),comment.id,comment.censure_enabled);
  }
  },

  display_new_comment: function(root_id)
  {
  if ($(this.new_comment_id(root_id)))
  {
    $(this.new_comment_id(root_id)).toggle();
  }
  else
  {
    if (root_id == this.root_id)
    {
      new Insertion.After(this.root_comments_id(),this.new_comment_html(root_id));
    }
    else
    {
    //  new Insertion.Bottom(this.comment_id(root_id),this.new_comment_html(root_id));
    }
    $(this.new_comment_button_id(root_id)).observe(
              "click",this.send_comment.bindAsEventListener(this,root_id)
                    );
    var editor = new Community.HTMLEditor(this.new_comment_body_id(root_id));
    if (root_id != this.root_id) this.comments_hash.get(root_id).editor_new = editor;
    else this.editor_new = editor;
  }
  },

  display_edit_comment: function(root_id)
  {
  if ($(this.edit_comment_id(root_id)))
  {
    $(this.edit_comment_id(root_id)).toggle();
  }
  else
  {
    new Insertion.Bottom(this.comment_id(root_id),this.edit_comment_html(root_id));
    $(this.edit_comment_button_id(root_id)).observe(
              "click",this.save_edit_comment.bindAsEventListener(this,root_id)
                    );
    var editor = new Community.HTMLEditor(this.edit_comment_body_id(root_id));
    this.comments_hash.get(root_id).editor_edit = editor;
  }
  },

  html: function(comments,level,posted_id)
  {
  var value = '<ul class="list comments">';
  var t_this = this;
  comments.each(function(comment)
          {
      value += t_this.comment_html(comment,level,posted_id);
          });
  value += '</ul>';
  return value;
  },

  comment_html: function(comment,level,posted_id)
  {
  var value = '';

  value += '<li';
  if (posted_id && posted_id == comment.id) value += ' class="active"';
  else if (comment.author_id != community.prop.user_id && this.lastviewtime && community.cmp_times(comment.time_edit,this.lastviewtime) > 0) value += ' class="active2"';
  value += ' id="'+this.comment_id(comment.id)+'" class="mt10 CommentDiv">';
  value += '<div style="margin-left:-20px;">' + community.user_title(comment.author_title,{avatar:1}) + '<td>: ' + comment.time_create + '</td></tr></table></div>';
  value += '<div class="mb10 mt10" id="'+this.comment_body_id(comment.id)+'"';
  if (comment.style) value += ' style="'+comment.style+'"';

  value += '>';
  if (Number(comment.censored))
  {
    value += '<div><a href="#" style="color:red" onclick="community.show_cut(this);return false;">[������ ����������� �����...]</a><span style="display:none;">';
  }
  value += community.convert_smiles(comment.body,10);
  if (Number(comment.censored))
  {
    value += '</span></div>';
  }
  value += '</div>';
  if (comment.hidden) value += '<div style="color:red;">��������� ������ �����������</div>';
  value += '<ul class="act ul">';
  if (comment.cnt_child>0)
  {
    value += '<li><a href="#" id="'+this.comment_answers_button_id(comment.id)+'" onclick="return false;">������&nbsp;('+comment.cnt_child+')</a> - <a href="#" id="'+this.comment_answers_all_button_id(comment.id)+'" onclick="return false;">���</a></li>';
  }
  if (comment.can_post - 0)
  {
    value=value+'<li><a href="#" id="'+this.comment_answer_button_id(comment.id)+'" onclick="return false;">��������</a></li>';
  }
  if (comment.can_edit - 0)
  {
    value=value+'<li><a href="#" id="'+this.comment_edit_button_id(comment.id)+'" onclick="return false;">�������������</a></li>';
  }
  if (comment.can_delete - 0)
  {
    value=value+'<li><a href="#" id="'+this.comment_delete_button_id(comment.id)+'" onclick="return false;">�������</a></li>';
  }
  if (comment.can_censure - 0)
  {
    value=value+'<li id="'+this.comment_censure_id(comment.id)+'" style="display:none;"></li>';
  }
  if (comment.can_hide - 0)
  {
    value=value+'<li><a href="#" id="'+this.comment_hide_button_id(comment.id)+'" onclick="return false;">';
    if (comment.hidden) value += '�������';
    else value += '������';
    value += '</a></li>';
  }
  value += '</ul>';
  value += '<input type="hidden" value="'+level+'" id="'+this.comment_level_id(comment.id)+'" />';
  return value;
  },

  new_comment_html: function(root_id)
  {
  var value='<div class="new_comment content p0 m0" id="'+this.new_comment_id(root_id)+'"';
  if (root_id == this.root_id) value=value+' style="display:block"';
  else value += '';
  value += '><h3>�������� ';
  if (root_id == this.root_id) value=value+'�����������:</h3>';
  else value += '�����:</h3>';
  value += '<div><textarea id="'+this.new_comment_body_id(root_id)+'" style="width:350px;height:180px;"></textarea></div>';
  value += '<p class="mt10 mb10" id="'+this.new_comment_status_id(root_id)+'">������: �� ������������</p>';
  value += '<input class="button" type="button" value="���������" id="'+this.new_comment_button_id(root_id)+'" />';
  value += '</div>';
  return value;
  },

  edit_comment_html: function(root_id)
  {
  var value='<div class="clear pt10" id="'+this.edit_comment_id(root_id)+'"';
  value += '><table><tbody><tr><td>������������� ';
  if (root_id == this.root_id) value += '�����������:';
  else value += '�����:';
  value += '</td></tr><td><textarea id="'+this.edit_comment_body_id(root_id)+'" style="width:350px;height:180px;">'+this.comments_hash.get(root_id).body+'</textarea></td></tr>';
  value += '<tr><td id="'+this.edit_comment_status_id(root_id)+'">������: �� ������������</td></tr>';
  value += '<tr><td><input class="button" type="button" value="���������" id="'+this.edit_comment_button_id(root_id)+'"></input>';
  value=value+'</td></tr></tbody></table><br /></div>';
  return value;
  },

  root_html: function()
  {
  var value = '<a style="float:right;" href="#" id="'+this.toggle_plain_view_id()+'" onclick="return false;">������� � ���������������� ���������</a><br /><a style="float:right;" href="#" id="'+this.open_all_comments_id()+'" onclick="return false;">������� ���</a>';
  value += '<div id="'+this.root_comments_id()+'"></div>';
  return value;
  },

  childs_html: function(root_id)
  {
  var value;
  if ($F(this.comment_level_id(root_id)) < 8) value ='<div style="margin-left: 14px;padding:0;" id="'+this.comment_childs_id(root_id)+'">���� ��������...</div>';
  else value ='<div id="'+this.comment_childs_id(root_id)+'">���� ��������...</div>';
  return value;
  },

  toggle_answers: function(event,root_id,all)
  {
  if ($(this.comment_childs_id(root_id)))
  {
    $(this.comment_childs_id(root_id)).toggle();
  }
  else
  {
    new Insertion.After(this.comment_id(root_id),this.childs_html(root_id));

    new Ajax.Request(community.url.comments_ajax,
        {
         parameters: {cmd:"get",id:root_id},
         onSuccess: this.get_answers_success.bind(this,root_id,all),
         onFailure: community.ajax_failure.bind(community)
        });
  }
  },

  open_all_comments: function(event)
  {
    var t_this = this;
    this.comments.each(function(comment)
     {
        if (Number(comment.cnt_child)) { t_this.toggle_answers(null,comment.id,"all"); }
     });
  },

  toggle_answers_all: function(event,root_id)
  {
   this.toggle_answers(event,root_id,"all");
  },

  get_answers_success: function(root_id,all,transport)
  {
   var comment_childs_element = $(this.comment_childs_id(root_id));
   if (comment_childs_element && comment_childs_element.innerHTML != "���� ��������...") return;
   var response=transport.responseText.split(" ");
   if (response[0] != "OK") { alert("������ ��������� �������:"+transport.responseText); return; }
   response.splice(0,1);
   var comments=$A(response.join(" ").evalJSON(true));
   if (root_id != this.root_id) this.comments_hash.get(root_id).childs = comments;
   var t_this = this;
   this.comments_hash = comments.inject(this.comments_hash,function(acc,value)
                {
                 if (root_id != t_this.root_id)
                 {
                  value.parent = root_id;
                 }
                 acc.set(value.id,value);
                 return acc;
                });
   this.display_comments(this.comment_childs_id(root_id),comments,$F(this.comment_level_id(root_id)) - 0 + 1);
   if (all)
   {
    comments.each(function(comment)
     {
        if (Number(comment.cnt_child)) { t_this.toggle_answers(null,comment.id,"all"); }
     });
   }
  },

  send_comment: function(event,root_id)
  {
  var ed = Community.HTMLEditor.get(this.new_comment_body_id(root_id));
  if (!ed) return;
  var sending_text = ed.getContent();
  ed.setContent('');
  $(this.new_comment_status_id(root_id)).update("������: ������������...");

  new Ajax.Request(community.url.comments_ajax,
       {
        parameters: {cmd:"post",id:root_id,body:sending_text,check:community.prop.check},
        onSuccess: this.send_comment_success.bind(this,root_id),
         onFailure: this.send_comment_failure.bind(this,root_id)
        }
      );
  },

  send_comment_success: function(root_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    $(this.new_comment_status_id(root_id)).update("������: ������ ("+transport.responseText+")");
    return;
  }
  var posted_id = response[1];
  response.splice(0,2);
  var comments = $A(response.join(" ").evalJSON(true));
  $(this.new_comment_status_id(root_id)).update("������: ������� ����������");

  if (root_id != this.root_id)
  {
    if (this.comments_hash.get(root_id).childs)
    {
      this.stop_comments(this.comments_hash.get(root_id).childs);
    }
    this.comments_hash.get(root_id).childs = comments;
  }
  var t_this = this;
  this.comments_hash = comments.inject(this.comments_hash,function(acc,value)
                {
                 if (root_id != t_this.root_id)
                 {
                  value.parent = root_id;
                 }
                 acc.set(value.id,value);
                 return acc;
                });
  if(root_id != this.root_id)
  {
    if(!$(this.comment_childs_id(root_id)))
    {
      new Insertion.After(this.comment_id(root_id),this.childs_html(root_id));
                }
    this.display_comments(this.comment_childs_id(root_id),comments,$F(this.comment_level_id(root_id)) - 0 + 1,posted_id);
    this.toggle_new_comment(null,root_id);
  }
  else
  {
    this.display_comments(this.root_comments_id(),comments,0,posted_id);
  }



  },

  send_comment_failure: function(root_id,transport)
  {
  $(this.new_comment_status_id(root_id)).update("������: ����������� ������!");
  },

  toggle_new_comment: function(event,root_id)
  {
  this.display_new_comment(root_id);
  if ($(this.new_comment_id(root_id)).visible())
  {
    if ($(this.new_comment_id(root_id)).scrollIntoView)
    {
      $(this.new_comment_id(root_id)).scrollIntoView(false);
    }
    else $(this.new_comment_id(root_id)).scrollTo();
  }
  },

  toggle_edit_comment: function(event,root_id)
  {
  this.display_edit_comment(root_id);
  if ($(this.edit_comment_id(root_id)).visible())
  {
    if ($(this.edit_comment_id(root_id)).scrollIntoView)
    {
      $(this.edit_comment_id(root_id)).scrollIntoView(false);
    }
    else $(this.edit_comment_id(root_id)).scrollTo();
  }
  },

  save_edit_comment: function(event,root_id)
  {
  var ed = Community.HTMLEditor.get(this.edit_comment_body_id(root_id));
  if (!ed) return;
  var sending_text = ed.getContent();
  ed.setContent('');
  $(this.edit_comment_status_id(root_id)).update("������: ������������...");

  new Ajax.Request(community.url.comments_ajax,
       {
        parameters: {cmd:"update",id:root_id,body:sending_text,check:community.prop.check},
        onSuccess: this.save_edit_comment_success.bind(this,root_id),
         onFailure: this.save_edit_comment_failure.bind(this,root_id)
        }
      );
  },

  save_edit_comment_success: function(root_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    $(this.edit_comment_status_id(root_id)).update("������: ������ ("+transport.responseText+")");
    return;
  }
  response.splice(0,1);

  var comment = response.join(" ").evalJSON(true);
  $(this.edit_comment_status_id(root_id)).update("������: ������� ����������");
  if ($(this.edit_comment_body_id(root_id))) Community.HTMLEditor.remove(this.edit_comment_body_id(root_id));
  if ($(this.new_comment_body_id(root_id))) Community.HTMLEditor.remove(this.new_comment_body_id(root_id));
  this.stop_comment(this.comments_hash.get(root_id));
  var old_parent = this.comments_hash.get(root_id).parent;
  var comments;
  if (old_parent) comments = this.comments_hash.get(old_parent).childs;
  else comments = this.comments;
  for (var i=0;i<comments.length;i++)
  {
    if (comments[i].id == comment.id) { comments[i] = comment; break; }
  }
  this.comments_hash.set(root_id,comment);
  $(this.comment_id(root_id)).replace(this.comment_html(comment,$F(this.comment_level_id(root_id)),root_id));
  this.observe_comment(comment);
  },

  save_edit_comment_failure: function(root_id,transport)
  {
  $(this.edit_comment_status_id(root_id)).update("������: ����������� ������");
  },

  delete_comment: function(event,root_id)
  {
  if (confirm("�� �������?"))
  {
    new Ajax.Request(community.url.comments_ajax,
         {
          parameters: {cmd:"post.delete",id:root_id,check:community.prop.check},
          onSuccess: this.delete_comment_success.bind(this,root_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  }
  },

  delete_comment_success: function(root_id,transport)
  {
  var response=transport.responseText;

  if (response != "OK") { alert("������ �������� �����������: "+transport.responseText); return; }
  this.stop_comment(this.comments_hash.get(root_id));
  var old_parent = this.comments_hash.get(root_id).parent;
  var comments;
  if (old_parent) comments = this.comments_hash.get(old_parent).childs;
  else comments = this.comments;
  for (var i=0;i<comments.length;i++)
  {
    if (comments[i].id == root_id) { comments.splice(i,1); break; }
  }
  this.comments_hash.unset(root_id);
  if ($(this.comment_childs_id(root_id))) $(this.comment_childs_id(root_id)).remove();
  $(this.comment_id(root_id)).remove();
  },

  hide_comment: function(event,root_id)
  {
  var comment = this.comments_hash.get(root_id);
  if (!comment) return;
  if (comment.hidden)
  {
    new Ajax.Request(community.url.blog_ajax,
      {
        parameters: {cmd:"post.unhide",id:root_id,check:community.prop.check},
        onSuccess: this.unhide_comment_success.bind(this,root_id),
        onFailure: community.ajax_failure.bind(community)
      });
  }
  else
  {
    new Ajax.Request(community.url.blog_ajax,
      {
        parameters: {cmd:"post.hide",id:root_id,check:community.prop.check},
        onSuccess: this.hide_comment_success.bind(this,root_id),
        onFailure: community.ajax_failure.bind(community)
      });
  }
  },

  hide_comment_success: function(root_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������ �������: "+transport.responseText); return; }
  var got_root_id = response[1];
  if (root_id != got_root_id) return;
  var comment = this.comments_hash.get(root_id);
  comment.hidden = 1;

  if ($(this.edit_comment_body_id(root_id))) Community.HTMLEditor.remove(this.edit_comment_body_id(root_id));
  if ($(this.new_comment_body_id(root_id))) Community.HTMLEditor.remove(this.new_comment_body_id(root_id));
  this.stop_comment(this.comments_hash.get(root_id));
  var old_parent = this.comments_hash.get(root_id).parent;
  var comments;
  if (old_parent) comments = this.comments_hash.get(old_parent).childs;
  else comments = this.comments;
  for (var i=0;i<comments.length;i++)
  {
    if (comments[i].id == comment.id) { comments[i] = comment; break; }
  }
  this.comments_hash.set(root_id,comment);
  $(this.comment_id(root_id)).replace(this.comment_html(comment,$F(this.comment_level_id(root_id))));
  this.observe_comment(comment);

  },

  unhide_comment_success: function(root_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������ ��������: "+transport.responseText); return; }
  var got_root_id = response[1];
  if (root_id != got_root_id) return;
  var comment = this.comments_hash.get(root_id);
  comment.hidden = 0;

  if ($(this.edit_comment_body_id(root_id))) Community.HTMLEditor.remove(this.edit_comment_body_id(root_id));
  if ($(this.new_comment_body_id(root_id))) Community.HTMLEditor.remove(this.new_comment_body_id(root_id));
  this.stop_comment(this.comments_hash.get(root_id));
  var old_parent = this.comments_hash.get(root_id).parent;
  var comments;
  if (old_parent) comments = this.comments_hash.get(old_parent).childs;
  else comments = this.comments;
  for (var i=0;i<comments.length;i++)
  {
    if (comments[i].id == comment.id) { comments[i] = comment; break; }
  }
  this.comments_hash.set(root_id,comment);
  $(this.comment_id(root_id)).replace(this.comment_html(comment,$F(this.comment_level_id(root_id))));
  this.observe_comment(comment);
  }
});

Community.Comments.id = 0;