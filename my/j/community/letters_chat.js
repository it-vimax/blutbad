//----------------------------------------------------------------------------------------------------
//Letters chat functions

var Letters = new Array();
var Contact = "";
var LettersChatTime = 0;
var NoMoreScrollUpdate = false;

function InitChat(contact)
{
  Contact=contact;

  $("ChatHint").update("���� ��������...");
  new Community.HTMLEditor("NewLetterBody");
  UpdateChat();
}

function UpdateChat()
{
  new Ajax.Request(community.url.letters_ajax,
    {
      parameters: { cmd: "letters.getchat",time:LettersChatTime,contact:Contact,check:community.prop.check },
      onSuccess: onChatUpdate,
      onFailure: community.ajax_failure.bind(community)
    });
}

function GetLetterIndexById(id)
{
  var ret_letter_index = null;
  Letters.each(function(letter_info,index)
      {
        if (letter_info.id == id) { ret_letter_index = index; throw $break; }
      });
  return ret_letter_index;
}

//Generating letter (i.e. letter header) html area
function GenerateLetterEntry(letter_info)
{
  var letter_flag = "&nbsp;";
  if (!letter_info.sent) letter_flag = "[�]";
  else if (!letter_info.recv) letter_flag = (letter_info.from == community.prop.user_id) ? "[?]" : "[*]"; else letter_flag="[ok]"
  var value = '<tr id="Letter' + letter_info.id + '" style="cursor: pointer;" onclick="ToggleLetter(' + letter_info.id + ');">';
  value += '<td style="vertical-align:top;width:200px;line-height:20px;">';
  value += '<b class="mr5">' + community.user_title(letter_info.title_from) + '</b>';
  if (letter_info.sent) value += letter_info.sent;
  if (letter_info.subject) value += '<p>' + letter_info.subject + '</p>';
  value += '</td>';
  value += '<td style="width:30px;vertical-align:top;line-height:20px;">' + letter_flag + '</td>';
  value += '<td style="vertical-align:top;">';

  value += '<p id="LetterBody' + letter_info.id + '" style="line-height:20px; padding:0;overflow: hidden; height: 20px;">';
  value += community.convert_smiles(letter_info.body);
  value += '</p></td></tr>';
  return value;
}

//Toggling between showing full letter body and just part of it
function ToggleLetter(id)
{
  if ($("LetterBody"+id).getStyle("overflow") == "hidden")
  {
    $("LetterBody"+id).setStyle({overflow: "visible",height: "auto"});
  }
  else
  {
    $("LetterBody"+id).setStyle({overflow: "hidden", height: "20px"});

  }
}

//Scrolling letters list. If position is near end, sending request for more letters
function onScrollLetters()
{
  if (NoMoreScrollUpdate) return;
  var len=Letters.length;
  if (len < 10) return;
  var height = $("LettersDiv").getHeight();
  if (($("LettersDiv").scrollTop + height) >= $("Letter"+Letters[len-1].id).offsetTop)
  {
    new Ajax.Request(community.url.letters_ajax,
        {
        parameters: {
                cmd: "letters.getchat",
                contact:Contact,
                last_id:Letters[len-1].id,
                check: community.prop.check
              },
        onSuccess: onScrollLettersSuccess,
        onFailure: function (transport) { return; }
        });
  }
}

//Receiving new letter list
function onScrollLettersSuccess(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") return;
  response.splice(0,2);
  var letters = $A(response.join(" ").evalJSON(true));
  if (!letters.length) { NoMoreScrollUpdate = true; return; }
  if (Letters.length && (letters[0].id >= Letters[Letters.length - 1].id)) return;
  var letters_html = "";
  letters.each(function(letter_info)
    {
      letters_html += GenerateLetterEntry(letter_info);
      Letters.push(letter_info);
    });
  new Insertion.Bottom("Letters",letters_html);
}

function SendLetter()
{
  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: "letter.save",
               id: "",
               chat: 1,
               title_to: Contact,
               subject: $F("NewLetterSubject"),
               body: tinyMCE.get("NewLetterBody").getContent(),
               send: 1,
               save_sended: $F("NewLetterSaveSended"),
               check: community.prop.check
             },
       onSuccess: onSuccessSendLetter,
       onFailure: community.ajax_failure.bind(community)
      });
}

function onSuccessSendLetter(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    alert("������ ���������� ������: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  LettersChatTime = response[0];
  response.splice(0,1);
  UpdateAll(response.join(" "));
  tinyMCE.get("NewLetterBody").setContent('');
  $("NewLetterSubject").value="";
}

function onChatUpdate(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") {
     $("ChatHint").update("������ ����������: "+transport.responseText);
     return;
  }
  response.splice(0,1);
  LettersChatTime = response[0];
  response.splice(0,1);
  $("ChatHint").update("");
  UpdateAll(response.join(" "));
}

//Updates objects and redrawes folders and letters (done after interval update)
function UpdateAll(letters_json)
{
  var letters = $A(letters_json.evalJSON(true));
  var value="";
  var top_letters = new Array();
  letters.each(function(letter_info,index)
    {
      if (!$("Letter"+letter_info.id))
      {
        value += GenerateLetterEntry(letter_info);
        top_letters.push(letter_info);
      }
    });
  Letters = top_letters.concat(Letters);
  new Insertion.Top("Letters",value);
}
