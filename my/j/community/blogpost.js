var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.BlogPost = Class.create({
  initialize: function(container,post,options)
  {
    if (!options) options = {};
    this.id = Community.BlogPost.id++;
    this.edit_window = null;
    this.edit_editor = null;
    this.edit_insertsurvey = null;
    this.edit_id = 0;
    this.container = $(container);
    if (!this.container) throw new Error("����������� ��������� ��� ������!");
    this.post = post;
    this.display();
    community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
    this.stop();
    this.container = null;
    if (this.edit_editor) this.edit_editor.clean();
    this.edit_editor = null;
    if (this.edit_insertsurvey) this.edit_insertsurvey.clean();
    this.edit_insertsurvey = null;
    if (this.edit_window) this.edit_window.clean();
    this.edit_window = null;
  },

  stop: function()
  {
    if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");

    if (this.edit_editor) this.edit_editor.stop();
    if (this.edit_insertsurvey) this.edit_insertsurvey.stop();
    if (this.edit_window) this.edit_window.clear();

    var delete_element = $(this.post_delete_id());
    if (delete_element) delete_element.stopObserving("click");
    var edit_element = $(this.post_edit_id());
    if (edit_element) edit_element.stopObserving("click");
    var hide_element = $(this.post_hide_id());
    if (hide_element) hide_element.stopObserving("click");
    var fix_element = $(this.post_fix_id());
    if (fix_element) fix_element.stopObserving("click");
    var body_all_element = $(this.post_body_all_id());
    if (body_all_element) body_all_element.stopObserving("click");
    if (this.post.tags) this.post.tags.clean();
    if (this.post.favorite) this.post.favorite.clean();
    if (this.post.sign) this.post.sign.clean();
    if (this.post.censure) this.post.censure.clean();
    if (this.post.survey) this.post.survey.clean();
    if (this.post.rating_obj) this.post.rating_obj.clean();
  },

  post_cell_id: function() { return "Community_BlogPost_Cell_"+this.id; },
  post_body_id: function() { return "Community_BlogPost_Body_"+this.id; },
  post_body_all_id: function() { return "Community_BlogPost_BodyAll_"+this.id; },
  post_survey_id: function() { return "Community_BlogPost_Survey_"+this.id; },
  post_edit_id: function() { return "Community_BlogPost_Edit_"+this.id; },
  post_delete_id: function() { return "Community_BlogPost_Delete_"+this.id; },
  post_hide_id: function() { return "Community_BlogPost_Hide_"+this.id; },
  post_fix_id: function() { return "Community_BlogPost_Fix_"+this.id; },
  post_check_id: function() { return "Community_BlogPost_Check_"+this.id; },
  post_censure_id: function() { return "Community_BlogPost_Censure_"+this.id; },
  post_tags_id: function() { return "Community_BlogPost_Tags_"+this.id; },
  post_favorite_id: function() { return "Community_BlogPost_Favorite_"+this.id; },
  post_rating_id: function() { return "Community_BlogPost_Rating_"+this.id; },
  post_sign_id: function() { return "Community_BlogPost_Sign_"+this.id; },
  edit_title_id: function() { return "Community_BlogPost_EditTitle_"+this.id; },
  edit_body_id: function() { return "Community_BlogPost_EditBody_"+this.id; },
  edit_rd_acc_id: function() { return "Community_BlogPost_EditRdAcc_"+this.id; },
  edit_wr_acc_id: function() { return "Community_BlogPost_EditWrAcc_"+this.id; },
  edit_status_id: function() { return "Community_BlogPost_EditStatus_"+this.id; },
  edit_insertsurvey_id: function() { return "Community_BlogPost_EditInsertSurvey_"+this.id; },
  edit_insertsurvey_info_id: function() { return "Community_BlogPost_EditInsertSurveyInfo_"+this.id; },

  display: function()
  {
    this.container.update(this.html());
    this.observe();
  },

  observe: function()
  {
    if ($(this.post_body_all_id()))
    {
      $(this.post_body_all_id()).observe("click",this.show_all_body.bind(this));
    }
    if (this.post.type == 1 && Number(this.post.survey_id))
    {
      this.post.survey = new Community.Survey.Survey(this.post_survey_id(),Number(this.post.survey_id),community.prop.user_id,{hide_title:true, can_view_result: true});
    }

    if (Number(this.post.can_delete ))
    {
      $(this.post_delete_id()).observe("click",this.del.bind(this));
    }
    if (Number(this.post.can_edit))
    {
      $(this.post_edit_id()).observe("click",this.edit.bind(this));
    }
    if (Number(this.post.can_hide))
    {
      $(this.post_hide_id()).observe("click",this.hide.bind(this));
    }
    if (Number(this.post.can_edit))
    {
      $(this.post_fix_id()).observe("click",this.fix.bind(this));
    }

    this.post.tags = new Community.Tags(
         this.post_tags_id(),this.post.id,this.post.type,this.post.author_id,this.post.can_edit_tag
    );

    this.post.favorite = new Community.Favorite(
         this.post_favorite_id(),this.post.id,this.post.type,this.post.favorite_included
    );

    this.post.sign = new Community.Sign(
         this.post_sign_id(),this.post.id,this.post.type,this.post.sign_included
    );

    if (Number(this.post.can_censure))
    {
      this.post.censure = new Community.Censure(
         this.post_censure_id(),this.post.id,this.post.censure_enabled);
    }

    if (Number(this.post.show_rating) && Number(this.post.can_rate)) {
      this.post.rating_obj = new Community.Rating(this.post_rating_id(),this.post.id);
    }
  },

  html: function()
  {
     var value = '';
     value += '<ul class="list">';
     value += '<li class="por">';
     value += '<div style="margin-left: -20px;">'+community.user_title(this.post.author_title,{avatar:1,avsize:1})+'<td>: '+this.post.time_create+'</td></tr></table></div>';
     value += '<h3><a href="/blog_comments='+this.post.id+'">';
     if (this.post.title) value += this.post.title;
     else value += '<em>(��� ��������)</em>';
     value += '</a>';
     if (Number(this.post.survey_id)) value += ' (�����)';
     value += '</h3>';

     value += '<div class="mb10 mt10">';
     if (this.post.mood) {
       value += '<p><em><strong>����������:</strong> '+this.post.mood;
       if (this.post.mood_smile) value += community.get_smile_img(this.post.mood_smile);
       value += '</em></p>';
     }

     if (this.post.music) {
       value += '<p><em><strong>�������:</strong> '+this.post.music+'</em></p>';
     }
     value += '</div>';

     value += '<div style="font-size:11px;" class="light">���������� ����������: '+this.post.totalviews+'</div>';
     if (Number(this.post.survey_id) && !Number(this.post.survey_pos)) {
        value += '<div class="blog_survey voting" id="'+this.post_survey_id()+'"></div>';
     }
     value += '<div class="mb20" id="'+this.post_body_id()+'" ';
     if (this.post.style) value += 'style="'+this.post.style+'"';
     value += '>';
     if (Number(this.post.censored)) {
        value += '<div><a href="#" style="color:red" onclick="community.show_cut(this);return false;">[������ ����������� �����...]</a><span style="display:none;">';
     }

     var orig_body = this.post.body;
     var body = orig_body.replace(/<\/cut>/g,'</span></div>');
     var is_cutted = false;
     body = body.replace(/<cut>/g,'<div><a href="#" onclick="community.show_cut(this);return false;">[������ ���������...]</a><span style="display:none;">');
     if (body.length == orig_body.length) { body = community.cut_body(body,2000); is_cutted = true; }

     value += community.convert_smiles(body);
     if (is_cutted && body.length != orig_body.length){
        value += '<p><a href="#" onclick="return false;" id="'+this.post_body_all_id()+'">[������ �����...]</a></p>';
     }

     if (Number(this.post.censored)) value += '</span></div>';
     value += '</div>';
     if (Number(this.post.survey_id) && Number(this.post.survey_pos)) {
        value += '<div class="blog_survey voting mt10" id="'+this.post_survey_id()+'"></div>';
     }
     value +=  '<div class="inner_tags" id="'+this.post_tags_id()+'">';
     value += '<div id="'+this.post_tags_id()+'"></div>';
     value += '</div>';
     value += '<div class="clear">';
     value +=  '<a href="/blog_comments='+this.post.id+'">����������� ('+this.post.cnt_child+')</a>';
     value += '</div></li></ul>';

     value += '<div class="fr light">';

     if (Number(this.post.can_edit))
     {
        value += '<a href="#" onclick="return false;" class="fr light pseudolink" id="'+this.post_edit_id()+'" style="margin-left: 5px;">������������� ������</a>';
     }
     if (Number(this.post.can_delete))
     {
        value += '<a href="#" onclick="return false;" class="fr light pseudolink" id="'+this.post_delete_id()+'" style="margin-left: 5px;">������� ������</a>';
     }

     if (Number(this.post.can_edit))
     {
        value += '<a href="#" class="fr light pseudolink" onclick="return false;" id="'+this.post_fix_id()+'" style="margin-left: 5px;">';
        if (!Number(this.post['fixed'])) value += '���������� ������';
        else value += '��������� ������';
        value += '</a>';
     }

     if (Number(this.post.can_hide))
     {
        value += '<a href="#" onclick="return false;" id="'+this.post_hide_id()+'">';
        if (Number(this.post.hidden)) value += '������� ������';
        else value += '������ ������';
        value += '</a>';
     }

     value += '<br><div class="fr light" id="'+this.post_sign_id()+'" style="display: none;padding-left: 5px;"></div>&nbsp;';
     value += '<div class="fr light" id="'+this.post_favorite_id()+'" style="display: none;padding-left: 5px;"></div>&nbsp;';
     value += '<div class="fr light" id="'+this.post_censure_id()+'" style="display: none;padding-left: 5px;"></div>';

     value += '</div>';

     if (Number(this.post.show_rating)) {
       value += '<div class="mt10 mb10">';
       value += '������� ������: <strong>'+this.post.rating+'</strong> (������� �������: <em>'+this.post.votes+'</em>)';
       if (Number(this.post.can_view_marks) && Number(this.post.votes)) {
         value += ' <a href="'+community.url.blog_votes+'?where='+this.post.id+'">���������...</a>';
       }
       value += '</div>';
       if (Number(this.post.can_rate)) {
         value += '<div id="'+this.post_rating_id()+'" style="display:none;"></div>';
       }
     }
     value += '<div class="clear"></div>';
     return value;
  },

  show_all_body: function()
  {
    $(this.post_body_all_id()).stopObserving("click");
    $(this.post_body_id()).update(community.convert_smiles(this.post.body));
  },

  edit: function()
  {            
    if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");
    if (this.edit_editor) { this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null; }
    if (this.edit_insertsurvey) { this.edit_insertsurvey.clean(); this.edit_insertsurvey = null; }
    if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
    this.edit_window = new Community.Window({
            title:"�������������� ������ "+(this.post.title ? this.post.title : "(��� ��������)"),
            left: 10,
            content:this.edit_window_content(),
            ok_button:true,
            on_ok:this.edit_ok.bind(this),
            on_before_close: this.edit_window_before_close.bind(this)
                 });
    $(this.edit_insertsurvey_id()).observe("click",this.edit_survey.bind(this));

    this.edit_window.show();
    this.edit_editor = new Community.HTMLEditor(this.edit_body_id(),{extended:true});
    this.edit_insertsurvey = new Community.InsertSurvey({survey_id:this.post.survey_id,
                                                       survey_pos:this.post.survey_pos,
                                                       on_insert:this.insert_survey.bind(this),
                                                       on_save:this.edit_ok2.bind(this)});
    if (!Number(this.post.hidden))
    {
      makeSelectBox("#"+this.edit_rd_acc_id());
      makeSelectBox("#"+this.edit_wr_acc_id());
    }
  jQuery('select').styler();
  },

  edit_window_content: function()
  {
    var value = '<table style="width:550px;"><tbody>';
    value += '<tr><td width="20%">���������:</td><td>';
    value += '<input id="' + this.edit_title_id() + '" type="text" class="inp_text" style="width:500px;" value="' + this.post.title + '"/></td></tr>';
    value += '<tr><td>������:</td><td>';
    value += '<textarea id="' + this.edit_body_id() + '" style="width:513px;height:200px;">' + this.post.body + '</textarea></td></tr>';
    value += '<tr><td>&nbsp;</td><td><div id="'+this.edit_insertsurvey_info_id()+'">';
    if (Number(this.post.survey_id)) value += '<strong>� ��������� ���������� �����</strong>';
    value += '</div><a href="#" id="'+this.edit_insertsurvey_id()+'" onclick="return false;">';
    if (Number(this.post.survey_id)) value += '�������� �����';
    else value += '���������� ����� � ���������';
    value += '</a></td></tr>';

    var t_this = this;
    if (Number(!this.post.hidden))
    {
      value += '<tr><td>����� ��������:</td><td><div style="z-index:3000;" class="por"><select class="selectbox" id="' + this.edit_rd_acc_id() + '" onchange="community.update_wr_acc_select(\'' + this.edit_rd_acc_id() + '\',\'' + this.edit_wr_acc_id() + '\');">';
      community.acc_title.each(function(pair)
          {
            value += '<option value="' + pair.key + '"';
            if (t_this.post.rd_acc == pair.key) value += ' selected="selected"';
            value += '>' + pair.value + '</option>';
          }
         );
      value += '</select></div></td></tr>';
      value += '<tr><td>����� ��������������:</td><td><div class="por"><select class="selectbox" id="' + this.edit_wr_acc_id() + '">';
      community.wr_acc_title.each(function(pair)
          {
            if (community.acc_order.get(pair.key) < community.acc_order.get(t_this.post.rd_acc)) return;
            value += '<option value="' + pair.key + '"';
            if (t_this.post.wr_acc == pair.key) value += ' selected="selected"';
            value += '>' + pair.value + '</option>';
          }
         );
      value += '</select></div></td></tr>';
    }
    value += '<tr><td>������:</td><td id="' + this.edit_status_id() + '">�� �����������</td></tr>';
    value += '</tbody></table>';
    return value;
  },

  edit_survey: function()
  {
    if (this.edit_insertsurvey) this.edit_insertsurvey.show();
  },

  insert_survey: function()
  {
    $(this.edit_insertsurvey_id()).update("�������� �����");
    $(this.edit_insertsurvey_info_id()).update("<strong>� ��������� ���������� �����</strong>");
  },

  edit_ok: function()
  {   
    if (!this.edit_insertsurvey) return;
    if (this.edit_insertsurvey.accepted)
    {
      this.edit_insertsurvey.save();
    }
    else this.edit_ok2();
  },

  edit_ok2: function(survey_id)
  {
    $(this.edit_status_id()).update("�����������...");
    var params = {
      cmd:"post.update",
      id:this.post.id,
      title:$F(this.edit_title_id()),
      body:this.edit_editor.get_value(),
      check:community.prop.check
    };
    if ($(this.edit_rd_acc_id())) params.rd_acc = $F(this.edit_rd_acc_id());
    if ($(this.edit_wr_acc_id())) params.wr_acc = $F(this.edit_wr_acc_id());
    if (survey_id) params.survey_id = survey_id;
    if (this.edit_insertsurvey) params.survey_pos = this.edit_insertsurvey.get_survey_pos();

    new Ajax.Request(community.url.blog_ajax,
         {
          parameters: params,
          onSuccess: this.edit_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  },

  edit_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK")
    {
      $(this.edit_status_id()).update("������ ����������: "+transport.responseText);
      return;
    }
    response.splice(0,1);
    var post_info = response.join(" ").evalJSON(true);
    this.stop();
    this.post.body = post_info.body;
    this.post.title = post_info.title;
    this.post.rd_acc = post_info.rd_acc;
    this.post.wr_acc = post_info.wr_acc;
    this.post.survey_id = post_info.survey_id;
    this.post.survey_pos = post_info.survey_pos;
    this.display();
  },

  edit_window_before_close: function()
  {
    if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");
    if (this.edit_editor) { this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null; }
    if (this.edit_insertsurvey) { this.edit_insertsurvey.clean(); this.edit_insertsurvey = null; }
    return true;
  },

  del: function()
  {
    if (confirm("�� �������?"))
    {
      new Ajax.Request(community.url.blog_ajax,
           {
            parameters: {cmd:"post.delete",id:this.post.id,check:community.prop.check},
            onSuccess: this.del_success.bind(this),
            onFailure: community.ajax_failure.bind(community)
           }
          );
    }
  },

  del_success: function(transport)
  {
    var response = transport.responseText;
    if (response != "OK") { alert("������ ��������: "+response); return; }
    document.location.href=community.url.blog+'?where='+this.post.owner_id;
  },

  fix: function()
  {
    if (Number(this.post['fixed']))
    {
      new Ajax.Request(community.url.blog_ajax,
        {
          parameters: {cmd:"post.unfix",id:this.post.id,check:community.prop.check},
          onSuccess: this.unfix_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
        });
    }
    else
    {
      new Ajax.Request(community.url.blog_ajax,
      {
        parameters: {cmd:"post.fix",id:this.post.id,check:community.prop.check},
        onSuccess: this.fix_success.bind(this),
        onFailure: community.ajax_failure.bind(community)
      });
    }
  },

  fix_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������: "+transport.responseText); return; }
    this.post['fixed'] = 1;
    if ($(this.post_fix_id())) $(this.post_fix_id()).update("��������� ������");
  },

  unfix_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������: "+transport.responseText); return; }
    this.post['fixed'] = 0;
    if ($(this.post_fix_id())) $(this.post_fix_id()).update("���������� ������");
  },

  hide: function()
  {
    if (Number(this.post.hidden))
    {
      new Ajax.Request(community.url.blog_ajax,
        {
          parameters: {cmd:"post.unhide",id:this.post.id,check:community.prop.check},
          onSuccess: this.unhide_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
      });
    }
    else
    {
      new Ajax.Request(community.url.blog_ajax,
        {
          parameters: {cmd:"post.hide",id:this.post.id,check:community.prop.check},
          onSuccess: this.hide_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
        });
    }
  },

  hide_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������: "+transport.responseText); return; }
    this.post['hidden'] = 1;
    if ($(this.post_hide_id())) $(this.post_hide_id()).update("������� ������");
  },

  unhide_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������: "+transport.responseText); return; }
    this.post['hidden'] = 0;
    if ($(this.post_hide_id())) $(this.post_hide_id()).update("������ ������");
  }

});

Community.BlogPost.id = 0;