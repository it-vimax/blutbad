var FolderWindow = null;

var Tree = null;
var Folders = null;
var TrashFolderId = 0;
var CurrentFolder = "";

var Letters = null;
var LastLettersUpdateTime = 0;
var MaxSimultaneousLetters = 1000;

var LettersBodyCache = new Hash();
var CurrentLetter = "";

var Contacts = null;

var reReSubj = new RegExp(/^Re(\(\d+\))?: ?(.*)$/);
var NoMoreScrollUpdate = false;

var WriteTo = '';
var ReadId = '';

var CheckedParams = false;

//--------------------------------------------------------------------------------------------------
//Folder functions

function InitFolders()
{
  $("Folders").update("���� ��������...");
  LoadFolders();
}

function UpdateFolders()
{
  new Ajax.Request(community.url.letters_ajax,
      {
        parameters: { cmd: "folders.get", check:community.prop.check },
        onSuccess: onUpdateFoldersSuccess,
        onFailure: community.ajax_failure.bind(community)
      });
}

function onUpdateFoldersSuccess(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") return;
  response.splice(0,1);
  ReupdateFolders(response.join(" "));
}

function LoadFolders()
{
  new Ajax.Request(community.url.letters_ajax,
      {
        parameters: { cmd: "folders.get", check:community.prop.check },
        onSuccess: onLoadFoldersSuccess,
        onFailure: community.ajax_failure.bind(community)
      });
}

function onLoadFoldersSuccess(transport)
{
  var response = transport.responseText.split(" ");

  if (response[0] != "OK") return;
  response.splice(0,1);
  ReloadFolders(response.join(" "));
}

function ReloadFolders(folders_json,force_open_folder,is_empty_trash)
{
  var ObjectsInfo = folders_json.evalJSON(true);
  Tree = $H(ObjectsInfo.tree);
  Tree.each(function(pair)
     {
      Tree.set(pair.key,$A(pair.value));
     });
  Folders = $H(ObjectsInfo.list);

  Folders.each(function(pair)
    {
      if (pair.value.system == 4) TrashFolderId = pair.key;
    });

  var value='<ul>';
  Tree.get("0").each(function(folder_info)
            {
        value=value+GenerateFolderEntry(folder_info);
            });
  value=value+'</ul>';
  $("Folders").update(value);
  if (!CurrentFolder || !Folders.get(CurrentFolder))
  {
    OpenFolder(Tree.get("0")[0].id);
  }
  else if ((is_empty_trash && IsInTrash(CurrentFolder)) || force_open_folder)
  {
    OpenFolder(CurrentFolder,'force');
  }
}

function ReupdateFolders(folders_json)
{
  var ObjectsInfo = folders_json.evalJSON(true);
  Tree = $H(ObjectsInfo.tree);
  Tree.each(function(pair)
     {
      Tree.set(pair.key,$A(pair.value));
     });

  Folders = $H(ObjectsInfo.list);
  Folders.each(function(pair)
    {
      if ($("Folder"+pair.key))
      {
        var openflag_el = $("FolderOpenIcon"+pair.key);
        var opened = (openflag_el && openflag_el.src == community.image.opened ? 1 : 0);
        $("Folder"+pair.key).replace(GenerateFolderEntry(pair.value,opened,1));
      }
    });
  if (CurrentFolder) UpdateLetters();
}

//Checking if folder with given id is trash or is in trash
function IsInTrash(id)
{
  var folder = Folders.get(id);
  if (!folder) return 0;
  if (folder.system == 4) return 1;
  return IsInTrash(folder.parent);
}

//Generating select for target folder
function GenerateTargetFolderSelect(id,isnoroot,exclude_id)
{
  var value = '<span class="por fl"><select id="'+id+'" style="width: 162px;">';
  if (!isnoroot) value=value+'<option value="0" selected>--������--</option>';
  var spaces=0;
  var put_options = function(parent)
    {
      if (!Tree.get(parent)) return;
      Tree.get(parent).each(function(folder)
                {
            if (folder.system == 4) return;
            if (exclude_id && folder.id == exclude_id) return;
            value=value+'<option value="'+folder.id+'">'+folder.label+'</option>';
            if (Tree.get(folder.id))
            {
              spaces++;
              put_options(folder.id);
              spaces--;
            }
                });
    };
  put_options(0);
  value += '</select></span>';

  return value;
}

//Opening or closing folder tree
function ToggleFolder(id)
{
  var src = $("FolderOpenIcon"+id).getAttribute("src");
  var src_array = src.split("/");
  if (src_array[src_array.length-1] == "folder_closed.gif")//community.image.closed)
  {
    $("FolderOpenIcon"+id).src="/i/comm/skin" + community.prop.skin + "/folder_opened.gif";//community.image.opened);
    if ($("ChildFolders"+id))
    {
      $("ChildFolders"+id).show();
    }
    else
    {
      var value="";
      Tree.get(id).each(function(folder_info)
          {
          value=value+GenerateFolderEntry(folder_info);
          });
      new Insertion.Bottom("Folder"+id,'<ul id="ChildFolders'+id+'">'+value+'</ul>');
    }
  }
  else
  {
    $("FolderOpenIcon"+id).src = "/i/comm/skin" + community.prop.skin + "/folder_closed.gif";//community.image.closed;
    $("ChildFolders"+id).hide();
  }
}

//Showing window for creating new folder
function NewFolderWindow()
{
  if (FolderWindow) FolderWindow.clear();
  var value='<table><tbody><tr><td>';
  value=value+'�����: </td><td>'+GenerateTargetFolderSelect('NewFolderTarget');
  value=value+'</td></tr><tr><td>��������: </td><td><input class="inp_text" type="text" id="NewFolderLabel" style="width:200px;" />';
  value=value+'</td></tr></tbody></table>';
  FolderWindow = new Community.Window({
          title:"�������� ����� �����",
          content: value,
          ok_button: true,
          on_ok: NewFolder
             });
  FolderWindow.show();
  makeSelectBox("#NewFolderTarget");
  $("NewFolderLabel").focus();
  jQuery('select').styler();
  new Community.BindInputWithButton("NewFolderLabel",FolderWindow.ok_button_element());
}

//Showing window for creating new folder
function MoveFolderWindow(id)
{
  if (FolderWindow) FolderWindow.clear();
  var value='<table><tbody><tr><td>';
  value=value+'�����: </td><td>'+GenerateTargetFolderSelect('MoveFolderTarget',null,id);
  value=value+'</td></tr></tbody></table>';
  FolderWindow = new Community.Window({
          title:"����������� ����� "+Folders.get(id).label,
          content: value,
          ok_button: true,
          on_ok: function() { MoveFolder(id); }
             });
  FolderWindow.show();
  makeSelectBox("#MoveFolderTarget");
}

//Sending request for new folder
function NewFolder()
{
  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: {  cmd: "folder.new",
          target: $F("NewFolderTarget"),
          label: $F("NewFolderLabel"),
          check: community.prop.check
             },
       onSuccess: onSuccessNewFolder,
       onFailure: community.ajax_failure.bind(community)
      });
  $("FoldersStatus").update("��������� ����� �����...");
  FolderWindow.clear(); FolderWindow = null;
}

function onSuccessNewFolder(transport)
{
  var response = transport.responseText.split(" ");
  $("FoldersStatus").update("");

  if (response[0] != "OK")
  {
    alert("������ �������� �����: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  ReloadFolders(response.join(" "));
}

//Sending request for delete folder
function DeleteFolder(id)
{
  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: "folder.move",
               id: id,
               to: TrashFolderId,
               check: community.prop.check
             },
       onSuccess: function(transport) { onSuccessDeleteFolder(transport,id); },
       onFailure: community.ajax_failure.bind(community)
      });
  $("FoldersStatus").update("����� ���������...");

}

function onSuccessDeleteFolder(transport,id)
{
  var response = transport.responseText.split(" ");
  $("FoldersStatus").update("");

  if (response[0] != "OK")
  {
    alert("������ �������� �����: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  ReloadFolders(response.join(" "));
}

//Sending request for move folder
function MoveFolder(id)
{
  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: "folder.move",
               id: id,
               to: $F("MoveFolderTarget"),
               check: community.prop.check
             },
       onSuccess: function(transport) { onSuccessMoveFolder(transport,id); },
       onFailure: community.ajax_failure.bind(community)
      });
  $("FoldersStatus").update("����� ������������...");
  FolderWindow.clear(); FolderWindow = null;
}

function onSuccessMoveFolder(transport,id)
{
  var response = transport.responseText.split(" ");
  $("FoldersStatus").update("");

  if (response[0] != "OK")
  {
    alert("������ ����������� �����: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  ReloadFolders(response.join(" "));
}

//Sending request for emptying trash
function EmptyTrash()
{
  if (confirm("�� �������, ��� ������ �������� �������?"))
  {
    new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: "trash.empty",check:community.prop.check },
       onSuccess: onSuccessEmptyTrash,
       onFailure: community.ajax_failure.bind(community)
      });
  }
  $("FoldersStatus").update("������� ���������...");

}

function onSuccessEmptyTrash(transport)
{
  var response = transport.responseText.split(" ");
  $("FoldersStatus").update("");

  if (response[0] != "OK")
  {
    alert("������ ������� �������: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  ReloadFolders(response.join(" "),0,'empty_trash');
}

//Generating folder html area
function GenerateFolderEntry(folder_info,opened,debug)
{
  var value='<li id="Folder'+folder_info.id+'"';
  if (CurrentFolder == folder_info.id) value=value+' class="active"';
  value=value+'>';
  if (Tree.get(folder_info.id))
  {
    if (opened) value=value+'<img class="listOpenClose" id="FolderOpenIcon'+folder_info.id+'" src="/i/comm/skin' + community.prop.skin + '/folder_opened.gif" onclick="ToggleFolder(\''+folder_info.id+'\'); return false" style="vertical-align: middle;"/>'; //community.image.opened
    else value=value+'<img class="listOpenClose" id="FolderOpenIcon'+folder_info.id+'" src="/i/comm/skin' + community.prop.skin + '/folder_closed.gif" onclick="ToggleFolder(\''+folder_info.id+'\'); return false" style="vertical-align: middle;"/>';
  }
  //else value=value+'<img class="listNo" id="FolderOpenIcon'+folder_info.id+'" src="'+community.image.square+'" />';
  value += '<a class="main_folder action_link" href="#" onclick="OpenFolder('+folder_info.id+'); return false">';
  value += folder_info.label;
  if (Folders.get(folder_info.id).unread > 0) value=value+'<strong>';
  value=value+' ('+Folders.get(folder_info.id).all+')';
  if (Folders.get(folder_info.id).unread > 0) value=value+'</strong>';
  value=value+'</a>';
  if (folder_info.system == 0)
  {
    value=value+'<a class="mr5" href="#" onclick="MoveFolderWindow(\''+folder_info.id+'\'); return false"><img src="/i/comm/skin' + community.prop.skin + '/icon_move.gif" alt="�����������" title="�����������" /></a>';
  }
  if (folder_info.system == 0 && !IsInTrash(folder_info.id))
  {
    value=value+'<a href="#" onclick="DeleteFolder(\''+folder_info.id+'\'); return false"><img src="/i/comm/skin' + community.prop.skin + '/icon_delete.gif" alt="�������" title="�������" /></a>';
  }
  if (folder_info.system == 4) value=value+'<a href="#" onclick="EmptyTrash(); return false"><img src="/i/comm/skin' + community.prop.skin + '/icon_delete.gif" alt="�������� �������" title="�������� �������" /></a>';
  value=value+'</li>';
  return value;
}

function OpenFolder(id,is_force)
{
  if (!is_force && CurrentFolder && CurrentFolder == id) return;
  LettersBodyCache = new Hash();
  if (CurrentFolder && CurrentFolder != id && $("Folder"+CurrentFolder))
  {
    $("Folder"+CurrentFolder).removeClassName("active");
  }
  CurrentFolder = id;
  $("Folder"+CurrentFolder).addClassName("active");
  InitLetters();
}

//-----------------------------------------------------------------------------------------------------------
//Letters functions

function InitLetters()
{
  if (!CurrentFolder) return;
  $("LettersDiv").update("���� ��������...");
  EmptyEditLetterArea();
  LastLettersUpdateTime = 0;
  NoMoreScrollUpdate = false;
  LoadLetters();
}

function LoadLetters()
{
  var MyCurrentFolder = CurrentFolder;
  new Ajax.Request(community.url.letters_ajax,
      {
        parameters: { cmd:"letters.get",id:CurrentFolder,check:community.prop.check },
        onSuccess: function(transport)
            {
              onLoadLettersSuccess(transport,MyCurrentFolder);
            },
        onFailure: community.ajax_failure.bind(community)
      });
}

function onLoadLettersSuccess(transport,my_current_folder)
{
  if (CurrentFolder != my_current_folder) return;
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { $("EditLetterArea").update("���� ��������..." + transport.responseText); return; }
  response.splice(0,1);
  LastLettersUpdateTime = response[0];
  response.splice(0,1);
  ReloadLetters(response.join(" "));
}

function ReloadLetters(letters_json)
{
  Letters = $A(letters_json.evalJSON(true));
  var value='<b>'+Folders.get(CurrentFolder).label+'</b>';
  $("FolderHeader").update(value);

  value="";
  if (!IsInTrash(CurrentFolder)) value=value+'<a class="button mr5" href="#" onclick="DeleteLetters();">������� ����������</a>';
  value=value+'<a class="button mr5" href="#" onclick="MoveLetters();">����������� ���������� �</a>';
  value=value+GenerateTargetFolderSelect("MoveLettersTarget",1);

  $("LettersOperations").update(value);
  value='<table class="eqtd"><tbody id="Letters">';
  Letters.each(function(letter_info,index)
        {
          value += GenerateLetterEntry(letter_info,1);
        });
  value=value+'</tbody></table>';
  var height;
  if (Letters.length <= 3) height = 90;
  else height = Letters.length * 30;
  if (height > 300) height = 300;
  $("LettersDiv").setStyle({height:height+"px"});
  $("LettersDiv").update(value);
  makeSelectBox("#MoveLettersTarget");
  $("SelectedAllLetters").checked = null;
  CheckParams();
  jQuery('select').styler();
}

function UpdateLetters()
{
  var MyCurrentFolder = CurrentFolder;
  new Ajax.Request(community.url.letters_ajax,
      {
        parameters: { cmd: "letters.get",time:LastLettersUpdateTime, id:CurrentFolder,check:community.prop.check },
        onSuccess: function(transport)
            {
              onUpdateLettersSuccess(transport,MyCurrentFolder);
            },
        onFailure: community.ajax_failure.bind(community)
      });
}

function onUpdateLettersSuccess(transport,my_current_folder)
{
  if (CurrentFolder != my_current_folder) return;
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") return;
  response.splice(0,1);
  LastLettersUpdateTime = response[0];
  response.splice(0,1);
  ReupdateLetters(response.join(" "));
}

function ReupdateLetters(letters_json)
{
  var NewLetters = $A(letters_json.evalJSON(true));
  if (!Letters) Letters = new Array();
  var len = Letters.length;
  var top_letters = new Array();
  var top_letters_html = "";
  NewLetters.each(function(letter_info)
    {
      var found = false;
      for (var index=0;index<len;index++)
      {
        if (Letters[index].id == letter_info.id)
        {
          Letters[index] = letter_info;
          $("Letter"+letter_info.id).replace(GenerateLetterEntry(letter_info));
          found = true;
          break;
        }
      }
      if (!found)
      {
        top_letters.push(letter_info);
        top_letters_html += GenerateLetterEntry(letter_info);
        len++;
      }
    });
  if (top_letters.length)
  {
    Letters = top_letters.concat(Letters);
    new Insertion.Top("Letters",top_letters_html);
  }
}

function ScrollLetters(letters_json)
{
  var NewLetters = $A(letters_json.evalJSON(true));
  if (!NewLetters.length) { NoMoreScrollUpdate = true; return; }
  if (Letters.length && (NewLetters[0].id >= Letters[Letters.length - 1].id)) return;
  var value = "";
  Letters = Letters.concat(NewLetters);
  NewLetters.each(function(letter_info,index)
    {
      value += GenerateLetterEntry(letter_info);
    });
  new Insertion.Bottom("Letters",value);
}

function GetLetterById(id)
{
  if (!CurrentFolder) return null;
  var ret_letter_info = null;
  Letters.each(function(letter_info)
      {
        if (letter_info.id == id) { ret_letter_info = letter_info; throw $break; }
      });
  return ret_letter_info;
}

function GetLetterIndexById(id)
{
  if (!CurrentFolder) return null;
  var ret_letter_index = null;
  Letters.each(function(letter_info,index)
      {
        if (letter_info.id == id) { ret_letter_index = index; throw $break; }
      });
  return ret_letter_index;
}

function GetLetterFolder(letter_info)
{
  if (letter_info.to == community.prop.user_id && letter_info.fid_to != 0) { return letter_info.fid_to; }
  if (letter_info.from == community.prop.user_id && letter_info.fid_from != 0) { return letter_info.fid_from; }
  return -1;
}

function SelectAllLetters()
{
  if (!CurrentFolder) return;
  if ($F("SelectedAllLetters"))
  {
    Letters.each(function(letter_info)
      {
        $("SelectedLetter"+letter_info.id).writeAttribute("checked","checked");
      });
    LetterSelected();
  }
  else UnselectAllLetters();
}

function UnselectAllLetters()
{
  if (!CurrentFolder) return;
  Letters.each(function(letter_info)
    {
      $("SelectedLetter"+letter_info.id).writeAttribute("checked",null);
    })
}

function LetterSelected()
{
  if (!CurrentFolder) return;
  var numselected = 0;
  Letters.each(function(letter_info)
    {
      if ($F("SelectedLetter"+letter_info.id))
      {
        numselected++;
      }
    });
  if (numselected > MaxSimultaneousLetters)
  {
    alert("��������: ������������� ��������� �����, ��� "+MaxSimultaneousLetters+" ����� ���������!");
  }
}

//Sending request for deleting letters
function DeleteLetters()
{
  if (!CurrentFolder) return;
  if (!confirm("������������� ������� ��������� ������?")) return;
  var sent_ids_array = new Array();
  var recv_ids_array = new Array();
  Letters.each(function(letter_info)
    {
      if ($F("SelectedLetter"+letter_info.id))
      {
        if (letter_info.fid_from == CurrentFolder) sent_ids_array.push(letter_info.id);
        else if (letter_info.fid_to == CurrentFolder) recv_ids_array.push(letter_info.id);
      }
    });
  var sent_ids = "";
  var recv_ids = "";
  if (sent_ids_array.length > 0) sent_ids = sent_ids_array.join(",");
  if (recv_ids_array.length > 0) recv_ids = recv_ids_array.join(",");

  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: "letters.move",
               sent_ids: sent_ids,
               recv_ids: recv_ids,
               from: CurrentFolder,
               to: TrashFolderId,
               check: community.prop.check
             },
       onSuccess: onSuccessDeleteLetters,
       onFailure: community.ajax_failure.bind(community)
      });
}

function DeleteLetter(id)
{
  if (!CurrentFolder) return;
  if (!confirm("������������� ������� ������?")) return;
  var sent_ids = "";
  var recv_ids = "";
  var letter_info = GetLetterById(id);
  if (!letter_info) return;
  if (letter_info.fid_from == CurrentFolder) sent_ids = id;
  else if (letter_info.fid_to == CurrentFolder) recv_ids = id;
  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: "letters.move",
               sent_ids: sent_ids,
               recv_ids: recv_ids,
               from: CurrentFolder,
               to: TrashFolderId,
               check: community.prop.check
             },
       onSuccess: onSuccessDeleteLetters,
       onFailure: community.ajax_failure.bind(community)
      });
}

function onSuccessDeleteLetters(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    alert("������ �������� �����: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  ReloadFolders(response.join(" "),'force');
}

//Sending request for moving letters
function MoveLetters(id)
{
  if (!CurrentFolder) return;
  var sent_ids_array = new Array();
  var recv_ids_array = new Array();
  Letters.each(function(letter_info)
    {
      if ($F("SelectedLetter"+letter_info.id))
      {
        if (letter_info.fid_from == CurrentFolder) sent_ids_array.push(letter_info.id);
        else if (letter_info.fid_to == CurrentFolder) recv_ids_array.push(letter_info.id);
      }
    });
  var sent_ids = "";
  var recv_ids = "";
  if (sent_ids_array.length > 0) sent_ids = sent_ids_array.join(",");
  if (recv_ids_array.length > 0) recv_ids = recv_ids_array.join(",");
  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: "letters.move",
               sent_ids: sent_ids,
               recv_ids: recv_ids,
               from: CurrentFolder,
               to: $F("MoveLettersTarget"),
               check: community.prop.check
             },
       onSuccess: onSuccessMoveLetters,
       onFailure: community.ajax_failure.bind(community)
      });
}

function onSuccessMoveLetters(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    alert("������ ����������� �����: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  ReloadFolders(response.join(" "),'force');
}

function MarkLettersReadAll()
{
  if (!CurrentFolder) return;
  if (!confirm("������������� �������� ��� ������, ��� �����������, � ������� �����?")) return;
  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: "letters.read_all",id: CurrentFolder,check: community.prop.check },
       onSuccess: onSuccessMarkLettersReadAll,
       onFailure: community.ajax_failure.bind(community)
      });
}

function onSuccessMarkLettersReadAll(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    alert("������ ������� ����� ��� �����������: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  ReloadFolders(response.join(" "),'force');
}

//Generating letter (i.e. letter header) html area
function GenerateLetterEntry(letter_info,notchecked)
{
  var cell_b = "";
  var celle_b = "";
  if (!letter_info.recv) { cell_b='<b>'; celle_b='</b>'; }

  var read_style = ' style="cursor: pointer;" onclick="ReadLetter(' + letter_info.id + ',';
  if (letter_info.to == community.prop.user_id && letter_info.sent && !letter_info.recv)
  {
    read_style += '1';
  }
  else read_style += '0';
  read_style += '); return false"';

  var value = '<tr id="Letter' + letter_info.id + '">';

  value += '<td style="width:30px;text-align:center;"><input type="checkbox" id="SelectedLetter' + letter_info.id + '"';
  if (!notchecked && $("SelectedLetter" + letter_info.id) && $F("SelectedLetter" + letter_info.id))
  {
    value += ' checked="checked"';
  }
  value += ' onclick="LetterSelected();" /></td>';

  if (Number(letter_info.from))
  {
    value += '<td style="width:100px; white-space:nowrap;">' + cell_b + (letter_info.title_from ? community.user_title(letter_info.title_from) : '') + celle_b + '</td>';
  }
  else
  {
    value += '<td style="width:100px; white-space:nowrap;">' + cell_b + letter_info.title_from + celle_b + '</td>';
  }

  if (letter_info.title_to) value=value+'<td style="width:100px; white-space:nowrap;">'+cell_b+(letter_info.title_to ? community.user_title(letter_info.title_to) : '')+celle_b+'</td>';
  else value += '<td style="width:100px; white-space:nowrap;">&nbsp;</td>';

  value += '<td' + read_style + '>' + cell_b + (letter_info.subject ? letter_info.subject : '<i>(��� ����)</i>') + celle_b + '</td>';

  value += '<td class="send_date"' + read_style + '>' + cell_b;
  if (letter_info.sent) value += letter_info.sent;
  else value += '&nbsp;';
  value += celle_b + '</td></tr>';

  return value;
}

//--------------------------------------------------------------------------------------------
//Edit (or read) letter area

//Clearing edit letter area
function EmptyEditLetterArea()
{
  if ($("EditLetterBody")) Community.HTMLEditor.remove("EditLetterBody");
  if ($("EditLetterArea")) $("EditLetterArea").update("");
  CurrentLetter="";
}

//Writing new letter request
function NewLetter()
{
  if (!CurrentFolder) return;
  EmptyEditLetterArea();
  $("EditLetterArea").update(GenerateEditLetterForm());
  new Community.HTMLEditor("EditLetterBody");
  $("EditLetterArea").scrollTo();
}

//Sending read letter request
function ReadLetter(id,receive)
{
  if (!CurrentFolder) return;
  EmptyEditLetterArea();
  if (LettersBodyCache.get(id))
  {
    var letter_info = LettersBodyCache.get(id);
    if (letter_info.sent) $("EditLetterArea").update(GenerateReadLetterForm(letter_info));
    else
    {
      $("EditLetterArea").update(GenerateEditLetterForm(letter_info));
      new Community.HTMLEditor("EditLetterBody");
    }
	$("EditLetterArea").scrollTo();
    CurrentLetter = id;
    return;
  }
  $("EditLetterArea").update("���� ��������...");
  $("EditLetterArea").scrollTo();
  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: (receive ? "letter.receive" : "letter.read"),
               id: id,
               check: community.prop.check
             },
       onSuccess: function (transport) { onSuccessReadLetter(transport,id,receive); },
       onFailure: community.ajax_failure.bind(community)
      });
}

function onSuccessReadLetter(transport,id,receive)
{
  var index = GetLetterIndexById(id);
  if (index == null) return;
  //if (!CurrentFolder || CurrentFolder != GetLetterFolder(Letters[index])) { $("EditLetterArea").update("���� ��������... CurrentFolder:"+CurrentFolder+"-"+GetLetterFolder(Letters[index]) + " - "+ transport.responseText); return; }
  if (!CurrentFolder || CurrentFolder != GetLetterFolder(Letters[index])) return;
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    alert("������ ��������� ������: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  EmptyEditLetterArea();
  CurrentLetter = id;

  var letter_info=response.join(" ").evalJSON(true);
  if (letter_info.sent)
  {
    Letters[index]=letter_info;
    $("EditLetterArea").update(GenerateReadLetterForm(letter_info));
  }
  else
  {
    $("EditLetterArea").update(GenerateEditLetterForm(letter_info));
    new Community.HTMLEditor("EditLetterBody");
  }
  if (receive)
  {
    Letters[index]=letter_info;
    $("Letter"+letter_info.id).replace(GenerateLetterEntry(letter_info));
    UpdateFolders();
  }
  LettersBodyCache.set(letter_info.id,letter_info);
}

//Answering letter request
function AnswerLetter()
{
  if (!CurrentLetter) return;
  var re_subject;
  var index = GetLetterIndexById(CurrentLetter);
  if (index == null) return;
  var ReadedInfo = Letters[index];
  if (!ReadedInfo) return;
  var SubjectArray = reReSubj.exec(ReadedInfo.subject);
  if (SubjectArray && SubjectArray.length == 3)
  {
    SubjectArray.splice(0,1);
    if (!SubjectArray[0]) re_subject = "Re(2): "+SubjectArray[1];
    else
    {
      re_subject = "Re("+(SubjectArray[0].slice(1,-1) - 0 + 1)+"): "+SubjectArray[1];
    }
  }
  else re_subject = "Re: "+ReadedInfo.subject;

  var AnswerInfo = {
        title_to: ReadedInfo.title_from,
        subject: re_subject,
        body: ""+ReadedInfo.title_from+" �����(�): "+ReadedInfo.body,
        id: ""
       };
  new Insertion.Bottom("EditLetterArea",GenerateEditLetterForm(AnswerInfo));
  $("AnswerLetterButton").disable();
  $("EditLetterBody").focus();
  new Community.HTMLEditor("EditLetterBody");
}

//Saving letter request
function SaveLetter(send,id)
{
  var title_to = $F("EditLetterTitleTo");
  new Ajax.Request(community.url.letters_ajax,
      {
       parameters: { cmd: "letter.save",
               id: (id ? id : ""),
               title_to: $F("EditLetterTitleTo"),
               subject: $F("EditLetterSubject"),
               body: tinyMCE.get("EditLetterBody").getContent(),
               send: send,
               save_sended: $F("EditLetterSaveSended"),
               check: community.prop.check
             },
       onSuccess: function (transport)
          {
            onSuccessSaveLetter(transport,id,send,title_to);
          },
       onFailure: community.ajax_failure.bind(community)
      });
  EmptyEditLetterArea();
  $("EditLetterArea").update("���� ����������...");
}

function onSuccessSaveLetter(transport,id,send,title_to)
{
  if (!CurrentFolder) return;
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    alert("������ ���������� ������: "+transport.responseText);
    $("EditLetterArea").update("");
    return;
  }
  response.splice(0,1);
  ReloadFolders(response.join(" "),'force');
  if (send) AddContact(title_to);
}

function GenerateEditLetterForm(letter_info)
{
  var value = '<div class="block"><h2>';
  if (letter_info && letter_info.id) value += '�������������� ������';
  else value += '����� ������';
  value += '</h2>';
  value += '<div class="content"><table class="info">';
  value += '<tr><td style="width:60px;">��:</td><td>'+community.user_title(community.prop.user_title)+'</td></tr>';
  value += '<tr><td>����:</td><td><input style="width:500px;" type="input" class="inp_text" id="EditLetterTitleTo" value="'+(letter_info ? letter_info.title_to : '')+'"></input></td></tr>';
  value += '<tr><td>����:</td><td><input style="width:500px;" type="input" class="inp_text" id="EditLetterSubject" value="'+(letter_info ? letter_info.subject : '')+'"></input></td></tr>';
  value += '<tr><td colspan="2"><textarea id="EditLetterBody" style="width:570px;height:180px;">'+(letter_info ? letter_info.body : '')+'</textarea>';
  value += '</td></tr></table>';
  value += '<div class="oh"><input type="checkbox" id="EditLetterSaveSended" checked>��������� ��������� ������</input><br /><br />';
  value += '<input type="button" class="button mr5" onclick="SaveLetter(1'+(letter_info && letter_info.id ? ','+letter_info.id : '')+');" value="�������" />';
  value += '<input class="button" type="button" onclick="SaveLetter(0'+(letter_info && letter_info.id ? ','+letter_info.id : '')+');" value="���������" />';
  value += '</div></div></div>';
  return value;
}

function GenerateReadLetterForm(letter_info)
{
  var value = '<div class="block"><h2>�������� ������</h2><div class="content ReadLetter">';
  value += '<table class="info"><tbody>';
  value += '<tr><td style="width:90px;">��:</td><td>' + (Number(letter_info.from) ? community.user_title(letter_info.title_from) : letter_info.title_from) + '</td></tr>';
  value += '<tr><td>����:</td><td>' + community.user_title(letter_info.title_to) + '</td></tr>';
  value += '<tr><td>����:</td><td>' + letter_info.subject + '</td></tr>';
  if (letter_info.sent) value += '<tr><td>����������:</td><td>' + letter_info.sent + '</td></tr>';
  if (letter_info.recv) value += '<tr><td>��������:</td><td>' + letter_info.recv + '</td></tr>';
  value += '</tbody></table><div class="LetterBody">' + community.convert_smiles(letter_info.body) + '</div>';
  value += '<div class="oh">';
  if (letter_info.title_to == community.prop.user_title && Number(letter_info.from)) value += '<input type="button" class="mr5 button" onclick="AnswerLetter();" id="AnswerLetterButton" value="��������" />';
  if (!IsInTrash(CurrentFolder)) value += '<input type="button" class="button" onclick="DeleteLetter(\'' + letter_info.id + '\');" value="�������" />';
  value += "</div></div></div>";
  return value;
}

//--------------------------------------------------------------
function onScrollLetters()
{
  if (!CurrentFolder || !Letters || NoMoreScrollUpdate) return;
  var len=Letters.length;
  if (len < 20) return;
  if (!$("Letter"+Letters[len-3].id)) return;
  var height = $("LettersDiv").getHeight();
  if (($("LettersDiv").scrollTop + height) >= $("Letter"+Letters[len-3].id).offsetTop)
  {
    var MyCurrentFolder = CurrentFolder;
    new Ajax.Request(community.url.letters_ajax,
        {
        parameters: {
                cmd: "letters.get",
                id:CurrentFolder,
                last_id:Letters[len-1].id,
                check: community.prop.check
              },
        onSuccess: function(transport) { onScrollLettersSuccess(transport,MyCurrentFolder); },
        onFailure: function (transport) { return; }
        });
  }
}

function onScrollLettersSuccess(transport,my_current_folder)
{
  if (CurrentFolder != my_current_folder) return;
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") return;
  response.splice(0,2);
  ScrollLetters(response.join(" "),1);
}

//----------------------------------------------------------------------------------------------------
//Contacts Functions

function InitContacts()
{
  new Ajax.Request(community.url.letters_ajax,
      {
      parameters: {
          cmd: "contacts.get",
          check: community.prop.check
            },
      onSuccess: onInitContacts,
      onFailure: community.ajax_failure.bind(community)
      });
}

function onInitContacts(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") return;
  response.splice(0,1);
  Contacts=$A(response.join(" ").evalJSON(true));
  var value = '<ul id="ContactsUL">';
  Contacts.each(function(contact,index)
         {
      value += GenerateContactEntry(contact,index);
         });
  value += '</ul>';
  $("Contacts").update(value);
}

function GenerateContactEntry(contact,index)
{
  var value = '<li><div class="mb10">';
  value += '<a href="#" onclick="DeleteContact(\''+index+'\'); return false">';
  value += '<img class="delete" src="/i/comm/skin' + community.prop.skin + '/icon_delete.gif" alt="�������" title="�������" /></a>'; //community.image.del
  value += community.user_title(contact) + '</div>';
  value += '<a class="link mr10" href="'+community.url.letters_chat+'?contact='+contact+'">���������</a>';
  value += '<a class="link" href="#" onclick="ActivateContact(\''+index+'\'); return false">��������</a>';

  value += '</li>';
  return value;
}

function AddContact(added_contact)
{
  var contact = added_contact;
  if (!contact) contact = $F("NewContact");
  if (!contact)
  {
    alert("������: ���������� ������� ���!");
    return;
  }
  new Ajax.Request(community.url.letters_ajax,
             {
        parameters: {cmd: "contact.add", contact: contact, check:community.prop.check },
        onSuccess: onSuccessAddContact,
        onFailure: community.ajax_failure.bind(community)
       });
}

function onSuccessAddContact(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    alert("������: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  var contact=response.join(" ");
  if (Contacts.indexOf(contact) == -1)
  {
    new Insertion.Bottom("ContactsUL",GenerateContactEntry(contact,Contacts.length));
    Contacts.push(contact);
  }
  $("NewContact").value="";
}


function DeleteContact(index)
{
  if (!confirm("������������� ������� ������� "+Contacts[index]+"?")) return;
  new Ajax.Request(community.url.letters_ajax,
             {
        parameters: {cmd: "contact.delete", contact: Contacts[index], check:community.prop.check },
        onSuccess: onSuccessDeleteContact,
        onFailure: community.ajax_failure.bind(community)
       });
}

function onSuccessDeleteContact(transport)
{
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    alert("������ �������� ��������: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  var contact=response.join(" ");
  var index = Contacts.indexOf(contact);
  if (index != -1)
  {
    Contacts.splice(index,1);
    InitContacts();
  }
}

function ActivateContact(index)
{
  if (!$("EditLetterTitleTo")) NewLetter();
  if (!$("EditLetterTitleTo")) return;
  $("EditLetterTitleTo").value=Contacts[index];
}

function CheckParams()
{
  if (CheckedParams) return;
  if (WriteTo)
  {
    if (!$("EditLetterTitleTo")) NewLetter();
    if (!$("EditLetterTitleTo")) return;
    $("EditLetterTitleTo").value=WriteTo;
  }
  else if (ReadId && Letters)
  {
    for (var i=0;i<Letters.length;i++)
    {
      var letter_info = Letters[i];
      if (letter_info.id == ReadId)
      {
         if (letter_info.to == community.prop.user_id && letter_info.sent && !letter_info.recv)
         {
           ReadLetter(letter_info.id,1);
         }
         else
         {
           ReadLetter(letter_info.id,0);
         }
        break;
      }
    }
  }
  CheckedParams = true;
}
