var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Photos.Add = Class.create({
  initialize: function(element,album_id,options)
  {
  this.id = Community.Photos.Add.id++;
  this.album_id = album_id;
  if (!options) options = {};
  this.def_rd_acc = options.def_rd_acc || "all";
  this.def_wr_acc = options.def_wr_acc || "community";
  this.upload_windows = new Array();
  this.upload_queue = new Array();
  this.num_upload_windows = 0;
  this.is_uploading = false;
  this.element = $(element);
  if (!this.element) throw new Error("����������� ��������� ��� ���������� ����������!");
  this.element.update(this.html());
  $(this.add_upload_id()).observe("click",this.add_upload_window.bindAsEventListener(this));
  $(this.upload_all_id()).observe("click",this.upload_all.bindAsEventListener(this));

  community.init.add_unload(this.clean.bind(this));
  this.photos_object = new Community.Photos(this.add_photos_id(),{can_edit:true,can_delete:true});
  this.add_upload_window(null);
  },

  clean: function()
  {
  this.stop();
  if (this.upload_windows)
  {
    this.upload_windows.each(function(value) { value.window.clean(); });
    this.upload_windows.clear();
  }
  if (this.photos_object) { this.photos_object.clean(); this.photos_object=null; }
  this.element = null;
  },

  stop: function()
  {
  var t_this = this;
  var add_upload_element = $(this.add_upload_id());
  if (add_upload_element) add_upload_element.stopObserving("click");
  if (this.upload_windows)
  {
    this.upload_windows.each(function(value)
    {
      t_this.stop_upload_window(value.upload_index);
    });
  }
  if (this.photos_object) this.photos_object.clear();
  },

  stop_upload_window: function(upload_index)
  {
  var iframe_element = $(this.add_upload_iframe_id(upload_index));
  if (iframe_element) iframe_element.stopObserving("load");
  var button_element = $(this.add_upload_button_id(upload_index));
  if (button_element) button_element.stopObserving("click");
  var body_line_element = $(this.add_upload_body_line_id(upload_index));
  if (body_line_element) body_line_element.stopObserving("click");
  var body_label_element = $(this.add_upload_body_label_id(upload_index));
  if (body_label_element) body_label_element.stopObserving("click");
  var upload_window = this.get_upload_window(upload_index);
  if (upload_window) upload_window.window.clear();
  },

  add_photos_id: function()
  { return "Community_Photos_Add_Photos_"+this.id; },
  add_upload_id: function()
  { return "Community_Photos_Add_Upload_"+this.id; },
  upload_all_id: function()
  { return "Community_Photos_Upload_All_"+this.id; },
  add_upload_container_id: function()
  { return "Community_Photos_Add_Upload_Container_"+this.id; },
  add_upload_photo_id: function()
  { return "Community_Photos_Add_Upload_Photo_"+this.id; },
  add_upload_arch_id: function()
  { return "Community_Photos_Add_Upload_Arch_"+this.id; },
  add_upload_iframe_id: function(upload_index)
  { return "Community_Photos_Add_Upload_IFrame_"+upload_index+'_'+this.id; },
  add_upload_form_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Form_"+upload_index+'_'+this.id; },
  add_upload_content_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Content_"+upload_index+'_'+this.id; },
  add_upload_button_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Button_"+upload_index+'_'+this.id; },
  add_upload_title_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Title_"+upload_index+'_'+this.id; },
  add_upload_body_line_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Body_Line_"+upload_index+'_'+this.id; },
  add_upload_body_form_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Body_Form_"+upload_index+'_'+this.id; },
  add_upload_body_label_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Body_Label_"+upload_index+'_'+this.id; },
  add_upload_body_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Body_"+upload_index+'_'+this.id; },
  add_upload_tags_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Tags_"+upload_index+'_'+this.id; },
  add_upload_rd_acc_id: function(upload_index)
  { return "Community_Photos_Add_Upload_RdAcc_"+upload_index+'_'+this.id; },
  add_upload_wr_acc_id: function(upload_index)
  { return "Community_Photos_Add_Upload_WrAcc_"+upload_index+'_'+this.id; },
  add_upload_status_id: function(upload_index)
  { return "Community_Photos_Add_Upload_Status_"+upload_index+'_'+this.id; },

  get_upload_window: function(upload_index)
  {
  for (var i=0;i<this.upload_windows.length;i++)
  {
    if (this.upload_windows[i].upload_index == upload_index) return this.upload_windows[i];
  }
  return null;
  },

  get_upload_window_index: function(upload_index)
  {
  for (var i=0;i<this.upload_windows.length;i++)
  {
    if (this.upload_windows[i].upload_index == upload_index) return i;
  }
  return null;
  },

  check_upload_queue: function(upload_index)
  {
    for (var i=0;i<this.upload_queue.length;i++)
    {
      if (this.upload_queue[i].upload_index == upload_index) return true;
    }
    return false;
  },

  html: function()
  {
  var value = '<div id="' + this.add_photos_id() + '"></div>';
  value += '<div class="clear"></div><br />';
  value += '<div id="' + this.add_upload_container_id() + '"></div><br />';
  value += '<a href="#" class="action_link" id="' + this.add_upload_id() + '" onclick="return false;">�������� ��� ����� ��� ��������</a><br /><br />';
  value += '<a href="#" class="action_link" id="' + this.upload_all_id() + '" onclick="return false;">��������� ���</a>';

  //value=value+'<input type="button" value="��������� �����" id="'+this.add_upload_arch_id()+'"></input>';
  return value;
  },

  upload_window_content: function(upload_index)
  {
  var value = '<iframe style="display:none" src="about:blank" id="' + this.add_upload_iframe_id(upload_index) + '" name="' + this.add_upload_iframe_id(upload_index) + '"></iframe>';

  value += '<form id="' + this.add_upload_form_id(upload_index) + '" method="post" enctype="multipart/form-data" target="' + this.add_upload_iframe_id(upload_index) + '" action="' + community.url.photo_add_ajax + '">';
  value += '<table class="table add_photo"><tbody><tr><td style="width:200px;">����������</td><td><input type="file" id="'+this.add_upload_content_id(upload_index)+'" name="content" size="30" />';
  value += '</td></tr><tr><td>���������</td><td>';
  value += '<input class="inp_text" name="title" id="'+this.add_upload_title_id(upload_index)+'" type="text" style="width:450px;"/></td></tr>';
  value += '<tr id="'+this.add_upload_body_line_id(upload_index)+'" style="cursor: pointer;"><td class="insideWinHeader">��������� ��������</td><td></td></tr>';
  value += '<tr id="'+this.add_upload_body_form_id(upload_index)+'" style="display:none;"><td style="cursor: pointer;" id="'+this.add_upload_body_label_id(upload_index)+'">��������</td><td>';
  value += '<textarea name="body" id="'+this.add_upload_body_id(upload_index)+'" class="inp_text" style="width:450px;height:180px;"></textarea></td></tr><tr><td>�����</td><td>';
  value += '<input name="tag" id="'+this.add_upload_tags_id(upload_index)+'" type="text" style="width:450px" class="inp_text"/></td></tr>';
  value += '<tr><td>����� ��������</td><td><div class="por" style="z-index:3000;"><select name="rd_acc" id="'+this.add_upload_rd_acc_id(upload_index)+'" onchange="community.update_wr_acc_select(\''+this.add_upload_rd_acc_id(upload_index)+'\',\''+this.add_upload_wr_acc_id(upload_index)+'\');">';
  var t_this = this;
  community.acc_title.each(function(pair)
          {
      value += '<option value="'+pair.key+'"';
      if (pair.key == t_this.def_rd_acc) value += ' selected="selected"';
      value += '>'+pair.value+'</option>';
          }
         );
  value += '</select></div></td></tr>';
  value += '<tr><td>����� ��������������</td><td><div class="por"><select name="wr_acc" id="'+this.add_upload_wr_acc_id(upload_index)+'">';
  community.wr_acc_title.each(function(pair)
          {
      value += '<option value="'+pair.key+'"';
      if (pair.key == t_this.def_wr_acc) value += ' selected="selected"';
      value += '>'+pair.value+'</option>';
          }
         );
  value += '</select></div></td></tr>';
  value += '<tr><td>������</td><td id="'+this.add_upload_status_id(upload_index)+'">�� �����������</td></tr>';
  value += '</tbody></table>';
  value += '<input type="hidden" name="check" value="'+community.prop.check+'"></input>';
  value += '<input type="hidden" name="cmd" value="photo_add"></input>';
  value += '<input type="hidden" name="where" value="'+this.album_id+'"></input>';
  value += '<input class="button mt10 mb10" type="button" id="'+this.add_upload_button_id(upload_index)+'" value="���������" />';
  value += '</form>';
  return value;
  },


  upload_window_arch_content: function(upload_index)
  {
  var value='<iframe style="display:none" src="about:blank" id="'+this.add_upload_iframe_id(upload_index)+'" name="'+this.add_upload_iframe_id(upload_index)+'"></iframe>';
  value=value+'<form id="'+this.add_upload_form_id(upload_index)+'" method="post" enctype="multipart/form-data" target="'+this.add_upload_iframe_id(upload_index)+'" action="'+community.url.photo_add_ajax+'">';
  value=value+'<table><tbody><tr><td width="20%">�����:</td><td><input type="file" id="'+this.add_upload_content_id(upload_index)+'" name="content" size="40"></input>';
  value=value+'</td><td><input type="button" id="'+this.add_upload_button_id(upload_index)+'" value="���������"></input></td></tr><tr><td>������:</td><td colspan="2" id="'+this.add_upload_status_id(upload_index)+'">�� �����������</td></tr>';
  value=value+'</tbody></table></td></tr></tbody></table>';
  value=value+'<input type="hidden" name="check" value="'+community.prop.check+'"></input>';
  value=value+'<input type="hidden" name="cmd" value="photo.archive"></input>';
  value=value+'<input type="hidden" name="where" value="'+this.album_id+'"></input></form></div>';
  return value;
  },

  add_upload_window: function(event)
  {
  var upload_index = this.num_upload_windows++;
  var new_window = new Community.Window({
            container: this.add_upload_container_id(),
            title:"�������� ����������",
            content:this.upload_window_content(upload_index),
            on_close:this.upload_window_close.bind(this,upload_index)
               });
  $(this.add_upload_iframe_id(upload_index)).observe("load",this.upload_success.bindAsEventListener(this,upload_index));
  $(this.add_upload_button_id(upload_index)).observe("click",this.add_to_upload.bindAsEventListener(this,upload_index));
  $(this.add_upload_body_line_id(upload_index)).observe("click",this.toggle_body.bindAsEventListener(this,upload_index));
  $(this.add_upload_body_label_id(upload_index)).observe("click",this.toggle_body.bindAsEventListener(this,upload_index));
  new_window.show();
  makeSelectBox("#"+this.add_upload_rd_acc_id(upload_index));
  makeSelectBox("#"+this.add_upload_wr_acc_id(upload_index));
  this.upload_windows.push({window:new_window,type:"photo",upload_index:upload_index});
  },

  add_upload_arch_window: function(event)
  {
  var upload_index = this.num_upload_windows++;
  var new_window = new Community.Window({
            container: this.upload_container_id(),
            title:"�������� ������",
            content:this.upload_window_arch_content(upload_index),
            on_close:this.upload_window_close.bind(this,upload_index)
               });
  $(this.add_upload_iframe_id()).observe("load",this.upload_success.bindAsEventListener(this,upload_index));
  $(this.add_upload_button_id()).observe("click",this.add_to_upload.bindAsEventListener(this,upload_index));
  new_window.show();
  this.upload_windows.push({window:new_window,type:"arch",upload_index:upload_index});
  },

  upload_window_close: function(upload_index)
  {
    this.stop_upload_window(upload_index);
    this.upload_windows.splice(this.get_upload_window_index(upload_index),1);
  },

  toggle_body: function(event,upload_index)
  {
  $(this.add_upload_body_form_id(upload_index)).toggle();
  $(this.add_upload_body_line_id(upload_index)).toggle();
  },

  add_to_upload: function(event,upload_index)
  {
  var upload_window=this.get_upload_window(upload_index);
  if (!upload_window) return;
  if (this.check_upload_queue(upload_index)) return;
  this.upload_queue.push({upload_index:upload_index,status:"none"});
  $(this.add_upload_status_id(upload_index)).update("��������...");
  this.check_upload_queue();
  },

  check_upload_queue: function()
  {
  if (this.is_uploading) return;
  var t_this = this;
  this.upload_queue.each(function(value,index)
    {
      if (value.status == "none")
      {
        t_this.start_upload(index);
        throw $break;
      }
    });
  },

  start_upload: function(queue_index)
  {
  var upload_index = this.upload_queue[queue_index].upload_index;
  $(this.add_upload_status_id(upload_index)).update("�����������...");
  this.upload_queue[queue_index].status="loading";
  this.is_uploading = true;
  this.uploading_index = queue_index;
  $(this.add_upload_form_id(upload_index)).submit();
  },

  upload_success: function(event,upload_index)
  {
  var i = $(this.add_upload_iframe_id(upload_index));
  var d;
        if (i.contentDocument) d = i.contentDocument;
        else if (i.contentWindow) d = i.contentWindow.document;
        else throw new Error("���������������� ��� iframe");
        if (d.location.href == "about:blank") return;
  var response = d.body.innerHTML;
  var upload_window = this.get_upload_window(upload_index);
  if (!upload_window) return;
  var params = response.split(' ');
  var upload_type=upload_window.type;

  if (params[0] != "OK")
  {
    $(this.add_upload_status_id(upload_index)).update("������ ��������: "+response);
    this.after_upload_success(upload_index);
    return;
  }

  var new_id = params[1];
  params.splice(0,2);

  var new_info = params.join(" ").evalJSON(true);

  if (upload_type == "photo")
  {
    this.photos_object.add({
          id:new_id,
          type:new_info.type,
          title:new_info.title,
          body:new_info.body,
          rd_acc:new_info.rd_acc,
          wr_acc:new_info.wr_acc,
          can_edit:new_info.can_edit,
          can_delete:new_info.can_delete
              });
    this.stop_upload_window(upload_index);
    this.upload_windows.splice(this.get_upload_window_index(upload_index),1);
  }
  else if (upload_type == "arch")
  {
    this.stop_upload_window(upload_index);
    this.upload_windows.splice(this.get_upload_window_index(upload_index),1);
  }
  this.after_upload_success(upload_index);
  },

  after_upload_success: function(upload_index)
  {
  this.is_uploading = false;
  var queue_index = -1;
  this.upload_queue.each(function(value,index)
    {
      if (value.upload_index == upload_index)
      {
        queue_index = index;
        throw $break;
      }
    });
  if (queue_index != -1) this.upload_queue.splice(queue_index,1);
  this.check_upload_queue();
  },

  upload_all: function(event)
  {
    if (!confirm("������ �������� ���� ��������� ����������?")) return;
    for (var i=0;i<this.upload_windows.length;i++)
    {
      var cur_window = this.upload_windows[i];
      this.add_to_upload(event,cur_window.upload_index);
    }
  }

});

Community.Photos.Add.id = 0;