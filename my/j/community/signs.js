var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Sign = Class.create({
  initialize: function(element,obj_id,type,included)
  {
  this.id = Community.Sign.id++;
  this.type = type || 0;
  this.obj_id = obj_id;
  this.element = $(element);
  if (!this.element) throw new Error("�� �������� ����������� ����� ��� ��������!");
  if (community.prop.user_id == 0) return;
  if (included == undefined)
  {
    this.element.update("���� ��������...").show();
    new Ajax.Request(community.url.signs_ajax,
       {
        parameters: {cmd:"sign.get",id:this.obj_id,type:this.type},
        onSuccess: this.init_success.bind(this),
        onFailure: community.ajax_failure.bind(community)
       }
      );
  }
  else
  {
      this.included = Number(included) ? true : false;
      this.display();
  }
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.element = null;
  },

  stop: function()
  {
  var sign_element = $(this.sign_mod_id());
  if (sign_element) sign_element.stopObserving("click");
  },

  sign_mod_id: function() { return "Community_Sign_Mod_"+this.id; },

  init_success: function(transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") throw new Error("������ ��������� ��������: "+transport.responseText);
  response.splice(0,1);
  this.included = (response[0] == "yes" ? true : false);
  this.display();
  },


  toggle: function(id,type)
  {
  this.element.toggle();
  },

  display: function()
  {
  this.stop();
  this.element.update(this.html()).show();
  $(this.sign_mod_id()).observe("click",this.mod_sign.bindAsEventListener(this));
  },

  html: function()
  {
  var value='';
  if (this.included) value += '<a href="#" id="'+this.sign_mod_id()+'" onclick="return false;" class="pseudolink">������� �� ��������</a>';
  else value += '<a href="#" id="'+this.sign_mod_id()+'" onclick="return false;" class="pseudolink">�����������</a>';
  return value;
  },

  mod_sign: function(event)
  {
  if (this.included)
  {
    new Ajax.Request(community.url.signs_ajax,
         {
          parameters: {cmd:"sign.delete",id:this.obj_id,type:this.type,check:community.prop.check},
          onSuccess: this.del_sign_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  }
  else
  {
    new Ajax.Request(community.url.signs_ajax,
         {
          parameters: {cmd:"sign.add",id:this.obj_id,type:this.type,check:community.prop.check},
          onSuccess: this.add_sign_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  }
  },

  add_sign_success: function(transport)
  {
  var response = transport.responseText;
  if (response != "OK") { alert("������ ����������: "+transport.responseText); return; }
  this.included = true;
  this.stop();
  this.display();
  },

  del_sign_success: function(transport)
  {
  var response = transport.responseText;
  if (response != "OK") { alert("������ ��������: "+transport.responseText); return; }
  this.included = false;
  this.stop();
  this.display();
  }

});

Community.Sign.id = 0;
