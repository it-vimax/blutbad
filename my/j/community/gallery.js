var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Gallery = Class.create({
  initialize: function(element,element_slideshow,element_selected,album_id,photos,options)
  {
    this.id = Community.Gallery.id++;
    if (!options) options = {};
    this.element = $(element); //������� ��� ������� ����������� ����������
    this.element_slideshow = $(element_slideshow);
    this.element_selected = $(element_selected); //������� ��� ������ ��������� ����������
    if (!this.element || !this.element_selected) throw new Error("����������� ��������� ��� �������!");
    this.album_id = album_id;
    this.photos = $A(photos); //���� ������ �������� �� ��� ����������, � ������ ��� ������ ��������
    this.selected_photo = -1;
    this.page = options.page || 0;
    this.min_page = this.page;
    this.photo_id = options.photo_id || 0;
    this.pages = Number(options.pages) || 1;
    this.element_length = Number(options.element_length) || 4;
    this.comments_element = $(options.comments_element) || $("CommentsArea");
    this.comments_div = $(options.comments_div) || $("CommentsDiv");
    this.comments = null;
    this.position = 0;
    this.slideshow_interval = 10;
    this.total = Number(options.total);
    this.is_slideshow = false;
    this.slideshow_pe = null;
	this.photo_marks = new Community.PhotoMarksView();
    Event.observe(window,"keydown",this.shortkey.bindAsEventListener(this));
    this.display();
    community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
    this.stop();
    this.element = null;
    this.element_selected = null;
  },

  stop: function()
  {
    this.stop_selected();
    var left_element = $(this.left_button_id());
    if (left_element) left_element.stopObserving("click");
    var right_element = $(this.right_button_id());
    if (right_element) right_element.stopObserving("click");
    var start_slideshow_element = $(this.start_slideshow_id());
    if (start_slideshow_element) start_slideshow_element.stopObserving("click");
    for (var i=0;i<this.element_length;i++)
    {
      var cell_element = $(this.photo_cell_id(i));
      if (cell_element) cell_element.stopObserving("click");
    }
  },

  stop_selected: function()
  {
	this.photo_marks.clean();
    if (this.selected_tags) this.selected_tags.clean();
    if (this.selected_favorite) this.selected_favorite.clean();
    if (this.selected_sign) this.selected_sign.clean();
    if (this.selected_cens) this.selected_cens.clean();
    if (this.selected_rating) this.selected_rating.clean();
    var previous_element = $(this.previous_photo_id());
    if (previous_element) previous_element.stopObserving("click");
    var next_element = $(this.next_photo_id());
    if (next_element) next_element.stopObserving("click");
    var selected_photo_comments_element = $(this.selected_photo_comments_id());
    if (selected_photo_comments_element) selected_photo_comments_element.stopObserving("click");
  },

  shortkey: function(event)
  {
    var keycode = event.keyCode;
    if (event.altKey || event.ctrlKey)
    {
      if (keycode == 40) this.select_next();
      else if (keycode == 38) this.select_previous();
    }
  },

  back_button_id: function() { return "Community_Gallery_Back_Button_"+this.id; },
  play_button_id: function() { return "Community_Gallery_Play_Button_"+this.id; },
  pause_button_id: function() { return "Community_Gallery_Pause_Button_"+this.id; },
  forward_button_id: function() { return "Community_Gallery_Forward_Button_"+this.id; },

  left_button_id: function() { return "Community_Gallery_Left_Button_"+this.id; },
  right_button_id: function() { return "Community_Gallery_Right_Button_"+this.id; },
  left_button_img_id: function() { return "Community_Gallery_Left_Button_Img_"+this.id; },
  right_button_img_id: function() { return "Community_Gallery_Right_Button_Img_"+this.id; },
  previous_photo_id: function() { return "Community_Gallery_Previous_Photo_"+this.id; },
  next_photo_id: function() { return "Community_Gallery_Next_Photo_"+this.id; },
  photo_counter_id: function(index) { return "Community_Gallery_Photo_Counter_"+index+"_"+this.id; },
  photo_cell_id: function(index) { return "Community_Gallery_Photo_Cell_"+index+"_"+this.id; },
  photo_img_id: function(index) { return "Community_Gallery_Photo_Img_"+index+"_"+this.id; },
  photo_title_id: function(index) { return "Community_Gallery_Photo_Title_"+index+"_"+this.id; },
  selected_photo_tags_id: function() { return "Community_Gallery_Selected_Photo_Tags_"+this.id; },
  selected_photo_favorite_id: function() { return "Community_Gallery_Selected_Photo_Favorite_"+this.id; },
  selected_photo_sign_id: function() { return "Community_Gallery_Selected_Photo_Sign_"+this.id; },
  selected_photo_cens_id: function() { return "Community_Gallery_Selected_Photo_Cens_"+this.id; },
  selected_photo_comments_id: function() { return "Community_Gallery_Selected_Photo_Comments_"+this.id; },
  selected_photo_img_id: function() { return "Community_Gallery_Selected_Photo_Img_"+this.id; },
  selected_photo_img_div_id: function() { return "Community_Gallery_Selected_Photo_ImgDiv_"+this.id; },
  selected_photo_marks_id: function() { return "Community_Gallery_Selected_Photo_Marks_"+this.id; },

  start_slideshow_id: function() { return "Community_Gallery_Start_SlideShow_"+this.id; },
  slideshow_interval_id: function() { return "Community_Gallery_SlideShow_Time_"+this.id; },
  rating_area_id: function() { return "Community_Gallery_RatingArea_"+this.id; },

  display: function()
  {
    this.element.update(this.html());
    this.element_slideshow.update(this.html_slideshow());

    $(this.left_button_id()).observe("click",this.roll_left_by.bindAsEventListener(this,this.element_length));
    $(this.right_button_id()).observe("click",this.roll_right_by.bindAsEventListener(this,this.element_length));
    $(this.start_slideshow_id()).observe("click",this.start_slideshow.bindAsEventListener(this));

    $(this.back_button_id()).observe("click",this.backward.bindAsEventListener(this));
    $(this.play_button_id()).observe("click",this.play.bindAsEventListener(this));
    $(this.pause_button_id()).observe("click",this.pause.bindAsEventListener(this));
    $(this.forward_button_id()).observe("click",this.forward.bindAsEventListener(this));

    for (var i=0;i<this.element_length;i++)
    {
      if ($(this.photo_cell_id(i)))
      {
        $(this.photo_cell_id(i)).observe("click",this.select.bindAsEventListener(this,i));
      }
    }
    //Preloading
    if ((this.element_length >= this.photos.length) && (this.pages > 1))
    {
      new Ajax.Request(community.url.albums_ajax,{
          parameters: { cmd:"photo.get",id:this.album_id, page:1 },
          onSuccess: this.get_photos_success.bind(this,0,false),
          onFailure: community.ajax_failure.bind(community)
        });
    }
    if (this.photos.length) { this.select(null,0); }
  },

  html_slideshow: function()
  {
    var value = '';
    value += '<table><tr><td style="vertical-align:middle;">��������&nbsp;</td><td style="vertical-align:middle;"><input type="text" class="inp_text" style="width:40px;" id="'+this.slideshow_interval_id()+'" value="'+this.slideshow_interval+'" /></td><td style="vertical-align:middle;">&nbsp;c��.</td></tr></table><div class="oh"><a class="mt5 button" href="#" onclick="return false;" id="'+this.start_slideshow_id()+'">������ ��������</a></div>';
    return value;
  },

  html: function()
  {
    var value = '';
    value += '<div>';
    value += '<table><tr><td style="vertical-align:middle; padding:5px;">���������:</td>';
    value += '<td style="vertical-align:middle; padding:5px; cursor: pointer;"><img src="/i/comm/back.png" alt="�����" title="�����" id="'+this.back_button_id()+'" /></td>';
    value += '<td style="vertical-align:middle; padding:5px; cursor: pointer;"><img src="/i/comm/play.png" alt="������" title="������" id="'+this.play_button_id()+'" /></td>';
    value += '<td style="vertical-align:middle; padding:5px; cursor: pointer;"><img src="/i/comm/pause.png" alt="����������" title="����������"  id="'+this.pause_button_id()+'"/></td>';
    value += '<td style="vertical-align:middle; padding:5px; cursor: pointer;"><img src="/i/comm/forward.png" alt="� �����" title="� �����"  id="'+this.forward_button_id()+'" /></td>';
    value += '</tr></table>';
    value += '<table style="width:100%;"><tr>';
    value += '<td class="arrow_left"><a href="#" onclick="return false;" id="'+this.left_button_id()+'"';
    value += ' class="not_active"';
    value += '>�����</a><!-- --></td></tr>';
    var i,j;
    for (i=this.position,j=0;i<this.position+this.element_length;i++,j++)
    {
      value += '<tr><td class="pics" colspan="2">';
      if (i < this.photos.length)
      {
        value += '<a href="#" onclick="return false;" id="'+this.photo_cell_id(j)+'">';
      }
      value += '<img id="'+this.photo_img_id(j)+'"';
      value += ' src="'
      if (i<this.photos.length)
      {
        if (Number(this.photos[i].censored)) value += '/i/comm/censored_thmb.png';
        else value += community.url.photo_src+'?size=thmb&photo='+this.photos[i].id;
        value += '" /></a>';
      }
      else value+= '/i/comm/photo_empty.jpg" />';
      value += '</td></tr>';
      if (i<this.photos.length) value += '<tr><td style="vertical-align:middle; text-align:center;" id="'+this.photo_counter_id(j)+'">'+(this.position+j+1)+'/'+this.total+'</td></tr>';
    }
    value += '<tr><td class="arrow_right" align="center">';
    value += '<a href="#" onclick="return false;" id="'+this.right_button_id()+'"';
    if ((this.element_length >= this.photos.length) && (this.pages <= 1))
    {
      value += ' class="not_active"';
    }
    value += '>������</a></td>';
    value += '</tr></tbody></table>';
    value += '</div>';
    return value;
  },

  roll_left: function(event,select)
  {
    if (this.position == 0) return;
    this.position--;
    this.photos_update(this.position+1);
    if (select) this.select(null,0);
  },

  roll_left_by: function(event,count,select)
  {
    if (this.position - count < 0) count = this.position;
    if (count <= 0) return;
    this.position -= count;
    this.photos_update(this.position+count);
    if (select) this.select(null,0);
  },

  roll_right: function(event,select)
  {
    if (this.position+this.element_length >= this.photos.length)
    {
      if (this.page+1 < this.pages)
      {
        new Ajax.Request(community.url.albums_ajax,{
          parameters: { cmd:"photo.get",id:this.album_id, page:this.page+1 },
          onSuccess: this.get_photos_success.bind(this,this.position,true,select ? true : false),
          onFailure: community.ajax_failure.bind(community)
          });
      }
      return;
    }
    this.position++;
    this.photos_update(this.position-1);
    if (select) this.select(null,this.element_length-1);
    this.preload_photos();
  },

  preload_photos: function()
  {
    if (((this.position+this.element_length+this.element_length-1) >= this.photos.length) && (this.page+1 < this.pages))
    {
      new Ajax.Request(community.url.albums_ajax,{
           parameters: { cmd:"photo.get",id:this.album_id, page:this.page+1 },
           onSuccess: this.get_photos_success.bind(this,this.position,false,false),
           onFailure: community.ajax_failure.bind(community)
         });
    }
  },

  preload_all_photos: function(to_end)
  {
    if (this.page+1 < this.pages)
    {
      new Ajax.Request(community.url.albums_ajax,{
           parameters: { cmd:"photo.get",id:this.album_id, page:this.page+1, all:1 },
           onSuccess: this.get_all_photos_success.bind(this,to_end),
           onFailure: community.ajax_failure.bind(community)
         });
    }
  },

  roll_right_by: function(event,count,select)
  {
    if (this.position+this.element_length+count > this.photos.length)
    {
      count = this.photos.length-this.position-this.element_length;
    }
    if (count <= 0) return;
    this.position += count;
    this.photos_update(this.position-count);
    if (select) this.select(null,this.element_length-1);
    this.preload_photos();
  },

  backward: function(event)
  {
    this.roll_left_by(event,this.position);
  },

  play: function(event)
  {
    if (this.play_timer) this.play_timer.stop();
    this.roll_right(null);
    if ((this.position+this.element_length >= this.photos.length) && (this.page+1 >= this.pages)) return;
    this.play_timer = new PeriodicalExecuter(this.update_play.bind(this),1);
  },

  update_play: function()
  {
    this.roll_right(null);
    if ((this.position+this.element_length >= this.photos.length) && (this.page+1 >= this.pages))
    {
      this.play_timer.stop();
    }
  },

  pause: function(event)
  {
    if (this.play_timer) this.play_timer.stop();
  },

  forward: function(event)
  {
    if (this.page+1 >= this.pages && this.photos.length-this.position-this.element_length>0)
    {
      this.roll_right_by(event,this.photos.length-this.position-this.element_length);
    }
    else this.preload_all_photos(true);
  },

  start_slideshow: function(event)
  {
    if (this.is_slideshow) this.stop_slideshow(event);
    else
    {
      this.is_slideshow = true;
      $(this.start_slideshow_id()).update("���������� ��������");
      var interval = Number($F(this.slideshow_interval_id()));
      if (interval == 0) interval = this.slideshow_interval;
      this.slideshow_pe = new PeriodicalExecuter(this.update_slideshow.bind(this),interval);
      this.init_slideshow();
    }
  },

  stop_slideshow: function(event)
  {
    if (this.slideshow_pe) { this.slideshow_pe.stop(); this.slideshow_pe = null; }
    this.is_slideshow = false;
    $(this.start_slideshow_id()).update("������ ��������");
  },

  init_slideshow: function()
  {
    if (this.selected_photo >= this.photos.length-1 && this.page+1 >= this.pages)
    {
      this.roll_left_by(null,this.position,true);
    }
    else this.select_next();
  },

  update_slideshow: function()
  {
    if (this.selected_photo >= this.photos.length-1 && this.page+1 >= this.pages) this.stop_slideshow(null);
    else this.select_next();
  },

  photos_update: function(old_position)
  {
    var i,j;
    for (i=this.position,j=0;i<this.position+this.element_length;i++,j++)
    {
      if (i<this.photos.length)
      {
        $(this.photo_img_id(j)).src = community.url.photo_src+'?size=thmb&photo='+this.photos[i].id;
        $(this.photo_counter_id(j)).update((this.position+j+1)+'/'+this.total);
      }
      else $(this.photo_img_id(j)).src = "/i/comm/photo_empty.jpg";
    }
    if (this.position == 0) $(this.left_button_id()).addClassName("not_active");
    else  $(this.left_button_id()).removeClassName("not_active");

    if (this.position+this.element_length >= this.photos.length && this.page+1 >= this.pages)
    {
      $(this.right_button_id()).addClassName("not_active");
    }
    else
    {
      $(this.right_button_id()).removeClassName("not_active");
    }
    if (this.selected_photo>=0) //���������� ��������� ����������
    {
      var old_selected_photo_index = this.selected_photo - old_position;
      if (old_selected_photo_index >= 0 && old_selected_photo_index < this.element_length)
      {
        $(this.photo_img_id(old_selected_photo_index)).setStyle({ border:""});
      }
      var selected_photo_index = this.selected_photo - this.position;
      if (selected_photo_index >= 0 && selected_photo_index < this.element_length)
      {
        $(this.photo_img_id(selected_photo_index)).setStyle({ border:"1px solid #000"});
      }
    }
  },

  get_photos_success: function(position,roll_right,select,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") throw new Error("������ ��������� ����������: "+transport.responseText);
    response.splice(0,1);
    var result = response.join(" ").evalJSON(true);
    var new_page = Number(result.page);
    if (new_page != this.page+1) return;
    this.page = new_page;
    this.photos = this.photos.concat(result.photos);
    if (roll_right && position == this.position) this.roll_right(null,select);
  },

  get_all_photos_success: function(to_end,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������ ��������� ����������: "+transport.responseText); return; }
    response.splice(0,1);
    var result = response.join(" ").evalJSON(true);
    var new_page = Number(result.page);
    this.page = new_page;
    this.photos = this.photos.concat(result.photos);
    if (to_end && this.photos.length-this.position-this.element_length)
    {
      this.roll_right_by(null,this.photos.length-this.position-this.element_length);
    }
  },

  select: function(event,index)
  {
    if (this.position+index >= this.photos.length) return;
    var selected_photo_index = this.selected_photo - this.position;
    if (selected_photo_index >= 0 && selected_photo_index < this.element_length)
    {
      $(this.photo_img_id(selected_photo_index)).setStyle({ border:""});
    }
    if (index == -1)
    {
      this.element_selected.update("");
      this.selected_photo = -1;
      return;
    }
    this.selected_photo = this.position + index;
    $(this.photo_img_id(index)).setStyle({ border:"1px solid #000" });

    if (this.comments) { this.comments.clean(); this.comments = null; }
    this.comments_div.hide();
    this.comments_element.update('');
    this.stop_selected();
    var photo_info = this.photos[this.position+index];
    this.element_selected.update(this.select_html(photo_info,index));
    this.selected_tags = new Community.Tags(this.selected_photo_tags_id(),photo_info.id,photo_info.type,
                      photo_info.author_id,photo_info.can_edit_tag
                       );
    this.selected_favorite = new Community.Favorite(this.selected_photo_favorite_id(),photo_info.id,
                          photo_info.type
                           );
    this.selected_sign = new Community.Sign(this.selected_photo_sign_id(),photo_info.id,
                          photo_info.type
                           );
    this.selected_cens = new Community.Censure(this.selected_photo_cens_id(),photo_info.id);

    if ($(this.rating_area_id()))
    {
      this.selected_rating = new Community.Rating(this.rating_area_id(),photo_info.id,3);
    }
    if ($(this.previous_photo_id()))
    {
      $(this.previous_photo_id()).observe("click",this.select_previous.bind(this));
    }
    if ($(this.next_photo_id()))
    {
      $(this.next_photo_id()).observe("click",this.select_next.bind(this));
    }
    $(this.selected_photo_comments_id()).observe("click",this.toggle_comments.bind(this));
    if ($(this.selected_photo_marks_id())) {
		this.photo_marks.set_photo(photo_info,this.selected_photo_img_id(),this.selected_photo_img_div_id(),this.selected_photo_marks_id());
	}
  },

  select_previous: function()
  {
    if (this.selected_photo < 0) { this.select(null,0); return; }
    if (this.selected_photo > this.position + this.element_length)
    {
      this.roll_right_by(null,this.selected_photo-this.position-this.element_length,true);
    }
    else if (this.selected_photo-this.position <= 0)
    {
      this.roll_left_by(null,this.position-this.selected_photo+1,true);
    }
    else this.select(null,this.selected_photo-this.position-1);
  },

  select_next: function()
  {
    if (this.selected_photo < 0) { this.select(null,0); return; }
    if (this.selected_photo >= this.photos.length-1 && this.page+1 >= this.pages) return;
    if (this.selected_photo < this.position-1)
    {
      this.roll_left_by(null,this.position-this.selected_photo-1,true);
    }
    else if (this.selected_photo >= this.position+this.element_length-1)
    {
      this.roll_right_by(null,this.selected_photo-this.element_length-this.position+2,true);
    }
    else this.select(null,this.selected_photo-this.position+1);
  },

  toggle_comments: function()
  {
    if (this.selected_photo < 0) return;
    if (this.comments) { this.comments_div.toggle(); return; }
    this.comments_div.show();
    if (Number(community.prop.user_comments_style))
    {
      this.comments = new Community.Comments.Plain(this.comments_element,this.photos[this.selected_photo].id);
    }
    else
    {
      this.comments = new Community.Comments(this.comments_element,this.photos[this.selected_photo].id);
    }
  },

  select_html: function(photo_info,index)
  {
    var value = '<div class="mb20" style="text-align:center;position:relative;width:100%;">';
    if (this.position+index > 0)
    {
      value += '<a style="position:absolute; left:0; top:0;" href="#" onclick="return false;" id="'+this.previous_photo_id()+'">����������</a>';
    }

    if (this.position+index+1 < this.photos.length || this.page+1 < this.pages)
    {
      value += ' <a style="position:absolute; right:0; top:0;" href="#" onclick="return false;" id="'+this.next_photo_id()+'">���������</a>';
    }
    value += '<span class="light">����������� ctrl+�����/���� ��� ���������</span>';
    value += '</div>';
    value += '<div class="mb20" style="text-align:left;">'+community.user_title(photo_info.author_title,{avatar:1,avsize:1})+'<td>: '+photo_info.time_create+'</td></tr></table></div>';
    value += '<table class="centre">';
    value += '<tr><td align="center">';
    value += '<div id="'+this.selected_photo_img_div_id()+'" style="position: relative;">';
    value += '<a href="#" onclick="window.open(\''+community.url.photo_src+'?size=orig&photo='+photo_info.id+'\'); return false;">';
    value += '<img src="';
    if (Number(photo_info.censored)) value += '/i/comm/censored_std.png';
    else value += community.url.photo_src+'?size=std&photo='+photo_info.id;
    value += '" id="'+this.selected_photo_img_id()+'" /></a></div></td></tr>';
    value += '</table>';

    value += '<div class="mt10 mb20"><div class="light fr" id="' + this.selected_photo_sign_id() + '" style="padding-left: 10px;"></div>';
    value += '<div class="light fr" id="' + this.selected_photo_favorite_id() + '"></div>';
    value += '<div class="light fr" id="' + this.selected_photo_cens_id() + '"></div>';
    value += '<p style="text-align:left;"><a href="#" onclick="window.open(\''+community.url.photo_src+'?size=orig&photo='+photo_info.id+'\'); return false;">';
    if (photo_info.title) value += photo_info.title;
    else value += "<i>(��� ��������)</i>"
    value += '</a></p></div>';
    value += '<p style="text-align:left;">���������� ����������: '+photo_info.totalviews+'</p>';
    if (photo_info.body) value += '<p style="text-align:left;"><i>' + photo_info.body + '</i></p>';
    value += '</div>';

	if (!Number(photo_info.censored) && photo_info.marks && photo_info.marks.length && Number(community.prop.user_user_id)) value += '<div id="'+this.selected_photo_marks_id()+'"></div>';

    value += '<div class="inner_tags border mb10 fl" style="width:100%;"><div style="width:100%;" id="'+this.selected_photo_tags_id()+'"></div></div>';

    value += '<div class="clear" style="text-align:left;"><a href="#" onclick="return false;" id = "'+this.selected_photo_comments_id()+'">����������� ('+photo_info.cnt_child+')</a></div>';

    value += '<div class="mt10 mb10" style="text-align:left;">';
    value += '������� ����������: <strong>'+photo_info.rating+'</strong> (������� �������: <em>'+photo_info.rating_votes+'</em>)';
    if (photo_info.can_view_marks && Number(photo_info.rating_votes))
    {
      value += ' <a href="'+'/photo_votes='+photo_info.id+'">���������...</a>';
    }
    value += '</div>';
    if (photo_info.author_id != community.prop.user_id && Number(community.prop.rating_vote_power))
    {
      value += '<div id="'+this.rating_area_id()+'" style="text-align:left;"></div>';
    }
    return value;
  }

});

Community.Gallery.id = 0;