var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Audio = Class.create({
  initialize: function(element,tracks,options)
  {
    options = options || {};
    this.id = Community.Audio.id++;
    this.element = $(element);
    this.disable_tags = options.disable_tags ? true :false;
    this.disable_favorite = options.disable_favorite ? true :false;
    this.disable_sign = options.disable_sign ? true :false;
    this.can_edit = options.can_edit || false;
    this.can_delete = options.can_delete || false;
    this.can_move = options.can_move || false;
    this.can_hide = options.can_hide || false;
    this.on_edit = options.on_edit || null;
    this.on_delete = options.on_delete || null;
    this.on_before_edit = options.on_before_edit || null;
    this.on_before_delete = options.on_before_delete || null;
    this.on_hide = options.on_hide || null;
    this.on_before_hide = options.on_before_hide || null;
    this.on_unhide = options.on_unhide || null;
    this.on_before_unhide = options.on_before_unhide || null;
    this.edit_window = null;
    this.edit_id = 0;
    if (!this.element) throw new Error("����������� ��������� ��� ������������!");
    this.tracks = $A(tracks);
    this.tracks_hash = this.tracks.inject(new Hash(),function(hash,value)
                {
                 hash.set(value.id,value);
                 return hash;
                });
    this.album_id = options.album_id || null;
    this.group_access = options.group_access || null;
    if (this.album_id && this.group_access && $(this.group_access)) {
      $(this.group_access).observe("click",this.show_group_access.bind(this));
    }
    community.init.add_unload(this.clean.bind(this));
    this.display();
  },

  clean: function()
  {
    for (var i=0;i<this.tracks.length;i++)
    {
      this.stop_track(this.tracks[i]);
    }
    if (this.group_access_window) this.group_access_window.clean();
    this.group_access_window = null;
    if (this.group_access && $(this.group_access)) $(this.group_access).stopObserving("click");
  },

  stop_track: function(track_info)
  {
    if (track_info.player) track_info.player.clean();
    if (track_info.favorite) track_info.favorite.clean();
    if (track_info.sign) track_info.sign.clean();
    if (track_info.tags_obj) track_info.tags_obj.clean();
    if ($(this.track_edit_id(track_info.id)))
    {
      $(this.track_edit_id(track_info.id)).stopObserving("click");
    }
    if ($(this.track_delete_id(track_info.id)))
    {
      $(this.track_delete_id(track_info.id)).stopObserving("click");
    }
    if ($(this.track_down_id(track_info.id)))
    {
      $(this.track_down_id(track_info.id)).stopObserving("click");
    }
    if ($(this.track_up_id(track_info.id)))
    {
      $(this.track_up_id(track_info.id)).stopObserving("click");
    }
    if ($(this.track_hide_id(track_info.id)))
    {
      $(this.track_hide_id(track_info.id)).stopObserving("click");
    }
  },

  track_player_id: function(id) { return "Community_Audio_TrackPlayer_"+id+"_"+this.id; },
  track_cell_id: function(id) { return "Community_Audio_TrackCell_"+id+"_"+this.id; },
  track_favorite_id: function(id) { return "Community_Audio_TrackFavorite_"+id+"_"+this.id; },
  track_sign_id: function(id) { return "Community_Audio_TrackSign_"+id+"_"+this.id; },
  track_edit_id: function(id) { return "Community_Audio_TrackEdit_"+id+"_"+this.id; },
  track_delete_id: function(id) { return "Community_Audio_TrackDelete_"+id+"_"+this.id; },
  track_down_id: function(id) { return "Community_Audio_TrackDown_"+id+"_"+this.id; },
  track_up_id: function(id) { return "Community_Audio_TrackUp_"+id+"_"+this.id; },
  track_hide_id: function(id) { return "Community_Audio_TrackHide_"+id+"_"+this.id; },
  track_tags_id: function(id) { return "Community_Audio_TrackTags_"+id+"_"+this.id; },
  edit_title_id: function() { return "Community_Audio_EditTitle_"+this.id; },
  edit_body_id: function() { return "Community_Audio_EditBody_"+this.id; },
  edit_rd_acc_id: function() { return "Community_Audio_EditRdAcc_"+this.id; },
  edit_wr_acc_id: function() { return "Community_Audio_EditWrAcc_"+this.id; },
  edit_status_id: function() { return "Community_Audio_EditStatus_"+this.id; },
  group_access_rd_acc_id: function() { return "Community_Audio_GroupAccessRdAcc_"+this.id; },
  group_access_wr_acc_id: function() { return "Community_Audio_GroupAccessWrAcc_"+this.id; },
  group_access_status_id: function() { return "Community_Audio_GroupAccessStatus_"+this.id; },

  display: function()
  {
    if (!community.is_sound_loaded) { i++; setTimeout(this.display.bind(this),200); return; }
    this.element.update(this.html());
    for (var i=0;i<this.tracks.length;i++)
    {
      this.observe_track(this.tracks[i]);
    }
  },

  observe_track: function(track_info)
  {
    track_info.player = new Community.Audio.Track(this.track_player_id(track_info.id),track_info.id,
               { on_finish_play: this.on_finish_play.bind(this,track_info.id),
                 on_play: this.on_play.bind(this,track_info.id)
               }
                                                 );
    if (this.can_edit && (track_info.can_edit - 0))
    {
      $(this.track_edit_id(track_info.id)).observe("click",this.edit.bindAsEventListener(this,track_info.id));
    }
    if (this.can_delete && (track_info.can_delete - 0))
    {
      $(this.track_delete_id(track_info.id)).observe("click",this.del.bindAsEventListener(this,track_info.id));
    }
    if (this.can_hide && (track_info.can_hide - 0))
    {
      $(this.track_hide_id(track_info.id)).observe("click",this.hide.bindAsEventListener(this,track_info.id));
    }
    if (!this.disable_favorite)
    {
      track_info.favorite = new Community.Favorite(this.track_favorite_id(track_info.id),track_info.id,track_info.type,track_info.favorite_included);
    }
    if (!this.disable_sign)
    {
      track_info.sign = new Community.Sign(this.track_sign_id(track_info.id),track_info.id,track_info.type,track_info.sign_included);
    }
    if (!this.disable_tags)
    {
      track_info.tags_obj = new Community.Tags(this.track_tags_id(track_info.id),track_info.id,track_info.type,track_info.author_id,track_info.can_edit_tag,track_info.tags);
    }
    if (this.can_move && (track_info.can_edit - 0))
    {
      if (track_info.sort_id < this.tracks.length)
      {
        $(this.track_down_id(track_info.id)).observe("click",this.move_down.bindAsEventListener(this,track_info.id));
      }
      if (track_info.sort_id > 1)
      {
        $(this.track_up_id(track_info.id)).observe("click",this.move_up.bindAsEventListener(this,track_info.id));
      }
    }
  },

  html: function()
  {
    value = '';
    for (var i=0;i<this.tracks.length;i++)
    {
      value += this.html_track(this.tracks[i]);
    }
    return value;
  },

  html_track: function(track_info)
  {
    var value = '<div><div class="mb20" id="'+this.track_cell_id(track_info.id)+'">';
    value += '<div class="smallTitle">' + community.user_title(track_info.author_title,{avatar:1,avsize:1}) + '<td>: ' + track_info.time_create + '</td></tr></table></div>';
    value += '<div class="info_wrap"><div class="infoTitle"><h3><a href="' + community.url.track_comments + '?where=' + track_info.id + '">';
    if (track_info.title) value += track_info.title;
    else value += '<em>(��� ��������)</em>';
    value += '</a></h3>';
    value += '<p style="font-size:11px;" class="light"><a class="n_u mr10" href="'+community.url.track_comments+'?where='+track_info.id+'">������������: ' + track_info.cnt_child + '</a>����������: '+track_info.totalviews + '</p>';
    value += '</div>';
    value += '<div class="mt10 mb20">' + track_info.body + '</div>';
    if (track_info.hidden) value += '<p style="color:red;" class="pb10">���������� �� ����������� ������ �����������</p>';
    value += '<div id="'+this.track_player_id(track_info.id)+'"></div>';
    value += '<div class="clear"></div>';
    value += '<ul class="tiny">';
    if (!this.disable_favorite) value += '<li><div id="' + this.track_favorite_id(track_info.id) + '"></div></li>';
    if (!this.disable_sign) value += '<li><div id="' + this.track_sign_id(track_info.id) + '"></div></li>';

    if (this.can_edit && (track_info.can_edit - 0))
    {
      value += '<li><a href="#" id="' + this.track_edit_id(track_info.id) + '" onclick="return false;">������������� �����������</a></li>';
    }
    if (this.can_delete && (track_info.can_delete - 0))
    {
      value += '<li><a href="#" id="' + this.track_delete_id(track_info.id) + '" onclick="return false;">������� �����������</a></li>';
    }
    if (this.can_move && (track_info.can_edit - 0))
    {
      if (track_info.sort_id < this.tracks.length)
      {
        value += '<li><a href="#" id="' + this.track_down_id(track_info.id) + '" onclick="return false;">����</a></li>';
      }
      if (track_info.sort_id > 1)
      {
        value += '<li><a href="#" id="' + this.track_up_id(track_info.id) + '" onclick="return false;">�����</a></li>';
      }
    }

    if (this.can_hide && (track_info.can_hide - 0))
    {
      value += '<li><a href="#" onclick="return false;" id="' + this.track_hide_id(track_info.id) + '">';
      if (track_info.hidden) value += '������� �����������';
      else value += '������ �����������';
      value += '</a></li>';
    }
    value += '</ul><div class="clear"></div>';
    if (!this.disable_tags) value += '<div class="inner_tags" id="' + this.track_tags_id(track_info.id) + '"></div>';
    value += '</div></div></div>';
    return value;
  },

  edit: function(event,track_id)
  {
    if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
    var track_info = this.tracks_hash.get(track_id);
    if (!track_info) return;
    this.edit_window = new Community.Window({
            title:"�������������� ����������� "+(track_info.title ? track_info.title : "(��� ��������)"),
            content:this.edit_window_content(track_info),
            ok_button:true,
            on_ok:this.edit_ok.bind(this)
                 });
    this.edit_window.show();
    if (!track_info.hidden)
    {
      makeSelectBox("#"+this.edit_rd_acc_id());
      makeSelectBox("#"+this.edit_wr_acc_id());
    }
    this.edit_id = track_id;
  },

  edit_window_content: function(track_info)
  {
    var value = '<table><tbody>';
    value += '<tr><td style="width:150px;">���������</td><td>';
    value += '<input id="' + this.edit_title_id() + '" type="text" style="width:500px;" class="inp_text" value="' + track_info.title + '"/></td></tr>';
    value += '<tr><td>��������</td><td>';
    value += '<textarea style="width:500px;height:180px;" class="inp_text" id="' + this.edit_body_id() + '" rows="5">' + track_info.body.gsub("<br />","&#10;") + '</textarea></td></tr>';
    if (!track_info.hidden)
    {
      value += '<tr><td>����� �������</td><td><div class="por" style="z-index:3000;"><select id="' + this.edit_rd_acc_id() + '" onchange="community.update_wr_acc_select(\'' + this.edit_rd_acc_id() + '\',\'' + this.edit_wr_acc_id() + '\');">';
      community.acc_title.each(function(pair)
          {
        value += '<option value="' + pair.key + '"';
        if (track_info.rd_acc == pair.key) value += ' selected="selected"';
        value += '>' + pair.value + '</option>';
          }
         );
      value += '</select></div></td></tr>';
      value += '<tr><td>����� ��������������</td><td><div class="por"><select id="' + this.edit_wr_acc_id() + '">';
      community.wr_acc_title.each(function(pair)
          {
        if (community.acc_order.get(pair.key) < community.acc_order.get(track_info.rd_acc)) return;
        value += '<option value="' + pair.key + '"';
        if (track_info.wr_acc == pair.key) value += ' selected="selected"';
        value += '>' + pair.value + '</option>';
          }
         );
      value += '</select></div></td></tr>';
    }
    value += '<tr><td>������</td><td id="' + this.edit_status_id() + '">�� �����������</td></tr>';
    value += '</tbody></table>';
    return value;
  },

  edit_ok: function()
  {
    $(this.edit_status_id()).update("�����������...");
    var params = {cmd:"track.update",id:this.edit_id,title:$F(this.edit_title_id()),body:$F(this.edit_body_id()),check:community.prop.check};
    if ($(this.edit_rd_acc_id())) params.rd_acc = $F(this.edit_rd_acc_id());
    if ($(this.edit_wr_acc_id())) params.wr_acc = $F(this.edit_wr_acc_id());
    new Ajax.Request(community.url.tracks_ajax,
         {
          parameters: params,
          onSuccess: this.edit_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  },

  edit_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK")
    {
      $(this.edit_status_id()).update("������ ����������: "+transport.responseText);
      return;
    }
    response.splice(0,1);
    var track_info = response.join(" ").evalJSON(true);
    if (this.on_before_edit)
    {
      if (!this.on_before_edit(track_info.id)) return;
    }

    if (track_info.id == this.edit_id)
    {
      if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
    }

    this.stop_track(this.tracks_hash.get(track_info.id));
    this.tracks_hash.set(track_info.id,track_info);
    for (var i=0;i<this.tracks;i++)
    {
      if (this.tracks[i].id == track_info.id) { this.tracks[i] = track_info; break; }
    }
    $($(this.track_cell_id(track_info.id)).parentNode).replace(this.html_track(track_info));
    this.observe_track(track_info);
    if (this.on_edit) this.on_edit(track_info.id);
  },

  del: function(event,track_id)
  {
    if (confirm("�� �������?"))
    {
      new Ajax.Request(community.url.tracks_ajax,
         {
          parameters: {cmd:"track.delete",id:track_id,check:community.prop.check},
          onSuccess: this.del_success.bind(this,track_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
    }
  },

  del_success: function(track_id,transport)
  {
    var track_info = this.tracks_hash.get(track_id);
    if (!track_info) return;

    var response = transport.responseText;
    if (response != "OK") { alert("������ ��������: "+response); return; }
    if (this.on_before_delete)
    {
      if (!this.on_before_delete(track_id)) return;
    }

    this.stop_track(track_info);
    this.tracks_hash.unset(track_info.id);
    for (var i=0;i<this.tracks.length;i++)
    {
      if (this.tracks[i].id == track_info.id)
      {
        this.tracks.splice(i,1);
        break;
      }
    }
    $($(this.track_cell_id(track_info.id)).parentNode).replace(this.html_track(track_info));

    if ($(this.track_cell_id(track_info.id)))
    {
      $($(this.track_cell_id(track_info.id)).parentNode).remove();
    }
    if (this.edit_window && this.edit_id == track_id) { this.edit_window.clean(); this.edit_window = null; }
    if (this.on_delete) this.on_delete(track_id);
  },

  hide: function(event,track_id)
  {
    var track_info = this.tracks_hash.get(track_id);
    if (!track_info) return;
    if (track_info.hidden)
    {
      new Ajax.Request(community.url.blog_ajax,
        {
          parameters: {cmd:"post.unhide",id:track_id,check:community.prop.check},
          onSuccess: this.unhide_success.bind(this,track_id),
          onFailure: community.ajax_failure.bind(community)
        });
    }
    else
    {
      new Ajax.Request(community.url.blog_ajax,
        {
          parameters: {cmd:"post.hide",id:track_id,check:community.prop.check},
          onSuccess: this.hide_success.bind(this,track_id),
          onFailure: community.ajax_failure.bind(community)
        });
    }
  },

  hide_success: function(track_id,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������ �������: "+transport.responseText); return; }
    var got_track_id = response[1];
    if (track_id != got_track_id) return;
    if (this.on_before_hide)
    {
      if (!this.on_before_hide(track_id)) return;
    }
    var track_info = this.tracks_hash.get(track_id);
    this.stop_track(track_info);
    track_info.hidden = 1;
    $($(this.track_cell_id(track_info.id)).parentNode).replace(this.html_track(track_info));
    this.observe_track(track_info);

    if (this.edit_window && this.edit_id == track_id)
    {
      this.edit_window.clean();
      this.edit_window = null;
    }
    if (this.on_hide) this.on_hide(track_id);
  },

  unhide_success: function(track_id,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������ ��������: "+transport.responseText); return; }
    var got_track_id = response[1];
    if (track_id != got_track_id) return;
    if (this.on_before_unhide)
    {
      if (!this.on_before_unhide(track_id)) return;
    }

    var track_info = this.tracks_hash.get(track_id);
    this.stop_track(track_info);
    track_info.hidden = 0;
    $($(this.track_cell_id(track_info.id)).parentNode).replace(this.html_track(track_info));
    this.observe_track(track_info);

    if (this.edit_window && this.edit_id == track_id)
    {
      this.edit_window.clean();
      this.edit_window = null;
    }
    if (this.on_unhide) this.on_unhide(track_id);
  },

  move_up: function(event,track_id)
  {
    new Ajax.Request(community.url.tracks_ajax,
         {
          parameters: {cmd:"track.move_up",id:track_id,check:community.prop.check},
          onSuccess: this.move_success.bind(this,track_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  },

  move_down: function(event,track_id)
  {
    new Ajax.Request(community.url.tracks_ajax,
         {
          parameters: {cmd:"track.move_down",id:track_id,check:community.prop.check},
          onSuccess: this.move_success.bind(this,track_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  },

  move_success: function(track_id,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK")
    {
      alert("������ �����������: "+transport.responseText);
      return;
    }
    response.splice(0,1);
    var tracks = response.join(" ").evalJSON(true);
    this.swap(tracks);
  },

  swap: function(tracks)
  {
    if (tracks.down == this.edit_id || tracks.up == this.edit_id)
    {
      if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
    }

    var was_up = null;
    var was_down = null;
    var was_up_index = 0;
    var was_down_index = 0;
 
    if (this.tracks_hash.get(tracks.up.id))
    {
      this.stop_track(this.tracks_hash.get(tracks.up.id));
      was_up = $($(this.track_cell_id(tracks.up.id)).parentNode);
      was_up.update('');
      this.tracks_hash.unset(tracks.up.id);
      for (var i=0;i<this.tracks.length;i++)
      {
        if (this.tracks[i].id == tracks.up.id)
        {
          was_up_index = i;
          break;
        }
      }
    }

    if (this.tracks_hash.get(tracks.down.id))
    {
      this.stop_track(this.tracks_hash.get(tracks.down.id));
      was_down = $($(this.track_cell_id(tracks.down.id)).parentNode);
      was_down.update('');
      this.tracks_hash.unset(tracks.down.id);
      for (var i=0;i<this.tracks.length;i++)
      {
        if (this.tracks[i].id == tracks.down.id)
        {
          was_down_index = i;
          break;
        }
      }
    }

    if (was_up)
    {
      this.tracks_hash.set(tracks.down.id,tracks.down);
      this.tracks[was_up_index] = tracks.down;
      was_up.replace(this.html_track(tracks.down));
      this.observe_track(tracks.down);
    }

    if (was_down)
    {
      this.tracks_hash.set(tracks.up.id,tracks.up);
      this.tracks[was_down_index] = tracks.up;
      was_down.replace(this.html_track(tracks.up));
      this.observe_track(tracks.up);
    }
  },

  on_finish_play: function(id)
  {
    for (var i=0;i<this.tracks.length;i++)
    {
      if (this.tracks[i].id == id) { i++; break; }
    }
    if (i<tracks.length) this.tracks[i].player.play();
  },

  on_play: function(id)
  {
    for (var i=0;i<this.tracks.length;i++)
    {
      if (this.tracks[i].id != id && this.tracks[i].player.playing && !this.tracks[i].player.paused)
      {
        this.tracks[i].player.play(); //Actually pauses playing
      }
    }
  },

  show_group_access: function()
  {
  if (this.group_access_window) { this.group_access_window.clean(); this.group_access_window = null; }
  this.group_access_window = new Community.Window({
            title:"��������� ��������� ����",
            left: 10,
            content:this.group_access_window_content(),
            ok_button:true,
            on_ok:this.group_access_ok.bind(this)
                 });

  this.group_access_window.show();
  makeSelectBox("#"+this.group_access_rd_acc_id());
  makeSelectBox("#"+this.group_access_wr_acc_id());
  },

  group_access_window_content: function()
  {
    var value = '<table style="width:550px;"><tbody>';
    value += '<tr><td>����� �������:</td><td><div style="z-index:3000;" class="por"><select class="selectbox" id="' + this.group_access_rd_acc_id() + '" onchange="community.update_wr_acc_select2(\'' + this.group_access_rd_acc_id() + '\',\'' + this.group_access_wr_acc_id() + '\');">';
    value += '<option value="-1">--��� ���������--</option>';
    community.acc_title.each(function(pair)
          {
            value += '<option value="' + pair.key + '">' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>����� ��������������:</td><td><div class="por"><select class="selectbox" id="' + this.group_access_wr_acc_id() + '">';
    value += '<option value="-1">--��� ���������--</option>';
    community.wr_acc_title.each(function(pair)
          {
      value += '<option value="' + pair.key + '">' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>������:</td><td id="' + this.group_access_status_id() + '">�� �����������</td></tr>';
    value += '</tbody></table>';
    return value;
  },

  group_access_ok: function()
  {
  $(this.group_access_status_id()).update("�����������...");
  new Ajax.Request(community.url.blog_ajax,
         {
          parameters: {
             cmd:"post.group_access",type:6,parent:this.album_id,
             rd_acc: $F(this.group_access_rd_acc_id()),
             wr_acc: $F(this.group_access_wr_acc_id()),
             where:community.prop.current_id,
             check:community.prop.check
          },
          onSuccess: this.group_access_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  },

  group_access_success: function(transport)
  {
   var response = transport.responseText.split(" ");
   if (response[0] != "OK")
   {
     $(this.group_access_status_id()).update("������ ���������� ���� �������: "+transport.responseText);
     return;
   }
   document.location.href=community.url.tracks+'?where='+this.album_id;
  }
});

Community.Audio.id = 0;
