var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.InsertPhoto = Class.create({
  initialize: function(options)
  {
    if (!options) options = {};
    this.id = Community.InsertPhoto.id++;
    this.photo_window = null;
    this.photos = null;
    this.albums = new Array();
    this.selected_album = -1;
    this.on_insert = options.on_insert || null;
    this.insert_big = options.insert_big || false;
    this.nolink = options.nolink || false;
    community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
    this.stop();
    this.iframe = null;
    if (this.photos) this.photos.clean();
    this.photos = null;
    if (this.photo_pages) this.photo_pages.clean();
    this.photo_pages = null;
    if (this.photo_window) this.photo_window.clean();
    this.photo_window = null;
  },

  stop: function()
  {
    if (this.iframe) this.iframe.stopObserving("load");
    if (this.photos) this.photos.clear();
    if (this.photo_pages) this.photo_pages.stop();
    if (this.albums) this.albums_stop();
    var addalbum_element = $(this.addalbum_id());
    if (addalbum_element) addalbum_element.stopObserving('click');
    var uploadphoto_element = $(this.uploadphoto_button_id());
    if (uploadphoto_element) uploadphoto_element.stopObserving('click');
    var addurl_element = $(this.addurl_button_id());
    if (addurl_element) addurl_element.stopObserving('click');
    if (this.photo_window) this.photo_window.clear();
  },

  photos_id: function() { return "Community_InsertPhoto_Photos_"+this.id; },
  photos_pagestop_id: function() { return "Community_InsertPhoto_Photos_Pages_Top_"+this.id; },
  photohint_id: function() { return "Community_InsertPhoto_PhotoHint_"+this.id; },
  albums_id: function() { return "Community_InsertPhoto_Albums_"+this.id; },
  albums_li_id: function(index) { return "Community_InsertPhoto_Albums_li_"+index+"_"+this.id; },
  addalbum_id: function() { return "Community_InsertPhoto_AddAlbum_"+this.id; },
  uploadphoto_id: function() { return "Community_InsertPhoto_UploadPhoto_"+this.id; },
  uploadphoto_where_id: function() { return "Community_InsertPhoto_UploadPhoto_Where_"+this.id; },
  uploadphoto_title_id: function() { return "Community_InsertPhoto_UploadPhoto_Title_"+this.id; },
  uploadphoto_button_id: function() { return "Community_InsertPhoto_UploadPhoto_Button_"+this.id; },
  addurl_id: function() { return "Community_InsertPhoto_AddUrl_"+this.id; },
  addurl_button_id: function() { return "Community_InsertPhoto_AddUrl_Button_"+this.id; },
  uploadform_id: function() { return "Community_InsertPhoto_UploadForm_"+this.id; },
  iframe_id: function() { return "Community_InsertPhoto_IFrame_"+this.id; },
  float_select_id: function() { return "Community_InsertPhoto_FloatSelect_"+this.id; },
  variant_select_id: function() { return "Community_InsertPhoto_VariantSelect_"+this.id; },

  albums_stop: function()
  {
    if (!this.albums) return;
    var t_this = this;
    this.albums.each(function(album_info,index)
                    {
                      if ($(t_this.albums_li_id(index)))
                      {
                        $(t_this.albums_li_id(index)).stopObserving("click");
                      }
                    });
  },

  show: function()
  {
    if (this.photo_window) { this.photo_window.show(); return; }
    this.photo_window = new Community.Window({
            title:"������� ����������",
            content:this.photo_window_content(),
            ok_button:true,
            on_ok:this.photo_window_ok.bind(this),
            on_close:this.photo_window_close.bind(this)
            });

    this.iframe = new Element('iframe',
      {
        style:'display:none;',src:'about:blank',
        id:this.iframe_id(),name:this.iframe_id()
      });

    document.body.appendChild(this.iframe);

    this.iframe.observe("load",this.upload_photo_success.bindAsEventListener(this));
    this.photos = new Community.Photos(this.photos_id(),{ selectable: true });

    $(this.addalbum_id()).observe('click',this.add_album.bindAsEventListener(this));
    $(this.uploadphoto_button_id()).observe('click',this.upload_photo.bindAsEventListener(this));

    if ($(this.addurl_button_id())) $(this.addurl_button_id()).observe('click',this.add_url.bind(this));

    $(this.albums_id()).update('���� ��������...');
    this.photo_window.show();
    new Ajax.Request(community.url.albums_ajax,
        {
          parameters: {cmd:"album.get",where:community.prop.user_id},
          onSuccess: this.get_albums_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
        });
  },

  photo_window_content: function()
  {
    var value = '';
    value += '<table><tr>';
    if (this.nolink)
    {
     value += '<td>';
     value += '<form class="clear" id="'+this.uploadform_id()+'" method="post" enctype="multipart/form-data" target="'+this.iframe_id()+'" action="'+community.url.photo_add_ajax+'">';
     value += '<input type="hidden" name="cmd" value="photo.add" />';
     value += '<input type="hidden" name="check" value="'+community.prop.check+'" /><p class="mt5">��������� ��������� ����</p>';
    }
    else
    {
     value += '<td><p>�� ������ �������� ������ �� ����������� � ����</p>';
     value += '<div class="oh mt10"><table><tr><td class="sel" style="font-weight:bold;line-height:23px;">http://&nbsp;</td><td><input style="width:210px;" class="inp_text mr5" id="'+this.addurl_id()+'" type="input"  /></td></tr></table>';
     value += '<input class="button clear" id="'+this.addurl_button_id()+'" type="button" value="��������" /></div>';
     value += '<form class="clear" id="'+this.uploadform_id()+'" method="post" enctype="multipart/form-data" target="'+this.iframe_id()+'" action="'+community.url.photo_add_ajax+'">';
     value += '<input type="hidden" name="cmd" value="photo.add" />';
     value += '<input type="hidden" name="check" value="'+community.prop.check+'" /><p class="mt5">��� ��������� ��������� ����</p>';
    }
    value += '<input id="'+this.uploadphoto_id()+'" type="file" name="content" style="width:230px;" />';
    value += '<input id="'+this.uploadphoto_title_id()+'" type="hidden" name="title" value="" />';
    value += '<input type="hidden" name="body" value="" />';
    value += '<input id="'+this.uploadphoto_where_id()+'" type="hidden" name="where" value="0" />';
    value += '<input type="hidden" name="rd_acc" value="all" />';
    value += '<input type="hidden" name="wr_acc" value="self" />';
    value += '&nbsp;<input type="button" class="button" id="'+this.uploadphoto_button_id()+'" value="���������" />';
    value += '</form>';
    value += '<p class="clear pt5">� ���� �� ����� �������� ��� ������� �����</p>';
    value += '<div class="oh mb10"><input class="button" type="button" id="'+this.addalbum_id()+'" value="�������� �����" /></div>';
    value += '<div class="scroll clear" style="height:200px;"><div class="scroll_content" style="padding:10px;"><ul style="width:100%;" id="'+this.albums_id()+'"></ul></div></div>';

	value += '<div class="mt10">���������: <select id="'+this.float_select_id()+'"><option value="none">--���--</option><option value="left">�����</option><option value="right">������</option></select></div>';
    value += '<div class="clear"></div>';

	if (!this.insert_big) {
		value += '<div class="mt10">�������: <select id="'+this.variant_select_id()+'"><option value="thmb">���������</option><option value="std">�����</option></select></div>';
		value += '<div class="clear"></div>';
	}

    value += '</td>';
    value += '<td>'
    value += '<div class="clear" id="'+this.photohint_id()+'"></div>';
    value += '<div class="clear" id="'+this.photos_pagestop_id()+'"></div>';
    value += '<div class="clear EditPhotosArea ml10" id="'+this.photos_id()+'"></div>';
    value += '</td>';
    value += '</tr></table>';

    return value;
  },

  photo_window_close: function()
  {
    this.photos.select(null,-1);
  },

  photo_window_ok: function()
  {
    if (this.photos.selected_photo == -1) { alert("���������� �� �������!"); return; }
    if (this.on_insert)
    {
      if (this.nolink)
      {
        this.on_insert({id:this.photos.selected_photo});
      }
      else
      {
        this.on_insert({src:((this.insert_big || $F(this.variant_select_id()) == "std") ? this.photos.photo_src_big(this.photos.selected_photo)
                                          : this.photos.photo_src(this.photos.selected_photo)),
                      title: this.photos.photos.get(this.photos.selected_photo).title,
                      link: community.url.photo_comments+'?where='+this.photos.selected_photo,
                      float: $F(this.float_select_id())
                    });
      }
    }
    this.photos.select(null,-1);
    this.photo_window.hide();
  },

  get_albums_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������ ��������� ��������: "+transport.responseText); return; }
    response.splice(0,1);

    var result = response.join(" ").evalJSON(true);
    var value = '';
    this.albums_stop();
    this.albums = $A(result.albums);
    var t_this = this;
    this.albums.each(function(album_info,index)
      {
        value=value+'<li id="'+t_this.albums_li_id(index)+'">'+album_info.title+'</li>';
      });
    $(this.albums_id()).update(value);
    this.albums.each(function(album_info,index)
      {
        $(t_this.albums_li_id(index)).observe("click",t_this.album_changed.bindAsEventListener(t_this,index));
      });
    this.photos.clear();
    if (this.albums.length > 0) this.album_changed(null,0);
    else this.selected_album = -1;
  },

  album_changed: function(event,album_index)
  {
    this.photos.clear();
    $(this.photohint_id()).update("���������� �����������...");
    if (this.selected_album > -1) $(this.albums_li_id(this.selected_album)).style.border="";
    this.selected_album = album_index;
    $(this.albums_li_id(album_index)).style.border="1px solid black";
    new Ajax.Request(community.url.albums_ajax,
      {
        parameters:{cmd:"photo.get",id:this.albums[album_index].id},
        onSuccess: this.get_photos_success.bind(this,album_index),
        onFailure: community.ajax_failure.bind(community)
      });
  },

  get_photos_success: function(album_index,transport)
  {
    if (album_index != this.selected_album) return;
    $(this.photohint_id()).update("");
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������ ��������� ����������: "+transport.responseText); return; }
    response.splice(0,1);
    this.photos.clear();
    var result = response.join(" ").evalJSON(true);
  //  console.log( JSON.stringify( result ) );
    $A(result.photos).each(this.photos.add.bind(this.photos));
    if (this.photo_pages) this.photo_pages.clean();
    this.photo_pages = new Community.Pages([$(this.photos_pagestop_id())],result.page,result.pages,
                 "","",{callback:this.photos_page_changed.bind(this)}
                );
  },

  photos_page_changed: function(page)
  {
    this.photos.clear();
    if (this.selected_album < 0) return;
    $(this.photohint_id()).update("���������� �����������...");
    new Ajax.Request(community.url.albums_ajax,
      {
        parameters:{cmd:"photo.get",page:page,id:this.albums[this.selected_album].id},
        onSuccess: this.get_photos_success.bind(this,this.selected_album),
        onFailure: community.ajax_failure.bind(community)
      });
  },

  add_album: function(event)
  {
    $(this.photohint_id()).update("����������� ������...");
    new Ajax.Request(community.url.albums_ajax,
    {
      parameters: {
                    cmd: "album.add", where: community.prop.user_id,
                    title: "����� ������", body: "", rd_acc: "all",
                    wr_acc: "self", tag: "",check:community.prop.check
                  },
      onSuccess: this.add_album_success.bind(this),
      onFailure: community.ajax_failure.bind(community)
  });
  },

  add_album_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    $(this.photohint_id()).update("");
    if (response[0] != "OK") { alert("������ ���������� �������: "+transport.responseText); return; }
    new Ajax.Request(community.url.albums_ajax,
      {
        parameters: {cmd:"album.get",where:community.prop.user_id},
        onSuccess: this.get_albums_success.bind(this),
        onFailure: community.ajax_failure.bind(community)
      });
  },

  upload_photo: function(event)
  {
    if (!$F(this.uploadphoto_id()))
    {
      alert("�� ������� ��� �����!");
      return;
    }
    if (this.selected_album < 0)
    {
      alert("�� ������ ������!");
      return;
    }
    $(this.photohint_id()).update("���������� �����������...");
    $(this.uploadphoto_where_id()).value=this.albums[this.selected_album].id;
    $(this.uploadphoto_title_id()).value=($F(this.uploadphoto_id()).split('\\')).last();
    $(this.uploadform_id()).submit();
  },

  upload_photo_success: function(event)
  {
    if (this.selected_album < 0) return;
    if ($F(this.uploadphoto_where_id()) != this.albums[this.selected_album].id) return;
    var i = $(this.iframe_id());
    var d;
    if (i.contentDocument) d = i.contentDocument;
    else if (i.contentWindow) d = i.contentWindow.document;
    else throw new Error("���������������� ��� iframe");
    if (d.location.href == "about:blank") return;
    $(this.photohint_id()).update("");
    var response = d.body.innerHTML;
    var params = response.split(' ');
    if (params[0] != "OK") { alert("������ ��������: "+response); return; }
    var photo_id = params[1];
    params.splice(0,2);
    var new_photo = params.join(" ").evalJSON(true);
    this.photos.add({
                      id:photo_id,
                      title:new_photo.title,
                      body:"",rd_acc:"all",wr_acc:"self",can_edit:"0",can_delete:"0"
                   });
    this.photos.select(event,photo_id);
  },

  add_url: function(event)
  {
    if (!$F(this.addurl_id()))
    {
      alert("�� ������� ��� �����!");
      return;
    }
    if (this.on_insert)
    {
      this.on_insert({ src:'http://'+$F(this.addurl_id()) });
    }
    this.photos.select(null,-1);
    this.photo_window.hide();
  }

});

Community.InsertPhoto.id = 0;