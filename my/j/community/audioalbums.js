var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.AudioAlbums = Class.create({
  initialize: function(container,options)
  {
  if (!options) options = {};
  this.id = Community.AudioAlbums.id++;
  this.disable_tags = options.disable_tags ? true :false;
  this.disable_favorite = options.disable_favorite ? true :false;
  this.disable_sign = options.disable_sign ? true :false;
  this.can_edit = options.can_edit || false;
  this.can_delete = options.can_delete || false;
  this.can_hide = options.can_hide || false;
  this.can_fix = options.can_fix || false;
  this.on_edit = options.on_edit || null;
  this.on_delete = options.on_delete || null;
  this.on_before_edit = options.on_before_edit || null;
  this.on_before_delete = options.on_before_delete || null;
  this.on_hide = options.on_hide || null;
  this.on_before_hide = options.on_before_hide || null;
  this.on_unhide = options.on_unhide || null;
  this.on_before_unhide = options.on_before_unhide || null;
  this.on_fix = options.on_fix || null;
  this.on_before_fix = options.on_before_fix || null;
  this.on_unfix = options.on_unfix || null;
  this.on_before_unfix = options.on_before_unfix || null;
  this.edit_window = null;
  this.edit_editor = null;
  this.edit_id = 0;
  this.container = $(container);
  if (!this.container) throw new Error("����������� ��������� ��� �������������!");
  this.albums = new Hash();
  this.group_access = options.group_access || null;
  if (this.group_access && $(this.group_access)) {
    $(this.group_access).observe("click",this.show_group_access.bind(this));
  }
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.container = null;
  if (this.edit_window) this.edit_window.clean();
  this.edit_window = null;
  if (this.group_access_window) this.group_access_window.clean();
  this.group_access_window = null;
  },

  stop: function()
  {
  if (this.edit_window) this.edit_window.clear();
  if (this.group_access_window) this.group_access_window.clear();
  var t_this = this;
  this.albums.each(function(pair)
  {
    t_this.stop_element(pair.value);
  });
  if (this.group_access && $(this.group_access)) $(this.group_access).stopObserving("click");
  },


  stop_element: function(album_info)
  {
  var delete_element = $(this.album_delete_id(album_info.id));
  if (delete_element) delete_element.stopObserving("click");
  var edit_element = $(this.album_edit_id(album_info.id));
  if (edit_element) edit_element.stopObserving("click");
  var hide_element = $(this.album_hide_id(album_info.id));
  if (hide_element) hide_element.stopObserving("click");
  var fix_element = $(this.album_fix_id(album_info.id));
  if (fix_element) fix_element.stopObserving("click");
  if (album_info.tags) album_info.tags.clean();
  if (album_info.favorite) album_info.favorite.clean();
  if (album_info.sign) album_info.sign.clean();
  },

  album_cell_id: function(album_id) { return "Community_AudioAlbum_Cell_"+this.id+"_"+album_id; },
  album_hide_id: function(album_id) { return "Community_AudioAlbum_Hide_"+this.id+"_"+album_id; },
  album_fix_id: function(album_id) { return "Community_AudioAlbum_Fix_"+this.id+"_"+album_id; },
  album_edit_id: function(album_id) { return "Community_AudioAlbum_Edit_"+this.id+"_"+album_id; },
  album_delete_id: function(album_id) { return "Community_AudioAlbum_Delete_"+this.id+"_"+album_id; },
  album_tags_id: function(album_id) { return "Community_AudioAlbum_Tags_"+this.id+"_"+album_id; },
  album_favorite_id: function(album_id) { return "Community_AudioAlbum_Favorite_"+this.id+"_"+album_id; },
  album_sign_id: function(album_id) { return "Community_AudioAlbum_Sign_"+this.id+"_"+album_id; },
  edit_title_id: function() { return "Community_AudioAlbum_EditTitle_"+this.id; },
  edit_body_id: function() { return "Community_AudioAlbum_EditBody_"+this.id; },
  edit_rd_acc_id: function() { return "Community_AudioAlbum_EditRdAcc_"+this.id; },
  edit_wr_acc_id: function() { return "Community_AudioAlbum_EditWrAcc_"+this.id; },
  edit_status_id: function() { return "Community_AudioAlbum_EditStatus_"+this.id; },
  group_access_rd_acc_id: function() { return "Community_Album_GroupAccessRdAcc_"+this.id; },
  group_access_wr_acc_id: function() { return "Community_Album_GroupAccessWrAcc_"+this.id; },
  group_access_status_id: function() { return "Community_Album_GroupAccessStatus_"+this.id; },

  html: function(album_info)
  {
  var value = '<div class="AlbumCell clear pb30 oh" id="'+this.album_cell_id(album_info.id)+'">';
  value += '<div class="smallTitle">' + community.user_title(album_info.author_title,{avatar:1,avsize:1}) + '<td>: ' + album_info.time_create + '</td></tr></table></div>';
  value += '<div class="info_wrap"><div class="infoTitle"><h3><a href="' + community.url.tracks + '?where=' + album_info.id + '">';
  if (album_info.title) value += album_info.title;
  else value += '<em>(��� ��������)</em>';
  value += '</a></h3>';
  value += '<p style="font-size:11px;" class="light"><a class="n_u mr10" href="'+community.url.tracks+'?where='+album_info.id+'">������������: ' + album_info.cnt_child + '</a>����������: '+album_info.totalviews + '</p>';
  value += '</div>';
  value += '<div class="mt10 mb20 oh">' + album_info.body + '</div>';
  if (album_info.hidden) value += '<p style="color:red;" class="pb10">���������� �� ������� ������ �����������</p>';
  value += '<ul class="tiny">';
  if (!this.disable_favorite) value += '<li><div id="' + this.album_favorite_id(album_info.id) + '"></div></li>';
  if (!this.disable_sign) value += '<li><div id="' + this.album_sign_id(album_info.id) + '"></div></li>';

  if (this.can_edit && (album_info.can_edit - 0))
  {
    value += '<li><a href="#" id="' + this.album_edit_id(album_info.id) + '" onclick="return false;">������������� ������</a></li>';
  }
  if (this.can_delete && (album_info.can_delete - 0))
  {
    value += '<li><a href="#" id="' + this.album_delete_id(album_info.id) + '" onclick="return false;">������� ������</a></li>';
  }
  if (this.can_fix && (album_info.can_edit - 0))
  {
    value += '<li><a href="#" onclick="return false;" id="' + this.album_fix_id(album_info.id) + '">';
    if (Number(album_info['fixed'])) value += '��������� ������';
    else value += '���������� ������';
    value += '</a></li>';
  }
  if (this.can_hide && (album_info.can_hide - 0))
  {
    value += '<li><a href="#" onclick="return false;" id="' + this.album_hide_id(album_info.id) + '">';
    if (album_info.hidden) value += '������� ������';
    else value += '������ ������';
    value += '</a></li>';
  }
  value += '</ul><div class="clear"><!-- --></div>';
  if (!this.disable_tags) value += '<div class="inner_tags AlbumTagsArea" id="' + this.album_tags_id(album_info.id) + '"></div>';
  value += '</div></div>';
  return value;
  },

  add: function(album_info)
  {
  var is_replace = false;
  if (this.albums.get(album_info.id))
  {
    is_replace = true;
    this.stop_element(this.albums.get(album_info.id));
  }

  this.albums.set(album_info.id,album_info);
  if (is_replace) $(this.album_cell_id(album_info.id)).replace(this.html(album_info));
  else new Insertion.Bottom(this.container,this.html(album_info));

  if (this.can_delete && (album_info.can_delete - 0))
  {
    $(this.album_delete_id(album_info.id)).observe("click",this.del.bindAsEventListener(this,album_info.id));
  }
  if (this.can_edit && (album_info.can_edit - 0))
  {
    $(this.album_edit_id(album_info.id)).observe("click",this.edit.bindAsEventListener(this,album_info.id));
  }
  if (this.can_hide && (album_info.can_hide - 0))
  {
    $(this.album_hide_id(album_info.id)).observe("click",this.hide.bindAsEventListener(this,album_info.id));
  }
  if (this.can_fix && (album_info.can_edit - 0))
  {
    $(this.album_fix_id(album_info.id)).observe("click",this.fix.bindAsEventListener(this,album_info.id));
  }
  if (!this.disable_tags)
  {
    this.albums.get(album_info.id).tags = new Community.Tags(this.album_tags_id(album_info.id),album_info.id,album_info.type,album_info.author_id,album_info.can_edit_tag);
  }
  if (!this.disable_favorite)
  {
    this.albums.get(album_info.id).favorite = new Community.Favorite(this.album_favorite_id(album_info.id),album_info.id,album_info.type);
  }
  if (!this.disable_sign)
  {
    this.albums.get(album_info.id).sign = new Community.Sign(this.album_sign_id(album_info.id),album_info.id,album_info.type);
  }
  },

  remove: function(album_id)
  {
  if (!this.albums.get(album_id)) return;
  this.albums.unset(album_id);
  $(this.album_cell_id(album_id)).remove();
  },

  edit: function(event,album_id)
  {
  if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
  var album_info = this.albums.get(album_id);
  if (!album_info) return;
  this.edit_window = new Community.Window({
            title:"�������������� ������������ "+(album_info.title ? album_info.title : "(��� ��������)"),
            content:this.edit_window_content(album_info),
            ok_button:true,
            on_ok:this.edit_ok.bind(this)
                 });
  this.edit_window.show();
  if (!album_info.hidden)
  {
    makeSelectBox("#"+this.edit_rd_acc_id());
    makeSelectBox("#"+this.edit_wr_acc_id());
  }
  this.edit_id = album_id;
  },

  edit_window_content: function(album_info)
  {
  var value = '<table><tbody>';
  value += '<tr><td style="width:150px;">���������</td><td>';
  value += '<input id="' + this.edit_title_id() + '" type="text" style="width:500px;" class="inp_text" value="' + album_info.title + '"/></td></tr>';
  value += '<tr><td>��������</td><td>';
  value += '<textarea style="width:500px;height:180px;" class="inp_text" id="' + this.edit_body_id() + '" rows="5">' + album_info.body.gsub("<br />","&#10;") + '</textarea></td></tr>';
  if (!album_info.hidden)
  {
    value += '<tr><td>����� ��������</td><td><div class="por" style="z-index:3000;"><select id="' + this.edit_rd_acc_id() + '" onchange="community.update_wr_acc_select(\'' + this.edit_rd_acc_id() + '\',\'' + this.edit_wr_acc_id() + '\');">';
    community.acc_title.each(function(pair)
          {
      value += '<option value="' + pair.key + '"';
      if (album_info.rd_acc == pair.key) value += ' selected="selected"';
      value += '>' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>����� ��������� �����������</td><td><div class="por"><select id="' + this.edit_wr_acc_id() + '">';
    community.wr_acc_title.each(function(pair)
          {
      if (community.acc_order.get(pair.key) < community.acc_order.get(album_info.rd_acc)) return;
      value += '<option value="' + pair.key + '"';
      if (album_info.wr_acc == pair.key) value += ' selected="selected"';
      value += '>' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
  }
  value += '<tr><td>������</td><td id="' + this.edit_status_id() + '">�� �����������</td></tr>';
  value += '</tbody></table>';
  return value;
  },

  edit_ok: function()
  {
  $(this.edit_status_id()).update("�����������...");
  var params = {cmd:"album.update",id:this.edit_id,title:$F(this.edit_title_id()),body:$F(this.edit_body_id()),check:community.prop.check};
  if ($(this.edit_rd_acc_id())) params.rd_acc = $F(this.edit_rd_acc_id());
  if ($(this.edit_wr_acc_id())) params.wr_acc = $F(this.edit_wr_acc_id());
  new Ajax.Request(community.url.audioalbums_ajax,
         {
          parameters: params,
          onSuccess: this.edit_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  },

  edit_success: function(transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    $(this.edit_status_id()).update("������ ����������: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  var album_info = response.join(" ").evalJSON(true);
  if (this.on_before_edit)
  {
    if (!this.on_before_edit(album_info.id)) return;
  }

  if (album_info.id == this.edit_id)
  {
    if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
  }
  this.add(album_info);
  if (this.on_edit) this.on_edit(album_info.id);
  },

  del: function(event,album_id)
  {
  if (confirm("�� �������?"))
  {
    new Ajax.Request(community.url.audioalbums_ajax,
         {
          parameters: {cmd:"album.delete",id:album_id,check:community.prop.check},
          onSuccess: this.del_success.bind(this,album_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  }
  },

  del_success: function(album_id,transport)
  {
  var response = transport.responseText;
  if (response != "OK") { alert("������ ��������: "+response); return; }
  if (this.on_before_delete)
  {
    if (!this.on_before_delete(album_id)) return;
  }
  this.remove(album_id);
  if (this.edit_window && this.edit_id == album_id) { this.edit_window.clean(); this.edit_window = null; }
  if (this.on_delete) this.on_delete(album_id);
  },

  hide: function(event,album_id)
  {
  var album_info = this.albums.get(album_id);
  if (!album_info) return;
  if (album_info.hidden)
  {
    new Ajax.Request(community.url.blog_ajax,
      {
        parameters: {cmd:"post.unhide",id:album_id,check:community.prop.check},
        onSuccess: this.unhide_success.bind(this,album_id),
        onFailure: community.ajax_failure.bind(community)
      });
  }
  else
  {
    new Ajax.Request(community.url.blog_ajax,
      {
        parameters: {cmd:"post.hide",id:album_id,check:community.prop.check},
        onSuccess: this.hide_success.bind(this,album_id),
        onFailure: community.ajax_failure.bind(community)
      });
  }
  },

  hide_success: function(album_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������ �������: "+transport.responseText); return; }
  var got_album_id = response[1];
  if (album_id != got_album_id) return;
  if (this.on_before_hide)
  {
    if (!this.on_before_hide(album_id)) return;
  }
  var album_info = this.albums.get(album_id);
  album_info.hidden = 1;
  this.add(album_info);
  if (this.edit_window && this.edit_id == album_id)
  {
    this.edit_window.clean();
    this.edit_window = null;
  }
  if (this.on_hide) this.on_hide(album_id);
  },

  unhide_success: function(album_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������ ��������: "+transport.responseText); return; }
  var got_album_id = response[1];
  if (album_id != got_album_id) return;
  if (this.on_before_unhide)
  {
    if (!this.on_before_unhide(album_id)) return;
  }
  var album_info = this.albums.get(album_id);
  album_info.hidden = 0;
  this.add(album_info);
  if (this.edit_window && this.edit_id == album_id)
  {
    this.edit_window.clean();
    this.edit_window = null;
  }
  if (this.on_unhide) this.on_unhide(album_id);
  },

  fix: function(event,album_id)
  {
    var album_info = this.albums.get(album_id);
    if (!album_info) return;
    if (Number(album_info['fixed']))
    {
      new Ajax.Request(community.url.blog_ajax,
        {
          parameters: {cmd:"post.unfix",id:album_id,check:community.prop.check},
          onSuccess: this.unfix_success.bind(this,album_id),
          onFailure: community.ajax_failure.bind(community)
        });
    }
    else
    {
      new Ajax.Request(community.url.blog_ajax,
      {
        parameters: {cmd:"post.fix",id:album_id,check:community.prop.check},
        onSuccess: this.fix_success.bind(this,album_id),
        onFailure: community.ajax_failure.bind(community)
      });
    }
  },

  fix_success: function(album_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������: "+transport.responseText); return; }
  var got_album_id = response[1];
  if (album_id != got_album_id) return;
  if (this.on_before_fix)
  {
    if (!this.on_before_fix(album_id)) return;
  }
  var album_info = this.albums.get(album_id);
  album_info['fixed'] = 1;
  this.add(album_info);
  if (this.edit_window && this.edit_id == album_id)
  {
    this.edit_window.clean();
    this.edit_window = null;
  }
  if (this.on_fix) this.on_fix(album_id);
  },

  unfix_success: function(album_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������: "+transport.responseText); return; }
  var got_album_id = response[1];
  if (album_id != got_album_id) return;
  if (this.on_before_unfix)
  {
    if (!this.on_before_unfix(album_id)) return;
  }
  var album_info = this.albums.get(album_id);
  album_info['fixed'] = 0;
  this.add(album_info);
  if (this.edit_window && this.edit_id == album_id)
  {
    this.edit_window.clean();
    this.edit_window = null;
  }
  if (this.on_unfix) this.on_unfix(album_id);
  },

  show_group_access: function()
  {
  if (this.group_access_window) { this.group_access_window.clean(); this.group_access_window = null; }
  this.group_access_window = new Community.Window({
            title:"��������� ��������� ����",
            left: 10,
            content:this.group_access_window_content(),
            ok_button:true,
            on_ok:this.group_access_ok.bind(this)
                 });

  this.group_access_window.show();
  makeSelectBox("#"+this.group_access_rd_acc_id());
  makeSelectBox("#"+this.group_access_wr_acc_id());
  },

  group_access_window_content: function()
  {
    var value = '<table style="width:550px;"><tbody>';
    value += '<tr><td>����� ��������:</td><td><div style="z-index:3000;" class="por"><select class="selectbox" id="' + this.group_access_rd_acc_id() + '" onchange="community.update_wr_acc_select2(\'' + this.group_access_rd_acc_id() + '\',\'' + this.group_access_wr_acc_id() + '\');">';
    value += '<option value="-1">--��� ���������--</option>';
    community.acc_title.each(function(pair)
          {
            value += '<option value="' + pair.key + '">' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>����� ��������� �����������:</td><td><div class="por"><select class="selectbox" id="' + this.group_access_wr_acc_id() + '">';
    value += '<option value="-1">--��� ���������--</option>';
    community.wr_acc_title.each(function(pair)
          {
      value += '<option value="' + pair.key + '">' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>������:</td><td id="' + this.group_access_status_id() + '">�� �����������</td></tr>';
    value += '</tbody></table>';
    return value;
  },

  group_access_ok: function()
  {
  $(this.group_access_status_id()).update("�����������...");

  new Ajax.Request(community.url.blog_ajax,
         {
          parameters: {
             cmd:"post.group_access",type:5,
             rd_acc: $F(this.group_access_rd_acc_id()),
             wr_acc: $F(this.group_access_wr_acc_id()),
             where:community.prop.current_id,
             check:community.prop.check
          },
          onSuccess: this.group_access_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  },

  group_access_success: function(transport)
  {
   var response = transport.responseText.split(" ");
   if (response[0] != "OK")
   {
     $(this.group_access_status_id()).update("������ ���������� ���� �������: "+transport.responseText);
     return;
   }
   document.location.href=community.url.audioalbums+'?where='+community.prop.current_id;

  }

});

Community.AudioAlbums.id = 0;
