
function makeSelectBox(obj) {
  try {
	jQuery(obj).selectbox();
  }
  catch(err) {
  }
}

jQuery(document).ready(function(){

  function hideLabel(obj) { jQuery(obj).css({"left" : "-1000px"}); }
  function showLabel(obj) { jQuery(obj).css({"left" : "0px"}); }
  
  jQuery(".inside_input").each(function(){
      if(jQuery(this).val() == "") showLabel(jQuery(this).parent().find(".inside"));
    });
  
  jQuery(".inside").click(function(){
      hideLabel(this);
    });
  jQuery(".inside_input").focus(function(){
      hideLabel(jQuery(this).parent().find(".inside"));
    });
  jQuery(".inside_input").blur(function(){
      if(jQuery(this).val() == "") showLabel(jQuery(this).parent().find(".inside"));
    });
  
  jQuery(".selectbox").each(function(){ makeSelectBox(jQuery(this)); });
  
  
  jQuery.datepicker.setDefaults( { changeMonth: true,
                              changeYear: true,
                              showOn: "both",
                              monthNamesShort: ['���','���','����','���','���','����','����','���','���','���','���','���'],
                              dayNamesMin: ['��','��','��','��','��','��','��'],
                              yearRange: '1900:2036',
                              altField: '#birthday',
                              dateFormat: 'dd-mm-yy',
                              buttonImageOnly: true,
                              buttonImage: '/i/comm/skin' + community.prop.skin + '/date_choose.gif',
                              closeText: 'X',
                              showButtonPanel: true } );
  
  jQuery("#form_make .button").click(function(){jQuery("#form_make").submit(); return false;});
  jQuery("#form_become .button").click(function(){jQuery("#form_become").submit(); return false;});

});