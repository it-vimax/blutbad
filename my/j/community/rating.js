var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Rating = Class.create({
  initialize: function(element,obj_id,type)
  {
	this.id=Community.Rating.id++;
	this.obj_id = obj_id;
	this.type = type || 1;
	this.element = $(element);
	if (!this.element) throw new Error("�� �������� ����������� ����� ��� ��������!");
	this.element.update("���� ��������...").show();
    this.init();
	community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
	this.stop();
	this.element = null;
  },

  stop: function()
  {
    var rating_inc_element = $(this.rating_inc_id());
    if (rating_inc_element) rating_inc_element.stopObserving("click");
	if (this.rating_window_bind) { this.rating_window_bind.clean(); this.rating_window_bind = null; }
    if (this.rating_window) { this.rating_window.clear(); this.rating_window = null; }
  },

  rating_value_id: function() { return "Community_Rating_Value_"+this.id; },
  rating_docket_id: function() { return "Community_Rating_Docket_"+this.id; },

  rating_inc_id: function() { return "Community_Rating_Inc_"+this.id; },

  init: function()
  {
	new Ajax.Request(community.url.rating,
			 {
			  parameters: {cmd:"rating.can",id:this.obj_id,check:community.prop.check},
			  onSuccess: this.init_success.bind(this),
			  onFailure: community.ajax_failure.bind(community)
			 }
			);
  },

  init_success: function(transport)
  {
	var response = transport.responseText.split(" ");
	if (response[0] != "OK") { this.element.update(transport.responseText); return; }
    this.rating_votes = response[1];
	this.display();
  },

  display: function()
  {
	this.stop();
	this.element.update(this.html());
	$(this.rating_inc_id()).observe("click",this.inc.bindAsEventListener(this));
  },

  html: function()
  {
    var type_str = '������';
    if (this.type == 3) type_str = '����������';
    if (this.type == 6) type_str = '�����������';
	var value = '������� '+type_str+': <form>';
    value += '<input class="" type="radio" id="'+this.rating_value_id()+'" name="'+this.rating_value_id()+'" value="-3">-3</input>';
    value += '<input class="ml10" type="radio" name="'+this.rating_value_id()+'" value="-2">-2</input>';
    value += '<input class="ml10" type="radio" name="'+this.rating_value_id()+'" value="-1">-1</input>';
    value += '<input class="ml10" type="radio" name="'+this.rating_value_id()+'" value="1">+1</input>';
    value += '<input class="ml10" type="radio" name="'+this.rating_value_id()+'" value="2">+2</input>';
    value += '<input class="ml10" type="radio" name="'+this.rating_value_id()+'" value="3">+3</input>';
    value += '<div class="clear"></div><input type="button" class="button" id="'+this.rating_inc_id()+'" value="�������" /></form>';
	if (this.rating_votes != -1)
    {
	  value += '<div class="clear">� ��� ��� �������� <strong>'+this.rating_votes+'</strong> �����'+community.word_end(this.rating_votes)+'</div>';
    }
    else value += '<div class="clear"></div>';
	return value;
  },

  inc: function(event)
  {
    var mark = community.$RF(this.rating_value_id());
    if (!mark) { alert("������ �� �������!"); return; }
	if (this.rating_window_bind) { this.rating_window_bind.clean(); this.rating_window_bind = null; }
	if (this.rating_window) { this.rating_window.clear(); this.rating_window = null; }
	var mark_visual = mark;
	if (mark_visual > 0) mark_visual = "+"+mark_visual;
	this.rating_window = new Community.Window({
			title: "��������� ������ "+mark_visual+"?",
			content: '������� ������� ����� ������ (�������������):<br /><input type="text" class="inp_text" size="50" id="'+this.rating_docket_id()+'"></input>',
			ok_button: true,
			on_ok: this.accept.bind(this,mark)
			});
	this.rating_window.show();
	$(this.rating_docket_id()).focus();
	var ok_button = this.rating_window.ok_button_element();
    this.rating_window_bind = new Community.BindInputWithButton(this.rating_docket_id(),ok_button);
  },

  accept: function(mark)
  {
	this.rating_window.hide();
	new Ajax.Request(community.url.rating,
			 {
			  parameters: {cmd:"rating.inc",id:this.obj_id,mark:mark,docket:$F(this.rating_docket_id()),check:community.prop.check},
			  onSuccess: this.success.bind(this),
			  onFailure: this.failure.bind(this)
			 }
			);
	this.stop();
	this.element.update("��������� ������������...");
  },

  success: function(transport)
  {
	if (transport.responseText != "OK") this.element.update("������: "+transport.responseText);
	else this.element.update("��� ����� �����");
  },

  failure: function(transport)
  {
	this.element.update("����������� ������ �������");
  }
});

Community.Rating.id = 0;
