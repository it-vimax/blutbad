var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Censure = Class.create({
  initialize: function(element,obj_id,enabled)
  {
    this.id = Community.Censure.id++;
    this.obj_id = obj_id;
    this.element = $(element);
    if (!this.element) throw new Error("�� �������� ����������� ����� ��� �������!");
    if (community.prop.user_id == 0) return;
    if (enabled == undefined)
    {
      this.element.update("���� ��������...").show();
      new Ajax.Request(community.url.cens_ajax,
         {
          parameters: {cmd:"cens.get",id:this.obj_id},
          onSuccess: this.init_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
    }
    else
    {
      this.enabled = Number(enabled) ? true : false;
      this.display();
    }
    community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
    this.stop();
    this.element = null;
  },

  stop: function()
  {
    var cens_element = $(this.cens_mod_id());
    if (cens_element) cens_element.stopObserving("click");
  },

  cens_mod_id: function() { return "Community_Censure_Mod_"+this.id; },

  init_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") throw new Error("������ ��������� �������: "+transport.responseText);
    response.splice(0,1);
	if (response[0] == "cant") { this.element.hide(); return; }
    this.enabled = (response[0] == "yes" ? true : false);
    this.display();
  },


  toggle: function(id,type)
  {
    this.element.toggle();
  },

  display: function()
  {
    this.stop();
    this.element.update(this.html()).show();
    $(this.cens_mod_id()).observe("click",this.mod_cens.bindAsEventListener(this));
  },

  html: function()
  {
    var value='';
    if (this.enabled) value += '<a href="#" id="'+this.cens_mod_id()+'" onclick="return false;">�������� �������</a>';
    else value += '<a href="#" id="'+this.cens_mod_id()+'" onclick="return false;">����������� ����������!</a>';
    return value;
  },

  mod_cens: function(event)
  {
    if (this.enabled)
    {
      new Ajax.Request(community.url.cens_ajax,
         {
          parameters: {cmd:"cens.delete",id:this.obj_id,check:community.prop.check},
          onSuccess: this.del_cens_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
        );
    }
    else
    {
      new Ajax.Request(community.url.cens_ajax,
         {
          parameters: {cmd:"cens.add",id:this.obj_id,check:community.prop.check},
          onSuccess: this.add_cens_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
        );
    }
  },

  add_cens_success: function(transport)
  {
    var response = transport.responseText;
    if (response != "OK") { alert("������ ��������������: "+transport.responseText); return; }
    this.enabled = true;
    this.stop();
    this.display();
  },

  del_cens_success: function(transport)
  {
    var response = transport.responseText;
    if (response != "OK") { alert("������ �������� �������: "+transport.responseText); return; }
    this.enabled = false;
    this.stop();
    this.display();
  }

});

Community.Censure.id = 0;
