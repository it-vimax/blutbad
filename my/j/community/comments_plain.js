var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Comments.Plain = Class.create({
  initialize: function(element,root_id,comments,pages,can_post,options)
  {
    options = options || {};
    this.id = Community.Comments.Plain.id++;
    this.element = $(element);
    this.root_id = root_id || 0;
    this.owner_id = options.owner_id || 0;
    this.type = options.type || 1;
    this.lastviewtime = options.lastviewtime || 0;
    this.can_post = Number(can_post) || false;
    if (!this.element) throw new Error("����������� ��������� ��� ������������!");
    this.element.update(this.root_html());
    this.page = 0;
    if (comments)
    {
      this.comments = $A(comments);
      this.pages = pages;
      this.comments_hash = this.comments.inject(new Hash(),function(acc,value) { acc.set(value.id,value); return acc; });
      this.display_comments();
      if (this.can_post && this.root_id) this.display_new_comment(this.root_id);
    }
    else this.init();
    community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.element = null;
  },

  stop: function()
  {
   var toggle_tree_view_element = $(this.toggle_tree_view_id());
   if (toggle_tree_view_element) toggle_tree_view_element.stopObserving("click");
   if (this.pages_top) this.pages_top.stop();
   if (this.pages_bottom) this.pages_bottom.stop();
   this.stop_comments();
   if (this.editor_new) { this.editor_new.stop(); this.editor_new = null; }
   if (this.root_id && $(this.new_comment_id(this.root_id)))
   {
     $(this.new_comment_id(this.root_id)).remove();
   }
  },

  stop_comments: function()
  {
    var t_this = this;
    this.comments.each(function(value) { t_this.stop_comment(value); });
  },

  stop_comment: function(comment)
  {
    if (comment.editor_new) { comment.editor_new.stop(); comment.editor_new = null; }
    if (comment.editor_edit) { comment.editor_edit.stop(); comment.editor_edit = null; }
    var answer_element = $(this.comment_answer_button_id(comment.id));
    if (answer_element) answer_element.stopObserving("click");
    var edit_element = $(this.comment_edit_button_id(comment.id));
    if (edit_element) edit_element.stopObserving("click");
    var delete_element = $(this.comment_delete_button_id(comment.id));
    if (delete_element) delete_element.stopObserving("click");
    var hide_element = $(this.comment_hide_button_id(comment.id));
    if (hide_element) hide_element.stopObserving("click");
    var new_element = $(this.new_comment_button_id(comment.id));
    if (new_element) new_element.stopObserving("click");
    var edit_element = $(this.edit_comment_button_id(comment.id));
    if (edit_element) edit_element.stopObserving("click");
    if (comment.censure) { comment.censure.clean(); comment.censure = null; }
  },

  init: function()
  {
    $(this.root_comments_id()).update("���� ��������...");
    if (this.root_id) {
       new Ajax.Request(community.url.comments_ajax,{
          parameters: { cmd:"get_plain",id:this.root_id,page:this.page },
          onSuccess: this.init_success.bind(this,this.page),
          onFailure: community.ajax_failure.bind(community)
        });
    }
    else if (this.owner_id) {
       new Ajax.Request(community.url.comments_ajax,{
          parameters: { cmd:"get_latest",owner_id:this.owner_id,type:this.type,page:this.page },
          onSuccess: this.init_success.bind(this,this.page),
          onFailure: community.ajax_failure.bind(community)
        });
    }
  },

  init_success: function(page,transport)
  {
    if (page != this.page) return;
    var response = transport.responseText.split(" ");
    if (response[0] != "OK")
    {
      alert("������ ��������� ������������: "+transport.responseText);
      return;
    }
    response.splice(0,1);
    var info = response.join(" ").evalJSON(true);
    this.comments = $A(info.comments);
    this.total = info.total;
    this.pages = info.pages;
    this.can_post = Number(info.can_post) || false;
    if (!this.lastviewtime)
    {
      this.lastviewtime = info.lastviewtime || 0;
    }
    this.comments_hash = this.comments.inject(new Hash(),function(acc,value) { acc.set(value.id,value); return acc; });
    this.display_comments();
    if (this.root_id && this.can_post) this.display_new_comment(this.root_id);
  },

  root_comments_id: function() { return "Community_Comments_Root_"+this.id; },
  toggle_tree_view_id: function() { return "Community_Comments_ToggleTreeView_"+this.id; },
  top_pages_id: function() { return "Community_Comments_TopPages_"+this.id; },
  bottom_pages_id: function() { return "Community_Comments_BottomPages_"+this.id; },
  comment_id: function(id) { return "Community_Comments_Comment_"+id+"_"+this.id; },
  comment_body_id: function(id) { return "Community_Comments_Body_"+id+"_"+this.id; },
  comment_answer_button_id: function(id) { return "Community_Comments_Answer_Button_"+id+"_"+this.id; },
  comment_edit_button_id: function(id) { return "Community_Comments_Edit_Button_"+id+"_"+this.id; },
  comment_delete_button_id: function(id) { return "Community_Comments_Delete_Button_"+id+"_"+this.id; },
  comment_hide_button_id: function(id) { return "Community_Comments_Hide_Button_"+id+"_"+this.id; },
  comment_censure_id: function(id) { return "Community_Comments_Censure_"+id+"_"+this.id; },
  comment_level_id: function(id) { return "Community_Comments_Level_"+id+"_"+this.id; },
  comment_childs_id: function(id) { return "Community_Comments_Childs_"+id+"_"+this.id; },
  new_comment_id: function(root_id) { return "Community_Comments_New_"+root_id+"_"+this.id; },
  new_comment_body_id: function(root_id) { return "Community_Comments_New_Body_"+root_id+"_"+this.id; },
  new_comment_status_id: function(root_id) { return "Community_Comments_New_Status_"+root_id+"_"+this.id; },
  new_comment_button_id: function(root_id) { return "Community_Comments_New_Button_"+root_id+"_"+this.id; },
  edit_comment_id: function(root_id) { return "Community_Comments_Edit_"+root_id+"_"+this.id; },
  edit_comment_body_id: function(root_id) { return "Community_Comments_Edit_Body_"+root_id+"_"+this.id; },
  edit_comment_status_id: function(root_id) { return "Community_Comments_Edit_Status_"+root_id+"_"+this.id; },
  edit_comment_button_id: function(root_id) { return "Community_Comments_Edit_Edit_Button_"+root_id+"_"+this.id; },

  display_comments: function()
  {
    $(this.root_comments_id()).update(this.html()).show();
    this.pages_top = new Community.Pages([$(this.top_pages_id())],this.page,this.pages,'','',{callback:this.gotopage.bind(this)});
    this.pages_bottom = new Community.Pages([$(this.bottom_pages_id())],this.page,this.pages,'','',{callback:this.gotopage.bind(this)});
    if ($(this.toggle_tree_view_id())) {
      $(this.toggle_tree_view_id()).observe("click",this.toggle_tree_view.bindAsEventListener(this));
    }
    this.comments.each(this.observe_comment.bind(this));
  },

  toggle_tree_view: function()
  {
    this.stop();
    this.element.update("");
    new Community.Comments(this.element,this.root_id,'','',
      {lastviewtime:this.lastviewtime});
  },

  gotopage: function(page)
  {
    this.stop();
    this.page = page;
    this.init();
  },

  observe_comment: function(comment)
  {
    if (comment.can_post - 0)
    {
      $(this.comment_answer_button_id(comment.id)).observe("click",this.toggle_new_comment.bindAsEventListener(this,comment.id));
    }
    if (comment.can_edit - 0)
    {
      $(this.comment_edit_button_id(comment.id)).observe("click",this.toggle_edit_comment.bindAsEventListener(this,comment.id));
    }
    if (comment.can_delete - 0)
    {
      $(this.comment_delete_button_id(comment.id)).observe("click",this.delete_comment.bindAsEventListener(this,comment.id));
    }
    if (comment.can_hide - 0)
    {
      $(this.comment_hide_button_id(comment.id)).observe("click",this.hide_comment.bindAsEventListener(this,comment.id));
    }
    if (comment.can_censure - 0)
    {
      comment.censure = new Community.Censure(this.comment_censure_id(comment.id),comment.id,comment.censure_enabled);
    }
  },

  display_new_comment: function(root_id)
  {
    if ($(this.new_comment_id(root_id)))
    {
      $(this.new_comment_id(root_id)).toggle();
    }
    else
    {
      if (root_id == this.root_id)
      {
        new Insertion.After(this.root_comments_id(),this.new_comment_html(root_id));
      }
      else
      {
        new Insertion.Bottom(this.comment_id(root_id),this.new_comment_html(root_id));
      }
      $(this.new_comment_button_id(root_id)).observe(
              "click",this.send_comment.bindAsEventListener(this,root_id)
                    );
      var editor = new Community.HTMLEditor(this.new_comment_body_id(root_id));
      if (root_id != this.root_id) this.comments_hash.get(root_id).editor_new = editor;
      else this.editor_new = editor;
    }
  },

  display_edit_comment: function(root_id)
  {
    if ($(this.edit_comment_id(root_id)))
    {
      $(this.edit_comment_id(root_id)).toggle();
    }
    else
    {
      new Insertion.Bottom(this.comment_id(root_id),this.edit_comment_html(root_id));
      $(this.edit_comment_button_id(root_id)).observe(
              "click",this.save_edit_comment.bindAsEventListener(this,root_id)
                    );
      var editor = new Community.HTMLEditor(this.edit_comment_body_id(root_id));
      this.comments_hash.get(root_id).editor_edit = editor;
    }
  },

  html: function()
  {
    var value = '';

    value += '<span id="'+this.top_pages_id()+'"><!-- --></span>';
    if (this.comments.length == 0) value+='<div>����������� �����������</div>';
    else
    {
      value += '<ul class="list comments plain">';
      var t_this = this;
      this.comments.each(function(comment)
          {
        value += t_this.comment_html(comment);
          });
      value += '</ul>';
    }
    value += '<div id="'+this.bottom_pages_id()+'"><!-- --></div>';
    return value;
  },

  comment_html: function(comment)
  {
    var value = '';

    value += '<li';
    if (comment.author_id != community.prop.user_id && this.lastviewtime && community.cmp_times(comment.time_edit,this.lastviewtime) > 0) value += ' class="active2"';
    value += ' id="'+this.comment_id(comment.id)+'" class="mt10 CommentDiv">';
    if (comment.root) {
     if (comment.root.type == 6) value += '<div>�� ����������� <a href="/track_comments='+comment.root.id+'"><strong>'+(comment.root.title?comment.root.title:'<em>��� ��������</em>')+'</strong></a></div>';
     else if (comment.root.type == 3) value += '<div>�� ���������� <a href="/photo_comments='+comment.root.id+'"><img src="'+community.url.photo_src+'?photo='+comment.root.id+'&size=thmb" alt="'+comment.root.title+'"  title = "'+comment.root.title+'" /></a></div>';
     else if (comment.root.type == 1) value += '<div>�� ������ <a href="/blog_comments='+comment.root.id+'"><strong>'+(comment.root.title?comment.root.title:'<em>��� ��������</em>')+'</strong></a></div>';
    }
    value += '<div style="margin-left:-20px;">' + community.user_title(comment.author_title,{avatar:1}) + '<td>: ' + comment.time_create + '</td></tr></table></div>';
    value += '<div class="mb10 mt10" id="'+this.comment_body_id(comment.id)+'"';
    if (comment.style) value += ' style="'+comment.style+'"';
    value += '>';
    if (Number(comment.censored))
    {
      value += '<div><a href="#" style="color:red" onclick="community.show_cut(this);return false;">[������ ����������� �����...]</a><span style="display:none;">';
    }
    value += community.convert_smiles(comment.body,10);
    if (Number(comment.censored))
    {
      value += '</span></div>';
    }
    value += '</div>';
    if (comment.hidden) value += '<div style="color:red;">��������� ������ �����������</div>';
    value += '<ul class="act ul">';
    if (comment.can_post - 0)
    {
      value=value+'<li><a href="#" id="'+this.comment_answer_button_id(comment.id)+'" onclick="return false;">��������</a></li>';
    }
    if (comment.can_edit - 0)
    {
      value=value+'<li><a href="#" id="'+this.comment_edit_button_id(comment.id)+'" onclick="return false;">�������������</a></li>';
    }
    if (comment.can_delete - 0)
    {
      value=value+'<li><a href="#" id="'+this.comment_delete_button_id(comment.id)+'" onclick="return false;">�������</a></li>';
    }
    if (comment.can_censure - 0)
    {
      value=value+'<li id="'+this.comment_censure_id(comment.id)+'" style="display:none;"></li>';
    }
    if (comment.can_hide - 0)
    {
      value=value+'<li><a href="#" id="'+this.comment_hide_button_id(comment.id)+'" onclick="return false;">';
      if (comment.hidden) value += '�������';
      else value += '������';
      value += '</a></li>';
    }
    value += '</ul>';
    return value;
  },

  new_comment_html: function(root_id)
  {
    var value='<div class="new_comment content p0 m0" id="'+this.new_comment_id(root_id)+'"';
    if (root_id == this.root_id) value=value+' style="display:block"';
    else value += '';
    value += '><h3>�������� ';
    if (root_id == this.root_id) value=value+'�����������:</h3>';
    else value += '�����:</h3>';
    value += '<div><textarea id="'+this.new_comment_body_id(root_id)+'" style="width:350px;height:180px;"></textarea></div>';
    value += '<p class="mt10 mb10" id="'+this.new_comment_status_id(root_id)+'">������: �� ������������</p>';
    value += '<input class="button" type="button" value="���������" id="'+this.new_comment_button_id(root_id)+'" />';
    value += '</div>';
    return value;
  },

  edit_comment_html: function(root_id)
  {
    var value='<div class="clear pt10" id="'+this.edit_comment_id(root_id)+'"';
    value += '><table><tbody><tr><td>������������� ';
    if (root_id == this.root_id) value += '�����������:';
    else value += '�����:';
    value += '</td></tr><td><textarea id="'+this.edit_comment_body_id(root_id)+'" style="width:350px;height:180px;">'+this.comments_hash.get(root_id).body+'</textarea></td></tr>';
    value += '<tr><td id="'+this.edit_comment_status_id(root_id)+'">������: �� ������������</td></tr>';
    value += '<tr><td><input class="button" type="button" value="���������" id="'+this.edit_comment_button_id(root_id)+'"></input>';
    value=value+'</td></tr></tbody></table><br /></div>';
    return value;
  },

  root_html: function()
  {
    var value = '';
    if (this.root_id) value += '<a style="float:right;" href="#" id="'+this.toggle_tree_view_id()+'" onclick="return false;">������� � ����������� ���������</a><div class="clear"></div>';
    value += '<div id="'+this.root_comments_id()+'"></div>';
    return value;
  },

  send_comment: function(event,root_id)
  {
    var ed = Community.HTMLEditor.get(this.new_comment_body_id(root_id));
    if (!ed) return;
    var sending_text = ed.getContent();
    ed.setContent('');
    $(this.new_comment_status_id(root_id)).update("������: ������������...");

    new Ajax.Request(community.url.comments_ajax,
       {
        parameters: {cmd:"post",id:root_id,body:sending_text,check:community.prop.check},
        onSuccess: this.send_comment_success.bind(this,root_id),
        onFailure: this.send_comment_failure.bind(this,root_id)
        }
      );
  },

  send_comment_success: function(root_id,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK")
    {
      $(this.new_comment_status_id(root_id)).update("������: ������ ("+transport.responseText+")");
      return;
    }
    this.stop();
    this.init();
  },

  send_comment_failure: function(root_id,transport)
  {
    $(this.new_comment_status_id(root_id)).update("������: ����������� ������!");
  },

  toggle_new_comment: function(event,root_id)
  {
    this.display_new_comment(root_id);
    if ($(this.new_comment_id(root_id)).visible())
    {
      if ($(this.new_comment_id(root_id)).scrollIntoView)
      {
        $(this.new_comment_id(root_id)).scrollIntoView(false);
      }
      else $(this.new_comment_id(root_id)).scrollTo();
    }
  },

  toggle_edit_comment: function(event,root_id)
  {
    this.display_edit_comment(root_id);
    if ($(this.edit_comment_id(root_id)).visible())
    {
      if ($(this.edit_comment_id(root_id)).scrollIntoView)
      {
        $(this.edit_comment_id(root_id)).scrollIntoView(false);
      }
      else $(this.edit_comment_id(root_id)).scrollTo();
    }
  },

  save_edit_comment: function(event,root_id)
  {
    var ed = Community.HTMLEditor.get(this.edit_comment_body_id(root_id));
    if (!ed) return;
    var sending_text = ed.getContent();
    ed.setContent('');
    $(this.edit_comment_status_id(root_id)).update("������: ������������...");

    new Ajax.Request(community.url.comments_ajax,
       {
        parameters: {cmd:"update",id:root_id,body:sending_text,check:community.prop.check},
        onSuccess: this.save_edit_comment_success.bind(this,root_id),
         onFailure: this.save_edit_comment_failure.bind(this,root_id)
        }
      );
  },

  save_edit_comment_success: function(root_id,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK")
    {
      $(this.edit_comment_status_id(root_id)).update("������: ������ ("+transport.responseText+")");
      return;
    }
    response.splice(0,1);
    var comment = response.join(" ").evalJSON(true);
    $(this.edit_comment_status_id(root_id)).update("������: ������� ����������");
    if ($(this.edit_comment_body_id(root_id))) Community.HTMLEditor.remove(this.edit_comment_body_id(root_id));
    if ($(this.new_comment_body_id(root_id))) Community.HTMLEditor.remove(this.new_comment_body_id(root_id));
    this.stop_comment(this.comments_hash.get(root_id));
    for (var i=0;i<this.comments.length;i++)
    {
      if (this.comments[i].id == comment.id) { this.comments[i] = comment; break; }
    }
    this.comments_hash.set(root_id,comment);
    $(this.comment_id(root_id)).replace(this.comment_html(comment));
    this.observe_comment(comment);
  },

  save_edit_comment_failure: function(root_id,transport)
  {
    $(this.edit_comment_status_id(root_id)).update("������: ����������� ������");
  },

  delete_comment: function(event,root_id)
  {
    if (confirm("�� �������?"))
    {
      new Ajax.Request(community.url.comments_ajax,
         {
          parameters: {cmd:"delete",id:root_id,check:community.prop.check},
          onSuccess: this.delete_comment_success.bind(this,root_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
    }
  },

  delete_comment_success: function(root_id,transport)
  {
    var response=transport.responseText;
    if (response != "OK") { alert("������ �������� �����������: "+transport.responseText); return; }
    this.stop();
    this.init();
  },

  hide_comment: function(event,root_id)
  {
    var comment = this.comments_hash.get(root_id);
    if (!comment) return;
    if (comment.hidden)
    {
      new Ajax.Request(community.url.blog_ajax,
        {
          parameters: {cmd:"post.unhide",id:root_id,check:community.prop.check},
          onSuccess: this.unhide_comment_success.bind(this,root_id),
          onFailure: community.ajax_failure.bind(community)
        });
    }
    else
    {
      new Ajax.Request(community.url.blog_ajax,
        {
          parameters: {cmd:"post.hide",id:root_id,check:community.prop.check},
          onSuccess: this.hide_comment_success.bind(this,root_id),
          onFailure: community.ajax_failure.bind(community)
        });
    }
  },

  hide_comment_success: function(root_id,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������ �������: "+transport.responseText); return; }
    var got_root_id = response[1];
    if (root_id != got_root_id) return;
    var comment = this.comments_hash.get(root_id);
    comment.hidden = 1;

    if ($(this.edit_comment_body_id(root_id))) Community.HTMLEditor.remove(this.edit_comment_body_id(root_id));
    if ($(this.new_comment_body_id(root_id))) Community.HTMLEditor.remove(this.new_comment_body_id(root_id));
    this.stop_comment(this.comments_hash.get(root_id));
    for (var i=0;i<this.comments.length;i++)
    {
      if (this.comments[i].id == comment.id) { this.comments[i] = comment; break; }
    }
    this.comments_hash.set(root_id,comment);
    $(this.comment_id(root_id)).replace(this.comment_html(comment));
    this.observe_comment(comment);
  },

  unhide_comment_success: function(root_id,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") { alert("������ ��������: "+transport.responseText); return; }
    var got_root_id = response[1];
    if (root_id != got_root_id) return;
    var comment = this.comments_hash.get(root_id);
    comment.hidden = 0;

    if ($(this.edit_comment_body_id(root_id))) Community.HTMLEditor.remove(this.edit_comment_body_id(root_id));
    if ($(this.new_comment_body_id(root_id))) Community.HTMLEditor.remove(this.new_comment_body_id(root_id));
    this.stop_comment(this.comments_hash.get(root_id));
    for (var i=0;i<this.comments.length;i++)
    {
      if (this.comments[i].id == comment.id) { this.comments[i] = comment; break; }
    }
    this.comments_hash.set(root_id,comment);
    $(this.comment_id(root_id)).replace(this.comment_html(comment));
    this.observe_comment(comment);
  }
});

Community.Comments.Plain.id = 0;