var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.PhotoMarks = Class.create({
	initialize: function(options) {
		if (!options) options = {};
		this.id = Community.PhotoMarks.id++;
		this.marks = null;
		this.mark_rectangle = null;
		this.faded_img_element = null;
		this.img_element = null;
		this.inner_img_element = null;
		this.div_element = null;
		this.cur_noting_mark = null;
		this.note_element = null;
		community.init.add_unload(this.clean.bind(this));
	},

	clean: function() {
		this.stop();
		this.marks = null;
		this.img_element = null;
		this.div_element = null;
	},

	stop: function() {
		if (this.img_element) this.img_element.stopObserving("mousemove").stopObserving("mouseout");
		this.stop_marks();
	},
	
	stop_marks: function() {
		this.mark_hide();
		this.mark_note_hide();
		if (this.marks) {
			this.marks.each(function(item) {
				var mark_element = $(item.mark_id);
				if (mark_element) mark_element.stopObserving("mouseover").stopObserving("mouseout");
			});
		}
	},
 	  
	set_photo: function(img_element,div_element) {
		this.stop();
		this.marks = null;
		this.img_element = img_element;
		this.div_element = div_element;
		this.img_element.observe("mousemove",this.img_mouse_move.bindAsEventListener(this));
		this.img_element.observe("mouseout",this.mark_note_hide.bind(this));
	},

	img_mouse_move: function(event) {
		if (!this.marks) return;
		
		var pos_on_div = this.pos_on_div(event);
		var mark = null;
		this.marks.each(function(item) {
			if (pos_on_div.top > item.top && pos_on_div.top < item.top + item.height &&
				pos_on_div.left > item.left && pos_on_div.left < item.left + item.width
			) {
				mark = item;
				throw $break;
			}
		});
		if (mark) {
			if (!this.cur_noting_mark || this.cur_noting_mark.name != mark.name) this.mark_note_show(mark);
		}
		else if (!mark) this.mark_note_hide();
	},
	
	set_marks: function(marks) {
		if (!marks) marks = new Array();
		this.stop_marks();
		this.marks = marks;
		this.marks.each(function(item) {
			$(item.mark_id).observe("mouseover",this.mark_show.bind(this,item)).observe("mouseout",this.mark_hide.bind(this));
		},this);
	},
	
	mark_show: function(mark) {
		this.mark_hide();
		var div_element_size = this.img_element.getDimensions();
		
		this.faded_img_element = new Element("div").setStyle({
			position: "absolute", backgroundColor: "black", opacity: "0.5",
			width: div_element_size.width+"px", height: div_element_size.height+"px",
			top: "0px", left: "0px", cursor: "se-resize", zIndex: "2"
		});
		this.div_element.appendChild(this.faded_img_element);
		
		this.mark_rectangle = new Element("div").setStyle({
			position: "absolute", backgroundColor: "transparent", width: mark.width+"px", height: mark.height+"px", overflow: "hidden",
			top: mark.top+"px", left: mark.left+"px", zIndex: "3", border: "2px solid gray"
		});
		this.div_element.appendChild(this.mark_rectangle);
		
		this.inner_img_element = new Element("img",{ src: this.img_element.src }).setStyle({
			backgroundColor: "white", position: "absolute", left: (-(mark.left+2))+"px", top: (-(mark.top+2))+"px"
		});
		this.mark_rectangle.appendChild(this.inner_img_element);
	},
	
	mark_hide: function() {
		if (!this.mark_rectangle) return;
		this.inner_img_element.remove(); this.inner_img_element = null;
		this.mark_rectangle.remove(); this.mark_rectangle = null;
		this.faded_img_element.remove(); this.faded_img_element = null;
	},
	
	mark_note_show: function(mark) {
		this.mark_note_hide();
		var div_element_size = this.img_element.getDimensions();
		
		var note_height = 18;

		var note_width = (mark.width);

		var note_min_length = mark.name.length * 12;
		if (note_width < note_min_length) note_width = note_min_length;

		var note_top = ((mark.top) + (mark.height));
	   	if (note_top + note_height > div_element_size.height) note_top = div_element_size.height - note_height;
		if (note_top < 0) note_top = 0;

		var note_left = (mark.left) - ((note_width) - (mark.width))/2;
	  	if ((note_left) + (note_width) > div_element_size.width) note_left = (div_element_size.width) - (note_width);
		if (note_left < 0) note_left = 0;

		this.note_element = new Element("div").setStyle({
			position: "absolute", backgroundColor: "black", color: "white", width: note_width+"px", height: note_height+"px",
			top: note_top+"px", left: note_left+"px", zIndex: "2", textAlign: "center"
		}).update(mark.name);
		this.div_element.appendChild(this.note_element);
		
		this.cur_noting_mark = mark;
	},
	
	mark_note_hide: function() {
		if (!this.cur_noting_mark) return;
		this.note_element.remove(); this.note_element = null;
		this.cur_noting_mark = null;
	},

	pos_on_div: function(event) {
		var div_offset = this.div_element.cumulativeOffset();
	   	var pos = { top: event.pointerY() - div_offset.top, left: event.pointerX() - div_offset.left };
		return pos;
	}

});

Community.PhotoMarks.id = 0;

//-----------------------------------------------------------------------------------------------------

Community.PhotoMarksView = Class.create({
	initialize: function(container,options) {
		if (!options) options = {};
		this.id = Community.PhotoMarksView.id++;
		this.photo_info = null;
		this.img_element = null;
		this.div_element = null;
		this.container = null;
		this.photo_marks = new Community.PhotoMarks();
		community.init.add_unload(this.clean.bind(this));
	},

	clean: function() {
		this.stop();
	},
	
	stop: function() {
		if ($(this.photo_mark_nome_id())) $(this.photo_mark_nome_id()).stopObserving("click");
		this.photo_marks.clean();
	},

	photo_mark_id: function(index) { return "Community_PhotoMarksView_PhotoMark_"+index+"_"+this.id; },
	photo_mark_nome_id: function() { return "Community_PhotoMarksView_PhotoMark_NoMe_"+this.id; },
	
	set_photo: function(photo_info,img_element,div_element,container) {
		this.stop();
		this.photo_info = photo_info;
		this.img_element = $(img_element);
		this.div_element = $(div_element);
		this.container = $(container);
		this.container.update(this.html_photo_marks());
		this.photo_marks.set_photo(this.img_element,this.div_element);
		this.photo_marks.set_marks(this.photo_info.marks);
		if ($(this.photo_mark_nome_id())) $(this.photo_mark_nome_id()).observe("click",this.mark_nome.bind(this));
	},
	
	mark_nome: function() {
		if (confirm("�� �������, ��� ������ ������� ������� ���� � ���� ����������?")) {
			new Ajax.Request(community.url.photo_marks_ajax,{
				parameters: { cmd: "mark.delete", id: this.photo_info.id, name: community.prop.user_title, check: community.prop.check },
				onSuccess: this.mark_nome_success.bind(this,this.photo_info.id),
				onFailure: community.ajax_failure.bind(community)
			});
		}
	},

	mark_nome_success: function(id,transport) {
		if (!this.photo_info || this.photo_info.id != id) return;
		if (transport.responseText != "OK") { alert("������ ��������: "+transport.responseText); return; }
		if (!this.photo_info.marks) this.photo_info.marks = new Array();
		this.photo_info.marks = this.photo_info.marks.findAll(function(item) { return item.name != community.prop.user_title });
		
		this.photo_marks.stop_marks();
		if ($(this.photo_mark_nome_id())) $(this.photo_mark_nome_id()).stopObserving("click");
		this.container.update(this.html_photo_marks());
		this.photo_marks.set_marks(this.photo_info.marks);
	},
	
	html_photo_marks: function() {
		var value = '';

		var is_me = false;
		if (!this.photo_info.marks || !this.photo_info.marks.length) return '';

		value += '<div style="text-align:left;">';
		value += '<strong>��������:</strong> ';
		for (var i=0;i<this.photo_info.marks.length;i++) {
			if (i>0) value += ', ';
			var mark = this.photo_info.marks[i];
			mark.mark_id = this.photo_mark_id(i);
			value += '<span id="'+this.photo_mark_id(i)+'">';
			if (Number(mark.is_user)) value += community.user_title(mark.name);
			else value += mark.name;
			value += '</span>';
			value += ' (<a href="'+community.url.photo_marks_search+'?cmd=search&amp;name='+mark.name+'">�����</a>)';
			if (community.prop.user_title == mark.name) is_me = true;
		}
		value += '</div>';
		if (is_me) {
			value += '<div style="text-align:left;"><a href="#" id="'+this.photo_mark_nome_id()+'" onclick="return false;">';
			value += '�� ���� ���������� ��� ����! ��� ������!';
			value += '</a></div>';
		}
		return value;
	}	
	
});

Community.PhotoMarksView.id = 0;
