var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.PhotoMarksEdit = Class.create({
	initialize: function(options) {
		if (!options) options = {};
		this.id = Community.PhotoMarksEdit.id++;
		this.photo_info = null;
		this.marks = null;
		this.mark_rectangle = null;
		this.faded_img_element = null;
		this.img_element = null;
		this.inner_img_element = null;
		this.div_element = null;
		this.div_element_size = null;
		this.mark_resize_rectangles = null;
		this.mark_dragger = null;
		this.mark_drag_mode = "se-resize";
		this.mark_dragging = false;
		this.mark_window = null;
		this.mark_window_bind_input = null;
		this.window = null;
		this.add_id = 0;
		this.photo_marks = new Community.PhotoMarks();
		community.init.add_unload(this.clean.bind(this));
	},

	clean: function() {
		this.stop();
	},

	stop: function() {
		this.photo_marks.clean();
		this.remove_mark();
		this.stop_marks();
		if (this.window) { this.window.clean(); this.window = null; }
	},

	stop_marks: function() {
		if (!this.marks) return;
		for (var i=0;i<this.marks.length;i++) {
			if ($(this.added_mark_delete_id(i))) $(this.added_mark_delete_id(i)).stopObserving("click");
		}
	},

	photo_img_id: function() { return "Community_PhotoMarks_PhotoImg_"+this.id; },
	photo_div_id: function() { return "Community_PhotoMarks_PhotoDiv_"+this.id; },
	add_mark_input_id: function() { return "Community_PhotoMarks_AddMarkInput_"+this.id; },
	add_mark_status_id: function() { return "Community_PhotoMarks_AddMarkStatus_"+this.id; },

	added_marks_id: function() { return "Community_PhotoMarks_AddedMarks_"+this.id; },
	added_mark_id: function(index) { return "Community_PhotoMarks_AddedMark_"+index+"_"+this.id; },
	added_mark_delete_id: function(index) { return "Community_PhotoMarks_AddedMark_Delete_"+index+"_"+this.id; },

	show: function(photo_info) {
		this.stop();
		if (this.photo_info && this.photo_info.id != photo_info.id) this.marks = null;

		this.photo_info = photo_info;
		this.window = new Community.Window({
			title:"�������� ����� �� ���������� "+(photo_info.title ? photo_info.title : "(��� ��������)"),
			content: this.marks ? this.window_content() : "���� ��������...",
			ok_button:true,
			on_ok:this.marks_ok.bind(this)
		});
		this.window.show();

		if (this.marks) this.init();
		else {
			new Ajax.Request(community.url.photo_marks_ajax,{
				parameters: { cmd: "marks_get", id: photo_info.id },
				onSuccess: this.load_success.bind(this),
				onFailure: community.ajax_failure.bind(community)
			});
		}
	},

	load_success: function(transport) {
		if (!this.window) return;
		var response = transport.responseText.split(" ");
		if (response[0] != "OK") { this.window.update("������ ����������: "+transport.responseText); return; }
		response.splice(0,1);
		this.marks = $A(response.join(" ").evalJSON(true));
		this.window.update(this.window_content());
		this.init();
	},

	init: function() {
		this.img_element = $(this.photo_img_id());
		this.div_element = $(this.photo_div_id());

		this.img_element.setStyle({ cursor: "crosshair" });
		this.img_element.observe("mousedown",this.start_mark.bindAsEventListener(this));

		this.photo_marks.set_photo(this.img_element,this.div_element);
		this.photo_marks.set_marks(this.marks);

		for (var i=0;i<this.marks.length;i++) {
			if ($(this.added_mark_delete_id(i))) $(this.added_mark_delete_id(i)).observe("click",this.delete_mark.bind(this,this.marks[i].name));
		}
	},

	marks_ok: function() {
		this.stop();
	},

	window_content: function() {
		var photo_info = this.photo_info;
		var value = '';
		value += '����� �������� ����-���� �� ����������, ����� ������� ������ ������� ������ � ������ ���.';
		value += '<div id="'+this.photo_div_id()+'" style="position:relative;">';
		value += '<img id="'+this.photo_img_id()+'" src="'+community.url.photo_src+'?size=std&amp;photo='+photo_info.id+'" />';
		value += '</div>';
		value += '<div>';
		value += '<strong>�� ���������� ��������:</strong> <span id="'+this.added_marks_id()+'">';
		value += this.html_added_marks();
		value += '</span>';
		value += '</div>';
		return value;
	},

 	mark_window_content: function() {
		var value = '';
		value += '<div style="margin-bottom: 10px;">���: <span id="'+this.add_mark_input_id()+'"></span></div>';
		value += '<div id="'+this.add_mark_status_id()+'"></div>';
		return value;
	},

	html_added_marks: function() {
		var value = '';
		if (!this.marks || !this.marks.length) return '���';
		for (var i=0;i<this.marks.length;i++) {
			if (i>0) value += ', ';
			var mark = this.marks[i];
			mark.mark_id = this.added_mark_id(i);
			value += '<span id="'+this.added_mark_id(i)+'">';
			if (Number(mark.is_user)) value += community.user_title(mark.name);
			else value += mark.name;
			value += '</span>';
			value += ' <a class="delete" href="#" id="'+this.added_mark_delete_id(i)+'" onclick="return false;">';
			value += '<img src="/i/comm/skin' + community.prop.skin + '/icon_delete.gif" alt="�������" title="�������" /></a>';
			value += ' (<a href="'+community.url.photo_marks_search+'?cmd=search&amp;name='+mark.name+'">�����</a>)';
		}
		return value;
	},

	start_mark: function(event) {
		this.remove_mark();

		this.div_element_size = this.img_element.getDimensions();

		var pos_on_div = this.pos_on_div(event);
		this.faded_img_element = new Element("div").setStyle({
			position: "absolute", backgroundColor: "black", opacity: "0.5",
			width: this.div_element_size.width+"px", height: this.div_element_size.height+"px",
			top: "0px", left: "0px", cursor: "se-resize", zIndex: "2"
		});
		this.div_element.appendChild(this.faded_img_element);

		this.mark_rectangle = new Element("div").setStyle({
			position: "absolute", backgroundColor: "transparent", width: "0px", height: "0px", overflow: "hidden",
			top: pos_on_div.top+"px", left: pos_on_div.left+"px", cursor: "se-resize", zIndex: "3", border: "2px solid gray"
		});
		this.div_element.appendChild(this.mark_rectangle);

		this.inner_img_element = new Element("img",{ src: this.img_element.src }).setStyle({
		   	backgroundColor: "white", position: "absolute", left: (-(parseInt(pos_on_div.left)+2))+"px", top: (-(parseInt(pos_on_div.top)+2))+"px"
		});
		this.mark_rectangle.appendChild(this.inner_img_element);

		this.mark_resize_rectangles = new Hash();

		var cursors = {
			top: "n-resize", left: "w-resize", right: "e-resize", bottom: "s-resize",
			top_left: "nw-resize", top_right: "ne-resize", bottom_left: "sw-resize", bottom_right: "se-resize"
		};
		$A([ "top", "left", "right", "bottom", "top_left", "top_right", "bottom_left", "bottom_right" ]).each(function(key) {
			var resize_rectangle = new Element("div").setStyle({
				position: "absolute", backgroundColor: "white", width: "10px", height: "10px", opacity: 0.8,
				top: "0px", left: "0px", display: "none", cursor: cursors[key], zIndex: "4"
			});
			this.div_element.appendChild(resize_rectangle);
			resize_rectangle.observe("mousedown",this.mark_move.bindAsEventListener(this,cursors[key]));
			this.mark_resize_rectangles.set(key,resize_rectangle);
		},this);

		this.mark_dragger = new Community.Dragger(this.mark_rectangle,{
			on_start_drag: this.mark_start_drag.bind(this),
			on_drag: this.mark_drag.bind(this),
			on_stop_drag: this.mark_stop_drag.bind(this)
		});

		this.mark_move(event,"se-resize");
	},

	mark_move: function(event,mode) {

		this.set_mark_drag_mode(mode);
		this.mark_dragger.start_drag(event);
	},

	mark_start_drag: function(event) {
		this.mark_resize_rectangles.each(function(pair) {
			pair.value.hide();
		});
		this.mark_dragging = true;
		this.faded_img_element.setStyle({ cursor: this.mark_drag_mode });
	},

	mark_drag: function(event) {

		var pos_on_div = this.pos_on_div(event);
		var mark_top = parseInt(this.mark_rectangle.style.top);
		var mark_left = parseInt(this.mark_rectangle.style.left);
		var mark_width = parseInt(this.mark_rectangle.style.width);
		var mark_height = parseInt(this.mark_rectangle.style.height);

		// Changing sizing direction
		if (this.mark_drag_mode == "se-resize") {
			if (pos_on_div.top < mark_top && pos_on_div.left < mark_left) this.set_mark_drag_mode("nw-resize");
			else if (pos_on_div.top < mark_top) this.set_mark_drag_mode("ne-resize");
			else if (pos_on_div.left < mark_left) this.set_mark_drag_mode("sw-resize");
		}
		else if (this.mark_drag_mode == "nw-resize") {
			if (pos_on_div.top > mark_top+mark_height && pos_on_div.left > mark_left+mark_width) this.set_mark_drag_mode("se-resize");
			else if (pos_on_div.top > mark_top+mark_height) this.set_mark_drag_mode("sw-resize");
			else if (pos_on_div.left > mark_left+mark_width) this.set_mark_drag_mode("ne-resize");
		}
		else if (this.mark_drag_mode == "sw-resize") {
			if (pos_on_div.top < mark_top && pos_on_div.left > mark_left+mark_width) this.set_mark_drag_mode("ne-resize");
			else if (pos_on_div.top < mark_top) this.set_mark_drag_mode("nw-resize");
			else if (pos_on_div.left > mark_left+mark_width) this.set_mark_drag_mode("se-resize");
		}
		else if (this.mark_drag_mode == "ne-resize") {
			if (pos_on_div.top > mark_top+mark_height && pos_on_div.left < mark_left) this.set_mark_drag_mode("sw-resize");
			else if (pos_on_div.top > mark_top+mark_height) this.set_mark_drag_mode("se-resize");
			else if (pos_on_div.left < mark_left) this.set_mark_drag_mode("nw-resize");
		}
		else if (this.mark_drag_mode == "n-resize") {
			if (pos_on_div.top > mark_top+mark_height) this.set_mark_drag_mode("s-resize");
		}
		else if (this.mark_drag_mode == "s-resize") {
			if (pos_on_div.top < mark_top) this.set_mark_drag_mode("n-resize");
		}
		else if (this.mark_drag_mode == "e-resize") {
			if (pos_on_div.left < mark_left) this.set_mark_drag_mode("w-resize");
		}
		else if (this.mark_drag_mode == "w-resize") {
			if (pos_on_div.left > mark_left+mark_width) this.set_mark_drag_mode("e-resize");
		}

		// Calculating new attributes
		if (this.mark_drag_mode == "se-resize" || this.mark_drag_mode == "sw-resize" || this.mark_drag_mode == "s-resize") {
			mark_height = pos_on_div.top - mark_top;
		}
		else if (this.mark_drag_mode == "ne-resize" || this.mark_drag_mode == "nw-resize" || this.mark_drag_mode == "n-resize") {
			mark_height = mark_height + (mark_top - pos_on_div.top);
			mark_top = pos_on_div.top;
		}

		if (this.mark_drag_mode == "se-resize" || this.mark_drag_mode == "ne-resize" || this.mark_drag_mode == "e-resize") {
			mark_width = pos_on_div.left - mark_left;
		}
		else if (this.mark_drag_mode == "sw-resize" || this.mark_drag_mode == "nw-resize" || this.mark_drag_mode == "w-resize") {
			mark_width = mark_width + (mark_left - pos_on_div.left);
			mark_left = pos_on_div.left;
		}

		if (this.mark_drag_mode == "move") {
			mark_top = pos_on_div.top - this.mark_dragger.drag_delta_y;
			mark_left = pos_on_div.left - this.mark_dragger.drag_delta_x;
			if (mark_top < 0) mark_top = 0;
			else if (mark_top+mark_height > this.div_element_size.height) mark_top = this.div_element_size.height - mark_height;
			if (mark_left < 0) mark_left = 0;
			else if (mark_left+mark_width > this.div_element_size.width) mark_left = this.div_element_size.width - mark_width;
		}

		this.mark_rectangle.setStyle({ width: mark_width+"px", height: mark_height+"px", top: mark_top+"px", left: mark_left+"px" });
	   	this.inner_img_element.setStyle({ top: (-(mark_top+2))+"px", left: (-(mark_left+2))+"px" });
	},

	mark_stop_drag: function(event) {
		this.mark_dragging = false;
		this.set_mark_drag_mode("move");
		this.faded_img_element.setStyle({ cursor: "crosshair" });

		var resize_rectangle_size = 10;
		var mark_top = parseInt(this.mark_rectangle.style.top);
		var mark_left = parseInt(this.mark_rectangle.style.left);
		var mark_width = parseInt(this.mark_rectangle.style.width);
		var mark_height = parseInt(this.mark_rectangle.style.height);
		if (mark_width < 30) { this.mark_rectangle.style.width = "30px"; mark_width = 30; }
		if (mark_height < 30) { this.mark_rectangle.style.height = "30px"; mark_height = 30; }

		this.mark_resize_rectangles.each(function(pair) {
			var style = { top: "0px", left: "0px" };
			var style_top = mark_top - resize_rectangle_size / 2 + 1;
			var style_left = mark_left - resize_rectangle_size / 2 + 1;
			if (pair.key == "top" || pair.key == "top_left" || pair.key == "top_right")
				style.top = style_top+"px";
			else if  (pair.key == "left" || pair.key == "right")
				style.top = (style_top + mark_height / 2 + 1)+"px";
			else if (pair.key == "bottom" || pair.key == "bottom_left" || pair.key == "bottom_right")
				style.top = (style_top + mark_height + 2)+"px";

			if (pair.key == "left" || pair.key == "top_left" || pair.key == "bottom_left")
				style.left = style_left+"px";
			else if (pair.key == "top" || pair.key == "bottom")
				style.left = (style_left + mark_width / 2 + 1)+"px";
			else if (pair.key == "right" || pair.key == "top_right" || pair.key == "bottom_right")
				style.left = (style_left + mark_width + 2)+"px";

			pair.value.setStyle(style);
			pair.value.show();
		});

		if (!this.mark_window) {
			this.mark_window = new Community.Window({
				title:"�������� ����� �������",
				content:this.mark_window_content(),
				ok_button:true,
				on_ok:this.add_mark.bind(this),
				on_close: this.add_mark_cancel.bind(this)
			});
			this.mark_window.show();
			this.mark_window_user_field = new Community.UserField(this.add_mark_input_id(),'');
			var t_this = this;
			setTimeout(function() {
				t_this.mark_window_bind_input = new Community.BindInputWithButton(
					t_this.mark_window_user_field.field_input_id(),
					t_this.mark_window.ok_button_element()
				);
				$(t_this.mark_window_user_field.field_input_id()).focus();
			},10);
		}
	},

	add_mark: function() {
		this.mark_window_user_field.change();
		var name = $F(this.add_mark_input_id()+"_Input");
		if (!name) return;
		if (this.marks.find(function(item) { return item.name == name; })) { alert("��� ������� ��� ���� �� ����������"); return; }

		var mark_top = parseInt(this.mark_rectangle.style.top);
		var mark_left = parseInt(this.mark_rectangle.style.left);
		var mark_width = parseInt(this.mark_rectangle.style.width);
		var mark_height = parseInt(this.mark_rectangle.style.height);

		this.add_id++;
		new Ajax.Request(community.url.photo_marks_ajax,{
			parameters: {
				cmd: "mark.add", add_id: this.add_id, id: this.photo_info.id, name: name, top: mark_top, left: mark_left,
				width: mark_width, height: mark_height, check: community.prop.check
			},
			onSuccess: this.add_mark_success.bind(this,this.add_id),
			onFailure: community.ajax_failure.bind(community)
		});
		$(this.add_mark_status_id()).update("������� �����������...");
	},

	add_mark_success: function(add_id,transport) {
		var response = transport.responseText.split(" ");
		if (response[0] != "OK") { $(this.add_mark_status_id()).update("������ ����������: "+transport.responseText); return; }
		response.splice(0,1);

		var mark = response.join(" ").evalJSON(true);

		if (mark.add_id != add_id) return;
		delete mark.add_id;
		if (!this.marks) this.marks = new Array();
		this.marks.push(mark);
		this.update_marks();
		this.remove_mark();
	},

	add_mark_cancel: function() {
		this.remove_mark();
	},

	delete_mark: function(name) {
		if (confirm("�� �������, ��� ������ ������� ������� \""+name+"\" � ���� ����������?")) {
			new Ajax.Request(community.url.photo_marks_ajax,{
				parameters: {
					cmd: "mark.delete", id: this.photo_info.id, name: name, check: community.prop.check
				},
				onSuccess: this.delete_mark_success.bind(this,name),
				onFailure: community.ajax_failure.bind(community)
			});
		}
	},

	delete_mark_success: function(name,transport) {
		if (transport.responseText != "OK") { alert("������ ��������: "+transport.responseText); return; }

		if (!this.marks) this.marks = new Array();
		this.marks = this.marks.findAll(function(item) { return item.name != name });
		this.update_marks();
	},

	update_marks: function() {
		this.photo_marks.stop_marks();
		this.stop_marks();
		$(this.added_marks_id()).update(this.html_added_marks());
		for (var i=0;i<this.marks.length;i++) {
			if ($(this.added_mark_delete_id(i))) $(this.added_mark_delete_id(i)).observe("click",this.delete_mark.bind(this,this.marks[i].name));
		}
		this.photo_marks.set_marks(this.marks);
	},

	pos_on_div: function(event) {
		var div_offset = this.div_element.cumulativeOffset();
		var pos = { top: event.pointerY() - div_offset.top, left: event.pointerX() - div_offset.left };

		if (pos.top < 0) pos.top = 0;
		else if (pos.top > this.div_element_size.height) pos.top = this.div_element_size.height;

		if (pos.left < 0) pos.left = 0;
		else if (pos.left > this.div_element_size.width) pos.left = this.div_element_size.left;

		return pos;
	},

	remove_mark: function() {
		if (!this.mark_rectangle) return;

		if (this.mark_window) {
			this.mark_window.clean(); this.mark_window = null;
			this.mark_window_bind_input.clean(); this.mark_window_bind_input = null;
			this.mark_window_user_field.clean(); this.mark_window_user_field = null;
		}
		this.mark_dragger.clean(); this.mark_dragger = null;
		this.inner_img_element.remove(); this.inner_img_element = null;
		this.mark_rectangle.remove(); this.mark_rectangle = null;
		this.faded_img_element.remove(); this.faded_img_element = null;

		this.mark_resize_rectangles.each(function(pair) {
			pair.value.stopObserving("mousedown");
			pair.value.remove();
		});
		this.mark_resize_rectangles = null;
	},

	set_mark_drag_mode: function(mode) {
		if (this.mark_drag_mode == mode) return;
		this.mark_rectangle.setStyle({ cursor: mode });
		if (this.mark_dragging) this.faded_img_element.setStyle({ cursor: mode });
		this.mark_drag_mode = mode;
	}

});

Community.PhotoMarksEdit.id = 0;