var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.UserSearch = Class.create({
  initialize: function(element,options)
  {
	if (!options) options = {};
	this.id = Community.UserSearch.id++;
	this.write = options.write || false;
	this.element = element;
	if (this.write)
	{
		document.write(this.html());
		community.init.add(this.init.bind(this));
	}
	else this.init();
	community.init.add_unload(this.clean.bind(this));
  },

  init: function()
  {
	this.element = $(this.element);
	if (!this.element) throw new Error("��������� ��� ������ ������������� �� ����������!");
	if (!this.write) this.element.update(this.html());
	$(this.search_button_id()).observe("click",this.search.bindAsEventListener(this));
	this.bind = new Community.BindInputWithButton(this.search_input_id(),this.search_button_id());
  },

  search_results_id: function() { return "Community_UserSearch_Results_"+this.id; },
  search_button_id: function() { return "Community_UserSearch_Button_"+this.id; },
  search_input_id: function() { return "Community_UserSearch_Input_"+this.id; },

  clean: function()
  {
	this.stop();
	this.element = null;
	if (this.bind) this.bind.clean();
	this.bind = null;
  },

  stop: function()
  {
	var button_element = $(this.search_button_id());
	if (button_element) button_element.stopObserving("click");
	if (this.bind) this.bind.stop();
  },

  html: function()
  {
  var value='';
  value=value+'<div id="'+this.search_results_id()+'" class="user_group_search_result"></div>';
  value=value+'<div class="clear"><input style="width: 140px; margin-right: 10px;" class="inp_text" type="text" id="'+this.search_input_id()+'" />';
  value=value+'<span class="button" id="'+this.search_button_id()+'">������</span>';
  value=value+'</div><p style="padding-top:5px;clear:both;"><small>*������� ������ 3 ��� ����� ���� �����</small></p>';
  return value;
  /*var value='<div class="bold">����� ������������� � �����:</div>';
  value=value+'<div id="'+this.search_results_id()+'" style="display:none;"></div>';
  value=value+'<div><input type="text" id="'+this.search_input_id()+'"></input>&nbsp;';
  value=value+'<input type="button" value="������" id="'+this.search_button_id()+'"></input>';
  value=value+'<small><br>*������� ������ 3 ��� ����� ���� �����</small></div>';
  return value; */
  },

  search: function(event)
  {
	new Ajax.Request(community.url.info_ajax,{
					parameters: { cmd:"search",title:$F(this.search_input_id()) },
					onSuccess: this.search_success.bind(this),
					onFailure: community.ajax_failure.bind(community)
				 		 });
	$(this.search_results_id()).update("���� ��������...").show();
  },

  search_success: function(transport)
  {
	var response = transport.responseText.split(",");
	if (response[0] != "OK") { alert("������ ������: "+transport.responseText); return; }
	response.splice(0,1);
	response = response.collect(function(result) { return "<span>[ "+community.user_title(result)+" ]</span>"; });
	$(this.search_results_id()).update("<p style='margin-bottom:15px;'>��������� ������: "+(response.join(" ") || "������ �� �������") + "</p>");
  }
});

Community.UserSearch.id = 0;
