var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

if (!Community.Audio) Community.Audio = new Object();
Community.Audio.Track = Class.create({
  initialize: function(element,track_id,options)
  {
    options = options || {};
    this.id = Community.Audio.Track.id++;
    this.element = $(element);
    if (!this.element) throw new Error("����������� ��������� ��� ������������ �����������!");
    this.track_id = track_id;
    this.playing = false;
    this.paused = false;
    this.bar_length = options.bar_length || 300;
    this.volume_bar_length = options.volume_bar_length || 100;
    this.on_finish_play = options.on_finish_play || null;
    this.on_play = options.on_play || null;
    this.on_pause = options.on_pause || null;
    community.init.add_unload(this.clean.bind(this));
    this.display();
  },

  clean: function()
  {
    if ($(this.play_button_id())) $(this.play_button_id()).stopObserving("click");
    if ($(this.play_bar_container_id())) $(this.play_bar_container_id()).stopObserving("click");
    if ($(this.volume_bar_container_id())) $(this.volume_bar_container_id()).stopObserving("click");
    if (this.dragger) this.dragger.clean();
    if (this.volume_dragger) this.volume_dragger.clean();
    if (this.sound)
    {
      this.sound.destruct();
      this.sound = null;
    }
  },

  display: function()
  {
    if (!community.is_sound_loaded) { setTimeout(this.display.bind(this),200); return; }
    if (!Number(community.prop.user_user_id))
    {
      this.element.update(this.html_error());
      return;
    }
    this.element.update(this.html());
    this.sound = soundManager.createSound({
        id: 'sound'+this.id,
        url: community.url.track_src+'?id='+this.track_id,
        autoLoad: false,
        autoPlay: false,
        onload: this.track_loaded.bind(this),
        whileloading: this.track_loading.bind(this),
        whileplaying: this.track_playing.bind(this),
        onfinish: this.track_finish.bind(this),
        ondataerror: this.track_dataerror.bind(this),
        onid3: this.track_id3.bind(this)
    });
    $(this.play_button_id()).observe("click",this.play.bind(this,true));
    $(this.play_bar_container_id()).observe("click",this.set_play_cursor.bindAsEventListener(this));
    $(this.volume_bar_container_id()).observe("click",this.set_volume_cursor.bindAsEventListener(this));
    this.dragger = new Community.Dragger(this.play_cursor_id(),
                                         {on_drag:this.drag_play_cursor.bind(this)}
                                        );
    this.volume_dragger = new Community.Dragger(this.volume_cursor_id(),
                                                {on_drag:this.drag_volume_cursor.bind(this)}
                                               );
  },

  play_button_id: function() { return "Community_Audio_Track_PlayButton_"+this.id; },
  time_span_id: function() { return "Community_Audio_Track_TimeSpan_"+this.id; },
  total_time_span_id: function() { return "Community_Audio_Track_TotalTimeSpan_"+this.id; },
  play_img_id: function() { return "Community_Audio_Track_PlayImg_"+this.id; },
  play_bar_container_id: function() { return "Community_Audio_Track_PlayBar_Container_"+this.id; },
  play_bar_id: function() { return "Community_Audio_Track_PlayBar_"+this.id; },
  loaded_bar_id: function() { return "Community_Audio_Track_LoadedBar_"+this.id; },
  play_cursor_id: function() { return "Community_Audio_Track_PlayCursor_"+this.id; },
  volume_bar_id: function() { return "Community_Audio_Track_VolumeBar_"+this.id; },
  volume_bar_container_id: function() { return "Community_Audio_Track_VolumeBar_Container_"+this.id; },
  volume_cursor_id: function() { return "Community_Audio_Track_VolumeCursor_"+this.id; },
  volume_percent_id: function() { return "Community_Audio_Track_VolumePercent_"+this.id; },
  volume_icon_id: function() { return "Community_Audio_Track_VolumeIcon_"+this.id; },

  html: function()
  {
    var value = '<table class="mb20"><tr>';
    value += '<td class="pb5">';
    value += '<a class="mr10" href="#" onclick="return false;" id="'+this.play_button_id()+'"><img style="vertical-align:middle;" src="/i/comm/play.png" id="'+this.play_img_id()+'" /></a>';
    value += '<span style="vertical-align:middle;" id="'+this.time_span_id()+'"></span> ';
    value += '<span style="vertical-align:middle;" id="'+this.total_time_span_id()+'"></span>';
    value += '</td>';
    value += '<td class="pl20">';
    value += '<img style="vertical-align:middle;display:none;" id="'+this.volume_icon_id()+'" src="/i/comm/volume.png" /><span style="vertical-align:middle;display:none;" id="'+this.volume_percent_id()+'">100%</span>';
    value += '</td></tr>';

    value += '<tr><td style="height:20px;cursor:pointer;" id="'+this.play_bar_container_id()+'">';
    value += '<div style="width:'+this.bar_length+'px;height:2px;background:gray;position:relative;">';
    value += '<div id="'+this.play_bar_id()+'" style="height:2px;width:'+this.bar_length+'px;position:absolute;left:0;top:0;background:black;display:none;"><!-- --></div>';
    value += '<div id="'+this.loaded_bar_id()+'" style="height:2px;position:absolute;left:0;top:0;width:0px;background:#4682B4;display:none;"><!-- --></div>';
    value += '<div style="cursor:pointer;position:absolute;left:-7px;top:3px;border-bottom:8px solid gray;border-left:8px solid #fff;border-right:8px solid #fff;display:none;" id="'+this.play_cursor_id()+'"><!-- --></div>';
    value += '</div>';
    value += '</td><td class="pl20" style="cursor:pointer;" id="'+this.volume_bar_container_id()+'">';
    value += '<div style="position:relative;width:'+this.volume_bar_length+'px;background:#4682B4;height:2px;display:none;" id="'+this.volume_bar_id()+'">';
    value += '<div style="cursor:pointer;position:absolute;left:'+(this.volume_bar_length-7)+'px;top:3px;border-bottom:8px solid gray;border-left:8px solid #fff;border-right:8px solid #fff;display:none;" id="'+this.volume_cursor_id()+'"><!-- --></div>';
    value += '</div>';
    value += '</td></tr></table>';
    return value;
  },

  html_error: function()
  {
     var value = '<div class="mb20"><em>��� ������������� ���������� ���������� ���� ������������������ � ���� <strong><a href="'+game_linker.game_domain()+'">'+game_linker.game_name()+'</a></strong></em></div>';
     return value;
  },

  play: function(emit)
  {
    if (this.paused)
    {
      $(this.play_img_id()).src="/i/comm/pause.png";
      this.sound.resume();
      this.paused = false;
      if (emit && this.on_play) this.on_play();
    }
    else if (this.playing)
    {
      $(this.play_img_id()).src="/i/comm/play.png";
      this.sound.pause();
      this.paused = true;
      if (emit && this.on_pause) this.on_pause();
    }
    else
    {
      $(this.play_img_id()).src="/i/comm/pause.png";
      this.sound.play();
      this.playing = true;
      if (emit && this.on_play) this.on_play();
    }
  },

  track_loading: function()
  {
    if ($(this.play_bar_id()).style.display == "none") {
      $(this.play_bar_id()).style.display = "";
      $(this.loaded_bar_id()).style.display = "";
      $(this.play_cursor_id()).style.display = "";
      $(this.volume_bar_id()).style.display = "";
      $(this.volume_cursor_id()).style.display = "";
      $(this.volume_percent_id()).style.display = "";
      $(this.volume_icon_id()).style.display = "";
    }
    $(this.loaded_bar_id()).style.width = Math.floor((this.sound.bytesLoaded/this.sound.bytesTotal)*this.bar_length)+"px";
  },

  track_loaded: function()
  {
    $(this.loaded_bar_id()).style.width = this.bar_length+"px";
    this.update_total_time();
  },

  track_playing: function()
  {
    this.update_position();
  },

  update_total_time: function()
  {
    var seconds = String(Math.floor(this.sound.duration / 1000) % 60);
    var minutes = String(Math.floor(this.sound.duration / (1000*60)));
    if (seconds.length == 1) seconds = "0"+seconds;
    if (minutes.length == 1) minutes = "0"+minutes;
    $(this.total_time_span_id()).update("<strong>("+minutes+":"+seconds+")</strong>");
  },

  update_position: function()
  {
    var loaded_bar_length = Math.floor((this.sound.bytesLoaded/this.sound.bytesTotal)*this.bar_length);
    $(this.play_cursor_id()).style.left = Math.floor((this.sound.position/this.sound.duration)*loaded_bar_length-7)+"px";
    var seconds = String(Math.floor(this.sound.position / 1000) % 60);
    var minutes = String(Math.floor(this.sound.position / (1000*60)));
    if (seconds.length == 1) seconds = "0"+seconds;
    if (minutes.length == 1) minutes = "0"+minutes;
    $(this.time_span_id()).update(minutes+":"+seconds);
  },

  track_finish: function()
  {
    this.playing = false;
    this.paused = false;
    $(this.play_img_id()).src="/i/comm/play.png";
    this.sound.setPosition(0);
    this.update_position();
    if (this.on_finish_play) this.on_finish_play();
  },

  track_dataerror: function()
  {
    this.playing = false;
    this.paused = false;
    $(this.play_img_id()).src="/i/comm/play.png";
    this.update_position();
  },

  track_id3: function()
  {
  },

  drag_play_cursor: function(event)
  {
    var loaded_bar_length = Math.floor((this.sound.bytesLoaded/this.sound.bytesTotal)*this.bar_length);
    var play_bar_offset = $(this.play_bar_id()).cumulativeOffset();
    var new_left = (event.pointerX() + 7 - play_bar_offset.left - this.dragger.drag_delta_x);
    if (new_left < 0) new_left = 0;
    if (new_left > loaded_bar_length) new_left = loaded_bar_length;
    var new_position = Math.floor((new_left / loaded_bar_length)*this.sound.duration);
    if (new_position==0) new_position=1;
    this.sound.setPosition(new_position);
    this.update_position();
  },

  drag_volume_cursor: function(event)
  {
    var volume_bar_offset = $(this.volume_bar_id()).cumulativeOffset();
    var new_left = (event.pointerX() + 7 - volume_bar_offset.left - this.volume_dragger.drag_delta_x);
    if (new_left < 0) new_left = 0;
    if (new_left > this.volume_bar_length) new_left = this.volume_bar_length;
    var new_volume = Math.floor((new_left / this.volume_bar_length)*100);
    this.sound.setVolume(new_volume);
    $(this.volume_percent_id()).update(new_volume+"%");
    $(this.volume_cursor_id()).style.left = Math.floor((new_volume/100)*this.volume_bar_length-7)+"px";
  },

  set_play_cursor: function(event)
  {
    var loaded_bar_length = Math.floor((this.sound.bytesLoaded/this.sound.bytesTotal)*this.bar_length);
    if (loaded_bar_length < 0) loaded_bar_length = 0;
    var play_bar_offset = $(this.play_bar_id()).cumulativeOffset();
    var new_left = (event.pointerX() - play_bar_offset.left);
    if (new_left < 0) new_left = 0;
    if (new_left > loaded_bar_length) new_left = loaded_bar_length;
    var new_position = Math.floor((new_left / loaded_bar_length)*this.sound.duration);
    if (new_position==0) new_position=1;
    this.sound.setPosition(new_position);
    this.update_position();
  },

  set_volume_cursor: function(event)
  {
   var volume_bar_offset = $(this.volume_bar_id()).cumulativeOffset();
    var new_left = (event.pointerX() - volume_bar_offset.left);
    if (new_left < 0) new_left = 0;
    if (new_left > this.volume_bar_length) new_left = this.volume_bar_length;
    var new_volume = Math.floor((new_left / this.volume_bar_length)*100);
    this.sound.setVolume(new_volume);
    $(this.volume_percent_id()).update(new_volume+"%");
    $(this.volume_cursor_id()).style.left = Math.floor((new_volume/100)*this.volume_bar_length-7)+"px";
  }

});

Community.Audio.Track.id = 0;