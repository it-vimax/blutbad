var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Settings = Class.create({
  initialize: function(element,data)
  {
  if (!data) data = {};
  this.id = Community.Settings.id++;
  this.element = $(element);
  if (!this.element) throw new Error("����������� ��������� ��� ��������!");
  this.body = data.body || "";
  this.comm_name = data.comm_name || "";
  this.rd_acc = data.rd_acc || "";
  this.wr_acc = data.wr_acc || "";
  this.confirm = data.confirm || "";
  this.hidden = data.hidden || 0;
  this.style = data.style || "";
  this.display_show();
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  },

  stop: function()
  {
  var change_button_element = $(this.settings_change_button_id());
  if (change_button_element) change_button_element.stopObserving("click");
  var save_button_element = $(this.settings_save_button_id());
  if (save_button_element) save_button_element.stopObserving("click");
  var rd_acc_element = $(this.settings_rd_acc_id());
  if (rd_acc_element)
  {
    rd_acc_element.onchange = null;
    if (rd_acc_element.selectObj) rd_acc_element.selectObj.remove();
  }
  var wr_acc_element = $(this.settings_wr_acc_id());
  if (wr_acc_element)
  {
    wr_acc_element.onchange = null;
    if (rd_acc_element.selectObj) wr_acc_element.selectObj.remove();
  }
  if (this.editor) { this.editor.clean(); this.editor = null; }
  },

  settings_change_button_id: function() { return "Community_Settings_ChangeButton_"+this.id; },
  settings_save_button_id: function() { return "Community_Settings_SaveButton_"+this.id; },
  settings_body_id: function() { return "Community_Settings_Body_"+this.id; },
  settings_comm_name_id: function() { return "Community_Settings_CommName_"+this.id; },
  settings_rd_acc_id: function() { return "Community_Settings_RdAcc_"+this.id; },
  settings_wr_acc_id: function() { return "Community_Settings_WrAcc_"+this.id; },
  settings_confirm_id: function() { return "Community_Settings_Confirm_"+this.id; },
  settings_status_id: function() { return "Community_Settings_Status_"+this.id; },

  display_show: function()
  {
  this.stop();
  this.element.update(this.html_show()).show();
  $(this.settings_change_button_id()).observe("click",this.change.bindAsEventListener(this));
  },

  html_show: function()
  {
    var value = '';
    value += '<div class="border"><table class="info_table"><col style="width:160px;" /><col /><tr><td>';
    if (community.prop.current_user_id != 0) value += '� ����:';
    else value += '� ������:';
    value += '</td><td class="light"';
    if (this.style) value += ' style="'+this.style+'"';
    value += '>' + community.convert_smiles(this.body) + '</td>';
    value += '<tr><td>�������� ����������:</td><td class="light">' + this.comm_name + '</td></tr>';
    if (!this.hidden)
    {
      value += '<tr><td>����� ��������:</td><td class="light">' + community.acc_title.get(this.rd_acc) + '</td></tr>';
      value += '<tr><td>����� ������:</td><td class="light">' + community.wr_acc_title.get(this.wr_acc) + '</td></tr>';
    }
    if (community.prop.current_user_id != 0)
    {
      value += '<tr><td>��������� ������������� ��� ������ � ������:</td><td class="light">';
    }
    else
    {
      value += '<tr><td>��������� ������������� ��� ����������:</td><td class="light">';
    }
    if (this.confirm != 0) value += '��';
    else value += '���';
    value += '</td></tr></table></div>';
    value += '<a class="button clear" id="' + this.settings_change_button_id() + '">��������</a>';
    return value;
  },

  change: function()
  {
  this.stop();
  this.element.update(this.html_change());
  this.editor = new Community.HTMLEditor(this.settings_body_id());
  if (!this.hidden)
  {
	makeSelectBox("#"+this.settings_rd_acc_id());
   	makeSelectBox("#"+this.settings_wr_acc_id());
    $(this.settings_rd_acc_id()).onchange = community.update_wr_acc_select.bind(community,this.settings_rd_acc_id(),this.settings_wr_acc_id());
  }
  $(this.settings_save_button_id()).observe("click",this.save.bindAsEventListener(this));
  jQuery('select').styler();
  },

  html_change: function()
  {
    var value = '';
    value += '<div class="border"><table class="info_table"><tr><td>';
    if (community.prop.current_user_id != 0) value += '� ����:';
    else value += '� ������:';
    value += '</td><td><textarea style="width:90%;height:300px;" id="'+this.settings_body_id()+'">'+this.body+'</textarea></td></tr>';
    value += '<tr><td>�������� ����������:</td><td><div class="oh"><input class="inp_text" type="text" id="'+this.settings_comm_name_id()+'" value="'+this.comm_name+'" style="width:385px;" /></div></td></tr>';
    if (!this.hidden)
    {
      value += '<tr><td>����� ��������:</td><td><div class="por" style="z-index:3000;"><select id="' + this.settings_rd_acc_id() + '" style="width: 160px;">';
      var t_this = this;
      community.acc_title.each(function(pair)
            {
        value += '<option value="' + pair.key + '"';
        if (pair.key == t_this.rd_acc) value += ' selected="selected"';
        value += '>' + pair.value + '</option>';
            }
          );
      value += '</select></div></td></tr>';
      value += '<tr><td>����� ������:</td><td><div class="por"><select id="' + this.settings_wr_acc_id() + '" style="width: 160px;">';
      community.wr_acc_title.each(function(pair)
            {
        value += '<option value="' + pair.key + '"';
        if (pair.key == t_this.wr_acc) value += ' selected="selected"';
        value += '>' + pair.value + '</option>';
            }
           );
      value += '</select></div></td>';
    }
    if (community.prop.current_user_id != 0)
    {
        value += '<tr><td>��������� ������������� ��� ������ � ������:</td><td><input type="checkbox" id="' + this.settings_confirm_id() + '"';
    }
    else
    {
      value += '<tr><td>��������� ������������� ��� ����������:</td><td><input type="checkbox" id="' + this.settings_confirm_id() + '"';
    }
    if (this.confirm != 0) value += ' checked="checked"';
    value += ' /></td></tr>';
    value += '<tr><td>������:</td><td id="' + this.settings_status_id() + '">�� �����������</td></tr>';
    value += '</table></div>';
    value += '<a class="clear button" id="' + this.settings_save_button_id() + '">���������</a>';
    return value;
  },

  save: function()
  {
  var params = {cmd:"save.settings",id:community.prop.current_id,
          body:tinyMCE.get(this.settings_body_id()).getContent(),
          comm_name:$F(this.settings_comm_name_id()),confirm:$F(this.settings_confirm_id()),
          check:community.prop.check
         };
  if ($(this.settings_rd_acc_id())) params.rd_acc = $F(this.settings_rd_acc_id());
  if ($(this.settings_wr_acc_id())) params.wr_acc = $F(this.settings_wr_acc_id());
  new Ajax.Request(community.url.info_ajax,
      {
        parameters: params,
        onSuccess: this.save_success.bind(this),
        onFailure: community.ajax_failure.bind(community)

      });
  $(this.settings_status_id()).update("�����������...");
  },

  save_success: function(transport)
  {
  var result=transport.responseText.split(" ");
  if (result[0] != "OK") { alert("������ ���������� ��������: "+transport.responseText); return; }
  result.splice(0,1);
  var user_info = result.join(" ").evalJSON(true);

  this.body = user_info.body || "";
  this.comm_name = user_info.comm_name || "";
  this.rd_acc = user_info.rd_acc || "";
  this.wr_acc = user_info.wr_acc || "";
  this.confirm = user_info.confirm || "";
  this.display_show();
  }
});

Community.Settings.id = 0;

//-----------------------------------------------------------------------------------------

Community.Settings.Editor = Class.create({
  initialize: function(element,data)
  {
  if (!data) data = {};
  this.id = Community.Settings.id++;
  this.element = $(element);
  if (!this.element) throw new Error("����������� ��������� ��� ��������!");
  this.font = data.font || "";
  this.fontsize = data.fontsize || "";
  this.fontcolor = data.fontcolor || "";
  this.display_show();
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  },

  stop: function()
  {
  var change_button_element = $(this.settings_change_button_id());
  if (change_button_element) change_button_element.stopObserving("click");
  var save_button_element = $(this.settings_save_button_id());
  if (save_button_element) save_button_element.stopObserving("click");
  var fontcolor_element = $(this.settings_fontcolor_id());
  if (fontcolor_element) fontcolor_element.stopObserving("keyup");
  var fontelement = $(this.settings_font_id());
  if (fontelement)
  {
    fontelement.onchange = null;
    fontelement.selectObj.remove();
  }
  var fontsizeelement = $(this.settings_fontsize_id());
  if (fontsizeelement)
  {
    fontsizeelement.onchange = null;
    fontsizeelement.selectObj.remove();
  }
  },

  settings_change_button_id: function() { return "Community_Settings_ChangeButton_"+this.id; },
  settings_save_button_id: function() { return "Community_Settings_SaveButton_"+this.id; },
  settings_status_id: function() { return "Community_Settings_Status_"+this.id; },
  settings_font_id: function() { return "Community_Settings_Font_"+this.id; },
  settings_fontsize_id: function() { return "Community_Settings_FontSize_"+this.id; },
  settings_fontcolor_id: function() { return "Community_Settings_FontColor_"+this.id; },
  settings_testtext_id: function() { return "Community_Settings_TestText_"+this.id; },
  selectcolor_element_id: function() { return "Community_SelectColor_Element_"+this.id; },

  display_show: function()
  {
  //this.stop();
  this.element.update(this.html_show()).show();
  $(this.settings_change_button_id()).observe("click",this.change.bindAsEventListener(this));
  },

  html_show: function()
  {
    var value = '';
    value += '<div class="border"><table class="info_table"><col style="width:260px;" /><col /><tr><td>';
    value += '����� ��-���������:</td><td>';
    if (Number(this.font)) value += community.editor_fonts_names[this.font];
    else value += '���';
    value += '</td></tr><tr><td>';
    value += '������ ������ ��-���������:</td><td>';
    if (Number(this.fontsize)) value += community.editor_fontsizes_names[this.fontsize];
    else value += '���';
    value += '</td></tr><tr><td>';
    value += '���� ������ ��-���������:</td><td>';
    if (this.fontcolor) value += this.fontcolor;
    else value += '���';
    value += '</td></tr><tr><td>';
    value += '������ ������:</td><td>';
    if (this.font) value += '<span style="font-family: '+community.editor_fonts[this.font]+';">';
    if (this.fontsize) value += '<span style="font-size: '+community.editor_fontsizes[this.fontsize]+';">';
    if (this.fontcolor) value += '<span style="color: '+this.fontcolor+';">';
    value += "��� �������� �����";
    if (this.font) value += '</span>';
    if (this.fontsize) value += '</span>';
    if (this.fontcolor) value += '</span>';
    value += '</td></tr></table></div>';
    value += '<a class="button clear" id="' + this.settings_change_button_id() + '">��������</a>';
    return value;
  },

  change: function()
  {
  this.stop();
  this.element.update(this.html_change());
  makeSelectBox("#"+this.settings_font_id());
  makeSelectBox("#"+this.settings_fontsize_id());
  $(this.settings_font_id()).onchange=this.onchange.bindAsEventListener(this);
  $(this.settings_fontsize_id()).onchange=this.onchange.bindAsEventListener(this);
  $(this.settings_fontcolor_id()).observe("keyup",this.onchange.bindAsEventListener(this));
  $(this.settings_save_button_id()).observe("click",this.save.bindAsEventListener(this));
  this.colorpicker = jQuery.farbtastic('#'+this.selectcolor_element_id(),this.onchange_color.bind(this))
                     .setColor(this.fontcolor);
  jQuery('select').styler();
  },

  onchange_color: function(color)
  {
    $(this.settings_fontcolor_id()).value = color;
    $(this.settings_testtext_id()).style.color = color;
  },

  onchange: function(event)
  {
    var font = $F(this.settings_font_id());
    var fontsize = $F(this.settings_fontsize_id());
    var fontcolor = $F(this.settings_fontcolor_id());
    if (Number(font)) $(this.settings_testtext_id()).style.fontFamily = community.editor_fonts[font];
    else $(this.settings_testtext_id()).style.fontFamily = '';
    if (Number(fontsize)) $(this.settings_testtext_id()).style.fontSize = community.editor_fontsizes[fontsize];
    else $(this.settings_testtext_id()).style.fontSize = '';
    if (fontcolor) {
      if (RegExp('^\#[a-f0-9]{6}$').test(fontcolor)) {
        $(this.settings_testtext_id()).style.color = fontcolor;
        if (this.colorpicker) this.colorpicker.setColor(fontcolor);
      }
    }
    else $(this.settings_testtext_id()).style.color = '';
  },

  html_change: function()
  {
    var value = '';
    value += '<div class="border"><table class="info_table"><col style="width:260px;" /><col /><col /><tr><td>';
    value += '����� ��-���������:</td><td>';
    value += '<div class="por" style="z-index:3000;"><select id="'+this.settings_font_id()+'" style="width: 160px;">';
    for (var i=0;i<community.editor_fonts.length;i++) {
      value += '<option value="'+i+'"';
      if (this.font == i) value += ' selected="selected"';
      value += '>';
      if (!community.editor_fonts[i]) value += '--���--';
      else value += community.editor_fonts_names[i];
      value += '</option>';
    }
    value += '</select></div></td><td rowspan="6"><div id="'+this.selectcolor_element_id()+'"></div></td></tr><tr><td>';

    value += '������ ������ ��-���������:</td><td>';
    value += '<div class="por"><select id="'+this.settings_fontsize_id()+'" style="width: 160px;">';
    for (var i=0;i<community.editor_fontsizes.length;i++) {
      value += '<option value="'+i+'"';
      if (this.fontsize == i) value += ' selected="selected"';
      value += '>';
      if (!community.editor_fontsizes[i]) value += '--���--';
      else value += community.editor_fontsizes_names[i];
      value += '</option>';
    }
    value += '</select></div></td></tr><tr><td>';

    value += '���� ������ �� ���������:</td><td>';
    value += '<input class="inp_text" id="'+this.settings_fontcolor_id()+'" type="text" size="7" maxlength="7" value="'+this.fontcolor+'"></input>';
    value += '</td></tr><tr><td>';
    value += '������ ������:</td><td id="'+this.settings_testtext_id()+'" style="';
    if (Number(this.font)) value += 'font-family: '+community.editor_fonts[this.font]+';';
    if (Number(this.fontsize)) value += 'font-size: '+community.editor_fontsizes[this.fontsize]+';';
    if (this.fontcolor) value += 'color: '+this.fontcolor+';';
    value += '">';
    value += "��� �������� �����";
    value += '</td></tr>';
    value += '<tr><td>������:</td><td id="' + this.settings_status_id() + '">�� �����������</td></tr>';
    value += '</table></div>';
    value += '<a class="clear button" id="' + this.settings_save_button_id() + '">���������</a>';

    return value;
  },

  save: function()
  {
  var params = {cmd:"save.editor_settings",id:community.prop.current_id,
          check:community.prop.check
         };
  params.font = $F(this.settings_font_id());
  params.fontsize = $F(this.settings_fontsize_id());
  params.fontcolor = $F(this.settings_fontcolor_id());
  new Ajax.Request(community.url.info_ajax,
      {
        parameters: params,
        onSuccess: this.save_success.bind(this),
        onFailure: community.ajax_failure.bind(community)
      });
  $(this.settings_status_id()).update("�����������...");
  },

  save_success: function(transport)
  {
  var result=transport.responseText.split(" ");
  if (result[0] != "OK") { alert("������ ���������� ��������: "+transport.responseText); return; }
  result.splice(0,1);
  var editor_info = result.join(" ").evalJSON(true);
  this.font = editor_info.font;
  this.fontsize = editor_info.fontsize;
  this.fontcolor = editor_info.fontcolor;

  this.display_show();
  }

});