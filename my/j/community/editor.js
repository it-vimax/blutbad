var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.HTMLEditor = Class.create({

  initialize: function(ed_id,options)
  {
    if (!options) options = {};
    this.id = Community.HTMLEditor.id++;
    this.ed_id = ed_id;
    Community.HTMLEditor.instances[ed_id] = this;
    this.bookmark = null;
    this.insertphoto = null;
    this.smiles_window = null;
    this.extended = options.extended || false;
    setTimeout(tinyMCE.execCommand.bind(tinyMCE,"mceAddControl", true, ed_id),0);
    community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
    this.stop();
    if (this.insertphoto) { this.insertphoto.clean(); this.insertphoto = null; }
    if (this.smiles_window) this.smiles_window.clean();
    this.smiles_window = null;
  },

  stop: function()
  {
    var t_this = this;
    try { t_this.remove(); }
    catch(e) {}
    if (this.insertphoto) this.insertphoto.stop();

    var smiles_single_element = $(this.editor_smiles_single_id());
    if (smiles_single_element) smiles_single_element.stopObserving('click');
    var smiles_group_element = $(this.editor_smiles_group_id());
    if (smiles_group_element) smiles_group_element.stopObserving('click');
    for (var i=0;i<game_linker.smiles.length;i++)
    {
      var smiles_smile_element = $(this.editor_smiles_smile_id(game_linker.smiles[i]));
      if (smiles_smile_element) smiles_smile_element.stopObserving('click');
    }
    if (this.smiles_window) this.smiles_window.clear();
  },

  remove: function()
  {
    if (this.ed) tinyMCE.execCommand("mceRemoveControl", true, this.ed_id);
    this.ed = null;
  },

  setup: function(ed)
  {
    this.ed=ed;
    ed.onNodeChange.add(this.set_bookmark.bind(this));
    ed.onChange.add(this.set_bookmark.bind(this));
    ed.addButton('blockquote',
      {
        title: '���������� ����������',
        onclick: this.quote_selected.bind(this)
      });

    ed.addButton('image',
      {
        title: '��������/������������� �����������',
        onclick: this.insert_photo.bind(this)
      });
    ed.addButton('smiles',
      {
        title: '�������� �������',
        onclick: this.insert_smile.bind(this),
        image: '/i/comm/smiles-button.gif'
      });

    ed.addButton('translit',
      {
        title: '��������',
        onclick: this.convert_translit.bind(this),
        image: '/i/comm/translit-button.png'
      });

    if (Community.HTMLEditor.full) {
      ed.addButton('pagebreak',
        {
          title: '������ ���������� �����',
          onclick: this.hidden_text.bind(this)
        });
    }
  },

  get_value: function()
  {
    if (this.ed) return this.ed.getContent();
    else return "";
  },

  set_value: function(value)
  {
    if (this.ed) this.ed.setContent(value);
  },

  //--------------------Insert quotes--------------------------

  set_bookmark: function()
  {
    this.bookmark = this.ed.selection.getBookmark();
  },

  quote_selected: function()
  {
    var text = document.selection ? document.selection.createRange().text : document.getSelection();
    this.ed.focus();
    if (this.bookmark) this.ed.selection.moveToBookmark(this.bookmark);
    this.ed.execCommand('mceInsertContent',false,'<em>&lt; '+text.gsub(/\r?\n/,'<br />&lt; ')+'<em><br />');
  },

//-----------------------------------------------------------------
  hidden_text: function()
  {
    var content = this.ed.selection.getContent();
    content = content.replace(/<cut>/g,'');
    content = content.replace(/<\/cut>/g,'');
    this.ed.execCommand('mceInsertRawHTML', false, '<cut>'+content+'</cut>');
  },

//-------------------------------------------------------------------------------------------------

  insert_photo: function()
  {
    if (!this.insertphoto) this.insertphoto = new Community.InsertPhoto({on_insert: this.insert_photo_ok.bind(this) });
    this.insertphoto.show();
  },

  insert_photo_ok: function(photo)
  {
    var value = '';
    if (photo.link) value += '<a href="'+photo.link+'" target="_blank">';
    value += '<img src="'+photo.src+'"';
    if (photo.title) value += ' title="'+photo.title+'" alt="'+photo.title+'"';
    if (photo.float == "right") {
		value += ' style="float: left;"';
	}
	else if (photo.float == "left") {
		value += ' style="float: right;"';
	}
	value += ' />';
    if (photo.link) value += '</a>';
    this.ed.execCommand('mceInsertContent',false,value);
  },

//--------------------------------------------------------------------------------------------------
  insert_smile: function()
  {
    if (this.smiles_window) { this.smiles_window.show(); return; }
    this.smiles_window = new Community.Window({
            title:"������� �������� � ���������",
            content:this.smiles_window_content()
            });
    $(this.editor_smiles_single_id()).observe("click",this.smiles_single_select.bindAsEventListener(this));
    $(this.editor_smiles_group_id()).observe("click",this.smiles_group_select.bindAsEventListener(this));
    for (var i=0;i<game_linker.smiles.length;i++)
    {
      $(this.editor_smiles_smile_id(game_linker.smiles[i])).observe("click",this.smiles_smile_select.bindAsEventListener(this,game_linker.smiles[i]));
    }
    this.smiles_window.show();
  },

  editor_smiles_single_id: function() { return "Community_HTMLEditor_Smiles_Single_"+this.id; },
  editor_smiles_group_id: function() { return "Community_HTMLEditor_Smiles_Group_"+this.id; },
  editor_smiles_single_panel_id: function() { return "Community_HTMLEditor_Smiles_Single_Panel_"+this.id; },
  editor_smiles_group_panel_id: function() { return "Community_HTMLEditor_Smiles_Group_Panel_"+this.id; },
  editor_smiles_smile_id: function(id) { return "Community_HTMLEditor_Smiles_Smile_"+id+"_"+this.id; },

  smiles_single_select: function(event)
  {
    $(this.editor_smiles_group_panel_id()).style.display = "none";
    $(this.editor_smiles_single_panel_id()).style.display = "inline";
  },

  smiles_group_select: function(event)
  {
    $(this.editor_smiles_single_panel_id()).style.display = "none";
    $(this.editor_smiles_group_panel_id()).style.display = "inline";
  },

  smiles_smile_select: function(event,smile)
  {
    this.ed.execCommand('mceInsertContent',false,':'+smile+':');
    this.smiles_window.hide();
  },

  smiles_window_content: function()
  {
    var value = '<table style="width:500px;">';
    value += '<tr><td id="'+this.editor_smiles_single_id()+'" align="center" style="background-color: gray; font-size: 8pt; cursor:pointer;"><b>���������</b></td>';
    value += '<td id="'+this.editor_smiles_group_id()+'" align="center" style="background-color: gray; font-size: 8pt; cursor:pointer;"><b>���������</b></td>';
    value += '</tr></table>';
    value += '<table style="width:500px;cursor:pointer;"><tr><td>';
    value += '<div id="'+this.editor_smiles_single_panel_id()+'" style="display: inline;">';

    for (var i=0;i<game_linker.smiles.length;i++)
    {
      if (game_linker.smiles_page[i] != 1) continue;
      value += '<img id="'+this.editor_smiles_smile_id(game_linker.smiles[i])+'" src="'+game_linker.smiles_url()+game_linker.smiles[i]+'.gif" width="';
      value += game_linker.smiles_x[i]+'" height="'+game_linker.smiles_y[i]+'" border="0" alt=":';
      value += game_linker.smiles[i]+':" title=":';
      value += game_linker.smiles[i]+':" onmouseover="this.style.backgroundColor=\'gray\'"';
      value += ' onmouseout="this.style.backgroundColor=\'\'" /> ';
    }

    value += '</div><div id="'+this.editor_smiles_group_panel_id()+'" style="display: none;">';

    for (var i=0;i<game_linker.smiles.length;i++)
    {
      if (game_linker.smiles_page[i] != 2) continue;
      value += '<img id="'+this.editor_smiles_smile_id(game_linker.smiles[i])+'" src="'+game_linker.smiles_url()+game_linker.smiles[i]+'.gif" width="';
      value += game_linker.smiles_x[i]+'" height="'+game_linker.smiles_y[i]+'" border="0" alt=":';
      value += game_linker.smiles[i]+':" onmouseover="this.style.backgroundColor=\'gray\'"';
      value += ' onmouseout="this.style.backgroundColor=\'\'" /> ';
    }
    value += '</div></td></tr></table>';
    return value;
  },

//-------------------------------------------------------------------------------------------------

  convert_translit: function()
  {
    var content = this.ed.getContent();
    content = community.translit(content);
    this.ed.setContent(content);
  }

});

Community.HTMLEditor.id = 0;

Community.HTMLEditor.instances = {};

Community.HTMLEditor.remove = function(ed_id)
{
  var instance = Community.HTMLEditor.instances[ed_id];
  if (!instance) return;
  instance.remove();
  delete Community.HTMLEditor.instances[ed_id];
}

Community.HTMLEditor.get = function(ed_id)
{
  var instance = Community.HTMLEditor.instances[ed_id];
  if (!instance) return null;
  return instance.ed;
}

Community.HTMLEditor.init_basic = function()
{
  tinyMCE.init({
    mode : "none",
    theme : "advanced",
    language : 'ru',
    theme_advanced_buttons1 : "fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,forecolor,|",
    theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,|,link,unlink,|,blockquote,image,smiles,|,undo,redo,|,translit,|,code,|",
    theme_advanced_buttons3 : "",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    font_size_style_values : "9px,11px,13px,15px,17px,19px,21px",
	theme_advanced_font_sizes : "1 (9px)=9px,2 (11px)=11px,3 (13px)=13px,4 (15px)=15px,5 (17px)=17px,6 (19px)=19px,7 (21 px)=21px",
    content_css: community.url.editor_css+"?fontcolor="+encodeURIComponent(community.prop.def_fontcolor)+"&fontsize="+encodeURIComponent(community.editor_fontsizes[community.prop.def_fontsize])+"&font="+encodeURIComponent(community.editor_fonts[community.prop.def_font])+"&version=6",
    extended_valid_elements : '@[id|class|style|title],img[alt|src|align|width|height|obj|param|embed|style],span[style],em/i,strong/b,br,p[style],a[href|target=_blank],cut,object[width|height|type|data],param[name|value]',
    invalid_elements:'script,td,tr,tbody,ol,li,ul,sub,sup,table,thead,tfoot,th,caption,pre,address,h1,h2,h3,h4,h5,h6,hr,dd,dl,dt,cite,abbr,acronym,del,code,div',
    setup: function(ed)
           {
      var instance = Community.HTMLEditor.instances[ed.id];
      if (instance) instance.setup(ed);
           }
      });
	Community.HTMLEditor.full = false;

}

Community.HTMLEditor.init_full = function(no_cut)
{
  tinyMCE.init({
    mode : "none",
    theme : "advanced",
    language : 'ru',
    theme_advanced_buttons1 : "fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,forecolor,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,|,",
    theme_advanced_buttons2 : "blockquote,image,media,smiles,"+(no_cut ? "" : "pagebreak,")+"|,cut,copy,paste,|,undo,redo,|,translit,|,fullscreen,|,code,|",
    theme_advanced_buttons3 : "",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    font_size_style_values : "9px,11px,13px,15px,17px,19px,21px",
	theme_advanced_font_sizes : "1 (9px)=9px,2 (11px)=11px,3 (13px)=13px,4 (15px)=15px,5 (17px)=17px,6 (19px)=19px,7 (21 px)=21px",
    theme_advanced_default_background_color : "transparent",
	plugins: "media,fullscreen",
    content_css: community.url.editor_css+"?fontcolor="+encodeURIComponent(community.prop.def_fontcolor)+"&fontsize="+encodeURIComponent(community.editor_fontsizes[community.prop.def_fontsize])+"&font="+encodeURIComponent(community.editor_fonts[community.prop.def_font])+"&version=6",
    extended_valid_elements : '@[id|class|style|title],img[alt|src|align|width|height|obj|param|embed|style],span[style],em/i,strong/b,br,p[style],a[href|target=_blank],cut,object[width|height|type|data],param[name|value]',
    invalid_elements:'script,td,tr,tbody,ol,li,ul,sub,sup,table,thead,tfoot,th,caption,pre,address,h1,h2,h3,h4,h5,h6,hr,dd,dl,dt,cite,abbr,acronym,del,code,div',
    custom_elements: 'cut',
    setup: function(ed)
           {
      var instance = Community.HTMLEditor.instances[ed.id];
      if (instance) instance.setup(ed);
           }
      });
	Community.HTMLEditor.full = true;
}