var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.UserField = Class.create({
  initialize: function(element,init,name,options)
  {
  if (!options) options = {};
  this.id = Community.UserField.id++;
  this.disable_name = options.disable_name || false;
  this.init = init || '';
  if (!this.disable_name) this.name = name || 'user';
  this.input_id = element+"_Input";
  this.element = $(element);
  if (!this.element) throw new Error("��������� ��� ���� ����� ������������ �� ����������!");
  this.element.update(this.html());
  this.autocompleter = new Community.AutoComplete(this.field_input_id(),new Community.AutoComplete.Users());
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.element = null;
  if (this.autocompleter) this.autocompleter.clean();
  this.autocompleter = null;
  },

  stop: function()
  {
  if (this.autocompleter) this.autocompleter.stop();
  },

  field_input_id: function() { return "Community_UserField_Input_"+this.id; },

  html: function()
  {
  var value='<input style="width:170px;" class="inp_text fn" type="text" id="'+this.field_input_id()+'" value="'+this.init+'" autocomplete="off" />';
  value += '<input type="hidden" id="'+this.input_id+'"';
  if (!this.disable_name) value += ' name="'+this.name+'"';
  value += ' value="'+this.init+'" />';
  return value;
  },

  change: function()
  {
  $(this.input_id).value = $F(this.field_input_id());
  },

  get_user: function()
  {
  return $F(this.field_input_id());
  }
});

Community.UserField.id = 0;

//-----------------------------------------------------------------------------------------------

Community.GroupField = Class.create({
  initialize: function(element,init,name,options)
  {
  if (!options) options = {};
  this.id = Community.GroupField.id++;
  this.disable_name = options.disable_name || false;
  this.init = init || '';
  if (!this.disable_name) this.name = name || 'user';
  this.input_id = element+"_Input";
  this.element = $(element);
  if (!this.element) throw new Error("��������� ��� ���� ����� ������ �� ����������!");
  this.element.update(this.html());
  this.autocompleter = new Community.AutoComplete(this.field_input_id(),new Community.AutoComplete.Groups());
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.element = null;
  if (this.autocompleter) this.autocompleter.clean();
  this.autocompleter = null;
  },

  stop: function()
  {
  if (this.autocompleter) this.autocompleter.stop();
  },

  field_input_id: function() { return "Community_GroupField_Input_"+this.id; },

  html: function()
  {
  var value='<input class="inp_text" type="text" id="'+this.field_input_id()+'" value="'+this.init.replace("$","")+'" autocomplete="off" />';
  value += '<input type="hidden" id="'+this.input_id+'"';
  if (!this.disable_name) value += ' name="'+this.name+'"';
  value += ' value="'+this.init+'" />';
  return value;
  },

  change: function()
  {
  var new_value = $F(this.field_input_id());
  if (new_value) $(this.input_id).value = new_value+"$";
  else $(this.input_id).value = "";
  },

  get_group: function()
  {
  var new_value = $F(this.field_input_id());
  if (new_value) return new_value+"$";
  else return "";
  }
});

Community.GroupField.id = 0;

//----------------------------------------------------------------------------------------------
Community.UserGroupList = Class.create({
  initialize: function(element,options)
  {
  this.id = Community.UserGroupList.id++;
  if (!options) options = {};
  this.element = $(element);
  if (!this.element) throw new Error("����������� ��������� ��� ������ ������������� � �����!");
  this.users = new Array();
  this.user_field = null;
  this.group_field = null;
  this.display();
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  if (this.user_field) this.user_field.clean();
  if (this.group_field) this.group_field.clean();
  this.element = null;
  },

  stop: function()
  {
  if (this.user_field) this.user_field.stop();
  if (this.group_field) this.group_field.stop();
  var add_user_element = $(this.add_user_button_id());
  if (add_user_element) add_user_element.stopObserving("click");
  var add_group_element = $(this.add_group_button_id());
  if (add_group_element) add_group_element.stopObserving("click");
  var t_this =this;
  this.users.each(function(user)
    {
      var del_user_element = $(t_this.delete_user_button_id(user.title));
      if (del_user_element) del_user_element.stopObserving("click");
    });
  },

  user_field_id: function() { return "Community_UserGroupList_User_Field_"+this.id; },
  group_field_id: function() { return "Community_UserGroupList_Group_Field_"+this.id; },
  add_user_button_id: function() { return "Community_UserGroupList_Add_User_Button_"+this.id; },
  add_group_button_id: function() { return "Community_UserGroupList_Add_Group_Button_"+this.id; },
  delete_user_button_id: function(title) { return "Community_UserGroupList_Delete_User_Button_"+title+"_"+this.id; },

  is_user_in_list: function(user)
  {
  var found = false;
  this.users.each(function(user)
    {
      if (user.user == user) { found = true; throw $break; }
    });
  return found;
  },

  display: function()
  {
  this.element.update(this.html());
  this.user_field = new Community.UserField(this.user_field_id(),null,null,{disable_name:true});
  this.group_field = new Community.GroupField(this.group_field_id(),null,null,{disable_name:true});
  $(this.add_user_button_id()).observe("click",this.add_user.bindAsEventListener(this));
  $(this.add_group_button_id()).observe("click",this.add_group.bindAsEventListener(this));

  var t_this =this;
  this.users.each(function(user)
    {
      $(t_this.delete_user_button_id(user.title)).observe("click",t_this.delete_user.bindAsEventListener(t_this,user.title));
    });
  },

  add_user: function(event)
  {
  var new_user = this.user_field.get_user();
  if (this.is_user_in_list(new_user)) { alert("���� �������� ��� �������� � ������!"); return; }
  this.stop();
  this.users.push({title:new_user});
  this.display();
  },

  add_group: function(event)
  {
  var new_group = this.user_group.get_group();
  if (this.is_user_in_list(new_group)) { alert("��� ������ ��� ��������� � ������!"); return; }
  this.stop();
  this.users.push({title:new_group});
  this.display();
  },

  delete_user: function(event,title)
  {
  var t_this = this;
  this.stop();
  this.users.each(function(user,index)
    {
      if (user.title == title) { t_this.users.splice(index,1); throw $break; }
    });
  this.display();
  },

  html: function()
  {
  var value='<span id="'+this.user_field_id()+'"></span>&nbspd<input type="button" id="'+this.add_user_button_id()+'" value="�������� ���������" /><br />';
  value += '<span id="'+this.group_field_id()+'"></span>&nbsp<input type="button" id="'+this.add_group_button_id()+'" value="�������� ������" /><br />';
  var t_this = this;
  this.users.each(function(user)
    {
      value += community.user_title(user.title)+'<a href="#" onclick="return false;"><img src="'+community.image.del+'" alt="�������" title="�������" width="11" height="11" id="'+t_this.delete_user_button_id(user.title)+'" /></a><br />';
    });
  return value;
  },

  get_users: function()
  {
  return this.users.pluck("title").join(",");
  }

});

Community.UserGroupList.id = 0;