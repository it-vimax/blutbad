var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Posts = Class.create({
  initialize: function(container,options)
  {
  if (!options) options = {};
  this.id = Community.Posts.id++;
  this.disable_tags = options.disable_tags ? true :false;
  this.disable_favorite = options.disable_favorite ? true :false;
  this.disable_sign = options.disable_sign ? true :false;
  this.disable_censure = options.disable_censure ? true :false;
  this.can_edit = options.can_edit || false;
  this.can_delete = options.can_delete || false;
  this.can_hide = options.can_hide || false;
  this.can_check = options.can_check || false;
  this.can_fix = options.can_fix || false;
  this.on_edit = options.on_edit || null;
  this.on_delete = options.on_delete || null;
  this.on_before_edit = options.on_before_edit || null;
  this.on_before_delete = options.on_before_delete || null;
  this.on_hide = options.on_hide || null;
  this.on_before_hide = options.on_before_hide || null;
  this.on_unhide = options.on_unhide || null;
  this.on_before_unhide = options.on_before_unhide || null;
  this.on_fix = options.on_fix || null;
  this.on_before_fix = options.on_before_fix || null;
  this.on_unfix = options.on_unfix || null;
  this.on_before_unfix = options.on_before_unfix || null;
  this.edit_window = null;
  this.edit_editor = null;
  this.edit_insertsurvey = null;
  this.edit_id = 0;
  this.container = $(container);
  if (!this.container) throw new Error("����������� ��������� ��� �������!");
  this.posts = new Hash();

  this.group_access = options.group_access || null;
  if (this.group_access && $(this.group_access)) {
    $(this.group_access).observe("click",this.show_group_access.bind(this));
  }
  this.display();
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.container = null;
  if (this.edit_editor) this.edit_editor.clean();
  this.edit_editor = null;
  if (this.edit_insertsurvey) this.edit_insertsurvey.clean();
  this.edit_insertsurvey = null;
  if (this.edit_window) this.edit_window.clean();
  this.edit_window = null;
  if (this.group_access_window) this.group_access_window.clean();
  this.group_access_window = null;
  },

  stop: function()
  {
  if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");

  if (this.edit_editor) this.edit_editor.stop();
  if (this.edit_insertsurvey) this.edit_insertsurvey.stop();
  if (this.edit_window) this.edit_window.clear();
  if (this.group_access_window) this.group_access_window.clear();
  var t_this = this;
  this.posts.each(function(pair)
  {
    t_this.stop_element(pair.value);
  });
  if (this.group_access && $(this.group_access)) $(this.group_access).stopObserving("click");
  },


  stop_element: function(post_info)
  {
  var delete_element = $(this.post_delete_id(post_info.id));
  if (delete_element) delete_element.stopObserving("click");
  var edit_element = $(this.post_edit_id(post_info.id));
  if (edit_element) edit_element.stopObserving("click");
  var hide_element = $(this.post_hide_id(post_info.id));
  if (hide_element) hide_element.stopObserving("click");
  var fix_element = $(this.post_fix_id(post_info.id));
  if (fix_element) fix_element.stopObserving("click");
  var body_all_element = $(this.post_body_all_id(post_info.id));
  if (body_all_element) body_all_element.stopObserving("click");
  if (post_info.tags) post_info.tags.clean();
  if (post_info.favorite) post_info.favorite.clean();
  if (post_info.sign) post_info.sign.clean();
  if (post_info.censure) post_info.censure.clean();
  if (post_info.survey) post_info.survey.clean();
  },

  post_cell_id: function(post_id) { return "Community_Post_Cell_"+this.id+"_"+post_id; },
  post_body_id: function(post_id) { return "Community_Post_Body_"+this.id+"_"+post_id; },
  post_body_all_id: function(post_id) { return "Community_Post_BodyAll_"+this.id+"_"+post_id; },
  post_survey_id: function(post_id) { return "Community_Post_Survey_"+this.id+"_"+post_id; },
  post_edit_id: function(post_id) { return "Community_Post_Edit_"+this.id+"_"+post_id; },
  post_delete_id: function(post_id) { return "Community_Post_Delete_"+this.id+"_"+post_id; },
  post_hide_id: function(post_id) { return "Community_Post_Hide_"+this.id+"_"+post_id; },
  post_fix_id: function(post_id) { return "Community_Post_Fix_"+this.id+"_"+post_id; },
  post_check_id: function(post_id) { return "Community_Post_Check_"+this.id+"_"+post_id; },
  post_censure_id: function(post_id) { return "Community_Post_Censure_"+this.id+"_"+post_id; },
  post_tags_id: function(post_id) { return "Community_Post_Tags_"+this.id+"_"+post_id; },
  post_favorite_id: function(post_id) { return "Community_Post_Favorite_"+this.id+"_"+post_id; },
  post_sign_id: function(post_id) { return "Community_Post_Sign_"+this.id+"_"+post_id; },
  edit_title_id: function() { return "Community_Post_EditTitle_"+this.id; },
  edit_body_id: function() { return "Community_Post_EditBody_"+this.id; },
  edit_rd_acc_id: function() { return "Community_Post_EditRdAcc_"+this.id; },
  edit_wr_acc_id: function() { return "Community_Post_EditWrAcc_"+this.id; },
  edit_status_id: function() { return "Community_Post_EditStatus_"+this.id; },
  edit_insertsurvey_id: function() { return "Community_Post_EditInsertSurvey_"+this.id; },
  edit_insertsurvey_info_id: function() { return "Community_Post_EditInsertSurveyInfo_"+this.id; },
  global_check_id: function() { return "Community_Post_Global_Check_"+this.id; },
  global_check_button_id: function() { return "Community_Post_Global_Check_Button_"+this.id; },
  group_access_rd_acc_id: function() { return "Community_Post_GroupAccessRdAcc_"+this.id; },
  group_access_wr_acc_id: function() { return "Community_Post_GroupAccessWrAcc_"+this.id; },
  group_access_status_id: function() { return "Community_Post_GroupAccessStatus_"+this.id; },

  display: function()
  {
  this.container.update(this.container_html());
  if (this.can_check)
  {
    $(this.global_check_id()).observe("click",this.global_check.bindAsEventListener(this));
    $(this.global_check_button_id()).observe("click",this.check_selected.bindAsEventListener(this));
  }
  },

  global_check: function(event)
  {
  var checked = $(this.global_check_id()).checked;
  var t_this = this;
  this.posts.each(function(pair)
    {
      if ($(t_this.post_check_id(pair.value.id)))
      {
        $(t_this.post_check_id(pair.value.id)).checked = checked;
      }
    });
  },

  check_selected: function(event)
  {
  var ids = new Array();
  var t_this = this;
  this.posts.each(function(pair)
    {
      if ($(t_this.post_check_id(pair.value.id)) &&
          $(t_this.post_check_id(pair.value.id)).checked
         )
      {
        ids.push(pair.value.id);
      }
    });
  if (ids.length)
  {
    document.location.href=community.url.admin_posts+"?cmd=posts.check&id="+ids.join(",")+"&check="+community.prop.check+"&"+Math.random();
  }
  },

  container_html: function()
  {
  var value = '';
  if (this.can_check)
  {
    value += '<input class="button fr" type="button" id="'+this.global_check_button_id()+'" value="��� ���������� ���������" />';
    value += '<input type="checkbox" id="'+this.global_check_id()+'" /><label for="'+this.global_check_id()+'">�������� ���</label><div class="clear pb20"><!-- --></div>';
  }
  return value;
  },

  html: function(post_info)
  {
  var value = '<li class="border PostCell" id="' + this.post_cell_id(post_info.id) + '">';
  if (this.can_check)
  {
    value += '<div class="fr"><input type="checkbox" id="'+this.post_check_id(post_info.id)+'" /><label for="'+this.post_check_id(post_info.id)+'">��������</label></div>';
  }
  value += '<div class="por" style="margin-left: -20px;">'+community.user_title(post_info.author_title,{avatar:1,avsize:1})+'<td>: '+post_info.time_create+'</td></tr></table></div>';
  if (!post_info.root_id || post_info.root_id == 0)
  {
    value += '<h3>';
    if (post_info.type == 1)
    {
      value += '<a class="n_u" href="/blog_comments='+post_info.id+'">';
    }
    else if (post_info.type == 2)
    {
      value += '<a class="n_u" href="/gallery='+post_info.id+'">';
    }
    else if (post_info.type == 3)
    {
      value += '<a class="n_u" href="/photo_comments='+post_info.id+'">';
    }
    else if (post_info.type == 5)
    {
      value += '<a class="n_u" href="'+community.url.tracks+'?where='+post_info.id+'">';
    }
    else if (post_info.type == 6)
    {
      value += '<a class="n_u" href="'+community.url.track_comments+'?where='+post_info.id+'">';
    }

    if (post_info.title) value += post_info.title;
    else value += '<em>(��� ��������)</em>';
    value += '</a>';
    if (post_info.survey_id) value += ' (�����)';
    value += '</h3>';

    if (post_info.mood || post_info.music) value += '<div class="mb10 mt10">';
    if (post_info.mood)
    {
      value += '<p><em><strong>����������:</strong> '+post_info.mood;
      if (post_info.mood_smile) value += ' '+community.get_smile_img(post_info.mood_smile);
      value += '</em></p>';
    }
    if (post_info.music)
    {
      value += '<p><em><strong>�������:</strong> '+post_info.music+'</em></p>';
    }
    if (post_info.mood || post_info.music) value += '</div>';


    value += '<div class="light mb10" style="font-size:11px;"><a class="n_u mr10" href="';
    if (post_info.type == 1)
    {
      value += '/blog_comments='+post_info.id;
    }
    else if (post_info.type == 2)
    {
      value += '/gallery='+post_info.id;
    }
    else if (post_info.type == 3)
    {
      value += '/photo_comments='+post_info.id;
    }
    else if (post_info.type == 5)
    {
      value += community.url.tracks+'?where='+post_info.id;
    }
    else if (post_info.type == 6)
    {
      value += community.url.track_comments+'?where='+post_info.id;
    }
    value += '">';
    if (post_info.type == 1) value += '������������: ';
    else if (post_info.type == 2) value += '����������: ';
    else if (post_info.type == 3) value += '������������: ';
    else if (post_info.type == 5) value += '������������: ';
    else if (post_info.type == 6) value += '������������: ';
    value += post_info.cnt_child;
    if (Number(post_info.count_unread)) value += ' <b>('+post_info.count_unread+')</b>';
    value += '</a>';
    value += '����������: '+post_info.totalviews + '</div>';
  }
  if (post_info.type == 3 && (post_info.root_id == 0 || !post_info.root_id))
  {
    value += '<div class="clear"><a href="#" onclick="return false;"><img src="'+community.url.photo_src+'?size=thmb&photo='+post_info.id+'" /></a></div>';
  }
  value += '<div class="pt5 mb10 clear">';
  if (post_info.type == 1 && post_info.survey_id && !Number(post_info.survey_pos))
  {
    value += '<div class="blog_survey voting" id="'+this.post_survey_id(post_info.id)+'"></div><div class="clear"></div>';
  }
  value += '<div id="'+this.post_body_id(post_info.id)+'"';
  if (post_info.style) value += ' style="'+post_info.style+'"';
  value += '>';
  if (Number(post_info.censored))
  {
    value += '<div><a href="#" style="color:red" onclick="community.show_cut(this);return false;">[������ ����������� �����...]</a><span style="display:none;">';
  }
  if (post_info.type == "1" && post_info.root_id == "0")
  {
     var body = post_info.body.replace(/<\/cut>/g,'</span></div>');
     var is_cutted = false;
     body = body.replace(/<cut>/g,'<div><a href="#" onclick="community.show_cut(this);return false;">[������ ���������...]</a><span style="display:none;">');
     if (body.length == post_info.body.length) { body = community.cut_body(body,500); is_cutted = true; }
     value += community.convert_smiles(body);
     if (is_cutted && body.length != post_info.body.length)
     {
       value += '<p><a href="#" onclick="return false;" id="'+this.post_body_all_id(post_info.id)+'">[������ �����...]</a></p>';
     }
  }
  else if (post_info.type == 1 || (post_info.type == 3 && post_info.root_id && post_info.root_id != 0) || (post_info.type == 6 && post_info.root_id && post_info.root_id != 0))
  {
    value += community.convert_smiles(post_info.body);
  }
  else
  {
    value += post_info.body;
  }
  if (Number(post_info.censored))
  {
    value += '</span></div>';
  }
  value += '</div>';
  if (post_info.type == 1 && post_info.survey_id && Number(post_info.survey_pos))
  {
    value += '<div class="blog_survey voting mt10" id="'+this.post_survey_id(post_info.id)+'"></div><div class="clear"></div>';
  }
  value += '</div>';
  if (post_info.hidden) value += '<p style="color:#ff0000;">��������� ������ �����������</p>';

  if (!this.disable_tags) value=value+'<div class="inner_tags" id="'+this.post_tags_id(post_info.id)+'"></div>';

  if (!this.disable_favorite) value=value+'<ul class="act clear"><li id="'+this.post_favorite_id(post_info.id)+'" style="display:none;"></li>';

  if (!this.disable_sign) value=value+'<li id="'+this.post_sign_id(post_info.id)+'" style="display:none;"></li>';

  if (!this.disable_censure && Number(post_info.can_censure))
  {
    value=value+'<li id="'+this.post_censure_id(post_info.id)+'" style="display:none;"></li>';
  }

  if (this.can_edit && (post_info.can_edit - 0))
  {
    value=value+'<li><a href="#" onclick="return false;" class="pseudolink" id="'+this.post_edit_id(post_info.id)+'">������������� ������</a></li>';
  }
  if (this.can_delete && (post_info.can_delete - 0))
  {
    value=value+'<li><a href="#" onclick="return false;" class="pseudolink" id="'+this.post_delete_id(post_info.id)+'">������� ������</a></li>';
  }
  if (this.can_fix && (post_info.can_edit - 0))
  {
    value=value+'<li><a href="#" onclick="return false;" class="pseudolink" id="'+this.post_fix_id(post_info.id)+'">';
    if (!Number(post_info['fixed'])) value += '���������� ������';
    else value += '��������� ������';
    value += '</a></li>';
  }
  if (this.can_hide && (post_info.can_hide - 0))
  {
    value=value+'<li><a href="#" onclick="return false;" class="pseudolink" id="'+this.post_hide_id(post_info.id)+'">';
    if (post_info.hidden) value += '������� ������';
    else value += '������ ������';
    value += '</a></li>';
  }
  value += '</ul><div class="clear"><!-- --></div>';
  value += '</li>';
  return value;
  },

  add: function(post_info)
  {
  var is_replace = false;
  if (this.posts.get(post_info.id))
  {
    is_replace = true;
    this.stop_element(this.posts.get(post_info.id));
  }
  this.posts.set(post_info.id,post_info);
  if (is_replace) $(this.post_cell_id(post_info.id)).replace(this.html(post_info));
  else new Insertion.Bottom(this.container,this.html(post_info));
  if (post_info.type == 1 && post_info.survey_id)
  {
    post_info.survey = new Community.Survey.Survey(this.post_survey_id(post_info.id),post_info.survey_id,community.prop.user_id,{hide_title:true, can_view_result: true});
  }

  if (this.can_delete && (post_info.can_delete - 0))
  {
    $(this.post_delete_id(post_info.id)).observe("click",this.del.bindAsEventListener(this,post_info.id));
  }
  if (this.can_edit && (post_info.can_edit - 0))
  {
    $(this.post_edit_id(post_info.id)).observe("click",this.edit.bindAsEventListener(this,post_info.id));
  }
  if (this.can_hide && (post_info.can_hide - 0))
  {
    $(this.post_hide_id(post_info.id)).observe("click",this.hide.bindAsEventListener(this,post_info.id));
  }
  if (this.can_fix && (post_info.can_edit - 0))
  {
    $(this.post_fix_id(post_info.id)).observe("click",this.fix.bindAsEventListener(this,post_info.id));
  }
  if ($(this.post_body_all_id(post_info.id)))
  {
    $(this.post_body_all_id(post_info.id)).observe("click",this.show_all_body.bindAsEventListener(this,post_info.id));
  }
  if (!this.disable_tags)
  {
    this.posts.get(post_info.id).tags = new Community.Tags(this.post_tags_id(post_info.id),post_info.id,post_info.type,post_info.author_id,post_info.can_edit_tag);
  }
  if (!this.disable_favorite)
  {
    this.posts.get(post_info.id).favorite = new Community.Favorite(this.post_favorite_id(post_info.id),post_info.id,post_info.type,post_info.favorite_included);
  }
  if (!this.disable_sign)
  {
    this.posts.get(post_info.id).sign = new Community.Sign(this.post_sign_id(post_info.id),post_info.id,post_info.type,post_info.sign_included);
  }
  if (!this.disable_censure && Number(post_info.can_censure))
  {
    this.posts.get(post_info.id).censure = new Community.Censure(this.post_censure_id(post_info.id),post_info.id,post_info.censure_enabled);
  }
  },

  show_all_body: function(event,post_id)
  {
    var post_info = this.posts.get(post_id);
    $(this.post_body_all_id(post_id)).stopObserving("click");
    $(this.post_body_id(post_id)).update(community.convert_smiles(post_info.body));
  },

  remove: function(post_id)
  {
  if (!this.posts.get(post_id)) return;
  this.posts.unset(post_id);
  $(this.post_cell_id(post_id)).remove();
  },

  edit: function(event,post_id)
  {
  if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");
  if (this.edit_editor) { this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null; }
  if (this.edit_insertsurvey) { this.edit_insertsurvey.clean(); this.edit_insertsurvey = null; }
  if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
  var post_info = this.posts.get(post_id);
  if (!post_info) return;

  this.edit_window = new Community.Window({
            title:"�������������� ������ "+(post_info.title ? post_info.title : "(��� ��������)"),
            left: 10,
            content:this.edit_window_content(post_info),
            ok_button:true,
            on_ok:this.edit_ok.bind(this),
            on_before_close: this.edit_window_before_close.bind(this)
                 });
  $(this.edit_insertsurvey_id()).observe("click",this.edit_survey.bind(this));

  this.edit_window.show();
  this.edit_editor = new Community.HTMLEditor(this.edit_body_id(),{extended:true});
  this.edit_insertsurvey = new Community.InsertSurvey({survey_id:post_info.survey_id,
                                                       survey_pos:post_info.survey_pos,
                                                       on_insert:this.insert_survey.bind(this),
                                                       on_save:this.edit_ok2.bind(this)});
  if (!post_info.hidden)
  {
    makeSelectBox("#"+this.edit_rd_acc_id());
    makeSelectBox("#"+this.edit_wr_acc_id());
  }
  this.edit_id = post_id;
  jQuery('select').styler();
  },

  edit_window_content: function(post_info)
  {
  var value = '<table style="width:550px;"><tbody>';
  value += '<tr><td width="20%">���������:</td><td>';
  value += '<input id="' + this.edit_title_id() + '" type="text" class="inp_text" style="width:500px;" value="' + post_info.title + '"/></td></tr>';
  value += '<tr><td>������:</td><td>';
  value += '<textarea id="' + this.edit_body_id() + '" style="width:513px;height:200px;">' + post_info.body + '</textarea></td></tr>';
  value += '<tr><td>&nbsp;</td><td><div id="'+this.edit_insertsurvey_info_id()+'">';
  if (post_info.survey_id) value += '<strong>� ��������� ���������� �����</strong>';
  value += '</div><a href="#" id="'+this.edit_insertsurvey_id()+'" onclick="return false;">';
  if (post_info.survey_id) value += '�������� �����';
  else value += '���������� ����� � ���������';
  value += '</a></td></tr>';

  if (!post_info.hidden)
  {
    value += '<tr><td>����� ��������:</td><td><div style="z-index:3000;" class="por"><select class="selectbox" id="' + this.edit_rd_acc_id() + '" onchange="community.update_wr_acc_select(\'' + this.edit_rd_acc_id() + '\',\'' + this.edit_wr_acc_id() + '\');">';
    community.acc_title.each(function(pair)
          {
      value += '<option value="' + pair.key + '"';
      if (post_info.rd_acc == pair.key) value += ' selected="selected"';
      value += '>' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>����� ��������������:</td><td><div class="por"><select class="selectbox" id="' + this.edit_wr_acc_id() + '">';
    community.wr_acc_title.each(function(pair)
          {
      if (community.acc_order.get(pair.key) < community.acc_order.get(post_info.rd_acc)) return;
      value += '<option value="' + pair.key + '"';
      if (post_info.wr_acc == pair.key) value += ' selected="selected"';
      value += '>' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
  }
  value += '<tr><td>������:</td><td id="' + this.edit_status_id() + '">�� �����������</td></tr>';
  value += '</tbody></table>';
  return value;
  },

  edit_survey: function()
  {
    if (this.edit_insertsurvey) this.edit_insertsurvey.show();
  },

  insert_survey: function()
  {
    $(this.edit_insertsurvey_id()).update("�������� �����");
    $(this.edit_insertsurvey_info_id()).update("<strong>� ��������� ���������� �����</strong>");
  },

  edit_ok: function()
  {
    if (!this.edit_insertsurvey) return;
    if (this.edit_insertsurvey.accepted)
    {
      this.edit_insertsurvey.save();
    }
    else this.edit_ok2();
  },

  edit_ok2: function(survey_id)
  {
    $(this.edit_status_id()).update("�����������...");
    var params = {
      cmd:"post.update",
      id:this.edit_id,
      title:$F(this.edit_title_id()),
      body:this.edit_editor.get_value(),
      check:community.prop.check
    };
    if ($(this.edit_rd_acc_id())) params.rd_acc = $F(this.edit_rd_acc_id());
    if ($(this.edit_wr_acc_id())) params.wr_acc = $F(this.edit_wr_acc_id());
    if (survey_id) params.survey_id = survey_id;
    if (this.edit_insertsurvey) params.survey_pos = this.edit_insertsurvey.get_survey_pos();

    new Ajax.Request(community.url.blog_ajax,
         {
          parameters: params,
          onSuccess: this.edit_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  },

  edit_success: function(transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    $(this.edit_status_id()).update("������ ����������: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  var post_info = response.join(" ").evalJSON(true);

  if (this.on_before_edit)
  {
    if (!this.on_before_edit(post_info.id)) return;
  }
  if (post_info.id == this.edit_id)
  {
    if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");
    if (this.edit_editor) { this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null; }
    if (this.edit_insertsurvey) { this.edit_insertsurvey.clean(); this.edit_insertsurvey = null; }
    if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
  }
  this.add(post_info);
  if (this.on_edit) this.on_edit(post_info.id);
  },

  edit_window_before_close: function()
  {
  if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");
  if (this.edit_editor) { this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null; }
  if (this.edit_insertsurvey) { this.edit_insertsurvey.clean(); this.edit_insertsurvey = null; }

  return true;
  },

  hide: function(event,post_id)
  {
  var post_info = this.posts.get(post_id);
  if (!post_info) return;
  if (post_info.hidden)
  {
    new Ajax.Request(community.url.blog_ajax,
      {
        parameters: {cmd:"post.unhide",id:post_id,check:community.prop.check},
        onSuccess: this.unhide_success.bind(this,post_id),
        onFailure: community.ajax_failure.bind(community)
      });
  }
  else
  {
    new Ajax.Request(community.url.blog_ajax,
      {
        parameters: {cmd:"post.hide",id:post_id,check:community.prop.check},
        onSuccess: this.hide_success.bind(this,post_id),
        onFailure: community.ajax_failure.bind(community)
      });
  }
  },

  hide_success: function(post_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������ �������: "+transport.responseText); return; }
  var got_post_id = response[1];
  if (post_id != got_post_id) return;
  if (this.on_before_hide)
  {
    if (!this.on_before_hide(post_id)) return;
  }
  var post_info = this.posts.get(post_id);
  post_info.hidden = 1;
  this.add(post_info);
  if (this.edit_window && this.edit_id == post_id)
  {
    if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");
    if (this.edit_editor)
    {
      this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null;
    }
    if (this.edit_insertsurvey)
    {
      this.edit_insertsurvey.clean(); this.edit_insertsurvey = null;
    }
    this.edit_window.clean();
    this.edit_window = null;
  }
  if (this.on_hide) this.on_hide(post_id);
  },

  unhide_success: function(post_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������ ��������: "+transport.responseText); return; }
  var got_post_id = response[1];
  if (post_id != got_post_id) return;
  if (this.on_before_unhide)
  {
    if (!this.on_before_unhide(post_id)) return;
  }
  var post_info = this.posts.get(post_id);
  post_info.hidden = 0;
  this.add(post_info);
  if (this.edit_window && this.edit_id == post_id)
  {
    if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");

    if (this.edit_editor)
    {
      this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null;
    }
    if (this.edit_insertsurvey)
    {
      this.edit_insertsurvey.clean(); this.edit_insertsurvey = null;
    }
    this.edit_window.clean();
    this.edit_window = null;
  }
  if (this.on_unhide) this.on_unhide(post_id);
  },

  del: function(event,post_id)
  {
  if (confirm("�� �������?"))
  {
    new Ajax.Request(community.url.blog_ajax,
         {
          parameters: {cmd:"post.delete",id:post_id,check:community.prop.check},
          onSuccess: this.del_success.bind(this,post_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  }
  },

  del_success: function(post_id,transport)
  {
  var response = transport.responseText;
  if (response != "OK") { alert("������ ��������: "+response); return; }
  if (this.on_before_delete)
  {
    if (!this.on_before_delete(post_id)) return;
  }
  this.remove(post_id);
  if (this.edit_window && this.edit_id == post_id)
  {
    if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");
    if (this.edit_editor)
    {
      this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null;
    }
    if (this.edit_insertsurvey)
    {
      this.edit_insertsurvey.clean(); this.edit_insertsurvey = null;
    }
    this.edit_window.clean();
    this.edit_window = null;
  }
  if (this.on_delete) this.on_delete(post_id);
  },

  fix: function(event,post_id)
  {
    var post_info = this.posts.get(post_id);
    if (!post_info) return;
    if (Number(post_info['fixed']))
    {
      new Ajax.Request(community.url.blog_ajax,
        {
          parameters: {cmd:"post.unfix",id:post_id,check:community.prop.check},
          onSuccess: this.unfix_success.bind(this,post_id),
          onFailure: community.ajax_failure.bind(community)
        });
    }
    else
    {
      new Ajax.Request(community.url.blog_ajax,
      {
        parameters: {cmd:"post.fix",id:post_id,check:community.prop.check},
        onSuccess: this.fix_success.bind(this,post_id),
        onFailure: community.ajax_failure.bind(community)
      });
    }
  },

  fix_success: function(post_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������: "+transport.responseText); return; }
  var got_post_id = response[1];
  if (post_id != got_post_id) return;
  if (this.on_before_fix)
  {
    if (!this.on_before_fix(post_id)) return;
  }
  var post_info = this.posts.get(post_id);
  post_info['fixed'] = 1;
  this.add(post_info);
  if (this.edit_window && this.edit_id == post_id)
  {
    if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");
    if (this.edit_editor)
    {
      this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null;
    }
    if (this.edit_insertsurvey)
    {
      this.edit_insertsurvey.clean(); this.edit_insertsurvey = null;
    }
    this.edit_window.clean();
    this.edit_window = null;
  }
  if (this.on_fix) this.on_fix(post_id);
  },

  unfix_success: function(post_id,transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK") { alert("������: "+transport.responseText); return; }
  var got_post_id = response[1];
  if (post_id != got_post_id) return;
  if (this.on_before_unfix)
  {
    if (!this.on_before_unfix(post_id)) return;
  }
  var post_info = this.posts.get(post_id);
  post_info['fixed'] = 0;
  this.add(post_info);
  if (this.edit_window && this.edit_id == post_id)
  {
    if ($(this.edit_insertsurvey_id())) $(this.edit_insertsurvey_id()).stopObserving("click");
    if (this.edit_editor)
    {
      this.edit_editor.remove(); this.edit_editor.clean(); this.edit_editor = null;
    }
    if (this.edit_insertsurvey)
    {
      this.edit_insertsurvey.clean(); this.edit_insertsurvey = null;
    }
    this.edit_window.clean();
    this.edit_window = null;
  }
  if (this.on_unfix) this.on_unfix(post_id);
  },

  show_group_access: function()
  {
  if (this.group_access_window) { this.group_access_window.clean(); this.group_access_window = null; }
  this.group_access_window = new Community.Window({
            title:"��������� ��������� ����",
            left: 10,
            content:this.group_access_window_content(),
            ok_button:true,
            on_ok:this.group_access_ok.bind(this)
                 });

  this.group_access_window.show();
  makeSelectBox("#"+this.group_access_rd_acc_id());
  makeSelectBox("#"+this.group_access_wr_acc_id());
  jQuery('select').styler();
  },

  group_access_window_content: function()
  {
    var value = '<table style="width:550px;"><tbody>';
    value += '<tr><td>����� ��������:</td><td><div style="z-index:3000;" class="por"><select class="selectbox" id="' + this.group_access_rd_acc_id() + '" onchange="community.update_wr_acc_select2(\'' + this.group_access_rd_acc_id() + '\',\'' + this.group_access_wr_acc_id() + '\');">';
    value += '<option value="-1">--��� ���������--</option>';
    community.acc_title.each(function(pair)
          {
            value += '<option value="' + pair.key + '">' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>����� ��������������:</td><td><div class="por"><select class="selectbox" id="' + this.group_access_wr_acc_id() + '">';
    value += '<option value="-1">--��� ���������--</option>';
    community.wr_acc_title.each(function(pair)
          {
      value += '<option value="' + pair.key + '">' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>������:</td><td id="' + this.group_access_status_id() + '">�� �����������</td></tr>';
    value += '</tbody></table>';
    return value;
  },

  group_access_ok: function()
  {
  $(this.group_access_status_id()).update("�����������...");

  new Ajax.Request(community.url.blog_ajax,
         {
          parameters: {
             cmd:"post.group_access",type:1,
             rd_acc: $F(this.group_access_rd_acc_id()),
             wr_acc: $F(this.group_access_wr_acc_id()),
             where:community.prop.current_id,
             check:community.prop.check
          },
          onSuccess: this.group_access_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  },

  group_access_success: function(transport)
  {
   var response = transport.responseText.split(" ");
   if (response[0] != "OK")
   {
     $(this.group_access_status_id()).update("������ ���������� ���� �������: "+transport.responseText);
     return;
   }
   document.location.href=community.url.blog+'?where='+community.prop.current_id;

  }

});

Community.Posts.id = 0;