var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Photos = Class.create({
  initialize: function(container,options)
  {
  if (!options) options = {};
  this.id = Community.Photos.id++;
  this.can_edit = options.can_edit || false;
  this.can_delete = options.can_delete || false;
  this.can_move = options.can_move || false;
  this.on_edit = options.on_edit || null;
  this.on_delete = options.on_delete || null;
  this.on_select = options.on_select || null;
  this.selectable = options.selectable || false;
  this.total = Number(options.total);
  this.edit_window = null;
  this.edit_id = 0;
  this.photo_marks_edit = null;
  this.container = $(container);
  if (!this.container) throw new Error("����������� ��������� ��� ����������!");
  this.photos = new Hash();
  this.selected_photo = -1;
  this.album_id = options.album_id || null;
  this.group_access = options.group_access || null;
  if (this.album_id && this.group_access && $(this.group_access)) {
    $(this.group_access).observe("click",this.show_group_access.bind(this));
  }
  community.init.add_unload(this.clean.bind(this));
  },

  photo_img_id: function(photo_id) { return "Community_Photo_Img_"+this.id+"_"+photo_id; },
  photo_move_left_id: function(photo_id) { return "Community_Photo_MoveLeft_"+this.id+"_"+photo_id; },
  photo_move_right_id: function(photo_id) { return "Community_Photo_MoveRight_"+this.id+"_"+photo_id; },
  photo_cell_id: function(photo_id) { return "Community_Photo_Cell_"+this.id+"_"+photo_id; },
  photo_edit_id: function(photo_id) { return "Community_Photo_Edit_"+this.id+"_"+photo_id; },
  photo_delete_id: function(photo_id) { return "Community_Photo_Delete_"+this.id+"_"+photo_id; },
  photo_marks_id: function(photo_id) { return "Community_Photo_Marks_"+this.id+"_"+photo_id; },
  edit_title_id: function() { return "Community_Photo_EditTitle_"+this.id; },
  edit_body_id: function() { return "Community_Photo_EditBody_"+this.id; },
  edit_rd_acc_id: function() { return "Community_Photo_EditRdAcc_"+this.id; },
  edit_wr_acc_id: function() { return "Community_Photo_EditWrAcc_"+this.id; },
  edit_status_id: function() { return "Community_Photo_EditStatus_"+this.id; },
  group_access_rd_acc_id: function() { return "Community_Photo_GroupAccessRdAcc_"+this.id; },
  group_access_wr_acc_id: function() { return "Community_Photo_GroupAccessWrAcc_"+this.id; },
  group_access_status_id: function() { return "Community_Photo_GroupAccessStatus_"+this.id; },

  photo_src: function(photo_id)
  {
  return community.url.photo_src+'?size=thmb&photo='+photo_id;
  },

  photo_src_big: function(photo_id)
  {
  return community.url.photo_src+'?size=std&photo='+photo_id;
  },

  html: function(photo_info)
  {
  var value = '<div class="AlbumPreview" style="display: '+ ((photo_info.cur_ind > 3)?"none":"block") +';"><table id="' + this.photo_cell_id(photo_info.id) + '">';
  value += '<tr><td style="text-align:center;font-size:10px;">';
  if (this.can_delete && (photo_info.can_delete - 0))
  {
    value += '<a class="mr5" href="#" id="' + this.photo_delete_id(photo_info.id) + '" onclick="return false;"><img alt="�������" height="16" src="http://img.blutbad.ru/i/contacts/group_del.gif" title="�������" width="16"></a>';
  }
  if (this.can_edit && (photo_info.can_edit - 0))
  {
    value += '<a href="#" id="' + this.photo_edit_id(photo_info.id) + '" onclick="return false;"><img alt="" height="16" src="http://img.blutbad.ru/i/contacts/contact_edit.gif" title="�������������" width="16"></a>';
  }

  var imsrc = '';
  var imsrc_b = '';
  if (Number(photo_info.censored)) imsrc = '/i/comm/censored_thmb.png'; else imsrc = this.photo_src(photo_info.id) + '&ext=' + photo_info.id + '.png';
  if (Number(photo_info.censored)) imsrc_b = '/i/comm/censored_thmb.png'; else imsrc_b = this.photo_src_big(photo_info.id) + '&ext=' + photo_info.id + '.png';

  value += '</td></tr><tr><td style="text-align: center; padding: 5px;"><a href="/photo_comments=' + photo_info.id + '" fhref="' + imsrc_b + '" fid="' + photo_info.id + '" class="fancybox" data-fancybox-group="gallery_' + photo_info.parent_id + '" title="' + photo_info.title + '" onclick="return false;"><img class="pics" id="' + this.photo_img_id(photo_info.id) + '" src="' + imsrc + '" \>';

  value += '</a></td></tr>';
  value += '<tr><td style="text-align:center;font-size:10px;">';
  if (this.can_move && (photo_info.can_edit - 0))
  {
    if (photo_info.sort_id > 1) value += '<a class="mr5" style="float: left;" href="#" id="' + this.photo_move_left_id(photo_info.id) + '" onclick="return false;">&lt;-</a>';
    if (photo_info.sort_id < this.total) value += '<a class="ml5" style="float: right;" href="#" id="' + this.photo_move_right_id(photo_info.id) + '" onclick="return false;">-&gt;</a>';
  }
  if (this.can_edit && (photo_info.can_edit - 0)) {
    value += '<a href="#" id="' + this.photo_marks_id(photo_info.id) + '" onclick="return false;">�������� �����</a>';
  }
  value += '</td></tr>';
  if (photo_info.hidden) value += '<tr><td class="bold center"><font color="red">���������� ������ �����������</font></td></tr>';
  value += '</table></div>';
  return value;
  },

  add: function(photo_info)
  {
  var is_replace = false;
  if (this.photos.get(photo_info.id))
  {
    is_replace = true;
    this.stop_element(this.photos.get(photo_info.id));
  }
  this.photos.set(photo_info.id,photo_info);
  if (is_replace) $($(this.photo_cell_id(photo_info.id)).parentNode).replace(this.html(photo_info));
  else new Insertion.Bottom(this.container,this.html(photo_info));

  this.observe_photo(photo_info);
  },

  observe_photo: function(photo_info)
  {
  $(this.photo_img_id(photo_info.id)).observe("click",this.select.bindAsEventListener(this,photo_info.id));
  if (this.can_delete && (photo_info.can_delete - 0))
  {
    $(this.photo_delete_id(photo_info.id)).observe("click",this.del.bindAsEventListener(this,photo_info.id));
  }
  if (this.can_edit && (photo_info.can_edit - 0))
  {
    $(this.photo_edit_id(photo_info.id)).observe("click",this.edit.bindAsEventListener(this,photo_info.id));
    $(this.photo_marks_id(photo_info.id)).observe("click",this.marks.bindAsEventListener(this,photo_info.id));
  }
  if (this.can_move && (photo_info.can_edit - 0))
  {
    if (photo_info.sort_id > 1)
    {
      $(this.photo_move_left_id(photo_info.id)).observe("click",this.move_left.bindAsEventListener(this,photo_info.id));
    }
    if (photo_info.sort_id < this.total)
    {
      $(this.photo_move_right_id(photo_info.id)).observe("click",this.move_right.bindAsEventListener(this,photo_info.id));
    }
  }
  },

  remove: function(photo_id)
  {
  if (!this.photos.get(photo_id)) return;
  this.photos.unset(photo_id);
  $($(this.photo_cell_id(photo_id)).parentNode).remove();
  if (this.selected_photo == photo_id) this.selected_photo = -1;
  },

  clean: function()
  {
  this.clear();
  this.container = null;
  if (this.edit_window) this.edit_window.clean();
  this.edit_window = null;
  if (this.group_access_window) this.group_access_window.clean();
  this.group_access_window = null;
  if (this.group_access && $(this.group_access)) $(this.group_access).stopObserving("click");
  },

  clear: function()
  {
  if (this.edit_window) this.edit_window.clear();
  if (this.group_access_window) this.group_access_window.clear();
  var t_this = this;
  this.photos.each(function(pair)
    {
      t_this.stop_element(pair.value);
    });
  if (this.container) this.container.update("");
  this.selected_photo = -1;
  this.photos = new Hash();
  },

  stop_element: function(photo_info)
  {
  var img_element = $(this.photo_img_id(photo_info.id));
  if (img_element) img_element.stopObserving("click");
  var delete_element = $(this.photo_delete_id(photo_info.id));
  if (delete_element) delete_element.stopObserving("click");
  var edit_element = $(this.photo_edit_id(photo_info.id));
  if (edit_element) edit_element.stopObserving("click");
  var marks_element = $(this.photo_marks_id(photo_info.id));
  if (marks_element) marks_element.stopObserving("click");
  var move_left_element = $(this.photo_move_left_id(photo_info.id));
  if (move_left_element) move_left_element.stopObserving("click");
  var move_right_element = $(this.photo_move_right_id(photo_info.id));
  if (move_right_element) move_right_element.stopObserving("click");
  },

  select: function(event,photo_id)
  {
  if (this.selectable)
  {
    if (this.selected_photo != -1) $(this.photo_img_id(this.selected_photo)).setStyle({ border:""});
    this.selected_photo = photo_id;
    if (photo_id != -1)
    {
      $(this.photo_img_id(photo_id)).setStyle({ border:"2px solid black"});
      if (this.on_select) this.on_select(photo_id);
    }
  }
  else
  {
    var photo_info = this.photos.get(photo_id);
    if (photo_info.isshow == 0) {
     if (photo_id != -1) document.location.href='/photo_comments='+photo_id;
    }
  }
  },

  marks: function(event,photo_id)
  {
  var photo_info = this.photos.get(photo_id);
  if (!photo_info) return;
  if (!this.photo_marks_edit) this.photo_marks_edit = new Community.PhotoMarksEdit();
  this.photo_marks_edit.show(photo_info);
  },

  edit: function(event,photo_id)
  {
  if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
  var photo_info = this.photos.get(photo_id);
  if (!photo_info) return;
  this.edit_window = new Community.Window({
            title:"�������������� ���������� "+(photo_info.title ? photo_info.title : "(��� ��������)"),
            content:this.edit_window_content(photo_info),
            ok_button:true,
            on_ok:this.edit_ok.bind(this)
                 });
  this.edit_window.show();
  if (!photo_info.hidden)
  {
    makeSelectBox("#"+this.edit_rd_acc_id());
    makeSelectBox("#"+this.edit_wr_acc_id());
  }
  this.edit_id = photo_id;
  jQuery('select').styler();
  },

  edit_window_content: function(photo_info)
  {
  var value='<table><tbody>';
  value=value+'<tr><td style="width:150px;">���������</td><td>';
  value=value+'<input id="'+this.edit_title_id()+'" type="text" class="inp_text" style="width:500px;" value="'+photo_info.title+'"/></td></tr>';
  value=value+'<tr><td>��������</td><td>';
  value=value+'<textarea id="'+this.edit_body_id()+'" rows="5" class="inp_text" style="width:500px;height:180px;">'+photo_info.body.gsub("<br />","&#10;")+'</textarea></td></tr>';
  if (!photo_info.hidden)
  {
    value=value+'<tr><td>����� ��������</td><td><div class="por" style="z-index:3000;"><select id="'+this.edit_rd_acc_id()+'" onchange="community.update_wr_acc_select(\''+this.edit_rd_acc_id()+'\',\''+this.edit_wr_acc_id()+'\');">';
    community.acc_title.each(function(pair)
          {
      value=value+'<option value="'+pair.key+'"';
      if (photo_info.rd_acc == pair.key) value=value+" selected";
      value=value+'>'+pair.value+'</option>';
          }
         );
    value=value+'</select></div></td></tr>';
    value=value+'<tr><td>����� ��������������</td><td><div class="por"><select id="'+this.edit_wr_acc_id()+'">';
    community.wr_acc_title.each(function(pair)
          {
      if (community.acc_order.get(pair.key) < community.acc_order.get(photo_info.rd_acc)) return;
      value=value+'<option value="'+pair.key+'"';
      if (photo_info.wr_acc == pair.key) value=value+" selected";
      value=value+'>'+pair.value+'</option>';
          }
         );
    value=value+'</select></div></td></tr>';
  }
  value=value+'<tr><td>������</td><td id="'+this.edit_status_id()+'">�� �����������</td></tr>';
  value=value+'</tbody></table>';
  return value;
  },

  edit_ok: function()
  {
  $(this.edit_status_id()).update("�����������...");
  var params = {cmd:"photo.update",id:this.edit_id,title:$F(this.edit_title_id()),body:$F(this.edit_body_id()),check:community.prop.check};
  if ($(this.edit_rd_acc_id())) params.rd_acc = $F(this.edit_rd_acc_id());
  if ($(this.edit_wr_acc_id())) params.wr_acc = $F(this.edit_wr_acc_id());
  new Ajax.Request(community.url.photo_edit_ajax,
         {
          parameters: params,
          onSuccess: this.edit_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  },

  edit_success: function(transport)
  {
  var response = transport.responseText.split(" ");
  if (response[0] != "OK")
  {
    $(this.edit_status_id()).update("������ ����������: "+transport.responseText);
    return;
  }
  response.splice(0,1);
  var photo_info = response.join(" ").evalJSON(true);
  if (photo_info.id == this.edit_id)
  {
    if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
  }
  this.add(photo_info);
  if (this.on_edit) this.on_edit(photo_info.id);
  },

  del: function(event,photo_id)
  {
  if (confirm("�� �������?"))
  {
    new Ajax.Request(community.url.photo_edit_ajax,
         {
          parameters: {cmd:"photo.delete",id:photo_id,check:community.prop.check},
          onSuccess: this.del_success.bind(this,photo_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  }
  },

  del_success: function(photo_id,transport)
  {
  var response = transport.responseText;
  if (response != "OK") { alert("������ ��������: "+response); return; }
  this.remove(photo_id);
  if (this.edit_window && this.edit_id == photo_id)
  {
    this.edit_window.clean(); this.edit_window = null;
  }
  if (this.on_delete) this.on_delete(photo_id);
  },

  move_left: function(event,photo_id)
  {
    new Ajax.Request(community.url.photo_edit_ajax,
         {
          parameters: {cmd:"photo.move_left",id:photo_id,check:community.prop.check},
          onSuccess: this.move_success.bind(this,photo_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  },

  move_right: function(event,photo_id)
  {
    new Ajax.Request(community.url.photo_edit_ajax,
         {
          parameters: {cmd:"photo.move_right",id:photo_id,check:community.prop.check},
          onSuccess: this.move_success.bind(this,photo_id),
          onFailure: community.ajax_failure.bind(community)
         }
        );
  },

  move_success: function(photo_id,transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK")
    {
      alert("������ �����������: "+transport.responseText);
      return;
    }
    response.splice(0,1);
    var photos = response.join(" ").evalJSON(true);
   // console.log(JSON.stringify(photos.left));
   // console.log(JSON.stringify(photos.right));
    this.swap(photos);
  },

  swap: function(photos)
  {
    if (photos.left == this.edit_id || photos.right == this.edit_id)
    {
      if (this.edit_window) { this.edit_window.clean(); this.edit_window = null; }
    }

    var was_left = null;
    var was_right = null;

    if (this.photos.get(photos.left.id))
    {
      this.stop_element(this.photos.get(photos.left.id));
      was_left = $($(this.photo_cell_id(photos.left.id)).parentNode);
      was_left.update('');
      this.photos.unset(photos.left.id);
    }

    if (this.photos.get(photos.right.id))
    {
      this.stop_element(this.photos.get(photos.right.id));
      was_right = $($(this.photo_cell_id(photos.right.id)).parentNode);
      was_right.update('');
      this.photos.unset(photos.right.id);
    }

    if (was_left)
    {
      this.photos.set(photos.right.id,photos.right);
      was_left.replace(this.html(photos.right));
      this.observe_photo(photos.right);
    }

    if (was_right)
    {
      this.photos.set(photos.left.id,photos.left);
      was_right.replace(this.html(photos.left));
      this.observe_photo(photos.left);
    }
  },

  show_group_access: function()
  {
  if (this.group_access_window) { this.group_access_window.clean(); this.group_access_window = null; }
  this.group_access_window = new Community.Window({
            title:"��������� ��������� ����",
            left: 10,
            content:this.group_access_window_content(),
            ok_button:true,
            on_ok:this.group_access_ok.bind(this)
                 });

  this.group_access_window.show();
  makeSelectBox("#"+this.group_access_rd_acc_id());
  makeSelectBox("#"+this.group_access_wr_acc_id());
  jQuery('select').styler();
  },

  group_access_window_content: function()
  {
    var value = '<table style="width:550px;"><tbody>';
    value += '<tr><td>����� ��������:</td><td><div style="z-index:3000;" class="por"><select class="selectbox" id="' + this.group_access_rd_acc_id() + '" onchange="community.update_wr_acc_select2(\'' + this.group_access_rd_acc_id() + '\',\'' + this.group_access_wr_acc_id() + '\');">';
    value += '<option value="-1">--��� ���������--</option>';
    community.acc_title.each(function(pair)
          {
            value += '<option value="' + pair.key + '">' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>����� ��������������:</td><td><div class="por"><select class="selectbox" id="' + this.group_access_wr_acc_id() + '">';
    value += '<option value="-1">--��� ���������--</option>';
    community.wr_acc_title.each(function(pair)
          {
      value += '<option value="' + pair.key + '">' + pair.value + '</option>';
          }
         );
    value += '</select></div></td></tr>';
    value += '<tr><td>������:</td><td id="' + this.group_access_status_id() + '">�� �����������</td></tr>';
    value += '</tbody></table>';
    return value;
  },

  group_access_ok: function()
  {
  $(this.group_access_status_id()).update("�����������...");
  new Ajax.Request(community.url.blog_ajax,
         {
          parameters: {
             cmd:"post.group_access",type:3,parent:this.album_id,
             rd_acc: $F(this.group_access_rd_acc_id()),
             wr_acc: $F(this.group_access_wr_acc_id()),
             where:community.prop.current_id,
             check:community.prop.check
          },
          onSuccess: this.group_access_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  },

  group_access_success: function(transport)
  {
   var response = transport.responseText.split(" ");
   if (response[0] != "OK")
   {
     $(this.group_access_status_id()).update("������ ���������� ���� �������: "+transport.responseText);
     return;
   }
   document.location.href='/photo_edit='+this.album_id;
  }

});

Community.Photos.id = 0;