var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.Tags = Class.create({
  initialize: function(element,obj_id,type,uid,can_edit,tags)
  {
  this.id = Community.Tags.id++;
  this.obj_id = obj_id;
  this.type = type || 0;
  this.uid = uid;
  this.can_edit = can_edit || 0;
  this.tags = new Array();
  this.element = $(element);
  if (!this.element) throw new Error("�� �������� ����������� ����� ��� �����!");
  if (tags == undefined)
  {
    this.element.update("���� ��������...").show();
    new Ajax.Request(community.url.tags_ajax,
       {
        parameters: {cmd:"tag.get",id:this.obj_id,type:this.type},
        onSuccess: this.init_success.bind(this),
        onFailure: community.ajax_failure.bind(community)
       }
      );

  } else {
    if (tags == "") this.tags = new Array();
    else this.tags = tags.split(",");
    this.display();
  }
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.element = null;
  if (this.autocompleter) this.autocompleter.clean();
  this.autocompleter = null;
  if (this.binder) this.binder.clean();
  this.binder = null;
  },

  stop: function()
  {
  var add_element = $(this.tags_addbutton_id());
  if (add_element)
  {
    add_element.stopObserving("click");
    for (var index=0;index<this.tags.length;index++)
    {
      var del_element = $(this.tags_delbutton_id(index));
      if (del_element) del_element.stopObserving("click");
    }
  }
  if (this.autocompleter) this.autocompleter.stop();
  if (this.binder) this.binder.stop();
  },

  tags_add_id: function() { return "Community_Tags_Add_"+this.id; },
  tags_addbutton_id: function() { return "Community_Tags_AddButton_"+this.id; },
  tags_delbutton_id: function(tag_index) { return "Community_Tags_DelButton_"+this.id+"_"+tag_index; },

  init_success: function(transport)
  {
  var response = transport.responseText.split(",");
  if (response[0] != "OK") throw new Error("������ ��������� �����: "+transport.responseText);
  response.splice(0,1);
  this.tags = response;
  this.display();
  },

  toggle: function()
  {
  this.element.toggle();
  },
 
  display: function()
  {
  this.stop();
  this.element.update(this.html()).show();
  if (this.can_edit != 0)
  {
    $(this.tags_addbutton_id()).observe("click",this.add_tag.bindAsEventListener(this));
    for (var index=0;index<this.tags.length;index++)
    {
      $(this.tags_delbutton_id(index)).observe("click",this.del_tag.bindAsEventListener(this,index));
    }
    this.autocompleter = new Community.AutoComplete(this.tags_add_id(),new Community.AutoComplete.Tags());
    this.binder = new Community.BindInputWithButton(this.tags_add_id(),this.tags_addbutton_id());
  }
  },

  html: function()
  {
  var value;
  value = '<h2>';
  if (this.type == 0) value += '��������';
  else value += '�����';
  value += '</h2><noindex><div class="content oh"';
  value += '><ul class="list"><li style="position:relative;overflow:hidden;width:1px;left:-10px;margin:0;">&nbsp;</li>';
  if(this.tags.length == 0) { value += '<li>���</li>'; }
    for (var index=0;index<this.tags.length;index++)
    {
      value += '<li><p class="ablink">';
      if (this.can_edit != 0)
      {
        value += '<a class="delete" href="#" id="'+this.tags_delbutton_id(index)+'" onclick="return false;">';
        value += '<img src="/i/comm/skin' + community.prop.skin + '/icon_delete.gif" alt="�������" title="�������" /></a>';
      }
      value += '<a href="'+community.url.tags_objects+'?cmd=tag.obj&user='+this.uid+'&tag='+this.tags[index]+'">'+this.tags[index]+'</a>';
      if ( index != this.tags.length - 1 ) value += ',';
      value += '</p></li>';
    }
  value += '</ul>';
  if (this.can_edit != 0)
  {
    value += '<div class="add_tag"><div class="fr oh"><input style="width:125px; margin-right:10px;" id="'+this.tags_add_id()+'" type="text" class="inp_text" autocomplete="off" />';
    value += '<a id="'+this.tags_addbutton_id()+'" class="button">��������</a></div></div>';
  }
  value += '</div></noindex>';
  return value;
  },

  add_tag: function(event)
  {
  new Ajax.Request(community.url.tags_ajax,
         {
          parameters: {cmd:"tag.add",id:this.obj_id,type:this.type,tag:$F(this.tags_add_id()),check:community.prop.check},
          onSuccess: this.add_tag_success.bind(this),
          onFailure: community.ajax_failure.bind(community)
         }
      );
  return false;
  },

  add_tag_success: function(transport)
  {
  var response = transport.responseText.split(",");
  if (response[0] != "OK") { alert("������ ����������: "+transport.responseText); return; }
  response.splice(0,1);
  this.stop();
  if (response.length>0) this.tags = response;
  this.display();
  },

  del_tag: function(event,tag_index)
  {
  if (confirm("�� �������, ��� ������ ������� ����� \""+this.tags[tag_index]+"\" c ����� �������?"))
  {
    new Ajax.Request(community.url.tags_ajax,
           {
            parameters: {cmd:"tag.delete",id:this.obj_id,type:this.type,tag:this.tags[tag_index],check:community.prop.check},
            onSuccess: this.del_tag_success.bind(this,tag_index),
            onFailure: community.ajax_failure.bind(community)
           }
          );
  }
  return false;
  },

  del_tag_success: function(tag_index,transport)
  {
  var response = transport.responseText;
  if (response != "OK") { alert("������ ��������: "+response); return; }
  this.stop();
  this.tags.splice(tag_index,1);
  this.display();
  }
});

Community.Tags.id = 0;
