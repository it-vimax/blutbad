var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.FriendLists = Class.create({
  initialize: function(container,friends,options)
  {
  if (!options) options = {};
  this.id = Community.LinkedAreas.id++;
  this.container = $(container);
  if (!this.container) throw new Error("����������� ��������� ��� ������� ������ ������");
  this.friends = $A(friends.evalJSON(true));
  this.linked_areas = new Community.LinkedAreas(this.container);
  this.init();
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.container = null;
  },

  stop: function()
  {
  this.linked_areas.stop();
  },

  init: function()
  {
  for (var i=0;i<Community.FriendLists.headers.length;i++)
  {
    this.linked_areas.add(Community.FriendLists.headers[i],this.html(i));
  }
  this.linked_areas.show();
  },

  html: function(index)
  {
  var value = "";
  var t_this = this;
  value += "<ul>";
  $A(this.friends[index]).each(function(friend)
    {
      value += '<li>' + community.user_title(friend.title)+' ('+friend.level_title+')';
      if ((friend.level == "good" || friend.level == "friend") &&
          (friend.level_r == "good" || friend.level_r == "friend")
         )
      {
        value += ' <img height="22" width="44" src="http://img.blutbad.ru/i/smilies/agree.gif" />';
      }
      else if ((friend.level == "enemy" || friend.level == "evil") &&
          (friend.level_r == "evil" || friend.level_r == "enemy")
         )
      {
        value += ' <img height="32" width="101" src="http://img.blutbad.ru/i/smilies/duel.gif" />';
      }
      if (friend.online) value += ' <small class="light ml10">(������)</small>';
      value += '</li>';
    });
  if (value == "<ul>") value += "<li>���</li>";
  value += "</ul>";
  return value;
  }
});

Community.FriendLists.id = 0;
Community.FriendLists.headers =  [ "������", "�����",
                  "� ������� �","�� ������ �"
                ];

//--------------------------------------------------------------------------------------------------

Community.GroupLists = Class.create({
  initialize: function(container,groups,options)
  {
  if (!options) options = {};
  this.id = Community.LinkedAreas.id++;
  this.container = $(container);
  if (!this.container) throw new Error("����������� ��������� ��� ������� ������ �����");
  this.groups = $A(groups.evalJSON(true));
  this.linked_areas = new Community.LinkedAreas(this.container);
  this.init();
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.container = null;
  },

  stop: function()
  {
  this.linked_areas.stop();
  },

  init: function()
  {
  for (var i=0;i<this.groups.length;i++)
  {
    this.linked_areas.add(Community.GroupLists.headers[i],this.html(i));
  }
  this.linked_areas.show();
  },

  html: function(index)
  {
  var value = "";
  var t_this = this;
  value += "<ul>";
  $A(this.groups[index]).each(function(group)
    {
      value += '<li>' + community.user_title(group.title)+' ('+group.level_title+')</li>';
    });
  if (value == "<ul>") value += "<li>���</li>";
  value += "</ul>";
  return value;
  }
});

Community.GroupLists.id = 0;
Community.GroupLists.headers = [ "������� � �������", "����� � �������" ];

//--------------------------------------------------------------------------------------------------

Community.GroupMembersLists = Class.create({
  initialize: function(container,members,options)
  {
  if (!options) options = {};
  this.id = Community.LinkedAreas.id++;
  this.container = $(container);
  if (!this.container) throw new Error("����������� ��������� ��� ������� ������ ���������� ������");
  this.members = $A(members.evalJSON(true));
  this.linked_areas = new Community.LinkedAreas(this.container);
  this.can_admin = options.can_admin || false;
  this.init();
  community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
  this.stop();
  this.container = null;
  },

  stop: function()
  {
  this.linked_areas.stop();
  },

  init: function()
  {
  for (var i=0;i<this.members.length;i++)
  {
    this.linked_areas.add(Community.GroupMembersLists.headers[i],this.html(i));
  }
  this.linked_areas.show();
  },

  html: function(index)
  {
  var value = '';
  var t_this = this;
  value += "<ul>";
  $A(this.members[index]).each(function(member)
    {
      value += "<li>";
      if (t_this.can_admin)
      {
        value += community.user_title(member.title,{popup_content:t_this.popup_html(member)});
      }
      else
      {
        value += community.user_title(member.title);
      }
      value += ' ('+member.level_title+')';
      if (member.online) value += ' <small class="light ml10">(������)</small>';
      value += '</li>';

    });
  if (value == "<ul>") value += '<li>���</li>';
  value += "</ul>";
  return value;
  },

  popup_html: function(member)
  {
  if (!member.title) return '';
  var vis_title = member.title.replace('$','');
  if (!vis_title) return '';
  var value='<b>'+vis_title+'</b><br /><br />';
  value += '<a href="'+community.url.letters+'?writeto='+member.title+'">�������� ������</a><br />';
  if (member.level != "self" && member.level != "enemy")
  {
    value += '<a href="?cmd=relation&user='+member.title+'&level=self&where='+community.prop.current_id+'&check='+community.prop.check+'">���������� �� ���������</a><br />';
  }
  if (member.level == "friend")
  {
    value += '<a href="?cmd=relation&user='+member.title+'&level=good&where='+community.prop.current_id+'&check='+community.prop.check+'">���������� � ����������</a><br />';
  }
  if (member.level == "good")
  {
    value += '<a href="?cmd=relation&user='+member.title+'&level=friend&where='+community.prop.current_id+'&check='+community.prop.check+'">����� �������������</a><br />';
  }
  if (member.level != "self" && member.level != "enemy")
  {
    value += '<a href="?cmd=relation&user='+member.title+'&level=all&where='+community.prop.current_id+'&check='+community.prop.check+'">���������</a><br />';
  }
  if (member.level == "enemy")
  {
    value += '<a href="?cmd=relation&user='+member.title+'&level=all&where='+community.prop.current_id+'&check='+community.prop.check+'">����� ����������</a><br />';
  }
//  if (!is_content) value += '��� ��������� ��������';
  return value;
  }
});

Community.GroupMembersLists.id = 0;
Community.GroupMembersLists.headers = [ "���������"," �����", "", "" ];
