var community;
if (!community || typeof community != "object") throw new Error("�� �������� ������ Community");

Community.InsertSurvey = Class.create({
  initialize: function(options)
  {
    if (!options) options = {};
    this.id = Community.InsertSurvey.id++;
    this.survey_window = null;
    this.on_insert = options.on_insert || null;
    this.on_save = options.on_save || null;
    this.survey_id = Number(options.survey_id) || 0;
    this.survey_pos = Number(options.survey_pos) || 0;

    this.survey = new Community.Survey(null);
    this.survey.update({survey_id:"",title:"�����",label:"",rd_acc:"community",can_public:"1",public:"1",
            date_open:community.current_date(),date_close:"31-12-2036",
            type:"vote",groups: [ $H({group_id:"",pos:0,
                                   title:"������",label:"",rd_acc:"community",can_public:"1",public:"1",
                                   can_edit:"1",issues: [
                                                         $H({title:"������",label:"",label_f:"",comment:"",answer:"",answer_f:"",type:"radio",
                                                          value:"",value_f:"",'default':"",index:"0",width:"0",
                                                          height:"0",required:"1",horizontal:"0",check_type:"none"
                                                         })
                                                        ]
                                   })
                                ]
            });
    this.loaded = 1;
    this.isnew = 1;
    this.accepted = 0;


    if (this.survey_id)
    {
      this.loaded = 0;
      new Ajax.Request(community.url.survey_edit_ajax,{ parameters: { cmd: "get", survey_id: this.survey_id },
                                                        onSuccess: this.get_survey_success.bind(this),
                                                        onFailure: community.ajax_failure.bind(community)
                                                      }
                      )
    }
    community.init.add_unload(this.clean.bind(this));
  },

  clean: function()
  {
    this.stop();
    if (this.survey_window) this.survey_window.clean();
    this.survey_window = null;
  },

  stop: function()
  {
    if (this.survey_window) this.survey_window.clear();
  },

  edit_issue_label_id: function() { return "Community_InsertSurvey_EditIssueLabel_"+this.id; },
  edit_issue_type_id: function() { return "Community_InsertSurvey_EditIssueType_"+this.id; },
  edit_issue_value_id: function() { return "Community_InsertSurvey_EditIssueValue_"+this.id; },
  edit_issue_dateclose_id: function() { return "Community_InsertSurvey_EditIssueDateClose_"+this.id; },
  edit_issue_position_id: function() { return "Community_InsertSurvey_EditIssuePosition_"+this.id; },

  get_survey_success: function(transport)
  {
    var response = transport.responseText.split(" ");
    if (response[0] != "OK") throw("������ �������� ������: "+transport.responseText);
    response.splice(0,1);
    response=response.join(" ");
    this.survey = new Community.Survey(response.evalJSON(true));

    this.unfilter_survey();
    this.loaded = 1;
    this.isnew = 0;
  },

  show: function()
  {
    if (!this.loaded) return;
    if (this.survey_window) { this.survey_window.show(); return; }
    this.survey_window = new Community.Window({
            title:"������� ������ � ���������",
            content:this.survey_window_content(),
            ok_button:true,
            on_ok:this.survey_window_ok.bind(this)
            });
    this.survey_window.show();
  },

  survey_window_content: function()
  {

    var edited_issue = this.survey.get("groups")[0].get("issues")[0];
    var value='<table><tbody><tr><td>';
    value += '<tr><td>������: </td><td><input id="'+this.edit_issue_label_id()+'" style="width:500px;" class="inp_text" value="';
    value += community.filter_body(edited_issue.get("label"));
    value += '" /></td></tr>';
    value += '<tr><td>�������� �������:<br /><small>(�� ������ � ������ ������)</small></td><td><textarea id="'+this.edit_issue_value_id()+'" cols="50" rows="5">';
    value += community.filter_body_textarea(edited_issue.get("value"));
    value += '</textarea></td></tr>';
    if (this.isnew)
    {
      value += '<tr><td>��������� ���������<br />��������� ������:</td><td><input type="checkbox" value="on" id="'+this.edit_issue_type_id()+'"';
      value += edited_issue.get("type") == "check_m" ? ' checked="checked"' : '';
      value += ' />';
      value += '</td></tr>';
    }
    else
    {
      if (edited_issue.get("type") == "check_m") value += '<tr><td colspan="2">��������� ��������� ��������� ������</td></tr>';
    }

    value += '<tr><td>���� ��������:</td><td><input class="inp_text" id="'+this.edit_issue_dateclose_id()+'" size="10" value="';
    value += community.filter_body(this.survey.get("date_close"));
    value += '"></input>';
    value += '<script type="text/javascript">jQuery("#'+this.edit_issue_dateclose_id()+'").datepicker();</script>';
    value += '</td></tr>';

    value += '<tr><td>������������:</td><td><form><input type="radio" name="'+this.edit_issue_position_id()+'" value="0" id="'+this.edit_issue_position_id()+'"';
    if (!this.survey_pos) value += ' checked="checked"';
    value += '>����� �������</input><input type="radio" name="'+this.edit_issue_position_id()+'" value="1"';
    if (this.survey_pos) value += ' checked="checked"';
    value += '>����� ������</input>';
    value += '</td></tr>';

    value += '</tbody></table>';
    return value;
  },

  survey_window_ok: function()
  {
    this.accepted = 1;
    this.survey.set("date_close",$F(this.edit_issue_dateclose_id()));
    this.survey.set("changed","1");
    this.survey.issue_set(0,0,"label",$F(this.edit_issue_label_id()));
    this.survey.issue_set(0,0,"value",$F(this.edit_issue_value_id()));
    if (this.isnew)
    {
      this.survey.issue_set(0,0,"type",$F(this.edit_issue_type_id()) ? "check_m" : "radio");
    }
    this.survey.issue_set(0,0,"changed","1");

    this.survey_pos = Number(community.$RF(this.edit_issue_position_id()));
    this.survey_window.hide();
    if (this.on_insert) this.on_insert();
  },

  get_survey_pos: function()
  {
    return this.survey_pos;
  },

  save: function()
  {
    new Ajax.Request(community.url.survey_edit_ajax,
       {
          parameters: {
            cmd:"save",
            survey_json:this.survey.survey.toJSON(),
            check:community.prop.check
                },
          onSuccess: this.save_success.bind(this),
          onFailure: this.save_failure.bind(this)
       }
      );
  },

  save_success: function(transport)
  {

    var response = transport.responseText.split(" ");
    if (response[0] != "OK")
    {
       alert("������ ����������: "+transport.responseText);
       return;
    }
    response.splice(0,1);
    response=response.join(" ");
    this.survey = new Community.Survey(response.evalJSON(true));
    this.survey_id = this.survey.get("survey_id");
    this.unfilter_survey();
    this.accepted = 0;
    this.isnew = 0;
    if (this.on_save) this.on_save(this.survey_id);
  },

  save_failure: function(transport)
  {
    alert("����������� ������ �������");
  },

  unfilter_survey: function()
  {
   this.survey.set("title",community.unfilter_body(this.survey.get("title")));
   this.survey.set("label",community.unfilter_body(this.survey.get("label")));
   var groups_length = this.survey.get("groups").length;
   for (var group_index=0;group_index<groups_length;group_index++)
   {
    var group = this.survey.get("groups")[group_index];
    group.set("title",community.unfilter_body(group.get("title")));
    group.set("label",community.unfilter_body(group.get("label")));
    var issues_length = group.get("issues").length;
    for (var issue_index=0;issue_index<issues_length;issue_index++)
    {
     var issue = group.get("issues")[issue_index];
     issue.set("title",community.unfilter_body(issue.get("title")));
     issue.set("comment",community.unfilter_body(issue.get("comment")));
     issue.set("value",community.unfilter_body(issue.get("value")));
     issue.set("label",community.unfilter_body(issue.get("label")));
     issue.set("answer",community.unfilter_body(issue.get("answer")));
    }
   }
  }
});

Community.InsertSurvey.id = 0;