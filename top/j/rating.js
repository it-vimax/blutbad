/**
 * ��������� �� ��������� ��� ������������ ���������
 */
var paginationDefaults = {
	callback: function(event, container) {
		container.prepend('��������:');
	},
	items_per_page: 1,
	next_text: '���������',
	next_show_always: false,
	num_edge_entries: 1,
	prev_show_always: false,
	prev_text: '����������'
}

$(function() {
	//total *= 9;
	//per_page = 30;

	var search = $('#search').val('').placeholder();
	var pagination = $('.pages');
	var rating = $('#rating').children('tbody').children('tr');
	var pages = Math.ceil(total / per_page);
	var current_page = Number(location.hash.toString().replace('#page-', ''));
	if(current_page) {
		if(current_page > pages)
			current_page = pages;
		current_page--;
	}

	function show_page(page) {
		rating.addClass('invisible').removeClass('even');
		var min = ((page * per_page) - 1);
		var max = ((page + 1) * per_page);
		var visible = rating.filter(':lt(' + max + ')');
		if(min > -1) {
			visible = visible.filter(':gt(' + min + ')');
		}
		visible.removeClass('invisible').filter(':odd').addClass('even');
	}

	if(pages > 1) {
		var link_to = '#page-__id__';
		$('.pages').removeClass('hidden').pagination(pages, $.extend({}, paginationDefaults, {
			 callback: function(index, container) {
				container.prepend('��������:');
				show_page(index);
				current_page = index;
			},
			current_page: current_page,
			link_to: link_to
		}));
	}

	if(pages > 1)
		show_page(current_page);

	$('#rating').removeClass('invisible');

	search.quicksearch('#rating tbody tr', {
		hide: function () {
			$(this).addClass('invisible');
		},
		onAfter: function () {
			if(search[0].value.length && !search.hasClass('placeholder')) {
				pagination.addClass('hidden');
				rating.filter(':visible').removeClass('even').filter(':odd').addClass('even');
			} else {
				pagination.removeClass('hidden');
				show_page(current_page);
			}
		},
		show: function () {
			$(this).removeClass('invisible');
		},
		selector: 'td.search'
	});
});