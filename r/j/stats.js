var CollectionRewards = new Object();

$(function() {
	$('.elapsed-time').removeClass('invisible').each(function(){
		$(this).countdown({
			format: 'yodhms',
			layout: '{y<}{yn} {yl} {y>}{o<}{on} {ol} {o>}{d<}{dn} {dl} {d>}{h<}{hn} {hl} {h>}{m<}{mn} {ml} {m>}{sn} {sl}',
			onExpiry: function() {
				var tr = $(this).parent().parent();
				
				if(tr.siblings().length) {
					tr.remove();	
				} else {
					var table = tr.closest('table');
					table.prev('h3').andSelf().remove();
				}
			},
			until: $(this).text()
		})
	});
	
	$('#achievements .toggle').click(function() {
		ul = $(this).next();
		
		if (ul.is(':visible')) {
			this.src = 'http://img.carnage.ru/i/sb_plus.gif';
		} else {
			this.src = 'http://img.carnage.ru/i/sb_minus.gif';
		}
		
		ul.toggle();
	});
	$('#achievements .ach-group-toggle').parent().andSelf().click(function(event) {
		var tbody = $(this).closest('thead').next();
		var img = this.tagName.toLowerCase() == "img" ? $(this) : $(this).find('img');
		if (tbody.is(':visible')) {
			img.attr('src','http://img.carnage.ru/i/sb_plus.gif');
			$.post("/stats.pl",{
				cmd: "close_ach_group",
				group: tbody.attr("group"),
				is_ajax: 1,
				state: tbody.attr("state")
			});
		} else {
			img.attr('src','http://img.carnage.ru/i/sb_minus.gif');
			$.post("/stats.pl",{
				cmd: "open_ach_group",
				group: tbody.attr("group"),
				is_ajax: 1,
				state: tbody.attr("state")
			});
		}
		tbody.toggle();
		event.stopPropagation();
	});
	

	$('img[reward_code]').tooltip($.extend({}, tooltipDefaults, {
		bodyHandler: function() {
			return CollectionRewards[$(this).attr('reward_code')];
		}
	}));	
});
