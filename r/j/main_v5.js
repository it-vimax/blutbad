var smiles = [["agree","angel","badevil","badtease","beer","boy","bye","chao","cheek","plak","evil","ponder","girl","gy","haha","hi","laught","mib","no","notknow","nunu","poll","protest","roll","rupor","shuffle","tongue","upset","wink","wow","yawn","yes","eyes","fire","ok","pray","smash","agent","swear","eusa","fingal","flame","kz","moder","duel","help","lebedi","vodka","vals","boylove","girllove","stp","hb","invalid","smoke","susel","joker","sneaky","kap","kuku","skull","ura","order","chaos","hug","maniak","tango","ma","bolen","kach","kiss","dustman","ghost","rev","appl","confused","hit","loony","serenada","stinky","budo","kosa","klizma","dwarf","yahoo","roga","dud","chupa","friday","sleep","tooth","wow2","strah","007","buket","nobravo","beggar","box","bravo","plak2","death","defender","dont","eating","figa","idea","kruger","mad","ok2","pank","pray2","priz","diler","shock","smoked","zasos","bang","hiya","lol","gipno","q","zub","newyear","moroz","snowgirl","medic","angel2","archer","ass1","ass2","candleman","compkiller","flytryin","gimme5","horseman","loveposter","sexover","skripka","waitlover","writer","victory","air_kiss","king","sv3"],
[44,41,20,29,59,21,20,25,39,23,20,20,21,33,26,29,20,20,20,39,24,37,33,20,36,20,20,20,20,20,20,20,20,20,25,35,34,20,43,26,23,31,20,85,101,27,75,29,93,20,20,42,31,74,21,64,33,20,50,24,23,40,48,47,60,100,66,48,23,40,51,52,30,39,28,19,45,36,33,53,54,48,37,46,42,65,64,27,57,21,33,32,35,23,50,35,35,28,35,47,70,60,28,25,27,48,34,48,22,28,27,50,36,63,33,44,30,23,17,20,42,20,47,64,35,35,37,80,25,25,29,38,36,41,60,51,60,51,51,43,30,24,28,43],
[20,21,20,20,20,22,20,20,20,20,23,20,22,20,21,22,20,20,20,20,20,25,25,20,20,20,20,24,20,20,20,20,20,20,23,22,29,20,20,22,20,20,26,20,34,20,25,31,24,22,22,24,29,23,22,29,31,20,36,21,23,40,33,32,30,37,25,24,30,31,23,33,40,24,21,19,22,30,52,28,37,23,37,48,27,32,37,24,28,22,23,20,28,23,30,26,27,22,26,32,40,30,29,20,25,28,27,23,21,31,24,27,30,25,42,27,23,36,17,20,30,22,48,31,35,31,27,40,26,28,27,29,37,20,42,20,33,45,36,55,23,26,28,25]];
var smilespage = [2,1,1,1,2,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,2,1,2,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,1,1,2,2,1,1,1,1,2,1,2,2,1,2,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,1,2,2,1,1,1,1,1,1,1,1,1,2,1,2,1,2,1,1,1,1,1,1,2,1,1,1,1,2,1,1,1,1,1,2,1,2,2,2,2,1,1,1,1,2];
var colors = [["����������","���������","������","������","���������","�������","��������","���������","�������","����","������"],
	      ["696969","191970","008080","008000","8B4513","808000","800000","7F0099","006666","737300","000000"]];

var monthTypes = new Array( 'winter', 'winter', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'winter' );
var month = ( typeof( month ) == 'undefined' ) ? 0 : month;
var type = monthTypes[ month ];
/*Encode URI - �������������� �������� � UTF-8 ��� �������� ������ ����� �������� ������*/
function urlEncode(str) {
    // ������� ��������
    if (!str || typeof(str) == "undefined") return;
    // ������� ��� ��� �������� ��������, ��� ���� - ��� ������,
    // � �������� - ��� ����������������� ����������
    var utf8Array = {};
    // ������� ��������� ����������� 255 ��������
    var i = j = j2 = 0;
    for (i = 0; i <= 255; i++) {
        j = parseInt(i/16); var j2 = parseInt(i%16);
        utf8Array[String.fromCharCode(i)] = ('%' + j.toString(16) + j2.toString(16)).toUpperCase();
    }
    // � �������� �������� ������� ���������
    var rusAdditional = {
        '_' : '%5F', '�' : '%C0', '�' : '%C1', '�' : '%C2', '�' : '%C3', '�' : '%C4', '�' : '%C5',
        '�' : '%C6', '�' : '%C7', '�' : '%C8', '�' : '%C9', '�' : '%CA', '�' : '%CB', '�' : '%CC',
        '�' : '%CD', '�' : '%CE', '�' : '%CF', '�' : '%D0', '�' : '%D1', '�' : '%D2', '�' : '%D3',
        '�' : '%D4', '�' : '%D5', '�' : '%D6', '�' : '%D7', '�' : '%D8', '�' : '%D9', '�' : '%DA',
        '�' : '%DB', '�' : '%DC', '�' : '%DD', '�' : '%DE', '�' : '%DF', '�' : '%E0', '�' : '%E1',
        '�' : '%E2', '�' : '%E3', '�' : '%E4', '�' : '%E5', '�' : '%E6', '�' : '%E7', '�' : '%E8',
        '�' : '%E9', '�' : '%EA', '�' : '%EB', '�' : '%EC', '�' : '%ED', '�' : '%EE', '�' : '%EF',
        '�' : '%F0', '�' : '%F1', '�' : '%F2', '�' : '%F3', '�' : '%F4', '�' : '%F5', '�' : '%F6',
        '�' : '%F7', '�' : '%F8', '�' : '%F9', '�' : '%FA', '�' : '%FB', '�' : '%FC', '�' : '%FD',
        '�' : '%FE', '�' : '%FF', '�' : '%B8', '�' : '%A8'
    }
    for (i in rusAdditional) utf8Array[i] = rusAdditional[i];
    // ����������� �������� ������� �� �� ����������������� �����������
    var res = "";
    for(i = 0; i < str.length; i++) {
        var simbol = str.substr(i,1);
        res += typeof utf8Array[simbol] != "undefined" ? utf8Array[simbol] : simbol;
    }
    // ������� �������� �� ����� 
    res = res.replace(/\s/g, "+");
    return res;
}

document.onmousedown = CtrlDown;
function CtrlDown(e){
	if (!e) 
		e = window.event;
	top.CtrlPress = e.ctrlKey;
}

function ClipBoard(text) {
	holdtext.innerText = text;
	var Copied = holdtext.createTextRange();
	Copied.execCommand("RemoveFormat");
	Copied.execCommand("Copy");
}

function ClipBoardNPC(text) {
	document.getElementById("copyinfo").innerText = text;
	var Copied = document.getElementById("copyinfo").createTextRange();
	Copied.execCommand("RemoveFormat");
	Copied.execCommand("Copy");
}

function screenSize() {
	var w, h; //  w - �����, h - ������
	w = (window.innerWidth ? window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.offsetWidth));
	h = (window.innerHeight ? window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.offsetHeight));
	return {w:w, h:h};
}

function CountMenuPositon(event) {
  
  var Hwindow, Wwindow, Hmenu, delta,Hsum;
  Wwindow=screenSize().w;
  if(!event) event=window.event;
  /* ���������� Y */

        Hwindow=screenSize().h; /*������ ������� ����� ����*/
        Hmenu=document.getElementById("contextMenus").clientHeight; /*������ ������������ ����*/
        Hsum=event.clientY + Hmenu; 
      
        if( Hsum<Hwindow ) { y = event.clientY; /*���� �� "�������" �� ������� ���������*/ }
        else 
        { /*������ �� ������� ���� ����������� ���� �� Y*/
          delta = Hmenu - Hwindow + event.clientY;
          y = event.clientY-delta;
          if (y<=0) y=2; /* �� ���� ���� �� ������� ���� ������*/ 
        }
        /* ���� ��� � ���������� */
        Hmenu=document.getElementById("contextMenus").clientWidth;
        Hsum=event.clientX + Hmenu;
        if (Hsum<Wwindow ) { x = event.clientX; }
        else 
        { 
          delta = Hmenu - Wwindow + event.clientX;
          x = event.clientX-delta-5;
          if(x<0) x=5;
        }
    
    /* �������� �� ���������� ���������� ��� ����������*/
    if (document.attachEvent != null)
    {
        x=x + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
        y=y + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);

    }
    else if (!document.attachEvent && document.addEventListener) { // Gecko
            x = x + window.scrollX;
            y = y + window.scrollY;
      } else {
            // Do nothing
      }
    return {x:x, y:y};

}

function UserMenu(battle,event) {
    var el, x, y, login;
	
    el = document.getElementById('contextMenus');
	
    if (!document.all || window.opera) el.onmouseout = new Function('return true');
	
    if(!event) event = window.event;
	
    var o = event.srcElement;
	
    if (!o) o = event.target;
	
    if (o.tagName != "SPAN") return true;
    login = o.innerText ? o.innerText : o.firstChild.nodeValue;
    if (o.getAttribute('alt') && o.getAttribute('alt').length > 0){
        login=o.getAttribute('alt');
    }
	//debug(login, o.innerText, o.firstChild.nodeValue);
    window.event.returnValue = false;
    event.returnValue = false;
    var i1, i2;
    if ((i1 = login.indexOf('[')) >= 0 && (i2 = login.indexOf(']')) > 0) {
		login = login.substring(i1 + 1, i2);
	}
    
    var shoot = '';

    if ( type == 'winter' ) {
		shoot = '<a class="menuItem" href="javascript:snezhok(\''+escape(login)+'\');contextMenus()">������&nbsp;�������</a>';
    } else if ( type == 'summer' ) {
		shoot = '<a class="menuItem" href="javascript:tort(\''+login+'\');contextMenus()">������&nbsp;������</a>';
    }
    el.innerHTML = '' +
		'<a class="menuItem" href="javascript:top.AddTo(\''+login+'\');contextMenus()">TO</a>'+
		'<a class="menuItem" href="javascript:top.AddToPrivate(\''+login+'\');contextMenus()">PRIVATE</a>'+
		'<img class="menuItem" height="6" src="http://img.carnage.ru/i/null.gif" />'+
		'<a class="menuItem" href="/inf.pl?user='+urlEncode(login)+'" target="_blank" onclick="contextMenus();return true;">����������</a>';
	
	if (!(!document.all || window.opera)) {
		el.innerHTML += '<a class="menuItem" href="javascript:ClipBoard(\'' + login + '\');contextMenus()">����������&nbsp;���</a>';
	}
	
    el.innerHTML += shoot +
		'<img class="menuItem" height="6" src="http://img.carnage.ru/i/null.gif" />'+
		'<a class=menuItem href="javascript:contacts2(\''+login+'\');">�&nbsp;��������</a>';
    if (top && top.ignore && top.ignore[login]) {
		el.innerHTML += '<a class="menuItem" href="javascript:unignore2(\''+login+'\');">��&nbsp;������</a>';
	}
    else { el.innerHTML += '<a class="menuItem" href="javascript:ignore2(\''+login+'\');">�&nbsp;�����</a>' }

    if (battle > 0) {
		el.innerHTML += '<a class="menuItem" href="/log.pl?log='+battle+'" target="_blank" onclick="contextMenus();return true;">����������&nbsp;���</A>'
	}
    if (!document.all || window.opera) el.innerHTML += '<a class="menuItem" href="#" onclick="return closeMenu();">�������</a>';
    
    x = CountMenuPositon(event).x;
    y = CountMenuPositon(event).y;  
    
    el.style.left = x + "px";
    el.style.top  = y + "px";
    el.style.visibility = "visible";
    
    return false;
}

function BotMenu(id,battle,meddle,attack,speaking,event) {
	var el, x, y, login;
  	
	el = document.getElementById('contextMenus') ? document.getElementById('contextMenus') : document.all("contextMenus");
  	
	if (!document.all || window.opera) el.onmouseout = new Function('return true');
  	
	var o = window.event.srcElement;
	
	if (!o) o = window.event.target;
  	
	if (o.tagName != "SPAN") return true;

  	login = o.innerText ? o.innerText : o.firstChild.nodeValue;

	window.event.returnValue = false;
	
	var i1, i2;
	if ((i1 = login.indexOf('['))>=0 && (i2 = login.indexOf(']'))>0) login=login.substring(i1+1, i2);
	
	el.innerHTML = "";
	if ( speaking > 0 ) {
	  	el.innerHTML += '<A class=menuItem HREF="javascript:speak(\''+id+'\');contextMenus()" onclick="contextMenus();return true;">����������</A>';
	}
	el.innerHTML += '<A class=menuItem HREF="javascript:top.AddTo(\''+login+'\');contextMenus()">TO</A><A class=menuItem HREF="javascript:top.AddToPrivate(\''+login+'\');contextMenus()">PRIVATE</A>';
	
	var shoot = '';
	if ( type == 'winter' ) {
		shoot = '<a class="menuItem" href="javascript:snezhok(\'' + escape(login) + '\');contextMenus()">������&nbsp;�������</a>';
	} else if(type == 'summer') {
		shoot = '<a class="menuItem" href="javascript:tort(\''+login+'\');contextMenus()">������&nbsp;������</a>';
	}
	
	el.innerHTML += 
		'<img src="http://img.carnage.ru/i/null.gif" height="6" class="menuItem" />'+
		'<a class="menuItem" href="/inf.pl?user='+urlEncode(login)+'" target=_blank onclick="contextMenus();return true;">����������</a>';
	
	if (!(!document.all || window.opera)) {
		el.innerHTML += '<a class="menuItem" href="javascript:ClipBoard(\'' + login + '\');contextMenus()">����������&nbsp;���</a>';
	}
	
	el.innerHTML += shoot;
	if (battle > 0) { 
		el.innerHTML += '<a class="menuItem" href="/log.pl?log='+battle+'" target=_blank onclick="contextMenus();return true;">����������&nbsp;���</a>';
		if ( meddle > 0) { 
			el.innerHTML += '<a class="menuItem" href="javascript:meddle(\''+id+'\');contextMenus()" onclick="contextMenus();return true;">���������</A>'  
		}
		if ( attack > 0 ) { 
			el.innerHTML += '<a class="menuItem" href="javascript:attack(\''+id+'\');contextMenus()" onclick="contextMenus();return true;">�������</A>'  
		}
	} else { 
	  	if ( attack > 0 ) { 
	  		el.innerHTML += '<a class="menuItem" href="javascript:attack(\''+id+'\');contextMenus()" onclick="contextMenus();return true;">�������</A>'  
		}
	}
	
	if (!document.all || window.opera) {
		el.innerHTML += '<a class="menuItem" href="#" onclick="return closeMenu()">�������</a>';
	}
	
	x=CountMenuPositon(event).x;
	y=CountMenuPositon(event).y;  
	
	el.style.left = x + "px";
	el.style.top  = y + "px";
	el.style.visibility = "visible";
	
	return false;
}

function BotMenuNPC(id, battle, meddle, attack, speaking, name, event){
	var monthTypes = new Array('winter', 'winter', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'winter');
	var m = new Date();
	
	var type = monthTypes[m.getMonth()];
	if (document.all && !window.opera) {
		var el, x, y, login;
		
		el = document.getElementById('contextMenus') ? document.getElementById('contextMenus') : document.all("contextMenus");
		if (!document.all || window.opera) 
			el.onmouseout = new Function('return true');
		var o = window.event.srcElement;
		if (!o) 
			o = window.event.target;
		
		login = name;
		
		el.innerHTML = "";
		if (speaking > 0) {
			el.innerHTML += '<a class="menuItem" href="javascript:speak(\'' + id + '\');contextMenus()" onclick="contextMenus();return true;">����������</A>';
		}
		
		el.innerHTML += '<a class="menuItem" href="javascript:top.AddTo(\'' + login + '\');contextMenus()">TO</A><a class="menuItem" href="javascript:top.AddToPrivate(\'' + login + '\');contextMenus()">PRIVATE</A>';
		var shoot = '';
		if (type == 'winter') {
			shoot = '<a class="menuItem" href="javascript:snezhok(\'' + escape(login) + '\');contextMenus()">������&nbsp;�������</a>';
		} else if (type == 'summer') {
			shoot = '<a class="menuItem" href="javascript:tort(\'' + login + '\');contextMenus()">������&nbsp;������</A>';
		}
		
		el.innerHTML += '<img src="http://img.carnage.ru/i/null.gif" height="6" width="10" class="menuItem">' +
		'<a class="menuItem" href="/inf.pl?user=' +
		urlEncode(login) +
		'" target=_blank onclick="contextMenus();return true;">����������</A>';
		if (!(!document.all || window.opera)) 
			el.innerHTML += '<a class="menuItem" href="javascript:ClipBoardNPC(\'' + login + '\');contextMenus()">����������&nbsp;���</A>';
		el.innerHTML += shoot;
		
		if (battle > 0) {
			el.innerHTML += '<a class="menuItem" href="/log.pl?log=' + battle + '" target=_blank onclick="contextMenus();return true;">����������&nbsp;���</A>';
			if (meddle > 0) {
				el.innerHTML += '<a class="menuItem" href="javascript:meddle(\'' + id + '\');contextMenus()" onclick="contextMenus();return true;">���������</A>'
			}
			if (attack > 0) {
				el.innerHTML += '<a class="menuItem" href="javascript:attack(\'' + id + '\');contextMenus()" onclick="contextMenus();return true;">�������</A>'
			}
		} else {
			if (attack > 0) {
				el.innerHTML += '<a class="menuItem" href="javascript:attack(\'' + id + '\');contextMenus()" onclick="contextMenus();return true;">�������</A>'
			}
		}
		if (!document.all || window.opera) 
			el.innerHTML += '<a class="menuItem" href="#" onclick="return closeMenu()">�������</a>';
		
		x = CountMenuPositon(event).x;
		y = CountMenuPositon(event).y;
		
		el.style.left = x + "px";
		el.style.top = y + "px";
		el.style.visibility = "visible";
	}
	return false;
}

function contextMenus() {
	/*
	document.getElementById("contextMenus").style.visibility = "hidden";
	document.getElementById("contextMenus").style.top="0px";
	top.frames['down'].window.document.F1.text.focus();
	*/
}

function closeMenu(event) {
	/*
	if (window.event && window.event.toElement && top.getIEVersion()){
		var cls = window.event.toElement.className;
		if (cls == "menuItem" || cls == "contextMenus") {
			return;
		}
	}
	var contextMenus = document.getElementById('contextMenus');
	contextMenus.style.visibility = "hidden";
	contextMenus.style.top = "0px";
	return false;
	*/
}

function tort(text){
	text = unescape(text);
	
	if (top.frames['chat'] && top.frames['chat']['chat2'].oChat) {
		top.frames['chat']['chat2'].oChat.throwThing(text, 'cake');
	}	
}

function snezhok(text){
	text = unescape(text);
	
	if (top.frames['chat'] && top.frames['chat']['chat2'].oChat) {
		top.frames['chat']['chat2'].oChat.throwThing(text, 'snowball');
	}
}

function attack(id){
	top.frames['carnage'].document.location.href = '/main.pl?cmd=attack_bot&bot=' + id;
}

function meddle(id){
	top.frames['carnage'].document.location.href = '/main.pl?cmd=meddle_bot&bot=' + id;
}

function speak(id){
	top.frames['carnage'].document.location.href = '/npc.pl?cmd=npc&nid=' + id + '&' + Math.random();
}

function Ignore(login){
	top.ignore[login] = true;
	top.chat.chat2.mes.innerHTML += '<font class=date>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font> �������� <span>' + login + '</span> ������ � ������ ������<br />';
}

function UnIgnore(login){
	delete top.ignore[login];
	top.chat.chat2.mes.innerHTML += '<font class=date>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font> �������� <span>' + login + '</span> �������� �� ������� ������<br />';
}

function ignore2(login){
	login = unescape(login);
	window.top.frames['carnage'].location = '/friends.pl?cmd=ignored&suser=' + login + '&' + Math.random();
	contextMenus();
}

function unignore2(login){
	login = unescape(login);
	window.top.frames['carnage'].location = '/friends.pl?cmd=ignored&duser=' + login + '&' + Math.random();
	contextMenus();
}

function contacts2(login){
	login = unescape(login);
	window.top.frames['carnage'].location = '/friends.pl?cmd=contacts&suser=' + login + '&' + Math.random();
	contextMenus();
}
