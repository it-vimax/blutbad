function checkHasPass(field) {
	hiddenFields = $('#row-pass label, #row-pass #password');
	if(field.value.length == 10) {
		$.post('/bank.pl', {
			cmd: 'haspass',
			bank: field.value,
			is_ajax: 1
		}, function(data) {
			if(data == '1') {
				hiddenFields.css('visibility', 'visible');
				$('#password').removeAttr('disabled').focus();
			} else {
				hiddenFields.css('visibility', 'hidden');
				$('#password').attr('disabled', true);
			}
		});
	} else {
		hiddenFields.css('visibility', 'hidden');
		$('#password').attr('disabled', true);
	}
}

$(function() {
	var accounts = $('#divAccounts');
	var accountField = $('#logAccount').keyup(function() {
		checkHasPass(this);
	});
	
	if(accountField.val()) {
		checkHasPass(accountField[0]);
	}
	
	if(accounts.length) {
		var buttonAccounts = $('#buttonAccounts');
		var position = new Array();
		
		accounts.dialog({
			autoOpen: false,
			height: 'auto',
			minHeight: 0,
			width: ''
		});
		
		buttonAccounts.click(function() {
			if(accounts.dialog('isOpen')) {
				accounts.dialog('close');
			} else {
				var offset = buttonAccounts.offset();
				position[0] = offset.left;
				position[1] = offset.top + buttonAccounts.outerHeight() + 2;
				accounts.dialog('option', 'position', position);
				accounts.dialog('open');
			}
		});
		
		accounts.find('a').click(function() {
			accountField.val($(this).text());
			accounts.dialog('close');
			
			checkHasPass(accountField[0]);
			
			return false;
		});
	}
});