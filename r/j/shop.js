var show_items = new Array();

function setSelect( name, value ) {
    var s = document.all[name];
    for( var i = 0; i < s.options.length; i++ ) {
		if ( s.options[i].value == value ) {
			s.selectedIndex = i;
			break;
		}
    }
}
function itemOut(number, item_main, item_property, item_request, sell_price, item_feat_list){
	var s = ''
	s += '' +
		'<tr>' +
			'<th>' + number + '</th>' +
			'<td class="img">' +
				'<div class="slot">';
			
	if (item_main.big == 1) {
		s += '' +
					'<a href="' + item_main.image_full + '" target="_blank">' +
						'<img src="' + item_main.image_preview + '" onmouseout="hh()" onmouseover="show_item(' + number + ',event)" />' +
					'</a>';
	} else {
		s += 		'<img src="' + item_main.image_preview + '" onmouseout="hh()" onmouseover="show_item(' + number + ',event)" />';
	}
	
	if(item_feat_list && item_feat_list.length) {
		s += 		'<ul class="enchants">';
		debug(item_feat_list);
		for(var i = 0; i < item_feat_list.length; i++) {
			if(item_feat_list[i].source == '�������������') {
				s +=	'<li class="enchant-' + item_feat_list[i].level + '">' +
							'<img alt="" class="tooltip" height="8" src="http://img.carnage.ru/i/enchant/enchant_' + item_feat_list[i].level + '.gif" title="' + item_feat_list[i].title + '" width="10" />' +
						'</li>';
			}
		}
		
		s +=		'</ul>';
	}
	
	s +=		'</div>' +
			'</td>' +
			'<td class="description">' +
				description(item_main, item_property, item_request,null,null,item_feat_list) +
			'</td>' +
			'<td>' +
				'<ul class="actions">' +
					'<li>' +
						sell_price + ' ��. ' +
						'<img src="http://img.carnage.ru/i/baccept.gif" alt="" title="�������" style="cursor:hand;" onclick="setSell(\'' + item_main.name + '\',\'' + item_main.entry + '\',\'' + item_main.madein + '\',' + sell_price + ');" />' +
					'</li>' +
				'</ul>' +
			'</td>' +
		'</tr>';
	document.write(s);
	show_items[number] = {
		'main': item_main,
		'property': item_property,
		'request': item_request
	};
}

function shopOut(number, shop_main, shop_property, shop_request, count, item_add){
	shop_main.durability0 = shop_main.durability;
	shop_main.durability = 0;
	var s = '';
	s += '' +
		'<tr>' +
			'<th>' + number + '</th>' +
			'<td class="img">';
	if (shop_main.big == 1) {
		s += '' +
				'<a href="' + shop_main.image_full + '" target="_blank">' +
					'<img alt="" src="' + shop_main.image_preview + '" data="' + number + '" />' +
				'</a>';
	} else {
		s += '' +
				'<img alt="" src="' + shop_main.image_preview + '" data="' + number + '" />';
	}
	s += '' +
			'</td>' +
			'<td class="description">' +
				description(shop_main, shop_property, shop_request, count, item_add) +
			'</td>' +
			'<td>' +
				'<ul class="actions">';
	var val = (count == 0) ? 0 : 1;
	s += '' +
					'<li>' +
						'<input class="text number" id="buy_' + shop_main.type + '_' + shop_main.num + '" name="buy_' + shop_main.type + '_' + shop_main.num + '" type="text" size="6" value="' + val + '" /> '+
						'<span>��.</span> ' +
						'<img src="http://img.carnage.ru/i/baccept.gif"';
	if (val > 0) {
		s += ' alt="������" onclick="setBuy(\'' + shop_main.name + '\',\'' + shop_main.type + '\',\'' + shop_main.num + '\');" title="������"';
	} else {
		s += ' alt="���������� ������" class="disabled" title="���������� ������"';
	}
	s += 					' />';
	s += '' +
					'</li>';
				
	if ((shop_main.type == 41) || (shop_main.type == 40)) { //flowers or souvenirs
		s += '' +
					'<li>' +
						'<b>��������</b> ' +
						'<img src="http://img.carnage.ru/i/baccept_1.gif"';
		if (val > 0) {
			s += ' style="cursor:pointer;" title="��������" alt="��������" onclick="setGiveCard(\'' + shop_main.name + '\',\'' + shop_main.type + '\',\'' + shop_main.num + '\',\'' + shop_main.price + '\');"';
		} else {
			s += ' class="gray" title="���������� ��������" alt=""'
		}
		s += 				' >';
		s += '' +
					'</li>';
	}
	
	s += '' +
				'</ul>' +
			'</td>' +
		'</tr>';
		
	document.write(s);
	
	show_items[number] = {
		'main': shop_main,
		'property': shop_property,
		'request': shop_request
	};
}

function descriptionAlt(item_main, item_property, item_request, item_add){
	var st = 'class="attention"';
	var s = '';
	s = '<b>' + item_main.name + '</b> ';
	if (user.maxWeight < (user.itemsWeight + item_main.weight)) {
		s += '<font color=red>(�����: ' + item_main.weight + ')</font><br />'
	} else {
		s += '(�����: ' + item_main.weight + ')<br />'
	}
	s += descriptionMain(item_main, item_property, item_request, st, item_add);
	return s;
}

function description(item_main, item_property, item_request, count, item_add,item_feat_list){
	var st = 'class="attention"';
	var s = '';
	if (count) {
		if (count == 0) {
			s += '<font color=red>����������: <i>' + count + '</font></i> (<a href="#" onclick="this.blur(); javascript:{object=window.open(\'?cmd=item.where&type=' + item_main.type + '&num=' + item_main.num + '\',\'show_where\',\'height=200,width=300,resizable=no,scrollbars=yes,menubar=no,status=no,toolbar=no,top=\'+(window.screen.height-200)/2+\',left=\'+(window.screen.width-300)/2); object.focus();return false;}"> <em>��� ��� ����� ����� ���� �������?</em> </a>)<br />'
		} else {
			s += '����������: <i>' + count + '</i><br />'
		}
	}
	s += '<b>' + item_main.name + '</b> ';
	if (user.maxWeight < (user.itemsWeight + item_main.weight)) {
		s += '<font color="red">(�����: ' + item_main.weight + ')</font>'
	} else {
		s += '(�����: ' + item_main.weight + ')'
	}
	if (item_main.old == 1) {
		s += ' <img alt="������� �������" src="http://img.carnage.ru/i/old.gif" title="������� �������" />';
	}
	if (item_main.soulbound == 1) {
		s += ' <img alt="������� �������" src="http://img.carnage.ru/i/soulbound.gif" title="������� �������" />';
	}
	if (item_main.rolling == 1) {
		s += ' <img alt="������� ���� � ������" src="http://img.carnage.ru/i/rolling.gif" title="������� ���� � ������" />';
	}
	if (item_main.gift == 1) {
		s += ' <img alt="������� �� ' + item_main.fromuser + '" src="http://img.carnage.ru/i/present.gif" title="������� �� ' + item_main.fromuser + '" />';
	}
	s += '<br />';
	s += descriptionMain(item_main, item_property, item_request, st, item_add);
	
	if(item_feat_list && item_feat_list.length > 0 ){
		s += '<b>�������������:</b><ul class="item-description-list">';
		for(var i=0; i < item_feat_list.length; i++){
			s += '<li>' + item_feat_list[i].title + '</li>';
		}
		s += '</ul>';
	}
	
	if (item_main.text) {
		s += '<b>�����:</b><br />' + item_main.text + '<br />';
	}
	
	return s;
}

function descriptionMain(item_main, item_property, item_request, st, item_add){
	var s = '';
	if (user.money < item_main.price) {
		s += '<font color="red"><i>����: <b>' + item_main.price + '</b>'+(item_main.old_price > 0 ? '(<b style="text-decoration:line-through">'+item_main.old_price+'</b>)' : '') +' ��.</i></font><br />'
	} else {
		s += '<i>����: <b>' + item_main.price + '</b>'+(item_main.old_price > 0 ? '(<b style="text-decoration:line-through">'+item_main.old_price+'</b>)' : '')+' ��.</i><br />'
	}
	s += '�������������: ' + item_main.durability + '/' + item_main.durability0 + '<br />';
	if (item_main.valid != "") {
		s += '���� ��������:&nbsp;' + item_main.valid + '<br />'
	}
	var s1 = '';
	
	if (item_request.level > 0) {
		if (user.level < item_request.level) {
			s1 += '<li ' + st + '>�������:&nbsp;' + item_request.level + '</li>'
		} else {
			s1 += '<li>�������:&nbsp;' + item_request.level + '</li>'
		}
	}
	if (item_request.str > 0) {
		if (user.strength < item_request.str) {
			s1 += '<li ' + st + '>����:&nbsp;' + item_request.str + '</li>'
		} else {
			s1 += '<li>����:&nbsp;' + item_request.str + '</li>'
		}
	}
	if (item_request.dex > 0) {
		if (user.dexterity < item_request.dex) {
			s1 += '<li ' + st + '>��������:&nbsp;' + item_request.dex + '</li>'
		} else {
			s1 += '<li>��������:&nbsp;' + item_request.dex + '</li>'
		}
	}
	if (item_request.suc > 0) {
		if (user.success < item_request.suc) {
			s1 += '<li ' + st + '>��������:&nbsp;' + item_request.suc + '</li>'
		} else {
			s1 += '<li>��������:&nbsp;' + item_request.suc + '</li>'
		}
	}
	if (item_request.end > 0) {
		if (user.endurance < item_request.end) {
			s1 += '<li ' + st + '>����������������:&nbsp;' + item_request.end + '</li>'
		} else {
			s1 += '<li>����������������:&nbsp;' + item_request.end + '</li>'
		}
	}
	if (item_request.intel > 0) {
		if (user.intelligence < item_request.intel) {
			s1 += '<li ' + st + '>���������:&nbsp;' + item_request.intel + '</li>'
		} else {
			s1 += '<li>���������:&nbsp;' + item_request.intel + '</li>'
		}
	}
	if (item_request.fishing_skill && item_request.fishing_skill > 0) {
		if (user.fishing_skill < item_request.fishing_skill) 
			s1 += '<li ' + st + '>����� �����������:&nbsp;' + item_request.fishing_skill + '</li>';
		else 
			s1 += '<li>����� �����������:&nbsp;' + item_request.fishing_skill + '</li>';
	}
	if (item_request.lic_war == 1) {
		if (user.warrior == 0) {
			s1 += '<li ' + st + '>�������� ��������</li>'
		} else {
			s1 += '<li>�������� ��������</li>'
		}
	}
	if (item_request.lic_med == 1) {
		if (user.medic == 0) {
			s1 += '<li ' + st + '>�������� ������</li>'
		} else {
			s1 += '<li>�������� ������</li>'
		}
	}
	
	if (s1) {
		s1 = '<b>����������:</b><br />' + '<ul class="item-description-list">' + s1 + '</ul>';
	}
	
	s += s1;
	
	var s0 = '';
	
	if (item_property.strength > 0) {
		s0 += '<li>����:&nbsp;+' + item_property.strength + '</li>';
	}
	if (item_property.dexterity > 0) {
		s0 += '<li>��������:&nbsp;+' + item_property.dexterity + '</li>';
	}
	if (item_property.success > 0) {
		s0 += '<li>��������:&nbsp;+' + item_property.success + '</li>';
	}
	if (item_property.endurance > 0) {
		s0 += '<li>������� �����:&nbsp;+' + item_property.endurance + '</li>';
	}
	if (item_property.weariness > 0) {
		s0 += '<li>������������:&nbsp;+' + item_property.weariness + '</li>';
	}
	if (item_property.intelligence > 0) {
		s0 += '<li>���������:&nbsp;+' + item_property.intelligence + '</li>';
	}
	if (item_property.krit > 0) {
		s0 += '<li>����������� ����:&nbsp;+' + item_property.krit + '</li>';
	}
	if (item_property.ukrit > 0) {
		s0 += '<li>������ ������������ �����:&nbsp;+' + item_property.ukrit + '</li>';
	}
	if (item_property.uvor > 0) {
		s0 += '<li>�����������:&nbsp;+' + item_property.uvor + '</li>';
	}
	if (item_property.uuvor > 0) {
		s0 += '<li>������ �����������:&nbsp;+' + item_property.uuvor + '</li>';
	}
	if (item_property.uronmin > 0) {
		s0 += '<li>����������� ����:&nbsp;+' + item_property.uronmin + '</li>';
	}
	if (item_property.uronmax > 0) {
		s0 += '<li>������������ ����:&nbsp;+' + item_property.uronmax + '</li>';
	}
	if (item_property.b1 > 0) {
		s0 += '<li>����� ������:&nbsp;+' + item_property.b1 + '</li>';
	}
	if (item_property.b2 > 0) {
		s0 += '<li>����� �������:&nbsp;+' + item_property.b2 + '</li>';
	}
	if (item_property.b3 > 0) {
		s0 += '<li>����� �����:&nbsp;+' + item_property.b3 + '</li>';
	}
	if (item_property.b4 > 0) {
		s0 += '<li>����� ���:&nbsp;+' + item_property.b4 + '</li>';
	}
	if (item_property.fishing_bonus && item_property.fishing_bonus > 0) {
		s0 += '<li>����� �����������:&nbsp;+' + item_property.fishing_bonus + '</li>';
	}
	if (item_property.abrasion > 0) {
		s0 += '<li>����������� ������������:&nbsp;' + item_property.abrasion + '%</li>';
	}
	if (item_property.duration > 0) {
		s0 += '<li>����������������� ��������:&nbsp;' + item_property.duration + ' ���.</li>';
	}
	
	if (s0) {
		s0 = '<b>��������:</b><br />' + '<ul class="item-description-list">' + s0 + '</ul>';
	}
	
	s += s0;
	if (item_main.action) {
		s += '<b>��������:</b><br />' + item_main.action;
	}
	if (item_main.descr) {
		s += '<b>��������:</b><br />' + item_main.descr + '<br />';
	}
	
	if (item_add) {
		s += '<b>' + item_add.title + '</b>';
		s += '<ul class="item-description-list">';
		for (var i in item_add.list) {
			s += "<li>" + item_add.list[i] + "</li>";
		}
		s += '</ul>';
	}
	
	return s;
}

function movehint(ev){
	if (!ev) 
		ev = window.event;
	var hint1 = document.getElementById('hint1');
	if (hint1 && hint1.style.visibility == 'visible') {
		setpos(ev)
	}
	return (true);
}

function big(type, num){
	window.open("http://enc.carnage.ru/" + type + "/" + num + ".html", "displayWindow", "height=600,width=540,resizable=yes,scrollbars=yes,menubar=no,status=no,toolbar=no,directories=no,location=no")
}

function show_item(number){	
	var da = '';
	
	var img = show_items[number];
	da += '' +
		'<table  class="hintspan">' +
			'<tr>' +
				'<td>';
	if (compared_main) {
		da += 		'<font color="#94A1AC"><b>�������:</b></font><br />';
	}
	
	da += 			descriptionAlt(img.main, img.property, img.request) + 
				'</td>';
	
	if (compared_main) {
		da += 	'<td>&nbsp;</td>' +
				'<td style="border-left: 1px dotted black;">&nbsp;</td>' +
				'<td valign="top" align="left">' +
					'<font color="#94A1AC"><b>������:</b></font><br />' + 
					descriptionAlt(compared_main, compared_property, compared_request) + 
				'</td>';
	}
	da += 	'</tr>' +
		'</table>';
	
	return da;
}

function setpos(event){
	var x, y;
	var hint1 = document.getElementById('hint1');
	if (event.clientX + hint1.clientWidth + 20 >= document.body.clientWidth) {
		x = document.documentElement.scrollLeft + event.clientX - hint1.clientWidth - 10;
	} else {
		x = event.clientX + document.documentElement.scrollLeft + 10;
	}
	
	if (event.clientY + hint1.clientHeight + 20 >= document.documentElement.clientHeight) {
		y = document.documentElement.scrollTop + document.documentElement.clientHeight - hint1.clientHeight - 20;
	} else {
		y = event.clientY + document.documentElement.scrollTop + 20;
	}
	
	hint1.style.left = x + 'px';
	hint1.style.top = y + 'px';
}

function hh(){
	document.getElementById('hint1').style.visibility = 'hidden';
}

/*function sh2(d, event){
	if (!event) 
		event = window.event;
	var hint1 = document.getElementById('hint1');
	if (hint1) {
		var s = '<table cellpadding=1 cellspacing=0><tr><td align=left nowrap><small>' + d + '</small></td></tr>';
		s += '</table>';
		hint1.innerHTML = s;
		setpos(event);
		hint1.style.visibility = 'visible';
		document.onmousemove = movehint;
	}
}*/

$(function() {
	$('.shop-table .img img[data]').tooltip($.extend({}, tooltipDefaults, {
		bodyHandler: function() {
			return show_item($(this).attr('data'))
		}
	}));
	
	$('.actions .text.number').keyup(function(event) {
		if(event.keyCode == 13) {
			$(this).nextAll('img')[0].onclick();
		}
	});
});
