function getTooltip(img, t, d, p) {
	var type, num;
	if ((img.indexOf('/') > 0) && (img.indexOf('.') > 0)) {
		j = img.indexOf('/');
		i = img.indexOf('.');
		type = img.substr(0, j);
		num = img.substr(j + 1, i - j - 1);
	}
	
	var tooltip = '';
	tooltip += 	'&lt;div class=&quot;item&quot;&gt;' +
					'&lt;div class=&quot;title&quot;&gt;' + t +'&lt;/div&gt;' +
					'Долговечность: ' + d;
	
	if(p) {
		tooltip += '&lt;ul class=&quot;description&quot;&gt;';
		
		var arr = p.split(',');
		for(var i = 1; i < arr.length; i++) {
			tooltip += 	'&lt;li&gt;' + arr[i] + '&lt;/li&gt;';
		}
		
		tooltip += '&lt;/ul&gt;';
	}
	
	tooltip +=	'&lt;/div&gt;';
	
	return tooltip;
}

function items(col, img, x, y, alpha, t, d, p, slot, part, use, big, nd){	
	var tooltip = getTooltip(img, t, d, p);
	
	document.write('<img alt="" data="' + tooltip + '" src="http://img.carnage.ru/i/' + img + '" style="filter:alpha(opacity=' + alpha + '); opacity: ' + (alpha / 100) + '" title="' + t + '" />');
}

function runes(img, alpha, t, d, p, slot, use){
	var tooltip = getTooltip(img, t, d, p);
	
	document.write('<img height="35" data="' + tooltip + '"' + (use ? ' class="active" onclick="' + use + '(\'' + slot + '\')' + '"' : '') + ' src="http://img.carnage.ru/i/' + img + '" style="filter:alpha(opacity=' + alpha + ') opacity: ' + (alpha / 100) + '" width="35" />');
}

function processRefreshButton( buttonId ) {
	var oButton = document.getElementById( buttonId );

	if ( oButton ) {
		setTimeout( function() {
			oButton.className = '';

			oButton.onclick = function() {
				this.onclick = null;
				this.className = 'disabled';
				location.href = '?' + Math.random();

			};

			oButton = null;

		}, 3000 );

	}

}

$(function() {
	$('.char-items img[data]').tooltip($.extend({}, tooltipDefaults, {
		bodyHandler: function(){
			return $(this).attr('data');
		}
	}));
});

if ($.browser.opera && $.browser.version < 10.5) {
	var dblClickTimeout;
	var clickDone = false;
	
	$(function(){
		$('.context').bind('click', function(event){
			if (!clickDone) {
				var $this = $(this);
				clickDone = true;
				dblClickTimeout = setTimeout(function(){
					var name = $this.text();
					if (top && top.AddTo) {
						top.AddTo(name, true);
					}
					clickDone = false;
				}, 250);
			}
		}).bind('dblclick', function(event){
			clickDone = false;
			clearTimeout(dblClickTimeout);
		});
	});
} else {
	$(function(){
		$('.context').bind('click', function(event){
			var name = $(this).text();
			if (top && top.AddTo) {
				top.AddTo(name, true);
			}
		});
	});
}