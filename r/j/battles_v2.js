var botBattle;
var botBattlePanel;
var currBlock = 1;
var crossArray = [];
crossArray[1] = ['1','2','3','4'];
crossArray[2] = ['12','23','34','14','13','24'];
var alignname = {
	0.98: '���� ����',
	0.99: '���� �����',
	0.97: '���� �����',
	0.96: '���� �������',
	0.198: '������ ����',
	0.199: '������ �����',
	0.197: '������ �����',
	0.196: '������ �������',
	2.2: '�����'
};

function keypress(event){
	var e = event;
	var code = e.keyCode - 48;
	
	// ����������� ������� ����� �� �������� ����������
	if(code >=49 && code <= 52)
		code -= 48;
		
	var form = document.forms.bform;
	
	if (botBattle && !botBattlePanel) {
		if (form && e.keyCode == 13) {
			form.submit();
		}
		return;
	}
	
	if (form && currBlock < 4 && code > 0 && code <= 6) {
		switch (currBlock) {
			case 1:
				if (setBlock('A', attackCount, code)) {
					currBlock++
				};
				break;
				
			case 2:
				if (setBlock('D', defendCount, code)) {
					currBlock++
				};
				break;
			case 3:
				if (!botBattle && setPosition(code)) {
					currBlock++
				};
				break;
		}
		checkSubmit(form);
	} else if (e.keyCode == 27) {
		currBlock = 1;
		setBoxes('A', '');
		setBoxes('D', '');
		document.getElementById('A5').checked = false;
		document.getElementById('D5').checked = false;
		checkForm(0);
	} else if(e.keyCode == 13 && (currBlock == 4 || (botBattle && currBlock == 3))) { 
		form.submit();
	}
}

function setBlock(prefix,maxitems,code) {
	if ( ( maxitems == 1 && code <= 4 ) || ( maxitems == 2 && code <= 6 ) ) {
		setBoxes(prefix, crossArray[maxitems][code - 1]);
		checkBlock(maxitems, prefix);
		return 1;
	}
	return 0;
}

function setPosition(code) {
	if ( code == 1 ) {
		document.getElementById('A5').checked=true;
		return 1;
	} else if ( code == 2 ) {
		document.getElementById('D5').checked=true;
		return 1;
	}
	return 0;
}

function setBoxes(prefix,boxes) {
	for ( var i = 1; i <= 4; i++ ) {
		document.getElementById(prefix+i).checked = false;
	}
	for ( var i = 1; i <= boxes.length; i++ ) {
		document.getElementById(prefix+boxes.substr(i-1,1)).checked = true;
	}
}

function wr(str) {
	document.write(str);
}

function img(str,w,h,alt) {
	wr('<img class="vam" src="http://img.blutbad.ru/i/'+str+'.gif" width="'+w+'" height="'+h+'" alt="'+alt+'" title="'+alt+'"/>');
}

function imgbl(str,w,h,alt) {
	return '<img class="vam" src="http://img.blutbad.ru/i/'+str+'.gif" width="'+w+'" height="'+h+'" alt="'+alt+'" title="'+alt+'"/>';
}

//##=f23=##
function zr1() {
	img('zarnica2009_1',25,16,'������� ������');wr(' ');
}

function zr2() {
	img('zarnica2009_2',25,16,'������� �������');wr(' ');
}

function zr3() {
	img('zarnica2009_3',25,16,'������� �������');wr(' ');
}

//##=a1=##
function zra1() {
	img('zarnica1',25,16,'������� &laquo;����&raquo;');wr(' ');
}

function zra2() {
	img('zarnica2',25,16,'������� &laquo;Green Peace&raquo;');wr(' ');
}

function zra3() {
	img('zarnica3',25,16,'������� &laquo;���������&raquo;');wr(' ');
}

function zra4() {
	img('zarnica4',25,16,'������� &laquo;����&raquo;');wr(' ');
}

function zra5() {
	img('zarnica5',25,16,'������� &laquo;������&raquo;');wr(' ');
}

//##=a1=##
function wr1() {
	img('zarnica2009_1',25,16,'������� ���������');wr(' ');
}

function wr2() {
	img('zarnica2009_2',25,16,'������� �������');wr(' ');
}

function wr3() {
	img('zarnica2009_3',25,16,'������� ��������');wr(' ');
}

//##=f23=##
function zr1bl() {
	return imgbl('zarnica2009_1',25,16,'������� ������');
}

function zr2bl() {
	return imgbl('zarnica2009_2',25,16,'������� �������');
}

function zr3bl() {
	return imgbl('zarnica2009_3',25,16,'������� �������');
}

//##=a1=##
function wr1bl() {
	return imgbl('zarnica2009_1',25,16,'������� ���������');
}

function wr2bl() {
	return imgbl('zarnica2009_2',25,16,'������� �������');
}

function wr3bl() {
	return imgbl('zarnica2009_3',25,16,'������� ��������');
}

function u(name, level, align, klan, klanname, guild, guildname, sex, side, alt, fPrivate, fClickable, fPrint){
	var alignstr = getAlignName(align);
	var user = '';

	if(fPrivate) {
		user += '<img alt="" class="private" src="http://img.blutbad.ru/i/pr.gif" onclick="top.AddToPrivate(\'' + name + '\', top.CtrlPress)" title="��������� ���������" />';
	}

	if (align != 0) {
		user += '<img alt="" class="align" src="http://img.blutbad.ru/i/align_' + align + '.gif" title="' + alignstr + '" />';
	}

	if (klan.length > 0) {
		user += '<img alt="" class="clan" src="http://img.blutbad.ru/i/klan/' + klan + '.gif" title="' + klanname + '" />';
	} else if (guild.length > 0) {
		user += '<img alt="" class="guild" src="http://img.blutbad.ru/i/guild/' + guild + '.gif" title="' + guildname + '" />';
	}

	if (fClickable) {
		if (alt && alt != '%alt%') {
			alt = ' style="text-decoration: underline;" onmouseover="sh2(\'' + alt + '\')" onmouseout="hh()"'
		} else {
			alt = '';
		};

		user += ' <b' + (alt ? alt : '') + ' class="name context user' + (side ? ' side-' + side : '') + '" onclick="top.AddTo(\'' + name + '\')">' + name + '</b>';
	} else {
		user += ' <b class="name' + (side ? ' side-' + side : '') + '" onclick="top.AddTo(\'' + name + '\')">' + name + '</b>';
	}

	user += ' ' +
			'<span class="level">[' + level + ']</span>' + ' ' +
			'<a class="info" href="inf.php?user=' + name + '" target="_blank">' +
				'<img alt="" class="sex" src="http://img.blutbad.ru/i/inf' + sex + '.gif" title="���������� � ���������" />' +
			'</a>';
	
	user = '<span class="nick">' + user + '</span>';

	if (fPrint) {
		wr(user);
	}

	return user;
}

function tr(id, rownum, time, icon, description, timeout, begintime, side1, side1count, side1descr, side2, side2count, side2descr, action, ismy, mode, actions, nd, dur, settings, battle_settings){
	var bgcolor;
	
	if (ismy) {
		bgcolor = 'bg_other';
	} else if (rownum % 2) {
		bgcolor = 'bg_odd';
	} else {
		bgcolor = 'bg_even';
	}
	
	document.write(
		'<tr class="' + bgcolor + '">' +
			'<th>' + rownum + '</th>' +
			'<td class="date" title="����� �������� ������">' + time + '</td>' +
			'<td class="type">' +
				'<table>' +
					'<tbody>' +
						'<tr>' +
							'<th class="side-1">' +
								'(' + side1count + ')' +
								'<div class="hidden">(' + side2count + ')</div>' +
							'</th>' +
							'<th class="img">' +
								'<img alt="��� ���" src="http://img.blutbad.ru/i/' + icon + '" title="' + description + '" />' +
							'</th>' +
							'<th class="side-2">' +
								'(' + side2count + ')' +
								'<div class="hidden">(' + side1count + ')</div>' +
							'</th>' +
						'</tr>' +
					'</tbody>' +
				'</table>' +
			'</td>'
	);
	if (mode == 'group') {
		document.write('<td class="nowrap" title="����������� �� ���������� � ������">' + side1descr + ' vs ' + side2descr + '</td>');
	}

	if (mode == 'tzar') {
		document.write('<td class="nowrap" title="����������� �� ���������� � ������">' + side1descr + '</td>');
	}

	var price = '';
	if (mode == 'haot') {
		/*var descr = '';
		if (!settings) settings = new Array();
		if (!battle_settings) battle_settings = new Object();

		if (battle_settings.sesterce > 0) {
			price += battle_settings.sesterce+'���.';
		}
		if (battle_settings.florine > 0) {
			if (price) price += ' ';
			price += battle_settings.florine+'��.';
		}
		if (!price) price = '���������';
		descr += '<b>����</b>: '+price+'<br>';

		for (var i=0;i<settings.length;i++) {
			var item = settings[i];
			if (!battle_settings[item.name]) continue;

			descr += '<b>'+item.title+'</b>: ';

			if (item.type == 'select') {
				descr += battle_settings_select_value(battle_settings,item);
			}
			if (item.type == 'flag') {
				descr += battle_settings_flag_value(battle_settings,item);
			}

			descr += '<br>';
		}*/

		document.write('<td class="nowrap" onmouseover="sh2(\''+battle_settings+'\')" onmouseout="hh()">��-��</td>');
	}
	
	document.write(
		'<td class="nowrap" title="�������">' + timeout + ' ���.</td>' +
		'<td class="nowrap" title="������������">' + dur + '</td>'
	);
	
	if (begintime >= 0) {
		if (begintime < 0) 
			begintime = 0;
			
		var secs = begintime % 60;
		var mins = (begintime - secs) / 60;
		var clas = begintime < 60 ? 'reddate' : 'date';
		
		mins = mins < 10 ? '0' + mins : mins;
		secs = secs < 10 ? '0' + secs : secs;
		document.write('' + 
			'<td class="timeout ' + clas + '" data="' + begintime + '" title="��� �������� �����">' + 
				mins + ':' + secs + 
			'</td>');
	}
	
	document.write('<td class="nick-col">');
	document.write('<div class="inner">');
	if (typeof side1 == 'object' && typeof side2 == 'object') {
		document.write(getSideHTML(side1, 1) + (side2.length ? ' <li class="vs">������</li> ' + getSideHTML(side2, 2) : ''));
	} else if(typeof side1 == 'object' &&  typeof side2 != 'object') {
		document.write(getSideHTML(side1, 1) + (side2.length ? '</ul> <b class="vs">������</b>  ' + side2 : ''));
	} else if(typeof side1 != 'object' &&  typeof side2 == 'object') {
		document.write(side1 + (side2.length ? ' <b class="vs">������</b> <ul>' + getSideHTML(side2, 2) : ''));
	} else {
		document.write(side1 + (side2.length ? ' <b class="vs">������</b> ' + side2 : ''));
	}
	document.write('</div>');
	document.write('</td>');

	if (action == 'accept') {
		action = 	'<a href="#" onclick="accept_haot_battle(\''+price+'\',\'?level=' + mode + '&cmd=' + mode + '.accept&nd=' + nd + '&battle_id=' + id + '\'); return false;">' +
						'<img alt="������� ������" src="http://img.blutbad.ru/i/baccept.gif" title="������� ������" />' +
					'</a>';
	} else if (action == 'accept00') {
		action = 	'<img alt="�������� ����������" class="disabled" src="http://img.blutbad.ru/i/baccept.gif" title="�������� ����������" />' +
					'&nbsp;&nbsp;' +
					'<img alt="�������� ����������" class="disabled" src="http://img.blutbad.ru/i/baccept.gif" title="�������� ����������" />';
	} else if (action == 'accept10') {
		action = 	'<a href="?level=' + mode + '.accept&side=1&nd=' + nd + '&battle_id=' + id + '">' +
						'<img src="http://img.blutbad.ru/i/baccept_1.gif" alt="����� �� 1-� �������" class="vam" />' +
					'</a>' +
					'&nbsp;&nbsp;' +
					'<img alt="�������� ����������" class="disabled" src="http://img.blutbad.ru/i/baccept.gif" title="�������� ����������" />';
	} else if (action == 'accept01') {
		action = 	'<img src="http://img.blutbad.ru/i/baccept.gif" class="disabled" alt="�������� ����������" title="�������� ����������" />' +
					'&nbsp;&nbsp;' +
					'<a href="?level=' + mode + '.accept&side=2&nd=' + nd + '&battle_id=' + id + '">' +
						'<img src="http://img.blutbad.ru/i/baccept_2.gif" alt="����� �� 2-� �������" title="����� �� 2-� �������" />' +
					'</a>';
	} else if (action == 'accept11') {
		action = 	'<a href="?level=' + mode + '.accept&side=1&nd=' + nd + '&battle_id=' + id + '">' +
						'<img alt="����� �� 1-� �������" src="http://img.blutbad.ru/i/baccept_1.gif" title="����� �� 1-� �������" />' +
					'</a>' +
					'&nbsp;&nbsp;' +
					'<a href="?level=' + mode + '.accept&nd=' + nd + '&side=2&battle_id=' + id + '">' +
						'<img alt="����� �� 2-� �������" src="http://img.blutbad.ru/i/baccept_2.gif" title="����� �� 2-� �������" />' +
					'</a>';
	} else if (action == 'cancel') {
		action = 	'<a href="?level=' + mode + '.cancel&nd=' + nd + '&battle_id=' + id + '">' +
						'<img alt="��������" src="http://img.blutbad.ru/i/bcancel.gif" title="�������� ������" />' +
					'</a>';
	} else {
		action = 	'<img alt="����������" src="http://img.blutbad.ru/i/' + (ismy ? 'bcancel' : 'baccept') + '.gif" class="disabled" title="�������� ����������"/>'
	}
	
	if (actions >= 0) {
		document.write('<td class="nowrap"> ' + action + '</td>');
	}
	document.write('</tr>');
}

function trc(id, rownum, time, icon, description, side1, side1live, side1count, side2, side2live, side2count, stopped, closed, winner) {
	var html = '';
	
	closed = Number(closed);
	var bgcolor;
	if (rownum % 2) {
		bgcolor = 'bg_odd'
	} else {
		bgcolor = 'bg_even'
	}
	
	var href = '/log.pl?id=' + id;
	
	html += '' +
		'<tr class="' + bgcolor + '">' +
			'<th>' + rownum + '</th>' +
			'<td class="date" title="����� ������ ���">' + time + '</td>' +
			'<td class="date" title="����� ��������� ���">' + stopped + '</td>' +
			'<td class="type" onclick="window.open(\'' + href + '\');" style="cursor: pointer;" title="��� ���">';
	
	var side1html = '';
	if (side1live) {
		side1html = '<span class="tooltip" title="�����: ' + side1live + '&lt;br /&gt;�����: ' + side1count + '">(' + side1live + '/' + side1count + ')</span>';
	} else {
		side1html = '(' + side1count + ')';
	}
	
	var side2html = '';
	if (side2live) {
		side2html = '<span class="tooltip" title="�����: ' + side2live + '&lt;br/&gt;�����: ' + side2count + '">(' + side2live + '/' + side2count + ')</span>';
	} else {
		side2html = '(' + side2count + ')';
	}
	
	var typeTable = '' +
		'<table class="no-borders">' +
			'<tbody>' +
				'<tr>' +
					'<th class="side-1">' +
						side1html +
						'<div class="hidden">' + side2html + '</div>' +
					'</th>' +
					'<th class="img">' +
						'<img alt="��� ���"' + (closed ? ' class="isolated"' : '') + ' src="http://img.blutbad.ru/i/' + icon + '" title="' + description + (closed ? ' (����������)' : '') + '" />' +
					'</th>' +
					'<th class="side-2">' +
						side2html +
						'<div class="hidden">' + side1html + '</div>' +
					'</th>' +
				'</tr>' +
			'</tbody>' +
		'</table>';
	
	html += typeTable;
	
	var flag = '&nbsp;<img alt="������" height="15" src="http://img.blutbad.ru/i/flag.gif" title="������" width="15" />';
	
	html += '' +
			'</td>' +
			'<td class="nick-col">' + 
				'<div>' + (typeof side1 == 'object' ? side1.join(', ') : side1) + (winner == 1 ? flag : '') + '</div>' + 
				'<div>' + (typeof side2 == 'object' ? side2.join(', ') : side2) + (winner == 2 ? flag : '') + '</div>' + 
			'</td>' +
			'<td>' +
				'<a href="' + href + '" target="_blank">' +
					'<img alt="��� ���" src="http://img.blutbad.ru/i/b_zoom.gif" title="��� ���" style="margin:0 5px" />' +
				'</a>' +
			'</td>' +
		'</tr>';
	
	document.write(html);
}

function allprivate(users) {
	top.frames['down'].window.document.F1.text.focus();
	top.frames['down'].document.forms[0].text.value = users + top.frames['down'].document.forms[0].text.value;
}

function runes(img,alpha,t,d,p,slot,use) {
	document.write('<td bgcolor="#dedede">');
	if ( use ) {
		document.write('<a href="#" onclick="'+use+'(\''+slot+'\');return false;">')
	}
	
	document.write('<img onmouseover="sh(\''+t+'\',\''+d+'\',\''+p+'\')" onmouseout="hh()" src="http://img.blutbad.ru/i/' + img + '" width="35" height="35" style="filter:alpha(opacity='+alpha+', style=0)">');
	if ( use ) {
		document.write('</a>')
	}
	
	document.write('</td>');
}

function userune_combatyell( slot ) {
	openDialogWindow('������ ����',{ 'text':'�����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_snowball( slot )
{
	openDialogWindow('������ �������',{ 'name':'�����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_iceberg( slot )
{
	openDialogWindow('������ ��������',{ 'name':'�����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_petard( slot )
{
	openDialogWindow('��������� �������',{ 'name':'�����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } ); 
	return false;
}
function userune_lightning( slot )
{
	openDialogWindow('��������� ������',{ 'name':'�����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } ); 
	return false;
}

function userune_russa( slot )
{
	openDialogWindow('������������ ������',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_weakening( slot )
{
	openDialogWindow('��������',{ 'name':'����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_panic( slot )
{
	openDialogWindow('������������ ������',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_panic_zar( slot )
{
	openDialogWindow('������������ �������',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_dungeon_save( slot )
{
	openDialogWindow('������',{ 'name':'����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_carrot( slot )
{
	openDialogWindow('������������ ��������',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_cheese( slot )
{
	openDialogWindow('������������ ���',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_sheep( slot )
{
	openDialogWindow('������������ �������',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_s_rabbit( slot )
{
	openDialogWindow('������������ ����� �������',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_s_cat( slot )
{
	openDialogWindow('������������ �������',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_s_dragon( slot )
{
	openDialogWindow('������������ �������',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_s_snake( slot )
{
	openDialogWindow('������������ ���������� �����',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_evil_santa( slot )
{
	if (confirm('������������ ������?')) { location='/battle.pl?cmd=magic.use&item='+slot+'&'+Math.random(); }
}

function userune_summon_nm( slot )
{
	if (confirm('������������ ������?')) { location='/battle.pl?cmd=magic.use&item='+slot+'&'+Math.random(); }
}

function userune_summonz( slot )
{
	if (confirm('������������ ������?')) { location='/battle.pl?cmd=magic.use&item='+slot+'&'+Math.random(); }
}

function userune_bunch( slot )
{
	openDialogWindow('������������ ����� ������',{ 'name':'�����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } ); 
	return false;
}

function userune_bread( slot )
{
	openDialogWindow('������������ �������',{ 'name':'�����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } ); 
	return false;
}

function userune_weakness_zar( slot )
{
	openDialogWindow('��������',{ 'name':'����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_stun( slot )
{
	openDialogWindow('��������',{ 'name':'����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_bleeding( slot )
{
	openDialogWindow('�������� �������� ����',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_ban( slot )
{
	openDialogWindow('�������',{ 'name':'����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}
function userune_weakness( slot )
{
	openDialogWindow('��������',{ 'name':'����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_skip( slot )
{
	openDialogWindow('��������',{ 'name':'����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_inversion( slot )
{
	openDialogWindow('���������� �������',{ 'name':'� ���' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_wound( slot )
{
	openDialogWindow('�������� �������� ����',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_putoff(slot){
	openDialogWindow('����� ���� �', {
		'name': '����'
	}, {
		'cmd': 'magic.use',
		'item': slot
	}, {
		action: '?'
	});
	return false;
}

function userune_lovesave( slot )
{
	openDialogWindow('������',{ 'name':'����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_f14_red_rose( slot )
{
	openDialogWindow('������������ ��������� ����',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } );
	return false;
}

function userune_fool( slot )
{
	openDialogWindow('������������',{ 'name':'�� ����' }, { 'cmd':'magic.use', 'item':slot }, { action:'?' } ); 
	return false;
}

function runes2(img, alpha, t, d, p, slot, use){
	document.write('<td bgcolor="#dedede">');
	if (use) {
		document.write('<a href="javascript:userune' + (use == 2 ? 'to' : '') + '(\'' + slot + '\')">')
	}
	document.write('<img onmouseover="sh(\'' + t + '\',\'' + d + '\',\'' + p + '\')" onmouseout="hh()" src="http://img.blutbad.ru/i/' + img + '" width="35" height="35" style="filter:alpha(opacity=' + alpha + ', style=0)">');
	if (use) {
		document.write('</a>')
	}
	document.write('</td>');
}

function userune(param){
	if (confirm('������������ ������?')) {
		location = '/battle.pl?cmd=magic.use&item=' + param + '&' + Math.random();
	}
}

function useruneto(param){
	if (confirm('������������ ������?')) {
		location = '/battle.pl?cmd=magic.use&item=' + param + '&' + Math.random();
	}
}

function group_private(){
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.writeToEditor( '!������ ' );
	}
	return true;
};

function private(list){
	if (list.length) {
		users = list.split(',');
		text = '';
		for (var i = 0; i < users.length; i++) {
			text += 'private [' + users[i] + '] '
		}
		wr('<img src="http://img.blutbad.ru/i/pr.gif" width="12" height="9" alt="��������� ���������" style="cursor:hand" onclick="allprivate(\'' + text + '\')">&nbsp;');
	}
}

function live_start() {
	wr('' +
		'<table class="null w100" id="vs-table" style="margin-top: .7em;">' +
			'<tr>' +
				'<td class="tac" style="background-image: url(\'http://img.blutbad.ru/i/battle/live_up_line.gif\'); font-size: 0;"><img src="http://img.blutbad.ru/i/battle/live_up.gif" class="vat"></td>' +
			'</tr>' +
			'<tr>' +
				'<td bgcolor="#dee2e7"><img src="http://img.blutbad.ru/i/null.gif" width="1" height="4"></td>' +
			'</tr>' +
			'<tr>'+
				'<td class="tac" bgcolor="#dee2e7">');
}

function live_end() {
	wr('<br/>' +
				'</td>' +
			'</tr>' +
			'<tr>' +
				'<td bgcolor="#dee2e7"><img src="http://img.blutbad.ru/i/null.gif" width="1" height="4"></td>' +
			'</tr>' +
			'<tr>' +
				'<td class="tac" style="background-image: url(\'http://img.blutbad.ru/i/battle/live_down_line.gif\');font-size: 0;"><img src="http://img.blutbad.ru/i/battle/live_down.gif" class="vab"></td>' +
			'</tr>' +
			'<tr>' +
				'<td><img src="http://img.blutbad.ru/i/null.gif" width="1" height="8"></td>' +
			'</tr>' +
		'</table>');
}

function showPageLinks(baseURL, currentPage, pageCount){
	var interval = 10;
	
	pageCount = parseInt(pageCount);
	pageCount = (isNaN(pageCount) || pageCount < 1) ? 1 : pageCount;
	
	currentPage = parseInt(currentPage);
	currentPage = (isNaN(currentPage) || currentPage > pageCount || currentPage < 1) ? pageCount : currentPage;
	
	if (pageCount == 1) {
		return;
	}
	
	var code = '';
	var start = 1;
	var end = interval;
	
	if (end > pageCount) {
		end = pageCount;
	}
	
	code += _getPageRange(baseURL, start, end, currentPage, pageCount);
	
	var lastEnd = end;
	
	start = currentPage - interval;
	end = currentPage + interval;
	
	var sep = ', ';
	
	if (start <= lastEnd + 1) {
		start = lastEnd + 1;
	} else {
		sep = ' ... <a href="' + baseURL + '&page=' + (start - 1) + '">&lt;</a> ';
	}
	
	if (end > pageCount) {
		end = pageCount;
	}
	
	if (end >= start) {
		code += sep;
		code += _getPageRange(baseURL, start, end, currentPage, pageCount);
		
	}
	
	lastEnd = end;
	
	start = pageCount - interval + 1;
	end = pageCount;
	
	sep = ', ';
	
	if (start <= lastEnd + 1) {
		start = lastEnd + 1;
	} else {
		sep = ' <a href="' + baseURL + '&page=' + (lastEnd + 1) + '">&gt;</a> ... ';
	}
	
	if (end > pageCount) {
		end = pageCount;
	}
	
	if (end >= start) {
		code += sep;
		code += _getPageRange(baseURL, start, end, currentPage, pageCount);
		
	}
	
	document.write(code);
}

function _getPageRange(baseURL, start, end, current, count){
	var links = new Array();
	
	for (var i = start; i <= end; i++) {
		links.push('<a href="' + baseURL + (i != count ? '&page=' + i : '') + '">' + (i == current ? '<b>' + i + '</b>' : i) + '</a>');
	}
	
	return links.join(', ');	
}

function getSideHTML(sideArr, side) {	
	if(sideArr && sideArr.length) {
		var MAX_PLAYERS_TO_SHOW = 20;
		
		var html = '';
		if (side == 1) {
			html += '<ul>';
		}
		
		for(var i = 0; i < sideArr.length; i++) {
			html += '<li' + (sideArr.length > MAX_PLAYERS_TO_SHOW && i > MAX_PLAYERS_TO_SHOW - 1 ? ' class="more-than-' + side + '"' : '') + '>' + sideArr[i];
			if(i < sideArr.length - 1)
				html += '<span class="comma' + (sideArr.length > MAX_PLAYERS_TO_SHOW && i == MAX_PLAYERS_TO_SHOW - 1 ? ' float-comma-' + side : '') + '">,</span>';
			html += '</li>';
		}
		if(sideArr.length > MAX_PLAYERS_TO_SHOW)
				html += '<li><a href="#" class="more" rel="' + side + '">... (����������)</a></li>';
		
		if (side == 2) {
			html += '</ul>';
		}
		
		return html;
	} else {
		return '';
	}
}

function battle_settings_select_value(battle_settings,item) {
	var name = item.name;
	var raw_value = battle_settings[name];
	
	if (name == 'level_limit') {
		if (raw_value == 1) return battle_settings.level;
		else if (raw_value == 2) return battle_settings.level+'-'+(Number(battle_settings.level)+1);
		else if (raw_value == 3) return (Number(battle_settings.level)-1)+'-'+battle_settings.level;
		else if (raw_value == 4) return (Number(battle_settings.level)-1)+'-'+(Number(battle_settings.level)+1);
	}

	for (var i=0;i<item.list.length;i++) {
		if (item.list[i].value == raw_value) {
			return item.list[i].title;
		}
	}

	return raw_value;
}

function battle_settings_flag_value(battle_settings,item) {
	return '��';
}

function accept_haot_battle(price,href) {
	if (price == '' || price == '���������') {
		document.location.href=href;
	} else {
		if (confirm('������� � ���� ��� ����� ������ ��� '+price+' ����������?')) {
			document.location.href=href;
		}
	}
}

if(typeof jQuery != 'undefined') {
	$(function() {
		$('.nick-col a.more').click(function() {
			var $this = $(this);
			var list = $this.parent().parent();
			if ($this.text() == '(��������)') {
				list.removeClass('show-all-' + this.rel);
				$(this).text('... (����������)');
			} else {
				list.addClass('show-all-' + this.rel);
				$(this).text('(��������)');
			}
			return false;
		});
		
		$('.table-list .timeout').each(function() {
			var $this = $(this);
			var until = Number($this.attr('data'));
			
			if (until > 0) {
				$this.countdown({
					compact: true,
					format: 'MS',
					onExpiry: function(){
						setTimeout(function() {
							location.href = reloadLink;
						}, 5000);
					},
					onTick: function(periods){
						var seconds = periods[0] * 31557600 + periods[1] * 2629800 + periods[2] * 604800 + periods[3] * 86400 + periods[4] * 3600 + periods[5] * 60 + periods[6];
						if (seconds < 60) {
							$this.addClass('attention');
						}
					},
					until: until
				});
			}
		});
	});
}
