function onkeyup(e){
	// Let's remember about poor Explorer.
	// It does not pass e as an argument
	
	if (!e) {
		e = window.event;
	}
	
	var handler;
	
	if (window.top.frames['carnage'] && window.top.frames['carnage'].current_inventory_uid) {
		var uid = window.top.frames['carnage'].current_inventory_uid;
		if (window.top.frames['carnage'].shortkey_handler) {
			handler = window.top.frames['carnage'].shortkey_handler[uid]
		}
	}
	
	var code = e.keyCode;
	
	if (e.altKey || e.ctrlKey) {
		// short keys
		if (code == 49) {
			if (handler) {
				handler('items');
			} else {
				window.top.frames['carnage'].location = '/char.pl?back=items'
			}
		} else if (code == 50) {
			if (handler) {
				handler('runes');
			} else {
				window.top.frames['carnage'].location = '/char.pl?back=runes'
			}
		} else if (code == 51) {
			if (handler) {
				handler('animals');
			} else {
				window.top.frames['carnage'].location = '/char.pl?back=animals'
			}
		} else if (code == 52) {
			if (handler) {
				handler('stuff');
			} else {
				window.top.frames['carnage'].location = '/char.pl?back=stuff'
			}
		} else if (code == 53) {
			if (handler) {
				handler('misc');
			} else {
				window.top.frames['carnage'].location = '/char.pl?back=misc'
			}
		} else if (code == 192) {
			window.top.frames['carnage'].document.location = '/map.pl?' + Math.random();
		} else if (code == 81) {
			window.top.document.location = '/main.pl?cmd=exit&' + Math.random();
		}/* else if (code == 70) {
			window.open('/forum.pl');
		}*/
	}
}

document.onkeyup = onkeyup;