var statuses = ['�����������', '���������', '�����', '��������'];

function send_menu(id, description){
	if (confirm("�� �������, ��� ������ �������� ������ '" + description + "'")) {
		document.forms['menuForm'].elements['id'].value = id;
		document.forms['menuForm'].submit();
	}
}

function setselect(name, value){
	var s = document.getElementById(name);
	for (var i = 0; i < s.options.length; i++) {
		if (s.options[i].value == value) {
			s.selectedIndex = i;
			break;
		}
	}
}

function sendForm1(){
	if (confirm("�� ������ ������ ������ �� �������� ������� ����� �������?")) {
		document.forms['sendForm'].submit();
	}
}

function _getPageRange(baseURL, start, end, current, count){
	var links = new Array();
	
	for (var i = start; i <= end; i++) {
		links.push('<a href="' + baseURL + '&page=' + i + '">' + (i == current ? '<b>' + i + '</b>' : i) + '</a>');
	}
	
	return links.join(', ');
}

function editDocket(entry){
	$('#reason-form-' + entry + ' :not(input[type=hidden])').toggleClass('invisible');
	moveCaretToEnd($('#reason-form-' + entry + ' .reason-comment').focus()[0]);
	
	$(document).bind('click.comment', function(event) {
		var target = $(event.target);
		
		if(!target.hasClass('reason-comment') && !target.hasClass('reason-save')) {
			$('.reason-save:visible').siblings(':not(input[type=hidden])').andSelf().toggleClass('invisible');
			$(document).unbind('click.comment');
		}
	});
	
	return false;
}

function showPageLinks(baseURL, currentPage, pageCount){
	var interval = 10;
	
	pageCount = parseInt(pageCount);
	pageCount = (isNaN(pageCount) || pageCount < 1) ? 1 : pageCount;
	
	currentPage = parseInt(currentPage);
	currentPage = (isNaN(currentPage) || currentPage > pageCount || currentPage < 1) ? pageCount : currentPage;
	
	if (pageCount == 1) {
		return false;
	}
	
	var code = '';
	var start = 1;
	var end = interval;
	
	if (end > pageCount) {
		end = pageCount;
	}
	
	code += _getPageRange(baseURL, start, end, currentPage, pageCount);
	
	var lastEnd = end;
	
	start = currentPage - interval;
	end = currentPage + interval;
	
	var sep = ', ';
	
	if (start <= lastEnd + 1) {
		start = lastEnd + 1;
	} else {
		sep = ' ... <a href="' + baseURL + '&page=' + (start - 1) + '">&lt;</a> ';
	}
	
	if (end > pageCount) {
		end = pageCount;
	}
	
	if (end >= start) {
		code += sep;
		code += _getPageRange(baseURL, start, end, currentPage, pageCount);
		
	}
	
	lastEnd = end;
	
	start = pageCount - interval + 1;
	end = pageCount;
	
	sep = ', ';
	
	if (start <= lastEnd + 1) {
		start = lastEnd + 1;
	} else {
		sep = ' <a href="' + baseURL + '&page=' + (lastEnd + 1) + '">&gt;</a> ... ';
	}
	
	if (end > pageCount) {
		end = pageCount;
	}
	
	if (end >= start) {
		code += sep;
		code += _getPageRange(baseURL, start, end, currentPage, pageCount);
		
	}
	
	document.write(code);
}

$(function() {
	$('#date').datepicker({
		maxDate: new Date(),
		onSelect: function(dateText, inst) {
			$('#date').removeClass('placeholder');
		}
	}).placeholder();
	
	$('.reason-form').ajaxForm({
		data: {
			is_ajax: 1
		},
		dataType: 'json',
		success: function(json, statusText, xhr, $form) {
			if(json && json.message) {
				var notify = $.pnotify({
					pnotify_type: json.error ? 'error' : 'notice', 
					pnotify_title: '��������!',
					pnotify_text: json.message
				});
				notify.appendTo('#notifies');
				
				if(!json.error) {
					var value = $form.children('.reason-comment').val();
					$form.children('.reason-text').text(value);
					$form.children('.reason-edit').text('[' + (value ? '��������' : '�������� �������') + ']');
					
					$form.children(':not(input[type=hidden])').toggleClass('invisible');
				}
			}
		}
	});
	
	// �������������� �������
	$('.reason-edit').click(function() {
		var entry = $(this).attr('data');
		var form = $('#reason-form-' + entry);
		form.children(':not(input[type=hidden])').toggleClass('invisible');
		form.children('.reason-comment').val(form.children('.reason-text').text());
		moveCaretToEnd($('#reason-form-' + entry + ' .reason-comment').focus()[0]);
		
		$(document).bind('click.comment', function(event) {
			var target = $(event.target);
			
			if(!target.hasClass('reason-comment') && !target.hasClass('reason-save')) {
				$('.reason-save:visible').siblings(':not(input[type=hidden])').andSelf().toggleClass('invisible');
				$(document).unbind('click.comment');
			}
		});
		
		return false;
	});
	
	$('.reason-save').click(function() {
		$(this).parent().submit();
		
		return false;
	});
	
	$('#clan-description').charCounter(2000, {
		container: $('#clan-description-counter')
	});
	
	if (pages > 1) {
		$('.pagination-container').pagination(pages, $.extend({}, paginationDefaults, {
			current_page: current_page,
			link_to: link_to
		}));
		
		// hotkey pagination
		$(document).bind('keyup', 'ctrl+left', function() {
			var prev = $('.pagination:first .prev');
			
			if(prev.length) {
				location.href = prev[0].href;
			}
		});
		
		$(document).bind('keyup', 'ctrl+right', function() {
			var next = $('.pagination:first .next');
			
			if(next.length) {
				location.href = next[0].href;
			}
		});
	}
})
