// ���������� ����������
var imgURL = 'http://img.carnage.ru/i/';
var libURL = 'http://lib.carnage.ru/';
var nullImg = imgURL + 'null.gif';

/**
 * ����������� ���������� �������� ��� �����
 * 
 * @param 	{Number} number
 * @param 	{String} den
 * @param 	{String} dnya
 * @param 	{String} dney
 * @return	{String}
 */
function suffix(number, den, dnya, dney){
	if (RegExp(/,|\./).test(number)) 
		return dnya
	
	number = Math.abs(number);
	
	var ones = number % 10;
	var ten = (number - ones) / 10;
	
	if (RegExp(/.*1\d$/).test(number)) 
		return dney
	
	if (ones == 1) 
		return den
	
	if (ones > 1 && ones < 5) 
		return dnya
	
	return dney
}

/**
 * ���������� ������� ��������� � ���������� ����������
 * 
 * @param {Number} num		�����
 * @param {Object} cases	�������� ����� {nom: '���', gen: '����', plu: '�����'}
 * @return {String}			
 */
function units(num, cases) {
	num = Math.abs(num);
	
	var word = '';
	
	if(num != NaN) {
		if (num.toString().indexOf('.') > -1) {
			word = cases.gen;
		} else { 
			word = (
				num % 10 == 1 && num % 100 != 11 
					? cases.nom
					: num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) 
						? cases.gen
						: cases.plu
			);
		}
	}
	
	return word;
}

/**
 * ��������������� ������� �� EN � RU
 * @param 	{String} 	s			����������������� ������		
 * @param 	{Boolean}	reverse 	
 * @return 	{String}				�������������� �� EN � RU, �� ���� (reverse==true) - RU � EN
 */
function transliterate(str, reverse) {
	if(jQuery.trim(str)) {
		var trans = str;
		var en_ru = [
			["shh", "SHH", "sh", "SH", "ch", "CH", "jo", "JO", "zh", "ZH", "je", "JE", "ju", "JU", "ja", "JA", "sx", "SX", "j/o", "J/O", "j/e", "J/E", "a", "A", "b", "B", "v", "V", "g", "G", "d", "D", "e", "E", "z", "Z", "i", "I", "j", "J", "k", "K", "l", "L", "m", "M", "n", "N", "o", "O", "p", "P", "r", "R", "s", "S", "t", "T", "u", "U", "f", "F", "x", "X", "h", "H", "c", "C", "w", "W", "##", "#", "y", "Y", "''", "'"], 
			["�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "��", "��", "��", "��", "��", "��", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�"]
		];
		
		if(reverse) {
			en_ru.reverse();
			
			for(var i = 0; i < en_ru[0].length; i++) {
				while (trans.indexOf(en_ru[0][i]) >= 0) {
					trans = trans.replace(en_ru[0][i], en_ru[1][i]);
				}
			}
		} else {
			// ��������� ������ ��� ���������� �������� �� ������ � ��������������
			var arr = trans.split(':');
			
			for(var j = 0; j < arr.length; j++) {
				// ��������� ����������� �������
				if(arr[j] && (!smilies[arr[j]] || j == 0 || j == arr.length -1)) {
					for(var i = 0; i < en_ru[0].length; i++) {
						while(arr[j].indexOf(en_ru[0][i]) >= 0) {
							arr[j] = arr[j].replace(en_ru[0][i], en_ru[1][i]);
						}
					}
				}
			}
			
			trans = arr.join(':');
		}
		
		return trans;
	} else {
		return '';
	}
}

/**
 * ���������� ������ ������� ������
 *
 * @param {Boolean} dot		���� true, �� ��������� ������� ������ �����
 * @return {String}			���������� ������� � �������
 */
function getDomain(dot) {
	var domain = document.domain;
	var arr = domain.split('.');
	var length = arr.length;
	
	return (dot ? '.' : '') + arr[length - 2] + '.' + arr[length - 1];
}

/**
 * ���������� ������� ������ ��� Firefox, ����������� console.log
 */
function debug() {
	if(typeof console != 'undefined' && jQuery.browser.firefox) {
		console.log.apply('', arguments);
	}
}

/**
 * ������ ��� ������������ ����� � ������
 */
(function($) {
	$.extend({
		buildForm: function(options) {
			var o = $.extend({}, $.buildForm.defaults, options);
			// ������� ��� ������ �����
			var form = $('<form />', {
				'action': o.url + '?' + Math.random(),
				'class': 'dialog-table', 
				'method': o.method
			});
			
			// ������� ������� ��� ���������� ����� �����
			var table = $('<table />', {
				'class': 'form-table'
			}).appendTo(form);
			var tbody = $('<tbody />').appendTo(table);
			
			var fields = o.fields;
			
			for(var i in fields) {
				var field = fields[i];
				
				if(!field.type && !field.group) {
					field.type = 'hidden';
				}
				
				if (field.type == 'hidden') {
					form.append(drawField(field));
				} else {
					tbody.append(drawRow(field));
				}
			}
			
			return form;
		}
	});
	
	/**
	 * ��������� ���� � �����
	 *
	 * @param {Object} field	����(�) ��� ���������
	 * @return					���������� ������ ���� tr
	 */
	function drawRow(field) {
		var row = $('<tr />');
		
		if(!field.colspan) {
			var labelTd = $('<td />', {
				'class': 'label'
			}).appendTo(row);
		}
		
		if(field.label) {
			$('<label />', {
				'for': field.id ? field.id : field.name,
				'text': field.label + ':'
			}).appendTo(labelTd);
		}
		
		var inputTd = $('<td />', {
			'class': 'input'
		}).appendTo(row);
		
		if(field.colspan) {
			inputTd.attr('colspan', 2).css('text-align', 'right');
		}
		
		if (field.group) {
			inputTd.addClass('nowrap');
			for(var i in field.group) {
				var input = field.group[i];
				
				inputTd.append(drawField(input)).append(' ');
			}
		} else {
			inputTd.append(drawField(field));
		}
		
		if(field.comment) {
			inputTd.append(field.comment);
		}
		
		return row;
	}
	
	/**
	 * ��������� ���� �����
	 * ��-�� IE 6,7 (��� � ��������� name) ���������� ������������ ���� ����� ������� �������� �������� �����
	 *
	 * @param {Object} field	���� ����� (inpu, select � �.�.)
	 * @return {Object}			���������� ������ ���� �����
	 */
	function drawField(field) {
		var el;
		
		// ���� ��� ���� �� ������, �� �� ��������� ��� hidden
		if(!field.type) {
			field.type = 'hidden';
		}
		
		switch(field.type) {			
			case 'button':
			case 'submit':
				el = $('<input name="' + field.name + '" type="' + field.type + '" />').attr({
					'class': 'button',
					'value': field.value
				}).click(field.click);
				break;
			
			case 'hidden':
				el = $('<input name="' + field.name + '" type="' + field.type + '" />').attr('value', field.value);
				break;
			
			case 'checkbox':
			case 'radio':
				el = $('<ul />', {
					'class': field.type + '-list'
				});
				
				for(var i in field.list) {
					var item = field.list[i];
					
					var li = $('<li />').appendTo(el);					
					$('<input' + (item.checked ? ' checked="checked"' : '') + ' name="' + field.name + '" type="' + field.type + '" />').attr({
						'class': field.type,
						'id': field.name + '-' + i,
						'value': item.value
					}).appendTo(li);
					
					$('<label />', {
						'for': field.name + '-' + i,
						'html': item.label
					}).appendTo(li);
				}
				break;
			
			case 'message':
				el = field.value;
				break;
				
			case 'text':
				el = $('<input name="' + field.name + '" type="' + field.type + '" />').attr({
					'autocomplete': field.autocomplete,
					'class': 'text' + (field['class'] ? ' ' + field['class'] : ''),
					'id': field.id ? field.id : field.name,
					'maxlength': field.maxlength,
					'size': field.size,
					'value': field.value
				});
				break;
				
			default: 
				break;
		}
		
		return el;
	}
	
	$.buildForm.defaults = {
		'method': 'post'
	};
})(jQuery);

/**
 * Context menu
 */
// ���������� ��� �������� ������
var monthTypes = new Array( 
	'winter', 'winter', 'summer', 
	'summer', 'summer', 'summer', 
	'summer', 'summer', 'summer', 
	'summer', 'summer', 'winter' 
);


// ������� ��� ��� �������� ��������, ��� ���� - ��� ������,
// � �������� - ��� ����������������� ����������
var utf8Array = {};
// ������� ��������� ����������� 255 ��������
//var i = j = j2 = 0;
for (var i = 0; i <= 255; i++) {
	var j = parseInt(i / 16);
	var j2 = parseInt(i % 16);
	utf8Array[String.fromCharCode(i)] = ('%' + j.toString(16) + j2.toString(16)).toUpperCase();
}
// � �������� �������� ������� ���������
var rusAdditional = {
	'_' : '%5F', '�' : '%C0', '�' : '%C1', '�' : '%C2', '�' : '%C3', '�' : '%C4', '�' : '%C5',
	'�' : '%C6', '�' : '%C7', '�' : '%C8', '�' : '%C9', '�' : '%CA', '�' : '%CB', '�' : '%CC',
	'�' : '%CD', '�' : '%CE', '�' : '%CF', '�' : '%D0', '�' : '%D1', '�' : '%D2', '�' : '%D3',
	'�' : '%D4', '�' : '%D5', '�' : '%D6', '�' : '%D7', '�' : '%D8', '�' : '%D9', '�' : '%DA',
	'�' : '%DB', '�' : '%DC', '�' : '%DD', '�' : '%DE', '�' : '%DF', '�' : '%E0', '�' : '%E1',
	'�' : '%E2', '�' : '%E3', '�' : '%E4', '�' : '%E5', '�' : '%E6', '�' : '%E7', '�' : '%E8',
	'�' : '%E9', '�' : '%EA', '�' : '%EB', '�' : '%EC', '�' : '%ED', '�' : '%EE', '�' : '%EF',
	'�' : '%F0', '�' : '%F1', '�' : '%F2', '�' : '%F3', '�' : '%F4', '�' : '%F5', '�' : '%F6',
	'�' : '%F7', '�' : '%F8', '�' : '%F9', '�' : '%FA', '�' : '%FB', '�' : '%FC', '�' : '%FD',
	'�' : '%FE', '�' : '%FF', '�' : '%B8', '�' : '%A8'
}

for (i in rusAdditional) {
	utf8Array[i] = rusAdditional[i];
}

/**
 * �������������
 * 
 * @param {String} str
 */
function urlEncode(str) {
    // ������� ��������
    if (!str || typeof(str) == "undefined") return false;
	
    // ����������� �������� ������� �� �� ����������������� �����������
    var res = "";
    for(i = 0; i < str.length; i++) {
        var simbol = str.substr(i,1);
        res += typeof utf8Array[simbol] != "undefined" ? utf8Array[simbol] : simbol;
    }
    // ������� �������� �� ����� 
    res = res.replace(/\s/g, "+");
    return res;
}

/*
 * ���������
 */
/**
 * ������������� �������������� ������������� � ���������
 * 
 * @param {Object} el
 */
function initInventoryPropertyExpander(el) {
	jQuery(el ? el : '.properties-expander').click(function() {
		var ul = jQuery(this).nextAll('ul.invisible:first');
		this.src = imgURL + (ul.is(':visible') ? 'sb_plus.gif' :'sb_minus.gif');
		ul.toggle();
	}).tooltip(jQuery.extend({}, tooltipDefaults, {
		bodyHandler: function() {
			return '<ul>' + jQuery(this).nextAll('ul.invisible:first').html() + '</ul>';
		},
		extraClass: 'properties'
	}));
}
/* end: ��������� */

/**
 * ���������� � �����
 * 
 * @param {Integer} id
 */
function speak(id){
	top.frames['carnage'].document.location.href = '/npc.pl?cmd=npc&nid=' + id + '&' + Math.random();
}

/**
 * ��������� ����
 * 
 * @param {Integer} id
 */
function attack(id){
	top.frames['carnage'].document.location.href = '/main.pl?cmd=attack_bot&bot=' + id + '&' + Math.random();
}

/**
 * ������ ������
 * 
 * @param {String} text
 */
function tort(text){
	text = unescape(text);
	
	if (top.frames['chat'] && top.frames['chat']['chat2'].oChat) {
		top.frames['chat']['chat2'].oChat.throwThing(text, 'cake');
	}	
}

/**
 * ������ �������
 * 
 * @param {String} text
 */
function snezhok(text){
	text = unescape(text);
	
	if (top.frames['chat'] && top.frames['chat']['chat2'].oChat) {
		top.frames['chat']['chat2'].oChat.throwThing(text, 'snowball');
	}
}

/**
 * ��������� ����������� ���� ���������
 * 
 * @param {String} login	��� ���������
 * @param {Number} battle	��������� ���������� � ��� 
 * @return					���������� HTML-��� ������������ ����
 */
function getUserContextMenu(login, battle) {
	var html = '';
		
	// �������� ���� ���� ����� �������
    var i1 = login.indexOf('[')
	var i2 = login.indexOf(']');
    if (i1 >= 0 && i2 > 0) {
		login = login.substring(i1 + 1, i2);
	}
	login = escape(login);
    
    html +=
		'<li><a href="#" onclick="top.AddTo(\'' + login + '\');return false;">�������</a></li>' +
		'<li><a href="#" onclick="top.AddToPrivate(\'' + login + '\');return false;">�������</a></li>' +
		'<li class="separator"><a href="#" onclick="window.open(\'/inf.pl?user=' + urlEncode(unescape(login)) + '\', \'_blank\');return false;">����������</a></li>' + 
		'<li><a href="#" id="context-copy-nick" onclick="return false;" rel="' + login + '">���������� ���</a></li>';
	
	var monthType = monthTypes[new Date().getMonth()];
    if(monthType == 'winter') {
		html += '<li><a href="#" onclick="snezhok(\'' + login + '\');return false;">������ �������</a></li>';
    } else if( monthType == 'summer' ) {
		html += '<li><a href="#" onclick="tort(\'' + login + '\');return false;">������ ������</a></li>';
    }
	
    html += '<li class="separator"><a href="#" onclick="contacts2(\'' + login + '\');return false;">� ��������</a></li>';
    
	if (top && top.ignore && top.ignore[unescape(login)]) {
		html += '<li><a href="#" onclick="unignore2(\'' + login + '\');return false;">�� ������</a></li>';
	} else { 
		html += '<li><a href="#" onclick="ignore2(\'' + login + '\');return false;">� �����</a></li>' 
	}
	
    if (battle > 0) {
		html += '<li><a href="#" onclick="window.open(\'/log.pl?log=' + battle + '\', \'_blank\');return false;">���������� ���</a></li>';
	}
	
    return html;
}

function getBotContextMenu(login, id, battle, meddle, attack, speaking){
	var html = '';
	
	var i1 = login.indexOf('[');
	var i2 = login.indexOf(']');
	if (i1 >= 0 && i2 > 0) {
		login = login.substring(i1 + 1, i2);
	}
	login = escape(login);
	
	html += 
		(speaking > 0 ? '<li><a href="#" onclick="speak(\'' + id + '\');return false;">����������</a></li>' : '') +
		'<li><a href="#" onclick="top.AddTo(\'' + login + '\');return false;">�������</a></li>' +
		'<li><a href="#" onclick="top.AddToPrivate(\'' + login + '\');return false;">�������</a></li>' +
		'<li class="separator"><a href="#" onclick="window.open(\'/inf.pl?user=' + urlEncode(unescape(login)) + '\', \'_blank\');return false;">����������</a></li>' +
		'<li><a href="#" id="context-copy-nick" rel="' + login + '" onclick="return false;">���������� ���</a></li>';
	
	var monthType = monthTypes[new Date().getMonth()];
	if (monthType == 'winter') {
		html += '<li><a href="#" onclick="snezhok(\'' + login + '\');return false;">������ �������</a></li>';
	} else if (monthType == 'summer') {
		html += '<li><a href="#" onclick="tort(\'' + login + '\');return false;">������ ������</a></li>';
	}
	
	if (battle > 0) {
		html += '<li><a href="#" onclick="window.open(\'/log.pl?log=' + battle + '\', \'_blank\');return false;">���������� ���</a></li>';
		
		if (meddle > 0) {
			html += '<li><a href="#" onclick="meddle(\'' + id + '\');return false;">���������</a></li>';
		}
		
		if (attack > 0) {
			html += '<li><a href="#" onclick="attack(\'' + id + '\');return false;">�������</a></li>';
		}
	} else if (attack > 0) {
		html += '<li><a href="#" onclick="attack(\'' + id + '\');return false;">�������</a></li>';
	}
	
	return html;
}

function getNPCContextMenu(login, id, battle, meddle, attack, speaking){
	var html = '';
	
	login = escape(login);
	
	if (speaking > 0) {
		html += '<li><a href="javascript:speak(\'' + id + '\');">����������</a></li>';
	}
	
	html += 
		'<li><a href="javascript:top.AddTo(\'' + login + '\');">�������</a></li>' +
		'<li><a href="javascript:top.AddToPrivate(\'' + login + '\');">�������</a></li>' +
		'<li class="separator"><a href="/inf.pl?user=' + urlEncode(unescape(login)) + '" target="_blank">����������</a></li>';/* +
		'<li><a href="#" id="context-copy-nick" rel="' + login + '" onclick="return false;">���������� ���</a></li>';*/
	
	var monthType = monthTypes[new Date().getMonth()];
	if (monthType == 'winter') {
		html += '<li><a href="javascript:snezhok(\'' + login + '\');">������ �������</a>';
	} else if (monthType == 'summer') {
		html += '<li><a href="javascript:tort(\'' + login + '\');">������ ������</a></li>';
	}
	
	if (battle > 0) {
		html += '<li><a href="/log.pl?log=' + battle + '" target="_blank">���������� ���</a></li>';
		
		if (meddle > 0) {
			html += '<li><a href="javascript:meddle(\'' + id + '\');">���������</a></li>';
		}
		if (attack > 0) {
			html += '<li><a href="javascript:attack(\'' + id + '\');">�������</a></li>';
		}
	} else if (attack > 0) {
		html += '<li><a href="javascript:attack(\'' + id + '\');">�������</a></li>';
	}
	
	return html;
}

function getSuspUserContextMenu(login) {
	var html = '';
	
	login = escape(login);
	
	html += '<li><a href="#" id="context-copy-nick" onclick="return false;" rel="' + login + '">���������� ���</a></li>';
	
	if (!options) {
		var options = {
			f_date: '',
			f_date2: '',
			f_city: '0',
			f_curcity: '0',
			f_status: 'none',
			f_type: '0',
			f_user: '',
			rows_on_page: '20'
		};
	}
	
	html += '<li><a href="#" onclick="window.open(\'/suspicions.pl?cmd=suspicions_show&f_date='+
		options.f_date+'&f_date2='+options.f_date2+'&f_city='+options.f_city+'&f_curcity='+
		options.f_curcity+'&f_type='+options.f_type+'&f_status='+options.f_status+'&rows_on_page='+
		options.rows_on_page+'&f_user='+
		urlEncode(unescape(login))+'\', \'_blank\');return false;">������� � ������ ����</a></li>';
	
	return html;
}

var nickZeroClip;

/**
 * ��������� ����������� ���������� ��� ���������
 * 
 * @param {Object} el
 */
function makeCopiableNick(el) {
	if(el) {
		if(jQuery.browser.msie && jQuery.browser.versionX == 6) {
			jQuery(el).click(function() {
				window.clipboardData.setData('Text', unescape(jQuery(this).attr('rel')));
			});
		} else {
			ZeroClipboard.setMoviePath('/f/ZeroClipboard.swf');
			if (nickZeroClip) {
				nickZeroClip.destroy();
			}
			nickZeroClip = new ZeroClipboard.Client();
			
			nickZeroClip.glue(el);
			nickZeroClip.setText(unescape(jQuery(el).attr('rel')));
			
			nickZeroClip.addEventListener('onComplete', hideContext);
			nickZeroClip.addEventListener('onMouseOver', preventAutoHide);
			nickZeroClip.addEventListener('onMouseOut', activateAutoHide);
		}
	}
}

/**
 * �������� ���������� ����
 */
function hideContext() {
	clearTimeout(autoHideContext);
	autoHideContextActive = false;
	jQuery(document).trigger('click.jeegoocontext');
}

var autoHideContext;
var autoHideContextActive = false;

/**
 * ���������� ����������� ������������ ����
 */
function activateAutoHide() {
	if (!autoHideContextActive) {
		autoHideContextActive = true;
		autoHideContext = setTimeout(function(){
			jQuery(document).trigger('click.jeegoocontext');
		}, 5000);
	}
}

/**
 * ������������� ������� ������������ ����
 */
function preventAutoHide() {
	clearTimeout(autoHideContext);
	autoHideContextActive = false;
}

/**
 * ��������� ����������� ���� � �������� ���������� context (� ��������� �� ���)
 * 
 * @param {Object} context	������ ��������, ������� ����������� ����������� ���� 
 * @param {Boolean} isChat	true ��� ���� ����
 */
function addContextMenu(context, isChat) {
	if (!context || !context.length) {
		context = jQuery('.context');
	}
	
	var oldOpera = jQuery.browser.opera && jQuery.browser.version < 10.5;
		
	// ������������� ������������ ����	
	if(jQuery.fn.jeegoocontext && context.length) {						
		var contextMenu = jQuery('#context-menu');
		if(!contextMenu.length) {
			contextMenu = jQuery('<ul />', {
				id: 'context-menu'
			})
			jQuery('body').append(contextMenu);
			
			contextMenu.hover(preventAutoHide, activateAutoHide);
		}
		
		var events = oldOpera ? 'click.context' : 'contextmenu.context'
		context.unbind(events);
		
		function getMenu(event) {
			// ������� ���������� ����
			contextMenu.empty();
			
			var $context = jQuery(event.target);
			var menu = jQuery('<li />');
			var login = $context.text();
			
			if (!(isChat && event.target.tagName.toLowerCase() != 'span')) {
				// ����� ���������������� ������������ ���� � ����������� �� ������
				if ($context.hasClass('user')) {
					var battle = parseInt($context.attr('rel'));
					menu = getUserContextMenu(login, battle);
					contextMenu.append(menu);
				} else if ($context.hasClass('bot')) {
					var arr = $context.attr('rel').split(',');
					menu = getBotContextMenu(login, arr[0], arr[1], arr[2], arr[3], arr[4]);
					contextMenu.append(menu);
				} else if ($context.hasClass('npc')) {
					var arr = $context.attr('rel').split(',');
					menu = getNPCContextMenu(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]);
					contextMenu.append(menu);
				} else if($context.is('img.status, img.avatar') && $context.parent().hasClass('npc')) {
					var arr = $context.parent().attr('rel').split(',');
					menu = getNPCContextMenu(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]);
					contextMenu.append(menu);
				} else if ($context.hasClass('susp_user')) {
					menu = getSuspUserContextMenu(login);
					contextMenu.append(menu);
				} else {
					var battle = parseInt($context.attr('rel'));
					menu = getUserContextMenu(login, battle);
					contextMenu.append(menu);
				}
			}
			
			top.copyNick = contextMenu.find('#context-copy-nick').get(0);
		}
		
		context.bind(events, getMenu);
		
		context.nojeegoocontext('context-menu');
		context.jeegoocontext('context-menu', {
			delay: 0,
			fadeIn: 0,
			heightOverflowOffset: 2,
			isChat: isChat,
			onHide: function(e, context) {
				preventAutoHide();
				if (nickZeroClip) {
					nickZeroClip.hide();
				} else {
					jQuery(top.copyNick).unbind('click');
				}
			},
			onHover: preventAutoHide,
			onShow: function(event, context, lol) {
				setTimeout(function(){
					makeCopiableNick(top.copyNick);
					if (nickZeroClip) {
						nickZeroClip.show();
					}
				}, 10);
				
				activateAutoHide();
			},
			operaEvent: oldOpera ? 'dblclick' : 'contextmenu',
			widthOverflowOffset: 2
		});
	}
}
/* end: Context menu */

/*
 * Dialog window functions
 */

/**
 * ���������� ������ ����������� ����. ������ ���������� ����, ���� ��� �����������
 * 
 * @param {String} 	id	id ����������� ����
 * @return {Object}		������ ����������� ����, false - ���� ��� jQuery UI
 */
function getDialog(id) {	
	if(!id) {
		id = 'common-dialog';
	}
	
	var dialog = jQuery('#' + id);
	
	if(dialog.length) {
		dialog.dialog('option', 'width', 'auto');
		return dialog;
	} else {
		dialog = jQuery('<div />', {
			'class': 'invisible',
			id: id
		}).appendTo('body');
		
		dialog.dialog({
			autoOpen: false,
			minHeight: 0,
			open: function(event, ui) {
				dialog.find('.text:first').focus();
			},
			width: 'auto'
		});
		
		return dialog;
	}
}

/**
 * ����� ������ ����������� ����
 * 
 * @param {String} url
 * @param {String} title
 * @param {String} name
 * @param {Object} hiddens
 * @param {Object} value
 */
function commonDialog(url, title, name, hiddens, value) {
	var common = getDialog();
	
	if(!common) return false;
	
	var html = '';
	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="nowrap" method="post">';
	
	for(var i in hiddens) {
		if(hiddens[i]) {
			html += '' +
			'<input name="' + i + '" type="hidden" value="' + hiddens[i] + '" />';
		}
	}
	
	html += '' +
			'<label for="' + name + '">�����:</label> ' +
			'<input class="text paste-nick" id="' + name + '" name="' + name + '" type="text"' + (value ? ' value="' + value + '"' : '') + ' /> ' +
			'<input class="button" type="submit" value="OK" />' +
		'</form>';
	
	common.html(html);
	common.dialog('option', 'title', title);
	
	var input = jQuery('#' + name);
	// ������ ����� �� ���� ����� ��� ������ ����
	common.unbind('dialogopen').bind('dialogopen', function(event, ui) {
		input.focus();
	});
	if(input.is(':visible')) {
		input.focus();
	}
	
	dialogOpen(common);	
}

/**
 * ����� ������ ������� �������������
 * 
 * @param {String} url
 * @param {Object} params
 * @param {String} question
 */
function commonConfirm(url, params, question) {
	if(!question) question = '������������ ������?';
	
	if (confirm(question)) {
		var loc = url + '?';
		for(var i in params) {
			loc += i + '=' + params[i] + '&';
		}
		loc += Math.random();
		window.location = loc;
	}
}

function teleportDialog(url, title, params){
	var teleport = getDialog();
	
	if(!teleport) return false;
	
	var html = '';
	html += '<form action="' + url + '?' + Math.random() + '" class="teleport-dialog-form nowrap" method="post" name="teleport-form" style="text-align: center;">';
	for(var v in params){
		if(v == "nick"){
			if(params.nick) {
				html += '' +
					'<label for="' + params.nick + '">�����:</label> ' +
					'<input class="text paste-nick" type="text" id="' + params.nick + '" name="' + params.nick + '" />' +
					'<br />';
			}
		}else if(v == "select"){
		}else{
			html +=
			'<input name="' + v + '" type="hidden" value="' + params[v] + '" />'
			;
		}
	}
	
	var counter = 1;
	
	params.select.list.sort(function(a, b) {
		return a.id - b.id;
	});
	
	for (var i in params.select.list) {
		if (params.select.current == params.select.list[i].id) {
			html += '<img alt="" class="current" src="' + imgURL + 'teleport/' + params.select.list[i].id + '.gif" title="' + params.select.list[i].name + '" />';
		} else {
			html += '<img alt="" onclick="forms[\'teleport-form\'].' + params.select.name + '.value = ' + params.select.list[i].id + '; forms[\'teleport-form\'].submit()" src="' + imgURL + 'teleport/' + params.select.list[i].id + '.gif" title="' + params.select.list[i].name + '" />';
		}
		
		if(counter % 4 == 0) html += '<br />';
			
		counter++;
	}
	
	html += '<input name="' + params.select.name + '" type="hidden" />' +
		'</form>';

	teleport.html(html);
	jQuery('.teleport-dialog-form img:not(.current)').hover(
		function() {
			jQuery(this).addClass('hover');
		},
		function() {
			jQuery(this).removeClass('hover');
		}
	);
	teleport.dialog('option', 'title', title);
	dialogOpen(teleport);
}

function commonPriceDialog(url, title, name, hiddens, value, price) {
	var common = getDialog();
	
	if(!common) return false;
	
	var html = '';
	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="price-table nowrap" method="post">';
	
	for(var i in hiddens) {
		if(hiddens[i]) {
			html += '' +
			'<input name="' + i + '" type="hidden" value="' + hiddens[i] + '" />';
		}
	}
	
	html += '' +
			'<table>' +
				'<tbody>' +
					'<tr>' +
						'<td>' +
							'<label for="' + name + '">�����:</label> ' +
						'</td>' +
						'<td>' +
							'<input class="text paste-nick" id="' + name + '" name="target" type="text"' + (value ? ' value="' + value + '"' : '') + ' />' +
						'</td>' +
						'<td>' +
							'<input type="submit" class="button" value="OK" />' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td>' +
							'<label for="magic-price">���� (��.):</label>' +
						'</td>' +
						'<td colspan="2">' +
							'<input class="text" id="magic-price" name="magic_price" type="text" value="' + price.value + '"/>' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td></td>' +
						'<td colspan="2">' + 
							'(���� �� ' +
							'<a class="js min-price" href="#">' + price.min + '</a>' +
							' �� ' + 
							'<a class="js max-price" href="#">' + price.max + '</a>' +
							' ��.)' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>' +
		'</form>';
	
	common.html(html);
	
	common.find('a.min-price').click(function() {
		jQuery('#magic-price').val(jQuery(this).text());
		return false;
	});
	
	common.find('a.max-price').click(function() {
		jQuery('#magic-price').val(jQuery(this).text());
		return false;
	});
	
	common.dialog('option', 'title', title);
	dialogOpen(common);
}

function commentDialog(url, title, name, params) {
	var dialog = getDialog();
	
	if(!dialog) return false;
	
	var html = '';
	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="comment-dialog-form" method="post">' +
			'<input name="cmd" type="hidden" value=' + params.cmd + ' />' +
			(params.nd ? '<input name="nd" type="hidden" value="' + params.nd + '" />' : '') +
			(params.entry ? '<input name="entry" type="hidden" value="' + params.entry + '" />' : '') +
			(params.num ? '<input name="num" type="hidden" value="' + params.num + '" />' : '') +
			
			'<table>' +
				'<tbody>' +
					'<tr>' +
						'<td class="label">' +
							'<label for="' + name + '">�����:</label>' +
						'</td>' +
						'<td class="input">' +
							'<input class="text paste-nick" type="text" id="' + name + '" name="' + name + '" />' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td class="label">' +
							'<label for="dialog-comment">' + params.param_text + ':</label>' +
						'</td>' +
						'<td class="input">' +
							'<input class="text" id="dialog-comment" maxlength="250" name="text" size="48" type="text" />' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td></td>' +
						'<td>' +
							'<input class="button" type="submit" value="OK" />' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>' +
		'</form>';
	dialog.html(html);
	dialog.dialog('option', 'title', title);
	dialogOpen(dialog);
	Hint3Name= name;
}

function commonVote(url, title, name, hiddens, radios) {
	var vote = getDialog();
	
	if(!vote) return false;
	
	var html = '';
	html += '<form action="' + url + '" class="vote-dialog-form nowrap" method="post" name="vote-form">';
	
	for(var i in hiddens) {
		if(hiddens[i]) {
			html += '' +
			'<input name="' + i + '" type="hidden" value="' + hiddens[i] + '" />';
		}
	}
	
	var radio;
	html += 	'<ul>';
	for(var i = 0; i < radios.length; i++) {
		radio = radios[i];
		html += '' +
					'<li>' +
						'<input id="vote-' + radio.value + '" name="' + name + '" type="radio" value="' + radio.value + '" />' +
						'<label for="vote-' + radio.value + '">' + radio.label + '</label>' +
					'</li>';
	}
	html += '' +
				'</ul>' +
				'<br />' +
				'<input class="button" type="submit" value="OK" />' +
			'</form>';

	vote.html(html);
	vote.dialog('option', 'title', title);
	dialogOpen(vote);
}

/**
 * ���������� ���� � ������� ���� � ����������� �����������
 */
function selectDialog(url, title, params) {
	var select = getDialog();
	
	if(!select) return false;
	
	var html = '';
	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="select-dialog-form" method="post">' +
			'<input name="cmd" type="hidden" value=' + params.cmd + ' />' +
			(params.nd ? '<input name="nd" type="hidden" value="' + params.nd + '" />' : '') +
			
			'<table>' +
				'<tbody>' +
					'<tr>' +
						'<td class="label">' +
							'<label for="' + params.nick + '">�����:</label>' +
						'</td>' +
						'<td class="input">' +
							'<input class="text paste-nick" type="text" id="' + params.nick + '" name="' + params.nick + '" />' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td class="label">' +
							(params.select.label ? '<label for="' + params.select.name + '">' + params.select.label + ':</label>' : '') +
						'</td>' +
						'<td class="input">' +
							'<select id="' + params.select.name + '" name="' + params.select.name + '">';
	for(var i in params.select.list) {
		html +=					'<option value="' + i +'">' + params.select.list[i] + '</option>';
	}
	html +=					'</select>' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td></td>' +
						'<td>' +
							'<input class="button" type="submit" value="OK" />' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>' +
		'</form>';
		
	select.html(html);
	select.dialog('option', 'title', title);
	dialogOpen(select);
}
/*
 * end: Dialog window functions
 */



/**
 * ����������� ��������� � ������� ������ ����
 */
function showNotification(text, error, title) {	
	if(text) {		
		if(!jQuery('#notofies').length) {
			jQuery('body').append('<div id="notifies" />');
		}
		var notify = jQuery.pnotify({
			pnotify_title: title ? title : '��������!',
			pnotify_type: error ? 'error' : 'notice',
			pnotify_text: text
		});
		notify.appendTo('#notifies').click(notify.pnotify_remove);
	}
}

function showHelpHint(helpHintObject) {	
	if(helpHintObject) {
		jQuery(function() {		
			var helpDialog = $('<div />', {
				css: {
					backgroundColor: helpHintObject.background,
					color: '#000000'
				},
				id: 'help-hint',
				html: helpHintObject.content,
				title: '���������'
			});
			
			helpDialog.append('<div style="margin-top: 1em; text-align: center; width: 100%;"><input class="button" type="button" value="�������" /></div>')
				
			helpDialog.appendTo('body').dialog({
				close: function(event, ui) {
					$.post('/settings_ajax.pl',{
						cmd: 'helphint_viewed',
						code: helpHintObject.code,
						is_ajax: 1
					});
				},
				dialogClass: 'help-hint',
				draggable: false,
				minHeight: 0,
				open: function(event, ui) {
					helpDialog.parent().css('opacity', .85);
				},
				width: Number(helpHintObject.width) + 22
			});
			
			helpDialog.find('.button').click(function() {
				helpDialog.dialog('close');
			});
			
			helpDialog.parent().append('<div class="tl"></div><div class="tr"></div><div class="bl"></div><div class="br"></div>');
	
			var helphint_time = (Number(helpHintObject.time) * 1000) || 60000;
			setTimeout(function() {
				helpDialog.dialog('close');
			}, helphint_time);
		});
	}
}

/**
 * ����������� ������� ��� �������� ����������� ���� ��� IE 6,7 � ������, ����� ��� ����� ����������
 * 
 * @see jQuery UI Dialog
 * @param {Object} dialog	jQuery-������ ����������� ����
 */
function dialogOpen(dialog) {
	width = dialog.dialog('option', 'width');
	if(width == 'auto' && jQuery.browser.msie && jQuery.browser.versionX < 8) {
		/*var prev = dialog.prev();
		prev.width(0);
		dialog.dialog('open');
		prev.width(dialog.outerWidth());*/
		
		var clone = $('<div />', {
			html: dialog.html()
		}); // jQuery 1.5+
		//var clone = dialog.clone(); // jQuery 1.4.4
		clone.appendTo('body').css({
			left: '-1000px',
			position: 'absolute',
			top: '-1000px'
		}).show();
		
		width = clone.width() + 22;
		dialog.dialog('option', 'width', width);
		dialog.dialog('open');
		clone.remove();
	} else {
		dialog.dialog('open');
	}
}

/**
 * ������ ��� ����������� ������� �������� � ������������
 * 
 * @param {Object} $ jQuery 1.4+
 */
(function($) {
	var interval = 100;
	var durationCoef = 3;
	var duration = interval * durationCoef;
	
	/**
	 * URL �������� ��� �������
	 * 
	 * @param {Object} o
	 * @param {Integer} width
	 */
	function getBgImage(o, width) {
		var css;
		if(width < 33) {
			css = '3';
		} else if(width < 66) {
			css = '2';
		} else {
			css = '1';
		}
		
		return imgURL + o.image + css + '.gif';
	}
	
	/**
	 * ������������� �������� �����
	 * 
	 * @param {Object} el
	 */
	function set(el) {
		var o = el.data('scale');
		if(o.current > o.max) {
			o.current = o.max;
		}
		
		var width = (o.max == 0) ? 0 : (o.current / o.max * 100);
		el.width(width + '%');
		
		var image = getBgImage(o, parseInt(width));
		el.css('background-image', 'url(' + image + ')');
	}
	
	/**
	 * ��������������� �� "������� �������" "������� �������"
	 * 
	 * @param {Object} el
	 */
	function regenerate(el) {
		if (typeof el != 'object') {
			el = this;
		}
		
		var o = el.data('scale');
		
		o.current += o.coef;
		
		if(o.current >= o.max) {
			clearInterval(o.interval);
			o.interval = 0;
		}
		
		set(el);
	}
	
	function getCoef(o) {
		return (o.max / 1000) * (o.speed / 100) / 1800 * interval;
	}
	
	$.fn.HpPw = function(options) {
		var self = true;
		for(var i in options) {
			if(options[i].self === false) {
				self = false;
			}
		}

		if(top.frames['panel'] && top.frames['panel'].$ && top.frames['panel'].$.fn.HpPwDigits && self) {
			top.frames['panel'].$('.hp-pw-digits').HpPwDigits(options);
		} else if(top.frames['menu'] && top.frames['menu'].$ && top.frames['menu'].$.fn.HpPwDigits && self) {
			top.frames['menu'].$('.hp-pw-digits').HpPwDigits(options);
		}
		
		return this.each(function() {
			var el = $(this);
			if (el.data('HpPw:options')) { // ���������� ������� �����
				var o = $(this).data('HpPw:options');
				
				for(var scale in options) {
					var scaleEl = $(this).find(o[scale].selector);
					var scaleOptions = scaleEl.data('scale');
					
					// ������������� �����������
					clearInterval(scaleOptions.interval);
					scaleOptions.interval = 0;
					
					// C����� �������� �����
					var current = scaleOptions.current;
					
					// ���������� ��������
					$.extend(scaleOptions, options[scale]);
					// ���������� �������� �������������� �����
					scaleOptions.coef = getCoef(scaleOptions);
					
					// ����� �������� ����� � ������ ��������
					var newCurWithAnim = Math.min(scaleOptions.current + scaleOptions.coef * durationCoef, scaleOptions.max);
					var delta = Math.abs(newCurWithAnim - current) / scaleOptions.max;
					
					if (delta > .1) { // ���� �������� ����� ������ ����������
						scaleOptions.current = newCurWithAnim;
						
						scaleEl.animate(
							{
								width: (newCurWithAnim / scaleOptions.max * 100) + '%'
							}, 
							duration, 'linear', 
							jQuery.proxy(function() {
								var scaleEl = this;
								var scaleOptions = scaleEl.data('scale');
								
								set(this);
							
								// ��������� ����������� � ������ �������������
								if(scaleOptions.speed && scaleOptions.current < scaleOptions.max) {
									scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
								}
							}, scaleEl)
						);
					} else {
						set(scaleEl);
						
						// ��������� ����������� � ������ �������������
						if(scaleOptions.speed && scaleOptions.max && scaleOptions.current < scaleOptions.max) {
							scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
						}
					}
				}
			} else { // �������������				
				var o = $.extend(true, {}, $.fn.HpPw.defaults, options);
				el.data('HpPw:options', o);
				
				for (var scale in o) {
					var scaleEl = el.find(o[scale].selector);
					var scaleOptions = o[scale];
					
					scaleOptions.coef = getCoef(scaleOptions);
					
					scaleEl.data('scale', scaleOptions);
					// ����������� ��������� ��� �������
					scaleEl.parent().tooltip({
						bodyHandler: function(){
							var data = $(this).children().data('scale');
							return '<span class="nowrap">' + data.name + ': ' + (data.invisible ? '??/??' : (Math.floor(data.current) + '/' + data.max)) + '</span>';
						},
						delay: 0,
						showURL: false,
						track: true
					});
					
					set(scaleEl);
					
					if (scaleOptions.speed && o[scale].max && scaleOptions.current < o[scale].max) {
						scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
					}
				}
			}
		});
	}
	
	$.fn.HpPw.defaults = {
		hp: {
			current: 1,
			image: 'hp',
			invisible: false,
			max: 1,
			name: '������� �����',
			selector: '.hp',
			speed: 0
		},
		pw: {
			current: 1,
			image: 'pw',
			invisible: false,
			max: 1,
			name: '������� ������������',
			selector: '.pw',
			speed: 0
		}
	}
})(jQuery);

/**
 * ������ ��� ����������� �������� �������� � ������������ � ����� ������� ����
 * 
 * @param {Object} $ jQuery 1.4+
 */
(function($) {
	var interval = 1000;
	var durationCoef = 3;
	var duration = interval * durationCoef;
	
	/**
	 * ������� ������ ��� ������� � ����������� �� ���������� ��������/������������
	 * 
	 * @param {Number} width
	 * @return {String} className
	 */
	function getClass(width) {
		var className = 'current';
		if(width < 33) {
			className += ' current-3';
		} else if(width < 66) {
			className += ' current-3';
		}
		
		return className;
	}
	
	/**
	 * ������������� �������� �����
	 * 
	 * @param {Object} el
	 */
	function set(el) {
		var o = el.data('scale');
		
		if(o.current > o.max) {
			o.current = o.max;
		}
		
		var className = getClass(o.current / o.max * 100);
		
		el.find('.current').text(Math.floor(o.current)).attr('class', className);
		el.find('.max').text(o.max);
	}
	
	/**
	 * ��������������� �� "������� �������" "������� �������"
	 * 
	 * @param {Object} el
	 */
	function regenerate(el) {
		if (typeof el != 'object') {
			el = this;
		}
		
		var o = el.data('scale');
		
		o.current += o.coef;
		
		if(o.current >= o.max) {
			clearInterval(o.interval);
			o.interval = 0;
		}
		
		set(el);
	}
	
	function getCoef(o) {
		return (o.max / 1000) * (o.speed / 100) / 1800 * interval;
	}
	
	$.fn.HpPwDigits = function(options) {
		return this.each(function() {
			var el = $(this);
			
			if(options.hp.max > 999 || options.pw.max > 999) {
				el.addClass('hp-pw-digits-small');
			} else {
				el.removeClass('hp-pw-digits-small');
			}
			
			if (el.data('HpPw:options')) { // ���������� ������� �����
				var o = $(this).data('HpPw:options');
				
				for(var scale in options) {
					var scaleEl = $(this).find(o[scale].selector);
					var scaleOptions = scaleEl.data('scale');
					
					// ������������� �����������
					clearInterval(scaleOptions.interval);
					scaleOptions.interval = 0;
					
					// ���������� ��������
					$.extend(scaleOptions, options[scale]);
					// ���������� �������� �������������� �����
					scaleOptions.coef = getCoef(scaleOptions);
					
					set(scaleEl);
					
					// ��������� ����������� � ������ �������������
					if(scaleOptions.regenerate && scaleOptions.current < scaleOptions.max) {
						scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
					}
				}
				
			} else { // �������������
				var o = $.extend(true, {}, $.fn.HpPwDigits.defaults, options);
				el.data('HpPw:options', o);
				
				for (var scale in o) {
					var scaleEl = el.find(o[scale].selector);
					var scaleOptions = o[scale];
					
					scaleOptions.coef = getCoef(scaleOptions);
					
					scaleEl.data('scale', scaleOptions);
					
					set(scaleEl);
					
					if (scaleOptions.regenerate && scaleOptions.speed && scaleOptions.current < o[scale].max) {
						scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
					}
				}
			}
		});
	}
	
	$.fn.HpPwDigits.defaults = {
		hp: {
			current: 1,
			image: 'hp',
			invisible: false,
			max: 1,
			regenerate: true,
			selector: '.hp',
			speed: 0
		},
		pw: {
			current: 1,
			image: 'pw',
			invisible: false,
			max: 1,
			regenerate: true,
			selector: '.pw',
			speed: 0
		}
	}
})(jQuery);

function itemSearchSubmit(form) {	
	if(form.search.value.length < 3) {
		alert('������� �������� ������. ������� ����� ���� ����.');
		return false;
	} else {
		return true;
	}
}

function itemSearchNoNumbers(event) {
	numcheck = /\d/;
	var keyCode = event.keyCode ? event.keyCode : event.charCode;
	return !numcheck.test(String.fromCharCode(keyCode));
}

function invisibilityOff(nd) {
	if (confirm('����� �����������?')) {
		$.post('/char.pl', {
			cmd: 'invisibility.off',
			is_ajax: 1,
			nd: nd
		}, function(json) {
			var notify = $.pnotify({
				pnotify_title: '��������!',
				pnotify_type: json.error ? 'error' : 'notice',
				pnotify_text: json.error ? json.error : json.message
			});
			notify.appendTo('#notifies').click(notify.pnotify_remove);
		}, 'json');
	}
}

/**
 * Nickname
 */
function getAlignName(alignId) {
	alignId = Number(alignId);
	
	switch(alignId) {
		case 10:
			return '���� ����';

		case 20:
			return '���� �����';
		
		case 30:
			return '���� �����';
		
		case 40:
			return '���� �������';
		
		case 15:
			return '������ ����';
		
		case 25:
			return '������ �����';
		
		case 35:
			return '������ �����';
		
		case 45:
			return '������ �������'
		
		case 19:
			return '������� ����';
		
		case 29:
			return '������� �����';
		
		case 39:
			return '������� �����';
		
		case 49:
			return '������� �������';
		
		default:
			return ''; 
	}
}

/**
 * ���������� ������ �� ���������� � ���������� � ���������� �������
 * 
 * @param {Integer} id
 * @return {String}
 */
function getAlignLink(id){
	id = Number(id);
	
	if (id) {
		var link = libURL + 'aligns/';
		
		switch(id) {
			case 10:
				link += 'dark/';
				break;
			
			case 20:
				link += 'light/';
				break;
			
			case 30:
				link += 'haos/';
				break;
			
			case 40:
				link += 'order/';
				break;
			
			case 15:
			case 25:
			case 35:
			case 45:
				link += 'consuls/';
				break;
				
			default:
				break;
		}
		
		return link;
	} else {
		return '';
	}
}

/**
 * ���������� ���������� ����������
 * 
 * @param {Integer} ax
 * @param {Integer} ay
 * @param {Integer} bx
 * @param {Integer} by
 * @return {Array}
 */
function getDungeonLocation(ax, ay, bx, by) {
	if (!bx) {
		bx = iDungeonX;
	}
	
	if (!by) {
		by = iDungeonY;
	}
	
	if (!bx || !by || !ax || !ay) {
		return false;
	}
	
	if (bx == ax && by == ay) {
		return false;
	}
	
	ay *= -1;
	by *= -1;
	
	var deltaX = ax - bx;
	var deltaY = ay - by;
	var steps = Math.ceil(Math.sqrt(deltaX * deltaX + deltaY * deltaY));
	
	var postfix = '��';
	if (steps.toString().match(/[234]$/)) {
		postfix = '�';
	}
	if (steps.toString().match(/1\d$/)) {
		postfix = '��';
	}
	if (steps.toString().match(/1$/)) {
		postfix = '';
	}
	
	var titles = {
		'e': '������',
		'w': "�����",
		's': '��',
		'n': '�����',
		'en': '������-������',
		"es": '���-������',
		"wn": '������-�����',
		"ws": '���-�����'
	};
	
	var quarter = 0;
	var degree = Math.atan(Math.abs(deltaY) / Math.abs(deltaX)) * 180 / Math.PI;
	if (deltaX < 0 && deltaY >= 0) {
		degree = 180 - degree;
	} else if (deltaX < 0 && deltaY < 0) {
		degree = 180 + degree;
	} else if (deltaX >= 0 && deltaY < 0) {
		degree = 360 - degree;
	}
	
	var arrow;
	if (degree >= 345 || degree < 15) {
		arrow = 'e';
	} else if (degree >= 15 && degree < 75) {
		arrow = 'en';
	} else if (degree >= 75 && degree < 105) {
		arrow = 'n';
	} else if (degree >= 105 && degree < 165) {
		arrow = 'wn';
	} else if (degree >= 165 && degree < 195) {
		arrow = 'w';
	} else if (degree >= 195 && degree < 255) {
		arrow = 'ws';
	} else if (degree >= 255 && degree < 295) {
		arrow = 's';
	} else {
		arrow = 'es';
	}
	
	return [arrow, titles[arrow] + ', ' + steps + ' ���' + postfix];
}

var defaultNickOptions = {
	alcohol: 0, // ������� ��������
	align: '', // {code: 29} ����������
	anotherCity: false, // �������� ��������� � ������ ������
	append: '', // ���������� ������������� ����������� � ���� ������
	battle: false, // �� � �����
	blind: false, // �������
	bot: '', // bot {attack: false, count: 1, id: 123, speak: false} 
	clan: '', // ���� clan: {img: '', name: ''}
	ownerDocument: document, // ������������� � ��������
	dungeon: '', // ���������� � ���������� dungeon: {hp: 0, maxhp:0, self: false, x: 0, y: 0}
	eventSide: '', //  
	guild: '', // ������� guild: {img: '', name: ''}
	ignore: false, // �������������
	invisible: false, // ���������
	isMentor: false, // �������� �� ������� �������� �����������
	hobble: false, // ����
	hurt: 0, // ������
	level: '', // �������
	mentor: false, // id ����������
	name: '', // ��� ���������
	nickClass: 'nick', // ����� ����
	offline: false, // �������� � ��������
	privateMess: true, // ���������� ������ ���������� ���������
	returnHtml: false, // ���������� ������� ������ ������
	sex: '', // ���
	silence: false, // ��������
	showEmpty: false // �� ���������� ������ ������������ ������ ����������� ����������, ������ � �������
}

var eventSidesNames = [
	/*
	'"����"', 
	'"Green Peace"', 
	'"���������"', 
	'"����"', 
	'"������"'
	*/
	'"������"',
	'"�������"',
	'"�������"'
	/*
	'"��������"',
	'"������"',
	'"�������"'
	*/
];

/**
 * ���������� ��� ���������
 * 
 * @param {Object} options	��������� ��. ���� defaultNickOptions
 * @return {Object|String}	Object - �� ���������, String, ����� returnHtml == true
 */
function getNick(options) {
	var $ = jQuery;
	options = $.extend({}, defaultNickOptions, options);
	
	if(!options.level) {
		options.level = 0;
	}
	
	var nickClass = options.nickClass;
	
	if(options.anotherCity) {
		nickClass += ' nick-another-city';
	}
	
	if(options.offline) {
		nickClass += ' nick-offline';
	}
	
	if(options.ignore) {
		nickClass += ' nick-ignore';
	}
	
	var nickname = $(options.ownerDocument.createElement('span')).attr({
		'class': nickClass
	});
	
	// ������ �������� ��� ��������� ������
	var height = $(options.ownerDocument.createElement('img')).attr({
		alt: '',
		'class': 'empty empty-height',
		src: nullImg
	});
	
	if (options.invisible) { // ���������
		var invisible = $(options.ownerDocument.createElement('i')).text('���������');
		
		nickname.append(invisible);
	} else {
		if (options.showEmpty) {
			var empty = $(options.ownerDocument.createElement('img')).attr({
				alt: '',
				'class': 'empty',
				src: nullImg
			});
		}
		
		// ��������� ���������
		if (options.privateMess && !options.bot) { 
			var privateMess = $(options.ownerDocument.createElement('img')).attr({
				alt: '',
				'class': 'private',
				src: imgURL + 'pr' + (options.offline || options.anotherCity ? '_grey' : '') + '.gif',
				title: '��������� ���������'
			});
			nickname.append(privateMess);
		} else if(options.showEmpty) {
			nickname.append(empty.clone().addClass('empty-private'));
		}
		
		// ������������ ��������� � ��� �� ������
		if (!options.anotherCity) {			
			// ���������� � ����������
			if(options.dungeon) {
				var location = [];
				if(sUser && sUser != options.name && options.dungeon.x && options.dungeon.y && iDungeonX && iDungeonY && getDungeonLocation) {
					location = getDungeonLocation(options.dungeon.x, options.dungeon.y);
				}
				var coords = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'location',
					src: imgURL + 'dungeon/online/arrow-' + (location[0] ? location[0] : 'dot') + '.gif',
					title: location[1] ? location[1] : ''
				});
				nickname.append(coords);
			}
			
			if(options.eventSide) {				
				var side = $(options.ownerDocument.createElement('img')).attr({
					alt: '�������',
					'class': 'event-side',
					src: imgURL + 'zarnica2009_' + options.eventSide + '.gif',
					title: '������� ' + eventSidesNames[options.eventSide - 1]
				});
				nickname.append(side);
			}
			
			if (options.topicon == 1) {
				var topicon = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'topicon',
					src: imgURL + 'icons/top.gif',
					title: '���'
				});
				
				nickname.append(topicon);
			}
			
			// ����������
			if(typeof options.align != 'undefined' && (Number(options.align) || Number(options.align.code))) {
				if(typeof options.align == 'object') {
					options.align = Number(options.align.code); 
				} else {
					options.align = Number(options.align);
				}
				
				var alignLink = $(options.ownerDocument.createElement('a')).attr({
					'class': 'align',
					href: getAlignLink(options.align),
					target: '_blank'
				});
				
				var align = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'align',
					src: imgURL + 'align' + options.align + '.gif',
					title: getAlignName(options.align)
				}, options.ownerDocument);
				
				alignLink.append(align);
				nickname.append(alignLink);
			} else if (options.showEmpty) {
				nickname.append(empty.clone());
			}
			
			// ����
			if (options.clan && options.clan.name) { 
				var clanLink = $(options.ownerDocument.createElement('a')).attr({
					'class': 'clan',
					href: libURL + 'clans/' + options.clan.img + '/',
					target: '_blank'
				});
				var clan = $(options.ownerDocument.createElement('img')).attr({
					alt: '����',
					'class': 'clan',
					src: imgURL + 'klan/' + options.clan.img + '.gif',
					title: options.clan.name
				});
				clanLink.append(clan);
				nickname.append(clanLink);
				
			// �������
			} else if (options.guild && options.guild.name) { 
				var guild = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'guild',
					src: imgURL + 'guild/' + options.guild.img + '.gif',
					title: options.guild.name
				});
				
				nickname.append(guild);
			}
			
			// ��������� ���������
			if (((typeof top.isMentor != 'undefined' && top.isMentor) || options.isMentor) && !options.mentor && !options.bot && options.level == 0) {
				var study = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'study',
					src: imgURL + 'study.gif',
					title: '��������� ���������'
				});
				
				nickname.append(study);
			}
			
			// ���: ����������
			if(options.bot && options.bot.count) {
				var count = $(options.ownerDocument.createElement('span')).attr({
					'class': 'count'
				}).text('(' + options.bot.count + ')');
				nickname.append(count);
			}
		}
		
		if (nickname.html() != '') { // ������ � �����
			nickname.append(' ');
		}
		
		// ���
		var name = $(options.ownerDocument.createElement('b')).attr({
			'class': 'name'
		}).text(options.name);
		
		if(options.bot) {
			name.addClass('bot');
			if(options.bot.speak) {
				name.addClass('speak');
			}
		}
		
		if (options.battle) {
			name.addClass('battle');
		}
		
		nickname.append(name);
		
		// �������
		if (!options.anotherCity) {
			var level = $(options.ownerDocument.createElement('span')).attr({
				'class': 'level'
			}).text('[' + options.level + ']');
			nickname.append(' ').append(level);
		}
		nickname.append(' ');
		
		// ���������� � ������������
		var href = '/inf.pl?user=';
		if(document.domain.toString().split('.')[0] == 'top')
			href = 'http://avrora.carnage.ru' + href;
			
		var info = $(options.ownerDocument.createElement('a')).attr({
			'class': 'info',
			href: href + options.name.replace(/\s/g, '+'),
			onclick: 'window.open(\'' + href + urlEncode(options.name) + '\'); return false;',
			target: '_blank',
			title: '���������� � ���������'
		});
		
		// ���� ������ "?" � ����������� �� ����
		var sex = $(options.ownerDocument.createElement('img')).attr({
			alt: '',
			'class': 'sex',
			src: imgURL + 'inf' + (options.anotherCity ? '' : options.sex) + '.gif'
		});
		info.append(sex);
		nickname.append(info);
		
		if(options.alcohol || options.blind || options.silence || options.hobble || options.hurt) { // ������ ��� ���. ������
			nickname.append(' ');
		}
		
		// ��������
		if (options.alcohol > 0) {
			var title = '';
			
			if(options.alcohol < 16) {
				title = '������ ����' + (options.sex == 'M' ? '' : '�');
			} else if (options.alcohol < 32) {
				title = '����' + (options.sex == 'M' ? '' : '�');
			} else if (options.alcohol < 64) {
				title = '������ ����' + (options.sex == 'M' ? '' : '�');
			} else {
				title = '����' + (options.sex == 'M' ? '' : '�') + ' � �������';
			}
			
			var alcohol = $(options.ownerDocument.createElement('img')).attr({
				alt: '��������',
				'class': 'alcohol',
				src: imgURL + 'ch_alcohol.gif',
				title: title
			});
			nickname.append(alcohol);
		}
		
		// ��������
		if (options.silence) { 
			var silence = $(options.ownerDocument.createElement('img')).attr({
				alt: '��������',
				'class': 'silence',
				src: imgURL + 'ch_silence.gif',
				title: '�������� �������� ��������'
			});
			nickname.append(silence);
		}
		
		// �������
		if (options.blind) { 
			var silence = $(options.ownerDocument.createElement('img')).attr({
				alt: '�������',
				'class': 'blind',
				src: imgURL + 'ch_blind.gif',
				title: '�������� �������� �������'
			});
			nickname.append(silence);
		}
		
		// ����
		if (options.hobble) { 
			var hobble = $(options.ownerDocument.createElement('img')).attr({
				alt: '����',
				'class': 'hobble',
				src: imgURL + 'ch_hobble.gif',
				title: '�������� �������� ���'
			});
			nickname.append(hobble);
		}
		
		// ������
		if (options.hurt) { 
			var hurt = $(options.ownerDocument.createElement('img')).attr({
				alt: '������',
				'class': 'hurt',
				src: imgURL + 'ch_travma.gif',
				title: '�����������' + (options.sex == 'M' ? '' : '�')
			});
			nickname.append(hurt);
		}

		// ���������
		if (options.zimmune) { 
			var hurt = $(options.ownerDocument.createElement('img')).attr({
				alt: '��������� � ��������� � ���� "�������"',
				'class': 'zimmune',
				src: imgURL + 'ch_zarnica_immun.gif',
				title: '��������� � ��������� � ���� "�������"'
			});
			nickname.append(hurt);
		}
		
		// �������� ��������� � ����������
		if(options.dungeon) {
			var hp = $(options.ownerDocument.createElement('span')).attr({
				'class': 'hp'
			}).text('[' + options.dungeon.hp + '/' + options.dungeon.maxhp + ']');
			nickname.append(' ').append(hp);
		}
		
		// 
		if(options.append) {
			nickname.append(options.append);
		}
		
		if (options.showEmpty) { // ������� ������� �������� �������
			empty.remove();
		}
	}
	
	if (options.returnHtml) {
		return '<span class="' + nickClass + '">' + nickname.html() + '</span>';
	} else {
		return nickname;
	}
}
/* end: Nickname */

/*
 * Smilies
 */
var smilies = {
	'agree': {
		codes: ['agree', '������'],
		group: 2,
		height: 20,
		width: 44
	},
	'angel': {
		codes: ['angel', '�����'],
		group: 1,
		height: 21,
		width: 41
	},
	'badevil': {
		codes: ['badevil', '������'],
		group: 1,
		height: 20,
		width: 20
	},
	'badtease': {
		codes: ['badtease', '������'],
		group: 1,
		height: 20,
		width: 29
	},
	'beer': {
		codes: ['beer', '����'],
		group: 2,
		height: 20,
		width: 59
	},
	'boy': {
		codes: ['boy', '������'],
		group: 1,
		height: 22,
		width: 21
	},
	'bye': {
		codes: ['bye', '����'],
		group: 1,
		height: 20,
		width: 20
	},
	'chao': {
		codes: ['chao', '���'],
		group: 1,
		height: 20,
		width: 25
	},
	'cheek': {
		codes: ['cheek', '����'],
		group: 2,
		height: 20,
		width: 39
	},
	'plak': {
		codes: ['plak', '����'],
		group: 1,
		height: 20,
		width: 23
	},
	'evil': {
		codes: ['evil', '������'],
		group: 1,
		height: 23,
		width: 20
	},
	'ponder': {
		codes: ['ponder', '�����'],
		group: 1,
		height: 20,
		width: 20
	},
	'girl': {
		codes: ['girl', '�������'],
		height: 22,
		group: 1,
		width: 21
	},
	'gy': {
		codes: ['gy', '��'],
		height: 20,
		group: 1,
		width: 33
	},
	'haha': {
		codes: ['haha', '����'],
		height: 21,
		group: 1,
		width: 26
	},
	'hi': {
		codes: ['hi', '������'],
		height: 22,
		group: 1,
		width: 29
	},
	'laught': {
		codes: ['laught', '����'],
		height: 20,
		group: 1,
		width: 20
	},
	'mib': {
		codes: ['mib', '���'],
		height: 20,
		group: 1,
		width: 20
	},
	'no': {
		codes: ['no', '���'],
		height: 20,
		group: 1,
		width: 20
	},
	'notknow': {
		codes: ['notknow', '������'],
		height: 20,
		group: 1,
		width: 39
	},
	'nunu': {
		codes: ['nunu', '����'],
		height: 20,
		group: 1,
		width: 24
	},
	'poll': {
		codes: ['poll', '�����'],
		height: 25,
		group: 1,
		width: 37
	},
	'protest': {
		codes: ['protest', '�������'],
		height: 25,
		group: 1,
		width: 33
	},
	'roll': {
		codes: ['roll', '�������'],
		height: 20,
		group: 1,
		width: 20
	},
	'rupor': {
		codes: ['rupor', '�����'],
		height: 20,
		group: 1,
		width: 36
	},
	'shuffle': {
		codes: ['shuffle', '�����'],
		height: 20,
		group: 1,
		width: 20
	},
	'tongue': {
		codes: ['tongue', '����'],
		height: 20,
		group: 1,
		width: 20
	},
	'upset': {
		codes: ['upset', '��������'],
		height: 24,
		group: 1,
		width: 20
	},
	'wink': {
		codes: ['wink', '�������'],
		height: 20,
		group: 1,
		width: 20
	},
	'wow': {
		codes: ['wow', '���'],
		height: 20,
		group: 1,
		width: 20
	},
	'yawn': {
		codes: ['yawn', '�����'],
		height: 20,
		group: 1,
		width: 20
	},
	'yes': {
		codes: ['yes', '��'],
		height: 20,
		group: 1,
		width: 20
	},
	'eyes': {
		codes: ['eyes', '������'],
		height: 20,
		group: 1,
		width: 20
	},
	'fire': {
		codes: ['fire', '�����'],
		height: 20,
		group: 1,
		width: 20
	},
	'ok': {
		codes: ['ok', '��'],
		height: 23,
		group: 1,
		width: 25
	},
	'pray': {
		codes: ['pray', '�������'],
		height: 22,
		group: 1,
		width: 35
	},
	'smash': {
		codes: ['smash', '���������'],
		height: 29,
		group: 1,
		width: 34
	},
	'agent': {
		codes: ['agent', '�����'],
		height: 20,
		group: 1,
		width: 20
	},
	'swear': {
		codes: ['swear', '������'],
		height: 20,
		group: 2,
		width: 43
	},
	'eusa': {
		codes: ['eusa', '����'],
		height: 22,
		group: 1,
		width: 26
	},
	'fingal': {
		codes: ['fingal', '������'],
		height: 20,
		group: 1,
		width: 23
	},
	'flame': {
		codes: ['flame', '������'],
		height: 20,
		group: 1,
		width: 31
	},
	'kz': {
		codes: ['kz', '��'],
		height: 26,
		group: 1,
		width: 20
	},
	'moder': {
		codes: ['moder', '�������'],
		height: 20,
		group: 1,
		width: 85
	},
	'duel': {
		codes: ['duel', '�����'],
		height: 34,
		group: 2,
		width: 101
	},
	'help': {
		codes: ['help', '������'],
		height: 20,
		group: 1,
		width: 27
	},
	'lebedi': {
		codes: ['lebedi', '������'],
		height: 25,
		group: 2,
		width: 75
	},
	'vodka': {
		codes: ['vodka', '�����'],
		height: 31,
		group: 1,
		width: 29
	},
	'vals': {
		codes: ['vals', '�����'],
		height: 24,
		group: 2,
		width: 93
	},
	'boylove': {
		codes: ['boylove', '�������'],
		height: 22,
		group: 1,
		width: 20
	},
	'girllove': {
		codes: ['girllove', '��������'],
		height: 22,
		group: 1,
		width: 20
	},
	'stp': {
		codes: ['stp', '�����'],
		height: 24,
		group: 2,
		width: 42
	},
	'hb': {
		codes: ['hb', '�������'],
		height: 29,
		group: 1,
		width: 31
	},
	'invalid': {
		codes: ['invalid', '�������'],
		height: 23,
		group: 1,
		width: 74
	},
	'smoke': {
		codes: ['smoke', '���'],
		height: 22,
		group: 1,
		width: 21
	},
	'susel': {
		codes: ['susel', '�����'],
		height: 29,
		group: 1,
		width: 64
	},
	'joker': {
		codes: ['joker', '���'],
		height: 31,
		group: 1,
		width: 33
	},
	'sneaky': {
		codes: ['sneaky', '��������'],
		height: 20,
		group: 1,
		width: 20
	},
	'kap': {
		codes: ['kap', '������'],
		height: 36,
		group: 1,
		width: 50
	},
	'kuku': {
		codes: ['kuku', '����'],
		height: 21,
		group: 1,
		width: 24
	},
	'skull': {
		codes: ['skull', '�����'],
		height: 23,
		group: 1,
		width: 23
	},
	'ura': {
		codes: ['ura', '���'],
		height: 40,
		group: 1,
		width: 40
	},
	'order': {
		codes: ['order', '�����'],
		height: 33,
		group: 1,
		width: 48
	},
	'chaos': {
		codes: ['chaos', '����'],
		height: 32,
		group: 1,
		width: 47
	},
	'hug': {
		codes: ['hug', '���'],
		group: 2,
		height: 30,
		width: 60
	},
	'maniak': {
		codes: ['maniak', '������'],
		group: 2,
		height: 37,
		width: 100
	},
	'tango': {
		codes: ['tango', '�����'],
		group: 2,
		height: 25,
		width: 66
	},
	'ma': {
		codes: ['ma', '����'],
		group: 2,
		height: 24,
		width: 48
	},
	'bolen': {
		codes: ['bolen', '�����'],
		group: 1,
		height: 30,
		width: 23
	},
	'kach': {
		codes: ['kach', '�����'],
		group: 1,
		height: 31,
		width: 40
	},
	'kiss': {
		codes: ['kiss', '�������'],
		group: 2,
		height: 23,
		width: 51
	},
	'dustman': {
		codes: ['dustman', '��������'],
		group: 2,
		height: 33,
		width: 52
	},
	'ghost': {
		codes: ['ghost', '����'],
		group: 1,
		height: 40,
		width: 30
	},
	'rev': {
		codes: ['rev', '�������'],
		group: 1,
		height: 24,
		width: 39
	},
	'appl': {
		codes: ['appl', '����'],
		group: 1,
		height: 21,
		width: 28
	},
	'confused': {
		codes: ['confused', '������'],
		group: 1,
		height: 19,
		width: 19
	},
	'hit': {
		codes: ['hit', '������'],
		group: 2,
		height: 22,
		width: 45
	},
	'loony': {
		codes: ['loony', '����'],
		group: 1,
		height: 30,
		width: 36
	},
	'serenada': {
		codes: ['serenada', '��������'],
		group: 2,
		height: 52,
		width: 33
	},
	'stinky': {
		codes: ['stinky', '���'],
		group: 2,
		height: 28,
		width: 53
	},
	'budo': {
		codes: ['budo', '�������'],
		group: 1,
		height: 37,
		width: 54
	},
	'kosa': {
		codes: ['kosa', '����'],
		group: 2,
		height: 23,
		width: 48
	},
	'klizma': {
		codes: ['klizma', '������'],
		group: 1,
		height: 37,
		width: 37
	},
	'dwarf': {
		codes: ['dwarf', '������'],
		group: 1,
		height: 48,
		width: 46
	},
	'yahoo': {
		codes: ['yahoo', '���'],
		group: 1,
		height: 27,
		width: 42
	},
	'roga': {
		codes: ['roga', '����'],
		group: 2,
		height: 32,
		width: 65
	},
	'dud': {
		codes: ['dud', '�����'],
		group: 1,
		height: 37,
		width: 64
	},
	'chupa': {
		codes: ['chupa', '�����'],
		group: 1,
		height: 24,
		width: 27
	},
	'friday': {
		codes: ['friday', '�������'],
		group: 2,
		height: 28,
		width: 57
	},
	'sleep': {
		codes: ['sleep', '���'],
		group: 1,
		height: 22,
		width: 21
	},
	'tooth': {
		codes: ['tooth', '�����'],
		group: 1,
		height: 23,
		width: 33
	},
	'wow2': {
		codes: ['wow2', '���2'],
		group: 1,
		height: 20,
		width: 32
	},
	'strah': {
		codes: ['strah', '�����'],
		group: 1,
		height: 28,
		width: 35
	},
	'007': {
		codes: ['007'],
		group: 1,
		height: 23,
		width: 23
	},
	'buket': {
		codes: ['buket', '�����'],
		group: 2,
		height: 30,
		width: 50
	},
	'nobravo': {
		codes: ['nobravo', '��������'],
		group: 1,
		height: 26,
		width: 35
	},
	'beggar': {
		codes: ['beggar', '������'],
		group: 1,
		height: 27,
		width: 35
	},
	'box': {
		codes: ['box', '����'],
		group: 1,
		height: 22,
		width: 28
	},
	'bravo': {
		codes: ['bravo', '�����'],
		group: 1,
		height: 26,
		width: 35
	},
	'plak2': {
		codes: ['plak2', '����2'],
		group: 1,
		height: 32,
		width: 47
	},
	'death': {
		codes: ['death', '������'],
		group: 2,
		height: 40,
		width: 70
	},
	'defender': {
		codes: ['defender', '������'],
		group: 2,
		height: 30,
		width: 60
	},
	'dont': {
		codes: ['dont', '������'],
		group: 1,
		height: 29,
		width: 28
	},
	'eating': {
		codes: ['eating', '���'],
		group: 1,
		height: 20,
		width: 25
	},
	'figa': {
		codes: ['figa', '����'],
		group: 1,
		height: 25,
		width: 27
	},
	'idea': {
		codes: ['idea', '����'],
		group: 1,
		height: 28,
		width: 48
	},
	'kruger': {
		codes: ['kruger', '������'],
		group: 1,
		height: 27,
		width: 34
	},
	'mad': {
		codes: ['mad', '����'],
		group: 1,
		height: 23,
		width: 48
	},
	'ok2': {
		codes: ['ok2', '��2'],
		group: 1,
		height: 21,
		width: 22
	},
	'pank': {
		codes: ['pank', '����'],
		group: 1,
		height: 31,
		width: 28
	},
	'pray2': {
		codes: ['pray2', '�������2'],
		group: 1,
		height: 24,
		width: 27
	},
	'priz': {
		codes: ['priz', '����'],
		group: 2,
		height: 27,
		width: 50
	},
	'diler': {
		codes: ['diler', '������'],
		group: 1,
		height: 30,
		width: 36
	},
	'shock': {
		codes: ['shock', '���'],
		group: 2,
		height: 25,
		width: 63
	},
	'smoked': {
		codes: ['smoked', '����'],
		group: 1,
		height: 42,
		width: 33
	},
	'zasos': {
		codes: ['zasos', '�����'],
		group: 2,
		height: 27,
		width: 44
	},
	'bang': {
		codes: ['bang', '�����'],
		group: 1,
		height: 23,
		width: 30
	},
	'hiya': {
		codes: ['hiya', '�����'],
		group: 1,
		height: 36,
		width: 23
	},
	'lol': {
		codes: ['lol', '���'],
		group: 1,
		height: 17,
		width: 17
	},
	'gipno': {
		codes: ['gipno', '������'],
		group: 1,
		height: 20,
		width: 20
	},
	'q': {
		codes: ['q', '��'],
		group: 1,
		height: 30,
		width: 42
	},
	'zub': {
		codes: ['zub', '���'],
		group: 1,
		height: 22,
		width: 20
	},
	'newyear': {
		codes: ['newyear', '��'],
		group: 2,
		height: 48,
		width: 47
	},
	'moroz': {
		codes: ['moroz', '��������'],
		group: 1,
		height: 31,
		width: 64
	},
	'snowgirl': {
		codes: ['snowgirl', '��������'],
		group: 1,
		height: 35,
		width: 35
	},
	'medic': {
		codes: ['medic', '�����'],
		group: 1,
		height: 31,
		width: 35
	},
	'angel2': {
		codes: ['angel2', '�����2'],
		group: 1,
		height: 27,
		width: 37
	},
	'archer': {
		codes: ['archer', '������'],
		group: 2,
		height: 40,
		width: 80
	},
	'ass1': {
		codes: ['ass1', '����1'],
		group: 1,
		height: 26,
		width: 25
	},
	'ass2': {
		codes: ['ass2', '����2'],
		group: 1,
		height: 28,
		width: 25
	},
	'candleman': {
		codes: ['candleman', '�����'],
		group: 1,
		height: 27,
		width: 29
	},
	'compkiller': {
		codes: ['compkiller', '��������'],
		group: 1,
		height: 29,
		width: 38
	},
	'flytryin': {
		codes: ['flytryin', '������'],
		group: 1,
		height: 37,
		width: 36
	},
	'gimme5': {
		codes: ['gimme5', '���5'],
		group: 2,
		height: 20,
		width: 41
	},
	'horseman': {
		codes: ['horseman', '�������'],
		group: 1,
		height: 42,
		width: 60
	},
	'loveposter': {
		codes: ['loveposter', '������'],
		group: 2,
		height: 20,
		width: 51
	},
	'sexover': {
		codes: ['sexover', '����'],
		group: 2,
		height: 33,
		width: 60
	},
	'skripka': {
		codes: ['skripka', '�������'],
		group: 2,
		height: 45,
		width: 51
	},
	'waitlover': {
		codes: ['waitlover', '���'],
		group: 2,
		height: 36,
		width: 51
	},
	'writer': {
		codes: ['writer', '��������'],
		group: 1,
		height: 55,
		width: 43
	},
	'victory': {
		codes: ['victory', '������'],
		group: 1,
		height: 23,
		width: 30
	},
	'air_kiss': {
		codes: ['air_kiss', '����2'],
		group: 1,
		height: 26,
		width: 24
	},
	'king': {
		codes: ['king', '������'],
		group: 1,
		height: 28,
		width: 28
	},
	'sv3': {
		codes: ['sv3', '����������'],
		group: 2,
		height: 25,
		width: 43
	}
}

smileyCodes = {
	'agree': 'agree',
	'������': 'agree',
	
	'angel': 'angel',
	'�����': 'angel',
	
	'badevil': 'badevil',
	'������': 'badevil',
	
	'badtease': 'badtease',
	'������': 'badtease',
	
	'beer': 'beer',
	'����': 'beer',
	
	'boy': 'boy',
	'������': 'boy',
	
	'bye': 'bye',
	'����': 'bye',
	
	'chao': 'chao',
	'���': 'chao',
	
	'cheek': 'cheek',
	'����': 'cheek',
	
	'plak': 'plak',
	'����': 'plak',
	
	'evil': 'evil',
	'������': 'evil',
	
	'ponder': 'ponder',				
	'�����': 'ponder',
	
	'girl': 'girl',
	'�������': 'girl',
	
	'gy': 'gy',					
	'��': 'gy',
	
	'haha': 'haha',
	'����': 'haha',
	
	'hi': 'hi',
	'������': 'hi',
	
	'laught': 'laught',
	'����': 'laught',
	
	'mib': 'mib',
	'���': 'mib',
	
	'no': 'no',
	'���': 'no',
	
	'notknow': 'notknow',
	'������': 'notknow',
	
	'nunu': 'nunu',
	'����': 'nunu',
	
	'poll': 'poll',
	'�����': 'poll',
	
	'protest': 'protest',
	'�������': 'protest',
	
	'roll': 'roll',
	'�������': 'roll',
	
	'rupor': 'rupor',
	'�����': 'rupor',
	
	'shuffle': 'shuffle',
	'�����': 'shuffle',
	
	'tongue': 'tongue',
	'����': 'tongue',
	
	'upset': 'upset',
	'��������': 'upset',
	
	'wink': 'wink',
	'�������': 'wink',
	
	'wow': 'wow',
	'���': 'wow',
	
	'yawn': 'yawn',
	'�����': 'yawn',
	
	'yes': 'yes',
	'��': 'yes',
	
	'eyes': 'eyes',
	'������': 'eyes',
	
	'fire': 'fire',
	'�����': 'fire',
	
	'ok': 'ok',
	'��': 'ok',
	
	'pray': 'pray',
	'�������': 'pray',
	
	'smash': 'smash',
	'���������': 'smash',
	
	'agent': 'agent',
	'�����': 'agent',
	
	'swear': 'swear',
	'������': 'swear',
	
	'eusa': 'eusa',
	'����': 'eusa',
	
	'fingal': 'fingal',
	'������': 'fingal',
	
	'flame': 'flame',
	'������': 'flame',
	
	'kz': 'kz',
	'��': 'kz',
	
	'moder': 'moder',
	'�������': 'moder',
	
	'duel': 'duel',
	'�����': 'duel',
	
	'help': 'help',
	'������': 'help',
	
	'lebedi': 'lebedi',
	'������': 'lebedi',
	
	'vodka': 'vodka',
	'�����': 'vodka',
	
	'vals': 'vals',
	'�����': 'vals',
	
	'boylove': 'boylove',
	'�������': 'boylove',
	
	'girllove': 'girllove',
	'��������': 'girllove',
	
	'stp': 'stp',
	'�����': 'stp',
	
	'hb': 'hb',
	'�������': 'hb',
	
	'invalid': 'invalid',
	'�������': 'invalid',
	
	'smoke': 'smoke',
	'���': 'smoke',
	
	'susel': 'susel',
	'�����': 'susel',
	
	'joker': 'joker',
	'���': 'joker',
	
	'sneaky': 'sneaky',
	'��������': 'sneaky',
	
	'kap': 'kap',
	'������': 'kap',
	
	'kuku': 'kuku',
	'����': 'kuku',
	
	'skull': 'skull',
	'�����': 'skull',
	
	'ura': 'ura',
	'���': 'ura',
	
	'order': 'order',
	'�����': 'order',
	
	'chaos': 'chaos',
	'����': 'chaos',
	
	'hug': 'hug',
	'���': 'hug',
	
	'maniak': 'maniak',
	'������': 'maniak',
	
	'tango': 'tango',
	'�����': 'tango',
	
	'ma': 'ma',
	'����': 'ma',
	
	'bolen': 'bolen',
	'�����': 'bolen',
	
	'kach': 'kach',
	'�����': 'kach',
	
	'kiss': 'kiss',
	'�������': 'kiss',
	
	'dustman': 'dustman',
	'��������': 'dustman',
	
	'ghost': 'ghost',
	'����': 'ghost',
	
	'rev': 'rev',
	'�������': 'rev',
	
	'appl': 'appl',
	'����': 'appl',
	
	'confused': 'confused',
	'������': 'confused',
	
	'hit': 'hit',
	'������': 'hit',
	
	'loony': 'loony',
	'����': 'loony',
	
	'serenada': 'serenada',
	'��������': 'serenada',
	
	'stinky': 'stinky',
	'���': 'stinky',
	
	'budo': 'budo',
	'�������': 'budo',
	
	'kosa': 'kosa',
	'����': 'kosa',
	
	'klizma': 'klizma',
	'������': 'klizma',
	
	'dwarf': 'dwarf',
	'������': 'dwarf',
	
	'yahoo': 'yahoo',
	'���': 'yahoo',
	
	'roga': 'roga',
	'����': 'roga',
	
	'dud': 'dud',
	'�����': 'dud',
	
	'chupa': 'chupa',
	'�����': 'chupa',
	
	'friday': 'friday',
	'�������': 'friday',
	
	'sleep': 'sleep',
	'���': 'sleep',
	
	'tooth': 'tooth',
	'�����': 'tooth',
	
	'wow2': 'wow2',
	'���2': 'wow2',
	
	'strah': 'strah',
	'�����': 'strah',
	
	'007': '007',
	
	'buket': 'buket',
	'�����': 'buket',
	
	'nobravo': 'nobravo',
	'��������': 'nobravo',
	
	'beggar': 'beggar',
	'������': 'beggar',
	
	'box': 'box',
	'����': 'box',
	
	'bravo': 'bravo',
	'�����': 'bravo',
	
	'plak2': 'plak2',
	'����2': 'plak2',
	
	'death': 'death',
	'������': 'death',
	
	'defender': 'defender',
	'������': 'defender',
	
	'dont': 'dont',
	'������': 'dont',
	
	'eating': 'eating',
	'���': 'eating',
	
	'figa': 'figa',
	'����': 'figa',
	
	'idea': 'idea',
	'����': 'idea',
	
	'kruger': 'kruger',
	'������': 'kruger',
	
	'mad': 'mad',
	'����': 'mad',
	
	'ok2': 'ok2',
	'��2': 'ok2',
	
	'pank': 'pank',
	'����': 'pank',
	
	'pray2': 'pray2',
	'�������2': 'pray2',
	
	'priz': 'priz',
	'����': 'priz',
	
	'diler': 'diler',
	'������': 'diler',
	
	'shock': 'shock',
	'���': 'shock',
	
	'smoked': 'smoked',
	'����': 'smoked',
	
	'zasos': 'zasos',
	'�����': 'zasos',
	
	'bang': 'bang',
	'�����': 'bang',
	
	'hiya': 'hiya',
	'�����': 'hiya',
	
	'lol': 'lol',
	'���': 'lol',
	
	'gipno': 'gipno',
	'������': 'gipno',
	
	'q': 'q',
	'��': 'q',
	
	'zub': 'zub',
	'���': 'zub',
	
	'newyear': 'newyear',
	'��': 'newyear',
	
	'moroz': 'moroz',
	'��������': 'moroz',
	
	'snowgirl': 'snowgirl',
	'��������': 'snowgirl',
	
	'medic': 'medic',
	'�����': 'medic',
	
	'angel2': 'angel2',
	'�����2': 'angel2',
	
	'archer': 'archer',
	'������': 'archer',
	
	'ass1': 'ass1',
	'����1': 'ass1',
	
	'ass2': 'ass2',
	'����2': 'ass2',
	
	'candleman': 'candleman',
	'�����': 'candleman',
	
	'compkiller': 'compkiller',
	'��������': 'compkiller',
	
	'flytryin': 'flytryin',
	'������': 'flytryin',
	
	'gimme5': 'gimme5',
	'���5': 'gimme5',
	
	'horseman': 'horseman',
	'�������': 'horseman',
	
	'loveposter': 'loveposter',
	'������': 'loveposter',
	
	'sexover': 'sexover',
	'����': 'sexover',
	
	'skripka': 'skripka',
	'�������': 'skripka',
	
	'waitlover':'waitlover',
	'���':'waitlover',
	
	'writer': 'writer',
	'��������': 'writer',
	
	'victory': 'victory',
	'������': 'victory',
	
	'air_kiss': 'air_kiss',
	'����2': 'air_kiss',
	
	'king': 'king',
	'������': 'king',
	
	'sv3': 'sv3',
	'����������': 'sv3'
};

var smiliesTarget, smiliesTargetMaxLength;

/**
 * �������������� ������� �� ����������
 * 
 * @param {Object} targetWindow
 */
function initSmilies(targetWindow) {
	var $ = jQuery;
	
	if(!targetWindow) {
		targetWindow = window;
	}
		
	smiliesTabs = $('#smilies-tabs');	
	
	
	if(!smiliesTabs.data('tabs-init')) {
		var single = smiliesTabs.find('#single-smilies').empty();
		var group = smiliesTabs.find('#group-smilies').empty();
		
		for(var name in smilies) {
			var smiley = smilies[name];
			var img = $('<img />', {
				alt: ':' + name + ':',
				click: function() {
					if(!targetWindow.smiliesTarget.disabled) {
						if (targetWindow.smiliesTargetMaxLength && targetWindow.smiliesTarget.value.length + this.alt.length + 1 > targetWindow.smiliesTargetMaxLength) {
							alert("����� ��������� ������������ ����� ��������!");
						} else {
							targetWindow.smiliesTarget.value += this.alt;
														
							targetWindow.smiliesTarget.focus();
							moveCaretToEnd(targetWindow.smiliesTarget);
							
							var smiliesDialog = $('#smilies-dialog');
							
							if (smiliesDialog.length) {
								smiliesDialog.dialog('close');
							} else {
								window.close();
							}
						}
					}
				},
				height: smiley.height + 'px',
				mouseenter:	function() {
					$(this).addClass('hover');
				},
				mouseleave:	function() {
					$(this).removeClass('hover');
				},
				src: imgURL + 'sm/' + name + '.gif',
				title: ':' + smilies[name].codes.join(':  :') + ':',
				width: smiley.width + 'px'
			});
			
			if(smilies[name].group == 1) {
				single.append(img);
			} else if(smilies[name].group == 2) {
				group.append(img);
			}
		}
		
		smiliesTabs.tabs().data('tabs-init', true).show();
	}
}

/**
 * �������������� ���� �� ����������
 * 
 * @param buttons	�������� ������ ��� ������ ���� ���������
 * @param target	�������� ��������, ���� ����������� ��������
 * @param maxLength	������������ ����� �������� � ������
 */
function initSmiliesDialog(buttons, target, maxLength) {
	var $ = jQuery;
	
	var smiliesDialog = $('#smilies-dialog');
	
	if(smiliesDialog.length) {
		smiliesDialog.dialog({
			autoOpen: false,
			height: 'auto',
			minHeight: 0,
			width: 400
		});
		
		$(buttons).click(function() {
			smiliesTarget = $(target)[0];
			smiliesTargetMaxLength = maxLength;
				
			if ($(window).height() > 520 && $(window).width() > 404) {
				smiliesDialog.dialog('open');
			} else {
				var left = Math.floor(screen.width / 2 - 400 / 2); 
				var top = Math.floor(screen.height / 2 - 500 / 2);
				window.open('/smilies.html', '_blank', 'height=500, left=' + left + ', location=no, resizable=no, top=' + top + ', width=400');
			}
		});
	}
}

/**
 * ������������ ���� ��������� � �������� � ������������ �� ����� ���������
 * 
 * @param {String} str		������ ��� ���������������
 * @param {Integer} limit 	����������� �� ����� �������������� �������
 * @return {String} 		���������� ����������������� ������
 */
function smiley2img(str, limit) {
	if (limit == undefined) {
		limit = 3;
	}
	
	if (limit !== 0) {
		var arr = str.match(/:[a-z�-�0-9_]+:/g);
		
		if (arr) {
			var i = 0;
			var counter = 0;
			
			while (counter < limit && i < arr.length) {
				var code = arr[i].replace(/:/g, '');
				
				if (smileyCodes[code]) {
					code = smileyCodes[code];
					str = str.replace(arr[i], '<img alt="&#058;' + code + '&#058;" class="smiley" height="' + smilies[code].height + '" src="' + imgURL + 'sm/' + code + '.gif" title="&#058;' + smilies[code].codes.join('&#058;  &#058;') + '&#058;" width="' + smilies[code].width + '" />');
					counter++;
				}
				
				i++;
			}
		}
	}
	
	return str;
}

/* end: Smilies */

/**
 * ������� ���� �� ��� ��������� ����
 * 
 * @param nick			������ ����
 * @param compatibility	�������� �������� ������������� � ���������� �������
 * @return 				true, ���� ������� ���� �������, false, ���� �� ����� ������� �� ����
 */
function pasteNick(nick, compatibility) {
	var fields = jQuery('.paste-nick:visible');
	
	if(jQuery('#win1').css('visibility') == 'hidden') {
		fields = fields.filter(':not(#win1 .paste-nick);');
	}
	
	if(fields.length) {
		fields.removeClass('placeholder').val(nick);
		var last = fields.filter(':last');
		last.focus();
		
		if (jQuery.browser.msie) {
			moveCaretToEnd(last[0]);
		}
		
		return compatibility ? false : true;
	} else {
		return compatibility ? true : false;
	}
}

function pnotify(message, error, title) {
	if(message) {
		var notify = $.pnotify({
			pnotify_title: title ? title : '��������!',
			pnotify_type: error ? 'error' : 'notice',
			pnotify_text: message //json.error ? json.error : json.message
		});
		notify.appendTo('#notifies').click(notify.pnotify_remove);
	}
}

/**
 * ���������� ������� � ����� ������
 * 
 * @param {Object} inputObject	������-������
 */
function moveCaretToEnd(inputObject) {
    if (inputObject.createTextRange) {
        var r = inputObject.createTextRange();
        r.collapse(false);
        r.select();
    }
    else if (typeof inputObject.selectionStart != 'undefined') {
        var end = inputObject.value.length;
        inputObject.setSelectionRange(end, end);
        inputObject.focus();
    }
}

/**
 * �������� ��������� � ������
 * 
 * @param {String} name		��� ���������
 */
function addToPrivate(name) {
	var privateText = 'private [' + name + ']';
			
	if (top.frames['down']) {
		var text = top.frames['down'].document.forms[0].text;
	} else if(window.opener && window.opener.top.frames['down']) {
		var text = window.opener.top.frames['down'].document.forms[0].text;
	}
	
	var toText = 'to [' + name + ']';
	while (text.value.indexOf(toText) !== -1) {
		text.value = text.value.replace(toText, privateText);
	}
	
	if (text.value.indexOf(privateText) == -1) {
		text.value = privateText + ' ' + text.value;
	}
	
	text.focus();
}

function mentorDealDialog(mentor,deal,nd) {
	var dialog = getDialog();
	
	var html = '' +
		'<form action="transfer.pl?' + Math.random() + '" method="post">' +			
			'<a href="/inf.pl?user=' + mentor + '" target="_blank">' +
				'<b>'+mentor+'</b>' +
			'</a> ' +
			'���������� ��� ����� ����� �����������.<br />' +
			'��������� ��������� ������� ��� ����������� � ���������� ����.<br />' +
			
			'<div style="margin-top: .5em; text-align: center;">' +
				'<input class="button" type="submit" value="������� � ����������" /> ' +			
			'</div>' +
		'</form>';
	
	dialog.html(html);
	dialog.dialog('option', 'title', '����������� ����������');
	dialog.dialog('option', 'width', 450);
	dialog.dialog('open');
}

function showAchBig(ach,user_id) {
	var dialog = getDialog();
	
	dialog.dialog('option', 'title', '����������');
	
	dialog.html('<div>���� ��������</div>');
	dialog.dialog('option', 'width', 642);
	dialog.dialog('open');
	
	var params = {
		ach: ach,
		cmd: 'get_ach',
		is_ajax: 1
	};
	if (user_id) params.user_id = user_id;
	else if (top.userId) params.user_id = top.userId;
	
	jQuery.post('common_ajax.pl',params,
		function(data) {
			dialog.html(htmlAchBig(data));
			dialog.dialog('option', 'height', 'auto');
		}
	);
}

function htmlAchBig(data) {
	var value = '';
	value += '<div><table class="table-list" style="width: 600px; margin: 10px;"><tr><td class="img">';
	value += '<img alt="" height="80" src="http://img.carnage.ru/i/achievement/'+data.code+'_f.jpg" width="80" /></td>';
	
	value += '<td><div><b>'+data.title+'</b></div>';
	value += '<div style="margin-left: 10px;">';
	if (data.desc) value += '<div><i>'+data.desc+'</i></div>';
	
	value += '<div>����:</div>';
	value += '<ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">';
	for (var i=0;i<data.aims.length;i++) {
		var aim = data.aims[i];
		var self_finished = false;
		if (data.self && ((data.self.state == "r" && aim.finished == 1) || data.self.state == "f")) self_finished = true;
		value += '<li>';
		if (self_finished) value += '<b>';
		value += aim.text;
		if (self_finished) value += '</b>';
		if (aim.list && aim.list.length) {
			value += '<ul class="ach_aim_list">';
			for (var j=0;j<aim.list.length;j++) {
				value += '<li>';
				if (self_finished || aim.list[j].done) value += '<b>';
				value += aim.list[j].text;
				if (self_finished || aim.list[j].done) value += '</b>';
				value += '</li>';
			}
			value += '</ul>';
		}
		value += '</li>';
	}
	value += '</ul>';
	
	value += '<div>�������:</div>';
	value += '<ul  style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">';
	for (var i=0;i<data.rewards.length;i++) {
		value += '<li>'+data.rewards[i].text+'</li>';
	}
	value += '</ul>';
	
	value += '</div>';
	
	if (data.self) {
		value += '<div>';
		if (data.self.state == "f") value += "���� ��� ���������� �������� "+data.self.datetime;
		else if (data.self.state == "n") value += "���� ��� ���������� �� ��������";
		else if (data.self.state == "r") value += "� ��� ��� ���������� � �������� ����������";
		value += '</div>';
	}
	value += '</td></tr></table></div>';
	
	return value;
}

/**
 * ��������� �� ��������� ��� ���������
 */
var tooltipDefaults = {
	bodyHandler: function() {
		return smiley2img(this.tooltipText);
	},
	showURL: false,
	track: true
}

/**
 * ��������� �� ��������� ��� ������������ ���������
 */
var paginationDefaults = {
	callback: function(event, container) {
		container.prepend('��������:');
	},
	items_per_page: 1,
	next_text: '&rarr;',
	next_show_always: false,
	num_edge_entries: 1,
	prev_show_always: false,
	prev_text: '&larr;'
}

jQuery(function($) {
	try {
		$('div.roll-tabs-js').tabs({
			cookie: {
				expires: 1 / 24
			}
		});
	} catch(e) {
	}

	if($.tooltip) {
		$('.tooltip[title]').tooltip(tooltipDefaults);
		$('.tooltip-next').tooltip($.extend({}, tooltipDefaults, {
			bodyHandler: function(){
				return smiley2img($(this).next().html());
			}
		}));
	}
	
	jQuery('.nick img.private').live('click', function() {
		var name = jQuery(this).parent().children('.name').text();
		var oWindow = window;

		if ( opener ) {
			oWindow = opener;
		}
		
		if (oWindow.top.frames['chat'] && oWindow.top.frames['chat']['chat2'].oChat) {
			oWindow.top.frames['chat']['chat2'].oChat.writeToEditorPrivate(name);
		}
	});
	
	jQuery('.nick .name').live('click.nick', function(event) {
		if (!jQuery(this).closest('.ui-dialog').length) {
			if (event.ctrlKey) {
				window.open('/inf.pl?user=' + jQuery(this).text(), '_blank');
			} else {
				if (top.frames['down']) {
					var name = jQuery(this).text();
					
					if (!pasteNick(name)) {
						if (top.frames['chat'] && top.frames['chat']['chat2'].oChat) {
							top.frames['chat']['chat2'].oChat.writeToEditorPrivate(name);
						}
					}
				}
			}
		}
	});
	
	// ��������� ���������� ����
	addContextMenu();
	
	var rightCol = jQuery('div.right-col-float').css('top', jQuery(window).scrollTop());
	jQuery(window).scroll(function() {		
		rightCol.stop().animate({
			top: jQuery(window).scrollTop()
		}, 100);
	});
});
