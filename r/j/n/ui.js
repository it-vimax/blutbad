jQuery(function(){	
	function hintOther(hash){
		if (hash.rtype == 'parameter') 
			return hash.text + (hash.param2 > 0 ? ' +' : ' ') + hash.param2;
		return '';
	}
	
	function getIEVersion(){
		var ua = navigator.userAgent;
		var offset = ua.indexOf('MSIE ');
		if (offset == -1) {
			return 0;
		} else {
			return parseFloat(ua.substring(offset + 5, ua.indexOf(';', offset)));
		}
	}
	
	jQuery('.hover').tooltip({ 
		track: getIEVersion() > 0 ? false : true, 
		delay: 0, 
		showURL: false, 
		showBody: " - " 
	});
	
	jQuery('#minimap .hand').tooltip({ 
		track: true, 
		delay: 0, 
		showURL: false, 
		showBody: " - ",
		bodyHandler: function(){
			return jQuery('.'+jQuery(this).attr('id')).html(); 
		}
	});
	
	jQuery('.hintItem_hover').tooltip({ 
		track: true, 
		delay: 0, 
		showURL: false, 
		showBody: " - ",
		bodyHandler: function(){ return hintItem($(this).next().html()); }
	});
	
	jQuery('.hintOther_hover').tooltip({ 
		track: true, 
		delay: 0, 
		showURL: false, 
		showBody: " - ",
		bodyHandler: function(){ return hintOther($(this).next().html()); }
	});
});