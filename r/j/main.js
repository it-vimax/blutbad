var colorpicker;

function input_color_focus(){
	var color = $('#userbar_color').attr('value');
	if (!color || !RegExp('^\#[a-f0-9]{6}$').test(color)) 
		$('#userbar_color').attr('value', '');
}

function input_color_blur(){
	var color = $('#userbar_color').attr('value');
	if (!color || !RegExp('^\#[a-f0-9]{6}$').test(color)) 
		$('#userbar_color').attr('value', '<�� ���������>');
}

function toggleUserbar() {
	var userbar = $('#form-userbar');
	
	if(!userbar.is(':visible')) {
		$.cookie('form-userbar', 1, { expires: 365 });
	} else {
		$.cookie('form-userbar', null);
	}
	
	userbar.toggle();
	
	return false;
}

$(function() {
 

	$('#shout-timeout .elapsed-time').removeClass('invisible').each(function(){
		$(this).countdown({
			format: 'yodhms',
			layout: '{y<}{yn} {yl} {y>}{o<}{on} {ol} {o>}{d<}{dn} {dl} {d>}{h<}{hn} {hl} {h>}{m<}{mn} {ml} {m>}{sn} {sl}',
			onExpiry: function() {
				$('li.shout').removeClass('invisible');
				$('#shout-timeout').remove();
			},
			until: $(this).text()
		})
	});
	
	if ($('#colorpicker, #userbar_color').length == 2) {
		colorpicker = $.farbtastic('#colorpicker', '#userbar_color');
	}
	
	$('#shout').charCounter(null, {
		container: $('#shout-charcounter')
	});
	
	$('#shout-cities').charCounter(null, {
		container: $('#shout-cities-charcounter')
	});
	
	$('#autoanswer').charCounter(null, {
		container: $('#autoanswer-charcounter')
	});
	
	if($.cookie('form-userbar')) {
		$('#form-userbar').removeClass('invisible');
	}
});