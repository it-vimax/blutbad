<?
function code_to_script($document){
  $document = preg_replace("/\r\n|\r|\n/",'<br>',$document);
 return $document;
}
?>
<!DOCTYPE html>
<html><head>
  <meta content="text/html; charset=utf8" http-equiv="Content-type">
  <meta name="description" content="Blutbad Games - ��� ��� ����">
  <meta name="keywords" content="����, ������, �������, �������, android, ��������, �����, ����, �����, ����, �����, 2048, ���, �����, ������, �����������, iq, ���������">
  <meta name="author" content="Blutbad Games">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="./css/reset.css" type="text/css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="./css/style.css" type="text/css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="./css/fonts.css" type="text/css" media="screen" charset="utf-8">
    <meta name="google-site-verification" content="Wr2NcX0HO73jz_IczgYfBJpBVeVjCGYI3MBroF2VuFo" />
    <title>Blutbad Games</title>
    <!--[if gte IE 7]><link href="css/ie.css" rel="stylesheet" type="text/css" /><![endif]-->
    <style>
      /* middle */
      #mid {
        background: url(../img/mid_bg.jpg) no-repeat 50% 0px;
        min-height: 452px !important;
        height: auto !important;
        overflow: hidden;
      }
      #mid p {
        line-height: 20px;
        padding: 0px 0px 19px 0px;
      }
      #mid b {
        font-weight: bold;
      }
      #about {
        padding: 84px 190px 0px 52px;
      }
      #contacts {
        padding: 84px 52px 50px 52px;
      }
      #contacts h1,
      #about h1 {
        width: 226px;
        float: left;
        padding: 0px 0px 0px 0px;
      }
      /*#contacts p,*/
      #about p {
        padding-left: 260px;
      }
      #contacts {
        /*background: url(../img/pic-contacts.jpg) no-repeat 713px 90px;*/
        min-height: 133px;
        background: none !important;
      }
      .fon_contacts {
        position: absolute;
        top: 60px; /*90px*/
        right: 122px;
      }
      #contacts .contacts_block {
        /*width: 626px;
        padding-left: 260px;*/
      }


      .contacts_item {
       padding: 0px 0px 25px 0px;
      }
      .contacts_item .contacts_foto {
        float: left;
       /* margin-left: 80px;*/
        margin-top: 5px;
		border-radius:5px;
      }
      .contacts_item h2 {
        font-family: 'Tahoma', helvetica, sans-serif;
        /*font-family: 'metabookc';*/
        font-size: 23px;
        letter-spacing: 1px;
        text-transform: uppercase;
        color: #4c4c4c;
        padding-bottom: 12px;
        padding-left: 150px;
      }
      #contacts .contacts_item p {
        padding-left: 150px;
        /*padding-right: 100px;*/
      }
    </style>
 </head>
 <body>
   <!-- RUSSIAN -->
   <div id="container" class="loc_rus">

       <!-- TOP -->
       <div id="top">
         <div class="top">
           <a href="http://games.blutbad.ru/index.php"><img class="logo" src="css/img/logo.png" alt="blutbadgames" title="blutbadgames"></a>
           <div class="menu">
             <ul class="tabs">
               <li class="sel"><div><a href="#projects">���� �������</a></div></li>
               <li class=""><div><a href="#about">� ��������</a></div></li>
               <li class=""><div><a href="#contacts">��������</a></div></li>
             </ul>
           </div>
           <div class="lang">
             <a href="http://games.blutbad.ru/index.php"><img class="lang_rus" src="0.gif" alt="�������" title="�������"></a>
             <div class="lang_box">
               <a href="http://games.blutbad.ru/index.php"><img class="lang_rus" src="0.gif" alt="�������" title="�������"></a>
               <a href="http://games.blutbad.ru/index.php?cmd=en"><img class="lang_eng" src="0.gif" alt="English" title="English"></a>
             </div>
           </div>
         </div>
       </div>

       <!-- MIDDLE -->
       <div id="mid">
         <div class="mid">

          <div id="projects" class="easytabs-tab-content" style="display: block;">

            <div id="carousel" class="carousel products" style="display: block;">
            <h1>���� �������</h1>

              <img src="0.gif" alt=" " class="prev">
              <img src="0.gif" alt=" " class="next">
              <div class="jCarouselLite" style="visibility: visible; overflow: hidden; position: relative; z-index: 2; left: 0px; width: 896px;">
                <ul style="margin: 0px; padding: 0px; position: relative; list-style-type: none; z-index: 1; width: 2688px; left: -1568px;">
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbad" class="info"><img src="fot/blutbad-car.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>Blutbad</h2>
                      <p>�&nbsp;������ ������� ������ ��������</p>
                      <a href="http://www.blutbad.ru/" target="_blank">www.blutbad.ru</a>
                    </div>
                    <a href="#blutbad" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadfillwords" class="info"><img src="fot/blutbad.fillwords.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>�������� - ����� ����</h2>
                      <p>�&nbsp;�������� - ������������� ���� ��� ����� ���������!</p>
                    </div>
                    <a href="#blutbadfillwords" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadbalda" class="info"><img src="fot/blutbad.balda.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>����� - ���� � ����� (Free)</h2>
                      <p>�&nbsp;���������� ���� ����� ������� ������ ��� �� �����.</p>
                    </div>
                    <a href="#blutbadbalda" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadfindword" class="info"><img src="fot/blutbad.findword.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>����� ����</h2>
                      <p>�&nbsp;��� ��������� ����-����������� ������ ���.</p>
                    </div>
                    <a href="#blutbadfindword" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadline_dots" class="info"><img src="fot/blutbad.line_dots.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>��������� �����  (Flow Free)</h2>
                      <p>�&nbsp;��������� ����� �������� �������, �� ������������� ������������.</p>
                    </div>
                    <a href="#blutbadline_dots" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadtangram" class="info"><img src="fot/blutbad.tangram.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>������� HD Free</h2>
                      <p>�&nbsp;������� ��� �������� � �����</p>
                    </div>
                    <a href="#blutbadtangram" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadflappyhero" class="info"><img src="fot/blutbad.flappyhero.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>�������� �����</h2>
                      <p>�&nbsp;������ ��� ���� ����� ����� �������� ������</p>
                    </div>
                    <a href="#blutbadflappyhero" class="more info">������ ������</a>
                  </li>

                 </ul>
              </div>

              <div class="jCarouselLite" style="padding-top: 15px;visibility: visible; overflow: hidden; position: relative; z-index: 2; left: 0px; width: 896px;">
                <ul style="margin: 0px; padding: 0px; position: relative; list-style-type: none; z-index: 1; width: 2688px; left: -1568px;">
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadx2048" class="info"><img src="fot/blutbad.x2048.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>2048 - ������ �����������</h2>
                      <p>�&nbsp;�������� ���������� ��� ������ �����!</p>
                    </div>
                    <a href="#blutbadx2048" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadpresslines" class="info"><img src="fot/blutbad.presslines.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>��� ����� (��� �������)</h2>
                      <p>�&nbsp;������������� ���� �� �������� � �������� �������.</p>
                    </div>
                    <a href="#blutbadpresslines" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadmatchespuzzles" class="info"><img src="fot/blutbad.matchespuzzles.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>������ � �����������</h2>
                      <p>�&nbsp;������ ������ ����������� �� ��������.</p>
                    </div>
                    <a href="#blutbadmatchespuzzles" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadiqquiz" class="info"><img src="fot/blutbad.iqquiz.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>IQ ���������</h2>
                      <p>�&nbsp;������� �� �������, ������� ����. ������� ���� ������� ����������.</p>
                    </div>
                    <a href="#blutbadiqquiz" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadbubbleshooter" class="info"><img src="fot/blutbad.bubbleshooter.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>�����������</h2>
                      <p>�&nbsp;������������� ����� 600 �������� ���������. ��� ����� ������ � �������!</p>
                    </div>
                    <a href="#blutbadbubbleshooter" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadwhere_logic" class="info"><img src="fot/blutbad.where_logic.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>����� ������</h2>
                      <p>�&nbsp;��������� ����������������</p>
                    </div>
                    <a href="#blutbadwordmaster" class="more info">������ ������</a>
                  </li>
                  <li style="overflow: hidden; float: left; width: 207px; height: 336px;text-align: center;">
                    <!-- img -->
                    <a href="#blutbadwordmaster" class="info"><img src="fot/blutbad.wordmaster.png" alt=" "></a>
                    <!-- descr -->
                    <div class="descr">
                      <h2>Word Master</h2>
                      <p>�&nbsp;������������ ���� ��� ��������� ������ Word Master</p>
                    </div>
                    <a href="#blutbadwordmaster" class="more info">������ ������</a>
                  </li>

                 </ul>
              </div>
            </div>

 <!-- �������� �������� -->

              <?
              include("description/blutbad.php");
              include("description/blutbad.fillwords.php");
              include("description/blutbad.balda.php");
              include("description/blutbad.findword.php");
              include("description/blutbad.line_dots.php");
              include("description/blutbad.tangram.php");
              include("description/blutbad.flappyhero.php");

              include("description/blutbad.x2048.php");
              include("description/blutbad.presslines.php");
              include("description/blutbad.matchespuzzles.php");
              include("description/blutbad.iqquiz.php");
              include("description/blutbad.bubbleshooter.php");
              include("description/blutbad.where_logic.php");
              include("description/blutbad.wordmaster.php");
              ?>

 <!-- /�������� �������� -->

          </div>

          <div id="about" class="easytabs-tab-content" style="display: none;">
            <h1>� ��������</h1>
            <p><b>Blutbad Games</b>&nbsp;� ��������, ������� ���������� ����������� �&nbsp;�������� ���������� ������-���, ���������� ��� �������� �&nbsp;��� ��� ��������� ���������. �������� ���������� ��&nbsp;���������� ����� �&nbsp;2013&nbsp;����. </p>
            <p>�������� ������������ �������� ��&nbsp;������ ������� ��&nbsp;������� ��������������, ������������ ���� ���� �&nbsp;��&nbsp;��������. �&nbsp;��� ������� ���������, ������� ������� ����������� 27&nbsp;���. �&nbsp;���� ���� ������ ��&nbsp;����� ����. </p>
          </div>

          <div id="contacts" class="easytabs-tab-content" style="display: none;">

            <!--<h1>��������</h1>-->
            <!--<img class="fon_contacts" src="img/pic-contacts.jpg" alt=" " />
            <p><b>��������������:</b><br />
            ��� ��������� �������������� ���������� �&nbsp;�������� <br />�������������� ������� <a href="mailto:info@games.blutbad.ru">info@games.blutbad.ru</a>.</p>
            <p><b>���������������:</b><br />
            ���� ��&nbsp;������ �������� �&nbsp;�������� <b>blutbadgames</b>, <br />��������� ��� ������ <a href="mailto:hr@games.blutbad.ru">hr@games.blutbad.ru</a>.</p>-->
<div class="contacts_block">

  <div class="contacts_item">

    ���������� ����������: <a href="mailto:info@games.blutbad.ru">support@blutbad.ru</a>

  </div>

  <div class="break"></div>

</div>


          </div>

         </div>
       </div>

       <!-- FOOTER -->
       <div id="foot">
         <div class="foot">
          <div class="l">�&nbsp;<nobr>2013�2017</nobr> Blutbad Games. ��� ����� ��������</div>
          <div class="r">���������� ����������: <a href="mailto:info@games.blutbad.ru">support@blutbad.ru</a></div>
         </div>
       </div>

   </div>

     <script src="jquery.min.js" type="text/javascript" charset="utf-8"></script>
     <script src="easytabs.js" type="text/javascript" charset="utf-8"></script>
     <script src="showdescr.js" type="text/javascript" charset="utf-8"></script>
     <script src="jcarousellite_1.0.1.min.js" type="text/javascript" charset="utf-8"></script>
     <script src="jquery.mousewheel-3.0.4.pack.js" type="text/javascript" charset="utf-8"></script>
     <script src="jquery.ba-hashchange.min.js" type="text/javascript" charset="utf-8"></script>
      <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
          $('#container').easyTabs({defaultContent:1});
          $('#projects').showDescr();
          //$(".products .prev").addClass("disabled");
          window.onhashchange = function(){
            var defaultDiv= $("#projects");
            defaultDiv.hide();

            if (document.location.hash.replace("#", "") == "") {
              defaultDiv.show();
            } else {
                var child = document.location.hash;
                var $parentId = $(child).parent().attr("id");
                if ($parentId) {
                  $("a[href='#"+$parentId+"']").trigger("click");
                }
                $("a[href='"+child+"']").trigger("click");
            }
          }

          window.onhashchange();
        });

        $(".jCarouselLite").jCarouselLite({
            btnNext: ".products .next",
            btnPrev: ".products .prev",
            visible: 4,
            //mouseWheel: true,
            circular: true
        });
      </script>



</body></html>