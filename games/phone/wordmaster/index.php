<?

 if (!substr_count($_SERVER['HTTP_USER_AGENT'], 'facebook') && !substr_count($_SERVER['HTTP_USER_AGENT'], 'vkShare')) {
  header("Location: http://games.blutbad.ru/#blutbadwordmaster");
 }
 // file_put_contents("dd.txt", serialize($_SERVER), LOCK_EX);
?>
<html>
<head>
<meta name="robots" content="index, follow" />
<meta name="application-name" content="Word Master" />
<? if ($_GET['lang'] == 'ru') { ?>
<meta name="title" content="Word Master - ����� ���������� ����� " />
<meta name="description" content="������� ����� ���������� Word Master � ����� �����, ���������� � ������ ������! " />
<meta name="keywords" content="Word, Master, WordMaster, blutbad, ����������, ����������, ���������, iOS, Android, iPhone, iPad, ���� " />
<meta property="og:title" content="Word Master - ����� ���������� ����� " />
<meta property="og:description" content="������� ����� ���������� Word Master � ����� �����, ���������� � ������ ������! " />
<meta property="og:url" content="http://games.blutbad.ru/phone/wordmaster/" />
<meta property="og:image" content="http://games.blutbad.ru/phone/wordmaster/partage_general_ru.png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="403" />
<meta property="og:image:type" content="image/png" />
<meta property="fb:app_id" content="108790056474207" />
<meta property="og:type" content="website" />
<meta name="twitter:image" content="http://games.blutbad.ru/phone/wordmaster/partage_general_ru.png" />
<meta name="twitter:card" content="app" />
<meta name="twitter:site" content="@WordMaster_App" />
<meta name="twitter:description" content="������ Word Master ��������� �� App Store � Google Play " />
<meta name="twitter:app:name:iphone" content="Word Master" />
<meta name="twitter:app:id:iphone" content="000" />
<meta name="twitter:app:url:iphone" content="wordmaster://" />
<meta name="twitter:app:name:ipad" content="Word Master" />
<meta name="twitter:app:id:ipad" content="000" />
<meta name="twitter:app:url:ipad" content="wordmaster://" />
<meta name="twitter:app:name:googleplay" content="Word Master" />
<meta name="twitter:app:id:googleplay" content="blutbad.wordmaster" />
<meta name="twitter:app:url:googleplay" content="wordMaster://app" />
<meta name="apple-itunes-app" content="app-id=000" />
<meta name="google-play-app" content="app-id=blutbad.wordmaster" />
<meta name="theme-color" content="#5FC2EA" />
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

<? } else { ?>

<meta name="title" content="Word Master - Find the hidden words!">
<meta name="description" content="Download the new Word Master app to find the hidden words in hundreds of grids!">
<meta name="keywords" content="Word, Master, WordMaster, blutbad, apps, application, free, iOS, Android, iPhone, iPad, game">
<meta property="og:title" content="Word Master - Find the hidden words!">
<meta property="og:description" content="Download the new Word Master app to find the hidden words in hundreds of grids!">
<meta property="og:url" content="http://games.blutbad.ru/phone/wordmaster/">
<meta property="og:image" content="http://games.blutbad.ru/phone/wordmaster/partage_general_en.png">
<meta property="og:image:width" content="600">
<meta property="og:image:height" content="403">
<meta property="og:image:type" content="image/png">
<meta property="fb:app_id" content="108790056474207">
<meta property="og:type" content="website">
<meta name="twitter:image" content="http://games.blutbad.ru/phone/wordmaster/partage_general_en.png">
<meta name="twitter:card" content="app">
<meta name="twitter:site" content="@WordMaster_App">
<meta name="twitter:description" content="Download Word Master for free on The App Store and Google Play">
<meta name="twitter:app:name:iphone" content="Word Master">
<meta name="twitter:app:id:iphone" content="000">
<meta name="twitter:app:url:iphone" content="wordmaster://">
<meta name="twitter:app:name:ipad" content="Word Master">
<meta name="twitter:app:id:ipad" content="000">
<meta name="twitter:app:url:ipad" content="wordmaster://">
<meta name="twitter:app:name:googleplay" content="Word Master">
<meta name="twitter:app:id:googleplay" content="blutbad.wordmaster">
<meta name="twitter:app:url:googleplay" content="wordmaster://app">
<meta name="apple-itunes-app" content="app-id=000">
<meta name="google-play-app" content="app-id=blutbad.wordmaster">
<meta name="theme-color" content="#5FC2EA">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">

<? } ?>

</head>
</html>