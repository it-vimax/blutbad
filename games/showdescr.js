﻿(function($) {

    $.fn.showDescr = function(option) {
        var param = jQuery.extend({
            fadeSpeed: "fast"
        }, option);
        $(this).each(function() {
            var thisId = "#" + this.id;

            // Объединяем все описания в массив и скрываем их
            $(thisId + " .carousel li a.info").each(function() {
                var tabToHide = $(this).attr('href').substr(1);
                $("#" + tabToHide).addClass('descr_content');
            });
            hideAll();

            //changeContent(defaultTab);
            function hideAll() {
                $(thisId + " .descr_content").hide();
            }

            function changeContent(tabId) {
                hideAll();
                $(thisId + " .carousel").hide();
                if (param.fadeSpeed != "none") {
                    $(thisId + " #" + tabId).fadeIn(param.fadeSpeed);
                } else {
                    $(thisId + " #" + tabId).show();
                }
            }

            // Открываем описание
            $(thisId + " .carousel a.info").click(function() {
                var tabId = $(this).attr('href').substr(1);
                changeContent(tabId);
            });
            // Возвращаемся к карусели
            $(thisId + " a.back").click(function() {
                hideAll();
                $(thisId + " .carousel").show();
                return false;
            });


        });
    }

})(jQuery);