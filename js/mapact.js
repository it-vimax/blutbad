


function sol(room){
	if (top.CtrlPress) {
		window.open('/online.html?room=' + top.currentCity + '_' + room + '&' + Math.random(), 'onlines', 'width=300,height=500,toolbar=no,location=no,scrollbars=yes,resizable=yes');
	} else {
		window.location.href = 'map.php?got=1&map=' + room + '&' + Math.random();
	}
}

function confirm_fee(room, fee){
	if (top.CtrlPress) {
		window.open('/online.html?room=' + top.currentCity + '_' + room + '&' + Math.random(), 'onlines', 'width=300,height=500,toolbar=no,location=no,scrollbars=yes,resizable=yes');

		return false;
	}

	var value = 0;
	var currency = '';
	var oRegExp = new RegExp("^([0-9\.]+)(\\w)(\\w?)$");

	if (oRegExp.exec(fee)) {
		value = RegExp.$1;
		currency = RegExp.$2;

		var userLevel = 0;

		if (typeof(user_level) != 'undefined' && user_level) {
			userLevel = user_level;
		} else if (typeof(user) != 'undefined' && user.level) {
			userLevel = user.level;
		}

		if (RegExp.$3 == 'l' && userLevel) {
			value = user_level * Math.round(100 * value) / 100;
		}

	} else {
		return sol(room);
	}

	if (currency == 's') {
		currency = '���.'
	} else {
		currency = '��.'
	}

	if (confirm('��������� ���� ������� ����� ' + value + ' ' + currency + '\n��������?')) {
		return sol(room);
	} else {
		return false;
	}
}
