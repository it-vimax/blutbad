/**
 *
 * jquery.charcounter.js version 1.2
 * requires jQuery version 1.2 or higher
 * Copyright (c) 2007 Tom Deater (http://www.tomdeater.com)
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 * 
 */
 
(function($) {
	/**
	 * attaches a character counter to each textarea element in the jQuery object
	 * usage: $("#myTextArea").charCounter(max, settings);
	 */
	
	$.fn.charCounter = function (max, settings) {
		max = max || 100;
		settings = $.extend({
			container: '<span></span>',
			classname: "charcounter",
			format: '������������ ��������: <span class="counter">%1</span> �� %2'
		}, settings);
		
		function count(el, container) {
			el = $(el);
			var maxLength = el.attr('maxlength');
			var elMax = maxLength && maxLength > 0 ? maxLength : max;
			if (el.val().length > elMax) {
			    el.val(el.val().substring(0, elMax));
			};
			
			var html = settings.format.replace(/%1/, el.val().length);
			html = html.replace(/%2/, elMax);
			container.html(html);
		};
		
		return this.each(function () {
			var container;
			if(typeof settings.container == 'object') {
				container = $(settings.container);
			} else if (!settings.container.match(/^<.+>$/)) {
				// use existing element to hold counter message
				container = $(settings.container);
			} else {
				// append element to hold counter message (clean up old element first)
				$(this).next("." + settings.classname).remove();
				container = $(settings.container)
								.insertAfter(this)
								.addClass(settings.classname);
			}
			$(this)
				.unbind(".charCounter")
				.bind("keydown.charCounter keypress.charCounter keyup.charCounter", function () { count(this, container); })
				.bind("focus.charCounter", function () { count(this, container); })
				.bind("mouseover.charCounter mouseout.charCounter", function () { count(this, container); })
				.bind("change.charCounter", function () { count(this, container); })
				.bind("paste.charCounter", function () { 
					var me = this;
					setTimeout(function () { 
						count(me, container); 
					}, 10);
				});
				
			if (this.addEventListener) {
				this.addEventListener('input', function () { count(this, container); }, false);
			};
			
			count(this, container);
		});
	};

})(jQuery);