<?
  include("../bd_connected.php");

  header('Content-type: application/json');
  header('Content-Type: text/html; charset=utf-8');

  function CP1251_to_UTF8($text = '', $revers = true){
   return $revers?@iconv('CP1251', 'UTF-8', $text):@iconv('UTF-8', 'CP1251', $text);
  }

  function f_format_string_date($date_str, $parm = array()) {
    if ($date_str == '' || empty($date_str)) return '';

    $date       = date('d.m.Y', $date_str);
    $date_month = date('d.m', $date_str);
    $date_year  = date('Y', $date_str);
    $date_time  = date('H:i', $date_str);

    $ndate_exp = explode('.', $date);
    $nmonth = array(1 => '������', 2 => '�������', 3 => '�����', 4 => '������', 5 => '���', 6 => '����', 7 => '����', 8 => '�������', 9 => '��������', 10 => '�������', 11 => '������', 12 => '�������');

     foreach ($nmonth as $key => $value) { if($key == intval($ndate_exp[1])) $nmonth_name = $value; }

     if (isset($parm['future'])) {
            if (($date_str - 3 * 60) <= time()){ $datetime = '������ ���'; }
       else if (($date_str - 59 * 60) <= time()){ $datetime = floor((time() - $date_str) / 60).' ���. ���'; }
       else if ($date == date('d.m.Y')){ $datetime = 'C������ � ' .$date_time; }
       else if ($date == date('d.m.Y', strtotime('+1 day'))) {$datetime = '������ � ' .$date_time;}
       else if ($date != date('d.m.Y') && $date_year != date('Y')){ $datetime = $ndate_exp[0].' '.$nmonth_name.' '.$ndate_exp[2]. ' � '.$date_time; }
       else { $datetime = $ndate_exp[0].' '.$nmonth_name.' � '.$date_time; }
     } else {
            if (($date_str + 3 * 60) >= time()){ $datetime = '������ ���'; }
       else if (($date_str + 59 * 60) >= time()){ $datetime = floor((time() - $date_str) / 60).' ���. �����'; }
       else if ($date == date('d.m.Y')){ $datetime = 'C������ � ' .$date_time; }
       else if ($date == date('d.m.Y', strtotime('-1 day'))) {$datetime = '����� � ' .$date_time;}
       else if ($date != date('d.m.Y') && $date_year != date('Y')){ $datetime = $ndate_exp[0].' '.$nmonth_name.' '.$ndate_exp[2]. ' � '.$date_time; }
       else { $datetime = $ndate_exp[0].' '.$nmonth_name.' � '.$date_time; }
     }

   return $datetime;
  }

/***------------------------------------
 * �������� �� ����� ������ ���������
 **/

function send_user_letter($id, $from, $subject, $text, $attaches = array()){
 $time_send = time()+2592000;

 sql_query("INSERT INTO `telegraph` (`owner`, `from`, `subject`, `text`, `attaches`, `read_letter`, `datesend`) values ('".$id."', '".$from."', '".$subject."', '".$text."', '".serialize($attaches)."', 1, '".$time_send."');");
}

    $login = CP1251_to_UTF8($_POST['login']?$_POST['login']:$_GET['login'], false);
    $pass  = CP1251_to_UTF8($_POST['pass']?$_POST['pass']:$_GET['pass'], false);
    $user_id  = CP1251_to_UTF8($_POST['user_id']?$_POST['user_id']:$_GET['user_id'], false);
    $user_hach  = CP1251_to_UTF8($_POST['user_hach']?$_POST['user_hach']:$_GET['user_hach'], false);

  if ($login == 'ww') $login = '�����';


  /***------------------------------------------
   *��������� ����� ���������
   **/

  if ($_POST['cmd'] == 'sendpost') {
    header("Content-Type: text/plain; charset=UTF-8");
    $post_from = CP1251_to_UTF8($_POST['to'], false); # �� ����
    $post_to = CP1251_to_UTF8($_POST['to'], false);
    $post_subject = CP1251_to_UTF8($_POST['subject'], false);
    $post_text = CP1251_to_UTF8($_POST['text'], false);


     $posts_user = sql_row("SELECT `id` FROM `users` WHERE `login` = '".$post_to."' LIMIT 1;");

     if (empty($posts_user['id']) || empty($post_from) || empty($post_text)) {
       die('{"data":[{"status":"error"}]}');
     } else {
       $post_text .= "\n\n--\n���������� � Android Phone";

       send_user_letter($posts_user['id'], '['.$post_from.']', $post_subject, $post_text);
       die('{"data":[{"status":"ok"}]}');
     }
   die('{"data":[{"status":"error"}]}');
  }

  /***------------------------------------------
   * �������� ������ ����� ���������
   **/

  if (isset($login)) {
     $login = CP1251_to_UTF8($login, false);
     $posts_user = sql_row("SELECT `id` FROM `users` WHERE `login` = '".$login."' LIMIT 1;");

     if (empty($posts_user['id'])) {
       die('{"data":[{"status":"error"}]}');
     } else {
       $posts = sql_query("SELECT * FROM `telegraph` WHERE `owner` = '".$posts_user['id']."' AND `read_letter` IN (1, 2, 3) ORDER BY `id` DESC;");

       $list_post = "";
       $list_post_ids = "";
       while ($row = fetch_array($posts)) {

        if (empty($row['subject'])) $row['subject'] = "��� ����";

        $row['subject'] = str_replace("'",'&#039;', $row['subject']);
        $row['text'] = str_replace("'",'&#039;', $row['text']);
        $row['text'] = str_replace("&nbsp;",' ', $row['text']);

       # ������� ��� html ����
         $row['subject'] = strip_tags($row['subject']);
         $row['text'] = strip_tags($row['text']);

         $list_post .= ($list_post?",":"")."{'from':'".CP1251_to_UTF8($row['from'])."', 'subject':'".CP1251_to_UTF8($row['subject'])."', 'text':'".CP1251_to_UTF8($row['text'])."', 'datesend':'".CP1251_to_UTF8(f_format_string_date($row['datesend']))."'}";

         if ($row['read_letter'] == 2) {
             $list_post_ids .= ($list_post_ids?",":"").$row['id'];
         }

       }

       if ($list_post_ids) {
        sql_query("UPDATE `telegraph` SET `read_letter` = 1 WHERE `owner` = '".$posts_user['id']."' AND `read_letter` = 2 AND `id` IN (".$list_post_ids.");");
       }
     }

     //$fr = ", "frends":[{"login":"BR", "level":"10"}, {"login":"dddf", "level":"9"}]";
     $fr = '';
     $sql_fr = sql_query("SELECT `friend_login` FROM `friends` WHERE `user` = '".$posts_user['id']."' AND `friend` > 0 ;");
     while ($row = fetch_array($sql_fr)) {
         $fr .= ($fr?',':'').'{"login":"'.CP1251_to_UTF8($row['friend_login']).'"}';
     }

     if ($list_post) {

       die('{"data":[{"status":"ok"}, '.$list_post.'], "frends":['.$fr.']}');

       /*die(json_encode(array(
                   'data'   => $list_post,
                   'status' => 'ok'
              )));*/

     } else {
       die('{"data":[{"status":"nopost"}], "frends":['.$fr.']}');
     }

  } else {
     die('{"data":[{"status":"error"}]}');
  }

?>