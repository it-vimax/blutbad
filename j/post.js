var oTo = null;
var oSubject = null;
var oBody = null;
var oGiftBody = null;
var oGiftText = null;
var oButtonContacts = null;
var oContacts = null;
var oSmiles = null;
var oButtonKlans = null;
var oKlans = null;
var show_items = new Array();
var oWin = null;
var oToCounter = null;
var oSubjectCounter = null;
var oBodyCounter = null;
var oGiftBodyCounter = null;
var oGiftTextCounter = null;
var oSelectAll = null;

function toComplaint(url, trgt, h, w){
	win = window.open(url, trgt, 'height=' + h + ',width=' + w + ',resizable=no,scrollbars=yes,menubar=no,status=no,toolbar=no');
	win.name = trgt;
	win.focus();
	return win;
}

function addFromList(formName){
	var itemsStr = getItemList(formName);
	
	if (itemsStr) {
		var tempStr = oTo.value;
		tempStr = tempStr.replace(/^\s+/, '');
		
		oTo.value = tempStr ? tempStr + ', ' : '';
		oTo.value += itemsStr;
	}
	
	countTo();
}

function addFromContactList(){
	addFromList('formContacts');
	hideContacts();
}

function gsmileclick(smile){
	o = document.getElementById('contents_smile');
	var tail = (o.value.charAt(o.value.length - 1) != " " ? " " : "") + ' :' + smile + ': ';
	
	if (o.value.length + tail.length <= 100) {
		o.value = o.value + tail;
		//countGiftBody();
		hideSmiles();
		o.focus();
	} else {
		alert("����� ��������� ������������ ����� ��������!");
	}
	
}

function smilenew(ipage){
	var i = 0, sa = ['', '', ''];
	while (i < smiles[0].length) {
		var s = smiles[0][i];
		var x = smiles[1][i];
		var y = smiles[2][i];
		var page = smilespage[i];
		i++;
		sa[page] += '<img src="http://img.carnage.ru/i/sm/' + s + '.gif" width="' + x + '" height="' + y + '" border="0" alt=":' + s + ':" title=":' + s + ':" onclick="gsmileclick(\'' + s + '\');" onMouseOver="this.style.backgroundColor=\'white\';this.style.cursor=\'pointer\'" onMouseOut="this.style.backgroundColor=\'\';this.style.cursor=\'default\'" /> ';
	}
	return sa[ipage];
}

function addFromKlanList(){
	addFromList('formKlans');
	hideKlans();
}

/*function prepareContacts(){
	oButtonContacts = document.getElementById('buttonContacts');
	oContacts = document.getElementById('contacts-dialog');
	oTo = document.getElementById('inputTo');
	
	var baseX = getPosX(oButtonContacts);
	var baseY = getPosY(oButtonContacts);
	
	oContacts.style.left = baseX + 'px';
	oContacts.style.top = (baseY + 20) + 'px';
}*/

function prepareSmiles(){
	oButtonSmiles = document.getElementById('buttonSmiles');
	oSmiles = document.getElementById('smiles-dialog');
}

function showSmiles() {
	if ( ! oSmiles ) {
		prepareSmiles();
	}
	if ( oKlans ) {
		hideKlans();
	}

	if ( oContacts ) {
		hideContacts();
	}
	
	$('smiles-dialog').dialog('open');
	
	var single = smilenew(1);
	var group = smilenew(2);
	
	$("#smiles_single").html(single);
	$("#smiles_group").html(group);
	
	osSingle = document.getElementById( 'smiles_single' );
	
	osGroup = document.getElementById( 'smiles_group' );		
}

function smilesGroupToggle(i)
{
	osSingle = document.getElementById( 'smiles_single' );
	osSingle.style.display = (i==1)?'':'none';
	
	osGroup = document.getElementById( 'smiles_group' );	
	osGroup.style.display = (i==1)?'none':'';
}

function hideSmiles() {
	$('#smiles-dialog').dialog('close');
}


/*function showContacts() {
	if ( ! oContacts ) {
		prepareContacts();
	}
	if ( oKlans ) {
		hideKlans();
	}

	if ( oSmiles ) {
		hideSmiles();
	}

	var list = document.forms[ 'formContacts' ].getElementsByTagName( 'INPUT' );
	var count = list.length;

	for ( var i = 0; i < count; i++ ) {
		list[ i ].checked = false;
	}
	oContacts.style.visibility = 'visible';
}*/

function hideContacts() {
	$('#contacts-dialog').dialog('close');
}

function setToFromContacts( user ) {
	oTo = document.getElementById('inputTo');
	oTo.value = user;
	hideContacts();
	countTo();
}

function prepareKlans() {
    oButtonKlans = document.getElementById( 'buttonKlans' );
	oKlans = document.getElementById( 'divKlans' );
	oTo = document.getElementById( 'inputTo' );

    var baseX = getPosX( oButtonKlans );
    var baseY = getPosY( oButtonKlans );

    oKlans.style.left = baseX + 'px';
    oKlans.style.top = ( baseY + 20 ) + 'px';
}

function showKlans() {
	if ( ! oKlans ) {
		prepareKlans();
	}
	if ( oContacts ) {
		hideContacts();
	}
	if ( oSmiles ) {
		hideSmiles();
	}

	var list = document.forms[ 'formKlans' ].getElementsByTagName( 'INPUT' );
	var count = list.length;

	for ( var i = 0; i < count; i++ ) {
		list[ i ].checked = false;
	}
	oKlans.style.visibility = 'visible';
}

function hideKlans() {
	oKlans.style.visibility = 'hidden';
}

function setToFromKlans( user ) {
	oTo.value = user;
	hideKlans();
	countTo();
}

function writeKlan( klan ) {
	var klanImg = '<img class="vam" src="http://img.carnage.ru/i/klan/' + klan[ 'str' ] + '.gif" width="25" height="16"/>';

	var alignImg = '';

	if ( klan[ 'align' ] != 0 ) {
		alignImg = '<img class="vam" src="http://img.carnage.ru/i/align' + klan[ 'align' ] + '.gif" width="25" height="16"/>';
	}

	var klanTitle = '&nbsp;<b>' + klan[ 'name' ] + '</b>';
	var user = '';

	if ( klan[ 'user' ] ) {
		user = '&nbsp;�����&nbsp;<b>' + klan[ 'user' ] + '</b>';
	}

	return alignImg + klanImg + klanTitle + user;

}

function writeGuild( guild ) {
	var guildImg = '<img class="vam" src="http://img.carnage.ru/i/guild/' + guild[ 'str' ] + '.gif" width="25" height="16"/>';

	var guildTitle = '&nbsp;<b>' + guild[ 'name' ] + '</b>';
	var user = '';

	if ( guild[ 'user' ] ) {
		user = '&nbsp;�����&nbsp;<b>' + guild[ 'user' ] + '</b>';
	}
	
	return guildImg + guildTitle + user;
}

function takeAttach( attach ) {
	document.forms[ 'formViewInbox' ].elements[ 'cmd' ].value = 'attach.take';
	document.forms[ 'formViewInbox' ].elements[ 'attach' ].value = attach;
	document.forms[ 'formViewInbox' ].submit();
}

function deleteAttach( attach ) {
	if ( confirm( '��������! ������ ���� �������, �� ������������ ��� ���������.\n\n�� �������, ��� ������ ������� ���� �������?' ) ) {
		document.forms[ 'formViewInbox' ].elements[ 'cmd' ].value = 'attach.delete';
		document.forms[ 'formViewInbox' ].elements[ 'attach' ].value = attach;
		document.forms[ 'formViewInbox' ].submit();
	}
}

function takeAttaches() {
	var itemsStr = getItemList( 'formInbox' );

	document.forms[ 'formInbox' ].elements[ 'cmd' ].value = 'attaches.take';
	document.forms[ 'formInbox' ].elements[ 'item_list' ].value = itemsStr;
	document.forms[ 'formInbox' ].submit();
}

function deleteInboxLetters(){
	if (confirm('��������! ���� ������ �������� ��������, �� ��� ����� ����� �������.\n\n�� �������, ��� ������ �������?')) {
		var itemsStr = getItemList('formInbox');
		
		document.forms['formInbox'].elements['cmd'].value = 'inbox.delete';
		document.forms['formInbox'].elements['item_list'].value = itemsStr;
		document.forms['formInbox'].submit();
	}
}

function deleteInboxLetter(){
	if (confirm('��������! ���� ������ �������� ��������, �� ��� ����� ����� �������.\n\n�� �������, ��� ������ �������?')) {
		document.forms['formViewInbox'].elements['cmd'].value = 'inbox.delete';
		document.forms['formViewInbox'].submit();
	}
}

function replyLetter(){
	document.forms['formViewInbox'].elements['cmd'].value = 'show.write';
	document.forms['formViewInbox'].submit();
}

function forwardLetter(){
	var dialog = getDialog();
	
	if(!dialog) return false;
	
	var html = '����:';
	html += ' <input type="text" value="" id="forward_letter_to" />';
	html += ' <input type="button" class="button" value="�����������" id="forward_ok_button" />';
	html += ' <br/><span style="color:red;">��������! ��������� ����� ����� ������ ���� ���! ������� ����� ��������� ��� ��� �� ������!</span>';
	dialog.html(html);
	jQuery('#forward_ok_button').click(function() {
		document.forms['formViewInbox'].elements['forward_to'].value = jQuery('#forward_letter_to').val();
		document.forms['formViewInbox'].elements['cmd'].value = 'post.forward';
		document.forms['formViewInbox'].submit();	
	});
	dialog.dialog('option', 'title', '��������� ������');
	dialogOpen(dialog);
}

function chooseGift( entry, madein, name, type, code ) {	
	opener.document.forms[ 'giftForm' ].elements[ 'entry' ].value = entry;
	opener.document.forms[ 'giftForm' ].elements[ 'madein' ].value = madein;

	var oGiftName = opener.document.getElementById( 'idGiftName' );
	oGiftName.innerHTML = name;

	var oPostcard = opener.document.getElementById( 'divPostcard' );

	if ( type == 42 ) {
		oPostcard.style.display = '';
	} else {
		opener.document.forms[ 'giftForm' ].elements[ 'txt' ].value = '';
		oPostcard.style.display = 'none';
	}
	
	opener.eval( code );
	close();
}

function selectGift() {
	var left = Math.floor(screen.width / 2 - 600 / 2); 
	var top = Math.floor(screen.height / 2 - 550 / 2);
	window.open('?cmd=show.giftlist&nd=' + mynd + '&' + Math.random(), 'gifts', 'height=550, left=' + left + ',location=no, resizable=no, scrollbars=yes, top=' + top + ',width=600');
}

function showDateTime() {
	var oDiv = document.getElementById( 'date-time' );
	var oImg = document.getElementById( 'imgCollapse' );
	
	$(oDiv).slideToggle('fast', function() {
		if(this.style.display == 'none') {
			oImg.src = imgURL + 'sb_plus.gif';
			oImg.title = '����������';
		} else {
			oImg.src = imgURL + 'sb_minus.gif';
			oImg.title = '��������';
		}
	});
}

function deleteSentLetters(){
	if (confirm('�� �������, ��� ������ �������?')) {
		var itemsStr = getItemList('formSent');
		
		document.forms['formSent'].elements['cmd'].value = 'sent.delete';
		document.forms['formSent'].elements['item_list'].value = itemsStr;
		document.forms['formSent'].submit();
	}
}

function deleteSentLetter(){
	if (confirm('�� �������, ��� ������ �������?')) {
		document.forms['formViewSent'].elements['cmd'].value = 'sent.delete';
		document.forms['formViewSent'].submit();
	}
}

function getItemList(formName){
	var list = document.forms[formName].getElementsByTagName('INPUT');
	var count = list.length;
	
	var items = new Array();
	
	for (var i = 0; i < count; i++) {
		if (list[i].checked && list[i].type == 'checkbox') {
			items.push(list[i].value);
		}
	}
	return items.join(', ');
}

function countTo(){
	if (!oToCounter) {
		oToCounter = document.getElementById('spanToCounter');
	}
	
	if (!oToCounter) {
		return;
	}
	
	if (!oTo) {
		oTo = document.getElementById('inputTo');
	}
	
	var code = oTo.value.length;
	
	if (code > 100) {
		oToCounter.style.color = '#f00';
	} else {
		oToCounter.style.color = '#000';
	}
	
	oToCounter.innerHTML = code;	
}

function countSubject(){
	if (!oSubjectCounter) {
		oSubjectCounter = document.getElementById('spanSubjectCounter');
	}
	
	if (!oSubjectCounter) {
		return;
	}
	
	if (!oSubject) {
		oSubject = document.forms['formWrite'].elements['subject'];
	}
	
	var code = oSubject.value.length;
	
	if (code > 100) {
		oSubjectCounter.style.color = '#f00';
	} else {
		oSubjectCounter.style.color = '#000';
	}
	
	oSubjectCounter.innerHTML = code;
}


function countBody(){
	if (!oBodyCounter) {
		oBodyCounter = document.getElementById('spanBodyCounter');
	}
	
	if (!oBodyCounter) {
		return;
	}
	
	if (!oBody) {
		oBody = document.forms['formWrite'].elements['body'];
	}
	
	var code = oBody.value.length;
	
	//
	if (code > 500) {
		oBodyCounter.style.color = '#f00';
	} else {
		oBodyCounter.style.color = '#000';
	}
	
	oBodyCounter.innerHTML = code;
	
	return code;
}

function countGiftBody(){
	if (!oGiftBodyCounter) {
		oGiftBodyCounter = document.getElementById('spanGiftBodyCounter');
	}
	
	if (!oGiftBodyCounter) {
		return;
	}
	
	if (!oGiftBody) {
		oGiftBody = document.forms['giftForm'].elements['body'];
	}
	
	var code = oGiftBody.value.length;
	
	if (code > 100) {
		oGiftBodyCounter.style.color = '#f00';
	} else {
		oGiftBodyCounter.style.color = '#000';
	}
	
	oGiftBodyCounter.innerHTML = code;
}

function countGiftText(){
	if (!oGiftTextCounter) {
		oGiftTextCounter = document.getElementById('spanGiftTextCounter');
	}
	
	if (!oGiftTextCounter) {
		return;
	}
	
	if (!oGiftText) {
		oGiftText = document.forms['giftForm'].elements['txt'];
	}
	
	var code = oGiftText.value.length;
	
	if (code > 500) {
		oGiftTextCounter.style.color = '#f00';
	} else {
		oGiftTextCounter.style.color = '#000';
	}
	
	oGiftTextCounter.innerHTML = code;	
}

function selectAll(formName){
	if (!oSelectAll) {
		oSelectAll = document.getElementById('oSelectAll');
	}
	
	var list = document.forms[formName].getElementsByTagName('INPUT');
	var count = list.length;
	var items = new Array();
	
	for (var i = 0; i < count; i++) {
		if (list[i].type == 'checkbox') {
			list[i].checked = oSelectAll.checked;
		}
		
	}
	
	var title = '';
	
	if (oSelectAll.checked) {
		title = '����� ��������� �� ����';
	} else {
		title = '�������� ���';
	}
	
	oSelectAll.title = title;	
}


// ���������� ���������� �� ��� X ��������
function getPosX(obj){
	var currentX = 0;
	
	while (obj.offsetParent != null) {
		currentX += obj.offsetLeft;
		obj = obj.offsetParent;
		
	}
	
	return currentX + obj.offsetLeft;	
}


// ���������� ���������� �� ��� Y ��������
function getPosY(obj){
	var currentY = 0;
	
	while (obj.offsetParent != null) {
		currentY += obj.offsetTop;
		obj = obj.offsetParent;
		
	}
	
	return currentY + obj.offsetTop;
}

function itemOut(number, item_main, item_property, item_request, remote){
	var s = '';
	if (item_main.big == 1) {
		s += '' +
			'<a href="' + item_main.image_full + '" target="_blank">' +
				'<img alt="" onmousemove="movehint(event)" onmouseout="hh()" onmouseover="show_item(' + number + ', event);" src="' + item_main.image_preview + '" />' +
			'</a>';
	} else {
		s += '<img alt="" onmousemove="movehint(event)" onmouseout="hh()" onmouseover="show_item(' + number + ', event)" src="' + item_main.image_preview + '" />';
	}
	if (remote != 1) {
		s += '' +
			'<ul class="attach-actions">' +
				'<li>' +
					'<a href="#" onclick="takeAttach(' + number + '); return false;">�������</a>' + 
				'</li>' +
				'<li>' +
					'<a href="#" onclick="deleteAttach(' + number + '); return false;">' +
						'<img alt="�������" src="http://img.carnage.ru/i/x.gif" title="�������" />' +
					'</a>' +
				'</li>' +
			'</ul>';
	}
	document.write(s);
	show_items[number] = {
		'main': item_main,
		'property': item_property,
		'request': item_request
	};
}

function descriptionAlt(item_main, item_property, item_request){
	var st = 'style="color: red;"';
	var s = '';
	s = '<b>' + item_main.name + '</b> ';
	if (user.maxWeight < (user.itemsWeight + item_main.weight)) {
		s += '<span style="color: red;">(�����: ' + item_main.weight + ')</span><br />';
	} else {
		s += '(�����: ' + item_main.weight + ')<br />';
	}
	s += descriptionMain(item_main, item_property, item_request, st);
	return s;
}

function descriptionMain(item_main, item_property, item_request, st){
	var s = '';
	
	if (user.money < item_main.price) {
		s += '<span color="red italic">����: <b>' + item_main.price + '</b> ��.</span><br />';
	} else {
		s += '����: <b>' + item_main.price + '</b> ��.<br />';
	}
	
	s += '�������������: ' + item_main.durability + '/' + item_main.durability0 + '<br />';
	
	if (item_main.valid != "") {
		s += '���� ��������: ' + item_main.valid + '<br />';
	}
	
	var s1 = '';
	if (item_request.level > 0) {
		if (user.level < item_request.level) {
			s1 += '<li ' + st + '>�������: ' + item_request.level + '</li>';
		} else {
			s1 += '<li>�������: ' + item_request.level + '</li>';
		}
	}
	if (item_request.str > 0) {
		if (user.strength < item_request.str) {
			s1 += '<li ' + st + '>����: ' + item_request.str + '</li>';
		} else {
			s1 += '<li>����: ' + item_request.str + '</li>';
		}
	}
	if (item_request.dex > 0) {
		if (user.dexterity < item_request.dex) {
			s1 += '<li ' + st + '>��������: ' + item_request.dex + '</li>';
		} else {
			s1 += '<li>��������: ' + item_request.dex + '</li>';
		}
	}
	if (item_request.suc > 0) {
		if (user.success < item_request.suc) {
			s1 += '<li ' + st + '>��������: ' + item_request.suc + '</li>';
		} else {
			s1 += '<li>��������: ' + item_request.suc + '</li>';
		}
	}
	if (item_request.end > 0) {
		if (user.endurance < item_request.end) {
			s1 += '<li ' + st + '>����������������: ' + item_request.end + '</li>';
		} else {
			s1 += '<li>����������������: ' + item_request.end + '</li>';
		}
	}
	if (item_request.intel > 0) {
		if (user.intelligence < item_request.intel) {
			s1 += '<li ' + st + '>���������: ' + item_request.intel + '</li>';
		} else {
			s1 += '<li>���������: ' + item_request.intel + '</li>';
		}
	}
	if (item_request.lic_war == 1) {
		if (user.warrior == 0) {
			s1 += '<li ' + st + '>�������� ��������</li>';
		} else {
			s1 += '<li>�������� ��������</li>';
		}
	}
	if (item_request.type == 43) {
		if (user.age < 18) {
			s1 += '<li ' + st + '>�������: �� 18 ���</li>'
		} else {
			s1 += '<li>�������: �� 18 ���</li>'
		}
	}
	if (s1) {
		s1 = '<b>����������:</b><ul>' + s1 + '</ul>';
	}
	s += s1;
	
	var s0 = '';
	if (item_property.strength > 0) {
		s0 += '<li>����: +' + item_property.strength + '</li>';
	}
	if (item_property.dexterity > 0) {
		s0 += '<li>��������: +' + item_property.dexterity + '</li>';
	}
	if (item_property.success > 0) {
		s0 += '<li>��������: +' + item_property.success + '</li>';
	}
	if (item_property.endurance > 0) {
		s0 += '<li>������� �����: +' + item_property.endurance + '</li>';
	}
	if (item_property.weariness > 0) {
		s0 += '<li>������������: +' + item_property.weariness + '</li>';
	}
	if (item_property.intelligence > 0) {
		s0 += '<li>���������: +' + item_property.intelligence + '</li>';
	}
	if (item_property.krit > 0) {
		s0 += '<li>����������� ����: +' + item_property.krit + '</li>';
	}
	if (item_property.ukrit > 0) {
		s0 += '<li>������ ������������ �����: +' + item_property.ukrit + '</li>';
	}
	if (item_property.uvor > 0) {
		s0 += '<li>�����������: +' + item_property.uvor + '</li>';
	}
	if (item_property.uuvor > 0) {
		s0 += '<li>������ �����������: +' + item_property.uuvor + '</li>';
	}
	if (item_property.uronmin > 0) {
		s0 += '<li>����������� ����: +' + item_property.uronmin + '</li>';
	}
	if (item_property.uronmax > 0) {
		s0 += '<li>������������ ����: +' + item_property.uronmax + '</li>'
	}
	if (item_property.b1 > 0) {
		s0 += '<li>����� ������: +' + item_property.b1 + '</li>'
	}
	if (item_property.b2 > 0) {
		s0 += '<li>����� �������: +' + item_property.b2 + '</li>'
	}
	if (item_property.b3 > 0) {
		s0 += '<li>����� �����: +' + item_property.b3 + '</li>'
	}
	if (item_property.b4 > 0) {
		s0 += '<li>����� ���: +' + item_property.b4 + '</li>'
	}
	if (item_property.abrasion > 0) {
		s0 += '<li>����������� ������������: ' + item_property.abrasion + '%</li>'
	}
	if (item_property.duration > 0) {
		s0 += '<li>����������������� ��������: ' + item_property.duration + ' ���.</li>'
	}
	if (s0) {
		s0 = '<b>��������:</b><ul>' + s0 + '</ul>';
	}
	s += s0;
	
	if (item_main.action && item_main.action != '<span></span>') {
		s += '<b>��������:</b><br />' + item_main.action
	}
	
	if (item_main.descr) {
		s += '<b>��������:</b><br />' + item_main.descr + '<br />';
	}
	return s;
}

function movehint(event){
	if (document.getElementById("hint1").style.visibility == 'visible') {
		setpos(event);
	}
	return (true);
}

function big(type, num){
	window.open("http://enc.carnage.ru/" + type + "/" + num + ".html", "displayWindow", "height=600,width=540,resizable=yes,scrollbars=yes,menubar=no,status=no,toolbar=no,directories=no,location=no")
}

function show_item(number, event){
	var da = '';
	var img = show_items[number];
	da += descriptionAlt(img.main, img.property, img.request);
	document.getElementById("hint1").innerHTML = da;
	setpos(event);
	document.getElementById("hint1").style.visibility = 'visible';
	document.onmousemove = movehint;
}

function setpos(event){
	if (event) {
		var x, y;
		var hint1 = document.getElementById('hint1');
		if (event.clientX + hint1.clientWidth + 20 >= document.body.clientWidth) {
			x = document.documentElement.scrollLeft + event.clientX - hint1.clientWidth - 10;
		} else {
			x = event.clientX + document.documentElement.scrollLeft + 10;
		}
		if (event.clientY + hint1.clientHeight + 20 >= document.documentElement.clientHeight) {
			y = document.documentElement.scrollTop + document.documentElement.clientHeight - hint1.clientHeight - 20;
		} else {
			y = event.clientY + document.documentElement.scrollTop + 20;
		}
		
		hint1.style.left = x + 'px';
		hint1.style.top = y + 'px';
	}
}

function hh() { 
	document.getElementById('hint1').style.visibility = 'hidden';
}

function itemOut2(number, item_main, item_property, item_request){
	var s = '';
	if (item_main.big == 1) {
		s += '' +
			'<a target="_blank" href="' + item_main.image_full + '">' +
				'<img alt="" onmousemove="movehint(event)" onmousemove="movehint(event)" onmouseout="hh()" onmouseover="show_item(' + number + ', event)" src="' + item_main.image_preview + '" />' +
			'</a>';
	} else {
		s += '<img alt="" onmousemove="movehint(event)" onmouseout="hh()" onmouseover="show_item(' + number + ', event)" src="' + item_main.image_preview + '" />'
	}
	show_items[number] = {
		'main': item_main,
		'property': item_property,
		'request': item_request
	};
	var oDivGift = document.getElementById('divGift');
	oDivGift.innerHTML = s;
}

function itemOut3(number, item_main, item_property, item_request){
	var s = '';
	if (item_main.big == 1) {
		s += '' +
			'<a target="_blank" href="' + item_main.image_full + '">' +
				'<img alt="" onmouseout="hh()" onmouseover="show_item(' + number + ', event)" src="' + item_main.image_preview + '" />' +
			'</a>';
	} else {
		s += '<img alt="" onmousemove="movehint(event)" onmouseout="hh()" onmouseover="show_item(' + number + ', event)" src="' + item_main.image_preview + '" />'
	}
	document.write(s);
	show_items[number] = {
		'main': item_main,
		'property': item_property,
		'request': item_request
	};
}

function do_trans(){
	// translit
	var tr_body = document.getElementsByName("body");
	var tr_subject = document.getElementsByName("subject");
	var tr_txt = document.getElementsByName("txt");
	
	if (tr_body.length) {
		tr_body[0].value = transliterate(tr_body[0].value);
	}
	
	if (tr_subject.length) {
		tr_subject[0].value = transliterate(tr_subject[0].value);
	}
	
	if (tr_txt.length) {
		tr_txt[0].value = transliterate(tr_txt[0].value);
	}
	
	return false;
}

$(function() {
	$('.table-list tbody tr, .contact_str').hover(
		function() {
			$(this).addClass('hover');
		},
		function() {
			$(this).removeClass('hover');
		}
	)
	
	$('.convert-smilies').each(function() {
		var smileLimit = undefined;
		if ( letterType && letterType == 11 ) {
			smileLimit = 1000000;
		}
		$(this).html(smiley2img($(this).html(), smileLimit));
	});
	
	if ($('#contents_smile').length) {
		$('#contents_smile').charCounter(null, {
			container: $('#gift-body-charcounter')
		});
	}
	
	$('input.limit, textarea.limit').each(function() {
		$(this).charCouner(null, {
			container: $('#' + this.id + '-charcounter')
		});
	});
	
	$('#translit-gift').click(function() {
		var field = $('#contents_smile');
		field.val(transliterate(field.val())).change();
	});
	
	var dateDelivery = $('#date_delivery');
	if (dateDelivery.length) {
		dateDelivery.datepicker({
			minDate: '-0'
		});
	}
	
	var contactsDialog = $('#contacts-dialog');
	if(contactsDialog.length) {
		contactsDialog.dialog({
			autoOpen: false,
			height: 'auto',
			minHeight: 0,
			width: 400	
		});
		
		$('#buttonContacts').click(function() {
			contactsDialog.dialog('open');
		});
	}
	
	$('#date-time-link').click(function() {
		showDateTime(); 
		return false;
	});
	
	if ($.mask) {
		$.mask.definitions['2'] = '[0-2]';
		$.mask.definitions['5'] = '[0-5]';
		$('#delivery-time').mask('29:59',{
			completed: function(){
				var arr = this.val().split(':');
				if(Number(arr[0]) > 23 || Number(arr[1]) > 59) {
					alert('�������� ������ �������');
				}
			}
		});
	}
});
