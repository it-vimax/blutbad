$(function() {
	var delay = 60 * 1000;
	var reloadTime = 60 * 60 * 1000; // 1 ���
	var viewsCounter = 0; // ������� �������
	var reloadCounter = 60; // ������� ������� ��������� �� ������������ ��������
	var reloadURL = '/top.php?' + Math.random();
	
	var pos = 0;
	var length = messages.length;
	var msg = $('#msg');
	var con = $('#msg .container');
	var isAnim = false;
	
	if(length) {
		showMessage();
	} else {
		setTimeout(function() {
			window.location = reloadURL;
		}, reloadTime);
	}

	function showMessage() {
		stopAnimation();
		setTimeout(function() {
			con.html(messages[ pos ]);
			//con.fadeIn('slow');
			con.show();
			checkWidth();
			pos++;
			
			if(pos >= length) pos = 0;
			
			if(length > 1) {
				viewsCounter++;
				if (viewsCounter > reloadCounter) {
					window.location = reloadURL;
				} else {
					setTimeout(showMessage, delay);
				}
			} else {
				setTimeout(function() {
					window.location = reloadURL;
				}, reloadTime);
			}
		}, 1000);
	}
	
	function startAnimation() {
		if(!isAnim) {
			isAnim = true;
			animateText();
		}
	}
	
	function stopAnimation() {
		isAnim = false;
		con.clearQueue().stop().hide();
		msg.css('text-align', 'center');
		con.css('left', 0);
	}
	
	function animateText() {
		if(isAnim) {
			msg.css('text-align', 'left');
			con.css('left', 0);
			width = con.outerWidth();
			
			var dist = width - $(window).width();
			
			con.delay(1000).animate({
				'left': -dist
			}, dist * 50, 'linear')
			.delay(2000)
			.animate({
				'left': 0
			}, dist * 50, function() {
				setTimeout(animateText, 2000);
			});
		}
	}
	
	$(window).resize(function() {
		checkWidth();
	});
	
	var timeout;
	var lastTime = new Date();
	
	function checkWidth() {
		if($(window).width() < con.outerWidth()) {
			lastTime = new Date().getTime();
			clearTimeout(timeout);
			timeout = setTimeout(function() {
				if(new Date().getTime() - lastTime > 800 && $(window).width() < con.outerWidth()) {
					startAnimation();
				}
			}, 1000);
		} else {
			stopAnimation();
			con.show();
		}
	}
});