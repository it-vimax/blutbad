<?php

 include_once("functions/jewel/FieldWayFinder.php");

class FieldCollapser {

 //	public static const COLLAPSE = 'COLLAPSE';

	private $vfield;

	private $count = 0;
	private $DIMENSION = 7;

	private $closeJewels;

	public $crashPoints = 0;

	private $wayFinder;

	private $columnDownPoints;

	public function __construct( $arg1) {
		$this->closeJewels = [];
		$this->columnDownPoints = [];

		$this->vfield = $arg1;
		$this->wayFinder = new FieldWayFinder($arg1);
	}

	public function getJewel( $arg1, $arg2 ) {
		return $this->vfield->getJewel($arg1, $arg2);
	}

	private function hidecloseJewels() {
		$loc4 = null;
		$loc1 = null;

		$this->columnDownPoints = [];

		$loc2 = false;

	   //	$this->closeJewels = SorterByJ::sort($this->closeJewels);

		$loc3 = 0;

		while($loc3 < count($this->closeJewels))  {
			$loc1 = $this->closeJewels[$loc3];

			if($loc1->hided == false)  {
				$this->vfield->clearJewel($loc1->I, $loc1->J);

				$loc4 = $this->crashPoints + 1;
				$this->crashPoints = $loc4;

				$this->columnDown($loc1->I, $loc1->J);

				$loc2 = true;
			}

			$loc1->type = -1;

			$loc3++;
		}

		//if($loc2) new Event(self::COLLAPSE);
	}

	public function collapse() {
		$loc1 = 0;

		$this->crashPoints = 0;
		$this->closeJewels = [];

		$loc2 = false;
		$loc3 = 0;

		while($loc3 < $this->DIMENSION) {
			$loc1 = ($this->DIMENSION - 1);

			while($loc1 >= 0) {
				if($this->selfCollapseOne($this->getJewel($loc3, $loc1))) $loc2 = true;

				$loc1--;
			}

			$loc3++;
		}

		$this->hidecloseJewels();

		return $loc2;
	}

	private function columnDown( $arg1, $arg2 ) {
		$loc1 = $arg2;

		while($loc1 >= 0) {
			$this->vfield->placeJewelDelay($this->getJewel($arg1, $loc1 - 1), $arg1, $loc1);

			$loc1--;
		}
	}

	private function selfCollapseOne( $arg1 ) {
		if($arg1 == null) return false;

		// ���� �� ����������� ��� �������
		$this->closeJewels = array_merge($this->closeJewels, $this->wayFinder->checkPointHor($arg1->I, $arg1->J));

		// ���� �� ���������� ��� �������
	    $this->closeJewels = array_merge($this->closeJewels, $this->wayFinder->checkPointVer($arg1->I, $arg1->J));

		$loc1 = false;

		if(count($this->closeJewels) != 0) $loc1 = true;

		return $loc1;
	}

}

?>