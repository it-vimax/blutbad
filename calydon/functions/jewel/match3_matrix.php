<?php

 include_once("functions/jewel/FieldWayFinder.php");
 include_once("functions/jewel/array_sorter.php");

class Game {

	private static $matrix          = array(); // ����� � ������; (y, x)
	private static $new_matrix      = array(); // ����� � ������ ���������; (x, y)

	private static $symbols         = 6; // ������� �������
	private static $DIMENSION       = 7; // ���� �� �������

# ������� ������������ ��������

	private static $array1          = array(array(-2, 0), array(-1, -1), array(-1, 1), array(2, -1), array(2, 1), array(3, 0));
	private static $array2          = array(array(1, -1), array(1, 1));
	private static $array3          = array(array(0, -2), array(-1, -1), array(1, -1), array(-1, 2), array(1, 2), array(0, 3));
	private static $array4          = array(array(-1, 1), array(1, 1));


	public static $jewels           = array(); // �������

	public static $newJewels        = array(); // ��������� ������� ����� ����

	public static $cur_newJewels    = array(); // ��� ��������� ������� ����� ���� �����
	public static $my_clear_jewels  = array(); // ��� ��������� �������
	public static $draw_matrix      = "";      // ����� ����������� ������

	public static $close_Jewels     = 0;       // ������� �������� ������
	public static $my_close_Jewels  = 0;       // ������� ���� ������


	public static $closeJewels      = array(); //
	public static $crashPoints      = 0;       // �������

	public static $wayFinder        = array(); // �������� �� ����������� � ���������
	public static $sorter           = array(); // ���������� �� ���������

    public static function getJewel($arg1, $arg2)
        {
            if (empty(self::$jewels) || self::$jewels == null)
            {
                return null;
            }
            if (empty(self::$jewels[$arg1]) || self::$jewels[$arg1] == null || self::$jewels[$arg1] == "")
            {
                return null;
            }
            if (empty(self::$jewels[$arg1][$arg2]) || self::$jewels[$arg1][$arg2] == null || self::$jewels[$arg1][$arg2] == "")
            {
                return null;
            }

            $jewel['type']    = self::$jewels[$arg1][$arg2]['type'];
            $jewel['wasHere'] = false;
            $jewel['hided']   = false;
            $jewel['I']       = $arg1;
            $jewel['J']       = $arg2;

            return $jewel;
        }

	public static function setJewel( $arg1, $arg2, $type, $val ) {
            $Jewel = self::getJewel($arg1, $arg2);
              if ($Jewel) {
                  $Jewel[$type] = $val;
                  return self::$jewels[$arg1][$arg2] = $Jewel;
              }
            return false;
	}

    public static function compileMatrix()
        {
            $loc1 = 0;
            $loc2 = 0;
            while ($loc2 < self::$DIMENSION)
            {
                $loc1 = 0;
                while ($loc1 < self::$DIMENSION)
                {
                    $jew = self::$jewels[$loc1][$loc2];
                    if ($jew['J'] < 0) {}
                     $jew['I'] = $loc1;
                     $jew['J'] = $loc2;
                     self::$jewels[$loc1][$loc2] = $jew;

                    ++$loc1;
                }
                ++$loc2;
            }
            return true;
        }

        public static function clearJewel($arg1, $arg2)
        {
            $loc1 = self::getJewel($arg1, $arg2);
            self::$jewels[$arg1][$arg2] = null;

             # ������� �� ��������
               $loc3 = 0;
               while ($loc3 < count(self::$closeJewels)) {
                $loc2 = self::$closeJewels[$loc3];
                 if ($loc2['I'] == $arg1 && $loc2['J'] == $arg2 && empty(self::$closeJewels[$loc3]["hided"])) {
                  self::$closeJewels[$loc3]["hided"] = true;
                 }
                 ++$loc3;
               }


            if ($loc1 != null)
            {
                self::setJewel($arg1, $arg2, "hided", true);
                return true;
            }

            return false;
        }

      # sort by parent_id in descending order
        public static function multi_sort($array, $key, $asc = true)
        {
            self::$sorter = new array_sorter($array, $key, $asc);
            return self::$sorter->sortit();
        }

        public static function hidecloseJewels() {
            $loc4             = "";
            $loc1             = null;
            $columnDownPoints = array();
            $loc2             = false;
            $loc3             = 0;

            self::$closeJewels = self::multi_sort(self::$closeJewels, "J", false);

            while ($loc3 < count(self::$closeJewels)) {
                $loc1 = self::$closeJewels[$loc3];

                if ($loc1["hided"] == false) {
                    self::clearJewel($loc1["I"], $loc1["J"]);

                    # $loc4 = self::$crashPoints + 1;
                    # self::$crashPoints = $loc4;

                  # �������� ���� �������
                    self::columnDown($loc1["I"], $loc1["J"]);
                    $loc2 = true;

                }
                # self::setJewel($loc1["I"], $loc1["J"], "type", -1);
                ++$loc3;
            }

            return true;
        }

        public static function columnDown($arg1, $arg2)
        {
            $loc1 = $arg2;
            while ($loc1 >= 0) {
                self::placeJewelDelay(self::getJewel($arg1, ($loc1 - 1)), $arg1, $loc1);
                $loc1--;
            }
            return true;
        }

     /***------------------------------------------
      * ������� ��������� ����
      **/

        public static function placeJewelDelay($arg1, $arg2, $arg3)
        {

           // echo $arg1["J"]."-".$arg3."<br>";
            self::$jewels[$arg2][$arg3] = $arg1;
            $arg_1 = $arg1;

           if ($arg1 == null)  return false;

            self::$jewels[$arg1["I"]][$arg1["J"]] = null;

            $loc3 = 0;

               while ($loc3 < count(self::$closeJewels)) {
                $loc2 = self::$closeJewels[$loc3];
                 if ($loc2['I'] == $arg_1["I"] && $loc2['J'] == $arg_1["J"]) {
                  self::$closeJewels[$loc3]["I"] = $arg2;
                  self::$closeJewels[$loc3]["J"] = $arg3;
                  self::$closeJewels[$loc3]["type"] = $arg_1['type'];
                 }
                 ++$loc3;
               }


          // $arg_1['I'] = $arg2;
          // $arg_1['J'] = $arg3;
          // self::$jewels[$arg1['I']][$arg1['J']] = $arg_1;
           return true;
        }

        public static function collapse() {
            $loc1              = 0;
            self::$crashPoints = 0;

            self::$closeJewels = array();
           // self::$newJewels   = array();

            $loc2              = false;
            $loc3              = 0;
            self::$wayFinder   = new FieldWayFinder(self::$jewels);

            while ($loc3 < self::$DIMENSION) {
                $loc1 = self::$DIMENSION - 1;
                while ($loc1 >= 0) {
                    if ($loc2 = self::selfCollapseOne(self::getJewel($loc3, $loc1))) {
						//$loc2 = true;
                    }
                    --$loc1;
                }
                ++$loc3;
            }

          if ($loc2) {

           self::$newJewels = self::draw_new_field();
           self::$newJewels = array();

          # �������� ����������
            self::hidecloseJewels();

          # �������� �����
            self::pushNewJewels();

          # �������� ������� ��������� ���������
            self::compileMatrix();
          }

          return $loc2;
        }

     public static function selfCollapseOne($arg1 = array()) {
           if (empty($arg1) || $arg1 == null) return false;

		 # ���� �� ����������� ��� �������
	 	   self::$closeJewels = array_merge(self::$closeJewels, self::$wayFinder->checkPointHor($arg1["I"], $arg1["J"]));
		 # ���� �� ���������� ��� �������
	       self::$closeJewels = array_merge(self::$closeJewels, self::$wayFinder->checkPointVer($arg1["I"], $arg1["J"]));

    	   $loc1 = false;

    	   if(count(self::$closeJewels) != 0) $loc1 = count(self::$closeJewels);

    	   return $loc1;
        }

        public static function pushNewJewels()
        {
            $loc1 = 0;

            $loc2 = self::getEmptyPoints();
            $loc3 = 0;
            $new_field = array();

               /* echo "jjj<br><pre>";
                 print_r(self::$newJewels);
                echo "</pre>";*/

           while ($loc3 < count($loc2))
            {
                $loc1 -= 1;
                while (@self::$jewels[$loc2[$loc3]["x"]][$loc1] != null)
                {
                    --$loc1;
                }

               // echo $loc2[$loc3]["x"]." - ".$loc1."<br>";

                if (!self::addJewelFromNewToField($loc2[$loc3]["x"], $loc1, $loc2[$loc3]["y"]))
                {
                    return false;
                }
                self::$jewels[$loc2[$loc3]["x"]][$loc1] = 0;
                ++$loc3;
            }
            self::shiftNewJewels();
            $loc3 = 0;
            while ($loc3 < self::$DIMENSION)
            {
                $loc1 = -1;
                while (@self::$jewels[$loc3][$loc1] != null)
                {
                    self::$jewels[$loc3][$loc1] = null;
                    --$loc1;
                }
                ++$loc3;
            }
            return true;
        }

        public static function addJewelFromNewToField($arg1, $arg2, $arg3)
        {
            $loc1 = @self::$newJewels[$arg1][$arg3];

            if (
               $loc1 == 0
            || @self::$newJewels[$arg1] == null
            || @self::$newJewels[$arg1][$arg3] == null)
            {
              // echo("������ � ���������� ������! ".$loc1." = ".$arg1." - ".($arg2)." - ".($arg3)." <br>");
                return false;
            }

            self::addJewel($loc1, $arg1, $arg2);

            self::$newJewels[$arg1][$arg3] = -1;
            self::placeJewelDelay(self::getJewel($arg1, $arg2), $arg1, $arg3);

            return true;
        }

        public static function draw_new_field()
        {
            $loc2  = array();

            return $loc2;
        }

        public static function getEmptyPoints()
        {
            $loc1  = 0;
            $loc2  = array();
            $loc_2 = array();
            $loc3  = 0;

            while ($loc3 < self::$DIMENSION)
            {
                $loc1 = (self::$DIMENSION - 1);

                while ($loc1 >= 0)
                {
                    if (self::$jewels[$loc3][$loc1] == null)
                    {
                        # $loc2.push(new Point($loc3, $loc1));
                        $type          = rand(1, 6);
                        $loc_2["x"]    = $loc3;
                        $loc_2["y"]    = $loc1;
                        $loc_2["type"] = $type;
                        array_push($loc2, $loc_2);

                        self::$newJewels[$loc3][$loc1] = $type;

                        self::$draw_matrix[$loc3][] = $type;
                    }
                    --$loc1;
                }
                ++$loc3;
            }

	       	self::$cur_newJewels = self::$newJewels;

            if (empty(self::$my_clear_jewels)) self::$my_clear_jewels = $loc2;

            return $loc2;
        }

        public static function shiftNewJewels()
        {
            $loc1 = 0;
            while ($loc1 < self::$DIMENSION)
            {
                if (@self::$newJewels[$loc1] != null)
                {
                    while (@self::$newJewels[$loc1][0] == -1)
                    {
                       array_shift(self::$newJewels[$loc1]);
                    }
                }
                ++$loc1;
            }
            return true;
        }

  # �������� � �������
    public static function addJewel($arg1, $arg2, $arg3)
        {
            $jewel            = array();
            $jewel['type']    = $arg1;
            $jewel['wasHere'] = false;
            $jewel['hided']   = false;
            $jewel['I']       = $arg2;
            $jewel['J']       = $arg3;

            self::$jewels[$arg2][$arg3] = $jewel;
            return true;
        }

  # ������� ��� � �������
    public static function sendMove($arg1, $arg2)
        {
            self::$jewels[$arg1["I"]][$arg1["J"]]["type"] = $arg2["type"];
            self::$jewels[$arg2["I"]][$arg2["J"]]["type"] = $arg1["type"];

			return true;
        }

  # ��������� �������
    public static function loadField($arg1 = array())
        {
            $loc1   = 0;
            self::$jewels = array();
            $loc2   = 0;
            while ($loc2 < count($arg1)) {
                self::$jewels[$loc2] = array();
                $loc1 = 0;
                while ($loc1 < count($arg1[$loc2])) {
                    if (@is_numeric($arg1[$loc2][$loc1])) {
                     self::addJewel($arg1[$loc2][$loc1], $loc2, $loc1);
                    } else {
                     self::addJewel(@$arg1[$loc2][$loc1]['type'], $loc2, $loc1);
                    }

                    ++$loc1;
                }
                ++$loc2;
            }

            /*while (self::collapse())
            {
                self::collapse();
            }*/
            self::$wayFinder   = new FieldWayFinder(self::$jewels);
            self::$newJewels = array();

            return true;
        }

  # ����������� �������
	public static function matrix_revers($matrix) {
		$new_matrix = array();
		for($i = 0; $i < self::$DIMENSION; $i++) {
			for($j = 0; $j < self::$DIMENSION; $j++) {
			  if (!empty(self::$matrix[$j][$i])) {
                $new_matrix[$i][$j] = self::$matrix[$j][$i];
			  }
			}
		}
		return $new_matrix;
	}

    public static function onCompleteLoad($data)
    {
        $_loc_4 = 0;
        $count = 0;
        $field = array();

            $_loc_4 = $count + 1;
            $count = $_loc_4;

            $_loc_4 = $data;

            for ($xs = 0; $xs < 7; $xs++) {

                $col         = $_loc_4[$xs];
                $c_x         = $xs - 0;
                // $field[$c_x] = array();

                for ($xd = 0; $xd < 7; $xd++) {

                    $el  = $col[$xd];
                    $c_y = $xd - 0;
                    $field[$c_x][$c_y] = $el;
                }

            }

        return $field;
    }











/***------------------------------------------
 * ����� ������
 **/

	public static function init() {
        while(true) {
			self::matrix();

			if(count(self::matches()) != 0) continue;
			if(self::moves()) break;
		}
	}

	private static function matrix() {
		$matrix = array();

		for($y = 0; $y < self::$DIMENSION; $y++) {
			for($x = 0; $x < self::$DIMENSION; $x++) {
				$matrix[$y][$x] = mt_rand(1, self::$symbols);
			}
		}

		self::$matrix =  $matrix;
	}

	private static function matches() {
		$matches = array();

		for($y = 0; $y < self::$DIMENSION; $y++) {
			for($x = 0; $x < self::$DIMENSION - 2; $x++) {
				$match = self::horizontal($x, $y);
				$n = count($match);

				if($n > 2) {
					$matches[] = $match;
					$x += $n - 1;
				}
			}
		}

		for($x = 0; $x < self::$DIMENSION; $x++) {
			for($y = 0; $y < self::$DIMENSION - 2; $y++) {
				$match = self::vertical($x, $y);
				$n = count($match);

				if($n > 2) {
					$matches[] = $match;
					$y += $n - 1;
				}
			}
		}

		return $matches;
	}

	private static function horizontal( $x, $y ) {
		$match[] = array($y, $x);

		for($i = 1; $x + $i < self::$DIMENSION; $i++) {
			if(self::$matrix[$y][$x] == self::$matrix[$y][$x + $i]) $match[] = array($y, $x + $i);
			else return $match;
		}

		return $match;
	}

	private static function vertical( $x, $y ) {
		$match[] = array($y, $x);

		for($i = 1; $y + $i < self::$DIMENSION; $i++) {
			if(self::$matrix[$y][$x] == self::$matrix[$y + $i][$x]) $match[] = array($y + $i, $x);
			else return $match;
		}

		return $match;
	}

	public static function moves() {
		for($i = 0; $i < self::$DIMENSION; $i++) {
			for($j = 0; $j < self::$DIMENSION; $j++) {
				if(self::m_patern($i, $j, array(array(1, 0)), self::$array1)) return true;
				if(self::m_patern($i, $j, array(array(2, 0)), self::$array2)) return true;
				if(self::m_patern($i, $j, array(array(0, 1)), self::$array3)) return true;
				if(self::m_patern($i, $j, array(array(0, 2)), self::$array4)) return true;
			}
		}
		return false;
	}

	public static function set_move( $d_x, $d_y, $m_x, $m_y ) {

		self::m_swap($d_x, $d_y, $m_x, $m_y);

		if(count(self::matches()) == 0) {
			self::m_swap($d_x, $d_y, $m_x, $m_y);

			return false;
		} else return true;
	}

	private static function remove( $matches) {
		for($i = 0; $i < count($matches); $i++) {
			for($j = 0; $j < count($matches[$i]); $j++) {
				$m1 = $matches[$i][$j][0];
				$m2 = $matches[$i][$j][1];

				self::$data[$m1][$m2] = -1;

				self::above($m1, $m2);
			}
		}

		self::add();

		if(count($matches) == 0)
			if(!self::moves()) return false;

		return true;
	}

	private static function add() {
		$objects = self::$new_matrix;

		for($i = 0; $i < self::$DIMENSION; $i++) {
			for($j = self::$DIMENSION - 1; $j >= 0; $j--) {
				if(self::$matrix[$j][$i] == -1) {
					self::$matrix[$j][$i] = mt_rand(1, self::$symbols);

					if(@$objects[$i][0] != '') {

						$objects[$i][] = self::$matrix[$j][$i];
					} else $objects[$i][0] = self::$matrix[$j][$i];

				}
			}
		}

		self::$new_matrix = $objects;
	}

# �������� ���� �������
	private static function above( $x, $_y ) {
		for($y = $x - 1; $y >= 0; $y--) {
			if(self::$matrix[$y][$_y] != -1) {
				self::$matrix[$y + 1][$_y] = self::$matrix[$y][$_y];
				self::$matrix[$y][$_y]     = -1;
			}
		}
	}

	private static function m_swap( $x1, $y1, $x2, $y2 ) {
		$symbol = self::$matrix[$y1][$x1];
		self::$matrix[$y1][$x1] = self::$matrix[$y2][$x2];
		self::$matrix[$y2][$x2] = $symbol;
	}

	private static function m_patern( $x, $y, $must, $need ) {
		$symbol = self::$matrix[$y][$x];

		for($i = 0; $i < count($must); $i++) if(!self::m_type($x + $must[$i][0], $y + $must[$i][1], $symbol)) return false;
		for($i = 0; $i < count($need); $i++) if(self::m_type($x + $need[$i][0], $y + $need[$i][1], $symbol)) return true;

		return false;
	}

	private static function m_type( $x, $y, $symbol ) {
		if(($x < 0) || ($x > self::$DIMENSION) || ($y < 0) || ($y > self::$DIMENSION)) return false;

		return (@self::$matrix[$y][$x] == $symbol);
	}

	public static function step() {
		$matches = self::matches();

		self::remove($matches);

		while($matches = self::matches()) self::remove($matches);

		return true;
	}

	public static function new_matrix() {
		if(count(self::$new_matrix)) return self::$new_matrix;
		else return false;
	}

	public static function setMatrix( $matrix ) {
		self::$matrix = $matrix;
	}

	public static function getMatrix() {
		return self::$matrix;
	}

# ����� �������

	public static function get_result_matrix() {

      $draw_matrix     = "";
      $matrix          = self::$matrix;
      $new_matrix      = self::$new_matrix;

      # ������� ����� �����
        $cur_newJewels = self::$cur_newJewels;

         $draw_matrix .= "<col x=\"1\">\n";

          foreach (self::$my_clear_jewels as $k => $v) {
           $symbol = $v["type"];
           $draw_matrix .= "   <el y=\"" . ($v["y"] + 1) . "\" type=\"".$symbol."\" />\n";
           self::$my_close_Jewels++;  # ������� ���� ������
          }

         $draw_matrix .= "</col>\n";

        # $closeJewels = SorterByJ.sort($closeJewels);

         if (!empty(self::$draw_matrix)) {
          foreach (self::$draw_matrix as $x => $objects) {

           $n_x = $x + 1;
           $draw_matrix .= "<col x=\"" . $n_x . "\">\n";

          # $objects = self::multi_sort($objects, "y", true);

           $y_id = 0;

           foreach($objects as $data_id => $type) {
              $y_id++;
              $draw_matrix .= "   <el  y=\"".($y_id)."\" type=\"".$type."\" />\n";
              self::$close_Jewels++;  # ������� ���� ������
           }
           $draw_matrix .= "</col>\n";
          }
         }
	   return $draw_matrix;
	}

}

?>