<?php

class FieldWayFinder {

	private $vfield = array();

	private $count     = 0;
	private $arg4      = array();

	public function __construct( $arg1 ) {
		$this->vfield = $arg1;
	}

	private function checkPointCHor( $arg1, $arg2, $arg3, $arg4 = array() ) {

        $Jewel = $this->getJewel($arg1, $arg2);
		if(empty($Jewel) || $this->getJewel($arg1, $arg2) == null) return false;
		$loc1 = $this->getJewel($arg1, $arg2);
		if(!($loc1['type'] == $arg3) || $loc1['wasHere']) return false;

        array_push($this->arg4, $loc1);

        $this->setJewel($arg1, $arg2, "wasHere", true);

		$loc2 = $this->count + 1;
		$this->count = $loc2;

		$this->checkPointCHor($arg1 + 1, $arg2, $arg3, array());
		$this->checkPointCHor($arg1 - 1, $arg2, $arg3, array());

        $this->setJewel($arg1, $arg2, "wasHere", false);
	}

	public function checkPointHor( $arg1, $arg2 ) {
		$loc1        = 0;
		$this->count = 0;

		$loc2        = array();
		$this->arg4  = array();

		if($this->getJewel($arg1, $arg2) == null) return $this->arg4;

	// 1
        $loc1 = $this->getJewel($arg1, $arg2);

		$this->checkPointCHor($arg1, $arg2, $loc1['type'], $this->arg4);

       // echo serialize($this->tmp_arg4),'<br>';

		if($this->count <= 2) $this->arg4 = Array();

		return $this->arg4;
	}

	private function checkPointCVer( $arg1, $arg2, $arg3, $arg4 = array() ) {

        $Jewel = $this->getJewel($arg1, $arg2);
		if(empty($Jewel) || $this->getJewel($arg1, $arg2) == null) return false;
		$loc1 = $this->getJewel($arg1, $arg2);
		if($loc1['type'] != $arg3 || $loc1['wasHere']) return false;

        array_push($this->arg4, $loc1);

        $this->setJewel($arg1, $arg2, "wasHere", true);

		$loc2 = $this->count + 1;
		$this->count = $loc2;

		$this->checkPointCVer($arg1, $arg2 + 1, $arg3, array());
		$this->checkPointCVer($arg1, $arg2 - 1, $arg3, array());

        $this->setJewel($arg1, $arg2, "wasHere", false);
	}

	public function checkPointVer( $arg1, $arg2 ) {
		$loc1           = 0;
		$this->count    = 0;

        $this->arg4     = array();

		if($this->getJewel($arg1, $arg2) == null) return $this->arg4;

		$loc1 = $this->getJewel($arg1, $arg2);

	    $this->checkPointCVer($arg1, $arg2, $loc1['type'], $this->arg4);

      # ���� ��� 3� �������� �� ������ �� ��������
		if($this->count <= 2) $this->arg4 = array();

		return $this->arg4;
	}

	public function getJewel( $arg1, $arg2 ) {
            if (empty($this->vfield) || $this->vfield == null) return null;
            if (empty($this->vfield[$arg1]) || $this->vfield[$arg1] == null) return null;
            if (empty($this->vfield[$arg1][$arg2]) || $this->vfield[$arg1][$arg2] == null) return null;
            return $this->vfield[$arg1][$arg2];
	}

	public function setJewel( $arg1, $arg2, $type, $val ) {
            $Jewel = $this->getJewel($arg1, $arg2);
            $Jewel[$type] = $val;
            return $this->vfield[$arg1][$arg2] = $Jewel;
	}

}

?>