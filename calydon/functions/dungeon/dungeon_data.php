<?

# ���������� ������ �� ������ � ������
   $dungeon_mapsize = array(
    '��� �����'                => array('width' => 9,  'height' => 14, 'main' => 'lab'),
    '�������� ������'          => array('width' => 37, 'height' => 40, 'main' => 'lab'),
    '��������'                 => array('width' => 46, 'height' => 37, 'main' => 'lab'),
    '������'                   => array('width' => 48, 'height' => 35, 'main' => 'cav'),
    '������ ������'            => array('width' => 39, 'height' => 30, 'main' => 'cav'),
    '���������'                => array('width' => 39, 'height' => 41, 'main' => 'kat'),
    '�����������'              => array('width' => 33, 'height' => 36, 'main' => 'kan'),
    '�����'                    => array('width' => 33, 'height' => 38, 'main' => 'min'),
    '��������� �����'          => array('width' => 45, 'height' => 37, 'main' => 'u_c'),
    '�������� ���'             => array('width' => 44, 'height' => 27, 'main' => 'd_f'),
    '������ ���������� ������' => array('width' => 48, 'height' => 38, 'main' => 'llm'),
   );

# ����� �������
   $dungeon_start = array(
    '��� �����'                => array('x' => 5,  'y' => 12, 'direction' => "n"),
    '�������� ������'          => array('x' => 19, 'y' => 12, 'direction' => "n"),
    '��������'                 => array('x' => 9,  'y' => 35, 'direction' => "n"),
    '������'                   => array('x' => 4,  'y' => 33, 'direction' => "n"),
    '������ ������'            => array('x' => 24, 'y' => 28, 'direction' => "w"),
    '���������'                => array('x' => 9,  'y' => 39, 'direction' => "w"),
    '�����������'              => array('x' => 17, 'y' => 4,  'direction' => "n"),
    '�����'                    => array('x' => 14, 'y' => 11, 'direction' => "s"),
    '��������� �����'          => array('x' => 29, 'y' => 35, 'direction' => "n"),
    '�������� ���'             => array('x' => 3,  'y' => 3,  'direction' => "e"),
    '������ ���������� ������' => array('x' => 5,  'y' => 36, 'direction' => "n"),
   );


 # ����� � dungeon
   $sounds = array("teleport", "chest_open", "magic_fontain", "switch", "switch_fail", "switch_grating_on", "switch_grating_off", "heal", "ambience", "open_door", "open_door_fail");


/*
������� �����
 */
# ������ � ��������
  $main_obj = array(
   'obj_grating_close'   => array('header' => '�������', 'miniheader' => '�������', 'mini' => '17', 'obraz' => '', 'action' => ''),
   'obj_grating_open'    => array('header' => '�������', 'miniheader' => '�������', 'mini' => '18', 'obraz' => '', 'action' => ''),

   'obj_switch'          => array('header' => '�����', 'miniheader' => '�����', 'mini' => '10', 'obraz' => 'obj_switch.png', 'action' => '3;'),
   'obj_switched'        => array('header' => '�����', 'miniheader' => '�����', 'mini' => '5', 'obraz' => 'obj_switched.png', 'action' => '3;'),

   'obj_irondoor_close'  => array('header' => '�������� �����', 'miniheader' => '�������� �����', 'mini' => '14', 'obraz' => '', 'action' => '9;'),
   'obj_irondoor_open'   => array('header' => '�������� �����', 'miniheader' => '�������� �����', 'mini' => '15', 'obraz' => '', 'action' => '10;'),

   'obj_golddoor_close'  => array('header' => '�������� �����', 'miniheader' => '�������� �����', 'mini' => '14', 'obraz' => '', 'action' => '9;'),
   'obj_golddoor_open'   => array('header' => '�������� �����', 'miniheader' => '�������� �����', 'mini' => '13', 'obraz' => '', 'action' => '10;'),

   'obj_stonedoor_close' => array('header' => '���������� �����', 'miniheader' => '���������� �����', 'mini' => '14', 'obraz' => '', 'action' => '9;'),
   'obj_stonedoor_open'  => array('header' => '���������� �����', 'miniheader' => '���������� �����', 'mini' => '13', 'obraz' => '', 'action' => '10;'),

   'obj_teleport'        => array('header' => '��������', 'miniheader' => '��������', 'mini' => '12', 'obraz' => 'obj_teleport.png', 'action' => '5;'),
   'obj_portal'          => array('header' => '�����', 'miniheader' => '�����', 'mini' => '7', 'obraz' => 'obj_portal.png', 'action' => '11;'),
   'obj_chest'           => array('header' => '������', 'miniheader' => '������', 'mini' => '8', 'obraz' => 'obj_chest.png', 'action' => '6;'),
   'obj_heal'            => array('header' => '����������������� ����', 'miniheader' => '����������������� ����', 'mini' => '9', 'obraz' => 'obj_heal.png', 'action' => '4;'),

   'obj_klan_buff'       => array('header' => '�����', 'miniheader' => '�����', 'mini' => '19', 'obraz' => 'obj_klan_buff.png', 'action' => '20;'),
   'obj_klan_chest'      => array('header' => '���������', 'miniheader' => '���������', 'mini' => '22', 'obraz' => 'obj_klan_chest.png', 'action' => '23;'),
   'obj_klan_repair'     => array('header' => '��������� ����������', 'miniheader' => '��������� ����������', 'mini' => '21', 'obraz' => 'obj_klan_repair.png', 'action' => '22;'),
   'obj_klan_heal'       => array('header' => '����������������� ����', 'miniheader' => '����������������� ����', 'mini' => '20', 'obraz' => 'obj_klan_heal.png', 'action' => '21;'),
   'obj_klan_shop'       => array('header' => '�������� �������', 'miniheader' => '�������� �������', 'mini' => '9', 'obraz' => 'obj_klan_shop.png', 'action' => '25;'),
   'obj_pond'            => array('header' => '�������', 'miniheader' => '�������', 'mini' => '15', 'obraz' => 'obj_pond.png', 'action' => '14;'),

   'obj_tree'            => array('header' => '������', 'miniheader' => '������', 'mini' => '24', 'obraz' => 'obj_tree.png', 'action' => ''),
   'obj_tree2'            => array('header' => '������', 'miniheader' => '������', 'mini' => '25', 'obraz' => 'obj_tree2.png', 'action' => ''),
   'obj_tree3'            => array('header' => '������', 'miniheader' => '������', 'mini' => '26', 'obraz' => 'obj_tree3.png', 'action' => ''),

   'obj_forge'           => array('header' => '���������� ����', 'miniheader' => '���������� ����', 'mini' => '32', 'obraz' => 'obj_forge.png', 'action' => '26;'),

  # ������
   'obj_res_gold'        => array('header' => '������� ������', 'miniheader' => '������� ������', 'mini' => '31', 'obraz' => 'obj_res_gold.png', 'action' => '29;'),
  # �������
   'obj_res_platinum'    => array('header' => '���������� ������',  'miniheader' => '���������� ������', 'mini' => '30', 'obraz' => 'obj_res_platinum.png', 'action' => '29;'),
  # �������
   'obj_res_silver'      => array('header' => '���������� ������', 'miniheader' => '���������� ������', 'mini' => '29', 'obraz' => 'obj_res_silver.png', 'action' => '29;'),
  # ����
   'obj_res_copper'      => array('header' => '������ ������', 'miniheader' => '������ ������', 'mini' => '28', 'obraz' => 'obj_res_copper.png', 'action' => '29;'),
  # �����
   'obj_res_coal'        => array('header' => '�������� ������', 'miniheader' => '�������� ������', 'mini' => '27', 'obraz' => 'obj_res_coal.png', 'action' => '29;'),

 );
     

# ������ � ��������
  $main_bot = array(

 # ��������

   'bot_wandering_guardian'   => array('header' => '���������� �����', 'miniheader' => '���������� �����', 'level' => '11', 'mini' => '1', 'obraz' => '1_bot_wandering_guardian.png', 'action' => '1'),
   'bot_flying_gloom'         => array('header' => '������� ����', 'miniheader' => '������� ����', 'level' => '11', 'mini' => '1', 'obraz' => '1_bot_flying_gloom.png', 'action' => '1'),
   'bot_leader_maze'          => array('header' => '����� ���������', 'miniheader' => '����� ���������', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_leader_maze.png', 'action' => '1'),
   'bot_gravedigger'          => array('header' => '���������', 'miniheader' => '���������', 'level' => '11', 'mini' => '1', 'obraz' => '1_bot_gravedigger.png', 'action' => '1'),
   'bot_werewolf_lucifer'     => array('header' => '��������� ��������', 'miniheader' => '��������� ��������', 'level' => '11', 'mini' => '1', 'obraz' => '1_bot_werewolf_lucifer.png', 'action' => '1'),
   'bot_stone_hour'           => array('header' => '�������� �������', 'miniheader' => '�������� �������', 'level' => '11', 'mini' => '1', 'obraz' => '1_bot_stone_hour.png', 'action' => '1'),
   'bot_nightmare'            => array('header' => '������ ������', 'miniheader' => '������ ������', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_nightmare.png', 'action' => '1'),
   'bot_stone_scout'          => array('header' => '������� ���������', 'miniheader' => '������� ���������', 'level' => '11', 'mini' => '1', 'obraz' => '1_bot_stone_scout.png', 'action' => '1'),
   'bot_hereafter_guard'      => array('header' => '��������� �����', 'miniheader' => '��������� �����', 'level' => '11', 'mini' => '1', 'obraz' => '1_bot_hereafter_guard.png', 'action' => '1'),
   'bot_bone_warden'          => array('header' => '�������� �����������', 'miniheader' => '�������� �����������', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_bone_warden.png', 'action' => '1'),

   'bot_master_maze'          => array('header' => '���������� ���������', 'miniheader' => '���������� ���������', 'level' => '12', 'mini' => '6', 'obraz' => '1_bot_master_maze.png', 'action' => '1'),

 # npc
   'quest_npc25790'           => array('header' => '��������� ���������', 'miniheader' => '��������� ���������', 'level' => '12', 'mini' => '11', 'obraz' => '1_bot_keepers_maze.png', 'action' => '7'),

 # ������

   'bot_hermit_ataman'        => array('header' => '��������� ������', 'miniheader' => '��������� ������', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_hermit_ataman.png', 'action' => '1'),
   'bot_nightmare_depths'     => array('header' => '������ ������', 'miniheader' => '������ ������', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_nightmare_depths.png', 'action' => '1'),
   'bot_bes'                  => array('header' => '���', 'miniheader' => '���', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_bes.png', 'action' => '1'),
   'bot_mechanical_golem'     => array('header' => '������������ �����', 'miniheader' => '������������ �����', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_mechanical_golem.png', 'action' => '1'),
   'bot_damn_squire'          => array('header' => '��������� ����������', 'miniheader' => '��������� ����������', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_damn_squire.png', 'action' => '1'),
   'bot_soul_kroggentayla'    => array('header' => '���� ������������', 'miniheader' => '���� ������������', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_soul_kroggentayla.png', 'action' => '1'),
   'bot_kroggentayl'          => array('header' => '�����������', 'miniheader' => '�����������', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_kroggentayl.png', 'action' => '1'),

   'bot_exile_mists'          => array('header' => '��������� ����', 'miniheader' => '��������� ����', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_exile_mists.png', 'action' => '1'),
   'bot_demon_wrath'          => array('header' => '����� �����', 'miniheader' => '����� �����', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_demon_wrath.png', 'action' => '1'),
   'bot_werewolf'             => array('header' => '���������', 'miniheader' => '���������', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_werewolf.png', 'action' => '1'),

   'bot_shipokryl'            => array('header' => '��������', 'miniheader' => '��������', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_shipokryl.png', 'action' => '1'),
   'bot_troll'                => array('header' => '������', 'miniheader' => '������', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_troll.png', 'action' => '1'),
   'bot_spirit_exilen'        => array('header' => '��� ��������', 'miniheader' => '��� ��������', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_spirit_exilen.png', 'action' => '1'),
   'bot_stone_krappt'         => array('header' => '�������� ������', 'miniheader' => '�������� ������', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_stone_krappt.png', 'action' => '1'),

   'bot_lord_caves'           => array('header' => '���������� ������', 'miniheader' => '���������� ������', 'level' => '10', 'mini' => '6', 'obraz' => '1_bot_lord_caves.png', 'action' => '1'),

 # ���������

   'bot_caretaker_haze'       => array('header' => '���������� ����', 'miniheader' => '���������� ����', 'level' => '8', 'mini' => '1', 'obraz' => '1_bot_caretaker_haze.png', 'action' => '1'),
   'bot_toothy_mucus'         => array('header' => '�������� �����', 'miniheader' => '�������� �����', 'level' => '8', 'mini' => '1', 'obraz' => '1_bot_toothy_mucus.png', 'action' => '1'),

   'bot_watchman_haze'        => array('header' => '������ ����', 'miniheader' => '������ ����', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_watchman_haze.png', 'action' => '1'),
   'bot_work_haze'            => array('header' => '������� ����', 'miniheader' => '������� ����', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_work_haze.png', 'action' => '1'),
   'bot_berserk'              => array('header' => '�������', 'miniheader' => '�������', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_berserk.png', 'action' => '1'),
   'bot_slimy_scavenger'      => array('header' => '��������� ���������', 'miniheader' => '��������� ���������', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_slimy_scavenger.png', 'action' => '1'),
   'bot_damned'               => array('header' => '���������', 'miniheader' => '���������', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_damned.png', 'action' => '1'),
   'bot_damn_zombies'         => array('header' => '��������� �����', 'miniheader' => '��������� �����', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_damn_zombies.png', 'action' => '1'),
   'bot_scavengers'           => array('header' => '���������� ������', 'miniheader' => '���������� ������', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_scavengers.png', 'action' => '1'),
   'bot_risen_zombies'        => array('header' => '���������� �����', 'miniheader' => '���������� �����', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_risen_zombies.png', 'action' => '1'),
   'bot_wandering_zombies'    => array('header' => '�������� �����', 'miniheader' => '�������� �����', 'level' => '9', 'mini' => '1', 'obraz' => '1_bot_wandering_zombies.png', 'action' => '1'),

   'bot_lord_catacombs'       => array('header' => '���������� ��������', 'miniheader' => '���������� ��������', 'level' => '10', 'mini' => '6', 'obraz' => '1_bot_lord_catacombs.png', 'action' => '1'),

 # npc
   'quest_npc26185'           => array('header' => '��������� ��������', 'miniheader' => '��������� ��������', 'level' => '10', 'mini' => '11', 'obraz' => '1_bot_keeper_catacombs.png', 'action' => '7'),

 # �����������   blutbadbot

   'bot_spider'               => array('header' => '����', 'miniheader' => '����', 'level' => '6', 'mini' => '1', 'obraz' => '1_bot_spider.png', 'action' => '1'),
   'bot_mummy'                => array('header' => '�����', 'miniheader' => '�����', 'level' => '6', 'mini' => '1', 'obraz' => '1_bot_mummy.png', 'action' => '1'),
   'bot_sewage_beetle'        => array('header' => '��������������� ���', 'miniheader' => '��������������� ���', 'level' => '6', 'mini' => '1', 'obraz' => '1_bot_sewage_beetle.png', 'action' => '1'),
   'bot_terrible_abomination' => array('header' => '������ ��������', 'miniheader' => '������ ��������', 'level' => '7', 'mini' => '1', 'obraz' => '1_bot_terrible_abomination.png', 'action' => '1'),
   'bot_sewer_spider'         => array('header' => '������� ����', 'miniheader' => '������� ����', 'level' => '7', 'mini' => '1', 'obraz' => '1_bot_sewer_spider.png', 'action' => '1'),
   'bot_terrible_rat'         => array('header' => '�������� �����', 'miniheader' => '�������� �����', 'level' => '7', 'mini' => '1', 'obraz' => '1_bot_terrible_rat.png', 'action' => '1'),
   'bot_flying_bestia'        => array('header' => '������� ������', 'miniheader' => '������� ������', 'level' => '7', 'mini' => '1', 'obraz' => '1_bot_flying_bestia.png', 'action' => '1'),

   'bot_sanguinary'           => array('header' => '��������', 'miniheader' => '��������', 'level' => '7', 'mini' => '1', 'obraz' => '1_bot_sanguinary.png', 'action' => '1'),
   'bot_mechanic_zombie'      => array('header' => '������� �����', 'miniheader' => '������� �����', 'level' => '7', 'mini' => '1', 'obraz' => '1_bot_mechanic_zombie.png', 'action' => '1'),
   'bot_zombie_plumber'       => array('header' => '����� ���������', 'miniheader' => '����� ���������', 'level' => '7', 'mini' => '1', 'obraz' => '1_bot_zombie_plumber.png', 'action' => '1'),
   'bot_foreman'              => array('header' => '������', 'miniheader' => '������', 'level' => '8', 'mini' => '1', 'obraz' => '1_bot_foreman.png', 'action' => '1'),

 # ��������� �����
   'bot_savage_soul'          => array('header' => '�������� ����', 'miniheader' => '�������� ����', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_savage_soul.png', 'action' => '1'),
   'bot_mardagayl'            => array('header' => '���������', 'miniheader' => '���������', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_mardagayl.png', 'action' => '1'),
   'bot_lindworm'             => array('header' => '��������', 'miniheader' => '��������', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_lindworm.png', 'action' => '1'),
   'bot_twilight_wanderer'    => array('header' => '��������� ��������', 'miniheader' => '��������� ��������', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_twilight_wanderer.png', 'action' => '1'),
   'bot_winged_demon'         => array('header' => '�������� �����', 'miniheader' => '�������� �����', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_winged_demon.png', 'action' => '1'),
   'bot_thalipedes'           => array('header' => '��������', 'miniheader' => '��������', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_thalipedes.png', 'action' => '1'),
   'bot_kedalion'             => array('header' => '��������', 'miniheader' => '��������', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_kedalion.png', 'action' => '1'),
   'bot_forgotten_ghost'      => array('header' => '������� �������', 'miniheader' => '������� �������', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_forgotten_ghost.png', 'action' => '1'),
   'bot_drakaina'             => array('header' => '��������', 'miniheader' => '��������', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_drakaina.png', 'action' => '1'),
   'bot_follower'             => array('header' => '�������������', 'miniheader' => '�������������', 'level' => '12', 'mini' => '1', 'obraz' => '1_bot_follower.png', 'action' => '1'),

   'bot_lord_chaos'           => array('header' => '���������� �����', 'miniheader' => '���������� �����', 'level' => '12', 'mini' => '6', 'obraz' => '1_bot_lord_chaos.png', 'action' => '1'),

 # �����

 # ��� �����

   'bot_klan_monster'         => array('header' => '���� ��� �����', 'miniheader' => '���� ��� �����', 'level' => '10', 'mini' => '1', 'obraz' => '1_bot_batd.png', 'action' => '1'),

 # ������ ���������� ������
   'bot_lord_material'        => array('header' => '���������� ������', 'miniheader' => '���������� ������', 'level' => '??', 'mini' => '1', 'obraz' => '1_bot_lord_material.png', 'action' => '1'),
 # npc
   'quest_npc10012'           => array('header' => '��������� ������', 'miniheader' => '��������� ������', 'level' => '??', 'mini' => '11', 'obraz' => '1_bot_keeper_lair.png', 'action' => '7'),


 );



 $obr_all = array(
 "0_0_F000",
 "0_0_F003",
 "0_0_F004",
 "0_0_F005",
 "0_0_F006",
 "0_0_F007",
 "0_0_F008",
 "0_0_F009",
 "0_0_F010",
 "0_0_F011",
 "0_0_F012",
 "0_0_F013",

 "0_1_F007",
 "0_1_F008",
 "0_1_F009",
 "0_1_F010",

 "0_0_M000",
 "0_0_M001",
 "0_0_M002",
 "0_0_M003",
 "0_0_M004",
 "0_0_M005",
 "0_0_M006",
 "0_0_M007",
 "0_0_M008",
 "0_0_M009",
 "0_0_M010",
 "0_0_M011",
 "0_0_M012",
 "0_0_M013",
 "0_0_M014",

 "0_1_M009",
 "0_1_M010",
 "0_1_M008",
 "0_1_M011",
 "0_1_M010"

 );

?>