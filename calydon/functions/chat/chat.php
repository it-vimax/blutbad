<?php

  include($_SERVER["DOCUMENT_ROOT"].'/xmpp/xmpp.php');

class CHAT {

    # ��������
    # init()

    # �������� ��� � ������������ � ��������� ����
    # add_system_movement($j_user, $room, $parm = array())

    # ������� ������� �� ������
    # chat_command ($j_user = array(), $command, $parm = array())

    # �������� ��������� ��������� ��������� � ���
    # chat_city_attention ($j_user = array(), $j_text = "", $parm = array())

    # �������� ��������� ��������� ��������� � ���
    # chat_attention ($j_user = array(), $text, $parm = array())

    # �������� ��������� ��������� ��������� � ���
    # chat_several_attention ($p_data = array(), $parm = array())

    # ������� ��� � ��������� ���
    # chat_system($j_user = array(), $text, $room = 0, $parm = array())

    # ������� ������ � js �����
    # chat_js_login($login)

    # ������� ������ � ������ ����� "BR TR" => "br_tr"
    # chat_format_login($login)

    # ������� ������� ������
    # chat_addXMPP()

	public static $encryptiond = true;
	public static $id_city     = 1;

	private static $xmppd;

/***------------------------------------------
 * ��������
 **/

	public static function init() {

        Cache::init();
        XMPP::init('admin', 'localhost', 'conference');
        self::$id_city = get_city(INCITY, 1);
	   //	self::$sersver = $sesrver;
	}

/***---------------------------------------------
 * �������� ��� � ������������ � ��������� ����
 *
 * $j_user:
 * $j_user['login']       = �������� �����
 * $j_user['login_jaber'] = ������ �����
 * $j_user['sex']         = ���
 * $j_user['room']        = ����� �������
 *
 * $room                  = � ����� ������� ��������
 *
 **/

	public static function add_system_movement($j_user, $room, $parm = array()) {
     global $rooms;

     if ($j_user['login'] && $j_user['login_jaber'] && $j_user['room']) {
      $new_room = $room;
      $old_room = $j_user['room'];
      $id_city  = self::$id_city;
      $str_room = $rooms[$room];

      if ($parm['roomName']) { $str_room = $parm['roomName']; }

      XMPP::system($id_city, $new_room, ''.self::chat_js_login($j_user).' ������������ ���');
      XMPP::system($id_city, $old_room, ''.self::chat_js_login($j_user).' ��������'.($j_user['sex']?"��":"���").' � '.$str_room, self::chat_format_login(       $j_user['login_jaber']));

      XMPP::changeRoom(self::chat_format_login($j_user['login_jaber']), $id_city, $new_room, $str_room);

      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
	}

/***--------------------------
 * ������� ������� �� ������
 *
 * �������:
 *
 * ������� � ������� ����          = "refresh|frame=main|url=/fbattle.php"
 * �������� ������� ������         = "reload_panel"
 * ��������� ������������ ��� ���� = "params|isBlind=0|roomName=�����|mentorName=|isSilence=0|align=0|groupId=|isInvisible=0|battleLogId=|roomId=3_120"
 * ��������� ����                  = "options|smilesOff=0|groupChannelOff=0|pokerChannelOff=0|clanChannelOff=0|messageStyle=color:#000000|autoscrollOff=0|systemChannelOff=0|tradeChannelOff=0|battleLogChannelOff=1|antimatOff=0"
 * ����� ��� �� �����              = "inbox|count=33"
 * ������� ��� ������              = "tutorial|event=begin" begin|stats_inc_all"
 * ������� ��� ���������           = "burn_fire|type=2"
 *
 **/

	public static function chat_command ($j_user = array(), $command, $parm = array()) { // array('addXMPP' => true)
     $tmp_user = $j_user;

     if(empty($j_user['login_jaber']) || strlen($j_user['login_jaber']) <= 1) {
       $j_user = array();
       $j_user['login_jaber'] = self::chat_format_login($tmp_user);
     }

     if ($command && $j_user['login_jaber'] && strlen($j_user['login_jaber']) > 1) {
      XMPP::command(self::chat_format_login($j_user['login_jaber']), $command . '|');
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
    }

/***------------------------------------------------
 * �������� ��������� ��������� ��������� � ���
 *
 * $j_user['login'] = ����� ��������
 *
 * $j_text = ����� �����
 *
 * $parm = �������
 * $parm['cityshout']  = ������� ����
 * $parm['cityshout2'] = ������� ����
 * $parm['city_alienspeech'] = ������� ����� ����
 * $parm['addXMPP'] = ����� �������
 *
 **/

    public static function chat_city_attention ($j_user = array(), $j_text = "", $parm = array()) {
     if ($j_text) {

      if ($parm['incity']) {
        $id_city = get_city($parm['incity'], 1);
      } else {
        $id_city = self::$id_city;
      }

      if ($parm['cityshout']) {
        XMPP::city($id_city, '<span class="cityshout">{['.$j_user['login'].']}: '.$j_text.'</span>');
      } elseif ($parm['cityshout2']) {
        XMPP::city($id_city, '<span class="cityshout2">{['.$j_user['login'].']}: '.$j_text.'</span>');
      } elseif ($parm['city_alienspeech']) {
        XMPP::city($id_city, '<span class="city_alienspeech">{['.$j_user['login'].']}: '.$j_text.'</span>');
      } else {
        XMPP::city($id_city, $j_text);
      }

      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
    }

/***-----------------------------------------------
 * �������� ��������� ��� �����
 *
 * $city_room = '3_120'; #
 *
 * $text = '������'; # ����� ���
 *
 * $parm['addXMPP'] = ����� �������
 *
 * CHAT::chat_room_message ('3_120', '['.$user_quest_men['login'].'] ������', array('addXMPP' => true));
 *
 **/

    public static function chat_room_message ($city_room = '', $text, $parm = array()) {
     if ($city_room) {
      XMPP::room_message($city_room, $text);
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
    }

/***-----------------------------------------------
 * �������� ��������� ��� �� ������ ������������
 *
 * $j_user['login_jaber'] = 'br'; # login_jaber
 *
 * $text = '������'; # ����� ���
 *
 * $parm['addXMPP'] = ����� �������
 *
 * CHAT::chat_log_message ($user, '['.$user_quest_men['login'].'] ������', array('addXMPP' => true));
 *
 **/

    public static function chat_log_message ($j_user = array(), $text, $parm = array()) {
     $tmp_user = $j_user;

     if(empty($j_user['login_jaber']) || strlen($j_user['login_jaber']) <= 1) {
       $j_user = array();
       $j_user['login_jaber'] = self::chat_format_login($tmp_user);
     }

     if ($j_user['login_jaber'] && strlen($j_user['login_jaber']) > 1) {
      XMPP::log_message(self::chat_format_login($j_user['login_jaber']), $text);
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
    }

/***-----------------------------------------------
 * �������� ��������� ��� �� ������ ������������
 *
 * $j_user['login_jaber'] = 'br'; # login_jaber
 *
 * $text = '������'; # ����� ���
 *
 * $parm['addXMPP'] = ����� �������
 *
 * CHAT::chat_private_message ($user, '['.$user_quest_men['login'].'] ������', array('addXMPP' => true));
 *
 **/

    public static function chat_private_message ($j_user = array(), $text, $parm = array()) {
     $tmp_user = $j_user;

     if(empty($j_user['login_jaber']) || strlen($j_user['login_jaber']) <= 1) {
       $j_user = array();
       $j_user['login_jaber'] = self::chat_format_login($tmp_user);
     }

     if ($j_user['login_jaber'] && strlen($j_user['login_jaber']) > 1) {
      XMPP::private_message(self::chat_format_login($j_user['login_jaber']), $text);
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
    }

/***-----------------------------------------------
 * �������� ��������� ��������� ��������� � ���
 * $parm = ����� ���� � ������
 *
 * $j_user['login_jaber'] = 'br'; # login_jaber
 *
 * $text = '������'; # ����� ���
 *
 * $parm['addXMPP'] = ����� �������
 **/

    public static function chat_attention ($j_user = '', $text, $parm = array()) {
     $tmp_user = $j_user;

     if(empty($j_user['login_jaber']) || strlen($j_user['login_jaber']) <= 1) {
       $j_user = array();
       $j_user['login_jaber'] = self::chat_format_login($tmp_user);
     }

     if ($j_user['login_jaber'] && strlen($j_user['login_jaber']) > 1) {
      XMPP::attention(self::chat_format_login($j_user['login_jaber']), $text);
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
    }

/***------------------------------------------------
 * �������� ��������� ��������� ��������� � ���
 * $parm = ����� ���� � ������
 *
 * $parm[0]['login_jaber'] = 'br';     # login_jaber
 * $parm[0]['text']        = '������'; # ����� ���
 *
 **/

    public static function chat_several_attention ($p_data = array(), $parm = array()) {
     if ($p_data) {
      foreach ($p_data as $data) {
        self::chat_attention($data['login_jaber'], $data['text']);
      }
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
    }

/***------------------------------------------
 * ������� ��� � ��������� ���
 *
 * $j_user['room'] = 100; # ����� ������� �����
 * $text = ����� ���
 * $room = ������������� ����� ������� �����
 * $parm = �������
 *
 * CHAT::chat_system($user_my->user, "������", 0, array('addXMPP' => true));
 *
 **/

	public static function chat_system($j_user = array(), $text, $room = 0, $parm = array()) {
     if ($text) {
      if ($room == 0) {$room = $j_user['room']; }

      XMPP::system(self::$id_city, $room, $text);
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
	}

/***------------------------------------------
 * ������� �����
 *
 * $j_user['login_jaber']; # login_jaber
 * $blind = ��������
 *
 * CHAT::chat_user_changeBlind($user_my->user, 1, array('addXMPP' => true));
 *
 **/

	public static function chat_user_changeBlind($j_user = array(), $blind = 0, $parm = array()) {
     $tmp_user = $j_user;

     if(!is_array($j_user) || empty($j_user['login_jaber']) || strlen($j_user['login_jaber']) <= 1) {
       $j_user = array();
       $j_user['login_jaber'] = self::chat_format_login($tmp_user);
     }

     if ($j_user['login_jaber'] && strlen($j_user['login_jaber']) > 1) {
      XMPP::changeBlind($j_user['login_jaber'], $blind);
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
	}

/***------------------------------------------
 * ������� �����
 *
 * $j_user['login_jaber']; # login_jaber
 * $silence = ��������
 *
 * CHAT::chat_user_changeBlind($user_my->user, 1, array('addXMPP' => true));
 *
 **/

	public static function chat_user_changeSilence($j_user = array(), $silence = 0, $parm = array()) {
     $tmp_user = $j_user;

     if(empty($j_user['login_jaber']) || strlen($j_user['login_jaber']) <= 1) {
       $j_user = array();
       $j_user['login_jaber'] = self::chat_format_login($tmp_user);
     }

     if ($j_user['login_jaber'] && strlen($j_user['login_jaber']) > 1) {
      XMPP::changeSilence($j_user['login_jaber'], $silence);
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
	}

/***------------------------------------------
 * ������� ������ - ������ � ��� ���
 *
 * $bat_id = 122332102; #  �� ���
 *
 * CHAT::chat_battle_c_glb(111111111, array('addXMPP' => true));
 *
 **/

	public static function chat_battle_c_glb($bat_id, $parm = array()) {

      $joinuser = null;

      if ($parm['joinuser']) {
       $joinuser = array();
       $joinuser = $parm['joinuser'];
      }

      XMPP::battleRoomManager(self::$id_city, $bat_id, 1, $joinuser);

      XMPP::groupRoomManager(self::$id_city, $bat_id, 1, 1);
      XMPP::groupRoomManager(self::$id_city, $bat_id, 2, 1);

      if ($joinuser) {
        foreach ($joinuser as $j_l) {
          if (isset($j_l['j_login'])) {
           XMPP::joinChannel($j_l['j_login'], self::$id_city, $bat_id);
           XMPP::joinChannel($j_l['j_login'], self::$id_city, $bat_id, $j_l['side']);
          }
        }
      }

      if ($parm['addXMPP']) { self::chat_addXMPP(); }
	}

/***------------------------------------------
 * ������� ������ - ������ � ��� ���
 *
 * $j_user = $j_user['login_jaber'] OR $j_user = ['br', '�����'] # ��� ��� ������ �����
 * $bat_id = 122332102;                                          # �� ���
 *
 * CHAT::chat_battle_c_glb(111111111, array('addXMPP' => true));
 *
 **/

	public static function chat_battle_jc_glb($j_user = array(), $bat_id, $parm = array()) {

     $tmp_user = $j_user;

     if(empty($j_user['login_jaber']) || strlen($j_user['login_jaber']) <= 1) {
       $j_user = array();
       $j_user['login_jaber'] = self::chat_format_login($tmp_user);
     }

     if ($j_user['login_jaber'] && strlen($j_user['login_jaber']) > 1) {
      XMPP::joinChannel($j_user['login_jaber'], self::$id_city, $bat_id);
      if ($parm['side'])    { XMPP::joinChannel($j_user['login_jaber'], self::$id_city, $bat_id, $parm['side']); }
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
     }
	}

/***------------------------------------------
 * ������� ������ - ������ � ��� ���
 *
 * $j_user = $j_user['login_jaber'] OR $j_user = ['br', '�����'] # ��� ��� ������ �����
 * $bat_id = 122332102;                                          # �� ���
 *
 * CHAT::chat_battle_c_glb(111111111, array('addXMPP' => true));
 *
 **/

	public static function chat_battle_del_glb($bat_id, $parm = array()) {
      XMPP::roomManager(self::$id_city . '_' . $my_dungeon_id, 2);
      if ($parm['addXMPP']) { self::chat_addXMPP(); }
	}

/***------------------------------------------
 * �������� ����� � ��� ���
 *
 * $text = "dsfdsf" # ��� ��� ������ �����
 * $bat_id = 122332102;                                          # �� ���
 *
 * CHAT::chat_battle_add_lb("fghfgh", 345345345, array('addXMPP' => true));
 *
 **/

	public static function chat_battle_add_lb($text, $bat_id, $parm = array()) {

      XMPP::battleLog(self::$id_city, $bat_id, $text);

      if ($parm['addXMPP']) { self::chat_addXMPP(); }
	}

/***------------------------------------------
 * ������� ������ ������������
 *
 * $jlogin = "BR TR" => "br_tr" # ������ ����� � ������ ������������� ������� �������� �� "_"
 * $jpass  = 6-10 ��������
 * $rlogin = �������� �����
 * $remail = ���� ������������ ���� ���� - �� �����������
 *
 **/

	public static function chat_create_juser($jlogin, $jpass, $rlogin, $remail = null, $parm = array()) {
      if ($jlogin && $jpass && $rlogin) {

        XMPP::newUser($jlogin, $jpass, $rlogin);

        if ($parm['addXMPP']) { self::chat_addXMPP(); }

        $options = "options|smilesOff=0|groupChannelOff=0|pokerChannelOff=0|clanChannelOff=0|messageStyle=color:#000000|autoscrollOff=0|systemChannelOff=0|tradeChannelOff=1|battleLogChannelOff=1|antimatOff=0|";
        sql_query("UPDATE `users` SET `login_jaber` = '".$jlogin."', `pass_jaber` = '".$jpass."', `chat_command_options` = '".$options."' WHERE `login` = '".$rlogin."' LIMIT 1;");

        return true;

      } else return false;
	}


/***------------------------------------------
 * ������� ������ ������������
 *
 * $jlogin = "BR TR" => "br_tr" # ������ ����� � ������ ������������� ������� �������� �� "_"
 * $jpass  = 6-10 ��������
 * $rlogin = �������� �����    - "-1" => ������� �����
 * $remail = ���� ������������ - "-1" => ������� ����
 *
 **/

	public static function chat_edit_juser($jlogin, $jpass = null, $rlogin = null, $remail = null, $parm = array()) {
      if ($jlogin && $jpass && $rlogin) {

        XMPP::editUser($jlogin, $jpass, $rlogin, $remail);

        if ($parm['addXMPP']) { self::chat_addXMPP(); }

        if ($jpass != null) sql_query("UPDATE `users` SET `pass_jaber` = '".$jpass."' WHERE `login_jaber` = '".$jlogin."' LIMIT 1;");

        return true;

      } else return false;
	}

/***----------------------------------------------------
 * ������� ������ � js �����
 **/

	public static function chat_js_login($login) {
      if (isset($login['invis']) && $login['invis'] == 1) $login = 'hidden';
      if (isset($login['login'])) $login = $login['login'];
      return "{[".$login."]}";
	}

/***----------------------------------------------------
 * ������� ������ � ������ ����� "BR TR" => "br_tr"
 **/

	public static function chat_format_login($login) {
      return str_replace(' ', '_', @mb_strtolower(str_replace('"', '', $login), "CP1251"));
	}

/***----------------------------------------------------
 * �������� ������ ��������
 **/

    public static function chat__hasBadChars( $text ) {
    	if (preg_match_all("/^[A-Za-z0-9�-��-���~`!@#\$%\^&\*\(\)\"�;:\?\-_\+=\/\\\|\s\[\]\{\}<>\'\.,\s]+$/", $text)) {
    		return true;
    	} else {
    		return false;
    	}
    }

/***----------------------------------------------------
 * ������ ������
 **/

    public static function filterCommandText( $text ) {
      $text = str_replace('=', '&amp;#61;', $text);
      $text = str_replace('<', '{{', $text);
      $text = str_replace('>', '}}', $text);
      $text = str_replace('"', '&amp;quot;', $text);
  	return $text;
    }

/***------------------------------------------
 * ������� ������� ������
 **/

	public static function chat_addXMPP() {
	  $r_xml = XMPP::xml();

      Cache::addXml($r_xml);

	  /*if (self::chat__hasBadChars($r_xml)) {
          Cache::addXml($r_xml);
	  } else {
    	  Cache::redis()->rpush('err', $r_xml);
	  }*/

      return $r_xml;
	}

}

CHAT::init();

/*
XMPP::changeUsername('test_5', 'test_5_123');


 */

?>