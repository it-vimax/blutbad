<?

/***-----------------------------
 * ������������ ��������� ����� �
 * ��������:
 *  "�dmin ion.gif" � "adminion.gif"
 **/

if(!function_exists('str_to_spacelower')){
 function str_to_spacelower($text) {
   $text = str_replace(" ", '', $text);
   return strtolower(stripslashes($text));
 }
}

/***------------------------------------------
 * ��������� ������������ ��������� ������
 **/

if(!function_exists('isSerialized')){
  function isSerialized($str) {
   return ($str == serialize(false) || @unserialize($str) !== false);
  }
}

/***----------------------------------------------------
 * �������� ��� ��� � ������� ������ � ������ ���� ���
 **/

function BATTLE_type_name($type) {
    $batletype = '';
    $strblood = '';

    switch ($type) {
    	case 1:  $batletype = '�����';                         	  break;
    	case 2:  $batletype = '������������� ���';	              break;
      	case 3:case 5: $batletype = 'X�������� ���';	          break;
      	case 4:  $batletype = '��������� ���';	                  break;
    	case 6:  $batletype = '���������';	                      break;
    	case 7:  $batletype = '����� � �����';	                  break;
        case 8:  $batletype = '�������� ���';	                  break;
        case 9:  $batletype = '��� c ��������';	                  break;
        case 10: $batletype = '��� � ����� ������ (��� ������)';  break;
        case 11: $batletype = '��� c �������� (��� ������)';	  break;
        case 12: $batletype = '����� �����������';	              break;
      	case 13: $batletype = '��� � �������';	                  break;
      	case 14: $batletype = '������� ��������';	              break;
        case 15: $batletype = '��� � ��������� ��������';	      break;
        case 16: $batletype = '��� c ����� ������';	              break;
      	case 17: $batletype = '����������� ��������';	          break;
      	case 18: $batletype = '���������� �������';	              break;
      	case 19: $batletype = '��� �� �����';	                  break;
      	case 20: $batletype = '���������';	                      break;
    	case 21: $batletype = '�������';	                      break;
    	case 22: $batletype = '����� �� ��';	                  break;
    	case 23: $batletype = '��� � ��������� ��������';	      break;
    	case 24: $batletype = '������ �����';                  	  break;
    	case 25: $batletype = '��������� ����� �����������';      break;
    	case 26: $batletype = '������������ ���';                 break;
    	case 27: $batletype = '��������� ������';                 break;
    	case 28: $batletype = '�������� �������';                 break;
    	case 29: $batletype = '��������� �����';                  break;
    	case 30: $batletype = '����� �����';                      break;
    	case 31: $batletype = '��� � ��������';                   break;
    }

  return $batletype;
}

/***----------------------------------------------------
 * �������� ��� ��� � ������� ������ � ������ ���� ���
 **/

function BATTLE_type_img($type, $blood = 0, $isolated = 0) {
  $batletype = ' ';
  if ($blood) { $strblood = ' (��� ������)'; } else { $strblood = ''; }
  if ($isolated) { $strisolated = ' isolated'; } else { $strisolated = ''; }   # ��� ����������

    switch ($type) {
    	case 1: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="�����'.$strblood.'">';	break;
    	case 2: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="������������� ���">';	break;
    	case 3:case 5:
                $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"bloodg":"group").'.gif" title="X�������� ���'.$strblood.'">';	break;
    	case 4: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"bloodg":"group").'.gif" title="��������� ���'.$strblood.'">';	break;
    	case 6: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="���������'.$strblood.'">';	break;
    	case 7: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="����� � �����'.$strblood.'">';	break;
      	case 8: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fight.gif" title="�������� ���'.$strblood.'">';	break;
        case 9: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fight.gif" title="��� c ��������'.$strblood.'">';	break;
        case 10: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fight.gif" title="��� � ����� ������'.$strblood.'">';	break;
        case 11: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fightbot.gif" title="��� c ��������'.$strblood.'">';	break;
        case 12: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fight.gif" title="����� �����������'.$strblood.'">';	break;
    	case 13: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fightbot.gif" title="��� � �������'.$strblood.'">';	break;
    	case 14: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fight.gif" title="������� ��������'.$strblood.'">';	break;
        case 15: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fightbot.gif" title="��� � ��������� ��������'.$strblood.'">';	break;
        case 16: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fightbot.gif" title="��� c ����� ������'.$strblood.'">';	break;
    	case 17: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fight.gif" title="����������� ��������'.$strblood.'">';	break;
    	case 18: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"ny201":"ny201").'.gif" title="���������� �������'.$strblood.'">';	break;
    	case 19: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="��� �� �����'.$strblood.'">';	break;
    	case 20: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"bloodg":"group").'.gif" title="���������'.$strblood.'">';	break;
    	case 21: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="�������'.$strblood.'">';	break;
    	case 22: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="����� �� ��'.$strblood.'">';	break;
        case 23: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fightbot.gif" title="��� � ��������� ��������'.$strblood.'">';	break;
        case 24: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/alcohol.gif" title="������ �����'.$strblood.'">';	break;
        case 25: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="��������� ����� �����������'.$strblood.'">';	break;
        case 26: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="������������ ���'.$strblood.'">';	break;
        case 27: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="��������� ������'.$strblood.'">';	break;
        case 28: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"zarnica":"zarnica").'.gif" title="�������� �������'.$strblood.'">';	break;
        case 29: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="��������� �����'.$strblood.'">';	break;
        case 30: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/'.($blood?"fightblood":"fight").'.gif" title="����� �����'.$strblood.'">';	break;
        case 31: $batletype = '<img alt="��� ���" class="tooltip'.$strisolated.'" src="http://img.blutbad.ru/i/fightbot.gif" title="��� � ��������'.$strblood.'">';	break;
    }
  return $batletype;
}
?>