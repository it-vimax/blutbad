<?

function tip_list_rooms($m_room, $a_rooms = array()){
 global $rooms;

 echo '<b>'.$rooms[$m_room].'</b><ul class="minimap-rooms">';

  foreach ($a_rooms as $k => $v) {
   if (in_array($v, array(100, 120, 130, 140, 150))) {
     echo '<li><b>'.$rooms[$v].'</b></li>';
   } else {
     echo '<li>'.$rooms[$v].'</li>';
   }
  }

 echo '</ul>';

}
?>

    <script type="text/javascript">
    	//<![CDATA[
    	$(function() {
    	   	$('<?= ('.hover-minimap img:not([i_seloc="'.$user_my->user['room'].'"])') ?>').hover(
    			function() {
    				this.src = this.src.replace('null.png', '' + $(this).attr('data'));
    			},
    			function() {
    				this.src = this.src.replace('' + $(this).attr('data'), 'null.png');
    			}
    		);
    	});
    	//]]>
    </script>

    <div class="right-col-inner">

      <? if (in_array($user_my->user['room'], array(120, 130, 150, 140)) && $user_my->user['id'] == 1) { /* �������: ����������� �������, ������ �������� �����, �������� �������, �������� ���� */?>

        <div class="hover-minimap" id="minimap" style="background-image: url(http://img.blutbad.ru/i/damask/minmap/map.png);">

           <div class="tooltip-next"><img data="sr_m.png" i_seloc="140" height="58" width="81" <?= ($user_my->user['room'] != 140)?'onclick="sol(140)"':'' ?> style="left: 0px; top: 0px;" src="http://img.blutbad.ru/i/damask/minmap/null.png"></div>
           <div class="invisible"><b>�������� ����</b><ul class="minimap-rooms"><li><b>�������� �������</b></li><li>������</li><li>������� �������</li><li>����� � ���������</li></ul></div>

           <div class="tooltip-next"><img data="central_m.png" i_seloc="120" height="112" width="160" <?= ($user_my->user['room'] != 120)?'onclick="sol(120)"':'' ?> style="left: 0px; top: 48px;" src="http://img.blutbad.ru/i/damask/minmap/null.png"></div>
           <div class="invisible"><b>����������� �������</b><ul class="minimap-rooms"><li><b>�������� �������</b></li><li><b>������ �������� �����</b></li><li>�����</li><li>�������</li><li>������� ����</li><li>���� ������</li><li>����</li><li>������� "������� ����"</li><li>��������� ����������</li><li><b>�����</b></li></ul></div>

           <div class="tooltip-next"><img data="sms_m.png" i_seloc="150" height="56" width="79" <?= ($user_my->user['room'] != 150)?'onclick="sol(150)"':'' ?> style="left: 0px; top: 37px;" src="http://img.blutbad.ru/i/damask/minmap/null.png"></div>
           <div class="invisible"><b>�������� �������</b><ul class="minimap-rooms"><li><b>����������� �������</b></li><li><b>�������� ����</b></li><li>��������� ����</li><li>�����</li><li>������� ������������ �����</li><li>����</li><li>�������</li><li>�������</li><li>������ ���������</li></ul></div>

           <div class="tooltip-next"><img data="ws_m.png" i_seloc="130" height="71" width="78" <?= ($user_my->user['room'] != 130)?'onclick="sol(130)"':'' ?> style="left: 82px; top: 20px;" src="http://img.blutbad.ru/i/damask/minmap/null.png"></div>
           <div class="invisible"><b>������ �������� �����</b><ul class="minimap-rooms"><li><b>����������� �������</b></li><li>��������</li><li>������</li><li>������</li><li>������� ���</li><li>�������</li><li>����� � �����������</li></ul></div>

        </div>
      <? } ?>

      <? if ($user_my->user['room'] == 120) { ?>
        <? if (@$_SESSION['fireworks_cnt']) { ?>
        <div class="map-actions">
		  <img alt="" class="tooltip" id="salute_cmd" onclick="callSalute()" src="http://img.blutbad.ru/i/rune/57.gif" title="��������� ��������� (�������� <?= @$_SESSION['fireworks_cnt'] ?>)">
		  <? /*<img alt="" class="tooltip" id="magic_salute_cmd" onclick="callMagicSalute()" src="http://img.blutbad.ru/i/rune/66.gif" title="��������� ��������� (�������� 0)">*/ ?>
		</div>
        <? } ?>
      	<div class="map-actions">
      		<img alt="" class="tooltip" onclick="commonDialog('', '������� ��', 'name', {cmd: 'attackb', nd: '<?= $user_my->user['nd'] ?>'});" src="http://img.blutbad.ru/i/rune/attack.gif" title="�������">
        	<img alt="" class="tooltip" onclick="commonDialog('', '��������� ��', 'name', {cmd: 'interveneb', nd: '<?= $user_my->user['nd'] ?>'});" src="http://img.blutbad.ru/i/rune/intervention.gif" title="���������">
        	<img alt="" class="tooltip" onclick="location.href='/fountain.php'" src="http://img.blutbad.ru/i/rune/fountain.gif" title="������� � �������">
        </div>
      <? } ?>
      <? if (($user_my->user['room'] == 150)) { ?>
        <div class="map-actions">
    	  <img alt="" class="tooltip" onclick="location.href='/fishing.php'" src="http://img.blutbad.ru/i/rune/fishing.gif" title="������� �� &quot;����� ����&quot;, ����� ������ ����">
        </div>
        <script type="text/javascript">
        	var user_level = '<?= $user_my->user['level'] ?>';
        </script>
      <? } ?>

    	<fieldset class="location-list" id="location_list" style="min-width: 220px;">
    		<legend>�������</legend>

    		<ul id="room_locations_main">
                <? if ($user_my->user['room'] >= 100 && $user_my->user['room'] <= 113) {

                  $count_room  = count_user_room(100);

                ?>
                  <li class="b">
                  <? if($user_my->user['room'] == 100) { ?>

                   <b>&nbsp;&raquo;</b> <span class="tooltip-next"><a href="#" class="menutop map_ch" seloc="120" onclick="return sol(120);"><?= setinloc(120); ?> ����������� �������<?= $count_room['120']; ?></a></span>
                   <div class="invisible"><?= tip_list_rooms(120, array(150, 130, 125, 124, 127, 121, 122, 126, 123)) ?></div>

                  <? } else { ?>
                   <b>&nbsp;&raquo;</b> <a href="#" class="menutop map_ch" seloc="100" onclick="return sol(100);">� �����<?= $count_room['100']; ?></a>
                  <? } ?>
                  </li>
                <?

                 /***------------------------------------------
                  * �������� ����� �� ��� � ��� �������
                  **/

                  function get_user_acess_room($room) {
                   global $user;
                   $rh = 1;
                   $userg = $user;

                      if ($room=='113') { if ($userg['align'] >= 1 && $userg['align'] <= 3) { $rh = 0; } }
                    # � ��� �������
                      if ($room=='112') {
                          if ($userg['align']=='0.96') { $rh = 0; }
                          if ($userg['align']=='0.196') { $rh = 0; }
                          if ($userg['align'] >= 1 && $userg['align'] <= 3) { $rh = 0; }
                      }
                    # � ��� �����
                      if ($room=='111') {
                          if ($userg['align']=='0.97') { $rh = 0; }
                          if ($userg['align']=='0.197') { $rh = 0; }
                          if ($userg['align'] >= 1 && $userg['align'] <= 3) { $rh = 0; }
                      }
                    # � ��� �����
                      if ($room=='110') {
                          if ($userg['align']=='0.99') { $rh = 0; }
                          if ($userg['align']=='0.199') { $rh = 0; }
                          if ($userg['align'] >= 1 && $userg['align'] <= 3) { $rh = 0; }
                      }
                    # � ��� ����
                      if ($room=='109') {
                          if ($userg['align']=='0.98') { $rh = 0; }
                          if ($userg['align']=='0.198') { $rh = 0; }
                          if ($userg['align'] >= 1 && $userg['align'] <= 3) { $rh = 0; }
                      }
                    # � ��� ������ ������ 0-7 �������
                      if ($room=='108') { if ($userg['level'] > 7 ) { $rh = 0; } }
                    # � ��� �������� ������ 0-3 �������
                      if ($room=='107') { if ($userg['level'] > 5 ) { $rh = 0; } }
                    # � ��� ��������� ������ 0-4 �������
                      if ($room=='106') { if ($userg['level'] > 3 ) { $rh = 0; } }
                    # � ��� ����������� ������ 0-2 �������
                      if ($room=='105') { if ($userg['level'] > 1 ) { $rh = 0; } }
                    # � ������� �������� ������ 0-1 �������
                      if ($room=='104') { if ($userg['level'] > 0 ) { $rh = 0; } }
                    # � ������� ����������� ������ 0-1 �������
                      if ($room=='103') { if ($userg['level'] > 0 ) { $rh = 0; } }
                    # ������� ����������� ������ 1-10 �������
                      if ($room=='102') {
                          $is_mentor = sql_number("select id from `inventory` where `in_box` = '0' and `in_p_box` = '0' and `owner` = '".($_SESSION['uid'])."' and `dressed` = 0 and `type` = 189; ");
                          if ($is_mentor ) { $rh = 0; }
                          if ($userg['level'] == 0 ) { $rh = 0; }
                          if ($userg['invis'] == 1 ) { $rh = 0; }
                          if ($userg['align'] >= 1 && $userg['align'] <= 3) { $rh = 0; }
                      }
                  # � ����������� ��� ������ 0-1 �������
                      if ($room=='101') { if($userg['level'] > 0 ) { $rh = 0; } }

                   return $rh;
                  }

                 /***------------------------------------------
                  * ������ ������� � ������ ���� � � ���
                  **/

                  function hide_in_user_room($room){
                   global $user_my;
                   $hider = '';
                   if ($user_my->user['room'] == $room) { $hider = ' style="display: none"'; }
                   echo $hider;
                  }


                ?>

                  <li<?= hide_in_user_room(113) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('113')) { ?>disabled<? } ?>" seloc="113" onclick="return sol(113);"><?= setinloc(113); ?> ����� ���������<?= $count_room['113']; ?></a></li>
                  <li<?= hide_in_user_room(112) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('112')) { ?>disabled<? } ?>" seloc="112" onclick="return sol(112);"><?= setinloc(112); ?> ��� �������<?= $count_room['112']; ?></a></li>
                  <li<?= hide_in_user_room(111) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('111')) { ?>disabled<? } ?>" seloc="111" onclick="return sol(111);"><?= setinloc(111); ?> ��� �����<?= $count_room['111']; ?></a></li>
                  <li<?= hide_in_user_room(110) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('110')) { ?>disabled<? } ?>" seloc="110" onclick="return sol(110);"><?= setinloc(110); ?> ��� �����<?= $count_room['110']; ?></a></li>
                  <li<?= hide_in_user_room(109) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('109')) { ?>disabled<? } ?>" seloc="109" onclick="return sol(109);"><?= setinloc(109); ?> ��� ����<?= $count_room['109']; ?></a></li>
                  <li<?= hide_in_user_room(108) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('108')) { ?>disabled<? } ?>" seloc="108" onclick="return sol(108);"><?= setinloc(108); ?> ��� "��������� ������"<?= $count_room['108']; ?></a></li>
                  <li<?= hide_in_user_room(107) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('107')) { ?>disabled<? } ?>" seloc="107" onclick="return sol(107);"><?= setinloc(107); ?> ��� ������������<?= $count_room['107']; ?></a></li>
                  <li<?= hide_in_user_room(106) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('106')) { ?>disabled<? } ?>" seloc="106" onclick="return sol(106);"><?= setinloc(106); ?> ��� �����������<?= $count_room['106']; ?></a></li>
                  <li<?= hide_in_user_room(105) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('105')) { ?>disabled<? } ?>" seloc="105" onclick="return sol(105);"><?= setinloc(105); ?> ��������� ���<?= $count_room['105']; ?></a></li>
                  <li<?= hide_in_user_room(104) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('104')) { ?>disabled<? } ?>" seloc="104" onclick="return sol(104);"><?= setinloc(104); ?> ������� ��������<?= $count_room['104']; ?></a></li>
                  <li<?= hide_in_user_room(103) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('103')) { ?>disabled<? } ?>" seloc="103" onclick="return sol(103);"><?= setinloc(103); ?> ������� ��������������<?= $count_room['103']; ?></a></li>
                  <li<?= hide_in_user_room(102) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('102')) { ?>disabled<? } ?>" seloc="102" onclick="return sol(102);"><?= setinloc(102); ?> ������������� ���<?= $count_room['102']; ?></a></li>
                  <li<?= hide_in_user_room(101) ?>>&nbsp;&bull; <a href="#" class="<? if (get_user_acess_room('101')) { ?>disabled<? } ?>" seloc="101" onclick="return sol(101);"><?= setinloc(101); ?> ����������� ���<?= $count_room['101']; ?></a></li>

                <? } elseif ($user_my->user['room'] == 120) { # ����������� �������

                  $count_room  = count_user_room(120);

                ?>

                  <li><b>&nbsp;&raquo;</b> <span class="tooltip-next"><a href="#" class="menutop map_ch" seloc="150" onclick="return sol(150);"><?= setinloc(150); ?> �������� �������<?= $count_room['150']; ?></a></span>
                      <div class="invisible"><?= tip_list_rooms(150, array(120, 140, 155, 152, 153, 154, 156, 157, 430)) ?></div>
                  </li>
                  <li><b>&nbsp;&raquo;</b> <span class="tooltip-next"><a href="#" class="menutop map_ch" seloc="130" onclick="return sol(130);"><?= setinloc(130); ?> ������ �������� �����<?= $count_room['130']; ?></a></span>
                      <div class="invisible"><?= tip_list_rooms(130, array(120, 140, 131, 132, 136, 134, 133,138, 139, 410)) ?></div>
                  </li>

                  <li>&nbsp;&bull; <a href="#" class="" seloc="125" onclick="return sol(125);"><?= setinloc(125); ?> �������<?= $count_room['125']; ?></a></li>
                  <? if(CONST_ROOM_TREE) { ?>
                  <li>&nbsp;<a href="#" class="" seloc="128" id="bmo_1" onclick="return sol(128);">&bull; <?= setinloc(128); ?> ���������� ������<?= $count_room['128']; ?></a></li>
                  <? } ?>
                  <? if(CONST_DENSE_FOREST == true) { ?>
                  <li>&nbsp;<a href="#" class="" seloc="129" id="bmo_1" onclick="return sol(129);">&bull; <?= setinloc(129); ?> ������ � ��������� ����<?= $count_room['129']; ?></a></li>
                  <? } ?>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="124" id="bmo_2" onclick="return sol(124);"><?= setinloc(124); ?> ����<?= $count_room['124']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="127" onclick="return sol(127);"><?= setinloc(127); ?> ������� ����<?= $count_room['127']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="121" onclick="return sol(121);"><?= setinloc(121); ?> ���� ������<?= $count_room['121']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="122" onclick="return sol(122);"><?= setinloc(122); ?> ����<?= $count_room['122']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="126" onclick="return sol(126);"><?= setinloc(126); ?> ������� "������� ����"<?= $count_room['126']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="123" onclick="return sol(123);"><?= setinloc(123); ?> ������<?= $count_room['123']; ?></a></li>
                  <li>&nbsp;&bull; <span class="tooltip-next"><a href="#" class="menutop map_ch" seloc="100" onclick="return sol(100);"><?= setinloc(100); ?> �����<?= $count_room['100']; ?></a></span>
                      <div class="invisible"><?= tip_list_rooms(100, array(120, 113, 112, 111, 110, 109, 108, 107, 106, 105, 104, 103, 102, 101)) ?></div>
                  </li>

                    <?
                    /*
                    <script type="text/javascript">
            			//<![CDATA[
            			var so = new SWFObject("http://img.blutbad.ru/i/<?= INCITY ?>/fire3.swf", "summer", "33", "48", "9", "");
            			so.addParam("wmode", "transparent");
            			so.write("swf1");
            			//]]>
            		</script>
                    */
                    ?>

                <? } elseif ($user_my->user['room'] == 130) { # ������ �������� �����

                  $count_room  = count_user_room(130);

                ?>

                  <li><b>&nbsp;&raquo;</b> <span class="tooltip-next"><a href="#" class="menutop map_ch" seloc="120" onclick="return sol(120);"><?= setinloc(120); ?> ����������� �������<?= $count_room['120']; ?></a></span>
                      <div class="invisible"><?= tip_list_rooms(120, array(150, 130, 125, 124, 127, 121, 122, 126, 123)) ?></div>
                  </li>
                  <li><b>&nbsp;&raquo;</b> <span class="tooltip-next"><a href="#" class="menutop map_ch" seloc="140" onclick="return sol(140);"><?= setinloc(140); ?> �������� ����<?= $count_room['140']; ?></a></span>
                      <div class="invisible"><?= tip_list_rooms(140, array(130, 141, 142, 440)) ?></div>
                  </li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="131" onclick="return sol(131);"><?= setinloc(131); ?> �����<?= $count_room['131']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="132" onclick="return sol(132);"><?= setinloc(132); ?> ��������� ����������<?= $count_room['132']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="136" onclick="return sol(136);"><?= setinloc(136); ?> �������<?= $count_room['136']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="134" onclick="return sol(134);"><?= setinloc(134); ?> ������� ���<?= $count_room['134']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="133" onclick="return sol(133);"><?= setinloc(133); ?> �������<?= $count_room['133']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="138" onclick="return sol(138);"><?= setinloc(138); ?> ������� �������<?= $count_room['138']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="139" onclick="return sol(139);"><?= setinloc(139); ?> �����<?= $count_room['139']; ?></a></li>

                  <? if (CONST_ZARNICA) { ?>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="137" onclick="return sol(137);"><?= setinloc(137); ?> ���� �������<?= $count_room['137']; ?></a></li>
                  <? } ?>
                  <? if ($user_my->user['klan'] == "Test") { ?>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="410" onclick="return sol(410);">����� � �����������<?= $count_room['410']; ?></a></li>
                  <? } ?>

                <? } elseif ($user_my->user['room'] == 150) { # �������� �������

                  $count_room  = count_user_room(150);

                ?>

                  <li><b>&nbsp;&raquo;</b> <span class="tooltip-next"><a href="#" class="menutop map_ch" seloc="120" onclick="return sol(120);"><?= setinloc(120); ?> ����������� �������<?= $count_room['120']; ?></a></span>
                      <div class="invisible"><?= tip_list_rooms(120, array(140, 130, 125, 124, 127, 121, 122, 126, 123)) ?></div>
                  </li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="155" onclick="return sol(155);"><?= setinloc(155); ?> ��������<?= $count_room['155']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="152" onclick="return sol(152);"><?= setinloc(152); ?> ��������� ����<?= $count_room['152']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="153" onclick="return sol(153);"><?= setinloc(153); ?> �����<?= $count_room['153']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="154" onclick="return sol(154);"><?= setinloc(154); ?> ������� ������������ �����<?= $count_room['154']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="156" onclick="return sol(156);"><?= setinloc(156); ?> ������<?= $count_room['156']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="157" onclick="return sol(157);"><?= setinloc(157); ?> �������<?= $count_room['157']; ?></a></li>
                  <? if (CONST_ZARNICA) { ?>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="158" onclick="return sol(158);"><?= setinloc(158); ?> ���� ������<?= $count_room['158']; ?></a></li>
                  <? } ?>
                  <? if ($user_my->user['klan'] == "Test") { ?>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="420" onclick="return sol(420);"><?= setinloc(420); ?> ������ ���������<?= $count_room['420']; ?></a></li>
                  <? } ?>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="430" onclick="return sol(430);"><?= setinloc(430); ?> ����� ��������<?= $count_room['430']; ?></a></li>

                <? } elseif ($user_my->user['room'] == 140) { # �������� ����

                  $count_room  = count_user_room(140);

                ?>

                  <li><b>&nbsp;&raquo;</b> <span class="tooltip-next"><a href="#" class="menutop map_ch" seloc="130" onclick="return sol(130);"><?= setinloc(130); ?> ������ �������� �����<?= $count_room['130']; ?></a></span>
                      <div class="invisible"><?= tip_list_rooms(130, array(120, 140, 131, 132, 136, 134, 133, 410)) ?></div>
                  </li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="141" onclick="return sol(141);"><?= setinloc(141); ?> ������<?= $count_room['141']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="142" onclick="return sol(142);"><?= setinloc(142); ?> ������� �������<?= $count_room['142']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="440" onclick="return sol(440);">����� � ���������<?= $count_room['440']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="460" onclick="return sol(460);">����� � ��������� �����<?= $count_room['460']; ?></a></li>
                  <li>&nbsp;&bull; <a href="#" class="" seloc="450" onclick="return sol(450);">���� � �����<?= $count_room['450']; ?></a></li>

                <? } ?>

    		</ul>

            <table id="room_delay" style="">
            	<tbody>
             		<tr>
             			<td rowspan="3"><img alt="" height="15"  src="http://img.blutbad.ru/i/nav/left.gif" width="16"></td>
            			<td><img alt="" height="3" src="http://img.blutbad.ru/i/nav/top.gif" width="128"></td>
            			<td rowspan="3"><img alt="" height="15" src="http://img.blutbad.ru/i/nav/right.gif" width="16"></td>
            		</tr>
            		<tr>
            			<td class="progress-col" height="9" style="background-image: url(http://img.blutbad.ru/i/nav/bg.gif); text-align: left;"><img alt="" height="9" id="timedelay" src="http://img.blutbad.ru/i/nav/line.gif" width="128"></td>
            		</tr>
             		<tr>
            			<td><img alt="" height="3" src="http://img.blutbad.ru/i/nav/down.gif" width="128"></td>
            		</tr>
            	</tbody>
            </table>

             <?

             if(@$_SESSION['perehod'] > 0 && @$_SESSION['perehod'] > time()){
               $vrme = $_SESSION['perehod'] - time();
              } else {
               $vrme = 0;
               $_SESSION['perehod'] = 0;
              }

            ?>

            <script type="text/javascript">
            	//<![CDATA[
            	var stop_time = <?= $vrme ?>;
            	var max_stop_time = stop_time;
            	var StopTimerID = -1;

            	function CheckMove(){
            		var img = $('#timedelay')[0];
            		var ret = $('#return')[0];

            		if (StopTimerID >= 0) {
            			clearTimeout(StopTimerID);
            		}

            		if (stop_time > 0) {
            			stop_time = stop_time - 0.1;
            			StopTimerID = setTimeout('CheckMove()', 100);
            			var width = ((max_stop_time - stop_time) / max_stop_time) * 128;

            			if (width > 128) {
            				width = 128;
            			}

            			img.width = width;

            			var delayTooltip = $('#room-delay-elapsed-time');
            			if(!delayTooltip.text()) {
            				delayTooltip.countdown({
            					format: 'hms',
            					layout: '{h<}{hn} {hl} {h>}{m<}{mn} {ml} {m>}{sn} {sl}',
            					onExpiry: function() {
            						setTimeout(function() {
            							$('#tooltip').hide();
            						}, 500);
            					},
            					until: +Math.ceil(stop_time)
            				});
            			}

            			if (ret) {
            				ret.disabled = true;
            			}
            		} else if (stop_time == 0) {
            			img.width = 128;

            			if (ret) {
            				ret.disabled = false;
            			}
            		} else if (ret) {
            			ret.disabled = false;
            		}

            		if (stop_time <= 0) {
            			var roomDelay = $('#room_delay');
            			roomDelay.hide();

            			var roomLocations = $('#room_locations');
            			if (roomLocations.length) {
            				roomLocations.show();
            			}

            			$('#room_locations_main').show();
            		}
            	}

            	CheckMove();
               /* RefrseshNPC(); */

            	$(function() {
            		$('#room_delay .progress-col').tooltip($.extend({}, tooltipDefaults, {
            			bodyHandler: function(){
            				return '<span class="nowrap">�������� ���: <span id="room-delay-elapsed-time"></span></span>';
            			}
            		}));
            	});
            	//]]>
            </script>   
    	</fieldset>
    </div>