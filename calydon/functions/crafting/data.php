<?

  # ������� ��������
    $pos_rudnic = array(
      "5_4"    => array("name" => "�������� ������", "name_orig" => "�����", "type" => "coal", "img" => "obj_res_coal.png"),   # 4-4
      "9_20"   => array("name" => "�������� ������", "name_orig" => "�����", "type" => "coal", "img" => "obj_res_coal.png"),   # 9-19
      "28_13"  => array("name" => "�������� ������", "name_orig" => "�����", "type" => "coal", "img" => "obj_res_coal.png"),   # 28-12
      "32_24"  => array("name" => "�������� ������", "name_orig" => "�����", "type" => "coal", "img" => "obj_res_coal.png"),   # 32-23

      "4_9"    => array("name" => "������ ������", "name_orig" => "����", "type" => "copper", "img" => "obj_res_copper.png"),     # 4-10
      "15_19"  => array("name" => "������ ������", "name_orig" => "����", "type" => "copper", "img" => "obj_res_copper.png"),     # 15-20
      "25_18"  => array("name" => "������ ������", "name_orig" => "����", "type" => "copper", "img" => "obj_res_copper.png"),     # 26-18
      "27_14"  => array("name" => "������ ������", "name_orig" => "����", "type" => "copper", "img" => "obj_res_copper.png"),     # 27-15
      "29_6"   => array("name" => "������ ������", "name_orig" => "����", "type" => "copper", "img" => "obj_res_copper.png"),     # 29-7
      "26_28"  => array("name" => "������ ������", "name_orig" => "����", "type" => "copper", "img" => "obj_res_copper.png"),     # 26-27
      "31_33"  => array("name" => "������ ������", "name_orig" => "����", "type" => "copper", "img" => "obj_res_copper.png"),     # 31-34

      "15_4"   => array("name" => "���������� ������", "name_orig" => "�������", "type" => "silver", "img" => "obj_res_silver.png"), # 16-4
      "4_21"   => array("name" => "���������� ������", "name_orig" => "�������", "type" => "silver", "img" => "obj_res_silver.png"), # 4-20
      "24_19"  => array("name" => "���������� ������", "name_orig" => "�������", "type" => "silver", "img" => "obj_res_silver.png"), # 24-20
      "31_11"  => array("name" => "���������� ������", "name_orig" => "�������", "type" => "silver", "img" => "obj_res_silver.png"), # 31-12
      "12_35"  => array("name" => "���������� ������", "name_orig" => "�������", "type" => "silver", "img" => "obj_res_silver.png"), # 12-34

      "5_16"   => array("name" => "���������� ������", "name_orig" => "�������", "type" => "platinum", "img" => "obj_res_platinum.png"), # 4-16
      "25_33"  => array("name" => "���������� ������", "name_orig" => "�������", "type" => "platinum", "img" => "obj_res_platinum.png"), # 25-34
      "11_29"  => array("name" => "���������� ������", "name_orig" => "�������", "type" => "platinum", "img" => "obj_res_platinum.png"), # 12-29
      "13_22"  => array("name" => "���������� ������", "name_orig" => "�������", "type" => "platinum", "img" => "obj_res_platinum.png"), # 12-22

      "20_4"   => array("name" => "������� ������", "name_orig" => "������", "type" => "gold", "img" => "obj_res_gold.png"),    # 20-5
      "7_35"   => array("name" => "������� ������", "name_orig" => "������", "type" => "gold", "img" => "obj_res_gold.png"),    # 6-35
      "21_37"  => array("name" => "������� ������", "name_orig" => "������", "type" => "gold", "img" => "obj_res_gold.png"),    # 22-37
      "30_18"  => array("name" => "������� ������", "name_orig" => "������", "type" => "gold", "img" => "obj_res_gold.png"),    # 31-18

      "16_29"  => array("name" => "��������", "name_orig" => "��������", "type" => "forge", "img" => "obj_forge.png"),          # 15-29
    );

  # ������� ��������
    $craf_res_tools = array(
    # �������� ������
      "coal_1"  => array("gr" => 0.01, "time_end" => 1,  "skill" => 0),       #
      "coal_2"  => array("gr" => 0.05, "time_end" => 5,  "skill" => 5),       #
      "coal_3"  => array("gr" => 0.10, "time_end" => 10, "skill" => 10),      #
      "coal_4"  => array("gr" => 0.30, "time_end" => 30, "skill" => 30),      #
      "coal_5"  => array("gr" => 0.55, "time_end" => 55, "skill" => 55),      #
      "coal_6"  => array("gr" => 0.99, "time_end" => 105, "skill" => 99),     #
      "coal_7"  => array("gr" => 1.55, "time_end" => 150, "skill" => 155),    #

    # ������ ������
      "copper_1"  => array("gr" => 0.01, "time_end" => 2,  "skill" => 57),    #
      "copper_2"  => array("gr" => 0.05, "time_end" => 6,  "skill" => 61),    #
      "copper_3"  => array("gr" => 0.10, "time_end" => 12, "skill" => 67),    #
      "copper_4"  => array("gr" => 0.30, "time_end" => 36, "skill" => 91),    #
      "copper_5"  => array("gr" => 0.55, "time_end" => 66, "skill" => 121),   #
      "copper_6"  => array("gr" => 0.99, "time_end" => 110, "skill" => 260),   #
      "copper_7"  => array("gr" => 1.55, "time_end" => 170, "skill" => 300),   #

    # ���������� ������
      "silver_1"  => array("gr" => 0.01, "time_end" => 3,  "skill" => 124),    #
      "silver_2"  => array("gr" => 0.05, "time_end" => 7,  "skill" => 128),    #
      "silver_3"  => array("gr" => 0.10, "time_end" => 13, "skill" => 137),   #
      "silver_4"  => array("gr" => 0.30, "time_end" => 39, "skill" => 163),   #
      "silver_5"  => array("gr" => 0.55, "time_end" => 72, "skill" => 194),   #
      "silver_6"  => array("gr" => 0.99, "time_end" => 120, "skill" => 435),   #
      "silver_7"  => array("gr" => 1.55, "time_end" => 190, "skill" => 475),   #

    # ���������� ������
      "platinum_1"  => array("gr" => 0.01, "time_end" => 4,  "skill" => 198),    #
      "platinum_2"  => array("gr" => 0.05, "time_end" => 8,  "skill" => 206),    #
      "platinum_3"  => array("gr" => 0.10, "time_end" => 14, "skill" => 220),   #
      "platinum_4"  => array("gr" => 0.30, "time_end" => 42, "skill" => 240),   #
      "platinum_5"  => array("gr" => 0.55, "time_end" => 77, "skill" => 275),   #
      "platinum_6"  => array("gr" => 0.99, "time_end" => 125, "skill" => 590),   #
      "platinum_7"  => array("gr" => 1.55, "time_end" => 200, "skill" => 630),   #

    # ������� ������
      "gold_1"  => array("gr" => 0.01, "time_end" => 5,  "skill" => 280),    #
      "gold_2"  => array("gr" => 0.05, "time_end" => 9,  "skill" => 289),    #
      "gold_3"  => array("gr" => 0.10, "time_end" => 15, "skill" => 295),   #
      "gold_4"  => array("gr" => 0.30, "time_end" => 45, "skill" => 325),   #
      "gold_5"  => array("gr" => 0.55, "time_end" => 83, "skill" => 363),   #
      "gold_6"  => array("gr" => 0.99, "time_end" => 135, "skill" => 745),   #
      "gold_7"  => array("gr" => 1.55, "time_end" => 210, "skill" => 800),   #

    # �������� ������
      "forge_1"  => array("gr" => 0.01, "time_end" => 5,  "skill" => 280),   #
      "forge_2"  => array("gr" => 0.05, "time_end" => 9,  "skill" => 289),   #
      "forge_3"  => array("gr" => 0.10, "time_end" => 15, "skill" => 295),   #
      "forge_4"  => array("gr" => 0.30, "time_end" => 45, "skill" => 325),   #
      "forge_5"  => array("gr" => 0.55, "time_end" => 83, "skill" => 363),   #
      "forge_6"  => array("gr" => 0.99, "time_end" => 135, "skill" => 363),   #
      "forge_7"  => array("gr" => 1.55, "time_end" => 210, "skill" => 363),   #
    );

# ����� ����� � �� �������
  $data_kirka = array(
    '����� - ������� ������'    => array("coal", "copper"),
    '����� - ������� ����������' => array("coal", "copper", "silver"),
    '����� - ������� �������'    => array("coal", "copper", "silver", "platinum", "gold"),
  );

  # ����� ���������
    $new_mine_resources = array(
       "coal"      => array("val" => "0.00"),
       "copper"    => array("val" => "0.00"),
       "silver"    => array("val" => "0.00"),
       "platinum"  => array("val" => "0.00"),
       "gold"      => array("val" => "0.00"),

       "skill"     => array("val" => "0"),    # ������ �������
       "time_mine" => array("maxtime" => "604800", "time" => "0", "starttime" => time(), "endtime" => time() + 30 * 24 * 60 * 60), # ����� ������ 168 ����� (7 ����) �� �����
    );

  # ������ ��� ���������
    $crafting_data = array(
       'coal'     => array('m_cr' => 2,  'max_cr' => 20,  'coal' => 10, 'copper' => 0,  'silver' => 0,  'platinum' => 0,  'gold' => 0),
       'copper'   => array('m_cr' => 4,  'max_cr' => 40,  'coal' => 15, 'copper' => 10, 'silver' => 0,  'platinum' => 0,  'gold' => 0),
       'silver'   => array('m_cr' => 6,  'max_cr' => 60,  'coal' => 20, 'copper' => 15, 'silver' => 10, 'platinum' => 0,  'gold' => 0),
       'platinum' => array('m_cr' => 8,  'max_cr' => 80,  'coal' => 25, 'copper' => 20, 'silver' => 15, 'platinum' => 10, 'gold' => 0),
       'gold'     => array('m_cr' => 10, 'max_cr' => 100, 'coal' => 30, 'copper' => 25, 'silver' => 20, 'platinum' => 15, 'gold' => 10),
    );

  # ������ ��� ��������� ������
    $crafting_stone_data = array(
       'gold'     => array('m_cr' => 0, 'max_cr' => 0, 'coal' => 10, 'copper' => 20, 'silver' => 30, 'platinum' => 40, 'gold' => 50),
    );

  # ��� ���������
    $mine_res_name = array(
       "coal"     => "�����",
       "copper"   => "����",
       "silver"   => "�������",
       "platinum" => "�������",
       "gold"     => "������",
    );

?>