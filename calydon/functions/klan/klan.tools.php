<? if ($user['id'] == $klan['glava']) { ?>

	<div class="settings">

		<fieldset class="cups-exchange">
			<legend>�������� ������</legend>

			<b>�����:</b>
			<b><?= $klan['cup'] ?></b>
			<img alt="�����" onclick="big('misc', 'trovei')" src="http://img.blutbad.ru/i/trovei.gif" style="cursor: pointer;" title="�����">
			<br><br>
			<form action="klan.php" name="change_cups_form" method="post" onsubmit="return confirm_change_cups();">
				<input name="nd" type="hidden" value="<?= $user['nd'] ?>">
				<input name="cmd" type="hidden" value="change.cups">

				<label for="exchange-cups">�������� ������</label>
				<select id="exchange-cups" name="change_cups">
					<option title="���������� ���������� ��������� ���� ��� ������ ����� ������� � ����" value="lim">��������� ����� � <?= $klan_user_max ?> �� <?= ($klan_user_max + 1) ?> (<?= $klan_monlim_user[($klan_user_max + 1)][0] ?> ��.)</option>
					<script type="text/javascript">abilities.push({ code: "lim", price: <?= $klan_monlim_user[($klan_user_max + 1)][0] ?>, currency: "dinar" });</script>

					<option title="���������� ���������� ��������� ���� �� ������ �����" value="a_lim">��������� ������ � <?= $klan['relicts_max_active'] ?> �� <?= ($klan['relicts_max_active'] + 1) ?> (50 ���.)</option>
					<script type="text/javascript">abilities.push({ code: "a_lim", price: 50, currency: "sterling" });</script>

                    <? if (empty($klan_settings['repair'])) { ?>
				    <option title="��������� ������� ������� ����� ������������� ���������� �������� �� ���� ����� ����� (��� ������� ��������� ���� ��������� ����������� ������� ��������� � �������� ����)." value="ability_repair">������ �� ���� ����� (1000.00 ��.)</option>
					<script type="text/javascript">abilities.push({ code: "ability_repair", price: 1000.00, currency: "dinar" });</script>
                    <? } ?>

                    <? if (empty($klan_settings['recovery'])) { ?>
			        <option title="��������� ������� ������� ����� ��������������� ������ ����� � ������������ �� ���� ����� ����� (��� ������� ��������� ���� ��������� ����������� ������� � �������� ����)." value="ability_recovery">������� �� ���� ����� (1000.00 ��.)</option>
					<script type="text/javascript">abilities.push({ code: "ability_recovery", price: 1000.00, currency: "dinar" });</script>
                    <? } ?>

                    <? if (empty($klan_settings['accept_exclude'])) { ?>
				    <option title="����� ��������� ������ �������� ����������� ��������� � ���� � ��������� �� ���� �� ���� �����." value="ability_accept_exclude">����� � ���������� �� ���� ����� (1000.00 ��.)</option>
					<script type="text/javascript">abilities.push({ code: "ability_accept_exclude", price: 1000.00, currency: "dinar" });</script>
                    <? } ?>

                    <? if (empty($klan_settings['hall'])) { ?>
				    <option title="��������� ��������� ����, ������������ ����������� ���������� ������� � ������� ����� ��� �������� �������; ����������� ������ �������� ������, ������� � �������." value="ability_hall" <?= ($klan['clanlevel'] < 2)?"disabled=\"disabled\"":"" ?>>�������� ��� (300.00 ��. + 30.00 ��. ���������)<?= ($klan['clanlevel'] < 2)?" - ��������� ������� ����� 2":"" ?></option>
					<script type="text/javascript">abilities.push({ code: "ability_hall", price: 300.00, currency: "dinar" });</script>
                    <? } ?>

                    <? if (empty($klan_settings['chest'])) { ?>
					<option title="������ � �������� ���� ��������� �����, � ������� ��� ������ ����� ������ ������� ���� ����." value="ability_chest" <?= ($klan['clanlevel'] < 2)?"disabled=\"disabled\"":"" ?>>��������� (50.00 ��. + 5.00 ��. ���������)<?= ($klan['clanlevel'] < 2)?" - ��������� ������� ����� 2":"" ?></option>
					<script type="text/javascript">abilities.push({ code: "ability_chest", price: 50.00, currency: "dinar" });</script>
                    <? } ?>

                    <? if (empty($klan_settings['shop'])) { ?>
			        <option title="������ � �������� ���� ��������� ��������, � ������� ��� ������ ����� ������ �������� ����." value="ability_shop" <?= ($klan['clanlevel'] < 3)?"disabled=\"disabled\"":"" ?>>�������� ������� (1000.00 ��.)<?= ($klan['clanlevel'] < 3)?" - ��������� ������� ����� 3":"" ?></option>
					<script type="text/javascript">abilities.push({ code: "ability_shop", price: 1000.00, currency: "dinar" });</script>
                    <? } ?>

                    <? if (empty($klan_settings['locator'])) { ?>
				    <option title="��������� ������ ��������� ��������, ������������ ���������� � ����������� �� ��������� ��������� ��� ���." value="ability_locator" <?= ($klan['clanlevel'] < 5)?"disabled=\"disabled\"":"" ?>>������������� �������� (1000.00 ��.)<?= ($klan['clanlevel'] < 5)?" - ��������� ������� ����� 5":"" ?></option>
					<script type="text/javascript">abilities.push({ code: "ability_locator", price: 1000.00, currency: "dinar" });</script>
                    <? } ?>

                    <? if (empty($klan_settings['buff'])) { ?>
					<option title="������ � �������� ���� ������ �����, �������������� ��� ��������� � ���� ��������� ��� (�� ����, ��� ���� ��� � 3 ����)." value="ability_buff" <?= ($klan['clanlevel'] < 4)?"disabled=\"disabled\"":"" ?>>����� (200.00 ��. + 20.00 ��. ���������)<?= ($klan['clanlevel'] < 4)?" - ��������� ������� ����� 4":"" ?></option>
					<script type="text/javascript">abilities.push({ code: "ability_buff", price: 200.00, currency: "dinar" });</script>
                    <? } ?>

                    <? if (empty($klan_settings['pond'])) { ?>
					<option title="������ � �������� ���� �������, � ������� ��� ������ ����� ������ ������ ����." value="ability_pond" <?= ($klan['clanlevel'] < 5)?"disabled=\"disabled\"":"" ?>>������� (300.00 ��. + 30.00 ��. ���������)<?= ($klan['clanlevel'] < 5)?" - ��������� ������� ����� 5":"" ?></option>
					<script type="text/javascript">abilities.push({ code: "ability_pond", price: 300.00, currency: "dinar" });</script>
                    <? } ?>

				</select>
				<input class="xbbutton" name="do_change" type="submit" value="��������">
			</form>

            <? if (!empty($klan_settings['locator'])) { ?>
    		<fieldset class="cups-exchange" style="display: inline-block;margin-right: 10px;background-color: #ECECEC;">
    			<legend>�������</legend>
    			<form action="/klan.php?0.469148721443506" name="locator_form" method="post" style="margin-top: 10px;" onsubmit="return confirm('�� �������, ��� ������ ������������ �������? ��������� �������� 30 ��.')">
    				<input name="nd" type="hidden" value="<?= $user['nd'] ?>">
    				<input name="cmd" type="hidden" value="change.cups">
    				<input name="change_cups" type="hidden" value="locator">
    				������������ ������� (<b>30</b> ��.) - ����������: � �������� ������������ ��� � �����������.<br><br>
    				��� ����:
    				<input class="text" name="target" type="text" value="<?= @$_POST['target'] ?>">
    				<input class="xbbutton" name="do_change" type="submit" value="������������"> <?= @$islocator ?>
    			</form>
            </fieldset>
             <? } ?>
    		<fieldset class="cups-exchange" style="display: inline-block;margin-right: 10px;background-color: #ECECEC;">
    			<legend>�����</legend>
    			<form action="/klan.php?0.35273631871107" name="cup2flor_form" method="post" style="margin-top: 10px;" onsubmit="return confirm_cup2dinar(50);">
    				<input name="nd" type="hidden" value="<?= $user['nd'] ?>">
    				<input name="cmd" type="hidden" value="change.cups">
    				<input name="change_cups" type="hidden" value="cup2flor">�������� ����� �� ������ �� �������� ���� (<b>1 ����� = 50 �������</b>).<br><br>
                    ������:
                    <input class="text number" name="count" size="5" type="text" id="cup2flor_input">
    				<input class="xbbutton" name="do_change" type="submit" value="��������">
    			</form>
            </fieldset>
<? if ($klan_settings['repair'] || $klan_settings['recovery'] || $klan_settings['hall'] || $klan_settings['chest'] || $klan_settings['accept_exclude'] || $klan_settings['shop'] || $klan_settings['locator'] || $klan_settings['buff'] || $klan_settings['pond']) { ?>
			<table class="table-list" id="active-services" style="margin-top: 1em; width: auto;">
				<thead>
					<tr>
						<th colspan="4">������������ ������</th>
					</tr>
					<tr>
						<th>��������</th>
						<th>��������</th>
						<th>���������� �����</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<? if (!empty($klan_settings['repair'])) { ?><tr>
						<th>������ �� ���� �����</th>
						<td>��������� ������� ������� ����� ������������� ���������� �������� �� ���� ����� �����<!-- (��� ������� ��������� ���� ��������� ����������� ������� ��������� � �������� ����)-->.</td>
						<td class="cost"></td>
						<td class="actions nowrap">
							<img alt="�������" onclick="drop_ability('repair','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/bcancel.gif" title="������� ������" />
						</td>
					</tr><? } ?>

					<? if (!empty($klan_settings['recovery'])) { ?><tr>
						<th>������� �� ���� �����</th>
						<td>��������� ������� ������� ����� ��������������� ������ ����� � ������������ �� ���� ����� �����<!-- (��� ������� ��������� ���� ��������� ����������� ������� � �������� ����)-->.</td>
						<td class="cost"></td>
						<td class="actions nowrap">
							<img alt="�������" onclick="drop_ability('recovery','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/bcancel.gif" title="������� ������" />
						</td>
					</tr><? } ?>

					<? if (!empty($klan_settings['hall'])) { ?><tr>
						<th>�������� ���</th>
						<td>��������� ��������� ����, ������������ ����������� ���������� ������� � ������� ����� ��� �������� �������; ����������� ������ �������� ������, ������� � �������.</td>
						<td class="cost">30.00 �������</td>
						<td class="actions nowrap">
							<img alt="�������" onclick="drop_ability('hall','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/bcancel.gif" title="������� ������" />
<?
/*                            <img alt="�����������" onclick="resume_ability('hall','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/run.gif" title="����������� ������" />
*/
?>                            <img alt="�������������" onclick="pause_ability('hall','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/stop.gif" title="������������� ������">
						</td>
					</tr><? } ?>

					<? if (!empty($klan_settings['chest'])) { ?><tr>
						<th>���������</th>
						<td>������ � �������� ���� ��������� �����, � ������� ��� ������ ����� ������ ������� ���� ����.</td>
						<td class="cost">5.00 �������</td>
						<td class="actions nowrap">
							<img alt="�������" onclick="drop_ability('chest','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/bcancel.gif" title="������� ������" />
<?
/*                            <img alt="�����������" onclick="resume_ability('chest','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/run.gif" title="����������� ������" />
*/
?>                            <img alt="�������������" onclick="pause_ability('chest','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/stop.gif" title="������������� ������">
						</td>
					</tr><? } ?>

					<? if (!empty($klan_settings['accept_exclude'])) { ?><tr>
						<th>����� � ���������� �� ���� �����</th>
						<td>����� ��������� ������ �������� ����������� ��������� � ���� � ��������� �� ���� �� ���� �����.</td>
						<td class="cost"></td>
						<td class="actions nowrap">
							<img alt="�������" onclick="drop_ability('accept_exclude','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/bcancel.gif" title="������� ������" />
						</td>
					</tr><? } ?>

					<? if (!empty($klan_settings['shop'])) { ?><tr>
						<th>�������� �������</th>
						<td>������ � �������� ���� ��������� ��������, � ������� ��� ������ ����� ������ �������� ����.</td>
						<td class="cost"></td>
						<td class="actions nowrap">
							<img alt="�������" onclick="drop_ability('shop','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/bcancel.gif" title="������� ������" />
						</td>
					</tr><? } ?>

					<? if (!empty($klan_settings['locator'])) { ?><tr>
						<th>������������� ��������</th>
						<td>��������� ������ ��������� ��������, ������������ ���������� � ����������� �� ��������� ��������� ��� ���.</td>
						<td class="cost"></td>
						<td class="actions nowrap">
							<img alt="�������" onclick="drop_ability('locator','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/bcancel.gif" title="������� ������" />
						</td>
					</tr><? } ?>

					<? if (!empty($klan_settings['buff'])) { ?><tr>
						<th>�����</th>
						<td>������ � �������� ���� ������ �����, �������������� ��� ��������� � ���� ��������� ��� (�� ����, ��� ���� ��� � 3 ����).</td>
						<td class="cost">20.00 �������</td>
						<td class="actions nowrap">
							<img alt="�������" onclick="drop_ability('buff','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/bcancel.gif" title="������� ������" />
                            <img alt="�������������" onclick="pause_ability('buff','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/stop.gif" title="������������� ������">
						</td>
					</tr><? } ?>

					<? if (!empty($klan_settings['pond'])) { ?><tr>
						<th>������������� �������</th>
						<td>��������� ������ ��������� �������, ������������ ���� ������� ����� ������ ���� � �������� �����.</td>
						<td class="cost">30.00 �������</td>
						<td class="actions nowrap">
							<img alt="�������" onclick="drop_ability('pond','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/bcancel.gif" title="������� ������" />
                            <img alt="�������������" onclick="pause_ability('pond','<?= $user['nd'] ?>');" src="http://img.blutbad.ru/i/stop.gif" title="������������� ������">
						</td>
					</tr><? } ?>

				</tbody>
			</table>
<? } ?>
			<span><br>������� ����� �������: <b>0/1000</b>.</span>
			<input class="xbbutton" type="button" value="���������" onclick="increase_ability('chest','<?= $user['nd'] ?>');"  disabled="disabled"/>
			<br>

		</fieldset>

		<?
/*<fieldset>
			<legend>����� ��������� ���������</legend>
			��������� ���������� ��� � ����� �������� � .<br><br>
			��������� ������� ��������� ��������� ����� <b>50</b> ������. <span style="color:red;">��������� �������� ����� <span class="elapsed-time nowrap invisible"></span></span>
			<form action="/klan.pl" enctype="multipart/form-data" method="post" onsubmit="return confirm('������������� �������� ����� ��������� ���������?');">
				<input class="text" style="vertical-align: middle; width: 3.5em;" id="bearer_time" maxlength="5" name="bearer_time" value="" size="5" type="text" disabled="disabled" /> (��:��)
				<input name="nd" type="hidden" value="3896743135" />
				<input name="cmd" type="hidden" value="change.bearer_time" />
				<input class="button" name="do_change" type="submit" value="��������" disabled="disabled" />
			</form>
		</fieldset>*/
?>

	<fieldset>
	<legend>������ ����� �����</legend>

	������ ����� ����� ������ <b>100 ���.</b> (� ������ ����������� ������ ����� ������������ ������ �� ������������)
	<form action="/klan.php" enctype="multipart/form-data" method="post" name="img_form">
	<input name="cmd" type="hidden" value="change.img" />
	<input name="nd" type="hidden" value="<?= $user['nd'] ?>" />

	<input type="file" name="klanimg" />
	<br>
	<span class="comment">(������ GIF, ������ 200�200, ��� ����������, ��� ��������)</span>
	<br><br>
	<input class="xbbutton" name="do_change" onclick="return confirm('�������� ������ �����?');" type="submit" value="���������" />
	</form>
	</fieldset>

	<fieldset>
	<legend>������ ������ �����</legend>

	������ ������ ����� ������ <b>100 ���.</b> (� ������ ����������� ������ ������ ������������ ������ �� ������������)

	<form action="/klan.php" enctype="multipart/form-data" method="post">
	<input name="cmd" type="hidden" value="change.icon" />
	<input name="nd" type="hidden" value="<?= $user['nd'] ?>" />

	<input type="file" name="klanicon" />
	<br>
	<span class="comment">(������ GIF, ������ 25x16, ��� ����������, ��� ��������, ����������� ���� ��� ���� ���� � ������)</span>
	<br><br>
	<input class="xbbutton" name="do_change" onclick="return confirm('�������� ������ ������?');" type="submit" value="���������" />
	</form>
	</fieldset>

	<fieldset>
	<legend>������� �����</legend>

	<form action="/klan.php" method="post" name="desc_form">
	<input name="cmd" type="hidden" value="change.desc" />
	<input name="nd" type="hidden" value="<?= $user['nd'] ?>" />
    ������� ����� ������������ � �������� ����� � ���������� ����. ������� ����� ����� �������� �� 25 ���.
     <table>
       <tr>
         <td style="padding: 5px;vertical-align: middle;">��������:</td>
         <td style="padding: 5px;"><textarea name="desc" maxlength="2500" style="width: 500px;height: 50px;"><?= script_to_code($klan['descr']) ?></textarea></td>
       </tr>
       <tr>
         <td style="padding: 5px;">&nbsp;</td>
         <td style="padding: 5px;"><input class="xbbutton" name="do_change" type="submit" value="���������"/></td>
       </tr>
     </table>

	</form>
	</fieldset>

	<fieldset>
	<legend>�������� ����</legend>

	<form action="/klan.php" method="post" name="konto_form">
	<input name="cmd" type="hidden" value="change.konto" />
	<input name="nd" type="hidden" value="<?= $user['nd'] ?>" />

	�������� �������� ����:
	<input class="text" maxlength="10" name="main_konto" size="10" type="text" value="<?= $klan['clan_bank'] ?>" />
	<input class="xbbutton" name="do_change" type="submit" value="���������"/>
	</form>
	</fieldset>
<?
/*
	<fieldset>
	<legend>������� � �������� ������</legend>

	<form action="/klan.php" method="post" name="rating_form">
	<input name="cmd" type="hidden" value="change.rating" />
	<input name="nd" type="hidden" value="4570850522" />

	<a href="http://top.blutbad.ru" class="b fr" style="display:block;" target="_blank">������� � ��������&nbsp;&gt;&gt;</a>
	���������� �������������: <span class="green">���������</span>
	<br/>
	<br/>

 <table class="rate-table">
	<tbody>
	<tr>
	<td class="label">
	<label for="rating-active">�������� ���� � �������?</label>
	</td>
	<td class="input">
	<input checked="checked" class="checkbox" id="rating-active" name="rating_active" type="checkbox" />
	</td>
	</tr>
	<tr>
	<td class="label">
	<label for="clan-site">����� ��������� �����</label>
	</td>
	<td class="input">
	<input class="text" id="clan-site" maxlength="50" name="http" size="50" type="text" value="http://blutbad.ru/" />
	</td>
	</tr>
	<tr>
	<td class="label">
	<label for="rating-description">�������� � ��������</label>
	</td>
	<td class="input">
	<input class="text" id="rating-description" maxlength="50" name="rating_description" size="50" type="text" value="����� ���� ��� ��� ��� ���������� �� ������ ������" />
	</td>
	</tr>
	</tbody>
	</table>
	<br/>
	<b>��������!</b><br/>
	��� ��������� ������ ��������� �����, ������� �� ������� ���� ����������.
	<br/><br/>

	<input class="xbbutton" name="do_change" type="submit" value="���������" />
	<input class="xbbutton" id="get-counter-code" type="button" value="�������� ��� ��������" />
	</form>
	</fieldset>	*/
?>


	</div>




	<script type="text/javascript">
	//<![CDATA[
	document.info_form.area.value = "tools";

	$(function() {
	$('#get-counter-code').click(function() {
	var left = (window.screen.width - 800) / 2;
	var top = (window.screen.height - 600) / 2;
	window.open('http://top.blutbad.ru/counters.php?id=200010&class=klan','counters','height=600,width=800,resizable=no,scrollbars=yes,menubar=no,status=no,toolbar=no,top=' + top + ',left=' + left);
	});
	});
	//]]>
	</script>


<? } else { ?>
    � ��� ��� ���� ��� ������� ��������� ����� �������<br><br>
    <? if (!empty($klan_settings['locator']) && !empty($polno[$user['id']]["locator"])) { ?>
		<fieldset class="cups-exchange" style="display: inline-block;margin-right: 10px;background-color: #ECECEC;">
			<legend>�������</legend>
			<form action="/klan.php" name="locator_form" method="post" style="margin-top: 10px;" onsubmit="return confirm('�� �������, ��� ������ ������������ �������? ��������� �������� 30 ��.')">
				<input name="nd" type="hidden" value="<?= $user['nd'] ?>">
				<input name="cmd" type="hidden" value="change.cups">
				<input name="change_cups" type="hidden" value="locator">
				������������ ������� (<b>30</b> ��.) - ����������: � �������� ������������ ��� � �����������.<br><br>
				��� ����:
				<input class="text" name="target" type="text" value="<?= @$_POST['target'] ?>">
				<input class="xbbutton" name="do_change" type="submit" value="������������"> <?= @$islocator ?>
			</form>
        </fieldset>
    <? } ?>
<? } ?>