<?
 if ($user['level'] == 0) {

  if (empty($is_inventory)) {
    if (empty($is_map)) {
      include("qwest/functions.quest.php");
    }
    checkperform_qwest();
  }

  $all_qwest        = sql_number("SELECT * FROM `quest_taken` WHERE `owner` = '".$user['id']."'");
  $qwest_stats      = sql_row("SELECT * FROM `quest_taken` WHERE `qt_cur_qwest_id` = 6 AND `owner` = '".$user['id']."' LIMIT 1");
  $qwest_inventory  = sql_row("SELECT * FROM `quest_taken` WHERE `qt_cur_qwest_id` = 7 AND `owner` = '".$user['id']."' LIMIT 1");
  $qwest_battle     = sql_row("SELECT * FROM `quest_taken` WHERE `qt_cur_qwest_id` = 8 AND `owner` = '".$user['id']."' LIMIT 1");
  $qwest_instructor = sql_row("SELECT * FROM `quest_taken` WHERE `qt_cur_qwest_id` = 10 AND `owner` = '".$user['id']."' LIMIT 1");


  $is_usersquests_onebattle = sql_row("SELECT * FROM `users_quests` WHERE quest_id = '4' AND `user_id` = '".$user['id']."' AND `status` = 2 LIMIT 1");

      $highlightAllTutorialElements = "";
      $text = "";
      $qwest = 0;

#-----#---------------------------------------
#  1  # ��� �������
#-----#---------------------------------------

      if ($all_qwest == 0) {
        $text = '����������� ���� � ������������� ���� �������!<br>
                 ���� ����� <b>������</b>, � � ���� ���� ����� ������� ����.<br><br>
                 ���� ����� ��������� ��� ������� ������� � ���� �����������, ��� ������ ���� ����� ����� ����.<br>
                 ����� ������ <i>(������ � ������� ���� "�������� => �������")</i> � �� ������� � ���� ��� ��������� ����� ��� ������. <img alt="" class="tooltip-next" src="http://img.blutbad.ru/i/obraz/1_bot_m_mentor_s.jpg" style="vertical-align: middle;"><div class="invisible"><b>������</b></div></span><br>
		         <span>������� �� ������ � ������� ������ �������.<br>
                 <hr style="width: 100%">
                 <ul>
      			   <li style="background: url(\'http://img.blutbad.ru/i/quest/dialog.png\') no-repeat 0 2px; margin: .7em 0; padding: 1px 0 2px 18px;"><b style="vertical-align: middle;">��������. �������������� ���������</b></li>
                 </ul>
        ';
        $highlightAllTutorialElements = 'top.highlightAllTutorialElements("battle");';
        $qwest = 1;
      }

#-----#---------------------------------------
#  2  # ����� ����� �����
#-----#---------------------------------------

      if ($all_qwest == 1 && empty($qwest_inventory['id'])) {
        $text = '�������! � ���� ���� ����������, �� ��������� � ���� ������ ��������, ������ ���� ��������� � ������� �������.<br><br>
                 <i>(������ � ������� ���� "�������� => �������")</i><br><br>
                 <span>����� ����� ��� ������ <img alt="" class="tooltip-next" src="http://img.blutbad.ru/i/obraz/1_bot_m_mentor_s.jpg" style="vertical-align: middle;"><div class="invisible"><b>������</b></div> ����� ����� �����, � ����������� ������� ������ <b>������</b> �������.</span><br>
                 <hr style="width: 100%">
                 <ul>
      			   <li style="background: url(\'http://img.blutbad.ru/i/quest/dialog.png\') no-repeat 0 2px; margin: .7em 0; padding: 1px 0 2px 18px;"><b style="vertical-align: middle;">��������: ���������</b></li>
                 </ul>
        ';
        $highlightAllTutorialElements = 'top.highlightAllTutorialElements("battle");';
        $qwest = 1;
      }

#-----#---------------------------------------
#  3  # ����� ����� �����
#-----#---------------------------------------

      if ($all_qwest == 2 && empty($qwest_battle['id'])) {
        $text = '�������! �� ��������� ��� � ����� ����� ���������.<br>
		         ������ � ���� ��������� ����� ������� � ���������� ��������� - �������� ������ ������ � ���.<br>
                 ������� ������� "�������� ������ ������" � ������ � ���!<br>

                 ���� ����� ������� <b>�������</b><br><br>
                 <i>(������ � ������� ���� "�������� => �������")</i><br><br>
                 <span>����� ����� ��� ������ <img alt="" class="tooltip-next" src="http://img.blutbad.ru/i/obraz/1_bot_m_mentor_s.jpg" style="vertical-align: middle;"><div class="invisible"><b>������</b></div> ����� ����� �����, � ����������� ������� ������ <b>������</b> �������.</span><br>
                 <hr style="width: 100%">
                 <ul>
      			   <li style="background: url(\'http://img.blutbad.ru/i/quest/dialog.png\') no-repeat 0 2px; margin: .7em 0; padding: 1px 0 2px 18px;"><b style="vertical-align: middle;">�������: �������� ������ ������</b></li>
                 </ul>
        ';
        $highlightAllTutorialElements = 'top.highlightAllTutorialElements("battle");';
        $qwest = 1;
      }

#-----#---------------------------------------
#  4  # ����������: � ������� ����������
#-----#---------------------------------------

      if ($all_qwest == 3 && empty($qwest_instructor['id'])) {
        $text = '��������� ����� ����� ���� ����������, ������� ������ ���� ���������� � ���������� ��� ������� ����, � ������ ����!<br>
                 � �� �� ����� ���� ����� ������� �������, ����� ��� �� ����� ��� � ����.<br>
		         ���������� - ��� ������� ������, ������� ������ ��� �����!<br>
				 ������� ������� � ������� ����������.<br>

                 ���� ����� ������� <b>�������</b><br><br>
                 <i>(������ � ������� ���� "�������� => �������")</i><br><br>
                 <span>����� ����� ��� ������ <img alt="" class="tooltip-next" src="http://img.blutbad.ru/i/obraz/1_bot_m_mentor_s.jpg" style="vertical-align: middle;"><div class="invisible"><b>������</b></div> ����� ����� �����, � ����������� ������� ������ <b>���������</b> �������.</span><br>
                 <hr style="width: 100%">
                 <ul>
      			   <li style="background: url(\'http://img.blutbad.ru/i/quest/dialog.png\') no-repeat 0 2px; margin: .7em 0; padding: 1px 0 2px 18px;"><b style="vertical-align: middle;">����������: � ������� ����������</b></li>
                 </ul>
        ';
        $highlightAllTutorialElements = 'top.highlightAllTutorialElements("battle");';
        $qwest = 1;
      }


#-----#---------------------------------------
#  1  # ��������. �������������� ���������
#-----#---------------------------------------

      if (!empty($qwest_stats['id'])) {
         switch ($qwest_stats['status']) {
           case 1:
            $text = '� ���, �� ��������� � ������� �������, ���� ����� ����� � ��������� � ���������� ��������������.
			         ����� ������� � ���������, ����� "���������"<br><br>
                     <i>(������ � ������� ���� "�������� => ���������")</i><br><br>
				     ������ �������� �������������� ��� ��� ���� �������, ���� ������ �� ������� �������������.<br><br>

                      <a href="#" class="blue" onclick="window.open(\'/faq.php?cmd=abilities.show\',\'displaywindow\',\'height=620,width=600,resizable=no,scrollbars=yes,,menubar=no,status=no,toolbar=no,directories=no,location=no\');" style="text-decoration: underline;">
						���������� �������������<br>�������������
					  </a><br>
            ';
            $highlightAllTutorialElements = 'top.highlightAllTutorialElements("stats");';
            $qwest = 1;
           break;
           case 2:
            $text = '���������� �� ��������� � ������ ��������, ������� � ������ �������, ���� ����� ����� ���� ����� � �����.<br><br>
                     <i>(������ � ������� ���� "�������� => �������")</i><br><br>
                     <span>����� ����� ��� ������ <img alt="" class="tooltip-next" src="http://img.blutbad.ru/i/obraz/1_bot_m_mentor_s.jpg" style="vertical-align: middle;"><div class="invisible"><b>������</b></div> ����� ����� �����, � ����������� ������� ������ <b>���������</b> �������.</span><br>
                     <hr style="width: 100%">
                     <ul>
          			   <li style="background: url(\'http://img.blutbad.ru/i/quest/dialog.png\') no-repeat 0 2px; margin: .7em 0; padding: 1px 0 2px 18px;"><b style="vertical-align: middle;">��������. �������������� ���������</b></li>
                     </ul>
            ';
            $highlightAllTutorialElements = 'top.highlightAllTutorialElements("battle");';
            $qwest = 1;
           break;
         }
      }

#-----#---------------------------------------
#  2  # ��������. ���������
#-----#---------------------------------------

      if (!empty($qwest_inventory['id'])) {
         switch ($qwest_inventory['status']) {
           case 1:
            $text = '��� ����� ��������?<br>
                     ���� �� ������� ��������, �� ������ ����������� ������� �� ������� "�������� ����������".<br><br>
			         ����� ��������� ������ ������� ���� ����� ����� � <u>���������</u>, � <u>������</u> �� ���� ��� ��������.<br><br>
                     <i>(������ � ������� ���� "�������� => ���������")</i><br><br>
                     <span>� ������ ����� ����� ������� <b>����</b> ��� ���� ���� <b>����������� ��������</b> �� � ����� <u>������</u><br><br>����� ���� ����������� ����� �������.</span><br>
            ';
            $highlightAllTutorialElements = 'top.highlightAllTutorialElements("inventory");';
            $qwest = 1;
           break;
           case 2:
            $text = '������� �� ��������!<br>
                      ���� ����� ������� <u>�������</u><br><br>
                      <i>(������ � ������� ���� "�������� => �������")</i><br><br>
                      <span>����� ����� ��� ������ <img alt="" class="tooltip-next" src="http://img.blutbad.ru/i/obraz/1_bot_m_mentor_s.jpg" style="vertical-align: middle;"><div class="invisible"><b>������</b></div> ����� ����� �����, � ����������� ������� ����� ������� ������ <b>���������</b>.</span><br>
                      <hr style="width: 100%">
                      <ul>
          			    <li style="background: url(\'http://img.blutbad.ru/i/quest/dialog.png\') no-repeat 0 2px; margin: .7em 0; padding: 1px 0 2px 18px;"><b style="vertical-align: middle;">��������. ���������</b></li>
                      </ul>
            ';
            $highlightAllTutorialElements = 'top.highlightAllTutorialElements("battle");';
            $qwest = 1;
           break;
         }
      }

#-----#---------------------------------------
#  3  # �������: �������� ������ ������
#-----#---------------------------------------

      if (!empty($qwest_battle['id'])) {
         switch ($qwest_battle['status']) {
           case 1:
            $text = '������� ����� ������������� � ������ ���<br>
                      ���� ����� ������� <b>��������</b><br>
                      <i>(������ � ������� ���� "�������� => ��������")</i><br><br>
                      <span>����� ������ ����� "�������������", ��� ������� "<u>������������� ���</u>" ��� ������ <b>������</b> � ������ ��������� �������.<br><br>����� ���� ����������� ����� �������.</span><br>
            ';
            $highlightAllTutorialElements = 'top.highlightAllTutorialElements("expa");';
            $qwest = 1;
           break;
           case 2:
            $text = '������� �� ��������?<br>
                      ���� ����� ������� <b>�������</b><br>
                      <i>(������ � ������� ���� "�������� => �������")</i><br>
                      <span>����� ����� ��� ������ <img alt="" class="tooltip-next" src="http://img.blutbad.ru/i/obraz/1_bot_m_mentor_s.jpg" style="vertical-align: middle;"><div class="invisible"><b>������</b></div> ����� ����� �����, � ����������� ������� ����� ������� ������ <b>���������</b>.</span><br>
                      <hr style="width: 100%">
                      <ul>
          			    <li style="background: url(\'http://img.blutbad.ru/i/quest/dialog.png\') no-repeat 0 2px; margin: .7em 0; padding: 1px 0 2px 18px;"><b style="vertical-align: middle;">�������: �������� ������ ������</b></li>
                      </ul>
            ';
            $highlightAllTutorialElements = 'top.highlightAllTutorialElements("battle");';
            $qwest = 1;
           break;
         }
      }

#-----#---------------------------------------
#  4  # ����������: � ������� ����������
#-----#---------------------------------------

      if (!empty($qwest_instructor['id'])) {
         switch ($qwest_instructor['status']) {
           case 1:
            $text = '�������� �� ������� ��������, � ���� �� ������ ���������� ������ � ������������ �� �����������.<br>
			          ���� �� ��� �� ������, � �� ������� � ������, ������ ���������.<br><br>
                      ������ � ���� ������� ��� ������ ����� ����������:<br>
					  "� ���� ���� ���� ����, ��� ��� ������ ���������, ������ � ��� - "����� ���������!",<br>
					  ��� ������ ����� ���� ���� ������ ����������, � ������ �������� ������, �� ������ ����� ����� �����������"<br><br>
					  ����� ��������� ������ ����, �� ��������� ���� ������, ���� ����� ����� ����� � ������ � ������� ��� �����������!<br><br>
					  <span><img alt="������" height="30" src="http://img.blutbad.ru/i/button/new/cm_transfer.gif" title="������" width="30" style="vertical-align: middle;"> ������ ���������� � ���� �� ������ ������ ��� ������ � ������ ������.</span>
            ';
            $highlightAllTutorialElements = ' ';
            $qwest = 1;
           break;
           case 2:
            $text = '����������! �� ��������� � �������� � ����� ���� ����������!<br><br>������� �� ��������!';
            $highlightAllTutorialElements = 'top.highlightAllTutorialElements("battle");';
            $qwest = 1;
           break;
         }
      }

  if ($qwest) {

?>


    <script>
     $(function() {

               <?
                  if ($highlightAllTutorialElements) {
                   echo 'top.setTutorialState("");';
                   echo $highlightAllTutorialElements;
                  }
               ?>


         var fyashow = true;
         var fyactive = false;

          $("#qwest").hover(
            function() {
              $(this).stop().animate({opacity: "1"}, 300);
              fyactive = true;
            },
            function() {
              $(this).stop().animate({opacity: "0.40"}, 3000);
              fyactive = false;
            }
          );

         setInterval(function() {
          try {
                if (fyashow == true && fyactive == false) {
                  $('#qwest').stop().animate({opacity: "0.40"}, 300);
                  fyashow = false;
                }else if(fyashow == false && fyactive == false) {
                  $('#qwest').stop().animate({opacity: "0.20"}, 1000);
                  fyashow = true;
                }
          } catch(e) {}
         }, 3000);


     $("#qwest").draggable({snap:".main-content"});
     $("#qwest").draggable({start:function(){ $("#qwest").css({"bottom" : ""});}, stop:function(){}, drag:function(){} });

     qw_close = <?= @$_SESSION["qw_close"]?"1":"0" ?>;

        if (qw_close) {
          $("#qwest_min").html("����������");
          $("#qwest").height("27");
          $("#qwest table").hide();
          $("input[name='qw_close']").val(qw_close);
        } else {
          $("#qwest_min").html("��������");
          $("#qwest").height("280");
          $("#qwest table").show();
          $("input[name='qw_close']").val(qw_close);
        }

		$('#qwest_min').click(function() {

          if (qw_close) {
            $(this).html("��������");
            $("#qwest").height("280");
            qw_close = 0;
            $("#qwest table").show();
            $("input[name='qw_close']").val(qw_close);
          } else {
            $(this).html("����������");
            $("#qwest").height("27");
            qw_close = 1;
            $("#qwest table").hide();
            $("input[name='qw_close']").val(qw_close);
          }

        	$.ajax({
        		cache: false,
        		data: {
        			close: qw_close,
        			cmd: "qw_close"
        		},
        		type: 'post',
        		url: '/npc.php?' + Math.random()
        	});

		});

    	/*var yaDialog = $('#qwest');
    	if (yaDialog.length) {
    		// AJAX-���������� ���������
    		yaDialog.find('form').ajaxForm({
    			data: {
    				ajax: true,
    				is_ajax: 1
    			},
    			dataType: 'json',
    			resetForm: true,
    			success: function(json) {
    				showNotification(json.message, json.error ? true : false);

    				if(!json.error) {
                     yaDialog.remove();
    				}
    			}
    		});
    	}*/
     });
    </script>


       <div style="position: fixed;right: 10px;bottom: 10px;background-color: #E4E8EA;  border: 1px solid #626262;padding: 8px; opacity: 0.5; width: 460px" id="qwest">

        <p style="color: #FFEFDC;background-color: #FFCB9F;border: 1px solid #626262;padding: 3px;margin: -9px -9px 5px -9px; cursor: pointer;background: -webkit-gradient(linear,left top,left bottom,from(#FFCF94),to(#A05A1E));    background: -moz-linear-gradient(top,#FFCF94,#A05A1E);    background: -o-linear-gradient(top,#FFCF94 0,#A05A1E 100%);    background: -ms-linear-gradient(top,#FFCF94,#A05A1E); background: linear-gradient(top,#FFCF94,#A05A1E);"><b>��������� �� �������?</b></p>
         <div style="width: 100%;"><a href="#" style="float: right;font-weight: bold" id="qwest_min">��������</a></div>

          <table id="qwest">
            <tbody><tr>
              <td style="padding-right: 10px;">
                  <table>
                    <tbody><tr>
                      <td style=" text-align: center; padding-bottom: 5px; "><span class="nick">
                          <b class="name side-0" onclick="top.AddTo('������')">������</b> <span class="level">[15]</span>
                          <a class="info" href="http://damask.blutbad.ru/inf.php?user=������" target="_blank">
                          <img alt="" class="sex" src="http://img.blutbad.ru/i/infM.gif" title="���������� � ���������"></a>
                          </span></td>
                    </tr>
                    <tr>
                      <td>
                      <img src="http://img.blutbad.ru/i/mentor.png" border="0" alt="" style="width: 120px;">
                      </td>
                    </tr>
                  </tbody></table>
              </td>
              <td style=" padding-top: 15px; ">
                  <!--����������� ����, <b>�������</b>!<br>
                  <br>                                     -->
                  <?= $text ?>
              </td>
            </tr>
            </tbody>
          </table>


       </div>

  <? } ?>
<? } ?>
