<?

#-----------------------------------------------
# �������� �������������� �������
  function __autoload($name) {
    if ($name) {
     include("class.".$name.".php");
    }
  }

class UserControl {

    public $user                = false;
    public $user_id             = 0;
    public $upd_sql             = "";
    public $inventory_data      = array(); # ��������� ������ �� �����
    public $load_inventory_data = false;   # �������� �����
    public $mod_inventory_data  = false;   # ��������� �� ��������� ������
    public $file_inventory_data = "";      # ������ ���������� �����
    public $komplekts           = "";      # ������ ���������� �����
    public $del_cur_mysql       = true;    # �� �������� ������ ������� ���������� � ����

    public $sql_print           = false;   #

  #-----------------------------------------------
  # �������� ������
    function __construct($data_uid, $sel_rows = "") {

      # ���� ����� ������ ������������ ������ � ������� ��� ���� ����
        if (!empty($data_uid['id']) && is_numeric($data_uid['id'])) {
          $bd_user = $data_uid;
        } else {
          if ($sel_rows)                $bd_user = sql_row("SELECT $sel_rows FROM `users` WHERE `id` = '".$data_uid."' LIMIT 1;");
          elseif(is_numeric($data_uid)) $bd_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$data_uid."' LIMIT 1;");
        }

      # ���� ������������� ���������
        if (!empty($bd_user['id'])) {
          $this->user      = $bd_user;
          $this->user_id   = $bd_user['id'];
          $this->komplekts = unserialize(@$bd_user['komplekts']);

          $this->file_inventory_data = $_SERVER["DOCUMENT_ROOT"]."/adata/inventory_data/".$this->user_id.".txt";
        }
    }

  #-----------------------------------------------
  # �������� ������
    function __destruct() {
      global $mysql;
      if ($this->user_id) {

    # ��������� ����� � ��������
      $this->upd_sql = $this->USER_check_karma_valor($this->upd_sql);

    # ��������� ��������� � ����
      if ($this->upd_sql) {
         $this->USER_upd_sql();    # ������� ��� ���� ��� �������
      }

     # ��������� ��������� � ������� ���������
       $this->USER_save_inventory_data();

       if ($mysql && $this->del_cur_mysql == true) { mysqli_close($mysql); }
      }
    }

  #-----------------------------------------------
  # ������ ����������� �����
    function USER_get(){
      if ($this->user_id) return $this->user;
      return false;
    }

  #-----------------------------------------------
  # ��������� ����� � ��������
    function USER_check_karma_valor($u_sql = ''){
      if ($u_sql) {
      # ��������� � �����
        if (substr_count($u_sql, "`karma`") || (!substr_count(@$this->user['karma'], "-") && @$this->user['karma'] > @$this->user['karma_next']) || (substr_count(@$this->user['karma'], "-") && @$this->user['karma'] < @$this->user['karma_next'])) {
          $u_sql = $this->USER_check_karma($u_sql);
        }

      # ��������� � ��������
        if (substr_count($u_sql, "`valor`") || (!substr_count(@$this->user['valor'], "-") && @$this->user['valor'] > @$this->user['valor_next'])) {
          $u_sql = $this->USER_check_valor($u_sql);
        }
      }
      return $u_sql;
    }

  #-----------------------------------------------
  # ��������� ������ �����
    function USER_upd_sql($u_sql = ""){
      if (empty($u_sql)) $u_sql = $this->upd_sql;

    # ��������� ����� � ��������
      $u_sql = $this->USER_check_karma_valor($u_sql);

      if ($this->del_cur_mysql == true) {
        if ($u_sql && $this->user_id) {
          sql_query("UPDATE `users` SET `upd_online` = '".time()."' ".$u_sql." WHERE `id` = '".$this->user_id."' LIMIT 1;");
          $this->upd_sql = $u_sql;
        }
      } else {
        if ($u_sql && $this->user_id) {
          sql_query("UPDATE `users` SET `upd_online` = `upd_online` ".$u_sql." WHERE `id` = '".$this->user_id."' LIMIT 1;");
          $this->upd_sql = $u_sql;
        }
      }

      if ($this->user_id == 1 && empty($_POST['is_ajax']) && empty($_GET['is_ajax']) && empty($_POST) && $this->sql_print) {
       echo "<br>".$this->upd_sql;
      }

      $this->upd_sql = "";
      return false;
    }

  #-----------------------------------------------
  # �������� ������ � ����� � ���������� �����
    function USER_load_inventory_data(){
      if (is_writable($this->file_inventory_data)) {
        $this->inventory_data      = unserialize(file_get_contents($this->file_inventory_data));       # ���������
        $this->load_inventory_data = true;
        $this->mod_inventory_data  = false;
        return true;
      } else return false;
    }

  #-----------------------------------------------
  # ���������� ������ � ����� � ��������� �����
    function USER_save_inventory_data(){
    # ��������� ������ ����� ���� ����� ����� � ������������ �����������
      if ($this->mod_inventory_data/* && $this->load_inventory_data && is_writable($this->file_inventory_data)*/) {

        if (is_writable($this->file_inventory_data)) {
          file_put_contents($this->file_inventory_data, serialize($this->inventory_data), LOCK_EX); # ���������
        } else {
          if (is_file($this->file_inventory_data)) {
            unlink($this->file_inventory_data);
          }
          file_put_contents($this->file_inventory_data, serialize($this->inventory_data), LOCK_EX); # ���������
        }
        $this->load_inventory_data = false;
        return true;
      } else return false;
    }

  #-----------------------------------------------
  # �� ����� ��������� ���������   ������ �������� "/inventory.php?0.92" = "inventory"
    function USER_in_page(){
      $page = preg_replace("#(.*)/(.*?).php(.*)#is", '$2', $_SERVER["REQUEST_URI"]);
      return $page;
    }

  #-----------------------------------------------
  # ������ ����� ����
    function USER_slot_dreass_item($item){
     global $itemtype_to_name;

      switch ($item['id']) {
        case $this->user['r1']:      $type = 'r1';     break;
        case $this->user['r2']:      $type = 'r2';     break;
        case $this->user['r3']:      $type = 'r3';     break;
        case $this->user['r4']:      $type = 'r4';     break;

        case $this->user['m1']:      $type = 'm1';     break;
        case $this->user['m2']:      $type = 'm2';     break;
        case $this->user['m3']:      $type = 'm3';     break;
        case $this->user['m4']:      $type = 'm4';     break;
        case $this->user['m5']:      $type = 'm5';     break;
        case $this->user['m6']:      $type = 'm6';     break;

        case $this->user['stone1']:  $type = 'stone1'; break;
        case $this->user['stone2']:  $type = 'stone2'; break;
        case $this->user['stone3']:  $type = 'stone3'; break;
        case $this->user['stone4']:  $type = 'stone4'; break;
        case $this->user['stone5']:  $type = 'stone5'; break;
        case $this->user['stone6']:  $type = 'stone6'; break;
        case $this->user['stone7']:  $type = 'stone7'; break;
        default:
         if (empty($itemtype_to_name[$item['type']][1])) {
            $type = '';
         } else {
            $type = $itemtype_to_name[$item['type']][1];
         }
        break;
      }

     return $type;
    }


  #-----------------------------------------------
  # �������� �����
    function USER_addkarma($karm){
       $this->user['karma'] = $karm;
    }

  #-----------------------------------------------
  # ��������� "�����"
    function USER_check_karma($u_sql){
      switch($this->user['align']){

         # �������
           case '0.96':
           case '0.196':
              $user_max_karma = sql_row("SELECT `karma` FROM `users` WHERE `karma` > 0 AND (`align` = '0.96' OR `align` = '0.196') AND `bot` = 0 ORDER BY `karma` DESC LIMIT 1;");
              $negat = '';
           break;

         # ����
           case '0.97':
           case '0.197':
              $user_max_karma = sql_row("SELECT `karma` FROM `users` WHERE `karma` < 0 AND (`align` = '0.97' OR `align` = '0.197') AND `bot` = 0 ORDER BY `karma` ASC LIMIT 1;");
              $negat = '-';
           break;

         # ����
           case '0.98':
           case '0.198':
              $user_max_karma = sql_row("SELECT `karma` FROM `users` WHERE `karma` < 0 AND (`align` = '0.98' OR `align` = '0.198') AND `bot` = 0 ORDER BY `karma` ASC LIMIT 1;");
              $negat = '-';
           break;

         # ����
           case '0.99':
           case '0.199':
              $user_max_karma = sql_row("SELECT `karma` FROM `users` WHERE `karma` > 0 AND (`align` = '0.99' OR `align` = '0.199') AND `bot` = 0 ORDER BY `karma` DESC LIMIT 1;");
              $negat = '';
           break;

         # ��� ��������� ����������
           /*case (!in_array($this->user['align'], array('0.96', '0.196', '0.97', '0.197', '0.98', '0.198', '0.99', '0.199'))):
              if ($this->user['karma'] < 0) {
                  $user_max_karma = sql_row("SELECT `karma` FROM `users` WHERE `karma` < 0 AND (`align` != 0.199 AND `align` != 0.99 AND `align` != 0.198 AND `align` != 0.98 AND `align` != 0.197 AND `align` != 0.97 AND `align` != 0.196 AND `align` != 0.96) AND `bot` = 0 ORDER BY `karma` ASC LIMIT 1;");
                  $negat = '-';
              } else {
                  $user_max_karma = sql_row("SELECT `karma` FROM `users` WHERE `karma` > 0 AND (`align` != 0.199 AND `align` != 0.99 AND `align` != 0.198 AND `align` != 0.98 AND `align` != 0.197 AND `align` != 0.97 AND `align` != 0.196 AND `align` != 0.96) AND `bot` = 0 ORDER BY `karma` DESC LIMIT 1;");
                  $negat = '';
              }
           break;*/
      }

    # ��� ��������� ����������
      if (empty($user_max_karma['karma'])) {
          if ($this->user['karma'] < 0) {
              $user_max_karma = sql_row("SELECT `karma` FROM `users` WHERE `karma` < 0 AND (`align` != 0.199 AND `align` != 0.99 AND `align` != 0.198 AND `align` != 0.98 AND `align` != 0.197 AND `align` != 0.97 AND `align` != 0.196 AND `align` != 0.96) AND `bot` = 0 ORDER BY `karma` ASC LIMIT 1;");
              $negat = '-';
          } else {
              $user_max_karma = sql_row("SELECT `karma` FROM `users` WHERE `karma` > 0 AND (`align` != 0.199 AND `align` != 0.99 AND `align` != 0.198 AND `align` != 0.98 AND `align` != 0.197 AND `align` != 0.97 AND `align` != 0.196 AND `align` != 0.96) AND `bot` = 0 ORDER BY `karma` DESC LIMIT 1;");
              $negat = '';
          }
      }

       $data_karma = $user_max_karma['karma'];
       if (!$data_karma) {
         $data_karma    = 64916.48;
         $data_minkarma = 126.79;
       } else {
         $data_minkarma = ($data_karma / 512);
       }

       $karma_positive  = array();
       $valint          = 512;
       $keyint          = 512;

     # ������ ����� ������� �����
       for ($i = 1; $i < 11; $i++ ) {
        $karma_positive[$data_karma * $valint][0] = $i;
        $karma_positive[$data_karma * $valint][1] = $data_karma / $keyint;

        $keyint -= ($valint / 2);
        $valint -= ($valint / 2);
       }

       $u_karma       = $this->user['karma'];
       $u_karma_next  = $this->user['karma_next'];
       $u_karma_level = $this->user['karma_level'];

      /* echo "<pre>";
       print_r($karma_positive);*/

          foreach ($karma_positive as $key => $value) {

             $value[1] = number_format($value[1], 2, ".","");

            # ���������� �����
              if (empty($negat)) {

                if ($this->user['karma'] >= $data_karma) {
                   $u_karma_next  = $u_karma;
                   $u_karma_level = 10;
                   break;
                }

                if ($this->user['karma'] <= $value[1]) {
                   $u_karma_next  = $value[1];
                   $u_karma_level = $value[0] - 1;
                   break;
                } else {
                   $u_karma_next  = $data_karma / 512;
                   $u_karma_level = 0;
                }

            # ���������� �����
              } else {

                if ($this->user['karma'] <= $data_karma) {
                   $u_karma_next  = $u_karma;
                   $u_karma_level = 10;
                   break;
                }

                if ($this->user['karma'] >= $value[1]) {
                   $u_karma_next  = $value[1];
                   $u_karma_level = $value[0] - 1;
                   break;
                } else {
                   $u_karma_next  = $data_karma / 512;
                   $u_karma_level = 0;
                }

              }
          }

         /*echo $this->user['karma_next'].'<br>';
         echo $u_karma_next.'<br>';
         echo $data_karma.'<br>';*/

         if ($this->user['karma_next'] <> $u_karma_next || $this->user['karma_level'] <> $u_karma_level) {
          $u_sql .= ", `karma_next` = ".$u_karma_next.", `karma_level` = ".$u_karma_level."";
          $this->user['karma_next']  = $u_karma_next;
          $this->user['karma_level'] = $u_karma_level;

           # -----------------------------------------------
           # ���������� "����������"
             $achievements_finish = explode(";", $this->user['achievements_finish']);
             if ($this->user_id && $u_karma_level && !in_array("karma_10", $achievements_finish)) {
              $kr = $u_karma_level;

              foreach ($achievements_finish as $k => $v_fame) {
                if ($kr > 0 && in_array($v_fame, array('karma_1','karma_2','karma_3','karma_4','karma_5','karma_6','karma_7','karma_8','karma_9','karma_10'))) {
                  $kr -= 1;
                }
              }

              if ($kr <= 10){
                achievements($this->user, array('karma_level' => array(1 => $kr)));
              }
             } # end "����������"

         }
      return $u_sql;
    }

  #-----------------------------------------------
  # ��������� "��������" ������� ������ ��������
    function USER_check_valor($u_sql){

      # ��������
       $user_max_valor = sql_row("SELECT `valor` FROM `users` WHERE `bot` = 0 ORDER BY `valor` DESC LIMIT 1;");
       $data_valor     = $user_max_valor['valor'];

       if (empty($data_valor)) {
         $data_valor    = 64916.48;
         $data_minvalor = 126.79;
       } else {
         $data_minvalor = ($data_valor / 512);
       }

        if ($this->user['valor'] >= 0) {
            $valor_positive = array(# ��������
                                      0                 => array (0,  ($data_valor/512)),
                                      ($data_valor/512) => array (1,  ($data_valor/256)),
                                      ($data_valor/256) => array (2,  ($data_valor/128)),
                                      ($data_valor/128) => array (3,  ($data_valor/64)),
                                      ($data_valor/64)  => array (4,  ($data_valor/32)),
                                      ($data_valor/32)  => array (5,  ($data_valor/16)),
                                      ($data_valor/16)  => array (6,  ($data_valor/8)),
                                      ($data_valor/8)   => array (7,  ($data_valor/4)),
                                      ($data_valor/4)   => array (8,  ($data_valor/2)),
                                      ($data_valor/2)   => array (9,  ($data_valor)),
                                      ($data_valor)     => array (10, ($data_valor))
                                   );

          # ��������� ��������
            foreach ($valor_positive as $key => $value) {
               $valor = number_format($value[1], 2, ".", "");
                if ($this->user['valor'] >= $key && $this->user['valor'] <= $valor) {
                  $valor_next  = $valor;
                  $valor_level = $value[0];
                }
            }

          # ���� �������� �� ����� ��� ���� ������ ������ ��������
            if ($this->user['valor_next'] != $valor_next) {
              $u_sql = ", `valor_next` = ".$valor_next.", `valor_level` = ".$valor_level." ";
              $this->user['valor_next']  = $valor_next;
              $this->user['valor_level'] = $valor_level;

             # -----------------------------------------------
             # ���������� "����������"
               $achievements_finish = explode(";", $this->user['achievements_finish']);
               if ($this->user_id && $valor_level && !in_array("dobla_10", $achievements_finish)) {
                $vl = $valor_level;

                foreach ($achievements_finish as $k => $v_fame) {
                  if ($vl > 0 && in_array($v_fame, array('dobla_1','dobla_2','dobla_3','dobla_4','dobla_5','dobla_6','dobla_7','dobla_8','dobla_9','dobla_10'))) {
                    $vl -= 1;
                  }
                }

                 if ($vl) achievements($this->user, array('fame_level' => array(1 => $vl)));
               } # end "����������"

            }

        } else {
            $data_minvalor = conv_hundredths($data_minvalor, 2);
            if ($this->user['valor_next'] <> $data_minvalor || $this->user['valor_level'] != 0) {
               $u_sql = ", `valor_next` = ".$data_minvalor.", `valor_level` = 0";
               $this->user['valor_next']  = $data_minvalor;
               $this->user['valor_level'] = 0;
            }
        }

      return $u_sql;
    }

  #-----------------------------------------------
  # ������ ����������� �����
    function USER_calculate_life() {
     if ($this->user_id && empty($_POST['is_ajax'])) {

       $n_hp        = 0;
       $n_maxhp     = 0;
       $n_mana      = 0;
       $n_maxmana   = 0;
       $n_hprestore = $this->user['speed_restoration'];

     # ���� XP ��� PW ������ ��� �����
       if ($this->user['hp'] < 0)   { $this->upd_sql .= ", `hp` = 0, `hp_precise` = 0, `fullhptime` = ".time().""; }
       if ($this->user['mana'] < 0) { $this->upd_sql .= ", `mana` = 0, `pw_precise` = 0, `fullmptime` = ".time().""; }
     # ���� XP ��� PW ������ ��� �����
       if (empty($this->user['battle'])) {
         if ($this->user['hp'] > $this->user['maxhp'])     { $this->upd_sql .= ", `hp` = `maxhp`, `hp_precise` = `maxhp`, `fullhptime` = ".time().""; }
         if ($this->user['mana'] > $this->user['maxmana']) { $this->upd_sql .= ", `mana` = `maxmana`, `pw_precise` = `maxmana`, `fullmptime` = ".time().""; }
       }

      #-------------------------------------------------------------
      # ����� ������������� ����� ������� - ��������� ���������...
        include $_SERVER["DOCUMENT_ROOT"]."/functions/classes/user_control/effects.php";

      #-------------------------------------------------------------
      # ���� ���������� "maxhp"
        if ($this->user['maxhp'] <> ($this->user['inv_ghp'] + $this->user['vinos'] * 10)) {
          $this->upd_sql      .= ",`maxhp` = `users`.`inv_ghp` + (`users`.`vinos` * 10)";
          $this->user['maxhp'] = $this->user['inv_ghp'] + $this->user['vinos'] * 10;
        }
      #-------------------------------------------------------------
      # ���� ���������� "maxmana"
        if ($this->user['maxmana'] <> ($this->user['inv_gpw'] + abs($this->user['sila'] + $this->user['inv_strength']) * 6)) {
          $this->upd_sql        .= ",`maxmana` = `users`.`inv_gpw` + abs((`users`.`sila` + `users`.`inv_strength`) * 6)";
          $this->user['maxmana'] = $this->user['inv_gpw'] + abs($this->user['sila'] + $this->user['inv_strength']) * 6;
        }

      #-------------------------------------------------------------
      # ������ ������� � ��� ����� ������ � ������������ "���������"
        if (substr_count($this->user['eff_all'], "1042;")) {
          $hprestore = sql_row("SELECT sum(hprestore) as hprestore_sum FROM `effects` WHERE `owner` = '".$this->user_id."' LIMIT 1;");
          if (!empty($hprestore['hprestore_sum'])) {
            $this->upd_sql .= ",`speed_restoration` =  ".($hprestore['hprestore_sum'])." + 100";
            $n_hprestore = $hprestore['hprestore_sum'] + 100;
          } elseif($this->user['speed_restoration'] != 100){
            $this->upd_sql .= ",`speed_restoration` = 100";
          }
        } else {
          if($this->user['speed_restoration'] != 100){
            $this->upd_sql .= ",`speed_restoration` = 100";
          }
        }

      #-------------------------------------------------------------
      # ���� �� � ���
    	if($this->user['battle'] == 0) {

        # ���� "hp" ��� "mana" �� ����� ��� "hp_precise" ��� "pw_precise" �� �������� ��
          if ($this->user['hp']   <> floor($this->user['hp_precise'])) { $this->upd_sql .= ",`hp_precise` = `hp`"; }
          if ($this->user['mana'] <> floor($this->user['pw_precise'])) { $this->upd_sql .= ",`pw_precise` = `mana`"; }


        # �������� �� ���������� HP
          $this->user['hp_precise'] = $this->user['hp_precise'] + (((time() - $this->user['fullhptime']) / 100) * ($this->user['maxhp'] / 17.97)) * (int)$n_hprestore / 100;
          if (($this->user['hp_precise'] - $this->user['hp']) > 1) {
            if ($this->user['hp'] < $this->user['maxhp']) {
              $this->upd_sql   .= ",`hp_precise` = ".$this->user['hp_precise'].", `hp` = floor(`hp_precise`), `fullhptime` = ".time()."";
              $this->user['hp'] = $this->user['hp_precise'];
            }
          }

        # �������� �� ���������� PW
          $this->user['pw_precise'] = $this->user['pw_precise'] + (((time() - $this->user['fullmptime']) / 100) * ($this->user['maxmana'] / 17.97)) * (int)$n_hprestore / 100;
          if (($this->user['pw_precise'] - $this->user['mana']) > 1) {
            if ($this->user['mana'] < $this->user['maxmana']) {
              $this->upd_sql     .= ",`pw_precise` = ".$this->user['pw_precise'].", `mana` = floor(pw_precise), `fullmptime` = ".time()." ";
              $this->user['mana'] = $this->user['pw_precise'];
            }
          }

    	} # end "���� �� � ���"
     }
    }

  #-----------------------------------------------
  # �������� ��������� ��������������
    function USER_stats_reset(){
      global $exptable;

        $statsin = 3;

        foreach ($exptable as $k => $v) {
         if ($k <= $this->user['nextup']) { $statsin += $v[0]; }
         if ($k == $this->user['nextup']) { $statsin -= $v[0]; }
        }

        $this->USER_remove_all_items();

        //$user_stats = sql_row("SELECT sum(gsila) as gsila_sum, sum(glovk) as glovk_sum, sum(ginta) as ginta_sum, sum(gintel) as gintel_sum FROM `inventory` WHERE `dressed` = 1 AND `owner` = '".$this->user_id."'");

        $gsila  = 0;//$user_stats[0];
        $glovk  = 0;//$user_stats[1];
        $ginta  = 0;//$user_stats[2];
        $gintel = 0;//$user_stats[3];

        sql_query("UPDATE `users` SET `stats` = ".$statsin.", `sila` = 3 + '".$gsila."', `lovk` = 3 + '".$glovk."', `inta` = 3 + '".$ginta."', `vinos` = 3,`intel` = 0 + '".$gintel."', `mudra` = 0, `hp` = 30, `maxhp` = 30, `hp_precise` = 30, `maxmana` = 18, `mana` = 18, `pw_precise` = 18 WHERE `id` = '".$this->user_id."' LIMIT 1;");

    }

  #-----------------------------------------------
  # ����� ��� ����
    function USER_remove_all_items($bd_upd = true){
     global $itemtype_to_name;

     if ($this->user_id) {

       /*if (is_writable($this->file_inventory_data))*/ {

        # �������� � ����������� ������ � �����
          $baf_slot = array('drink', 'food', 'ngbonus', 'fish_yha');
          foreach ($baf_slot as $k => $v_baf) {
            if ($this->user[$v_baf] && $this->inventory_data[$v_baf]['id'] != $this->user[$v_baf]) {
  	        $put_item = sql_row("SELECT * FROM `inventory` WHERE `id` = '".$this->user[$v_baf]."' AND `owner` =  '".$this->user_id."' LIMIT 1;");
             if ($put_item['id']) {
               $this->USER_put_item($put_item['id'], $put_item);
              # echo "error ".$v_baf." ".$this->user[$v_baf]." <br>";
             }
            }
          }

          $inv_ghp = 0;
          $inv_gpw = 0;

          $tmp_inv_stats = array('inv_strength' => 0, 'inv_dexterity' => 0, 'inv_success' => 0, 'inv_intelligence' => 0, 'hp' => 0, 'mana' => 0, 'skill_fishing' => 0, 'inv_ghp' => 0, 'inv_gpw' => 0, 'inv_krit' => 0, 'inv_akrit' => 0, 'inv_uvor' => 0, 'inv_auvor' => 0, 'inv_extra_kick' => 0, 'inv_absorbs_uron' => 0, 'inv_absorbs_krit' => 0, 'inv_power_uron' => 0, 'inv_power_krit' => 0, 'inv_minu' => 0, 'inv_maxu' => 0, 'inv_bron1' => 0, 'inv_bron2' => 0, 'inv_bron3' => 0, 'inv_bron4' => 0);
          $baf_slot = array('drink', 'food', 'ngbonus', 'fish_yha');
          foreach ($baf_slot as $k => $slot_name) {
                 if ($gsila  = $this->inventory_data[$slot_name]['gsila'])      $tmp_inv_stats['inv_strength']     += $gsila;
                 if ($glovk  = $this->inventory_data[$slot_name]['glovk'])      $tmp_inv_stats['inv_dexterity']    += $glovk;
                 if ($ginta  = $this->inventory_data[$slot_name]['ginta'])      $tmp_inv_stats['inv_success']      += $ginta;
                 if ($gintel = $this->inventory_data[$slot_name]['gintel'])     $tmp_inv_stats['inv_intelligence'] += $gintel;

                 if ($minu   = $this->inventory_data[$slot_name]['minu'])       $tmp_inv_stats['inv_minu']  += $minu;
                 if ($maxu   = $this->inventory_data[$slot_name]['maxu'])       $tmp_inv_stats['inv_maxu']  += $maxu;
                 if ($extra_kick = $this->inventory_data[$slot_name]['extra_kick']) $tmp_inv_stats['inv_extra_kick'] += $extra_kick;

                 if ($krit   = $this->inventory_data[$slot_name]['mfkrit'])     $tmp_inv_stats['inv_krit']  += $krit;
                 if ($akrit  = $this->inventory_data[$slot_name]['mfakrit'])    $tmp_inv_stats['inv_akrit'] += $akrit;
                 if ($uvor   = $this->inventory_data[$slot_name]['mfuvorot'])   $tmp_inv_stats['inv_uvor']  += $uvor;
                 if ($auvor  = $this->inventory_data[$slot_name]['mfauvorot'])  $tmp_inv_stats['inv_auvor'] += $auvor;

                 if ($absorbs_uron = $this->inventory_data[$slot_name]['absorbs_uron']) $tmp_inv_stats['inv_absorbs_uron'] += $absorbs_uron;
                 if ($absorbs_krit = $this->inventory_data[$slot_name]['absorbs_krit']) $tmp_inv_stats['inv_absorbs_krit'] += $absorbs_krit;
                 if ($power_uron   = $this->inventory_data[$slot_name]['power_uron'])   $tmp_inv_stats['inv_power_uron']   += $power_uron;
                 if ($power_krit   = $this->inventory_data[$slot_name]['power_krit'])   $tmp_inv_stats['inv_power_krit']   += $power_krit;

                 if ($bron1  = $this->inventory_data[$slot_name]['bron1'])      $tmp_inv_stats['inv_bron1'] += $bron1;
                 if ($bron2  = $this->inventory_data[$slot_name]['bron2'])      $tmp_inv_stats['inv_bron2'] += $bron1;
                 if ($bron3  = $this->inventory_data[$slot_name]['bron3'])      $tmp_inv_stats['inv_bron3'] += $bron1;
                 if ($bron4  = $this->inventory_data[$slot_name]['bron4'])      $tmp_inv_stats['inv_bron4'] += $bron1;

                 if ($inv_ghp   = $this->inventory_data[$slot_name]['ghp'])     $tmp_inv_stats['hp']        += $inv_ghp;
                 if ($inv_gpw   = $this->inventory_data[$slot_name]['gmana'])   $tmp_inv_stats['mana']      += $inv_gpw;
                 if ($skill_fishing  = $this->inventory_data[$slot_name]['gskill_fishing']) $tmp_inv_stats['skill_fishing'] += $skill_fishing;
          }

       # $tmp_inv_stats = array('sila' => 0, 'lovk' => 0, 'inta' => 0, 'intel' => 0, 'hp' => 0, 'mana' => 0, 'skill_fishing' => 0);
       # $tmp_inv_stats = array('inv_strength' => 0, 'inv_dexterity' => 0, 'inv_success' => 0, 'inv_intelligence' => 0, 'hp' => 0, 'mana' => 0, 'skill_fishing' => 0);

       # ������� ��� ���� � ��������� �����
         $no_rem_slot = array('drink', 'food', 'ngbonus', 'fish_yha', '');
         if ($this->user['drink']    == 0) unset($no_rem_slot[0]);
         if ($this->user['food']     == 0) unset($no_rem_slot[1]);
         if ($this->user['ngbonus']  == 0) unset($no_rem_slot[2]);
         if ($this->user['fish_yha'] == 0) unset($no_rem_slot[3]);

         if (!empty($this->inventory_data)) {
         foreach ($itemtype_to_name as $k_idslot => $slot) {
          $slot_name = $slot[1];

          // echo "<br> <b>�����</b> ".$this->inventory_data[$slot_name]['id'].'-'.$this->user[$slot_name].'-'.$this->inventory_data[$slot_name]['name'];

           if (!empty($this->user[$slot_name]) && !in_array($slot_name, $no_rem_slot)) {
            if (!empty($this->inventory_data[$slot_name])) {

               if ($this->user[$slot_name] && $this->user[$slot_name] == $this->inventory_data[$slot_name]['id']) {

               # ������� ���
                 if ($this->inventory_data[$slot_name]['name'] == '����� ���������' && $this->user['invis']) {
                   $this->USER_bafcontrol('����� ���������', false);
                 }

                 $this->user[$slot_name] = 0;
                 /*
                 if ($gsila  = $this->inventory_data[$slot_name]['gsila'])  $tmp_inv_stats['inv_strength']     += $gsila;
                 if ($glovk  = $this->inventory_data[$slot_name]['glovk'])  $tmp_inv_stats['inv_dexterity']    += $glovk;
                 if ($ginta  = $this->inventory_data[$slot_name]['ginta'])  $tmp_inv_stats['inv_success']      += $ginta;
                 if ($gintel = $this->inventory_data[$slot_name]['gintel']) $tmp_inv_stats['inv_intelligence'] += $gintel;

                 if ($hp     = $this->inventory_data[$slot_name]['ghp'])    $tmp_inv_stats['hp']               += $hp;
                 if ($mana   = $this->inventory_data[$slot_name]['gmana'])  $tmp_inv_stats['mana']             += $mana;
                 */
                 if ($skill_fishing  = $this->inventory_data[$slot_name]['gskill_fishing']) $tmp_inv_stats['skill_fishing'] += $skill_fishing;
                 unset($this->inventory_data[$slot_name]);
                 $this->mod_inventory_data = true;
               } # else echo "<br> ���".$this->inventory_data[$slot_name]['id'].'-'.$this->inventory_data[$slot_name]['name'];
            } else {
               unset($this->inventory_data[$slot_name]);
               $this->mod_inventory_data = true;
            }

           } else if(empty($this->user[$slot_name]) && !empty($this->inventory_data[$slot_name])){

            if (!in_array($slot_name, $no_rem_slot)) {
             unset($this->inventory_data[$slot_name]);
             $this->mod_inventory_data = true;
             $this->user[$slot_name] = 0;
            }

           }
         }
         }

        # ��������� ����
          $eff_user                       = sql_row("SELECT sum(sila) as sila_sum,sum(lovk) as lovk_sum,sum(inta) as inta_sum,sum(intel) as intel_sum,sum(inv_ghp) as inv_ghp_sum,sum(inv_gpw) as inv_gpw_sum,sum(inv_minu) as inv_minu_sum,sum(inv_maxu) as inv_maxu_sum,sum(inv_krit) as inv_krit_sum,sum(inv_akrit) as inv_akrit_sum,sum(inv_uvor) as inv_uvor_sum,sum(inv_auvor) as inv_auvor_sum,sum(inv_bron) as inv_bron_sum,sum(inv_absorbs_uron) as inv_absorbs_uron_sum,sum(inv_absorbs_krit) as inv_absorbs_krit_sum,sum(inv_power_uron) as inv_power_uron_sum,sum(inv_power_krit) as inv_power_krit_sum,sum(inv_extra_kick) as inv_extra_kick_sum FROM `effects` WHERE `owner` = '".$this->user_id."' LIMIT 1;");
          $inv_ghp                       += abs($eff_user['inv_ghp_sum'])  + $tmp_inv_stats['inv_ghp'];
          $inv_gpw                       += abs($eff_user['inv_gpw_sum'])  + $tmp_inv_stats['inv_gpw'];
          $inv_minu                       = abs($eff_user['inv_minu_sum']) + $tmp_inv_stats['inv_minu'];
          $inv_maxu                       = abs($eff_user['inv_maxu_sum']) + $tmp_inv_stats['inv_maxu'];
          $inv_krit                       = abs($eff_user['inv_krit_sum']) + $tmp_inv_stats['inv_krit'];
          $inv_akrit                      = abs($eff_user['inv_akrit_sum'])+ $tmp_inv_stats['inv_akrit'];
          $inv_uvor                       = abs($eff_user['inv_uvor_sum']) + $tmp_inv_stats['inv_uvor'];
          $inv_auvor                      = abs($eff_user['inv_auvor_sum'])+ $tmp_inv_stats['inv_auvor'];
          $inv_bron1                      = abs($eff_user['inv_bron_sum']) + $tmp_inv_stats['inv_bron1'];
          $inv_bron2                      = abs($eff_user['inv_bron_sum']) + $tmp_inv_stats['inv_bron2'];
          $inv_bron3                      = abs($eff_user['inv_bron_sum']) + $tmp_inv_stats['inv_bron3'];
          $inv_bron4                      = abs($eff_user['inv_bron_sum']) + $tmp_inv_stats['inv_bron4'];

          $inv_absorbs_uron               = abs($eff_user['inv_absorbs_uron_sum']) + $tmp_inv_stats['absorbs_uron'];
          $inv_absorbs_krit               = abs($eff_user['inv_absorbs_krit_sum']) + $tmp_inv_stats['absorbs_krit'];
          $inv_power_uron                 = abs($eff_user['inv_power_uron_sum']) + $tmp_inv_stats['power_uron'];
          $inv_power_krit                 = abs($eff_user['inv_power_krit_sum']) + $tmp_inv_stats['power_krit'];
          $inv_extra_kick                 = abs($eff_user['inv_extra_kick_sum']) + $tmp_inv_stats['inv_extra_kick'];

        # ��������� ������ � �����
          /*
          $this->user['inv_strength']     = abs($this->user['inv_strength']     - $tmp_inv_stats['inv_strength']);
          $this->user['inv_dexterity']    = abs($this->user['inv_dexterity']    - $tmp_inv_stats['inv_dexterity']);
          $this->user['inv_success']      = abs($this->user['inv_success']      - $tmp_inv_stats['inv_success']);
          $this->user['inv_intelligence'] = abs($this->user['inv_intelligence'] - $tmp_inv_stats['inv_intelligence']);
          */
          $this->user['inv_strength']     = abs($tmp_inv_stats['inv_strength']);
          $this->user['inv_dexterity']    = abs($tmp_inv_stats['inv_dexterity']);
          $this->user['inv_success']      = abs($tmp_inv_stats['inv_success']);
          $this->user['inv_intelligence'] = abs($tmp_inv_stats['inv_intelligence']);

        # ���� ����� �� ��������
          if (empty($this->inventory_data)) {
            $this->user['inv_strength']     = 0;
            $this->user['inv_dexterity']    = 0;
            $this->user['inv_success']      = 0;
            $this->user['inv_intelligence'] = 0;
          }

        # ���������
          $this->user['inv_ghp']          = $inv_ghp;
          $this->user['inv_gpw']          = $inv_gpw;

          $this->user['inv_minu']         = $inv_minu;
          $this->user['inv_maxu']         = $inv_maxu;
          $this->user['inv_krit']         = $inv_krit;
          $this->user['inv_akrit']        = $inv_akrit;
          $this->user['inv_uvor']         = $inv_uvor;
          $this->user['inv_auvor']        = $inv_auvor;

          $this->user['inv_absorbs_uron'] = $inv_absorbs_uron;
          $this->user['inv_absorbs_krit'] = $inv_absorbs_krit;
          $this->user['inv_power_uron']   = $inv_power_uron;
          $this->user['inv_power_krit']   = $inv_power_krit;

          $this->user['inv_extra_kick']   = $inv_extra_kick;

          $this->user['inv_bron1']        = $inv_bron1;
          $this->user['inv_bron2']        = $inv_bron2;
          $this->user['inv_bron3']        = $inv_bron3;
          $this->user['inv_bron4']        = $inv_bron4;

          $this->user['stuff_cost']       = 0;
          $this->user['maxWeightget']     = $this->USER_inv_maxWeightget();
          $this->user['KBO']              = 0;


          $maxhp                          = abs($this->user['vinos'] * 10) + $inv_ghp;
          $maxmana                        = abs(($this->user['sila'] + $this->user['inv_strength']) * 6) + $inv_gpw;
          $this->user['maxhp']            = $maxhp;
          $this->user['maxmana']          = $maxmana;
          if ($this->user['hp']   > $maxhp)   { $this->user['hp']   = $maxhp;   }
          if ($this->user['mana'] > $maxmana) { $this->user['mana'] = $maxmana; }

        # ��������� �� ���������� �����
          $this->upd_sql .= ",
    			`maxhp`             = ".$maxhp.",
    			`maxmana`           = ".$maxmana.",
    			`hp`                = ".$this->user['hp'].",
    			`mana`              = ".$this->user['mana'].",

    			`hp_precise`        = ".$this->user['hp'].",
    			`pw_precise`        = ".$this->user['mana'].",
    			`fullhptime`        = ".time().",
    			`fullmptime`        = ".time().",

    			`inv_strength`      = ".abs($this->user['inv_strength']).",
    			`inv_dexterity`     = ".abs($this->user['inv_dexterity']).",
    			`inv_success`       = ".abs($this->user['inv_success']).",
    			`inv_intelligence`  = ".abs($this->user['inv_intelligence']).",

    			`inv_ghp`           = ".abs($inv_ghp).",
    			`inv_gpw`           = ".abs($inv_gpw).",

                `inv_minu`          = ".abs($inv_minu).",
                `inv_maxu`          = ".abs($inv_maxu).",
                `inv_extra_kick`    = ".abs($inv_extra_kick).",
                `inv_krit`          = ".abs($inv_krit).",
                `inv_akrit`         = ".abs($inv_akrit).",
                `inv_uvor`          = ".abs($inv_uvor).",
                `inv_auvor`         = ".abs($inv_auvor).",

                `inv_absorbs_uron`  = ".abs($inv_absorbs_uron).",
                `inv_absorbs_krit`  = ".abs($inv_absorbs_krit).",
                `inv_power_uron`    = ".abs($inv_power_uron).",
                `inv_power_krit`    = ".abs($inv_power_krit).",

                `inv_bron1`         = ".abs($inv_bron1).",
                `inv_bron2`         = ".abs($inv_bron2).",
                `inv_bron3`         = ".abs($inv_bron3).",
                `inv_bron4`         = ".abs($inv_bron4).",

    			`stuff_cost`        = 0,
    			`maxWeightget`      = abs(".$this->user['maxWeightget']."),
    			`KBO`               = 0,

    			`skill_fishing`     = `skill_fishing`        - ".$tmp_inv_stats['skill_fishing'].",

                 belt = 0, naruchi = 0, sergi = 0, kulon = 0, perchi = 0, weap = 0, weap2 = 0, bron = 0, helm = 0, shit = 0, boots = 0,
                 r1 = 0, r2 = 0, r3 = 0, r4 = 0,
                 m1 = 0, m2 = 0, m3 = 0, m4 = 0, m5 = 0, m6 = 0,
                 stone1 = 0, stone2 = 0, stone3 = 0, stone4 = 0, stone5 = 0, stone6 = 0, stone7 = 0";

          sql_query("UPDATE `inventory` SET `dressed` = 0, `update` = '".date('Y.m.d H:i:s')."' WHERE `dressed` = 1 AND `owner` = '".$this->user_id."' AND type != 39 AND type != 40 AND type != 41 AND type != 42;");

        # ��������� ����� ������
          if ($bd_upd) $this->USER_upd_sql();
        # ��������� ����� ������
          $this->mod_inventory_data = true;
          $this->USER_save_inventory_data();

        # ������� ����
        # unlink($this->file_inventory_data);

          return true;
       }
      return false;
     }
    }

  #-----------------------------------------------
  # ����� �������
    function USER_remove_item($slot_type){
     global $itemtype_to_name;

      if (is_numeric($slot_type)) {
        # ���� ������ �����
        $slot = @$itemtype_to_name[$slot_type][1];
      } else {
        # ���� ������ ���
        $slot = $slot_type;
      }

      if (!empty($slot)) {

       if ($slot && $this->user[$slot]) {

        $KBO_to_level     = $this->USER_inv_KBO_to_level();
        $animalsWeightget = $this->USER_inv_animalsWeightget();

      # ��� ���
        if ($this->inventory_data[$slot]['type'] != 25) {
          $KBO_stuff_cost = "
              `users`.`stuff_cost`       = abs(`users`.`stuff_cost` - `inventory`.`cost`),
              `users`.`KBO`              = abs(`users`.`stuff_cost` / $KBO_to_level * 100),
          ";
        } else $KBO_stuff_cost = "";

      # ������� ���
        if ($this->inventory_data[$slot]['name'] == '����� ���������' && $this->user['invis']) {
         $this->USER_bafcontrol('����� ���������', false);
        }

  	   	if (sql_query("UPDATE `users`, `inventory` SET
              `inventory`.`dressed`      = 0,
              `inventory`.`update`       = '".date('Y.m.d H:i:s')."',
              `users`.{$slot}            = 0,

  			  `users`.`maxhp`            = abs(`users`.`maxhp` - `inventory`.`ghp`),
  			  `users`.`maxmana`          = abs(`users`.`maxmana` - `inventory`.`gmana` - (`inventory`.`gsila` * 6)),

    	      `users`.`inv_strength`     = abs(`users`.`inv_strength` - `inventory`.`gsila`),
    	      `users`.`inv_dexterity`    = abs(`users`.`inv_dexterity` - `inventory`.`glovk`),
    	      `users`.`inv_success`      = abs(`users`.`inv_success` - `inventory`.`ginta`),
    	      `users`.`inv_intelligence` = abs(`users`.`inv_intelligence` - `inventory`.`gintel`),

              `users`.`inv_ghp`          = abs(`users`.`inv_ghp`   - `inventory`.`ghp`),
              `users`.`inv_gpw`          = abs(`users`.`inv_gpw`   - `inventory`.`gmana`),

              `users`.`inv_minu`         = abs(`users`.`inv_minu`  - `inventory`.`minu`),
              `users`.`inv_maxu`         = abs(`users`.`inv_maxu`  - `inventory`.`maxu`),
              `users`.`inv_extra_kick`   = abs(`users`.`inv_extra_kick` - `inventory`.`extra_kick`),

              `users`.`inv_krit`         = abs(`users`.`inv_krit`  - `inventory`.`mfkrit`),
              `users`.`inv_akrit`        = abs(`users`.`inv_akrit` - `inventory`.`mfakrit`),
              `users`.`inv_uvor`         = abs(`users`.`inv_uvor`  - `inventory`.`mfuvorot`),
              `users`.`inv_auvor`        = abs(`users`.`inv_auvor` - `inventory`.`mfauvorot`),

              `users`.`inv_absorbs_uron` = abs(`users`.`inv_absorbs_uron` - `inventory`.`absorbs_uron`),
              `users`.`inv_absorbs_krit` = abs(`users`.`inv_absorbs_krit` - `inventory`.`absorbs_krit`),
              `users`.`inv_power_uron`   = abs(`users`.`inv_power_uron`   - `inventory`.`power_uron`),
              `users`.`inv_power_krit`   = abs(`users`.`inv_power_krit`   - `inventory`.`power_krit`),

              `users`.`inv_bron1`        = abs(`users`.`inv_bron1` - `inventory`.`bron1`),
              `users`.`inv_bron2`        = abs(`users`.`inv_bron2` - `inventory`.`bron2`),
              `users`.`inv_bron3`        = abs(`users`.`inv_bron3` - `inventory`.`bron3`),
              `users`.`inv_bron4`        = abs(`users`.`inv_bron4` - `inventory`.`bron4`),

              `users`.`kit_lord`         = 0,

  			  `users`.`skill_fishing`    = abs(`users`.`skill_fishing` - `inventory`.`gskill_fishing`),
               ".$KBO_stuff_cost."
              `users`.`maxWeightget`     = abs((((`users`.`sila` + `users`.`inv_strength`) * 6) + $animalsWeightget))

  			WHERE `inventory`.`id` = `users`.{$slot} AND `inventory`.`dressed` = 1 AND `inventory`.`owner` = '".$this->user_id."' AND `users`.`id` = '".$this->user_id."';")
  		) {

          # ��������� ������ ������� �� ��������� ����

              # �������� � �������� �����
                $this->user[$slot]            = 0;

                $this->user['maxhp']         -= $this->inventory_data[$slot]['ghp'];
                $this->user['maxmana']       -= abs($this->inventory_data[$slot]['gmana'] - ($this->inventory_data[$slot]['gsila'] * 6));
                 
                $this->user['inv_strength']  -= $this->inventory_data[$slot]['gsila'];
                $this->user['inv_dexterity'] -= $this->inventory_data[$slot]['glovk'];
                $this->user['inv_success']   -= $this->inventory_data[$slot]['ginta'];
                $this->user['inv_intelligence'] -= $this->inventory_data[$slot]['gintel'];

                $this->user['inv_ghp']       -= $this->inventory_data[$slot]['ghp'];
                $this->user['inv_gpw']       -= $this->inventory_data[$slot]['gmana'];

                $this->user['inv_minu']      -= $this->inventory_data[$slot]['minu'];
                $this->user['inv_maxu']      -= $this->inventory_data[$slot]['maxu'];
                $this->user['inv_extra_kick']-= $this->inventory_data[$slot]['extra_kick'];

                $this->user['inv_krit']      -= $this->inventory_data[$slot]['mfkrit'];
                $this->user['inv_akrit']     -= $this->inventory_data[$slot]['mfakrit'];
                $this->user['inv_uvor']      -= $this->inventory_data[$slot]['mfuvorot'];
                $this->user['inv_auvor']     -= $this->inventory_data[$slot]['mfauvorot'];

                $this->user['inv_absorbs_uron'] -= $this->inventory_data[$slot]['absorbs_uron'];
                $this->user['inv_absorbs_krit'] -= $this->inventory_data[$slot]['absorbs_krit'];
                $this->user['inv_power_uron']   -= $this->inventory_data[$slot]['power_uron'];
                $this->user['inv_power_krit']   -= $this->inventory_data[$slot]['power_krit'];

                $this->user['inv_bron1']     -= $this->inventory_data[$slot]['bron1'];
                $this->user['inv_bron2']     -= $this->inventory_data[$slot]['bron2'];
                $this->user['inv_bron3']     -= $this->inventory_data[$slot]['bron3'];
                $this->user['inv_bron4']     -= $this->inventory_data[$slot]['bron4'];

                $this->user['skill_fishing'] -= $this->inventory_data[$slot]['gskill_fishing'];

             # ��� ���
               if ($this->inventory_data[$slot]['type'] != 25) {
                $this->user['stuff_cost']    -= $this->inventory_data[$slot]['cost'];
                $this->user['KBO']            = floor($this->user['stuff_cost'] / $this->USER_inv_KBO_to_level() * 100);
               }

                $this->user['maxWeightget']   = $this->USER_inv_maxWeightget();

                 if ($this->user['KBO'] > 200) {
                     //$text = 'USER_remove_item � '.$this->user['login'].' ����� KBO '.$this->user['KBO'].''.$_SERVER["REQUEST_URI"];
                     //send_user_letter(1, '[KBO]', 'KBO USER_remove_item', $text);
              	     sql_query("UPDATE `users` SET KBO = 200 WHERE `id` = '".$this->user['id']."' LIMIT 1;");
                 }

                $tmp_item = $this->inventory_data[$slot];

                unset($this->inventory_data[$slot]);
                $this->mod_inventory_data = true;

            return array("slot" => $slot, "item" => $tmp_item);
  		  } else {

            return false;
  		  }

       } else {    
        // echo "��� ����";
       }
      }
    }

  #-----------------------------------------------
  # ������ ����
    function USER_put_item($put_id, $tmp_item = false, $slot_name = "", $parm = array()){
     global $itemtype_to_name;

     if ($tmp_item) {
	    $put_item = $tmp_item;
        $put_id   = $put_item['id'];
     } else {
	    $put_item = sql_row("SELECT * FROM `inventory` WHERE  `duration` < `maxdur` AND `id` = '".$put_id."' AND `dressed` = 0 AND `owner` =  '".$this->user_id."' AND `setsale` = 0 AND `setbuying` = 0 AND `in_box` = 0 LIMIT 1;");
     }

       $a_sila  = $this->user['sila']  + $this->user['inv_strength'];
       $a_lovk  = $this->user['lovk']  + $this->user['inv_dexterity'];
       $a_inta  = $this->user['inta']  + $this->user['inv_success'];
       $a_intel = $this->user['intel'] + $this->user['inv_intelligence'];

       if (!empty($put_item['id'])
           && $this->user['level'] >= $put_item['nlevel']
           && $a_sila  >= $put_item['nsila']
           && $a_lovk  >= $put_item['nlovk']
           && $a_inta  >= $put_item['ninta']
           && $this->user['vinos'] >= $put_item['nvinos']
           && $a_intel >= $put_item['nintel']
           && $this->user['mudra'] >= $put_item['nmudra']
           && $this->user['skill_fishing'] >= $put_item['nskill_fishing']
       ) {

         $slot = $slot_name;
         if (empty($slot_name)) {

            $slot = @$itemtype_to_name[$put_item['type']][1];

        	if($put_item['type'] == 5) {
        		    if(!$this->user['r1']) { $slot = 'r1';}
        		elseif(!$this->user['r2']) { $slot = 'r2';}
        		elseif(!$this->user['r3']) { $slot = 'r3';}
        		elseif(!$this->user['r4']) { $slot = 'r4';}
        		else {
        			$slot = 'r1';
        			$this->USER_remove_item(5);
        		}
        	} elseif($put_item['type']==20) {
        		    if(!$this->user['stone1']) { $slot = 'stone1';}
        		elseif(!$this->user['stone2']) { $slot = 'stone2';}
        		elseif(!$this->user['stone3']) { $slot = 'stone3';}
        		elseif(!$this->user['stone4']) { $slot = 'stone4';}
        		elseif(!$this->user['stone5']) { $slot = 'stone5';}
        		elseif(!$this->user['stone6']) { $slot = 'stone6';}
        		elseif(!$this->user['stone7']) { $slot = 'stone7';}
        		else {
        			$slot = 'stone1';
        		   	$this->USER_remove_item(31);
        		}
        	} elseif($put_item['type'] == 25) {
        		    if(!$this->user['m1']) { $slot = 'm1';}
        		elseif(!$this->user['m2']) { $slot = 'm2';}
        		elseif(!$this->user['m3']) { $slot = 'm3';}
        		elseif(!$this->user['m4']) { $slot = 'm4';}
        		elseif(!$this->user['m5']) { $slot = 'm5';}
        		elseif(!$this->user['m6']) { $slot = 'm6';}
                else {
        			$slot = 'm1';
        			$this->USER_remove_item(12);
        		}
        	} else {
        		$this->USER_remove_item($put_item['type']);
        	}
         } else {
           $slot = $itemtype_to_name[$slot_name][1];
         }

          # �������� ������� �� ���� � ��������� �������������� �����
            $slot_name = $slot;

      		if ($slot_name && !($put_item['type'] == 25 && $this->user['level'] < 1)) {
                $KBO_to_level     = $this->USER_inv_KBO_to_level();
                $animalsWeightget = $this->USER_inv_animalsWeightget();

              # ��� ���
                if ($put_item['type'] != 25) {
                  $KBO_stuff_cost = "
                    `users`.`stuff_cost`       = `users`.`stuff_cost`  + `inventory`.`cost`,
                    `users`.`KBO`              = floor(`users`.`stuff_cost` / $KBO_to_level * 100),
                  ";
                } else $KBO_stuff_cost = "";

              # ���� ���
                if ($put_item['name'] == '����� ���������' && $this->user['invis'] == 0) {
                  $this->USER_bafcontrol('����� ���������', true);
                }

                $kit_lord = 0; # ����� ��� ���� ���������� ������

                if (substr_count($put_item['name'], "���������� ������")) {
                  $er = 1;
                   foreach ($this->inventory_data as $k => $v_data) {
                     if (substr_count($v_data['name'], "���������� ������")) $er++;
                   }
                 # ���� ��� 14 ��������� �� ���������� ��������
                   if ($er == 14) {
                     $kit_lord = 1;
                   }
                }

                if (sql_query("UPDATE `users`, `inventory` SET
                    `users`.{$slot_name}       = '".$put_id."',
                    `inventory`.`dressed`      = 1,

      			    `users`.`maxhp`            = `users`.`maxhp`   + `inventory`.`ghp`,
      			    `users`.`maxmana`          = `users`.`maxmana` + `inventory`.`gmana` + (`inventory`.`gsila` * 6),

      			    `users`.`inv_strength`     = `users`.`inv_strength` + `inventory`.`gsila`,
      			    `users`.`inv_dexterity`    = `users`.`inv_dexterity` + `inventory`.`glovk`,
      			    `users`.`inv_success`      = `users`.`inv_success` + `inventory`.`ginta`,
      			    `users`.`inv_intelligence` = `users`.`inv_intelligence` + `inventory`.`gintel`,

                    `users`.`inv_ghp`          = `users`.`inv_ghp`   + `inventory`.`ghp`,
                    `users`.`inv_gpw`          = `users`.`inv_gpw`   + `inventory`.`gmana`,

                    `users`.`inv_minu`         = `users`.`inv_minu`  + `inventory`.`minu`,
                    `users`.`inv_maxu`         = `users`.`inv_maxu`  + `inventory`.`maxu`,
                    `users`.`inv_extra_kick`   = `users`.`inv_extra_kick` + `inventory`.`extra_kick`,

                    `users`.`inv_krit`         = `users`.`inv_krit`  + `inventory`.`mfkrit`,
                    `users`.`inv_akrit`        = `users`.`inv_akrit` + `inventory`.`mfakrit`,
                    `users`.`inv_uvor`         = `users`.`inv_uvor`  + `inventory`.`mfuvorot`,
                    `users`.`inv_auvor`        = `users`.`inv_auvor` + `inventory`.`mfauvorot`,

                    `users`.`inv_absorbs_uron` = `users`.`inv_absorbs_uron`  + `inventory`.`absorbs_uron`,
                    `users`.`inv_absorbs_krit` = `users`.`inv_absorbs_krit` + `inventory`.`absorbs_krit`,
                    `users`.`inv_power_uron`   = `users`.`inv_power_uron`  + `inventory`.`power_uron`,
                    `users`.`inv_power_krit`   = `users`.`inv_power_krit` + `inventory`.`power_krit`,

                    `users`.`inv_bron1`        = `users`.`inv_bron1` + `inventory`.`bron1`,
                    `users`.`inv_bron2`        = `users`.`inv_bron2` + `inventory`.`bron2`,
                    `users`.`inv_bron3`        = `users`.`inv_bron3` + `inventory`.`bron3`,
                    `users`.`inv_bron4`        = `users`.`inv_bron4` + `inventory`.`bron4`,

                    `users`.`kit_lord`         = '".$kit_lord."',

                     ".$KBO_stuff_cost."

                    `users`.`skill_fishing`    = `users`.`skill_fishing` + `inventory`.`gskill_fishing`,
                    `users`.`maxWeightget`     = ((`users`.`sila` + `users`.`inv_strength`) * 6 + $animalsWeightget)

      			   WHERE
      			    `inventory`.`id`           = '".$put_id."' AND
      			    `inventory`.`dressed`      = 0 AND
      			    `inventory`.`owner`        = '".$this->user_id."' AND

      			    `users`.`sila` != 0 AND
      			    `users`.`lovk` != 0 AND
      			    `users`.`inta` != 0 AND

      			    `users`.`id` = '".$this->user_id."';")) {

      			       $put_item['dressed']             = 1;
      			       $this->user[$slot_name]          = $put_id;

                       $this->user['maxhp']            += $put_item['ghp'];
                       $this->user['maxmana']          += abs($put_item['gmana'] + ($put_item['gsila'] * 6));

                       $this->user['inv_strength']     += $put_item['gsila'];
                       $this->user['inv_dexterity']    += $put_item['glovk'];
                       $this->user['inv_success']      += $put_item['ginta'];
                       $this->user['inv_intelligence'] += $put_item['gintel'];

                       $this->user['inv_ghp']          += $put_item['ghp'];
                       $this->user['inv_gpw']          += $put_item['gmana'];

                       $this->user['inv_minu']         += $put_item['minu'];
                       $this->user['inv_maxu']         += $put_item['maxu'];
                       $this->user['inv_extra_kick']   += $put_item['extra_kick'];

                       $this->user['inv_krit']         += $put_item['mfkrit'];
                       $this->user['inv_akrit']        += $put_item['mfakrit'];
                       $this->user['inv_uvor']         += $put_item['mfuvorot'];
                       $this->user['inv_auvor']        += $put_item['mfauvorot'];

                       $this->user['inv_absorbs_uron'] += $put_item['absorbs_uron'];
                       $this->user['inv_absorbs_krit'] += $put_item['absorbs_krit'];
                       $this->user['inv_power_uron']   += $put_item['power_uron'];
                       $this->user['inv_power_krit']   += $put_item['power_krit'];

                       $this->user['inv_bron1']        += $put_item['bron1'];
                       $this->user['inv_bron2']        += $put_item['bron2'];
                       $this->user['inv_bron3']        += $put_item['bron3'];
                       $this->user['inv_bron4']        += $put_item['bron4'];

                       $this->user['skill_fishing']    += $put_item['gskill_fishing'];

                       $old_stuff_cost                  = $this->user['stuff_cost'];

                   # ��� ���
                     if ($put_item['type'] != 25) {
                       $this->user['stuff_cost']       += $put_item['cost'];
                       $this->user['KBO']               = floor($this->user['stuff_cost'] / $this->USER_inv_KBO_to_level() * 100);
                     }
                       $this->user['maxWeightget']      = $this->USER_inv_maxWeightget();

                 if ($this->user['KBO'] > 200) {
                     //$text = 'USER_put_item � '.$this->user['login'].' <br> slot '.$slot.'  <br>  old_stuff_cost'.$old_stuff_cost.'   <br> stuff_cost'.$this->user['stuff_cost'].'  <br>   KBO_to_level'.$KBO_to_level.'  <br>      ����� KBO '.$this->user['KBO'].' <br>HTTP_REFERER '.$_SERVER["HTTP_REFERER"].' <br>REQUEST_URI '.$_SERVER["REQUEST_URI"];
                     //send_user_letter(1, '[KBO]', 'KBO USER_put_item', $text);
              	     sql_query("UPDATE `users` SET `KBO` = 200 WHERE `id` = '".$this->user['id']."' LIMIT 1;");
                 }
                       $this->inventory_data[$slot_name] = $put_item;
                       $this->mod_inventory_data = true;

      			       return $slot_name;
                } else return false;
      	      }

       } else { return false; }

    }

  #-----------------------------------------------
  # ������ ������ �� �������������� ������� �������
    function USER_keep_subject($row_item){

      $a_sila  = $this->user['sila']  + $this->user['inv_strength'];
      $a_lovk  = $this->user['lovk']  + $this->user['inv_dexterity'];
      $a_inta  = $this->user['inta']  + $this->user['inv_success'];
      $a_intel = $this->user['intel'] + $this->user['inv_intelligence'];

      if ($a_sila  != 0
       && $a_lovk  != 0
       && $a_inta  != 0
       && $a_sila  >= $row_item['nsila']
       && $a_lovk  >= $row_item['nlovk']
       && $a_inta  >= $row_item['ninta']
       && $this->user['vinos'] >= $row_item['nvinos']
       && $a_intel >= $row_item['nintel']
       && $this->user['mudra'] >= $row_item['nmudra']
       && $this->user['level'] >= $row_item['nlevel']
       && $this->user['skill_fishing'] >= $row_item['nskill_fishing']
       && $row_item['duration']        < $row_item['maxdur']
         ) {
             return false;
           } else {
      	     return true;
      	   }
    }

  #-----------------------------------------------
  # �������� ������������� ����� �� ���������������
    function USER_�heck_char_items(){
     global $itemtype_to_name;
       $tmp_rem_count = 0;
       foreach ($itemtype_to_name as $k_type => $slot) {
         if (!empty($this->user[$slot[1]]) && !empty($this->inventory_data[$slot[1]]) && $slot[1] != 'drink' && $slot[1] != 'food' && $slot[1] != 'ngbonus' && $slot[1] != 'fish_yha') {
          if ($this->USER_keep_subject($this->inventory_data[$slot[1]])) {
            $this->USER_remove_item($k_type);
            $tmp_rem_count++;
          }
         }
       }
     # ���� ��� ��������
       if ($tmp_rem_count) return true;
     return false;
    }

  #-----------------------------------------------
  # ������� ���� ���� �������
    function USER_destruct_item($item, $upd_curWeightget = true){
     global $itemtype_to_name;

       if (empty($item['id'])) $destruct_item = sql_row("SELECT `id`, `type`, `owner`, `dressed` FROM `inventory` WHERE `id` = '".$item."' AND `owner` = '".$this->user_id."' LIMIT 1;");
       else                    $destruct_item = $item;

       $id   = (int)$destruct_item['id'];
       $slot = false;

       if ($id) {
        switch ($id) {
          case $this->user['sergi']:   $slot = 'sergi';     $destruct_item['type'] = 1;  break;
          case $this->user['kulon']:   $slot = 'kulon';     $destruct_item['type'] = 2;  break;
          case $this->user['naruchi']: $slot = 'naruchi';   $destruct_item['type'] = 22; break;
          case $this->user['belt']:    $slot = 'belt';      $destruct_item['type'] = 23; break;
          case $this->user['perchi']:  $slot = 'perchi';    $destruct_item['type'] = 9;  break;
          case $this->user['bron']:    $slot = 'bron';      $destruct_item['type'] = 4;  break;
          case $this->user['helm']:    $slot = 'helm';      $destruct_item['type'] = 8;  break;
          case $this->user['shit']:    $slot = 'shit';      $destruct_item['type'] = 10; break;
          case $this->user['boots']:   $slot = 'boots';     $destruct_item['type'] = 11; break;
          case $this->user['weap']:    $slot = 'weap';      $destruct_item['type'] = 3;  break;
          case $this->user['weap2']:   $slot = 'weap2';     $destruct_item['type'] = 46; break;

          case $this->user['drink']:   $slot = 'drink';     $destruct_item['type'] = 39; break;
          case $this->user['food']:    $slot = 'food';      $destruct_item['type'] = 40; break;
          case $this->user['ngbonus']: $slot = 'ngbonus';   $destruct_item['type'] = 41; break;
          case $this->user['fish_yha']:$slot = 'fish_yha';  $destruct_item['type'] = 42; break;

          case $this->user['r1']:      $slot = 'r1';     $destruct_item['type'] = 5;  break;
          case $this->user['r2']:      $slot = 'r2';     $destruct_item['type'] = 6;  break;
          case $this->user['r3']:      $slot = 'r3';     $destruct_item['type'] = 7;  break;
          case $this->user['r4']:      $slot = 'r4';     $destruct_item['type'] = 38; break;

          case $this->user['m1']:      $slot = 'm1';     $destruct_item['type'] = 12; break;
          case $this->user['m2']:      $slot = 'm2';     $destruct_item['type'] = 13; break;
          case $this->user['m3']:      $slot = 'm3';     $destruct_item['type'] = 14; break;
          case $this->user['m4']:      $slot = 'm4';     $destruct_item['type'] = 15; break;
          case $this->user['m5']:      $slot = 'm5';     $destruct_item['type'] = 16; break;
          case $this->user['m6']:      $slot = 'm6';     $destruct_item['type'] = 17; break;

          case $this->user['stone1']:$slot = 'stone1'; $destruct_item['type'] = 31; break;
          case $this->user['stone2']:$slot = 'stone2'; $destruct_item['type'] = 32; break;
          case $this->user['stone3']:$slot = 'stone3'; $destruct_item['type'] = 33; break;
          case $this->user['stone4']:$slot = 'stone4'; $destruct_item['type'] = 34; break;
          case $this->user['stone5']:$slot = 'stone5'; $destruct_item['type'] = 35; break;
          case $this->user['stone6']:$slot = 'stone6'; $destruct_item['type'] = 36; break;
          case $this->user['stone7']:$slot = 'stone7'; $destruct_item['type'] = 37; break;
        }

        # ������� ���� ����� �������
   		  //if ($destruct_item['dressed'] == 1 && $slot) $this->USER_remove_item($destruct_item['type']);
   		  if ($destruct_item['dressed'] == 1 && $slot) $this->USER_remove_item($slot);

        # ������� �� ���������
  	   	  sql_query("DELETE FROM `inventory` WHERE `id` = '".$id."' AND `owner` = '".$this->user_id."' LIMIT 1;");

        # ��������� ����� ���� �����
          if ($upd_curWeightget) $this->USER_inv_curWeightget(true);

          return true;
       } else {
          return false;
       }
    }

  #-----------------------------------------------
  # ������������ ����
    function USER_use_rune($rune_id, $target, $old_rune = false){
       global $user, $fbattle, $bet, $text_intervened, $text_adopted, $magicinf;

       $user = $this->user;
       $type_hint = $this->user['battle']?"battle_hint":"rune_hint";

       if ($old_rune) $use_rune = $old_rune;
       else           $use_rune = sql_row("SELECT * FROM `inventory` WHERE `owner` = '".$this->user_id."' AND `id` = '".$rune_id."' AND `dressed` = 1 LIMIT 1;");

       if (empty($use_rune['id'])) {
         return false;
       } else {

        if ($use_rune['duration'] < $use_rune['maxdur']) {

            $a_sila  = $this->user['sila']  + $this->user['inv_strength'];
            $a_lovk  = $this->user['lovk']  + $this->user['inv_dexterity'];
            $a_inta  = $this->user['inta']  + $this->user['inv_success'];
            $a_vinos = $this->user['vinos'];
            $a_intel = $this->user['intel'] + $this->user['inv_intelligence'];

        	if ((($a_sila     >= $use_rune['nsila'])  &&
        		 ($a_lovk     >= $use_rune['nlovk'])  &&
        		 ($a_inta     >= $use_rune['ninta'])  &&
        		 ($a_vinos    >= $use_rune['nvinos']) &&
        		 ($a_intel    >= $use_rune['nintel']) &&
        		 ($this->user['level']    >= $use_rune['nlevel']) &&
        		 (user_age($this->user)   >= $use_rune['nage'])   &&
        		 ($use_rune['type'] < 29 || in_array($use_rune['type'], array(50, 188, 39, 40, 41, 42, 90)) || $use_rune['can_use'])
        		) || $use_rune['magic'] == 248 # ��������� ������������ "���������"
                  || $use_rune['magic'] == 249 # ��������� ������������ "�������"
        	)
        	{

        	 $magic = $magicinf[$use_rune['magic']];

              if ($magic && is_numeric($rune_id) && $rune_id > 0) {
                $row = $use_rune;              
                include($_SERVER["DOCUMENT_ROOT"]."/magic/".stripslashes($magic['file']));

                  # ����� ���� ��������
                    $s_type = @$this->USER_slot_dreass_item($use_rune);

        			if($use_rune['maxdur'] <= ($use_rune['duration'] + 1) && $bet) {
                      $this->USER_destruct_item($use_rune);
                     # ��������� ������� �� ��������� �����
                       if ($s_type) {
                         unset($this->inventory_data[$s_type]);
                         $this->mod_inventory_data = true;
                       }

        			} else {

        			  if($use_rune['magic'] && $bet) {
            		   	 sql_query("UPDATE `inventory` SET `duration` = `duration` + 1 WHERE `id` = '".$rune_id."' LIMIT 1;");
                         $use_rune['duration'] += 1;
                       # ��������� ������� �� ��������� �����
                         if ($s_type) {
                           $this->inventory_data[$s_type] = $use_rune;
                           $this->mod_inventory_data = true;
                           $this->USER_save_inventory_data();
                         }
        			  }
        			}
                if ($this->mod_inventory_data) {
                   return true;
                }
              } else {
                $_SESSION[$type_hint] = "�� �� ������ ������������ ��� ����...";
              }

        	} else {
        	  if ($use_rune['type'] == 25) {
            	  $_SESSION[$type_hint] = "�� �� ������ ������������ ��� ����...";
        	  } else {
        	      $_SESSION[$type_hint] = "�� �� ������ ������������ ���� �������...";
        	  }
        	}
        }
       }
      return false;
    }

  #-----------------------------------------------
  # ��������� ������
    function USER_put_trauma($shanse_tr = 100, $persent_time = 100){

      	if($this->user['klan'] == "Test" || $this->user['level'] < 5 || $this->user['bot']) {
      		return "";
      	} elseif($this->user_id) {

              if (rand(1, 100) <= $shanse_tr && $this->user['sila'] > 0 && $this->user['lovk'] > 0 && $this->user['inta'] > 0) {

                $is_travma   = array ("�������� ������","������� ������",);
                $is_travma_l = array ("�������� ��������� ������ � �� ����� �������.","������� ��������� ������ � �� ����� �������.",);

                $abil = mt_rand(1, 3);

                $s    = 0; # ����
                $l    = 0; # ��������
                $i    = 0; # ��������
                $st   = 0 + floor(mt_rand(0, $this->user['level']) / 2);
                $letal_trv = false;
                $cnt_stat  = 0;

    			switch($abil){
    				case 1:
                        $s =  floor(($this->user['sila'] / 2) + $this->user['level']);
                        if (($this->user['sila'] - $s) <= 0) { $s = $this->user['sila']; $letal_trv = true; }
                        $trv      = '��������� �������������� ���� -'.$s;
                        $cnt_stat = $s;
                        $this->user['sila'] -= $cnt_stat;
                    break;
    				case 2:
                        $l = floor(($this->user['lovk'] / 2) + $this->user['level']);
                        if (($this->user['lovk'] - $l) <= 0) { $l = $this->user['lovk']; $letal_trv = true; }
                        $trv      = '��������� �������������� �������� -'.$l;
                        $cnt_stat = $l;
                        $this->user['lovk'] -= $cnt_stat;
                    break;
    				case 3:
                        $i =  floor(($this->user['inta'] / 2) + $this->user['level']);
                        if (($this->user['inta'] - $i) <= 0) { $i = $this->user['inta']; $letal_trv = true; }
                        $trv      = '��������� �������������� �������� -'.$i;
                        $cnt_stat = $i;
                        $this->user['inta'] -= $cnt_stat;
                    break;
    			}

                # ����� ������ (������� ��� ���������)
                  if ($letal_trv) $str_trv = $is_travma_l[$this->user['sex']];
                  else            $str_trv = $is_travma[$this->user['sex']];

                # ����� ������
                  if ($persent_time > 100) {$persent_time = 100;}
                  $time = (4000 * $this->user['level']) * $persent_time / 100;

                # ������ ������
      		   	  sql_query("INSERT INTO `effects` (`owner`, `name`, `time`, `type`, `sila`, `lovk`, `inta`, `opisan`, `total`, `from_whom`, `create_date`, `incity`) values (
                    '".$this->user_id."',
                    '������',
                    ".(time()+$time).",
                    12,
                    '".$s."',
                    '".$l."',
                    '".$i."',
                    '".$trv."',
                    '-".$cnt_stat."',
                    '���',
                    '".time()."',
                    '".INCITY."'
                  );");

                    $eff_all = $this->user['eff_all'].'12;';
                    $this->upd_sql .= ", `eff_all` = '".$eff_all."', `sila` = abs(`sila` - ".$s."), `lovk` = abs(`lovk` - ".$l."), `inta` = abs(`inta` - ".$i.") ";

                    $this->user['eff_all'] = $eff_all;
                    /*
                    $this->user['sila'] -= $s;
                    $this->user['lovk'] -= $l;
                    $this->user['inta'] -= $i;
                    */

                    if ($letal_trv) {
                     # ����� ��� ��� ����
                       $this->USER_remove_all_items();
                    } else {
                     # ����� ��� ��� ������� ������� �� ���������������
                       $this->USER_�heck_char_items();
                    }

                  return " <font color=\"red\">".$str_trv."</font>";
              } else {
                  return "";
              }
      	}
    }

  #-----------------------------------------------
  # ������� ������
    function USER_del_trauma($trv){
       if (!empty($trv['id'])) {
        sql_query("DELETE FROM `effects` WHERE `id` = '".$trv['id']."' LIMIT 1;");
        $eff_all = $this->USER_my_del_eff($trv['type']);
        $this->upd_sql .= ", `sila` = abs(`sila` + ".$trv['sila']."), `lovk` = abs(`lovk` + ".$trv['lovk']."), `inta` = abs(`inta` + ".$trv['inta']."), `eff_all` = '".$eff_all."'";

        $this->user['eff_all'] = $eff_all;
        $this->user['sila'] += $trv['sila'];
        $this->user['lovk'] += $trv['lovk'];
        $this->user['inta'] += $trv['inta'];

        # ���������� �����, ���� ���� ������ �� ����
        if($trv['sila']){
          $this->USER_inv_curWeightget(true);
        }

       }
    }

  #-----------------------------------------------
  # �������� ���� ���������������
    function USER_inv_animalsWeightget($view_new = false){
      $gruz = 0;
      if ($animals = unserialize($this->user['animals'])) {
       foreach ($animals as $k => $v_an) { if ($v_an['gruz']) $gruz += $v_an['gruz']; }
      }
      return abs($gruz);
    }

  #-----------------------------------------------
  # ������ ���� ���������� ���
    function USER_inv_maxWeightget($view_new = false){
      return (($this->user['sila'] + $this->user['inv_strength']) * 6) + $this->USER_inv_animalsWeightget();
    }

  #-----------------------------------------------
  # ������ ��� ���� ���������
    function USER_inv_curWeightget($view_new = false){
       $this->user['itemsWeight'] = strrtrim($this->user['itemsWeight'], '.00');

       if ($view_new) {
         $itemsWeight = sql_row("SELECT sum(massa) as massa_sum FROM `inventory` WHERE `owner` = '".$this->user_id."' AND `setsale` = 0 AND `setbuying` = 0 AND `in_box` = 0 AND `gift` = 0;");
         $this->user['itemsWeight'] = $itemsWeight['massa_sum'];
         $this->upd_sql .= ", `itemsWeight` = '".$this->user['itemsWeight']."'";
       }

       return $this->user['itemsWeight'];
    }

  #-----------------------------------------------
  # ���������� ����� � �������
    function USER_inv_items_count(){
     return sql_number("SELECT `id` FROM `inventory` WHERE `owner` = '".$this->user_id."' AND `setsale` = 0 AND `setbuying` = 0 AND `in_box` = 0 AND `gift` = 0; ");
    }

  #-----------------------------------------------
  # ��������� ��� ������
    function USER_inv_KBO_to_level(){
     global $kbo_user;
      if (!empty($kbo_user[$this->user['level']][0]))
           return $kbo_user[$this->user['level']][0];
      else return 1;
    }

  #-----------------------------------------------
  # ��������� ��� � ����� ���� ������� ���������
    function USER_inv_KBO_stuff(){
      $kbo_stuff = array('KBO' => 1, 'stuff' => 0);

      if (in_array($this->user['level'], array(0, 21, 30))) $kbo_stuff['KBO'] = 100;
      else $kbo_stuff['KBO'] = floor($this->user['stuff_cost'] / $this->USER_inv_KBO_to_level() * 100);

      $kbo_stuff['stuff'] = $this->user['stuff_cost'];
      return $kbo_stuff;
    }

  #-----------------------------------------------
  # �������� ��������������
    function USER_get_user_invstate($type){
        $vin       = 0;
        $mudra     = 0;
        $sila_vis  = '';
        $lovk_vis  = '';
        $int_vis   = '';
        $int_intel = '';

        $user_sila  = $this->user['sila'] + $this->user['inv_strength'];
        $user_lovk  = $this->user['lovk'] + $this->user['inv_dexterity'];
        $user_inta  = $this->user['inta'] + $this->user['inv_success'];
        $user_intel = $this->user['intel'] + $this->user['inv_intelligence'];

        if (substr_count($this->user['eff_all'], "12;")) {
           $eff_user = sql_row("SELECT sum(sila) as sila_sum,sum(lovk) as lovk_sum,sum(inta) as inta_sum FROM `effects` WHERE `owner` = '".$this->user_id."' AND (`sila` > 0 OR `lovk` > 0 OR `inta` > 0);");
        } else {
           $eff_user['sila_sum'] = 0;
           $eff_user['lovk_sum'] = 0;
           $eff_user['inta_sum'] = 0;
        }

        $inv_gsila  = $this->user['inv_strength'];
        $inv_glovk  = $this->user['inv_dexterity'];
        $inv_ginta  = $this->user['inv_success'];
        $inv_gintel = $this->user['inv_intelligence'];

        $vin2   = $this->user['vinos'] - $vin;
        $mudra2 = $this->user['mudra'];

    # ����
        $eff_sila   = $user_sila + strrtrim($eff_user['sila_sum'], '-') - $inv_gsila;
        $total_sila = $user_sila - $eff_sila;
        if(!$total_sila) { $sila_vis = 'class=invisible'; }
        $s_str = "<b id=str>".$user_sila."</b> <span id=str_info ".$sila_vis."> (<span id=str_base>".$eff_sila."</span> <span id=str_add>".get_sign($total_sila)." ".strrtrim($total_sila, '-')."</span>) </span>";

    # ��������
        $eff_lovk   = $user_lovk + strrtrim($eff_user['lovk_sum'], '-') - $inv_glovk;
        $total_lovk = $user_lovk - $eff_lovk;
        if(!$total_lovk) {  $lovk_vis = 'class=invisible'; }
        $s_dex = "<b id=dex>".$user_lovk."</b> <span id=dex_info ".$lovk_vis."> (<span id=dex_base>".$eff_lovk."</span> <span id=dex_add>".get_sign($total_lovk)." ".strrtrim($total_lovk, '-')."</span>) </span>";

    # ��������
        $eff_inta   = $user_inta + strrtrim($eff_user['inta_sum'], '-') - $inv_ginta;
        $total_inta = $user_inta - $eff_inta;
        if(!$total_inta) {  $int_vis = 'class=invisible'; }
        $s_suc = "<b id=suc>".$user_inta."</b> <span id=suc_info ".$int_vis."> (<span id=suc_base>".$eff_inta."</span> <span id=suc_add>".get_sign($total_inta)." ".strrtrim($total_inta, '-')."</span>) </span>";

    # ����������������
        $vi = "<b id=end>".$this->user['vinos']."</b> ".($vin?" <span id=end_info> (<span id=end_base>".$vin2."</span> <span id=end_add>+ ".$vin."</span>) </span>":"<span id=end_info class=invisible> (<span id=end_base>".$vin2."</span> <span id=end_add>- 0</span>) </span>");

    # ���������
        $eff_intel   = $user_intel - $inv_gintel;
        $total_intel = $user_intel - $eff_intel;
        if(!$total_intel) {  $int_intel = 'class=invisible'; }
        $s_int = "<b id=int>".$user_intel."</b> <span id=int_info ".$int_intel."> (<span id=int_base>".$eff_intel."</span> <span id=int_add>".get_sign($total_intel)." ".strrtrim($total_intel, '-')."</span>) </span>";

    # ��������
        $smu = "<b id=wis>".$this->user['mudra']."</b> ".($mudra?" <span id=wis_info> (<span id=wis_base>".$mudra2."</span> <span id=wis_add>+ ".$mudra."</span>) </span>":"<span id=wis_info class=invisible> (<span id=wis_base>".$mudra2."</span> <span id=wis_add>- 0</span>) </span>");

        $all = array();

        if ($type == 'all') {
         $all['sila']  = $s_str;
         $all['lovk']  = $s_dex;
         $all['inta']  = $s_suc;
         $all['vinos'] = $vi;
         $all['intel'] = $s_int;
         $all['mudra'] = $smu;
        }

        switch ($type) {
            case 'sila':  return $s_str;  break;
            case 'lovk':  return $s_dex;  break;
            case 'inta':  return $s_suc;  break;
            case 'vinos': return $vi;     break;
            case 'intel': return $s_int;  break;
            case 'mudra': return $smu;    break;
            case 'all':   return $all;    break;
        }
    }

  #-----------------------------------------------
  # �������� ������
    function USER_add_effect($parm = array()){

      $owner     = $this->user_id;
      $name      = $parm['name'];
      $time      = $parm['time'];  # (time()+$time)
      $type      = $parm['type'];

      $sila      = empty($parm['sila'])?0:$parm['sila'];
      $lovk      = empty($parm['lovk'])?0:$parm['lovk'];
      $inta      = empty($parm['inta'])?0:$parm['inta'];
      $vinos     = empty($parm['vinos'])?0:$parm['vinos'];
      $intel     = empty($parm['intel'])?0:$parm['intel'];

      $inv_ghp   = empty($parm['inv_ghp'])?0:$parm['inv_ghp'];     # ������� ������
      $inv_gpw   = empty($parm['inv_gpw'])?0:$parm['inv_gpw'];     # ������� ������������

      $inv_minu  = empty($parm['inv_minu'])?0:$parm['inv_minu'];   # ����������� ����
      $inv_maxu  = empty($parm['inv_maxu'])?0:$parm['inv_maxu'];   # ������������ ����

      $inv_krit  = empty($parm['inv_krit'])?0:$parm['inv_krit'];
      $inv_akrit = empty($parm['inv_akrit'])?0:$parm['inv_akrit'];
      $inv_uvor  = empty($parm['inv_uvor'])?0:$parm['inv_uvor'];
      $inv_auvor = empty($parm['inv_auvor'])?0:$parm['inv_auvor'];

      $inv_absorbs_uron = empty($parm['inv_absorbs_uron'])?0:$parm['inv_absorbs_uron'];
      $inv_absorbs_krit = empty($parm['inv_absorbs_krit'])?0:$parm['inv_absorbs_krit'];
      $inv_power_uron   = empty($parm['inv_power_uron'])?0:$parm['inv_power_uron'];
      $inv_power_krit   = empty($parm['inv_power_krit'])?0:$parm['inv_power_krit'];

      $opisan    = empty($parm['opisan'])?"":$parm['opisan'];
      $total     = empty($parm['total'])?"":$parm['total'];
      $from_whom = empty($parm['from_whom'])?"":$parm['from_whom'];

    # ������
 	  sql_query("INSERT INTO `effects` (`owner`, `name`, `time`, `type`,
                                        `sila`, `lovk`, `inta`, `vinos`, `intel`,
                                        `inv_ghp`, `inv_gpw`, `inv_minu`, `inv_maxu`, `inv_krit`, `inv_akrit`, `inv_uvor`, `inv_auvor`,
                                        `inv_absorbs_uron`, `inv_absorbs_krit`, `inv_power_uron`, `inv_power_krit`,
                                        `opisan`, `total`, `from_whom`, `create_date`, `incity`
                                        ) values (
                                        '".$owner."', '".$name."', '".$time."', '".$type."',
                                        '".$sila."', '".$lovk."', '".$inta."', '".$vinos."', '".$intel."',
                                        '".$inv_ghp."', '".$inv_gpw."', '".$inv_minu."', '".$inv_maxu."', '".$inv_krit."', '".$inv_akrit."', '".$inv_uvor."', '".$inv_auvor."',
                                        '".$inv_absorbs_uron."', '".$inv_absorbs_krit."', '".$inv_power_uron."', '".$inv_power_krit."',
                                        '".$opisan."', '".$total."', '".$from_whom."', '".time()."', '".INCITY."'
        );");
    }


  #-----------------------------------------------
  # ������ ������ ��������
    function USER_my_efflist(){
      $efflist = explode(";", $this->user['eff_all']);
      return $efflist;
    }

  #-----------------------------------------------
  # ������� ���� ������ �� ������
    function USER_my_del_eff($d_eff = ""){
      $efflist = $this->USER_my_efflist();
      if (!empty($efflist)) {
        foreach ($efflist as $k => $c_eff) {
         if ($d_eff == $c_eff) {
           unset($efflist[$k]);
           break;
         }
        }
       return  implode(";", $efflist);
      } else {
       return  "";
      }
      # $eff_all = preg_replace("@1022;@", "", $this->user['eff_all'], 1);
    }

  #-----------------------------------------------
  # ���������� ������ ������
    function USER_bafcontrol($baf_name = "", $baf_comand = ""){

     switch ($baf_name) {
       case '����� ���������': # ��� ����������� �� �������� "����� ���������"

          if ($baf_comand == true) {

             if ($this->user_id == 1) $time_use = time() + 10;
             else $time_use = time() + 24*60*60;

             sql_query("INSERT INTO `effects` (`owner`,`name`,`time`,`type`,`opisan`,`from_whom`) values ('".$this->user_id."','����� ���������','{$time_use}',1022,'��� �� ����� ������ ������','����� ���������');");
             $eff_all = $this->user['eff_all'].'1022;';
             $this->USER_upd_sql(", `invis` = 1, `eff_all` = '".$eff_all."'");
             $this->user['eff_all'] = $eff_all;

             CHAT::chat_system($this->user, CHAT::chat_js_login($this->user)." ".($this->user['sex']?"����":"�����")." ���������", $this->user['room'], array('addXMPP' => true));
             $this->user['invis']   = 1;

             XMPP::changeInvisible(CHAT::chat_format_login($this->user['login']), 1);
             CHAT::chat_addXMPP();

           # ��������� ������ �������� �������
             CHAT::chat_command($this->user, "params|isInvisible=1", array('addXMPP' => true));

          } else {
             sql_query("DELETE FROM `effects` WHERE `type` = 1022 AND `owner` = '".$this->user_id."' AND `name` = '����� ���������' LIMIT 1;");
             $eff_all = str_replace('1022;', '', $this->user['eff_all']);
             $this->USER_upd_sql(", `invis` = 0, `eff_all` = '".$eff_all."'");
             $this->user['invis']   = 0;
             $this->user['eff_all'] = $eff_all;

             XMPP::changeInvisible(CHAT::chat_format_login($this->user['login']), 0);
             CHAT::chat_command($this->user, "params|isInvisible=0", array('addXMPP' => true));
          }

       break;
       case '':

       break;
     }

    }
  #-----------------------------------------------
  # ���������� �������� ������
    function USER_questcontrol($quest_parm = ""){

     $del_q = function ($qid, $qlist, $str = false){
       $n_qlist  = array();
       $ns_qlist = "";
       foreach ($qlist as $k => $v_id) { if ($v_id != $qid && $v_id != '') { $n_qlist[] = $v_id; $ns_qlist .= $v_id.";"; } }

       if ($str == true) return $ns_qlist; else return $n_qlist;
     };


     $quest_all = explode(";", $this->user['quest_all']);
     $n_quest_all = "";

     if (isset($quest_parm['del_quest'])) {
       $n_quest_all = $del_q($quest_parm['del_quest'], $quest_all, true);
       //echo $n_quest_all;
       $this->USER_upd_sql(", `quest_all` = '".$n_quest_all."'");
     }

     if (isset($quest_parm['add_quest'])) {
       $n_quest_all = $this->user['quest_all'].$quest_parm['add_quest'].";";
       $this->USER_upd_sql(", `quest_all` = '".$n_quest_all."'");
     }

    }


  #-----------------------------------------------
  # ������� �� � ����
    function id_to_user($data_user) {

        if (!is_numeric($data_user) && !empty($data_user['id'])) {
          $id_user = (int)$data_user['id'];
        } else {
          $id_user = (int)$data_user;
        }

        $old_id = $id_user;

    	if($id_user > _BOTSEPARATOR_) {
            if (!is_numeric($data_user) && !empty($data_user['id'])) {
              $id_user   = $data_user['id'];
    	      $bots      = $data_user;
            } else {
    	      $bots = sql_row("SELECT * FROM `bots` WHERE `id` = '".$id_user."' LIMIT 1;");
            }

            if ($this->user_id == $bots['prototype']) $to_user = $this->user; else $to_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$bots['prototype']."' LIMIT 1;");

    		$to_user['bot_id'] = $to_user['id'];
    		$to_user['login']  = $bots['name'];
    		$to_user['hp']     = $bots['hp'];
    		$to_user['mana']   = $bots['mana'];
    		$to_user['id']     = $bots['id'];
    		$to_user['battle'] = $bots['battle'];

        	  $new_stats = array();
        	  $new_stats = unserialize($bots['stats']);

              if ($new_stats) {
                $isbots_stats          = 1;
                $new_stats['hp']       = $bots['hp'];
                $new_stats['mana']     = $bots['mana'];

        	    $new_stats             = unserialize($bots['stats']);

                $to_user['sex']        = $new_stats['sex'];
                $to_user['guild']      = $new_stats['guild'];
                $to_user['align']      = $new_stats['align'];
                $to_user['incity']     = $new_stats['incity'];
                $to_user['klan']       = $new_stats['klan'];
                $to_user['invis']      = $new_stats['invis'];
                $to_user['shit']       = $new_stats['shit'];
                $to_user['weap']       = $new_stats['weap'];
                $to_user['weap2']      = $new_stats['weap2'];
                $to_user['shadow']     = $new_stats['obraz'];
                $to_user['bot']        = 1;
                $to_user['bothidro']   = $new_stats['bothidro'];
                $to_user['bothran']    = $new_stats['bothran'];
                $to_user['animals']    = "";

                $to_user['sila']       = empty($new_stats['sila'])?0:$new_stats['sila'];
                $to_user['lovk']       = empty($new_stats['lovk'])?0:$new_stats['lovk'];
                $to_user['inta']       = empty($new_stats['inta'])?0:$new_stats['inta'];
                $to_user['vinos']      = empty($new_stats['vinos'])?0:$new_stats['vinos'];
                $to_user['mudra']      = empty($new_stats['mudra'])?0:$new_stats['mudra'];
                $to_user['intel']      = empty($new_stats['intel'])?0:$new_stats['intel'];
                $to_user['login']      = $new_stats['login'];
                $to_user['level']      = $new_stats['level'];
                $to_user['top']        = 0;

                $to_user['maxhp']      = $new_stats['maxhp'];
                $to_user['maxmana']    = $new_stats['maxmana'];
         }

    	} else {
            if ($this->user_id == $id_user) $to_user = $this->user; else $to_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$id_user."' LIMIT 1;");
            $to_user['bot_id'] = $old_id;
    	}
     return $to_user;
    }

  #-----------------------------------------------
  # ���������� �����
    function USER_view_user_block($data_user, $pas = 0, $battle = 0, $me = 0, $show_pr = 0, $userclass = 0){
        include($_SERVER["DOCUMENT_ROOT"]."/functions/classes/view_user_block.class.php");
    }

  #-----------------------------------------------
  # ���������� ����� �����
    function USER_view_user_inter_block($data_user, $pas = 0, $battle = 0, $me = 0, $show_pr = 0, $userclass = 0){
        include($_SERVER["DOCUMENT_ROOT"]."/functions/classes/view_user_inter_block.class.php");
    }


// end class
}
?>