<?

/***------------------------------------------
 * �������� �����
 **/

if (!empty($_SESSION['uid']) && !empty($user_my->user_id)) {

  $fun_eff = function ($name, $type, $total, $time, $opisan, $stage, $from_whom){
    return array('name' => $name, 'type' => $type, 'total' => $total, 'time' => $time, 'opisan' =>$opisan, 'stage' => $stage, 'from_whom' => $from_whom);
  };

 # �������� ���������
   if ($user_my->load_inventory_data == false) {
       $user_my->USER_load_inventory_data();
   }



    # ������
      if (!empty($user_my->inventory_data['boots']) && $user_my->inventory_data['boots']['grind']) {
        $my_effects['eff'][] = $fun_eff('������', 0, '-'.($user_my->inventory_data['boots']['grind']*3).'%', '���������', '�������� ������������ ����� ���������', 0, '������');
      }

    # ��������
      if (!empty($user_my->user['animals'])) {
        $animals = unserialize($user_my->user['animals']);
        foreach ($animals as $k => $pet) {
          if ($pet['war'] == 0 && $pet['gruz']) {
            $my_effects['eff'][] = $fun_eff($pet['name'], 0, '+'.$pet['gruz'], '���������', '���������� �����', 0, '��������');
          }
          if ($pet['war'] == 0 && $pet['speed']) {
            $my_effects['eff'][] = $fun_eff($pet['name'], 0, '-'.$pet['speed'].'%', '���������', '�������� ������������ ����� ���������', 0, '��������');
          }
          if ($pet['war'] == 0 && $pet['speed'] && $pet['additional'] && $pet['name'] == '������') {
            if ($pet['additional']['speed']) {
              $my_effects['eff'][] = $fun_eff('����� ��� �������', 0, '-'.$pet['additional']['speed'].'%', '���������', '�������� ������������ ����� ���������', 0, '����� ���������');
            }
          }
          if ($pet['war'] == 0 && $pet['speed'] && $pet['additional'] && $pet['name'] == '������') {
            if ($pet['additional']['speed']) {
              $my_effects['eff'][] = $fun_eff('����� ��� ������', 0, '-'.$pet['additional']['speed'].'%', '���������', '�������� ������������ ����� ���������', 0, '����� ���������');
            }
          }

        }
      }

   # ��������
     $user_collection = unserialize($user_my->user['collection']);
     if($user_collection)foreach ($user_collection as $k_name => $v) {
       if (isset($v['fin'])) {
         switch ($k_name) {
           case "lot":   $my_effects['eff'][] = $fun_eff('���������� ���������', 0, '+'. $user_my->user['level'], '���������', '��������� ������������� �����', 0, '���������'); break;
           case "pik":   $my_effects['eff'][] = $fun_eff('��������� ���', 0, '+'. $user_my->user['level'], '���������', '��������� ������������ �����', 0, '���������'); break;
           case "tref":  $my_effects['eff'][] = $fun_eff('��������� ����', 0, '+'. $user_my->user['level'], '���������', '��������� �����������', 0, '���������'); break;
           case "cherv": $my_effects['eff'][] = $fun_eff('��������� ����', 0, '+'. $user_my->user['level'], '���������', '��������� ������ ������������ �����', 0, '���������'); break;
           case "bubn":  $my_effects['eff'][] = $fun_eff('��������� �����', 0, '+'. $user_my->user['level'], '���������', '��������� ������ �����������', 0, '���������'); break;

           case "fish":  $my_effects['eff'][] = $fun_eff('��������� ��������', 0, '+'. $user_my->user['level'], '���������', '��������� ������ �����������', 0, '���������'); break;


           case "monstr":   $my_effects['eff'][] = $fun_eff('��������� �������', 0, '+'. $user_my->user['level'], '���������', '��������� ������������ �����', 0, '���������'); break;
         }
       }
     }

    # �������� �������
      if (!empty($user_my->user['klan_relicts'])) {

         $relicts = unserialize($user_my->user['klan_relicts']);
         
         $bkrit    = 0;
         $bakrit   = 0;
         $buvorot  = 0;
         $bauvorot = 0;

         $bminu    = 0;
         $bmaxu    = 0;

         $bhp      = 0;
         $bpw      = 0;

         $bbron   = 0;

         foreach ($relicts as $k => $v) {
           $bkrit    += $v['krit'];
           $bakrit   += $v['akrit'];
           $buvorot  += $v['uvor'];
           $bauvorot += $v['auvor'];

           $bminu    += $v['minu'];
           $bmaxu    += $v['maxu'];

           $bhp      += $v['hp'];
           $bpw      += $v['pw'];

           $bbron   += $v['bron1'];
         }

           if($bkrit)    $my_effects['eff'][] = $fun_eff('����������� ����', 0, '+'.$bkrit, '���������', '��������� ������������ �����', 0, '�������� ������');
           if($bakrit)   $my_effects['eff'][] = $fun_eff('������ ������������ �����', 0, '+'.$bakrit, '���������', '��������� ������ ������������ �����', 0, '�������� ������');
           if($buvorot)  $my_effects['eff'][] = $fun_eff('�����������', 0, '+'.$buvorot, '���������', '��������� �����������', 0, '�������� ������');
           if($bauvorot) $my_effects['eff'][] = $fun_eff('������ �����������', 0, '+'.$bauvorot, '���������', '��������� ������ �����������', 0, '�������� ������');

           if($bminu)    $my_effects['eff'][] = $fun_eff('����������� ����', 0, '+'.$bminu, '���������', '��������� ������������ �����', 0, '�������� ������');
           if($bmaxu)    $my_effects['eff'][] = $fun_eff('������������ ����', 0, '+'.$bmaxu, '���������', '��������� ������������� �����', 0, '�������� ������');

           if($bhp)      $my_effects['eff'][] = $fun_eff('������� �����', 0, '+'.$bhp, '���������', '��������� ������� ������', 0, '�������� ������');
           if($bpw)      $my_effects['eff'][] = $fun_eff('������������', 0, '+'.$bpw, '���������', '��������� ������������', 0, '�������� ������');

           if($bbron)   $my_effects['eff'][] = $fun_eff('�����', 0, '+'.$bbron, '���������', '��������� �����', 0, '�������� ������');
      }

# ����� �����

      $new_q_event = array();
      # �� ������
      if(date("w") < 6 && date("w")>0){
      	# ����� � 1 �� 8 ����
      	if (date("G") >= 1 && date("G") <= 8){
          $my_effects['eff'][] = $fun_eff('���������� ����', 0, '+20%', $enddate, '������ ����� +20% � �����', 0, '����');
      	}
      } else {
      # �� ��������
          if (date("w") == 6) {
            $enddate = mktime(23,59,59,date("m"), date("d")+1, date("Y"));
            $my_effects['eff'][] = $fun_eff('���������� ����', 0, '+50%', $enddate, '�������� ����� +50% � �����', 0, '����');
          } else if (date("w") == 0) {
            $enddate = mktime(23,59,59,date("m"), date("d"), date("Y"));
            $my_effects['eff'][] = $fun_eff('���������� ����', 0, '+50%', $enddate, '�������� ����� +50% � �����', 0, '����');
          }
      }
       
    # ����� ������ ������� � �����
      if (isset($eff_obraz[$user_my->user['shadow']]['persent_exp'])) {
         $my_effects['eff'][] = $fun_eff('���������� ����', 0, '+'.$eff_obraz[$user_my->user['shadow']]['persent_exp'].'%', '���������', '���������� ���� � ���� �������� �� +'.$eff_obraz[$user_my->user['shadow']]['persent_exp'].'%', 0, '�����');
      }

/***------------------------------------------
 * delay
 **/

    # ������� ��������� ��������
      if (!empty($_SESSION['perehod'])) {
         if((@$_SESSION['perehod'] - time()) > 0 && @$_SESSION['perehod'] > time()){
           $vrme = $_SESSION['perehod'];
           $my_effects['delay'][] = $fun_eff('��������', 0, '', $vrme, '����������� ���������', 0, '�����������');
         }
      }


/***------------------------------------------
 * baf
 **/

      if (isset($all_effects['baf'][$m_eff['type']])) {
      //  $my_effects['baf'][] = $m_eff;
      }

/***------------------------------------------
 * debaf
 **/

      if (isset($all_effects['debaf'][$m_eff['type']])) {
      //  $my_effects['debaf'][] = $m_eff;
      }
}

?>