<?
   $file_blutbadjack        = 'adata/casino/blutbadjack.txt';
   $file_blutbadjack_table  = 'adata/casino/blutbadjack_tablenumbers.txt';  # ���������� ���� ������
   $file_blutbadjack_enters = 'adata/casino/blutbadjack_enters.txt';

   $bet_num = array(1 => "0.2", 2 => "0.5", 3 => "1", 4 => "2", 5 => "5");

   $max_users = 5;          #  ������������ ���������� �������� ������

   $new_end_game    = 100;
   $time_start      = 30;    # ��������� ����� ���������� ����
   $time_card_hand  = 30;    # ����� �� ���������� ����
   $time_result     = 10;    #  ����� �� �������� ���������� ������

 # �����
   $cards = array(

      #--- ������ ---------------------

        2  => 2,  # 2 ������
        3  => 3,  # 3 ������
        4  => 4,  # 4 ������
        5  => 5,  # 5 ������
        6  => 6,  # 6 ������
        7  => 7,  # 7 ������
        8  => 8,  # 8 ������
        9  => 9,  # 9 ������
        10 => 10, # 10 ������
        11 => 10, # �����  ������
        12 => 10, # ����   ������
        13 => 10, # ������ ������
        14 => 11, # ���    ������

      #--- ���� ---------------------

        15  => 2,  # 2 ����
        16  => 3,  # 3 ����
        17  => 4,  # 4 ����
        18  => 5,  # 5 ����
        19  => 6,  # 6 ����
        20  => 7,  # 7 ����
        21  => 8,  # 8 ����
        22  => 9,  # 9 ����
        23  => 10, # 10 ����
        24  => 10, # �����  ����
        25  => 10, # ����   ����
        26  => 10, # ������ ����
        27  => 11, # ���    ����

      #--- ����� ---------------------

        28  => 2,  # 2 �����
        29  => 3,  # 3 �����
        30  => 4,  # 4 �����
        31  => 5,  # 5 �����
        32  => 6,  # 6 �����
        33  => 7,  # 7 �����
        34  => 8,  # 8 �����
        35  => 9,  # 9 �����
        36  => 10, # 10 �����
        37  => 10, # �����  �����
        38  => 10, # ����   �����
        39  => 10, # ������ �����
        40  => 11, # ���    �����

      #--- ���� ---------------------

        41  => 2,  # 2 ����
        42  => 3,  # 3 ����
        43  => 4,  # 4 ����
        44  => 5,  # 5 ����
        45  => 6,  # 6 ����
        46  => 7,  # 7 ����
        47  => 8,  # 8 ����
        48  => 9,  # 9 ����
        49  => 10, # 10 ����
        50  => 10, # �����  ����
        51  => 10, # ����   ����
        52  => 10, # ������ ����
        53  => 11, # ���    ����

   );



  if (is_file($file_blutbadjack_table)) {
    $blutbadjack_table = unserialize(file_get_contents($file_blutbadjack_table));
     if (!empty($blutbadjack_table['tablenumbers'])) {
      $blutbadjack_tablenumbers = implode(",", $blutbadjack_table['tablenumbers']);
     } else {
      $blutbadjack_tablenumbers = '';
     }
  } else {
    $blutbadjack_tablenumbers = '';
  }

/***------------------------------------------
 * ������� ����� ������
 **/

function show_error_mes($text, $code = "0"){
 $str = "";
 $str .= "<data>\n";
 $str .= "   <error text=\"" . @iconv('CP1251', 'UTF-8', $text) . "\" code=\"" . $code . "\" />\n";
 $str .= "</data>\n";
// return $str;
 die($str);
}

/***------------------------------------------
 * ������� ����� ������
 **/

function get_timer_state($blutbadjack){
 $timer = 0;
  if (empty($blutbadjack['timer'])) {
   $timer = 0;
  } else {
   $timer = $blutbadjack['timer'] - time();
  }

 if ($timer < 0) { $timer = 0; }
 return (int)$timer;
}


/***------------------------------------------
 * ��������� ����� �� ������������� ����
 **/

  function hasAce($blutbadjack, $id, $is_diler = '') {
      $_loc_1 = 0;

      if ($is_diler) {
        $box_hand = $blutbadjack['dealer_hand'];
      } else {
        $box_hand = $blutbadjack['users'][$id]['box_hand'];
      }

      foreach ($box_hand as $k => $v) {
        if ($v['point'] == 11) { return true; }
      }
      return false;
  }

/***------------------------------------------
 * ��������� ����� �� ���� ���� 21 ���� 2 �����
 **/

 function getMinPoints($blutbadjack, $id, $is_diler = '') {
      $_loc_1 = 0;

      if ($is_diler) {
        $box_hand = $blutbadjack['dealer_hand'];
      } else {
        $box_hand = $blutbadjack['users'][$id]['box_hand'];
      }

      foreach ($box_hand as $k => $v) {
          if ($v['point'] == 11) {
              $_loc_1++;
          } else  {
              $_loc_1 = $_loc_1 + $v['point'];
          }
      }

      return $_loc_1;
  }

/***------------------------------------------
 * ��������� ����� �� ���� ���� 21
 **/

  function getMaxPoints($blutbadjack, $id, $is_diler = '') {
    $_loc_1 = getMinPoints($blutbadjack, $id, $is_diler);

    if (!hasAce($blutbadjack, $id, $is_diler)) { return $_loc_1; }
    if ($_loc_1 + 10 <= 21)                   { return $_loc_1 + 10; }
    return $_loc_1;
  }

/***------------------------------------------
 * ��������� ������ �� ������ "BlackJack"
 **/

  function isDiller_BlackJack($blutbadjack) {
    $dealer_hand = $blutbadjack['dealer_hand'];
    if (count($dealer_hand) != 2) { return false; }

    return getMaxPoints($blutbadjack, 0, 'diller') == 21;
  }

/***------------------------------------------
 * ��������� ������ �� � "BlackJack"
 **/

  function isMy_BlackJack($blutbadjack, $table_id) {
    $box_hand = $blutbadjack['users'][$table_id]['box_hand'];
    if (count($box_hand) != 2) { return false; }

    return getMaxPoints($blutbadjack, $table_id) == 21;
  }

/***------------------------------------------
 * ��������� ���� �� ��������
 **/

  function getPrise($blutbadjack, $table_id) {

      $getHand = $blutbadjack['users'][$table_id];

      $money = 0;

      if (isMy_BlackJack($blutbadjack, $table_id) && isDiller_BlackJack($blutbadjack)) {
          return $getHand['bet'];
      }

      if (isMy_BlackJack($blutbadjack, $table_id)) {
          return $getHand['bet'] * 2.5;
      }

      if (isDiller_BlackJack($blutbadjack)) {
          if ($getHand['insurance'] != 0) {
              $money = $money + $getHand['insurance'] * 3;
          }
      } else {

          foreach ($getHand as $k => $v) { }

              if (getMaxPoints($blutbadjack, $table_id) > 21) {  # � ����
                  $money = $money + 0;
              } else if (getMaxPoints($blutbadjack, 0, 'diller') <= 21) {
                  if (getMaxPoints($blutbadjack, $table_id) == getMaxPoints($blutbadjack, 0, 'diller')) {  # � ���� � � ������
                      $money = $money + $getHand['bet'];
                  } else if (getMaxPoints($blutbadjack, $table_id) > getMaxPoints($blutbadjack, 0, 'diller')) {  # � ����  ������ ��� � ������
                      $money = $money + $getHand['bet'] * 2;
                  }
              } else {
                  $money = $money + $getHand['bet'] * 2;
              }

      }
      return $money;
  }

/***------------------------------------------
 * ������ ����� � ������
 **/

function get_user_boxes($boxes, $user_login, $parm = ''){
 $user_box = '';
  for ($id = 1; $id <= 5; $id++) {
    $b_user = empty($boxes['users'][$id]['user'])?"":$boxes['users'][$id]['user'];
     if ($b_user == $user_login) {
       $user_box = $boxes['users'][$id][$parm];
     }
  }
 return $user_box;
}

/***------------------------------------------
 * ��������� ���������� �������� ������
 **/

function get_count_active_table($blutbadjack){

 $coint = 0;

    for ($id = 1; $id <= 5; $id++) {
      if (!empty($blutbadjack['users'][$id]['user']) && $blutbadjack['users'][$id]['bet']) {
         $coint++;
      }
    }
 return $coint;
}

/***------------------------------------------
 * �������� �����
 **/

function add_user_card(){
  global $cards;
  $type  = rand(2, 53);
#  $sdf = array(12, 27);
#  $type  = $sdf[rand(0, count($sdf)-1)];
  $point = $cards[$type];

   $box_hand = array();
   $box_hand['type']  = $type;
   $box_hand['point'] = $point;

 return $box_hand;
}

/***------------------------------------------------
 * ������ ���������� ����� �� ���� � ��������� �����
 **/

function get_user_card_points($blutbadjack, $bid){

  $s_count = 0;
  $card_count = 0;

  if (!empty($blutbadjack['users'][$bid]['box_hand'])) {
    $box_hand = $blutbadjack['users'][$bid]['box_hand'];
     foreach ($box_hand as $k => $card) {
      $card_count++;

      $zamena = 0;

      if ($card_count > 1 && count($box_hand) == 2 && $box_hand[1]['point'] == 11) { $zamena = 1; }
      if ($card_count > 1 && count($box_hand) == 2 && $box_hand[0]['point'] != 10 && $box_hand[1]['point'] == 11) { $zamena = 1; }
      if ($card_count > 1 && count($box_hand) >= 3) { $zamena = 1; }

      if ($zamena) {   # ���� ��� ���� 1 ����
        if ($card['point'] == 11) { # � ���� ����� ��� = 11 �����
         $card['point'] = 1;        # ������ 1 ���� ���
        }
      }

      $s_count += $card['point'];
     }
  }

 return $s_count;
}

/***------------------------------------------
 * ��������� �����
 * ���� �� � ���� ��� ����� �� ������� � �� �����
 * ���� ������ ����� �� ������� ����� �� �����
 **/

function get_table_game($blutbadjack, $str = ''){
 global $user_login_utf;

 # ���� ��� ������� ����
   $is_my_curbox = 0;

 # ���� ����� ������� ����
   $is_game_curbox = 0;

  # $iddx = '';

  # ��������� ���� �� ��� ���� � ���� ���� ��� �� ����������� �����
    for ($id = 1; $id <= 5; $id++) {
      if (!empty($blutbadjack['users'][$id]['user'])) {

      # my_table
        if ($blutbadjack['users'][$id]['user'] == $user_login_utf) { if ($blutbadjack['users'][$id]['curbox'] && $blutbadjack['users'][$id]['first']) { $is_my_curbox++; } }
      # game_table
        if ($blutbadjack['users'][$id]['curbox'] && $blutbadjack['users'][$id]['first']) { $is_game_curbox++; }

       # $iddx .= '('.$blutbadjack['users'][$id]['curbox'].' = '.$blutbadjack['users'][$id]['first'].'), ';
      }
    }


 # addchp ('<font color=red>��������!</font> <b>get_table_game</b> count '.count($blutbadjack['users']).'  curbox first '.$iddx.' '.$str.' ', "BR");

 return array('my_table' => $is_my_curbox, 'game_table' => $is_game_curbox);
}

/***------------------------------------------------
 * �������� ����� ������ �� ������ � 17 �����
 **/

function add_diller_cards($blutbadjack, $endgame = 0){

 $points_max = 16;
 $stop       = true;
 $timer      = get_timer_state($blutbadjack);     # ������� �����

  # ���� ������� ����
  # ��������� ���� �� ��� ���� � ���� ���� ��� �� ����������� �����
    $is_my_curbox = get_table_game($blutbadjack, 'add_diller_cards');

  # ��������� �� ������� ��������� �����
    $istable = 0;

     for ($id = 1; $id <= 5; $id++) {
      if (!empty($blutbadjack['users'][$id]['user'])) {
      # if (get_user_card_points($blutbadjack, $id) <= 21) {
       if (getMaxPoints($blutbadjack, $id) <= 21) {
         $istable = 1;
       }
      }
     }

     if ($istable && ($endgame || ($is_my_curbox['my_table'] || $is_my_curbox['game_table']))) {
      while ($stop) {
       # ��������� �� �������� �� ����� �����
        # if (get_diller_card_points($blutbadjack) > $points_max) {
         if (getMaxPoints($blutbadjack, 0, 'diller') > $points_max) {
           $stop = false;
           break;
         } else {
         # ��������� ��������� ����� ������
           $blutbadjack['dealer_hand'][] = add_user_card();
         }
      }
     }

 return $blutbadjack;
}

/***------------------------------------------
 * ������� ����� � ������
 **/

function set_user_boxes($boxes, $user_login, $parm = '', $val = ''){
  for ($id = 1; $id <= 5; $id++) {
    $b_user = empty($boxes['users'][$id]['user'])?"":$boxes['users'][$id]['user'];
     if ($b_user == $user_login) {
       $boxes['users'][$id][$parm] = $val;
       return $boxes;
     }
  }
 return false;
}

/***------------------------------------------
 * �������� ������ � ������
 **/

function set_clear_bet($boxes_users){
$sd = array();
  for ($id = 1; $id <= 5; $id++) {
   if (!empty($boxes_users[$id])) {
    $sd[$id] = $boxes_users[$id];
    $sd[$id]['bet']      = 0;
    $sd[$id]['box_hand'] = array();
   }
  }
 return $sd;
}

/***------------------------------------------
 * ��������� � �������� ����������
 **/

function set_new_statistic($boxes_statistica, $boxes_statistica_day, $new_num){

 $iday = 0;
 $boxes_statistica["moneybackday"] = 0;
 $boxes_statistica["moneybackweek"] = 0;
 $boxes_statistica["moneybackmonth"] = 0;

 krsort($boxes_statistica_day);

 foreach ($boxes_statistica_day as $k => $v) {
  $iday++;
  if ($iday <= 1)  { $boxes_statistica["moneybackday"]   += $v['rates']; }
  if ($iday <= 7)  { $boxes_statistica["moneybackweek"]  += $v['rates']; }
  if ($iday <= 30) { $boxes_statistica["moneybackmonth"] += $v['rates']; }
 }

 return $boxes_statistica;
}

/***------------------------------------------
 * ����� ������
 **/

function end_raund($blutbadjack){
  global $diller, $user_money, $user_login_utf, $file_blutbadjack_table, $file_blutbadjack_enters;

  $s_stavka = array();

  $winers = array();

   # ��������� ������
     for ($id = 1; $id <= 5; $id++) {

      $win_bet   = 0;
      $spent_bet = 0;

        if (!empty($blutbadjack['users'][$id]['user'])) {

         $s_user    = $blutbadjack['users'][$id];
         $user_name = @iconv('UTF-8', 'CP1251', $s_user['user']);
         $user_id   = $s_user['id'];

         $point_mycard = getMaxPoints($blutbadjack, $id);

          if ($point_mycard) {

           $cost_mnoj = 2;
           $cost_BJ   = 2.5;

             $win_bet   = getPrise($blutbadjack, $id);
             $spent_bet += $s_user['bet'];


             if (empty($winers[$user_name]['win'])) {
               $winers[$user_name]['win']  = $win_bet;
             } else {
               $winers[$user_name]['win'] += $win_bet;
             }

             if (empty($winers[$user_name]['spent'])) {
               $winers[$user_name]['spent']  = $spent_bet;
             } else {
               $winers[$user_name]['spent'] += $spent_bet;
             }

             # addchp ('<font color=red>��������!</font> ����� ������ �'.$id.' ��� ������� '.$win_bet.' ��. ����: � ���: '.$point_mycard.', � ������: '.$point_dealer.' ', $name_user);
          }
        }
     }

    #------------------------
    # ��������� ����������

        if (is_file($file_blutbadjack_table)) {
          $blutbadjack_table = unserialize(file_get_contents($file_blutbadjack_table));      # ��������� ����������
          $boxes_statistica       = $blutbadjack_table['statistica'];            # statistica
          $boxes_statistica_day   = $blutbadjack_table['statistica_day'];        # statistica_day
        } else {
          $blutbadjack_table = array();
          $boxes_statistica = array();
          $boxes_statistica["moneybackday"] = 0;
          $boxes_statistica["moneybackweek"] = 0;
          $boxes_statistica["moneybackmonth"] = 0;
        }


   # ���� �������
     if (!empty($winers)) {
       foreach ($winers as $user_name => $u_bet) {

             $win_bet   = $u_bet['win'];
             $spent_bet = $u_bet['spent'];

            # ���� �������
              sql_query("UPDATE `users` SET `money` = `money` + '".$win_bet."'  WHERE `login` = '".$user_name."' LIMIT 1;");

             # addchp ('<font color=red>��������!</font> ����� ������! ��� ������� �������� '.$win_bet.' ��. ', $user_name);

              if (is_file($file_blutbadjack_enters)) {
                $users_enter = unserialize(file_get_contents($file_blutbadjack_enters));
              } else {
                $users_enter = array();
              }

              if (empty($users_enter[$user_name]['rates'])) {
                $users_enter[$user_name]['rates'] = $win_bet;
              } else {
                $users_enter[$user_name]['rates'] += $win_bet;
              }

              file_put_contents($file_blutbadjack_enters, serialize($users_enter), LOCK_EX);

           #
             if (empty($boxes_statistica_day[date("d.m.Y")])) {
              $boxes_statistica_day[date("d.m.Y")]['rates']  = $win_bet;
              $boxes_statistica_day[date("d.m.Y")]['stavka'] = $spent_bet;
             } else {
              $boxes_statistica_day[date("d.m.Y")]['rates']  += $win_bet;
              $boxes_statistica_day[date("d.m.Y")]['stavka'] += $spent_bet;
             }

             $boxes_statistica = set_new_statistic($boxes_statistica, $boxes_statistica_day, $win_bet);

             $blutbadjack_table['statistica']       = $boxes_statistica;
             $blutbadjack_table['statistica_day']   = $boxes_statistica_day;
             file_put_contents($file_blutbadjack_table, serialize($blutbadjack_table), LOCK_EX);    # ��������� ����������
       }
     }


    #--------------------------------
    # ��������� ���������� �������

        if (is_file($file_blutbadjack_table)) {
          $blutbadjack_table = unserialize(file_get_contents($file_blutbadjack_table));      # ��������� ����������
          $blutbadjack_tablenumbers = empty($blutbadjack_table['tablenumbers'])?array():$blutbadjack_table['tablenumbers'];          # ������� ������ ��������� �������� �����
        } else {
          $blutbadjack_table = array();
          $blutbadjack_tablenumbers = array();
        }

         $new_num = getMaxPoints($blutbadjack, 0, 'diller');

         if ($new_num > 21) {
           $new_num = "bo";
         } else if(isDiller_BlackJack($blutbadjack)) {
           $new_num = "bj";
         }

         $blutbadjack_tablenumbers[] = (string)$new_num;  # ��������� ����� ���������
         krsort($blutbadjack_tablenumbers);       # ��������� �� ����� � ������

       # ���� ������ 9 ������� �� ������� ���������
         if (count($blutbadjack_tablenumbers) > 9) { array_pop($blutbadjack_tablenumbers); }

         $blutbadjack_table['tablenumbers']     = $blutbadjack_tablenumbers;
         file_put_contents($file_blutbadjack_table, serialize($blutbadjack_table), LOCK_EX);    # ��������� ����������
         $tablenumbers = implode(",", $blutbadjack_table['tablenumbers']);

}



 /***------------------------------------------
  * �������� ������� ����� ����
  **/

 function data_draw() {
  global $diller, $user_money, $user_login_utf, $cards,
         $time_start, $time_card_hand, $time_result,
         $file_blutbadjack, $blutbadjack_tablenumbers, $file_blutbadjack_table;

  if (is_file($file_blutbadjack)) {
    $blutbadjack = unserialize(file_get_contents($file_blutbadjack));

    $boxes        = '';

    $count_stavka = 0;                           # ���������� ������
    $need_seve    = 0;

    $curbox = 0;                                 # ������� ����
    $status = $blutbadjack['status'];            # ������� ������
    $ready  = $blutbadjack['ready'];             # ����������
    $timer  = get_timer_state($blutbadjack);     # ������� �����

    $dealer_hand = "";  # ����� ������

   # ��������� ������
     for ($id = 1; $id <= 5; $id++) {
      if (!empty($blutbadjack['users'][$id]) && !empty($blutbadjack['users'][$id]['id'])) {
       if ($blutbadjack['users'][$id]['id'] == $id) {
         $user_jack = $blutbadjack['users'][$id];

         if ($user_jack['bet']) { $count_stavka++; }

           if (empty($curbox) && $user_jack['bet'] && ($status == 2 || $status == 3) && $count_stavka) {
              if ($user_jack['curbox'] != -1 && !isMy_BlackJack($blutbadjack, $id)) { # ����
                $curbox = $user_jack['curbox'];
              }
           } else {
             if (empty($user_jack['bet']) && ($status == 2 || $status == 3)) {
              unset($blutbadjack['users'][$id]); # ������� ����� ��� ������
             # addchp ('<font color=red>��������!</font>  ������� ����� ��� ������  ', "BR");
            }
           }

       }
      }
     }


     if (empty($count_stavka) && $status == 1 && $curbox == 0) {
      # ���� ������ ��� �� ������� ���� ���� �����
        if (empty($timer)) {
          for ($id = 1; $id <= 5; $id++) {
           unset($blutbadjack['users'][$id]);
          }

         $status = 0;
         $ready = 1;

         $blutbadjack['status'] = $status;
         $blutbadjack['ready'] = $ready;
         $need_seve = 1;

        # addchp ('<font color=red>��������!</font>  ���� ������ ��� �� ������� ���� ���� �����  ', "BR");

        }
     } else {

        # �������� ������
          if ($status == 0 && empty($timer) && $count_stavka) {

           $status = 1;
           $ready  = 0;
           $blutbadjack['status'] = $status;
           $blutbadjack['ready']  = $ready;

           $blutbadjack['timer']  = time() + $time_card_hand;
           $timer  = get_timer_state($blutbadjack);

           $need_seve = 1;

        # �������� �����
          } else if ($status == 1 && empty($timer) && $count_stavka) {
           $status = 2;
           $ready  = 0;
           $blutbadjack['status'] = $status;
           $blutbadjack['ready']  = $ready;

           $blutbadjack['timer']  = time() + $time_card_hand;
           $timer  = get_timer_state($blutbadjack);

           $need_seve = 1;

        # �������� ���������
          } else if ($status == 2 && empty($timer) && $count_stavka) {
           $status = 3;
           $ready  = 0;
           $blutbadjack['status'] = $status;
           $blutbadjack['ready']  = $ready;
           $blutbadjack['timer']  = time() + $time_result;
           $timer  = get_timer_state($blutbadjack);

         # ���� ����� ������
           $blutbadjack = add_diller_cards($blutbadjack, 1);

         # ���� �������
           end_raund($blutbadjack);

            # ������� ��� �����
              /*for ($id = 1; $id <= 5; $id++) {
               unset($blutbadjack['users'][$id]);
              }*/
              for ($id = 1; $id <= 5; $id++) {
                 $blutbadjack['users'][$id]['curbox']    = 0;
                 $blutbadjack['users'][$id]['first']     = 0;
                 $blutbadjack['users'][$id]['blackjack'] = 0;
              }



         # ������� ����
           $curbox    = 0;

           $need_seve = 1;

        # ������ ������ �� ������ �������� �������
          } else if ($status == 3 && empty($timer)) {
           $status = 1;
           $ready   = 0;
           $blutbadjack['status'] = $status;
           $blutbadjack['ready']  = $ready;

           $blutbadjack['users']       = set_clear_bet($blutbadjack['users']); # ������� ��� ������
           $blutbadjack['dealer_hand'] = array();                              # ������� ��� ������ � ������
           $blutbadjack['end_game']    = time() + $new_end_game;

           $blutbadjack['timer']  = time() + $time_start;
           $timer  = get_timer_state($blutbadjack);

         # ������� ����
           $curbox    = 0;

           $need_seve = 1;
          }

     }

   # ������� ����� ��� ������
     for ($did = 1; $did <= 5; $did++) {
      if (!empty($blutbadjack['users'][$did]) && !empty($blutbadjack['users'][$did]['id'])) {
       if ($blutbadjack['users'][$did]['id'] == $did && empty($blutbadjack['users'][$did]['bet']) && ($status == 2 || $status == 3)) {
          unset($blutbadjack['users'][$did]);
       }
      }
     }

     for ($id = 1; $id <= 5; $id++) {
        $user_jack['id']        = $id;
        $user_jack['first']     = 1;
        $user_jack['insurance'] = 0;
        $user_jack['curhand']   = 1;
        $user_jack['sex']       = "";
        $user_jack['user']      = "";
        $user_jack['user_id']   = 0;
        $user_jack['ready']     = 0;
        $user_jack['blackjack'] = 0;
        $user_jack['bet']       = 0;
        $user_jack['old_bet']   = 0;
        $user_jack['box_hand']  = array();
        $user_jack['curbox']    = 0;

        $hand                   = "";

        if (!empty($blutbadjack['users'][$id]) && !empty($blutbadjack['users'][$id]['id'])) {
         if ($blutbadjack['users'][$id]['id'] == $id) {
           $user_jack = $blutbadjack['users'][$id];
         # �������� ����� ��� ����������
           if ($status == 2 || $status == 3) {

           # ��������� 2 ��������� ����� ���� �����
             if (empty($user_jack['box_hand']) && $user_jack['bet']) {
                for ($i = 0; $i < 2; $i++) {
                 $user_jack['box_hand'][] = add_user_card();
                 $need_seve = 1;
                }

              $blutbadjack['users'][$id] = $user_jack;

              # blackjack
               # if (get_user_card_points($blutbadjack, $id) == 21 && count($user_jack['box_hand']) == 2 && ($status == 2 || $status == 3)) {
                if (isMy_BlackJack($blutbadjack, $id) && ($status == 2 || $status == 3)) {
                  $user_jack['blackjack'] = 1;
                  $blutbadjack['users'][$id]['blackjack'] = 1;
                } else {
                  $user_jack['blackjack']                 = 0;
                  $blutbadjack['users'][$id]['blackjack'] = 0;
                }

             }

           # ����� �����
             if (!empty($blutbadjack['users'][$id]['box_hand']) ) {
               foreach ($blutbadjack['users'][$id]['box_hand'] as $k => $card) {
                 $hand .= "<card point=\"".$card['point']."\" type=\"".$card['type']."\" />";
               }
             }

           # ������ ������� ����
             if (empty($curbox) && $blutbadjack['users'][$id]['bet'] && $status == 2 && $count_stavka) {
              if ($blutbadjack['users'][$id]['curbox'] != -1 && !isMy_BlackJack($blutbadjack, $id)) { # ����
                $curbox = $blutbadjack['users'][$id]['curbox'];
              }
             }

           }
         }
        }

        $boxes .= "<box first=\"".$user_jack['first']."\" insurance=\"".$user_jack['insurance']."\" curhand=\"".$user_jack['curhand']."\" sex=\"".$user_jack['sex']."\" user=\"".$user_jack['user']."\" id=\"".$user_jack['id']."\" ready=\"".$user_jack['ready']."\" blackjack=\"".$user_jack['blackjack']."\">
      				<hand bet=\"".$user_jack['bet']."\" id=\"1\">".$hand."
      				</hand>
      				<hand bet=\"0\" id=\"2\">
      				</hand>
      		      </box>";
     }


 # �������� ����� ������� ��� ����������
   if ($status == 2 || $status == 3) {

     # ��������� 2 ��������� ����� ���� �����
       if (empty($blutbadjack['dealer_hand'])) {
          for ($i = 0; $i < rand(1, 2); $i++) {
           $blutbadjack['dealer_hand'][] = add_user_card();
           $need_seve = 1;
          }

           /*$box_hand['point'] = 11;
           $box_hand['type']  = 27;
           $blutbadjack['dealer_hand'][] = $box_hand;
           $box_hand['point'] = 10;
           $box_hand['type']  = 25;
           $blutbadjack['dealer_hand'][] = $box_hand;*/

       }

      # ����� �����
         if (!empty($blutbadjack['dealer_hand']) ) {
           foreach ($blutbadjack['dealer_hand'] as $k => $card) {
             $dealer_hand .= "<card point=\"".$card['point']."\" type=\"".$card['type']."\" />";
           }
         }
   }

# ���� ��� ������ ������� � ������ ���� �� ������� ���������
  if (empty($curbox) && $status == 2) {

     for ($id = 1; $id <= 5; $id++) {
       $blutbadjack['users'][$id]['curbox'] = 0;
       $blutbadjack['users'][$id]['first']  = 0;
     }

     $status = 3;
     $ready  = 0;
     $blutbadjack['status'] = $status;
     $blutbadjack['ready']  = $ready;
     $blutbadjack['timer']  = time() + $time_result;
  }

  if ($need_seve) {
    file_put_contents($file_blutbadjack, serialize($blutbadjack), LOCK_EX);
  }

  if (is_file($file_blutbadjack_table)) {
    $boxes_table = unserialize(file_get_contents($file_blutbadjack_table));      # ��������� ����������
  }
  $boxes_statistica = empty($boxes_table['statistica'])?array():$boxes_table['statistica'];                  # statistica
  $moneybackday     = empty($boxes_statistica["moneybackday"])?0:$boxes_statistica["moneybackday"];
  $moneybackweek    = empty($boxes_statistica["moneybackday"])?0:$boxes_statistica["moneybackweek"];
  $moneybackmonth   = empty($boxes_statistica["moneybackday"])?0:$boxes_statistica["moneybackmonth"];

    $statistic = "<statistic moneybackday=\"".$moneybackday."\" moneybackweek=\"".$moneybackweek."\" moneybackmonth=\"".$moneybackmonth."\" dealerpoints=\"".$blutbadjack_tablenumbers."\" />";
    $state = "<state curbox=\"".$curbox."\" status=\"".$status."\" user=\"".$user_login_utf."\" ready=\"".$ready."\" timer=\"".$timer."\" money=\"".$user_money."\" />";

    $data = "
    <data>
    	<table id=\"1\">
            ".$statistic."
    		<dealer user=\"".$diller."\">
    			<hand>
                ".$dealer_hand."
    			</hand>
    		</dealer>
    		<boxes>
            ".$boxes."
    		</boxes>
            ".$state."
    	</table>
    </data>
      ";
    return $data;

  } else {
   // echo show_error_mes("�������� ������ ���", 0);
  }

 }


?>