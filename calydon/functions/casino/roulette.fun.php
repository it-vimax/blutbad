<?


# �����

   $numbers = array(

    # ��� ������� "rn" 'red'
      'rn' => array(1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36),
    # ��� ������ "bn" 'black'
      'bn' => array(2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35),

    # ��� ������ "en" 'even'
      'en' => array(2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 35),
    # ��� �� ������ "on" 'noteven'
      'on' => array(1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35),

    # ������ 12 = 1-12 "cl" 'twelve_first'
      'cl' => array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
    # ������ 12 = 13-24 "cm" 'twelve_second'
      'cm' => array(13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24),
    # ������ 12 = 25-36 "cr" 'twelve_third'
      'cr' => array(25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36),

    # ������ �������� 1-18 "fh" 'firsthalf'
      'fh' => array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18),
    # ������ �������� 19-36 "sh" 'secondhalf'
      'sh' => array(19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36),

    # ��� ������ "lt" 'row_first'
      'lt' => array(3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36),
    # ��� ������ "lm" 'row_second'
      'lm' => array(2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35),
    # ��� ������ "lb" 'row_third'
      'lb' => array(1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34),

    # ������ ����
      'groups' => array("rn", "bn", "en", "on", "cl", "cm", "cr", "fh", "sh", "lt", "lm", "lb"),
    # ������ �� 12 ����
      'twelve_groups' => array("cl", "cm", "cr"),
    # ������ �����
      'row_groups' => array("lt", "lm", "lb"),
    # ������ ���� ��� "����"
      'row_on_zero' => array("rn", "bn", "en", "on"),
    # ������� � ������
      'row_rnbn' => array("rn", "bn")

  );



/***------------------------------------------
 * �������� ������
 **/

function wins_result($boxes, $win_num){
 global $numbers, $file_enters, $file_table, $user_login_utf;

 $timer = 0;

   for ($id = 1; $id <= 4; $id++) {

      $rate_type      = '';
      $res_money      = 0;
      $value_money    = 0;
      $res_bets       = 0;
      $res_types      = '';
      $str_res_types  = '';
      $b_user         = empty($boxes['user'][$id]['user'])?"":@iconv('UTF-8', 'CP1251', $boxes['user'][$id]['user']);
      $b_bets         = empty($boxes['user'][$id]['bets'])?array():$boxes['user'][$id]['bets'];
      $b_ready        = empty($boxes['user'][$id]['ready'])?0:$boxes['user'][$id]['ready'];

      if (count($b_bets) > 0 && $b_user) {
      if ($b_user == $user_login_utf) {
     #  addchp ('<font color=red>��������! </font> <b>'.$win_num.'</b> �������� ������� �� ', $b_user);
      }

       foreach ($b_bets as $k => $bet) {

         $res_bets++;
         $str_res_types .= $bet['type'].', ';
         $value_money += $bet['value'];

         if (empty($bet['stop'])) {

           $bet['stop'] = 1;
           $boxes['user'][$id]['bets'][$k]['stop'] = 1;

           $res_type = $bet['type'];
           $res_types = explode(",", $bet['type']);

             if (in_array($res_type, $numbers['groups'])) {   # �������� ������
             #===================
                $rate_type = '�������� ������';

               if (in_array($res_type, $numbers['twelve_groups'])) {      # ������ �� 12 ����

                if (in_array($win_num, $numbers[$res_type])) {
                   $res_money += $bet['value'] * 3;
                }

               } elseif (in_array($res_type, $numbers['row_groups'])) {   # ������ �����

                if (in_array($win_num, $numbers[$res_type])) {
                   $res_money += $bet['value'] * 3;
                }

               } elseif ($win_num == 0 && in_array($res_type, $numbers['row_on_zero'])) {   # ������ row_on_zero

                 $res_money += $bet['value'] / 2;

               } elseif (in_array($win_num, $numbers[$res_type])) {       # ������ ����

                if (in_array($win_num, $numbers[$res_type])) {
                   $res_money += $bet['value'] * 2;
                }

               }

             #===================
             } elseif(is_numeric($res_type)) {                # ���� ������ �� �����
             #===================

               if ($res_type == $win_num) { $res_money += $bet['value'] * 36; $rate_type = '���� ������ �� �����';  }

             #===================
             } elseif($res_type == "ze") {                      # ���� ������ �� ����� ����
             #===================

               if ($win_num == 0) { $res_money += $bet['value'] * 36; $rate_type = '���� ������ �� ����� ����';  }

             #===================
             } elseif(count($res_types) > 0 && in_array($win_num, $res_types)) {                # ������ �� ���� �����
             #===================


               foreach ($res_types as $k => $v) {

                $pl_money = 0;

                  switch (count($res_types)) {
                    case 2:
                       $pl_money += ($bet['value'] * 17) + $bet['value'];
                    break;
                    case 3:
                       $pl_money += ($bet['value'] * 12);
                    break;
                    case 4:
                       $pl_money += ($bet['value'] * 8) + $bet['value'];
                    break;
                  }

                  if ($v == $win_num && $pl_money) { $res_money += $pl_money; $rate_type = '������ �� ���� �����'; }

               }

             #===================
             }

         }
       }

      if ($b_user && $res_money) {
         $user_g = sql_row("SELECT * FROM `users` WHERE `login` = '".$b_user."' LIMIT 1;");
         sql_query("UPDATE `users` SET `money` = `money` + '".$res_money."' WHERE `login` = '".$b_user."' LIMIT 1;");
         # addchp ('<font color=red>��������! </font>��������  ���� <b>'.$user_g['money'].'</b> �� <b>'.$rate_type.'</b> = <b>'.$user_g['login'].'</b> ������� <b>'.$res_money.'</b> ��.  ������ ('.$res_bets.') <b>'.$value_money.'</b> ��.   ���� '.$str_res_types.' ', $b_user);

        # ��������� ���������� ������� ���������
          $users_enter = unserialize(file_get_contents($file_enters));

            if (empty($users_enter[$b_user]['rates'])) {
                $users_enter[$b_user]['rates'] = $res_money;
            } else {
                $users_enter[$b_user]['rates'] += $res_money;
            }

          file_put_contents($file_enters, serialize($users_enter), LOCK_EX);


          #------------------------
          # ��������� ����������

              if (is_file($file_table)) {
                $boxes_table            = unserialize(file_get_contents($file_table));      # ��������� ����������
                $boxes_statistica       = $boxes_table['statistica'];            # statistica
                $boxes_statistica_day   = $boxes_table['statistica_day'];        # statistica_day
              } else {
                $boxes_table                        = array();
                $boxes_statistica                   = array();
                $boxes_statistica["moneybackday"]   = 0;
                $boxes_statistica["moneybackweek"]  = 0;
                $boxes_statistica["moneybackmonth"] = 0;
              }

             if (empty($boxes_statistica_day[date("d.m.Y")])) {
              $boxes_statistica_day[date("d.m.Y")]['rates']  = $res_money;
              $boxes_statistica_day[date("d.m.Y")]['stavka'] = $value_money;
             } else {
              $boxes_statistica_day[date("d.m.Y")]['rates']  += $res_money;
              $boxes_statistica_day[date("d.m.Y")]['stavka'] += $value_money;
             }

             $boxes_statistica = set_new_statistic($boxes_statistica, $boxes_statistica_day, $res_money);

             $boxes_table['statistica']       = $boxes_statistica;
             $boxes_table['statistica_day']   = $boxes_statistica_day;
             file_put_contents($file_table, serialize($boxes_table), LOCK_EX);    # ��������� ����������

      } else {

        if ($value_money) {

          #------------------------
          # ��������� ����������

              if (is_file($file_table)) {
                $boxes_table = unserialize(file_get_contents($file_table));      # ��������� ����������
                $boxes_statistica_day   = $boxes_table['statistica_day'];        # statistica_day
              } else {
                $boxes_table = array();
              }

             if (empty($boxes_statistica_day[date("d.m.Y")]['stavka'])) {
              $boxes_statistica_day[date("d.m.Y")]['stavka'] = $value_money;
             } else {
              $boxes_statistica_day[date("d.m.Y")]['stavka'] += $value_money;
             }

             if (empty($boxes_statistica_day[date("d.m.Y")]['rates'])) {
              $boxes_statistica_day[date("d.m.Y")]['rates'] = 0;
             }

             $boxes_table['statistica_day']   = $boxes_statistica_day;
             file_put_contents($file_table, serialize($boxes_table), LOCK_EX);    # ��������� ����������

        }
      }
      }

    }

 return $boxes;
}



/***------------------------------------------
 * ��������� � �������� ����������
 **/

function set_new_statistic($boxes_statistica, $boxes_statistica_day, $new_num){

 $iday = 0;
 $boxes_statistica["moneybackday"] = 0;
 $boxes_statistica["moneybackweek"] = 0;
 $boxes_statistica["moneybackmonth"] = 0;

 krsort($boxes_statistica_day);

 foreach ($boxes_statistica_day as $k => $v) {

  $iday++;

  if ($iday <= 1) {
   $boxes_statistica["moneybackday"] += $v['rates'];
  }

  if ($iday <= 7) {
   $boxes_statistica["moneybackweek"] += $v['rates'];
  }

  if ($iday <= 30) {
   $boxes_statistica["moneybackmonth"] += $v['rates'];
  }

 }

 return $boxes_statistica;
}

/***------------------------------------------
 * ������� ����� ������
 **/

function show_error_mes($text, $code = "0"){
 $str = "";
 $str .= "<data>\n";
 $str .= "   <error text=\"" . @iconv('CP1251', 'UTF-8', $text) . "\" code=\"" . $code . "\" />\n";
 $str .= "</data>\n";
 return $str;
}

/***------------------------------------------
 * ������� ����� ������
 **/

function get_timer_state($boxes){
 $timer = 0;
  if (empty($boxes['timer'])) {
   $timer = 0;
  } else {
   $timer = $boxes['timer'] - time();
  }

 if ($timer < 0) { $timer = 0; }
 return (int)$timer;
}

/***------------------------------------------
 * ������ ����� � ������
 **/

function get_user_boxes($boxes, $user_login, $parm = ''){
 $user_box = '';
  for ($id = 1; $id <= 4; $id++) {
    $b_user = empty($boxes['user'][$id]['user'])?"":$boxes['user'][$id]['user'];
     if ($b_user == $user_login) {
       $user_box = $boxes['user'][$id][$parm];
     }
  }
 return $user_box;
}

/***------------------------------------------
 * ������� ����� � ������
 **/

function set_user_boxes($boxes, $user_login, $parm = '', $val = ''){
  for ($id = 1; $id <= 4; $id++) {
    $b_user = empty($boxes['user'][$id]['user'])?"":$boxes['user'][$id]['user'];
     if ($b_user == $user_login) {
       $boxes['user'][$id][$parm] = $val;
       return $boxes;
     }
  }
 return false;
}

/***------------------------------------------
 * ������� ��������� ����
 **/

function get_game_station($boxes){
 global $user, $file, $file_table, $new_end_game, $new_timer, $new_timer_end, $user_login_utf, $numbers, $tablenumbers;
 $data_bet_count   = 0;
 $game_boxes       = "";
 $status           = $boxes['status'];
 $data             = "";
 $data_boxes       = "";
 $data_users_count = 0;
 $boxes_box        = array();
 $timer            = get_timer_state($boxes);

 $user_box_empty['user']  = "";
 $user_box_empty['sex']   = "";
 $user_box_empty['ready'] = 0;
 $user_box_empty['bets']  = array();

# status = 2 ��� ������ ������� ������� ����� � ������� � �������� ���������

        $s_turn = true;

           for ($id = 1; $id <= 4; $id++) {

             $b_user = empty($boxes['user'][$id]['user'])?"":$boxes['user'][$id]['user'];

              if ($b_user) { $data_users_count++; }

                $b_bets = empty($boxes['user'][$id]['bets'])?array():$boxes['user'][$id]['bets'];

                $boxes_box[$id]['id']    = $id;
                $boxes_box[$id]['user']  = empty($boxes['user'][$id]['user'])?"":$boxes['user'][$id]['user'];
                $boxes_box[$id]['sex']   = empty($boxes['user'][$id]['sex'])?"":$boxes['user'][$id]['sex'];
                $boxes_box[$id]['ready'] = empty($boxes['user'][$id]['ready'])?0:$boxes['user'][$id]['ready'];
                $boxes_box[$id]['bets']  = $b_bets;

                $data_bet_count = count($b_bets);

                # ������� ���� ���� �����
                  if (!empty($b_user) && $timer == 0) {

                    if ($boxes['status'] == "2") {
                      if ($data_bet_count) {
                       $boxes['timer']             = time() + $new_timer;
                       $boxes['end_game']          = time() + $new_end_game;
                       $status                     = "1";
                       $boxes['user'][$id]['bets'] = array();
                       $boxes_box[$id]['bets']     = array();

                      } else {
                       $status = "0";
                       unset($boxes['user'][$id]);
                       $boxes_box[$id]       = $user_box_empty;
                       $boxes_box[$id]['id'] = $id;
                      }

                    } else {

                      if ($data_bet_count) {

                        if ($boxes['status'] != "2") {

                       // addchp ('<font color=red>��������! </font>  bet count <b>'.count($b_bets).'</b>  data_bet_count <b>'.$data_bet_count.'</b>   ', "BR");

                         $status = "2";
                       # $boxes['status'] = "2";                  # ������ ������ "��� ������ ������� ������� ����� � ����������"

                        if ($s_turn) {
                          $boxes['timer'] = time() + $new_timer_end; # ��������� ����� ��� ��������� ������� � ������ ����������

                          if (rand(0, 36) == 0) {
                            $new_num = 0;
                          } else {
                            $groups_rand = $numbers['row_rnbn'][rand(0, count($numbers['row_rnbn'])-1)];
                            $new_num     = $numbers[$groups_rand][rand(0, count($numbers[$groups_rand])-1)];
                          }

                          $s_turn = false;
                          $boxes  = wins_result($boxes, $new_num); # ��������� ��������� � ������ �������
                        }

                      #------------------------
                      # ��������� ����������

                          if (is_file($file_table)) {
                            $boxes_table            = unserialize(file_get_contents($file_table));      # ��������� ����������
                            $boxes_tablenumbers     = empty($boxes_table['tablenumbers'])?array():$boxes_table['tablenumbers'];          # ������� ������ ��������� �������� �����
                          } else {
                            $boxes_table            = array();
                            $boxes_tablenumbers     = array();
                          }

                         $boxes_tablenumbers[]      = $new_num;  # ��������� ����� ���������
                         krsort($boxes_tablenumbers);            # ��������� �� ����� � ������

                       # ���� ������ 9 ������� �� ������� ���������
                         if (count($boxes_tablenumbers) > 9) { array_pop($boxes_tablenumbers); }

                         $boxes_table['tablenumbers'] = $boxes_tablenumbers;
                         file_put_contents($file_table, serialize($boxes_table), LOCK_EX);    # ��������� ����������
                         $tablenumbers = implode(", ", $boxes_table['tablenumbers']);
                        }

                      } else {
                        $boxes['timer']       = 0;
                        $status               = "0";
                        $boxes_box[$id]       = $user_box_empty;
                        $boxes_box[$id]['id'] = $id;
                        unset($boxes['user'][$id]);
                      }

                    }

                  }
           }

     # ������ �������� ���� ��� �����
       /*if () {
         $boxes  = wins_result($boxes, $new_num); # ��������� ��������� � ������ �������
       }*/

          $boxes['status'] = $status;

           $timer = get_timer_state($boxes);
           $ready = get_user_boxes($boxes, $user_login_utf, 'ready');

           if ($boxes['status'] != "2" && $timer == 0) {

            if ($data_bet_count && $data_users_count) {
              $boxes['status'] = "1";
            }

            if ($data_bet_count && $timer == 0)  {
              $boxes['status'] = "2";
              $boxes = set_user_boxes($boxes, $user_login_utf, 'ready', "0");
            }

           }

         #------------------------
         # ������ "boxes" �������

           foreach ($boxes_box as $k => $v) {

            $box_id    = $v['id'];
            $box_user  = $v['user'];
            $box_sex   = $v['sex'];
            $box_ready = $v['ready'];
            $box_bets  = $v['bets'];

            $data_boxes .= "      <box id=\"" . $box_id . "\" sex=\"" . $box_sex . "\" ready=\"" . $box_ready . "\" user=\"" . $box_user . "\">\n";
            $data_boxes .= "        <bets>\n";

              if (count($box_bets) > 0) {
               foreach ($box_bets as $k_b => $bet) {
                 $data_boxes .= "          <bet value=\"" . $bet['value'] . "\" type=\"" . $bet['type'] . "\" />\n";
               }
              }

            $data_boxes .= "        </bets>\n";
            $data_boxes .= "      </box>\n";

           }
  if (is_file($file_table)) {
    $boxes_table = unserialize(file_get_contents($file_table));      # ��������� ����������
  }
  $boxes_statistica = empty($boxes_table['statistica'])?array():$boxes_table['statistica'];                  # statistica
  $moneybackday     = empty($boxes_statistica["moneybackday"])?0:$boxes_statistica["moneybackday"];
  $moneybackweek    = empty($boxes_statistica["moneybackday"])?0:$boxes_statistica["moneybackweek"];
  $moneybackmonth   = empty($boxes_statistica["moneybackday"])?0:$boxes_statistica["moneybackmonth"];

 $data .= "<data>\n";
 $data .= "  <table id=\"1\">\n";
 $data .= "    <boxes>\n";
 $data .= $data_boxes;
 $data .= "    </boxes>\n";
 $data .= "    <state bets=\"" . $data_bet_count . "\" status=\"" . $boxes['status'] . "\" timer=\"" . $timer . "\" money=\"" . $user['money'] . "\" ready=\"" . $ready . "\" user=\"" . $user_login_utf . "\" />\n";
 $data .= "    <statistic moneybackday=\"" . $moneybackday . "\" moneybackmonth=\"" . $moneybackmonth . "\" moneybackweek=\"" . $moneybackweek . "\" tablenumbers=\"" . $tablenumbers . "\" />\n";
 $data .= "  </table>\n";
 $data .= "</data>";

 file_put_contents($file, serialize($boxes), LOCK_EX);
 return $data;
}


?>