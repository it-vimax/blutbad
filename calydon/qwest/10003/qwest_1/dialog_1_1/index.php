<?
    include "functions/exchange/exchange.fun.php";

  #------------------
  # ��� ������ �����
    $mine_resources = unserialize($user_my->user['mine_resources']);

    $my_coal     = empty($mine_resources['coal']['val'])?"0.00":$mine_resources['coal']['val'];
    $my_copper   = empty($mine_resources['copper']['val'])?"0.00":$mine_resources['copper']['val'];
    $my_silver   = empty($mine_resources['silver']['val'])?"0.00":$mine_resources['silver']['val'];
    $my_platinum = empty($mine_resources['platinum']['val'])?"0.00":$mine_resources['platinum']['val'];
    $my_gold     = empty($mine_resources['gold']['val'])?"0.00":$mine_resources['gold']['val'];
    $my_din      = $user_my->user['money'];
    $my_stl      = $user_my->user['ekr'];

  # ��������� �����������������
    $s_time_format = function ($t) {
     if ($t > (3600 * 24))
          return (floor($t/86400)).' ���.';
     else return ($t/60).' ���.';

    };

    $cur_npc_id = 10003;
    $cur_url    = '/npc.php?cmd=dialog&nid='.$cur_npc_id.'&dialog_id=1&qwest_id=1&nd='.$user_my->user['nd'];

  # ���� �����
    if (empty($_GET['menu'])) { $_GET['menu'] = "m_money"; }

  # ���� ������ ����. ������
    $glass_data = array(
      "glass_din_coal"     => array("gr" => 0.01, "din" => 0.01), # ������� ���� ������ ����������: 0.01 ����� = 0.01 ������
      "glass_din_copper"   => array("gr" => 0.01, "din" => 0.03), # ������� ���� ������ ����������: 0.01 ����� = 0.02 ������
      "glass_din_silver"   => array("gr" => 0.01, "din" => 0.06), # ������� ���� ������ ����������: 0.01 ����� = 0.03 ������
      "glass_din_platinum" => array("gr" => 0.01, "din" => 0.09), # ������� ���� ������ ����������: 0.01 ����� = 0.04 ������
      "glass_din_gold"     => array("gr" => 0.01, "din" => 0.12), # ������� ���� ������ ����������: 0.01 ����� = 0.05 ������

      "glass_stl_coal"     => array("gr" => 25,  "stl" => 1),      # ������� ���� ������ ����������: 1 ����� = 1 ��������
      "glass_stl_copper"   => array("gr" => 20,  "stl" => 1),      # ������� ���� ������ ����������: 1 ����� = 2 ��������
      "glass_stl_silver"   => array("gr" => 15,  "stl" => 1),      # ������� ���� ������ ����������: 1 ����� = 3 ��������
      "glass_stl_platinum" => array("gr" => 10,  "stl" => 1),      # ������� ���� ������ ����������: 1 ����� = 4 ��������
      "glass_stl_gold"     => array("gr" => 5,   "stl" => 1),      # ������� ���� ������ ����������: 1 ����� = 5 ��������
    );

  # ����� ���������
    $mine_res_name = array(
       "coal"     => "�����",
       "copper"   => "����",
       "silver"   => "�������",
       "platinum" => "�������",
       "gold"     => "������",
    );

  # ��������� ���������
    $ex_mineral_item = array(
       11111177 => array("stl" => 0, "din" => 50,  "coal" => 0,  "copper" => 0,  "silver" => 0,  "platinum" => 0, "gold" => 0),
       11111178 => array("stl" => 0, "din" => 100, "coal" => 25, "copper" => 15, "silver" => 0,  "platinum" => 0, "gold" => 0),
       11111179 => array("stl" => 0, "din" => 200, "coal" => 50, "copper" => 25, "silver" => 15, "platinum" => 0, "gold" => 0),
    );

  # ��������� ���������
    $ex_mineral_rune = array(
       103      => array("name" => '�������� 60', "time_end" => 60, "opisan" => "", "stl" => 0, "din" => 5, "coal" => 5, "copper" => 3,  "silver" => 0,  "platinum" => 0, "gold" => 0),
       121      => array("name" => '���� 60', "time_end" => 60, "opisan" => "", "stl" => 0, "din" => 10, "coal" => 7, "copper" => 5, "silver" => 0, "platinum" => 0, "gold" => 0),
       11111114 => array("name" => '������� 60', "time_end" => 60, "opisan" => "", "stl" => 0, "din" => 5, "coal" => 5, "copper" => 3, "silver" => 0, "platinum" => 0, "gold" => 0),
       1547     => array("name" => '����������� 60', "time_end" => 60, "opisan" => "", "stl" => 0, "din" => 15, "coal" => 7, "copper" => 5, "silver" => 0, "platinum" => 0, "gold" => 0),
       129      => array("name" => '��������� �������', "time_end" => 0, "opisan" => "��������� �������� � �����", "stl" => 1, "din" => 20, "coal" => 7, "copper" => 5, "silver" => 0, "platinum" => 0, "gold" => 0),
       1455     => array("name" => '������������� �������', "time_end" => 0, "opisan" => "��������� ����������� � �����", "stl" => 2, "din" => 25, "coal" => 7, "copper" => 5, "silver" => 0, "platinum" => 0, "gold" => 0),
    );

  # ��������� �����
    $ex_mineral_baf = array(
       1   => array("name" => '����� ����� �����',   "img" => "starter/45.gif", "time_end" => 3600,  "opisan" => "�������� ������ ����� �� ������� ��������� ���� ���� ���������� ����� �� ��� ���� �����",  "stl" => 0, "din" => 5,  "coal" => 5, "copper" => 3,  "silver" => 0,  "platinum" => 0, "gold" => 0),
       2   => array("name" => '������� ����� �����', "img" => "starter/45.gif", "time_end" => 7200,  "opisan" => "�������� ������ ����� �� ������� ��������� ���� ���� ���������� ����� �� ������ ���� �����",  "stl" => 0, "din" => 10, "coal" => 10, "copper" => 7,  "silver" => 0,  "platinum" => 0, "gold" => 0),
       3   => array("name" => '������� ����� �����', "img" => "starter/45.gif", "time_end" => 10800, "opisan" => "�������� ������ ����� �� ������� ��������� ���� ���� ���������� ����� �� ������ ��� �����", "stl" => 0, "din" => 15,  "coal" => 20, "copper" => 10,  "silver" => 0,  "platinum" => 0, "gold" => 0),

       4   => array("name" => '���������� ���������������', "img" => "quest/rewards/univers_baf.gif", "time_end" => 10800, "opisan" => "����������� ����������� ���� �� + (������� * 25)",            "stl" => 0, "din" => 20,  "coal" => 15, "copper" => 10,  "silver" => 7,  "platinum" => 1, "gold" => 0),
       5   => array("name" => '���������� ���� ���������������', "img" => "quest/rewards/univers_baf.gif", "time_end" => 10800, "opisan" => "����������� ������ ������-�� ����� �� + (������� * 25)", "stl" => 0, "din" => 20,  "coal" => 15, "copper" => 10,  "silver" => 7,  "platinum" => 1, "gold" => 0),
       6   => array("name" => '���������� ������������', "img" => "quest/rewards/univers_baf.gif", "time_end" => 10800, "opisan" => "����������� ����������� �� + (������� * 25)",                    "stl" => 0, "din" => 20,  "coal" => 15, "copper" => 10,  "silver" => 7,  "platinum" => 1, "gold" => 0),
       7   => array("name" => '���������� ���� ������������', "img" => "quest/rewards/univers_baf.gif", "time_end" => 10800, "opisan" => "����������� ������ ����������� �� + (������� * 25)",        "stl" => 0, "din" => 20,  "coal" => 15, "copper" => 10,  "silver" => 7,  "platinum" => 1, "gold" => 0),

       8   => array("name" => '����� ���������� ��������', "img" => "quest/rewards/univers_baf.gif", "time_end" => 172800, "opisan" => "����������� �������� ������ ��������� �� -10%",           "stl" => 5, "din" => 0,  "coal" => 1, "copper" => 0,  "silver" => 0,  "platinum" => 0, "gold" => 0),
       9   => array("name" => '������� ���������� ��������', "img" => "quest/rewards/univers_baf.gif", "time_end" => 172800, "opisan" => "����������� �������� ������ ��������� �� -25%",         "stl" => 10, "din" => 0,  "coal" => 2, "copper" => 0,  "silver" => 0,  "platinum" => 0, "gold" => 0),
       10  => array("name" => '������� ���������� ��������', "img" => "quest/rewards/univers_baf.gif", "time_end" => 172800, "opisan" => "����������� �������� ������ ��������� �� -50%",         "stl" => 25, "din" => 0,  "coal" => 5, "copper" => 0,  "silver" => 0,  "platinum" => 0, "gold" => 0),

       11  => array("name" => '�������� ������������', "img" => "quest/rewards/univers_baf.gif", "time_end" => 259200, "opisan" => "����������� �������� ������������ ����� ��������� � ������ �� +100%", "stl" => 0, "din" => 10,  "coal" => 3, "copper" => 0,  "silver" => 0,  "platinum" => 0, "gold" => 0),
    );

  # ���������
    $ex_msg = "";

/***------------------------------------------
 * ����� �������
 **/

if (@$_GET['cmd_act'] == "ex_mineral_din") {

  $ex_mineral_din_sel = @$_GET['ex_mineral_din_sel'];
  $ex_mineral_din_val = abs($_GET['ex_mineral_din_val']);

  $gr  = $glass_data['glass_din_'.$ex_mineral_din_sel]['gr'];
  $din = $glass_data['glass_din_'.$ex_mineral_din_sel]['din'];
  $ex_mineral_stl_val_din = round($ex_mineral_din_val / $gr * $din * 100 ) / 100;

  $my_val = $mine_resources[$ex_mineral_din_sel]['val'];

  if (!in_array($ex_mineral_din_sel, array("coal", "copper", "platinum", "silver", "gold")) ) {
    $ex_msg = "����� �� �� ";
  } elseif ($ex_mineral_din_val <= 0) {
    $ex_msg = "����� �������";
  } elseif ($my_val < $ex_mineral_din_val) {
    $ex_msg = "�� ������� ���������";
  } else {

    $res_name = $mine_res_name[$ex_mineral_din_sel];
    $mine_resources[$ex_mineral_din_sel]['val'] -= $ex_mineral_din_val;

    $user_my->USER_upd_sql(", `money` = `money` + '".$ex_mineral_stl_val_din."', `mine_resources` = '".serialize($mine_resources)."'");
    $user_my->user['money'] += $ex_mineral_stl_val_din;
    $user_my->user['mine_resources'] = $mine_resources;

     # �������
        $my_coal     = empty($mine_resources['coal']['val'])?"0.00":$mine_resources['coal']['val'];
        $my_copper   = empty($mine_resources['copper']['val'])?"0.00":$mine_resources['copper']['val'];
        $my_silver   = empty($mine_resources['silver']['val'])?"0.00":$mine_resources['silver']['val'];
        $my_platinum = empty($mine_resources['platinum']['val'])?"0.00":$mine_resources['platinum']['val'];
        $my_gold     = empty($mine_resources['gold']['val'])?"0.00":$mine_resources['gold']['val'];
        $my_din      = $user_my->user['money'];

    $ex_msg = "����� �������� '".$res_name."' ".$ex_mineral_din_val." �� ".$ex_mineral_stl_val_din." ��.";
  }

}

/***------------------------------------------
 * ����� ����������
 **/

if (@$_GET['cmd_act'] == "ex_mineral_stl") {
  $ex_mineral_stl_sel = @$_GET['ex_mineral_stl_sel'];
  $ex_mineral_stl_val = abs($_GET['ex_mineral_stl_val']);

  $gr  = $glass_data['glass_stl_'.$ex_mineral_stl_sel]['gr'];
  $stl = $glass_data['glass_stl_'.$ex_mineral_stl_sel]['stl'];
  $ex_mineral_stl_val_stl = round($ex_mineral_stl_val / $gr * $stl * 100 ) / 100;

  $my_val = $mine_resources[$ex_mineral_stl_sel]['val'];

  if (!in_array($ex_mineral_stl_sel, array("coal", "copper", "platinum", "silver", "gold")) ) {
    $ex_msg = "����� �� �� ";
  } elseif ($ex_mineral_stl_val <= 0) {
    $ex_msg = "����� �������";
  } elseif ($my_val < $ex_mineral_stl_val) {
    $ex_msg = "�� ������� ���������";
  } else {

    $res_name = $mine_res_name[$ex_mineral_stl_sel];
    $mine_resources[$ex_mineral_stl_sel]['val'] -= $ex_mineral_stl_val;

    $user_my->USER_upd_sql(", `ekr` = `ekr` + '".$ex_mineral_stl_val_stl."', `mine_resources` = '".serialize($mine_resources)."'");
    $user_my->user['ekr'] += $ex_mineral_stl_val_stl;
    $user_my->user['mine_resources'] = $mine_resources;

     # �������
        $my_coal     = empty($mine_resources['coal']['val'])?"0.00":$mine_resources['coal']['val'];
        $my_copper   = empty($mine_resources['copper']['val'])?"0.00":$mine_resources['copper']['val'];
        $my_silver   = empty($mine_resources['silver']['val'])?"0.00":$mine_resources['silver']['val'];
        $my_platinum = empty($mine_resources['platinum']['val'])?"0.00":$mine_resources['platinum']['val'];
        $my_gold     = empty($mine_resources['gold']['val'])?"0.00":$mine_resources['gold']['val'];
        $my_din      = $user_my->user['money'];

    $ex_msg = "����� �������� '".$res_name."' ".$ex_mineral_stl_val." �� ".$ex_mineral_stl_val_stl." ���.";
  }
}

/***------------------------------------------
 * [ ������ ] = ��������� �� ����� �������
 **/

if (@$_GET['cmd_act'] == "ex_mineral_exmin") {

  $ex_mineral_exmin_sel = @$_GET['ex_mineral_exmin_sel'];      #
  $ex_mineral_exmin_val = abs($_GET['ex_mineral_exmin_val']);  # ���-��
  $ex_mineral_exmin_din = abs($_GET['ex_mineral_exmin_din']);  # ���� �� �����

  $ex_mineral_exmin_val_din = round($ex_mineral_exmin_val / 0.01 * $ex_mineral_exmin_din * 100 ) / 100;

  $ex_exmin_gr     = $glass_data['glass_din_'.$ex_mineral_exmin_sel]['din'];
  $ex_exmin_gr_max = conv_hundredths($ex_exmin_gr * 30, 2);

  $my_val = $mine_resources[$ex_mineral_exmin_sel]['val'];

  if (!in_array($ex_mineral_exmin_sel, array("coal", "copper", "platinum", "silver", "gold")) ) {
    $ex_msg = "����� �� �� ";
  } elseif ($ex_mineral_exmin_val <= 0) {
    $ex_msg = "����� �������";
  } elseif ($my_val < $ex_mineral_exmin_val) {
    $ex_msg = "�� ������� ���������";
  } elseif ($ex_mineral_exmin_din > $ex_exmin_gr_max || $ex_mineral_exmin_din < $ex_exmin_gr) {
    $ex_msg = "����������� ����� �� ����� ���������� �� ".$ex_exmin_gr." �� ".$ex_exmin_gr_max;
  } else {

    $res_name = $mine_res_name[$ex_mineral_exmin_sel];
    $mine_resources[$ex_mineral_exmin_sel]['val'] -= $ex_mineral_exmin_val;

    $user_my->USER_upd_sql(", `mine_resources` = '".serialize($mine_resources)."'");
    $user_my->user['mine_resources'] = $mine_resources;

 # ����� �������
   $date_bay = rand(1, 3) * rand(2, 24) * rand(2, 60) * 60;

    sql_query("INSERT INTO `exchange_minerals` (`owner`, `price_purchase`, `price_selling`, `amount`, `active`, `type_mineral`, `date_bay`, `datecreate`, `date_trans`, `incity`) values (
    '".$user_my->user_id."',
    '0',
    '".$ex_mineral_exmin_din."',
    '".$ex_mineral_exmin_val."',
    '1',
    '".$ex_mineral_exmin_sel."',
    '".(time() + $date_bay)."',
    '".time()."',
    '".date("Y.m.d", time())."',
    '".INCITY."');");

     # �������
        $my_coal     = empty($mine_resources['coal']['val'])?"0.00":$mine_resources['coal']['val'];
        $my_copper   = empty($mine_resources['copper']['val'])?"0.00":$mine_resources['copper']['val'];
        $my_silver   = empty($mine_resources['silver']['val'])?"0.00":$mine_resources['silver']['val'];
        $my_platinum = empty($mine_resources['platinum']['val'])?"0.00":$mine_resources['platinum']['val'];
        $my_gold     = empty($mine_resources['gold']['val'])?"0.00":$mine_resources['gold']['val'];

     $ex_msg = "������� �������� �� ����� ��� ������� '".$res_name."' ".$ex_mineral_exmin_val." �����. �� ".$ex_mineral_exmin_din." ��. �� �����.";
  }
}


/***------------------------------------------
 * [ ������ ] = ������ ������� �� ���.
 **/

if (@$_GET['cmd_act'] == "ex_stl_mineral") {

  $ex_stl_mineral_sel = @$_GET['ex_stl_mineral_sel'];      #
  $ex_stl_mineral_val = abs($_GET['ex_stl_mineral_val']);  # ���-��
  $need_stl           = $ex_stl_mineral_val * $glass_data['glass_din_'.$ex_stl_mineral_sel]['din'] * 100;  # �������� ���-�� ���.

  $my_val = $mine_resources[$ex_stl_mineral_sel]['val'];

  $exchange_minerals = sql_number("SELECT * FROM `exchange_minerals` WHERE `type_mineral` = '".$ex_stl_mineral_sel."' AND `date_bay` < '".time()."';");

  if (!in_array($ex_stl_mineral_sel, array("coal", "copper", "platinum", "silver", "gold")) ) {
    $ex_msg = "����� �� �� ";
  } elseif ($need_stl > $user_my->user['ekr']) {
    $ex_msg = "� ��� ������������ �������";
  } elseif ($ex_stl_mineral_val <= 0) {
    $ex_msg = "����� �������";
  } elseif ($exchange_minerals) {
    $ex_msg = "������ ����������. �� ����� ���� ������ �� ������� �������� '".$mine_res_name[$ex_stl_mineral_sel]."'";
  } else {

    $res_name = $mine_res_name[$ex_stl_mineral_sel];

    $mine_resources[$ex_stl_mineral_sel]['val'] += $ex_stl_mineral_val;

    $user_my->USER_upd_sql(", `mine_resources` = '".serialize($mine_resources)."', `ekr` = `ekr` - '".$need_stl."'");
    $user_my->user['mine_resources'] = $mine_resources;
    $user_my->user['ekr'] -= $need_stl;

     # �������
        $my_coal     = empty($mine_resources['coal']['val'])?"0.00":$mine_resources['coal']['val'];
        $my_copper   = empty($mine_resources['copper']['val'])?"0.00":$mine_resources['copper']['val'];
        $my_silver   = empty($mine_resources['silver']['val'])?"0.00":$mine_resources['silver']['val'];
        $my_platinum = empty($mine_resources['platinum']['val'])?"0.00":$mine_resources['platinum']['val'];
        $my_gold     = empty($mine_resources['gold']['val'])?"0.00":$mine_resources['gold']['val'];

     $ex_msg = "�� �������� ".$need_stl." ���. �� ".$ex_stl_mineral_val." �����. �������� '".$mine_res_name[$ex_stl_mineral_sel]."'";
  }
}

/***------------------------------------------
 * ����� ���������
 **/

if (@$_GET['cmd_act'] == "bayitem" && isset($_GET['bayitem'])) {

 $bayitem = abs($_GET['bayitem']);

 if (in_array($bayitem, array(11111177, 11111178, 11111179))) {

   $coal     = $ex_mineral_item[$bayitem]['coal'];
   $copper   = $ex_mineral_item[$bayitem]['copper'];
   $silver   = $ex_mineral_item[$bayitem]['silver'];
   $platinum = $ex_mineral_item[$bayitem]['platinum'];
   $gold     = $ex_mineral_item[$bayitem]['gold'];

   $din      = $ex_mineral_item[$bayitem]['din'];

   if ($din > $my_din) {
     $ex_msg = "� ��� �� ������� �������";
   } elseif  ($coal > $my_coal) {
     $ex_msg = $coal."� ��� �� ������� ����".$my_coal;
   } elseif ($copper > $my_copper) {
     $ex_msg = "� ��� �� ������� ����";
   } elseif ($silver > $my_silver) {
     $ex_msg = "� ��� �� ������� �������";
   } elseif ($platinum > $my_platinum) {
     $ex_msg = "� ��� �� ������� �������";
   } elseif ($gold > $my_gold) {
     $ex_msg = "� ��� �� ������� ������";
   } else {

        $item = sql_row("SELECT * FROM `shop` WHERE `id` = '".$bayitem."' LIMIT 1;");
        if (!empty($item['id'])) {

          $id_drop = fun_create_shop($item, $user_my->user_id);

           if ($id_drop) {
            if ($coal)     { $mine_resources['coal']['val']     -= $coal; }
            if ($copper)   { $mine_resources['copper']['val']   -= $copper; }
            if ($silver)   { $mine_resources['silver']['val']   -= $silver; }
            if ($platinum) { $mine_resources['platinum']['val'] -= $platinum; }
            if ($gold)     { $mine_resources['gold']['val']     -= $gold; }

             $user_my->USER_upd_sql(", `mine_resources` = '".serialize($mine_resources)."', `money` = `money` - '".$din."'");
             $user_my->user['money'] -= $din;
             $ex_msg = "�� ������ '".$item['name']."'";

           # �������
              $my_coal     = empty($mine_resources['coal']['val'])?"0.00":$mine_resources['coal']['val'];
              $my_copper   = empty($mine_resources['copper']['val'])?"0.00":$mine_resources['copper']['val'];
              $my_silver   = empty($mine_resources['silver']['val'])?"0.00":$mine_resources['silver']['val'];
              $my_platinum = empty($mine_resources['platinum']['val'])?"0.00":$mine_resources['platinum']['val'];
              $my_gold     = empty($mine_resources['gold']['val'])?"0.00":$mine_resources['gold']['val'];
              $my_din      = $user_my->user['money'];

           }

        }

   }

 }

}

/***------------------------------------------
 * ����� ���
 **/

if (@$_GET['cmd_act'] == "bayrune" && isset($_GET['bayrune'])) {

 $bayrune = abs($_GET['bayrune']);

 if (in_array($bayrune, array(103, 121, 11111114, 1547, 129, 1455))) {

   $coal     = $ex_mineral_rune[$bayrune]['coal'];
   $copper   = $ex_mineral_rune[$bayrune]['copper'];
   $silver   = $ex_mineral_rune[$bayrune]['silver'];
   $platinum = $ex_mineral_rune[$bayrune]['platinum'];
   $gold     = $ex_mineral_rune[$bayrune]['gold'];

   $din      = $ex_mineral_rune[$bayrune]['din'];
   $stl      = $ex_mineral_rune[$bayrune]['stl'];

   if ($stl > $my_stl) {
     $ex_msg = "� ��� �� ������� ����������";
   } elseif  ($din > $my_din) {
     $ex_msg = "� ��� �� ������� �������";
   } elseif  ($coal > $my_coal) {
     $ex_msg = "� ��� �� ������� ����";
   } elseif ($copper > $my_copper) {
     $ex_msg = "� ��� �� ������� ����";
   } elseif ($silver > $my_silver) {
     $ex_msg = "� ��� �� ������� �������";
   } elseif ($platinum > $my_platinum) {
     $ex_msg = "� ��� �� ������� �������";
   } elseif ($gold > $my_gold) {
     $ex_msg = "� ��� �� ������� ������";
   } else {

        $item = sql_row("SELECT * FROM `shop` WHERE `id` = '".$bayrune."' LIMIT 1;");
        if (!empty($item['id'])) {

          $item['cost'] = 0;
          $item['name'] = $ex_mineral_rune[$bayrune]['name'];
          if ($ex_mineral_rune[$bayrune]['opisan']) $item['opisan'] = $ex_mineral_rune[$bayrune]['opisan'];

          $id_drop = fun_create_shop($item, $user_my->user_id);

           if ($id_drop) {

             if ($coal)     { $mine_resources['coal']['val']     -= $coal; }
             if ($copper)   { $mine_resources['copper']['val']   -= $copper; }
             if ($silver)   { $mine_resources['silver']['val']   -= $silver; }
             if ($platinum) { $mine_resources['platinum']['val'] -= $platinum; }
             if ($gold)     { $mine_resources['gold']['val']     -= $gold; }

             $user_my->USER_upd_sql(", `mine_resources` = '".serialize($mine_resources)."', `money` = `money` - '".$din."', `ekr` = `ekr` - '".$stl."'");
             $user_my->user['money'] -= $din;
             $user_my->user['ekr']   -= $stl;
             $ex_msg = "�� ������ '".$item['name']."'";

           # �������
              $my_coal     = empty($mine_resources['coal']['val'])?"0.00":$mine_resources['coal']['val'];
              $my_copper   = empty($mine_resources['copper']['val'])?"0.00":$mine_resources['copper']['val'];
              $my_silver   = empty($mine_resources['silver']['val'])?"0.00":$mine_resources['silver']['val'];
              $my_platinum = empty($mine_resources['platinum']['val'])?"0.00":$mine_resources['platinum']['val'];
              $my_gold     = empty($mine_resources['gold']['val'])?"0.00":$mine_resources['gold']['val'];
              $my_din      = $user_my->user['money'];
              $my_stl      = $user_my->user['ekr'];

           }

        }

   }

 }

}


/***------------------------------------------
 * ����� ���
 **/

if (@$_GET['cmd_act'] == "baybaf" && isset($_GET['baybaf'])) {

 $bayrune = abs($_GET['baybaf']);
 if (in_array($bayrune, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11))) {

   $coal     = $ex_mineral_baf[$bayrune]['coal'];
   $copper   = $ex_mineral_baf[$bayrune]['copper'];
   $silver   = $ex_mineral_baf[$bayrune]['silver'];
   $platinum = $ex_mineral_baf[$bayrune]['platinum'];
   $gold     = $ex_mineral_baf[$bayrune]['gold'];

   $din      = $ex_mineral_baf[$bayrune]['din'];
   $stl      = $ex_mineral_baf[$bayrune]['stl'];

   if ($stl > $my_stl) {
     $ex_msg = "� ��� �� ������� ����������";
   } elseif ($din > $my_din) {
     $ex_msg = "� ��� �� ������� �������";
   } elseif  ($coal > $my_coal) {
     $ex_msg = "� ��� �� ������� ����";
   } elseif ($copper > $my_copper) {
     $ex_msg = "� ��� �� ������� ����";
   } elseif ($silver > $my_silver) {
     $ex_msg = "� ��� �� ������� �������";
   } elseif ($platinum > $my_platinum) {
     $ex_msg = "� ��� �� ������� �������";
   } elseif ($gold > $my_gold) {
     $ex_msg = "� ��� �� ������� ������";
   } else {

        $baf_ok   = false;
        $baf_name = $ex_mineral_baf[$bayrune]['name'];
        $baf_time_end = $ex_mineral_baf[$bayrune]['time_end'];
        $baf_opisan = $ex_mineral_baf[$bayrune]['opisan'];

        $baf_eff  = array();

        switch ($bayrune) {
          case 1: # ����� ����� �����
          case 2: # ������� ����� �����
          case 3: # ������� ����� �����
               $baf_eff['name']      = $baf_name;
               $baf_eff['type']      = 1092;
               $baf_eff['time']      = time() + $baf_time_end;

             # ���� ����
               $arexp   = array(0, 200, 400, 800);
               $val_inv = ($base_mods[$user['level']][0]) * $arexp[$bayrune] / 100;

               $baf_eff['opisan']    = $baf_opisan;
               $baf_eff['total']     = "+".$val_inv;
               $baf_eff['from_whom'] = "����� ��������";
               $baf_ok   = true;
          break;
          case 4: # ���������� ���������������
          case 5: # ���������� ���� ���������������
          case 6: # ���������� ������������
          case 7: # ���������� ���� ������������

               $eff_speed  = sql_row("SELECT `name` FROM `effects` WHERE `owner` = '".$user_my->user_id."' AND `type` = 1093;");

               if (empty($eff_speed['name'])) {

                 $baf_eff['name'] = $baf_name." [".$user_my->user['level']."]";
                 $baf_eff['type'] = 1093;
                 $baf_eff['time'] = time() + $baf_time_end;

                 $val_inv = $user_my->user['level'] * 25;

                 if ($bayrune == 4) { $baf_eff['inv_krit']  = $val_inv; $user_my->upd_sql .= ", `inv_krit`  = `inv_krit`  + ".$val_inv.""; } # ����������� ����
                 if ($bayrune == 5) { $baf_eff['inv_akrit'] = $val_inv; $user_my->upd_sql .= ", `inv_akrit` = `inv_akrit` + ".$val_inv.""; } # ������ ������-�� �����
                 if ($bayrune == 6) { $baf_eff['inv_uvor']  = $val_inv; $user_my->upd_sql .= ", `inv_uvor`  = `inv_uvor`  + ".$val_inv.""; } # �����������
                 if ($bayrune == 7) { $baf_eff['inv_auvor'] = $val_inv; $user_my->upd_sql .= ", `inv_auvor` = `inv_auvor` + ".$val_inv.""; } # ������ �����������

                 $baf_eff['opisan']    = $baf_opisan;
                 $baf_eff['total']     = "+".$val_inv;
                 $baf_eff['from_whom'] = "����� ��������";
                 $baf_ok   = true;

               } else {
                 $ex_msg   = "� ��� ��� ���� ���";
                 $baf_ok   = false;
               }
          break;
          case 8:  # ����� ���������� ��������
          case 9:  # ������� ���������� ��������
          case 10: # ������� ���������� ��������
               $eff_speed  = sql_row("SELECT `name` FROM `effects` WHERE `owner` = '".$user_my->user_id."' AND `type` = 1090;");

               if (empty($eff_speed['name'])) {

                 $baf_eff['name'] = $baf_name;
                 $baf_eff['type'] = 1090;
                 $baf_eff['time'] = time() + 2*24*60*60;

                 if ($bayrune == 8)  { $val_inv = 10; } # �����
                 if ($bayrune == 9)  { $val_inv = 25; } # �������
                 if ($bayrune == 10) { $val_inv = 50; } # �������

                 $baf_eff['opisan']    = $baf_opisan;
                 $baf_eff['total']     = "-".$val_inv;
                 $baf_eff['from_whom'] = "����� ��������";
                 $baf_ok   = true;

               } else {
                 $ex_msg   = "� ��� ��� ���� ���";
                 $baf_ok   = false;
               }
          break;
          case 11: # �������� ������������
               $eff_speed  = sql_row("SELECT `name` FROM `effects` WHERE `owner` = '".$user_my->user_id."' AND `type` = 1089;");

               if (empty($eff_speed['name'])) {

                 $baf_eff['name'] = $baf_name;
                 $baf_eff['type'] = 1089;
                 $baf_eff['time'] = time() + $baf_time_end;

                 $val_inv         = 100;

                 $baf_eff['opisan']    = $baf_opisan;
                 $baf_eff['total']     = $val_inv;
                 $baf_eff['from_whom'] = "����� ��������";
                 $baf_ok   = true;

                 $eff_all = $user_my->user['eff_all'].'1089;';
                 $user_my->USER_upd_sql(",`eff_all` = '".$eff_all."'");

               } else {
                 $ex_msg   = "� ��� ��� ���� ���";
                 $baf_ok   = false;
               }
          break;
        }


        if ($baf_ok) {

             if ($coal)     { $mine_resources['coal']['val']     -= $coal; }
             if ($copper)   { $mine_resources['copper']['val']   -= $copper; }
             if ($silver)   { $mine_resources['silver']['val']   -= $silver; }
             if ($platinum) { $mine_resources['platinum']['val'] -= $platinum; }
             if ($gold)     { $mine_resources['gold']['val']     -= $gold; }

             $user_my->USER_add_effect($baf_eff);

             $user_my->USER_upd_sql(", `mine_resources` = '".serialize($mine_resources)."', `money` = `money` - '".$din."', `ekr` = `ekr` - '".$stl."'");
             $user_my->user['money'] -= $din;
             $user_my->user['ekr']   -= $stl;

             $ex_msg = "�� ������ '".$baf_name."'";

           # �������
              $my_coal     = empty($mine_resources['coal']['val'])?"0.00":$mine_resources['coal']['val'];
              $my_copper   = empty($mine_resources['copper']['val'])?"0.00":$mine_resources['copper']['val'];
              $my_silver   = empty($mine_resources['silver']['val'])?"0.00":$mine_resources['silver']['val'];
              $my_platinum = empty($mine_resources['platinum']['val'])?"0.00":$mine_resources['platinum']['val'];
              $my_gold     = empty($mine_resources['gold']['val'])?"0.00":$mine_resources['gold']['val'];
              $my_din      = $user_my->user['money'];

        } else {
          // ������� ��� �� ������
        }

   }
 }
}

?>
    <style type="text/css">.craft td{ padding: 3px; } .craft .craft_td_l{ text-align: right; } #tab_cur_ojb td { padding: 3px; }
      #m_menu li{padding: 3px;cursor: pointer;}
      #m_menu li:hover {text-decoration: underline;}
      #m_menu li a{font-weight: normal;}
      #m_menu .m_li_sel {font-weight: bold;background: url('http://img.blutbad.ru/i/quest/dialog.png') no-repeat 0 2px;/*margin: .7em 0;*/ padding: 1px 0 2px 18px;margin-left: -18px;}
      #tb_mon .tdl {text-align: right; }

    </style>
    <script>
      function p_din_f() {
      	var result;
      	var tmp;

  	    var din_text = new Array();

        din_text['gold']     = new Array(<?= $glass_data['glass_din_gold']['gr'] ?>, <?= $glass_data['glass_din_gold']['din'] ?>);
        din_text['platinum'] = new Array(<?= $glass_data['glass_din_platinum']['gr'] ?>, <?= $glass_data['glass_din_platinum']['din'] ?>);
        din_text['silver']   = new Array(<?= $glass_data['glass_din_silver']['gr'] ?>, <?= $glass_data['glass_din_silver']['din'] ?>);
        din_text['copper']   = new Array(<?= $glass_data['glass_din_copper']['gr'] ?>, <?= $glass_data['glass_din_copper']['din'] ?>);
        din_text['coal']     = new Array(<?= $glass_data['glass_din_coal']['gr'] ?>, <?= $glass_data['glass_din_coal']['din'] ?>);

        var cur_mineral     = $("#ex_mineral_din_sel option:selected").attr("value");

        var ex_din_gr  = parseFloat(din_text[cur_mineral][0]).toFixed(2);
        var ex_din_din = parseFloat(din_text[cur_mineral][1]).toFixed(2);

        var ex_din_text = $("#ex_din_text");
        ex_din_text.html('������� ���� ������ ����������: ' + ex_din_gr + ' �����. = ' + ex_din_din + ' ����� (���� ����� ��������)');

      	tmp = document.forms['din_form'].elements['ex_mineral_din_val'].value;
      	result = Math.round(parseFloat(tmp).toFixed(2) / ex_din_gr * ex_din_din * 100 ) / 100;

      	if (isNaN(result) || result<0) {
      		document.forms['din_form'].elements['ex_mineral_din_res'].value = '0.0';
      	} else {
      		document.forms['din_form'].elements['ex_mineral_din_res'].value = result;
      	}
      }
      function p_stl_f() {
      	var result;
      	var tmp;

 	    var stl_text = new Array();

        stl_text['gold']     = new Array(<?= $glass_data['glass_stl_gold']['gr'] ?>, <?= $glass_data['glass_stl_gold']['stl'] ?>);
        stl_text['platinum'] = new Array(<?= $glass_data['glass_stl_platinum']['gr'] ?>, <?= $glass_data['glass_stl_platinum']['stl'] ?>);
        stl_text['silver']   = new Array(<?= $glass_data['glass_stl_silver']['gr'] ?>, <?= $glass_data['glass_stl_silver']['stl'] ?>);
        stl_text['copper']   = new Array(<?= $glass_data['glass_stl_copper']['gr'] ?>, <?= $glass_data['glass_stl_copper']['stl'] ?>);
        stl_text['coal']     = new Array(<?= $glass_data['glass_stl_coal']['gr'] ?>, <?= $glass_data['glass_stl_coal']['stl'] ?>);

        var cur_mineral     = $("#ex_mineral_stl_sel option:selected").attr("value");

        var ex_stl_gr  = parseFloat(stl_text[cur_mineral][0]).toFixed(2);
        var ex_stl_stl = parseFloat(stl_text[cur_mineral][1]).toFixed(2);

        var ex_stl_text = $("#ex_stl_text");
        ex_stl_text.html('������� ���� ������ ����������: ' + ex_stl_gr + ' �����. = ' + ex_stl_stl + ' ���������� (���� ����� ��������)');

      	tmp = document.forms['stl_form'].elements['ex_mineral_stl_val'].value;
      	result = Math.round(parseFloat(tmp).toFixed(2) / ex_stl_gr * ex_stl_stl * 100 ) / 100;

      	if (isNaN(result) || result<0) {
      		document.forms['stl_form'].elements['ex_mineral_stl_res'].value = '0.0';
      	} else {
      		document.forms['stl_form'].elements['ex_mineral_stl_res'].value = result;
      	}
      }
      function p_exmin_f() {
      	var result;
      	var tmp;

  	    var din_text = new Array();

        din_text['gold']     = new Array(<?= $glass_data['glass_din_gold']['gr'] ?>, <?= $glass_data['glass_din_gold']['din'] ?>);
        din_text['platinum'] = new Array(<?= $glass_data['glass_din_platinum']['gr'] ?>, <?= $glass_data['glass_din_platinum']['din'] ?>);
        din_text['silver']   = new Array(<?= $glass_data['glass_din_silver']['gr'] ?>, <?= $glass_data['glass_din_silver']['din'] ?>);
        din_text['copper']   = new Array(<?= $glass_data['glass_din_copper']['gr'] ?>, <?= $glass_data['glass_din_copper']['din'] ?>);
        din_text['coal']     = new Array(<?= $glass_data['glass_din_coal']['gr'] ?>, <?= $glass_data['glass_din_coal']['din'] ?>);

        var cur_mineral = $("#ex_mineral_exmin_sel option:selected").attr("value");
        var ex_exmin_din = $("#ex_mineral_exmin_din").attr("value");

        var ex_exmin_gr = parseFloat(din_text[cur_mineral][1]).toFixed(2);
        var ex_exmin_gr_max = parseFloat(ex_exmin_gr * 30).toFixed(2);

        if (ex_exmin_din <= ex_exmin_gr_max && ex_exmin_din >= ex_exmin_gr) {
           $("#ex_exmin_text").html("&nbsp;");
        } else {
           $("#ex_exmin_text").html("����������� ����� �� ����� ���������� �� " + ex_exmin_gr + " �� " + ex_exmin_gr_max);
        }

      	tmp = document.forms['exmin_form'].elements['ex_mineral_exmin_val'].value;
      	result = Math.round(parseFloat(tmp).toFixed(2) / 0.01 * ex_exmin_din * 100 ) / 100;

      	if (isNaN(result) || result<0) {
      		document.forms['exmin_form'].elements['ex_mineral_exmin_res'].value = '0.0';
      	} else {
      		document.forms['exmin_form'].elements['ex_mineral_exmin_res'].value = result;
      	}
      }
      function p_stl_mineral_f() {
      	var result;
      	var tmp;

  	    var din_text = new Array();

        din_text['gold']     = new Array(<?= $glass_data['glass_din_gold']['gr'] ?>, <?= $glass_data['glass_din_gold']['din'] ?>);
        din_text['platinum'] = new Array(<?= $glass_data['glass_din_platinum']['gr'] ?>, <?= $glass_data['glass_din_platinum']['din'] ?>);
        din_text['silver']   = new Array(<?= $glass_data['glass_din_silver']['gr'] ?>, <?= $glass_data['glass_din_silver']['din'] ?>);
        din_text['copper']   = new Array(<?= $glass_data['glass_din_copper']['gr'] ?>, <?= $glass_data['glass_din_copper']['din'] ?>);
        din_text['coal']     = new Array(<?= $glass_data['glass_din_coal']['gr'] ?>, <?= $glass_data['glass_din_coal']['din'] ?>);

        var cur_mineral = $("#ex_stl_mineral_sel option:selected").attr("value");
        var ex_stl_mineral = $("#ex_stl_mineral_val").attr("value");

        var ex_exmin_gr = parseFloat(din_text[cur_mineral][1]).toFixed(2);
        var ex_exmin_gr_max = parseFloat(ex_exmin_gr * 30).toFixed(2);

        $("#stl_mineral_text").html("������� ���� ������� ����������: 0.01 �����. = " + ex_exmin_gr + " ���. (���� ����� ��������)");

      	result = parseFloat(ex_stl_mineral * ex_exmin_gr * 100).toFixed(2);

      	if (isNaN(result) || result<0) {
      		document.forms['stl_mineral_form'].elements['ex_stl_mineral_res'].value = '0.00';
      	} else {
      		document.forms['stl_mineral_form'].elements['ex_stl_mineral_res'].value = result;
      	}
      }


    </script>

<?
 echo $ex_msg?'<div style="text-align: center;"><div style="border: 1px solid #556775;margin-bottom: 10px;padding: 5px;background-color: #FFCE78;display: inline-block;padding-left: 10px;padding-right: 10px;"><b>'.$ex_msg.'</b></div></div>':'';
?>

<table style="width: 100%;">
  <tr>
    <td style="width: 220px;">
      <fieldset id="auth">
        <legend>��� ��������</legend>
        <table class="craft">
          <tr>
            <td class="craft_td_l" style="vertical-align: middle;">������ <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_gold.gif" alt="gold" title="������" width="15" height="15">: </td>
            <td><b id="s_gold" style="vertical-align: middle;"><?= conv_hundredths($my_gold, 2) ?></b> �����.</td>
          </tr>
          <tr>
            <td class="craft_td_l" style="vertical-align: middle;">������� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_platinum.gif" alt="platinum" title="�������" width="15" height="15">:</td>
            <td><b id="s_platinum" style="vertical-align: middle;"><?= conv_hundredths($my_platinum, 2) ?></b> �����.</td>
          </tr>
          <tr>
            <td class="craft_td_l" style="vertical-align: middle;">������� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_silver.gif" alt="silver" title="�������" width="15" height="15">:</td>
            <td><b id="s_silver" style="vertical-align: middle;"><?= conv_hundredths($my_silver, 2) ?></b> �����.</td>
          </tr>
          <tr>
            <td class="craft_td_l" style="vertical-align: middle;">���� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_copper.gif" alt="copper" title="����" width="15" height="15">:</td>
            <td><b id="s_copper" style="vertical-align: middle;"><?= conv_hundredths($my_copper, 2) ?></b> �����.</td>
          </tr>
          <tr>
            <td class="craft_td_l" style="vertical-align: middle;">����� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_coal.gif" alt="coal" title="�����" width="15" height="15">:</td>
            <td><b id="s_coal" style="vertical-align: middle;"><?= conv_hundredths($my_coal, 2) ?></b> �����.</td>
          </tr>
        </table>
      </fieldset>
    </td>
    <td>
      <div style="padding-left: 10px; padding-right: 35px;">
       <?
       switch ($_GET['menu']) {
         case "m_money":
           ?>
            <style type="text/css">
             .slot { display: inline-block; } .sbay{ cursor: pointer; } .sbay_b{ font-weight: bold; }
             #td_rune, #td_item{ display: none; }
            </style>
            <script>
                $(function(){

            		$('.sbay').click(function(){

                        cur_show = $(this).attr('id');

                        $('.sbay').removeClass("sbay_b");
                        $(this).addClass("sbay_b");

                        if (cur_show == "s_money") {
                         $('td #d_money_exchange').show();
                         $('td #d_mineral_exchange').hide();
                        } else if (cur_show == "s_mineral") {
                         $('td #d_money_exchange').hide();
                         $('td #d_mineral_exchange').show();
                        }

            			return false;
            		});

                    <?
                        if ($_GET['cmd_act'] == "ex_stl_mineral") {
                          ?>
                            $('td #d_money_exchange').hide();
                            $('td #d_mineral_exchange').show();
                            $('.sbay').removeClass("sbay_b");
                            $('#s_mineral').addClass("sbay_b");
                          <?
                        }
                    ?>

                });
            </script>

            <div id="s_shows" style="border: 1px solid #556775;padding: 5px;margin-bottom: 10px;background-color: #DAD8D8;">��������: <span class="sbay sbay_b" id="s_money">[ �������� ]</span> <span class="sbay" id="s_mineral">[ ������ ]</span></div>

            <div id="d_money_exchange">

              <fieldset id="auth" style="width: 100%;">
                <legend>����� ��������� �� ������</legend>

                  <fieldset id="auth" style="width: 485px;background-color: #E4E4E4;">
                    <legend>����� �� ������</legend>
                     <span id="ex_din_text">������� ���� ������ ����������: <?= $glass_data['glass_din_coal']['gr'] ?> �����. = <?= $glass_data['glass_din_coal']['din'] ?> ����� (���� ����� ��������)</span>
                     <br><br>
                     <form action="<?= $cur_url ?>" method="get" name="din_form">
                        <input type="hidden" name="cmd_act" value="ex_mineral_din">

                        <input type="hidden" name="cmd" value="<?= @$_GET['cmd'] ?>">
                        <input type="hidden" name="nid" value="<?= @$_GET['nid'] ?>">
                        <input type="hidden" name="dialog_id" value="<?= @$_GET['dialog_id'] ?>">
                        <input type="hidden" name="qwest_id" value="<?= @$_GET['qwest_id'] ?>">
                        <input type="hidden" name="nd" value="<?= @$_GET['nd'] ?>">

                        �������:&nbsp;
                        <select name="ex_mineral_din_sel" size="1" id="ex_mineral_din_sel" onchange="p_din_f()">
                            <option value="gold">������</option>
                            <option value="platinum">�������</option>
                            <option value="silver" >�������</option>
                            <option value="copper">����</option>
                            <option value="coal" selected="selected">�����</option>
                        </select>
                        ��������:&nbsp;
                        <input name="ex_mineral_din_val" type="text" onkeyup="javascript:p_din_f()" size="6" value="0.00" class="text" style="width: 60px;">
                        &nbsp;=&nbsp;
                        <input name="ex_mineral_din_res" type="text" size="6" value="0.00" class="text" disabled="disabled" readonly="readonly" style="width: 60px;">
                        &nbsp;��.
                        <input type="submit" value="��������" class="xbbutton">
                     </form>
                  </fieldset>
                  <br><br>
                  <fieldset id="auth" style="width: 485px;background-color: #E4E4E4;">
                    <legend>����� �� ���������</legend>
                     <span id="ex_stl_text">������� ���� ������ ����������: <?= $glass_data['glass_stl_coal']['gr'] ?> �����. = <?= $glass_data['glass_stl_coal']['stl'] ?> ���������� (���� ����� ��������)</span>
                     <br><br>
                     <form action="<?= $cur_url ?>" method="get" name="stl_form">
                        <input type="hidden" name="cmd_act" value="ex_mineral_stl">

                        <input type="hidden" name="cmd" value="<?= @$_GET['cmd'] ?>">
                        <input type="hidden" name="nid" value="<?= @$_GET['nid'] ?>">
                        <input type="hidden" name="dialog_id" value="<?= @$_GET['dialog_id'] ?>">
                        <input type="hidden" name="qwest_id" value="<?= @$_GET['qwest_id'] ?>">
                        <input type="hidden" name="nd" value="<?= @$_GET['nd'] ?>">

                        �������:&nbsp;
                        <select name="ex_mineral_stl_sel" size="1" id="ex_mineral_stl_sel" onchange="javascript:p_stl_f()">
                            <option value="gold">������</option>
                            <option value="platinum">�������</option>
                            <option value="silver" >�������</option>
                            <option value="copper">����</option>
                            <option value="coal" selected="selected">�����</option>
                        </select>
                        ��������:&nbsp;
                        <input name="ex_mineral_stl_val" type="text" onkeyup="javascript:p_stl_f()" size="6" value="0.00" class="text" style="width: 60px;">
                        &nbsp;=&nbsp;
                        <input name="ex_mineral_stl_res" type="text" size="6" value="0.00" class="text" disabled="disabled" readonly="readonly" style="width: 60px;">
                        &nbsp;���.
                        <input type="submit" value="��������" class="xbbutton">
                     </form>
                  </fieldset>

              </fieldset>
              <br>
              <fieldset id="auth" style="width: 100%;">
                <legend>��������� �� ����� ��������</legend>
                     <span id="ex_exmin_text" style="color: red;font-weight: bold;">&nbsp;</span>
                     <br><br>
                     <form action="<?= $cur_url ?>" method="get" name="exmin_form">
                        <input type="hidden" name="cmd_act" value="ex_mineral_exmin">

                        <input type="hidden" name="cmd" value="<?= @$_GET['cmd'] ?>">
                        <input type="hidden" name="nid" value="<?= @$_GET['nid'] ?>">
                        <input type="hidden" name="dialog_id" value="<?= @$_GET['dialog_id'] ?>">
                        <input type="hidden" name="qwest_id" value="<?= @$_GET['qwest_id'] ?>">
                        <input type="hidden" name="nd" value="<?= @$_GET['nd'] ?>">

                        �������:&nbsp;
                        <select name="ex_mineral_exmin_sel" size="1" id="ex_mineral_exmin_sel" onchange="javascript:p_exmin_f()">
                            <option value="gold">������</option>
                            <option value="platinum">�������</option>
                            <option value="silver" >�������</option>
                            <option value="copper">����</option>
                            <option value="coal" selected="selected">�����</option>
                        </select>
                        ���-��:&nbsp;
                        <input id="ex_mineral_exmin_val" name="ex_mineral_exmin_val" type="text" onkeyup="javascript:p_exmin_f()" size="6" value="0.00" class="text" style="width: 60px;">
                        �����.&nbsp; ���� �� �����:&nbsp;
                        <input id="ex_mineral_exmin_din" name="ex_mineral_exmin_din" type="text" onkeyup="javascript:p_exmin_f()" size="6" value="0.00" class="text"style="width: 60px;">
                        ��.&nbsp; �����:&nbsp;
                        <input id="ex_mineral_exmin_res" name="ex_mineral_exmin_res" type="text" size="6" value="0.00" class="text" style="width: 60px;" disabled="disabled" readonly="readonly">
                        ��.
                        <input type="submit" value="���������" class="xbbutton">
                     </form>
                     <br>
                     ��� ������ �� �������:<br>

                     <?
                     $exchange_minerals = sql_query("SELECT * FROM `exchange_minerals` WHERE `owner` = '".$user_my->user_id."' ORDER BY `id` ASC LIMIT 10;");
                     if (num_rows($exchange_minerals) == 0) {
                       echo "��� ������";
                     } else {

                     ?>
                      <table class="table-list shop-table buy-table">
                  		<thead>
                  			<tr>
                  				<th>#</th>
                  				<th>�������</th>
                  				<th>����������</th>
                  				<th>���� �� �����</th>
                  				<th>��������</th>
                  			</tr>
                  		</thead>
                  		<tbody>
                                <?
                                $m_i = 0;
                                 while ($row = fetch_array($exchange_minerals)) {
                                  $m_i++;
                                ?>
                            	  <tr>
                  				<th class="number"><?= $m_i ?></th>
                  				<td class="img">
                                     <?= $mine_res_name[$row['type_mineral']] ?>
                  				</td>
                  				<td class="name">
                  					<?= $row['amount'] ?>
                  				</td>
                  				<td class="bids">
                                    <?= $row['price_selling'] ?>
                  				</td>
                  				<td class="stats">

                  				</td>
                  			</tr>
                              <? } ?>
                  		</tbody>
                      </table>
                     <? } ?>
              </fieldset>

            </div>

            <div id="d_mineral_exchange" style="display: none">
                  <fieldset id="auth" style="width: 485px;background-color: #E4E4E4;">
                    <legend>������ ���������</legend>
                     <span id="stl_mineral_text">������� ���� ������� ����������: 0.01 �����. = 0.01 ���. (���� ����� ��������)</span>
                     <br><br>
                     <form action="<?= $cur_url ?>" method="get" name="stl_mineral_form">
                        <input type="hidden" name="cmd_act" value="ex_stl_mineral">

                        <input type="hidden" name="cmd" value="<?= @$_GET['cmd'] ?>">
                        <input type="hidden" name="nid" value="<?= @$_GET['nid'] ?>">
                        <input type="hidden" name="dialog_id" value="<?= @$_GET['dialog_id'] ?>">
                        <input type="hidden" name="qwest_id" value="<?= @$_GET['qwest_id'] ?>">
                        <input type="hidden" name="nd" value="<?= @$_GET['nd'] ?>">

                        �������:&nbsp;
                        <select name="ex_stl_mineral_sel" size="1" id="ex_stl_mineral_sel" onchange="p_stl_mineral_f()">
                            <option value="gold">������</option>
                            <option value="platinum">�������</option>
                            <option value="silver" >�������</option>
                            <option value="copper">����</option>
                            <option value="coal" selected="selected">�����</option>
                        </select>
                        ������:&nbsp;
                        <input name="ex_stl_mineral_val" id="ex_stl_mineral_val" type="text" onkeyup="javascript:p_stl_mineral_f()" size="6" value="0.00" class="text" style="width: 60px;">
                        &nbsp;=&nbsp;
                        <input name="ex_stl_mineral_res" type="text" size="6" value="0.00" class="text" disabled="disabled" readonly="readonly" style="width: 60px;">
                        &nbsp;���.
                        <input type="submit" value="������" class="xbbutton">
                     </form>
                  </fieldset>
            </div>

           <?
         break;
         case "m_item":
           ?>
            <fieldset id="auth" style="width: 100%;">
              <legend>������� ���������</legend>
              <?
              	$data_bay_pick = sql_query("SELECT * FROM `shop` WHERE
                      `cost`      = 0
                  AND `razdel`    = 19
                  ORDER BY `nlevel`, `name` ASC;");

               # $data_sale_count = num_rows($data_bay_pick);
              ?>

              <table class="table-list shop-table buy-table" style="margin-top: 0em;">
              		<thead>
              			<tr>
              				<th>#</th>
              				<th>�������</th>
              				<th>�������</th>
              				<th></th>
              			</tr>
              		</thead>
              		<tbody>
                      <?
                       $i_it = 0;

                       while($row = fetch_array($data_bay_pick)) {

                              $i_it++;
                      ?>
              			<tr>
              				<th><?= $i_it ?></th>
              				<td class="img" style="text-align: left;">
                                <table>
                                  <tr>
                                    <td style="border: 0px;width: 70px;text-align: center;">
                                      <img alt="<?= $row['name'] ?>" class="thumb <?= (fun_broken_item($row)?" broken":"") ?> button" data-type-num="<?= $row['type'] ?>" onclick="big(\'<?= convert_item_data($row['razdel'], 1) ?>\', <?= $row['num'] ?>)" src="http://img.blutbad.ru/i/<?= $row['img'] ?>" title="<?= $row['name'] ?>">
                                    </td>
                                    <td style="border: 0px;">
                                       <?= show_invisible($row, 0, false) ?>  <!--     padding-left: 45px; -->
                                    </td>
                                  </tr>
                                </table>
              				</td>
              				<td align="right" class="nowrap">
              					<table class="actions-table">
              						<tbody>

              							<tr>
              								<td>
                                                <table id="tb_mon">

              									<?
                                                 $din      = $ex_mineral_item[$row['id']]['din'];
                                                 $stl      = @$ex_mineral_item[$row['id']]['stl'];

                                                 $coal     = $ex_mineral_item[$row['id']]['coal'];
                                                 $copper   = $ex_mineral_item[$row['id']]['copper'];
                                                 $silver   = $ex_mineral_item[$row['id']]['silver'];
                                                 $platinum = $ex_mineral_item[$row['id']]['platinum'];
                                                 $gold     = $ex_mineral_item[$row['id']]['gold'];

                                                 if ($din)      { echo '<tr><td class="tdl">�����:</td><td>'.$din.' �����.</td></tr>'; }
                                                 if ($stl)      { echo '<tr><td class="tdl">��������:</td><td>'.$stl.' ���.</td></tr>'; }

                                                 if ($coal)     { echo '<tr><td class="tdl">����� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_coal.gif" alt="coal" title="�����" width="15" height="15">:</td><td>'.$coal.' �����.</td></tr>'; }
                                                 if ($copper)   { echo '<tr><td class="tdl">���� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_copper.gif" alt="copper" title="����" width="15" height="15">:</td><td>'.$copper.' �����.</td></tr>'; }
                                                 if ($silver)   { echo '<tr><td class="tdl">������� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_silver.gif" alt="silver" title="�������" width="15" height="15">:</td><td>'.$silver.' �����.</td></tr>'; }
                                                 if ($platinum) { echo '<tr><td class="tdl">������� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_platinum.gif" alt="platinum" title="�������" width="15" height="15">:</td><td>'.$platinum.' �����</td></tr>'; }
                                                 if ($gold)     { echo '<tr><td class="tdl">������ <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_gold.gif" alt="gold" title="������" width="15" height="15">:</td><td>'.$gold.' �����.</td></tr>'; }
              								    ?>
                                                </table>
              								</td>
              								<td>
              									<img alt="������ ������" height="16" onclick="changeLocation('<?= $cur_url ?>&menu=m_item&cmd_act=bayitem&bayitem=<?= $row['id'] ?>');" src="http://img.blutbad.ru/i/baccept.gif" title="������ ������" width="16">
              								</td>
              							</tr>
              						</tbody>
              					</table>
              				</td>
              				<td align="center">

              				</td>
              			</tr>
                      <? } ?>
              		</tbody>
              </table>

            </fieldset>
           <?
         break;
         case "m_rune":
           ?>
            <fieldset id="auth" style="width: 100%;">
              <legend>������� ���</legend>
              <?
                $ids = "";
                foreach ($ex_mineral_rune as $k_id => $v) {
                  if (empty($ids)) {
                     $ids = " `id` = '".$k_id."' ";
                  } else {
                     $ids .= " OR `id` = '".$k_id."' ";
                  }
                }

                if ($ids) { $ids = ' AND ('.$ids.')'; }

              	$data_bay_pick = sql_query("SELECT * FROM `shop` WHERE
                      `type`      = 25
                      ".$ids."
                  ORDER BY `nlevel`, `name` ASC;");

               # $data_sale_count = num_rows($data_bay_pick);
              ?>

              <table class="table-list shop-table buy-table" style="margin-top: 0em;">
              		<thead>
              			<tr>
              				<th>#</th>
              				<th>�������</th>
              				<th>�������</th>
              				<th></th>
              			</tr>
              		</thead>
              		<tbody>
                      <?
                       $i_it = 0;

                       while($row = fetch_array($data_bay_pick)) {
                         $row['cost'] = 0;

                         $i_it++;

                         if (!empty($ex_mineral_rune[$row['id']]['name'])) {
                          $row['name'] = $ex_mineral_rune[$row['id']]['name'];
                          $magicinf[$row['magic']]['time'] = $ex_mineral_rune[$row['id']]['time_end'];
                         }

                         $opisan = $ex_mineral_rune[$row['id']]['opisan'];
                         if ($opisan) {
                          $row['opisan'] = $opisan;
                         }
                      ?>
              			<tr>
              				<th><?= $i_it ?></th>
              				<td class="img" style="text-align: left;">
                                <table>
                                  <tr>
                                    <td style="border: 0px;width: 70px;text-align: center;">
                                      <img alt="<?= $row['name'] ?>" class="thumb <?= (fun_broken_item($row)?" broken":"") ?> button" data-type-num="<?= $row['type'] ?>" onclick="big(\'<?= convert_item_data($row['razdel'], 1) ?>\', <?= $row['num'] ?>)" src="http://img.blutbad.ru/i/<?= $row['img'] ?>" title="<?= $row['name'] ?>">
                                    </td>
                                    <td style="border: 0px;">
                                       <?= show_invisible($row, 0, false) ?>  <!--     padding-left: 45px; -->
                                    </td>
                                  </tr>
                                </table>
              				</td>
              				<td align="right" class="nowrap">
              					<table class="actions-table">
              						<tbody>

              							<tr>
              								<td>
                                                <table id="tb_mon">

              									<?
                                                 $din      = @$ex_mineral_rune[$row['id']]['din'];
                                                 $stl      = @$ex_mineral_rune[$row['id']]['stl'];

                                                 $coal     = @$ex_mineral_rune[$row['id']]['coal'];
                                                 $copper   = @$ex_mineral_rune[$row['id']]['copper'];
                                                 $silver   = @$ex_mineral_rune[$row['id']]['silver'];
                                                 $platinum = @$ex_mineral_rune[$row['id']]['platinum'];
                                                 $gold     = @$ex_mineral_rune[$row['id']]['gold'];

                                                 if ($din)      { echo '<tr><td class="tdl">�����:</td><td>'.$din.' �����.</td></tr>'; }
                                                 if ($stl)      { echo '<tr><td class="tdl">��������:</td><td>'.$stl.' ���.</td></tr>'; }

                                                if ($coal)     { echo '<tr><td class="tdl">����� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_coal.gif" alt="coal" title="�����" width="15" height="15">:</td><td>'.$coal.' �����.</td></tr>'; }
                                                 if ($copper)   { echo '<tr><td class="tdl">���� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_copper.gif" alt="copper" title="����" width="15" height="15">:</td><td>'.$copper.' �����.</td></tr>'; }
                                                 if ($silver)   { echo '<tr><td class="tdl">������� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_silver.gif" alt="silver" title="�������" width="15" height="15">:</td><td>'.$silver.' �����.</td></tr>'; }
                                                 if ($platinum) { echo '<tr><td class="tdl">������� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_platinum.gif" alt="platinum" title="�������" width="15" height="15">:</td><td>'.$platinum.' �����</td></tr>'; }
                                                 if ($gold)     { echo '<tr><td class="tdl">������ <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_gold.gif" alt="gold" title="������" width="15" height="15">:</td><td>'.$gold.' �����.</td></tr>'; }
              								    ?>
                                                </table>
              								</td>
              								<td>
              									<img alt="������ ������" height="16" onclick="changeLocation('<?= $cur_url ?>&menu=m_rune&cmd_act=bayrune&bayrune=<?= $row['id'] ?>');" src="http://img.blutbad.ru/i/baccept.gif" title="������ ������" width="16">
              								</td>
              							</tr>
              						</tbody>
              					</table>
              				</td>
              				<td align="center">

              				</td>
              			</tr>
                      <? } ?>
              		</tbody>
              </table>
            </fieldset>
           <?
         break;
         case "m_elic":
           ?>
            <fieldset id="auth" style="width: 100%;">
              <legend>������� ���������</legend>
               ��� ���������, ��������� �����
            </fieldset>
           <?
         break;
         case "m_baf":
           ?>
            <fieldset id="auth" style="width: 100%;">
              <legend>������� �����</legend>

              <table class="table-list shop-table buy-table" style="margin-top: 0em;">
              		<thead>
              			<tr>
              				<th>#</th>
              				<th>�������</th>
              				<th>�������</th>
              				<th></th>
              			</tr>
              		</thead>
              		<tbody>
                      <?
                       $i_it = 0;

                       foreach ($ex_mineral_baf as $k_id => $row) {
                         $row['id'] = $k_id;
                         $i_it++;

                      ?>
              			<tr>
              				<th><?= $i_it ?></th>
              				<td class="img" style="text-align: left;">
                                <table>
                                  <tr>
                                    <td style="border: 0px;width: 70px;text-align: center;">
                                      <img alt="<?= $row['name'] ?>" class="thumb <?= (fun_broken_item($row)?" broken":"") ?> button" src="http://img.blutbad.ru/i/<?= $row['img'] ?>" title="<?= $row['name'] ?>">
                                    </td>
                                    <td style="border: 0px;">
                                        <b><?= $row['name'] ?></b><br>
                                        �������: 1<br>
                                        <b>��������:</b><br>
                                        <?= (($k_id==1 || $k_id==2 || $k_id==3)?"����� ������":"����������������� ��������") ?>: <?= $s_time_format($row['time_end']) ?><br>
                                        <b>��������:</b><br>
                                        <?= $row['opisan'] ?>

                                    </td>
                                  </tr>
                                </table>
              				</td>
              				<td align="right" class="nowrap">
              					<table class="actions-table">
              						<tbody>

              							<tr>
              								<td>
                                                <table id="tb_mon" style="white-space: nowrap;">

              									<?
                                                 $din      = @$ex_mineral_baf[$row['id']]['din'];
                                                 $stl      = @$ex_mineral_baf[$row['id']]['stl'];

                                                 $coal     = @$ex_mineral_baf[$row['id']]['coal'];
                                                 $copper   = @$ex_mineral_baf[$row['id']]['copper'];
                                                 $silver   = @$ex_mineral_baf[$row['id']]['silver'];
                                                 $platinum = @$ex_mineral_baf[$row['id']]['platinum'];
                                                 $gold     = @$ex_mineral_baf[$row['id']]['gold'];

                                                 if ($din)      { echo '<tr><td class="tdl">�����:</td><td>'.$din.' �����.</td></tr>'; }
                                                 if ($stl)      { echo '<tr><td class="tdl">��������:</td><td>'.$stl.' ���.</td></tr>'; }

                                                 if ($coal)     { echo '<tr><td class="tdl">����� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_coal.gif" alt="coal" title="�����" width="15" height="15">:</td><td>'.$coal.' �����.</td></tr>'; }
                                                 if ($copper)   { echo '<tr><td class="tdl">���� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_copper.gif" alt="copper" title="����" width="15" height="15">:</td><td>'.$copper.' �����.</td></tr>'; }
                                                 if ($silver)   { echo '<tr><td class="tdl">������� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_silver.gif" alt="silver" title="�������" width="15" height="15">:</td><td>'.$silver.' �����.</td></tr>'; }
                                                 if ($platinum) { echo '<tr><td class="tdl">������� <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_platinum.gif" alt="platinum" title="�������" width="15" height="15">:</td><td>'.$platinum.' �����</td></tr>'; }
                                                 if ($gold)     { echo '<tr><td class="tdl">������ <img class="tooltip" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_gold.gif" alt="gold" title="������" width="15" height="15">:</td><td>'.$gold.' �����.</td></tr>'; }
              								    ?>
                                                </table>
              								</td>
              								<td>
              									<img alt="������ ������" height="16" onclick="changeLocation('<?= $cur_url ?>&menu=m_baf&cmd_act=baybaf&baybaf=<?= $row['id'] ?>');" src="http://img.blutbad.ru/i/baccept.gif" title="������ ������" width="16">
              								</td>
              							</tr>
              						</tbody>
              					</table>
              				</td>
              				<td align="center">

              				</td>
              			</tr>
                      <? } ?>
              		</tbody>
              </table>


            </fieldset>
           <?
         break;
       }
       ?>
      </div>
    </td>
    <td style="width: 100px;">
      <ul id="m_menu">
        <li <? if (@$_GET['menu'] == "m_money") { ?>class="m_li_sel"<? } ?> onclick="changeLocation('<?= $cur_url ?>&menu=m_money');">������</li>
        <li <? if (@$_GET['menu'] == "m_item") { ?>class="m_li_sel"<? } ?> onclick="changeLocation('<?= $cur_url ?>&menu=m_item');">��������</li>
        <li <? if (@$_GET['menu'] == "m_rune") { ?>class="m_li_sel"<? } ?> onclick="changeLocation('<?= $cur_url ?>&menu=m_rune');">����</li>
        <li <? if (@$_GET['menu'] == "m_elic") { ?>class="m_li_sel"<? } ?> onclick="changeLocation('<?= $cur_url ?>&menu=m_elic');">��������</li>
        <li <? if (@$_GET['menu'] == "m_baf") { ?>class="m_li_sel"<? } ?> onclick="changeLocation('<?= $cur_url ?>&menu=m_baf');">����</li>
      </ul>
    </td>
  </tr>
</table>