var token = null;

(function() {
	'use strict';

	var target = null;
	var tf = {};

	function template(name, data) {
		if (!tf[name]) tf[name] = doT.template($('#' + name).text());
		return tf[name](data);
	}

	function hashChange() {
		var action = location.hash.replace(/^#\!?/, '');
		if (!isToken(action)) hash('');
		else {
			viewToken(action);
		}
	}

	jQuery.fn.textAreaAdjust = function() {
		var o = this.get(0)
 		o.style.height = "1px";
	 	o.style.height = (o.scrollHeight)+"px";

	};

	function hash(uri) {
		history.pushState ? history.pushState(null, null, '#!' + uri) : location.hash = '#!' + uri;
	}

	function isToken(t) {
		return (/^\d{12}$/.test(t));
	}

	function init() {
		var index = location.href.indexOf('?');
		if (index != -1) {
			var href = location.href.substring(0, index) + location.hash;
			location.href = href;
		} else {
			hashChange();
		}
	}

	function numRows(selector) {
		var $obj = $(selector)
		return($obj.delay(100).outerHeight() / parseInt($obj.css("line-height")))
	}
	
	function viewToken(t) {
		$.getJSON('/j_object_view/' + t).done(function (j) {
			if (j.result) {
				updateUploadTarget(j.fs_id);
				token = j.token;
				hash(token);

				$("#obj_num").text(token).attr("data-clipboard-text", token);;
				if(j.post) {
					$(".obj_post").addClass("has_post");
					$('#post').attr("readonly", "readonly").val(j.post).show();
				}
				
				$('#upload_list').empty().append(uploadListBody(j.upload_list));
				$(".stage").hide();
				$('#object').fadeIn().attr("data-token", j.token);
				$("#post").textAreaAdjust();
				if(numRows("#post") > 10) {
					$(".obj_post").addClass("faded")
				}
			}
			else {
				hash('');
				notify("error", "Объекта с таким ключом не существует");
			}
		}).fail(function () {
		});
	}
		
	function updateUploadTarget(fs_id) {
		target = location.protocol + '//' + 'fs' + fs_id + '.' + location.host.replace(/^www\./, '') + '/upload/';
	}

	function uploadListBody(upload_list) {
		// Перед отстройкой шаблона модифицируем входящие данные 
		for(var i=0;i<upload_list.length;i++) {
			// Конвертируем размер
			upload_list[i].converted_size = convertSize(upload_list[i].size)
			// Конвертируем дату, если даты нет - значит файл только загружен, подставляем текущую
			upload_list[i].converted_date = upload_list[i].upload_time ? convertDate(upload_list[i].upload_time) : moment().format("LLL");
			// Разбиваем filename на имя и расширение
			var fnarr = upload_list[i].name.split(".")
			if (fnarr.length > 1) {
				upload_list[i].file_ext = "."+fnarr.pop();
				upload_list[i].file_title = fnarr.join(".");
			} else {
				upload_list[i].file_ext = "";
				upload_list[i].file_title = upload_list[i].name;
			}
		}
		var $list = $(template('t_upload_list', upload_list));
		$list.find('.upload_delete').click(function() {
			var upload_id = $(this).data('upload_id');
			$.getJSON('/j_upload_delete/' + token + '/' + upload_id).done(function(j) {
				if (j.result) $('.upload_row[data-upload_id="' + upload_id + '"]').remove();
			}).fail(function() {
			});
		});
		return $list;
	}
	function progressForFile(file) {
		if (!file.$upload_progress) file.$upload_progress = $('#upload_progress_' + file.uniqueIdentifier);
		return file.$upload_progress;
	}
	function progressBarForFile(file) {
		if (!file.$upload_progress_bar) file.$upload_progress_bar = $('#progress_bar_' + file.uniqueIdentifier);
		return file.$upload_progress_bar;
	}
	function topProgressBarForFile(file) {
		if (!file.$top_progress_bar) file.$top_progress_bar = $('#top_progress_' + file.uniqueIdentifier);
		return file.$top_progress_bar;
	}

	function convertSize(bytes) {
		var step = 1024
		if(bytes > Math.pow(step, 3)) {
			return (bytes / Math.pow(step, 3)).toFixed(2) + " GB"
		} else if (bytes > Math.pow(step, 2)) {
			return (bytes / Math.pow(step, 2)).toFixed(1) + " MB"
		} else if (bytes > step) {
			return (bytes / step).toFixed() + " KB"
		} else {
			return bytes + " bytes"
		}
		
	}

	function notify(type, text) {
		$("#note").removeAttr("data-style").attr("data-style", type).text(text);
		$(".note_wr").add("#note").fadeIn().delay(3000).fadeOut();
	}

	moment.locale('ru')

	new Clipboard('#obj_num')

	function convertDate(ut) {
		return moment.unix(ut).format('LLL');

	}

	function setTopProgressBar() {
		var totalSize = 0,
			totalShares = 0;
		$("#progress_top .item").each(function() {
			totalSize += parseInt($(this).attr("data-size"));
		})
		$("#progress_top .item").each(function() {
			if ($(this).index("#progress_top .item") != $("#progress_top .item").length - 1) {
				var share = parseFloat((parseInt($(this).attr("data-size")) / totalSize * 100).toFixed(2));
				$(this).attr("data-share", share)
				totalShares = parseFloat((totalShares + share).toFixed(2));
			} else {
				$(this).attr("data-share", 100 - totalShares)
			}
			$(this).css("width", $(this).attr("data-share")+"%")
		})

	}

	var flow = new Flow({
		target: function(file) {
			return target + file.token;
		},
		testChunks: false,
		maxChunkRetries: 0,
		chunkSize: 1099511627776,
		allowDuplicateUploads: true,
		simultaneousUploads: 1
	});
	flow.on('fileAdded', function(file, event) {
		file.token = token;
		return token;
	});
	flow.on('filesSubmitted', function(files, event) {
		$.each(files, function(n, el) {
			var i = 1;
			if(flow.files.indexOf(el) != -1) {
				i++;
				el.uniqueIdentifier = el.uniqueIdentifier+(Math.random()*1000000).toFixed();
			}
		})
		console.log("FLOWFILES", flow.files);
		$(template('top_progress_tpl', files)).appendTo("#progress_top");
		$("#progress_top").show();
		setTopProgressBar()
		var $list = $(template('t_upload_list_new', files));
		$list.find('.upload_cancel').click(function() {
			var uid = $(this).data('uid');
			for (var i = 0; i < flow.files.length; i++) {
				if (flow.files[i].uniqueIdentifier == uid) {
					topProgressBarForFile(flow.files[i]).parent().remove();
					setTopProgressBar();
					flow.files[i].cancel();
					break;
				}
			}
		});
		$('#upload_list').append($list);
		if (!flow.isUploading()) flow.upload();
	});
	flow.on('fileRemoved', function(file) {
		$('.upload_row_new[data-uid="' + file.uniqueIdentifier + '"]').remove();
	});
	flow.on('fileSuccess', function(file, message) {
		var j = JSON.parse(message);
		if (j.result) {
			$('#upload_list').find('.upload_row_new[data-uid="' + file.uniqueIdentifier + '"]').replaceWith(uploadListBody([j]));
		} else {
			progressForFile(file).text('failed to load');
		}
	});
	flow.on('fileError', function(file, message) {
		progressForFile(file).text('failed to load');
	});
	flow.on('fileProgress', function(file, chunk) {
		var percentage = (file.progress() * 100).toFixed(1) + '%';
		progressForFile(file).text(percentage);
		progressBarForFile(file).css("width", percentage);
		topProgressBarForFile(file).css("width", percentage);
	});
	flow.on('complete', function() {
		$("#progress_top").empty().hide();
	})
	flow.assignBrowse($('#upload'));
	flow.assignDrop($('#object'))

	$('#create').click(function() {
		$.getJSON('/j_object_create').done(function (j) {
			if (j.result) {
				updateUploadTarget(j.fs_id);
				token = j.token;
				hash(token);

				$('#obj_num').text(token).attr("data-clipboard-text", token);
				$('#post').val('').textAreaAdjust();
				$('#upload_list').empty();
				$(".stage").hide();
				$('#object').fadeIn().attr("data-token", j.token);
				$("#upload").click();
			}
			else {
				alert('Failed to create new token!');
			}
		}).fail(function () {
		});
	});
	$('#get_form').submit(function() {
		var t = $('#token_input').val();
		if (isToken(t)) viewToken(t);
		else notify("error", "Ключ должен состоять из 12 цифр.");
		return false;
	});
	$('#close').click(function() {
		token = null;
		hash('');

		$('#token_input').val('');
		$('#object').hide();
		$('#token').empty();
		$('#post').val("");
		$('#upload_list').empty();
	});
	$('#delete').click(function() {
		$.getJSON('/j_object_delete/' + token).done(function (j) {
			if (j.result) {
				token = null;
				hash('');

				$('#token_input').val('');
				$('#object').hide();
				$('#token').empty();
				$('#post').val("");
				$('#upload_list').empty();
			}
			else {
				notify("error", 'Объекта с таким ключом не существует');
			}
		}).fail(function () {
		});
	});

	$("#obj_num").click(function() {
		notify("info", "Ключ скопирован")
	})

	$("#post").on("keyup", function() {
		$(this).textAreaAdjust();
	})

	$(".obj_post").click(function() {
		$(this).animate({"height": $("#post").height()+88+"px"}, function() {
			$(".hider").hide();
			$(".obj_post").removeClass("faded").css("height", "auto");
		}) // 88 - прибавка к высоте, необхходимая для симметричного отображения кнопок, выведена эмпирически
	})

	$("#create_post, #edit_post").click(function() {
		$("#post").show().removeAttr("readonly").textAreaAdjust();
		$(".obj_post").addClass("has_post editing");
		$("#post").focus();
	})

	$("#save_post").click(function() {
		var token = $("#object").attr("data-token"),
		content = $("#post").val();
		$.ajax("/j_object_update/"+token, {
			type: "POST",
			dataType: "JSON",
			data: {
				post: content
			}, success: function(res) {
				if (res.result == 1) {
					$("#post").attr("readonly", "readonly");
					$(".obj_post").removeClass("editing");
					notify("info", "Описание сохранено");
				} else {
					notify("error", "Ошибка при сохранении описания");
				}
			}
		})
	})

	$("#delete_post").click(function() {
		var token = $("#object").attr("data-token"),
		content = $("#post").val();
		$.ajax("/j_object_update/"+token, {
			type: "POST",
			dataType: "JSON",
			data: {
				post: ""
			}, success: function(res) {
				if (res.result == 1) {
					$("#post").val("").attr("readonly", "readonly").hide();
					$(".obj_post").removeClass("editing has_post");
					notify("info", "Описание удалено");
				} else {
					notify("error", "Ошибка при удалении описания");
				}
			}
		})
	})

	$(document).on("click", ".poster", function() {
		$("#preview .content").empty();
		var $self = $(this);
		switch($self.attr("data-type")) {
			case "video":
				var video = document.createElement('video');
				video.src = $self.attr("data-href");
				video.autoPlay = true;
				video.controls = true;
				video.id = 'player';
				video.style["width"] = "100%"
				$(video).appendTo("#preview .content");
				video.play();
				$("#preview_fn").text($self.attr("data-name"));
				$("#preview").attr("data-upload_id", $self.closest(".upload_row").attr("data-upload_id"));
				$("#preview .content").attr("data-type", $self.attr("data-type"));
				$("#preview").show();
				$("#preview").find(".top").show().delay(3000).fadeOut();
				break;
			case "image":
				var img = document.createElement('img')
				img.src = $self.attr("data-href");
				img.id = "shower";
				// img.style["width"] = "100%";
				$(img).appendTo("#preview .content");
				$("#preview_fn").text($self.attr("data-name"));
				$("#preview").attr("data-upload_id", $self.closest(".upload_row").attr("data-upload_id"));
				$("#preview .content").attr("data-type", $self.attr("data-type"));
				$("#preview").show();
				$("#preview").find(".top").show().delay(3000).fadeOut();
				break;
			default:
				notify("error", "Файл данного типа не может быть отображен")
		}
	})

	$("#preview_close").click(function() {
		if($("#player")[0]) {
			$("#player")[0].pause();
		}
		$("#preview .content").empty();
		$("#preview").hide();
	})

	$("#draggable_wr").draggable();
	$("#preview").resizable({
		resize: function(event, ui) {
			var showing = $("#preview>.content>*");
		}
	});

	$("#preview").mousemove(function() {
		$(this).find(".top").show();
	})

	$("#preview").mouseleave(function() {
		$(this).find(".top").delay(3000).fadeOut();
	})

	$(document).on("click", ".filename", function() {
		$(this).parent().find(".poster").click();
	})

	$(document).on("click", "#preview .content", function() {
		if($(this).attr("data-type") == "image") {
			var index = $(".upload_row[data-upload_id="+$("#preview").attr("data-upload_id")+"]").index(".upload_row");
			if(index == $(".upload_row").length - 1) {
				index = 0;
			} else {
				index++;
			}
			var foundImg = false;
			while(foundImg == false) {
				if($($(".upload_row").get(index)).attr("data-type") == "image") {
					foundImg = true;
				} else {
					if(index == $(".upload_row").length - 1) {
						index = 0;
					} else {
						index++;
					}	
				}
				
			}
			$($(".upload_row").get(index)).find(".poster").click();
		}
	})

	$(".get").click(function() {
		if(!$(".get .off").hasClass("animated")) {
			$(".get .off").addClass("animated");
		}
	})

	$(".get .off").click(function() {
		$(this).parent().addClass("on");
		$("#token_input").focus();
	})
	$(".get .on .get_close").click(function() {
		$(this).closest(".get").removeClass("on");
		$("#token_input").val("");
	})

	function loadJs(src) {
		var a = document.createElement('script');
		a.setAttribute('src', src);
		a.async = 'true';
		var b = document.getElementsByTagName('script')[0];
		b.parentNode.insertBefore(a, b);
	}

	function countHit() {
		if (!document.cookie) document.cookie = "b=b";

		loadJs('http://c.hit.ua/hit?i=7' +
			'&g=0&x=3&s=1' +
			'&t=' + (new Date()).getTimezoneOffset() +
			(document.cookie ? '&c=1' : '') +
			(self != top ? '&f=1' : '') +
			(navigator.javaEnabled() ? '&j=1' : '') +
			(typeof(screen) != 'undefined' ? '&w=' + screen.width + '&h=' + screen.height + '&d=' + (screen.colorDepth ? screen.colorDepth : screen.pixelDepth) : '') +
			'&r=' + escape(document.referrer) +
			'&u=' + escape(window.location.href) +
			'&' + Math.random()
		);
	}

	$(window).on('hashchange', hashChange);
	$(window).on('popstate', hashChange);
	init();

}());
