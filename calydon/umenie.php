<?
   include("a_process_start.php");
   session_start();
   if ($_SESSION['uid'] == null) header("Location: index.php");
   include "connect.php";
   include "functions.php";
 // if ($user_my->user['battle'] != 0) { header('location: fbattle.php'); die(); }
    
/***------------------------------------------
 * �������� � �������� ���� ����������
 **/

if ((@$_POST['cmd'] == 'open_ach_group' || @$_POST['cmd'] == 'close_ach_group') && @$_POST['group'] && @$_POST['is_ajax'] && @$_POST['state']) {
 $user_achievements = unserialize($user_my->user['achievements']);

 $group = @$_POST['group'];
 $state = @$_POST['state'];

 switch (@$_POST['cmd']) {
   case 'open_ach_group':  $user_achievements[$group][$state] = 1; break;
   case 'close_ach_group': $user_achievements[$group][$state] = 0; break;
 }

 sql_query("UPDATE `users` SET `achievements` = '".serialize($user_achievements)."' WHERE `id` = '".$user_my->user['id']."' LIMIT 1;");
 die("OK");
}

/***------------------------------------------
 * ��������� ���������
 **/

 $relationship = array(
   400 => '������������',
   100 => '������������',
   50  => '�����������',
   0   => '�����������',
 );

/***------------------------------------------
 * ����� ��������
 **/

$user_collection = unserialize($user['collection']);

$collection_data = array(

 # �������� ��������� �������� ����� �������� �� �������� ��������.��� � ��������� ������������ �������� �� �������.
   'fish'            => array('id_name' => 'fishing', 'name' => '��������� ��������',                     'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('�������� ������', '������� ������', '��������', '�����', '���������', '����������� �������', '�������� �������'), 'item_fin' => '���������� +5%'),
 # �������� ��������� ���������� ����� �������� �� �������� ����������. � ��������� ������������ �������� �� ����� ��� ���������� ������ ���������� ������
   'rog'             => array('id_name' => 'rogue', 'name' => '��������� ����������',                     'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('������', '������ �������', '������ �����', '�����', '������ �������', '����-���������', '�����'), 'item_fin' => '�������� +5%'),
 # �������� ��������� ������� � �������� ������� � ��������� ������������ ����� ������� �� ������ ������� ������� � ����������� (����, ������, �������, ���� ��������, ��������� �� �����, ��������)
   'monstr'          => array('id_name' => 'monsters', 'name' => '��������� �������',                     'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('������ �����', '����� �������', '����������� ������', '������ ����', '������ �����', '������ ����������', '�������'), 'item_fin' => '���� +5%'),
 # �������� ���������� ���������, � ��� �� ��������� ���, ����, ���� � ����� ����� ������� � ������� ������ ���������.
   'lot'             => array('id_name' => 'lottery', 'name' => '���������� ���������',                   'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('������� ���������', '������� ���������', '������� ������', '������� �����', '������� ��������', '������� ����������', '������� ��������'), 'item_fin' => '���� +5%'),
   'pik'             => array('id_name' => 'pik', 'name' => '��������� ���',                              'items_idname' => array('9', '10', 'valet', 'dama', 'king', 'tuz', 'joker'), 'items_name' => array('������� ���', '��c���� ���', '����� ���', '���� ���', '������ ���', '��� ���', '������ ���'), 'item_fin' => '����� �������� ��� ������ +2%'),
   'tref'            => array('id_name' => 'tref', 'name' => '��������� ����',                            'items_idname' => array('9', '10', 'valet', 'dama', 'king', 'tuz', 'joker'), 'items_name' => array('������� ����', '��c���� ����', '����� ����', '���� ����', '������ ����', '��� ����', '������ ����'), 'item_fin' => '����� �������� ���� ������������ +2%'),
   'cherv'           => array('id_name' => 'cherv', 'name' => '��������� ����',                           'items_idname' => array('9', '10', 'valet', 'dama', 'king', 'tuz', 'joker'), 'items_name' => array('������� ����', '��c���� ����', '����� ����', '���� ����', '������ ����', '��� ����', '������ ����'), 'item_fin' => '����� �������� ���� ���� +2%'),
   'bubn'            => array('id_name' => 'bubn', 'name' => '��������� �����',                           'items_idname' => array('9', '10', 'valet', 'dama', 'king', 'tuz', 'joker'), 'items_name' => array('������� �����', '��c���� �����', '����� �����', '���� �����', '������ �����', '��� �����', '������ �����'), 'item_fin' => '����� �������� ����� �������� +2%'),
 # �������� ���������� ��������� ����� �������� � ������� �� ������� ������ � ����� ������������ ������ ����. ������ ���������, � ������� �� �������� �������, ������� ����� �������� �� ����, ���� ��������.
   'ny2011_clown'    => array('id_name' => 'ny2011_clown', 'name' => '������� �����',                     'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('�������� ������', '����� � ���������', '����� ������', '����', '������� ������', '������� ���', '������� ������'), 'item_fin' => '������� �����'),
   'ny2011_elf'      => array('id_name' => 'ny2011_elf', 'name' => '������� ����',                        'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('������� �����', '���������� �������', '������� �����', '������ �����', '��������� �������', '�������� �����', '������� ������'), 'item_fin' => '������� ����'),              # �� 10 ���� http://enc.carnage.ru/present/167.html
   'ny2011_ded'      => array('id_name' => 'ny2011_ded', 'name' => '������� �����',                       'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('����� � ���������', '����� ���� ������', '���� ���� ������', '����� ���� ������', '���� ���� ������', '�������', '��������  ���� ������'), 'item_fin' => '������� �����'),
 # �������� �� ��������� ���������� �� �������� ��� �������� ������ ����.
   'priest'          => array('id_name' => 'priest', 'name' => '��������� ����������',                    'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('����', '�����', '����������� ������', '�����������', '�����', '������������� � �����', '��������'), 'item_fin' => '������������ ������'),
 # �������� �� ��������� �������� �� �������� ��� �������� ������ �������.
   'trader'          => array('id_name' => 'trader', 'name' => '��������� ��������',                      'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('������ ��������', '���������� ����', '�������������', '������ �������', '�����', '�������� ��������', '������ ����'), 'item_fin' => '������������ �����'),
 # ����� � ���������� ��������� ���������� � 17 ���� �� 9 ������� 2016 ���� ����� ������. �� �������� ��� �� ����� ������ �� �����.
   'miner'           => array('id_name' => 'miner', 'name' => '��������� ��������',                       'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('������', '�����', '������', '������', '�����', '���������', '����'), 'item_fin' => '��������: 1, ����: 5% ����������� ���� ����� ������� ����� � ����� ����������.'),
 # ����� � ���������� ��������� ���������� � 9 �� 31 ������� 2016 ���� ����� ������. �� �������� ��� �� ����� ������ �� �����.
   'smith'           => array('id_name' => 'smith', 'name' => '��������� �������',                        'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('������', '�����', '�������', '����', '�����', '������� �����', '����������'), 'item_fin' => '+0% ����������� ���� ��������� ������������� ���������'),
 # �������� ��������� ����� ����� ������� �� �������� �����. �������� � ���� � ������� � ���������� �����
   'clan'            => array('id_name' => 'clan', 'name' => '��������� �����',                           'items_idname' => array('9', '10', 'valet', 'dama', 'king', 'tuz', 'joker'), 'items_name' => array('�����', '��������', '����', '���', '��������', '�������', '�������� �� ��������'), 'item_fin' => '�������� +10%'),
 # �������� ��������� ������� ����� �������� �� ����� ��������� ����������� ������� � 23 �������. ����� �� ����� � ������� � ���������� �������.
   'zar_collection'  => array('id_name' => 'zar_collection', 'name' => '��������� �������',               'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('�����', '�����', '����', '���������', '���������', '�������', '�������'), 'item_fin' => '������� ����� -10%'),
 # �������� ��������� �������� �� ��������� ����� �������� ����� ������ �� ������ �����-������������� (�� 17 ���� 2016 ����) ��� ������ �� �����.
   'monster_hunter'  => array('id_name' => 'monster_hunter', 'name' => '��������� �������� �� ���������', 'items_idname' => array('1', '2', '3', '4', '5', '6', '7'), 'items_name' => array('�����', '������� ���', '������� ������', '����', '����� ������', '������', '������'), 'item_fin' => '10 ���. +20% ����������� ���� ������� �� �������� � �����������.'),

);

/***------------------------------------------
 * �������� ��������
 **/

 /* $user_collection['rog']['c'][0] = 1;
  $user_collection['rog']['c'][1] = 2;
  $user_collection['rog']['c'][2] = 2;
  $user_collection['rog']['c'][3] = 2;
  $user_collection['rog']['c'][4] = 2;
  $user_collection['rog']['c'][5] = 2;
  $user_collection['rog']['c'][6] = 2;

  $user_collection['monstr']['fin'] = false;

  bprint($user_collection);
*/

if ($_GET['cmd'] == 'collect_collection') {
 $code = $_GET['code'];

 if (empty($code)) {
   $_SESSION['error_content'] = '��� ���� ��������';
 } else if(empty($user_collection[$code])) {
   $_SESSION['error_content'] = '�� ������� ���������';
 } else if(count($user_collection[$code]['c']) < 7) {
   $_SESSION['error_content'] = '�� ������� ���������';
 } else if(isset($user_collection[$code]['fin'])) {
   $_SESSION['error_content'] = '��� �������';
 } else {
     $user_collection[$code]['fin'] = true;
     sql_query("UPDATE `users` SET `collection` = '".serialize($user_collection)."' WHERE `id` = '".$user['id']."' LIMIT 1;");

     $_SESSION['error_content'] = '��������� �������';
 }

 $_GET['cmd'] = 'collections.show';
}

?>
<html xmlns="http://www.w3.org/1999/xhtml" class="js win chrome chrome27 webkit webkit537"><head>
	<title>������</title>

	<meta http-equiv="content-type" content="text/html; charset=windows-1251">

	<link href="/css/main.css" rel="stylesheet" type="text/css">
    <link href="/c/stats.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui-1.8.17.custom.css" type="text/css" media="screen" charset="utf-8"/>

	<script src="/j/jquery/jquery.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery-ui.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.browser.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.cookie.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.tooltip.mod.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.pnotify.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.placeholder.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.countdown.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.jeegoocontext.mod.js" type="text/javascript"></script>

    <script type="text/javascript">
  		var isBattle = Number(<?= $user_my->user['battle'] ?>);
  		var isBid = Number(<?= $user_my->user['zayavka'] ?>);
    </script>

	<script src="/js/iner.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.charcounter.mod.js" type="text/javascript"></script>
	<script src="/j/shortkeys.js" type="text/javascript"></script>
	<script src="/j/user_v2.js" type="text/javascript"></script>
    <script src="/j/stats.js" type="text/javascript"></script>

    <style type="text/css">
      .table-list .name {width: 150px;}
      .table-list .value {width: 75px;}
      .table-list .time {width: 150px;}
      .table-list .source {width: 190px;}
      .table-list tbody .name {white-space: nowrap;}
      .table-list .title {width: 150px;	text-align: center;}
      .prset-highlight{background-color: #FFF0AA;}
      .pagerset-highlight{ background-color: #FFF0AA;}
    </style>

      <? if ($user_my->user['level']==0) { ?>
      <script type="text/javascript">
      		//<![CDATA[
      		$(function() {
                  <? if ($is_usersquests_umenie) { ?>
          			top.highlightAllTutorialElements("umenie");
                  <? } ?>
      		});
      		//]]>
      </script>
      <? } ?>

</head>
<body class="main-content">
	<div id="hint1" class="hint"></div>

	<table class="main-table">
		<tbody><tr>
			<td>

<?
if (!empty($_SESSION['error_content'])) {
?>
<div class="top-error">
		<fieldset>
			<legend>��������!</legend>
			<div class="error-content"><?= $_SESSION['error_content'] ?></div>
		</fieldset>
</div>
<? $_SESSION['error_content']='';} ?>
<div id="tabs">
     <div id="tab" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
        <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
          <li class="ui-state-default ui-corner-top <? if ((@$_GET['cmd']=='status.show') or (empty($_GET['cmd']))) { ?>ui-tabs-selected ui-state-active<? } ?>"><a href="?cmd=status.show">���������</a></li>
          <li class="ui-state-default ui-corner-top <? if ((@$_GET['cmd']=='rep.show')) { ?>ui-tabs-selected ui-state-active<? } ?>"><a href="?cmd=rep.show">���������</a></li>
          <li class="ui-state-default ui-corner-top <? if ((@$_GET['cmd']=='skill.show')) { ?>ui-tabs-selected ui-state-active<? } ?>"><a href="?cmd=skill.show">������</a></li>
          <li class="ui-state-default ui-corner-top <? if ((@$_GET['cmd']=='achievement.show')) { ?>ui-tabs-selected ui-state-active<? } ?>"><a href="?cmd=achievement.show">����������</a></li>
          <li class="ui-state-default ui-corner-top <? if ((@$_GET['cmd']=='collections.show')) { ?>ui-tabs-selected ui-state-active<? } ?>"><a href="?cmd=collections.show">���������</a></li>
        </ul>

          <div class="ui-block ui-tabs-panel ui-widget-content ui-corner-bottom">

            <? if ((@$_GET['cmd']=='status.show') or (empty($_GET['cmd']))) { ?>
            <style type="text/css">.table-list { background-color: #DEE2E7; border: 1px solid #808080; width: 100%; } .table-list th, .table-list td {border: 1px solid #808080;padding: .3em .5em;}</style>

            <table width="100%">
            	<tr>
            		<td width="50%" valign="top">

                        <?

                       # ��� �����
                         $my_effects = array();
                       # �������� �����
                         include($_SERVER["DOCUMENT_ROOT"]."/functions/classes/user_control/pas_effects.php");

                       # �������� ���� ������
                         $all_effects = array(
                          'eff' => array(
                                         21 => array('type' => 21, 'name' => '������'),
                                         1043 => array('type' => 1043, 'name' => '��� ��������� ������'),
                                         0 => array('type' => 0, 'name' => ''),
                                        ),

                          'baf' => array(
                                         1090 => array('type' => 1090, 'name' => '���������� ��������'),
                                         1091 => array('type' => 1091, 'name' => '��������� � ��������� � ���� "�������"'),
                                         1093 => array('type' => 1093, 'name' => '������������� ���������'),
                                         1094 => array('type' => 1094, 'name' => '������������ �������'),
                                         1095 => array('type' => 1095, 'name' => '��������� �����'),
                                         1096 => array('type' => 1096, 'name' => '����������������� ������'),
                                         1097 => array('type' => 1097, 'name' => '�������������� �����'),
                                         1099 => array('type' => 1099, 'name' => '����������� ���������'),
                                         1101 => array('type' => 1101, 'name' => '������� �� �������'),
                                         1102 => array('type' => 1102, 'name' => '���������� �����'),
                                         1103 => array('type' => 1103, 'name' => '���'),
                                         1022 => array('type' => 1022, 'name' => '�����������'),
                                         1023 => array('type' => 1023, 'name' => '������ �� ��������'),
                                         1024 => array('type' => 1024, 'name' => '������ �� ������ ��������'),
                                         1038 => array('type' => 1038, 'name' => '�������� ����'),
                                         1039 => array('type' => 1039, 'name' => '�������������� % � �����'),
                                         1040 => array('type' => 1040, 'name' => '������� ����� ���� ������ ��� ����������'),
                                         1042 => array('type' => 1042, 'name' => '�������� ������������� ������ � ������������'),
                                         1089 => array('type' => 1089, 'name' => '�������� ������������'),
                                         1031 => array('type' => 1031, 'name' => '��������� ���������������'),
                                         1032 => array('type' => 1032, 'name' => '���������� ������'),
                                         1033 => array('type' => 1033, 'name' => '�����'),

                                        ),

                          'debaf' => array(
                                           12   => array('type'  => 12, 'name' => '������'),
                                           1026 => array('type'  => 1026, 'name' => '������'),
                                           1034 => array('type'  => 1034, 'name' => '���� ��������'),
                                           0    =>  array('type' => 0, 'name' => '����������'),

                                          ),

                          'delay' => array(
                                            1 => array('type' => 1, 'name' => '�������� ����� � ����'),
                                            2 => array('type' => 2, 'name' => '��������'),
                                            4 => array('type' => 4, 'name' => '�������'),
                                            5 => array('type' => 5, 'name' => '������� ����������'),
                                            10 => array('type' => 10, 'name' => '����'),
                                            20 => array('type' => 20, 'name' => '�������� �� �������'),
                                            1098 => array('type' => 1098, 'name' => '�������� � ������ �����'),
                                            1025 => array('type' => 1025, 'name' => '�������� �� ����� � ����'),
                                            1027 => array('type' => 1027, 'name' => '����� "��������� ������"'),
                                            1028 => array('type' => 1028, 'name' => '����� "����� ������"'),
                                            1029 => array('type' => 1029, 'name' => '����� � "�����������"'),
                                            1030 => array('type' => 1030, 'name' => '����� � "��������"'),
                                            1036 => array('type' => 1036, 'name' => '����� � "���������"'),
                                            1037 => array('type' => 1037, 'name' => '�������� �� ��� � ������'),
                                            1041 => array('type' => 1041, 'name' => '��������� ���������'),
                                            1044 => array('type' => 1044, 'name' => '������ �����������'),
                                            1092 => array('type' => 1092, 'name' => '����� �����'),
                                            1100 => array('type' => 1100, 'name' => '������� � �����'),
                                            0 => array('type' => 0, 'name' => ''),
                                          ),

                         );

                         $cur_eff_all = ''; # ������ ������� �������

                         $eff_all = sql_query("SELECT * FROM `effects` WHERE `owner` = '".$user_my->user_id."' ORDER BY `type` DESC;");
                         while ($row = fetch_array($eff_all)) {
                          $cur_eff_all .= $row['type'].';';
                          $m_eff = array('name' => $row['name'], 'type' => $row['type'], 'total' => $row['total'], 'time' => $row['time'], 'opisan' => $row['opisan'], 'stage' => $row['stage'], 'from_whom' => $row['from_whom']);

                          if (!empty($all_effects['eff'][$m_eff['type']])) {
                            $my_effects['eff'][] = $m_eff;
                          }
                          if (!empty($all_effects['baf'][$m_eff['type']])) {
                            $my_effects['baf'][] = $m_eff;
                          }
                          if (!empty($all_effects['debaf'][$m_eff['type']])) {
                            $my_effects['debaf'][] = $m_eff;
                          }
                          if (!empty($all_effects['delay'][$m_eff['type']])) {
                            $my_effects['delay'][] = $m_eff;
                          }
                         }

                       # ���������� ������� ���� ��� ����� ����
                         if ($user_my->user['eff_all'] != $cur_eff_all) {
                           $user_my->upd_sql .= ", `eff_all` = '".$cur_eff_all."'";
                         } elseif($user_my->user['invis'] && !substr_count($user_my->user['eff_all'], '1022;')) {
                           $user_my->upd_sql .= ", `invis` = 0";
                           XMPP::changeInvisible(CHAT::chat_format_login($user_my->user['login']), 0);
                           CHAT::chat_command($user_my->user, "params|isInvisible=0", array('addXMPP' => true));
                           $user_my->user['invis'] = 0;
                         }

                         /*echo "<pre>";
                         print_r($my_effects);
                         echo "</pre>";*/
                         if (empty($my_effects)) {
                          echo "� ��� ��� �������";
                         }
                        ?>

                        <? if (!empty($my_effects['eff'])) { ?>
                          <h3 style=" padding-bottom: 5px; font-weight: bold;font-size: 14px;">�������: (<?= count($my_effects['eff']) ?>)</h3>
                          <table class="table-list">
                          <thead>
                        		<tr>
                        			<th class="name">��������</th>
                        			<th class="value">��������</th>
                        			<th class="time">����� ��������</th>
                        			<th class="description">��������</th>
                        			<th class="source">��������</th>
                        		</tr>
                         </thead>
                          <?
                           foreach ($my_effects['eff'] as $row) {
                           # ��������� ����������� �����
                             if ($row['type'] == 1039) $row['total'] = "+".$row['total']."%";
                             if ($row['type'] == 1042) $row['total'] = str_replace('%', '', $row['total'])."%";
                             if ($row['type'] == 1089) $row['total'] = "+".$row['total']."%";
                             if ($row['type'] == 1026) $row['total'] = "(����: ".$row['total']."%)";

                                ?>
                                  <tr>
                                    <td class="name"><b><?= $row['name'] ?></b></td>
                                    <td style="text-align: center"><?= $row['total'] ?></td>
                                    <td style="text-align: center" class="time"><? if ($row['time']=='���������') { ?>���������<? } else { ?><span class="elapsed-time nowrap invisible"><?= ($row['time'] - time()) ?></span><? } ?></td>
                                    <td><?= $row['opisan'] ?></td>
                                 <? if ($row['stage']) { ?>
                                    <td><?= $row['from_whom'] ?> ������� [<?= $row['stage'] ?>]</td>
                                 <? } else { ?>
                                    <td><?= $row['from_whom'] ?></td>
                                 <? } ?>
                                  </tr>
                                <?
                          }
                          ?>
                          </table>
                        <? } ?>
                        <? if (!empty($my_effects['baf'])) { ?>
                          <h3 style=" padding-bottom: 5px; font-weight: bold;font-size: 14px;">����: (<?= count($my_effects['baf']) ?>)</h3>
                          <table class="table-list">
                          <thead>
                        		<tr>
                        			<th class="name">��������</th>
                        			<th class="value">��������</th>
                        			<th class="time">����� ��������</th>
                        			<th class="description">��������</th>
                        			<th class="source">��������</th>
                        		</tr>
                         </thead>
                          <?
                           foreach ($my_effects['baf'] as $row) {
                           # ��������� ����������� �����
                             if ($row['type'] == 1039) $row['total'] = "+".$row['total']."%";
                             if ($row['type'] == 1042) $row['total'] = str_replace('%', '', $row['total'])."%";
                             if ($row['type'] == 1089) $row['total'] = "+".$row['total']."%";
                             if ($row['type'] == 1026) $row['total'] = "(����: ".$row['total']."%)";

                                ?>
                                  <tr>
                                    <td class="name"><b><?= $row['name'] ?></b></td>
                                    <td style="text-align: center"><?= $row['total'] ?></td>
                                    <td style="text-align: center" class="time"><? if ($row['time']=='���������') { ?>���������<? } else { ?><span class="elapsed-time nowrap invisible"><?= ($row['time'] - time()) ?></span><? } ?></td>
                                    <td><?= $row['opisan'] ?></td>
                                 <? if ($row['stage']) { ?>
                                    <td><?= $row['from_whom'] ?> ������� [<?= $row['stage'] ?>]</td>
                                 <? } else { ?>
                                    <td><?= $row['from_whom'] ?></td>
                                 <? } ?>
                                  </tr>
                                <?
                          }
                          ?>
                          </table>
                        <? } ?>
                        <? if (!empty($my_effects['debaf'])) { ?>
                          <h3 style=" padding-bottom: 5px; font-weight: bold;font-size: 14px;">������: (<?= count($my_effects['debaf']) ?>)</h3>
                          <table class="table-list">
                          <thead>
                        		<tr>
                        			<th class="name">��������</th>
                        			<th class="value">��������</th>
                        			<th class="time">����� ��������</th>
                        			<th class="description">��������</th>
                        			<th class="source">��������</th>
                        		</tr>
                         </thead>
                          <?
                           foreach ($my_effects['debaf'] as $row) {
                           # ��������� ����������� �����
                             if ($row['type'] == 1039) $row['total'] = "+".$row['total']."%";
                             if ($row['type'] == 1042) $row['total'] = str_replace('%', '', $row['total'])."%";
                             if ($row['type'] == 1089) $row['total'] = "+".$row['total']."%";
                             if ($row['type'] == 1026) $row['total'] = "(����: ".$row['total']."%)";

                                ?>
                                  <tr>
                                    <td class="name"><b><?= $row['name'] ?></b></td>
                                    <td style="text-align: center"><?= $row['total'] ?></td>
                                    <td style="text-align: center" class="time"><? if ($row['time']=='���������') { ?>���������<? } else { ?><span class="elapsed-time nowrap invisible"><?= ($row['time'] - time()) ?></span><? } ?></td>
                                    <td><?= $row['opisan'] ?></td>
                                 <? if ($row['stage']) { ?>
                                    <td><?= $row['from_whom'] ?> ������� [<?= $row['stage'] ?>]</td>
                                 <? } else { ?>
                                    <td><?= $row['from_whom'] ?></td>
                                 <? } ?>
                                  </tr>
                                <?
                          }
                          ?>
                          </table>
                        <? } ?>
                        <? if (!empty($my_effects['delay'])) { ?>
                          <h3 style=" padding-bottom: 5px; font-weight: bold;font-size: 14px;">��������: (<?= count($my_effects['delay']) ?>)</h3>
                          <table class="table-list">
                          <thead>
                        		<tr>
                        			<th class="name">��������</th>
                        			<th class="value">��������</th>
                        			<th class="time">����� ��������</th>
                        			<th class="description">��������</th>
                        			<th class="source">��������</th>
                        		</tr>
                         </thead>
                          <?
                           foreach ($my_effects['delay'] as $row) {
                           # ��������� ����������� �����
                             if ($row['type'] == 1039) $row['total'] = "+".$row['total']."%";
                             if ($row['type'] == 1042) $row['total'] = str_replace('%', '', $row['total'])."%";
                             if ($row['type'] == 1089) $row['total'] = "+".$row['total'];
                             if ($row['type'] == 1026) $row['total'] = "(����: ".$row['total']."%)";

                                ?>
                                  <tr>
                                    <td class="name"><b><?= $row['name'] ?></b></td>
                                    <td style="text-align: center"><?= $row['total'] ?></td>
                                    <td style="text-align: center" class="time"><? if ($row['time']=='���������') { ?>���������<? } else { ?><span class="elapsed-time nowrap invisible"><?= ($row['time'] - time()) ?></span><? } ?></td>
                                    <td><?= $row['opisan'] ?></td>
                                 <? if ($row['stage']) { ?>
                                    <td><?= $row['from_whom'] ?> ������� [<?= $row['stage'] ?>]</td>
                                 <? } else { ?>
                                    <td><?= $row['from_whom'] ?></td>
                                 <? } ?>
                                  </tr>
                                <?
                          }
                          ?>
                          </table>
                        <? } ?>
            		</td>
            	</tr>
            </table>

            <? } elseif (($_GET['cmd']=='rep.show')) { ?>

            <?

             $rep = unserialize($user_my->user['rep']);
             /*
               residents_city_damask  =  ������ ������ ������
               residents_city_alamut  =  ������ ������ ������
               fishing_guild          =  ������� ���������
               beggars                =  ����������
               order_dungeon_keeper   =  ����� ���������� ����������
              */
            ?>

             <table class="table-list" id="reputation">
            		<thead>
            			<tr>
            				<th class="color">#</th>
            				<th>�������</th>
            				<th>���������</th>
            				<th>���������</th>
            	            <th>���� ����������</th>
            			</tr>
            		</thead>
            		<tbody>
            			<tr>
            				<td style="background-color: #ffdd00"></td>
            				<td>������ ������ ������</td>
            				<td>�����������</td>
            				<td>
            					<?= (!empty($rep['residents_city_damask'])?"+".$rep['residents_city_damask']:"0") ?>
            				</td>
            	            <td>0</td>
            			</tr>
            			<tr>
            				<td style="background-color: #ffdd00"></td>
            				<td>������ ������ ������</td>
            				<td>�����������</td>
            				<td>
            					<?= (!empty($rep['residents_city_alamut'])?"+".$rep['residents_city_alamut']:"0") ?>
            				</td>
            	            <td>0</td>
            			</tr>
            			<tr>
            				<td style="background-color: #ffdd00"></td>
            				<td>������� ���������</td>
            				<td>�����������</td>
            				<td>
            					<?= (!empty($rep['fishing_guild'])?"+".$rep['fishing_guild']:"0") ?>
            				</td>
            	            <td>0</td>
            			</tr>
            			<tr>
            				<td style="background-color: #ffdd00"></td>
            				<td>����������</td>
            				<td>�����������</td>
            				<td>
            					<?= (!empty($rep['beggars'])?"+".$rep['beggars']:"0") ?>
            				</td>
            	            <td>0</td>
            			</tr>
            			<tr>
            				<td style="background-color: #ffdd00"></td>
            				<td>����� ���������� ����������</td>
            				<td>�����������</td>
            				<td>
            					<?= (!empty($rep['order_dungeon_keeper'])?"+".$rep['order_dungeon_keeper']:"0") ?>
            				</td>
            	            <td>0</td>
            			</tr>
            			<tr>
            				<td style="background-color: #ffdd00"></td>
            				<td>���� �����������</td>
            				<td>�����������</td>
            				<td>
            					<?= (!empty($rep['league_gladiators']['glasses'])?"+".$rep['league_gladiators']['glasses']:"0") ?>
            				</td>
            	            <td>0</td>
            			</tr>
            			<tr>
            				<td style="background-color: #ffdd00"></td>
            				<td>���� ����������� �����</td>
            				<td>�����������</td>
            				<td>
            					<?= (!empty($rep['league_gladiators_arena']['glasses'])?"+".$rep['league_gladiators_arena']['glasses']:"0") ?>
            				</td>
            	            <td>0</td>
            			</tr>
            			<tr>
            				<td style="background-color: #ffdd00"></td>
            				<td>�������� ��������</td>
            				<td>�����������</td>
            				<td>
            					<?= (!empty($rep['blutbad']['glasses'])?"+".$rep['blutbad']['glasses']:"0") ?>
            				</td>
            	            <td>0</td>
            			</tr>
            		</tbody>
            	</table>

            <? } elseif (($_GET['cmd']=='skill.show')) { ?>

            	<table class="table-list" id="skills">
            		<thead>
            			<tr>
            				<th style="width: 50%;">������</th>
            				<th>��������</th>
            			</tr>
            		</thead>
            		<tbody>
            			<tr>
            				<td>�����������</td>
            				<td class="value"><?= $user_my->user['skill_fishing'] ?></td>
            			</tr>
            			<tr>
            				<td>�������</td>
            				<td class="value"><?= $user_my->user['skill_craft'] ?></td>
            			</tr>
            			<tr>
            				<td>������</td>
            				<td class="value"><?= $user_my->user['skill_mine'] ?></td>
            			</tr>
            		</tbody>
            	</table>
            	<i>������������ �������� ��� ����� �������</i>

            <? } elseif (($_GET['cmd']=='achievement.show')) { ?>
            <?
              /*
                <p><span class="time-system-attention">13:13:43</span> <span class="system-attention">��������!</span> <span class="user-from-room" name="technique">technique</span> ������� ���������� <strong style="cursor: pointer;" onclick="top.frames['blutbad'].showAchBig( 'fame_pos_1' );">�������</strong> (5 �����)!</p>
                ���� ��� ���������� �� ��������
                ���� ��� ���������� �������� 02-04-2015 16:37:30
               */
            ?>
                <div class="tabs-content">

                	<div id="achievements">

                	<table class="table-list">
                		<thead>
                			<tr>
                				<th colspan="2">��������</th>
                			</tr>
                		</thead>
                      <?
                        $user_g_achievements = unserialize($user_my->user['achievements']);
                        $user_achievements = array();
                        $list_active_achievements = array();
                        $list_no_active_achievements = array();
                        $list_active_groupe_achievements = array();

                        $users_achiev = sql_query("SELECT * FROM `users_achievements` WHERE `owner` = '".$user_my->user['id']."' ;");
                        while ($row = fetch_array($users_achiev)) {
                          $groupe = unserialize($row['groupe']);
                          foreach ($groupe as $k => $v) {
                           // echo $k.'  -  '.serialize($v).'<br>';
                            $user_achievements[$k] = $v;
                          }
                        }

                        if (!empty($user_achievements)) {
                          foreach ($user_achievements as $k => $v) {

                        //  echo $k.'-'.$v['status'].'<br>';
                           if ($v['status'] == '2') {
                            $list_no_active_achievements[] = $k;
                           }

                           if ($v['status'] == '3') {
                            $list_active_achievements[] = $k;
                            foreach ($groops_achievements as $ka => $va) {
                             if (in_array($k, $va['list'])) {
                               $list_active_groupe_achievements[] = $ka;
                             }
                            }
                           }

                          }
                        }

                       foreach ($groops_achievements as $id_g_achiev => $g_achiev) {

                        if ($g_achiev['list'] && in_array($id_g_achiev, $list_active_groupe_achievements)) {

                        $state_gr = empty($user_g_achievements[$id_g_achiev]['r'])?0:1;

                       ?>
                    	<thead>
                    		<tr>
                    			<th colspan="2">
                    				<img alt="" class="ach-group-toggle" src="http://img.blutbad.ru/i/sb_<?= ($state_gr?"minus":"plus") ?>.gif">
                    				<?= $g_achiev['name'] ?>
                    			</th>
                    		</tr>
                    	</thead>

                    	<tbody group="<?= $id_g_achiev ?>" state="r" style="<?= ($state_gr?"table-row-group":"display: none") ?>;">
                         <?
                          $achievements_finish = explode(";", $user_my->user['achievements_finish']);
                          foreach ($g_achiev['list'] as $id_achiev => $v_achiev) {

                           $achiev = $achievements[$v_achiev];

                           /*if (!in_array($v_achiev, $achievements_finish) && substr_count($user_my->user['achievements_finish'], $achiev['done_achiev'])) { */
                           if (in_array($v_achiev, $list_active_achievements)) {

                         ?>
                    		<tr>
                                <td class="img">
                    				<img alt="" height="80" src="http://img.blutbad.ru/i/achievement/<?= $v_achiev ?>_r.jpg" width="80">
                    			</td>
                    			<td>
                    				<div>
                    					<b><?= $achiev['name'] ?></b>
                    				</div>
                    				<div style="margin-left: 10px;">
                                        <?
                                            $valmax = $achiev['goals'][1]['valmax'];
                                            $achiev['description'] = str_replace('%s%', ''.$valmax.'', $achiev['description']);
                                        ?>
                    					<div>
                    						<i><?= $achiev['description'] ?></i>
                    					</div>

                    					<div>����:</div>
                    					<ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">
                                          <?

                                           foreach ($achiev['goals'] as $id_goals => $goals) {

                                             if (!empty($user_achievements[$v_achiev]['goals'][$id_goals]['listgoals'])) {
                                               $goals['listgoals'] = $user_achievements[$v_achiev]['goals'][$id_goals]['listgoals'];
                                             }

                                             $goals['val']   = $user_achievements[$v_achiev]['goals'][$id_goals]['val'];

                                             $goals['description'] = str_replace('%s/s%', '<strong>'.$goals['val'].'/'.$goals['valmax'].'</strong>', $goals['description']);

                                          ?>
                                        	<li>
                                                <?= $goals['description'] ?>
                                            <?
                                            if (!empty($goals['listgoals'])) {
                                             echo '<img alt="" class="toggle" src="http://img.blutbad.ru/i/sb_plus.gif">';
                                             echo '<ul class="ach_aim_list invisible">';
                                             foreach ($goals['listgoals'] as $k_listgoals => $v_listgoals) {
                                            ?>
                                        		<li>
                                        			<?= $v_listgoals['done']?"<b>":"" ?><?= $v_listgoals['name'] ?><?= $v_listgoals['done']?"</b>":"" ?>
                                        		</li>

                                            <?
                                             }
                                             echo '</ul>';
                                             }
                                            ?>
                                        	</li>
                                          <?
                                           }
                                          ?>
                    					</ul>
                    					<div>�������:</div>
                    					<ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">
                                          <?
                                           foreach ($achiev['honors'] as $id_honors => $honors) {
                                          ?>
                                        	<li>
                                                <em><?= $honors['name'] ?></em> <?= $honors['description'] ?>
                                        	</li>
                                          <?
                                           }
                                          ?>

                    					</ul>
                    				</div>
                    			</td>
                    		</tr>
                         <?
                           }
                          }
                         ?>

                    	</tbody>
                       <?
                        }
                       }
                       ?>

                	</table>


                	<table class="table-list">
                		<thead>
                			<tr>
                				<th colspan="2">�� �����������</th>
                			</tr>
                		</thead>
                       <?

                       $achievements_finish = explode(";", $user_my->user['achievements_finish']);
                       $arh_no_active = array();

                        foreach ($groops_achievements as $k => $v) {

                         $list_no_active = array();
                         $list_no_active['gname'] = $k;
                         $list_no_active['name']  = $v['name'];

                         foreach ($v['list'] as $id_achiev => $v_achiev) {

                           $achiev = $achievements[$v_achiev];

                             $is_active = 0;
                             if (!empty($achievements_finish)) {
                               foreach ($achievements_finish as $kq => $vq) {
                                if (!empty($achiev['done_achiev']) && $achiev['done_achiev'] == $vq) {
                                  $is_active = 1;
                                }
                               }
                             }

                              if (!$is_active && !in_array($v_achiev, $achievements_finish) && !in_array($v_achiev, $list_active_achievements)) {
                                 $list_no_active['list'][] = $v_achiev;
                              } elseif(in_array($v_achiev, $list_no_active_achievements) && $is_active) {
                                 $list_no_active['list'][] = $v_achiev;
                              }

                         }
                          if (!empty($list_no_active['list'])) { $arh_no_active[] = $list_no_active; }

                        }

                       foreach ($arh_no_active as $id_g_achiev => $g_achiev) {

                        if (!empty($g_achiev['list'])) {
                        $id_g_ach = $g_achiev['gname'];
                        $state_gr = empty($user_g_achievements[$id_g_ach]['n'])?0:1;

                       ?>
                    	<thead>
                    		<tr>
                    			<th colspan="2">
                    				<img alt="" class="ach-group-toggle" src="http://img.blutbad.ru/i/sb_<?= ($state_gr?"minus":"plus") ?>.gif">
                    				<?= $g_achiev['name'] ?><? /* <span class="attention">(���������)</span>*/ ?>
                    			</th>
                    		</tr>
                    	</thead>

                    	<tbody group="<?= $id_g_ach ?>" state="n" style="<?= ($state_gr?"table-row-group":"display: none") ?>;">
                         <?

                          foreach ($g_achiev['list'] as $id_achiev => $v_achiev) {

                           $achiev = $achievements[$v_achiev];

                           $is_done = 0;
                           foreach ($achievements_finish as $kb => $vb) {
                            if (!empty($achiev['done_achiev']) && $achiev['done_achiev'] == $vb) {
                              $is_done = 1;
                            }
                           }

                           if ((($achievements_finish && $is_done) || (!in_array($v_achiev, $list_active_achievements) && empty($achiev['done_achiev']))) && !in_array($v_achiev, $achievements_finish)) {

                         ?>
                    		<tr>
                    			<td class="img">
                    				<img alt="" height="80" src="http://img.blutbad.ru/i/achievement/<?= $v_achiev ?>_n.jpg" width="80">
                    			</td>
                    			<td>
                    				<div>
                    					<b><?= $achiev['name'] ?></b>
                    				</div>
                    				<div style="margin-left: 10px;">
                                        <?
                                            $valmax = $achiev['goals'][1]['valmax'];
                                            $achiev['description'] = str_replace('%s%', ''.$valmax.'', $achiev['description']);
                                        ?>
                    					<div>
                    						<i><?= $achiev['description'] ?></i>
                    					</div>

                    					<div>����:</div>
                    					<ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">
                                          <?
                                           foreach ($achiev['goals'] as $id_goals => $goals) {

                                             $goals['description'] = str_replace('%s/s%', '<strong>'.$goals['val'].'/'.$goals['valmax'].'</strong>', $goals['description']);

                                          ?>
                                        	<li>
                                                <?= $goals['description'] ?>
                                            <?
                                            if ($goals['listgoals']) {
                                             echo '<img alt="" class="toggle" src="http://img.blutbad.ru/i/sb_plus.gif">';
                                             echo '<ul class="ach_aim_list invisible">';
                                             foreach ($goals['listgoals'] as $k_listgoals => $v_listgoals) {
                                            ?>
                                        		<li>
                                        			<?= $v_listgoals['done']?"<b>":"" ?><?= $v_listgoals['name'] ?><?= $v_listgoals['done']?"</b>":"" ?>
                                        		</li>

                                            <?
                                             }
                                             echo '</ul>';
                                             }
                                            ?>
                                        	</li>
                                          <?
                                           }
                                          ?>
                    					</ul>
                    					<div>�������:</div>
                    					<ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">
                                          <?
                                           foreach ($achiev['honors'] as $id_honors => $honors) {
                                          ?>
                                        	<li>
                                                <em><?= $honors['name'] ?></em> <?= $honors['description'] ?>
                                        	</li>
                                          <?
                                           }
                                          ?>

                    					</ul>
                    				</div>
                    			</td>
                    		</tr>
                         <?
                           }
                          }
                         ?>

                    	</tbody>
                       <?
                        }
                       }
                       ?>

                	</table>

                	<table class="table-list">
                		<thead>
                			<tr>
                				<th colspan="2">�����������</th>
                			</tr>
                		</thead>

                      <?

                        $user_achievements = explode(";", $user_my->user['achievements_finish']);
                        $list_active_achievements = array();
                        $list_active_groupe_achievements = array();

                        foreach ($user_achievements as $k => $v) {
                          $list_active_achievements[] = $v;

                          foreach ($groops_achievements as $ka => $va) {
                           if (in_array($v, $va['list'])) {
                             $list_active_groupe_achievements[] = $ka;
                           }
                          }
                        }



                       foreach ($groops_achievements as $id_g_achiev => $g_achiev) {

                        if ($g_achiev['list'] && in_array($id_g_achiev, $list_active_groupe_achievements)) {

                        $state_gr = empty($user_g_achievements[$id_g_achiev]['f'])?0:1;

                       ?>
                    	<thead>
                    		<tr>
                    			<th colspan="2">
                    				<img alt="" class="ach-group-toggle" src="http://img.blutbad.ru/i/sb_<?= ($state_gr?"minus":"plus") ?>.gif">
                    				<?= $g_achiev['name'] ?>
                    			</th>
                    		</tr>
                    	</thead>

                    	<tbody group="<?= $id_g_achiev ?>" state="f" style="<?= ($state_gr?"table-row-group":"display: none") ?>;">
                         <?
                          $achievements_finish = explode(";", $user_my->user['achievements_finish']);
                          foreach ($g_achiev['list'] as $id_achiev => $v_achiev) {

                           $achiev = $achievements[$v_achiev];

                           if (in_array($v_achiev, $achievements_finish)) {

                         ?>
                    		<tr>
                                <td class="img">
                    				<img alt="" height="80" src="http://img.blutbad.ru/i/achievement/<?= $v_achiev ?>_f.jpg" width="80">
                    			</td>
                    			<td>
                    				<div>
                    					<b><?= $achiev['name'] ?></b>
                    				</div>
                    				<div style="margin-left: 10px;">
                                        <?
                                            $valmax = $achiev['goals'][1]['valmax'];
                                            $achiev['description'] = str_replace('%s%', ''.$valmax.'', $achiev['description']);
                                        ?>
                    					<div>
                    						<i><?= $achiev['description'] ?></i>
                    					</div>

                    					<div>����:</div>
                    					<ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">
                                          <?

                                           foreach ($achiev['goals'] as $id_goals => $goals) {

                                             $goals['val'] = $goals['valmax'];

                                             $goals['description'] = str_replace('%s/s%', '<strong>'.$goals['val'].'/'.$goals['valmax'].'</strong>', $goals['description']);

                                          ?>
                                        	<li>
                                                <?= $goals['description'] ?>
                                            <?
                                            if ($goals['listgoals']) {
                                             echo '<img alt="" class="toggle" src="http://img.blutbad.ru/i/sb_plus.gif">';
                                             echo '<ul class="ach_aim_list invisible">';
                                             foreach ($goals['listgoals'] as $k_listgoals => $v_listgoals) {

                                                 $b_s = "<b>";
                                                 $b_e = "</b>";

                                               if ($goals['valmax']) {
                                                 $b_s = "";
                                                 $b_e = "";
                                               }

                                            ?>
                                        		<li>
                                        			<?= $b_s.$v_listgoals['name'].$b_e ?>
                                        		</li>
                                            <?
                                             }
                                             echo '</ul>';
                                             }
                                            ?>
                                        	</li>
                                          <?
                                           }
                                          ?>
                    					</ul>
                    					<div>�������:</div>
                    					<ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">
                                          <?
                                           foreach ($achiev['honors'] as $id_honors => $honors) {
                                          ?>
                                        	<li>
                                                <em><?= $honors['name'] ?></em> <?= $honors['description'] ?>
                                        	</li>
                                          <?
                                           }
                                          ?>

                    					</ul>
                    				</div>
                    			</td>
                    		</tr>
                         <?
                           }
                          }
                         ?>

                    	</tbody>
                       <?
                        }
                       }
                       ?>

                	</table>

                  		<b>������ ������� �� ����������</b>
                  		<ul id="achievement-rewards">
                          <?

                          $finish_baf = array(
                           'winner_25000'       => '"������� �������"',
                           'fight_6'            => '����� "����������� ����"',
                           'tavern_all'         => '"������ 20% ��� ������ ����� ���� � �������������� �������� � ��������"',
                           'naemnik_500'        => '"������ 10% ��� ������� ��� "�������������""',
                           'mentor_50'          => '"���������� ���������� ���������� ����������� ����� �� ������ �������� �� 50%"',
                           'waterman_360'       => '"�������������� ����������� "������ �������� ���������" � ���� � ��������"',
                           'dark_f_270'         => '"���������� ������� ������� ����� ������ ����������� ���������� "����" ��� ����������� �� ������������� ����������"',
                           'light_f_270'        => '"���������� ������� ������� ����� ������ ����������� ���������� "����" ��� ����������� �� ������������� ����������"',
                           'chaos_f_270'        => '"���������� ������� ������� ����� ������ ����������� ���������� "����" ��� ����������� �� ������������� ����������"',
                           'order_f_270'        => '"���������� ������� ������� ����� ������ ����������� ���������� "�������" ��� ����������� �� ������������� ����������"',
                           'dobla_10'           => '"��������� ����"',
                           'karma_10'           => '"100%-�� ������������ ����� ������ ����������� ����������"',
                           'presents_500'       => '"������ 20% ��� ������� ����� ��������� ���������"',
                           'friends_100'        => '"����������� ���������� ������ ��������� �� ������� ������ �� 120"',
                           'use_rune_hp_500000' => '"������ 10% �� ��� ��������� ���� �������"',
                           'use_rune_pw_500000' => '"������ 10% �� ��� ��������� ���� ��������������"',
                           'use_rune_zah_1000'  => '"������ 10% �� ��� ���� ��������� "������""',
                           'use_rune_opl_1000'  => '"������ 10% �� ��� ���� ��������� "�����""',
                           'use_rune_tak_1000'  => '"������ 10% �� ��� ���� ��������� "������""',
                           'use_rune_pok_1000'  => '"������ 10% �� ��� ���� ��������� "������""',
                           'use_rune_vozd_1000' => '"������ 10% �� ��� ���� ��������� "���������""'
                          );

                          foreach ($achievements_finish as $k => $v) {

                           if (!empty($finish_baf[$v])) {
                            echo "<li>".$finish_baf[$v]."</li>";
                           }

                          }

                         ?>
                  		</ul>

                		<table class="table-list" id="all-achievements">
                			<thead>
                				<tr>
                		            <th>����������</th>
                		            <th>��������</th>
                				</tr>
                			</thead>
                			<tbody>
                				<tr>
                					<td>��������� ������� �������</td>
                					<td class="value">0</td>
                				</tr>
                				<tr>
                					<td>��������� �� �������</td>
                					<td class="value"><?= $user_my->user['fish_catch'] ?></td>
                				</tr>
                				<tr>
                					<td>������� "�����"</td>
                					<td class="value">0</td>
                				</tr>
                				<!--<tr>
                					<td>��������� ����</td>
                					<td class="value">0</td>
                				</tr>
                				<tr>
                					<td>�������� ����� � ��������</td>
                					<td class="value">0</td>
                				</tr>-->
                				<tr>
                					<td>������� ����� � ���������</td>
                					<td class="value"><?= floor($user_my->user['victorina_score']) ?></td>
                				</tr>
                				<tr>
                					<td>������� ����� ����������</td>
                					<td class="value"><?= $user_my->user['achievements_glasses'] ?></td>
                				</tr>
                				<tr>
                					<td>���������� ������</td>
                					<td class="value"><?= $user_my->user['chances_wheel'] ?> �� <?= $user_my->user['chances_wheel_max'] ?></td>
                				</tr>
                			</tbody>
                		</table>
                	</div>

                </div>
            <? } elseif (($_GET['cmd']=='collections.show')) { ?>
                 <div class="tabs-content">


    <script type="text/javascript">
		var nd = '<?= $user['nd'] ?>';
		var rand = '<?= "0.".time() ?>';

        $(function() {
            var cur_f_id = '';
        	$('.ftdlink').click(function() {
        		var f_id = $(this).attr('f_id');
                $('.ftdlink').attr( 'style', '');
                if (cur_f_id != f_id) {

                  $('.ftdlink').stop();
                  $('.ftdlink').removeClass('ftdsellink');

                  if (cur_f_id) {
                    $('#' + cur_f_id).animate({marginLeft: '-10px',opacity: 0.4}, 100, function() {
                      $('.f_items').hide();
                      $( '#' + f_id).css({marginLeft: '-10px', opacity: 0.4}).hide();
                      $( '#' + f_id).show();
                      $( '#' + f_id).animate({marginLeft: '0px',opacity: 1});
                    });
                  } else {
                      $('.f_items').hide();
                      $( '#' + f_id).css({marginLeft: '-10px', opacity: 0.4}).hide();
                      $( '#' + f_id).show();
                      $( '#' + f_id).animate({marginLeft: '0px',opacity: 1});
                  }
                  cur_f_id = f_id;

                        $.cookie('cur-tabs-collection', (cur_f_id), {
    						domain: getDomain(true),
    						expires: 1/24/2 // ������ ������� ������� �������
    					});

                  $('.ftdlink').removeAttr("background-color");
                  $(this).addClass('ftdsellink');
                }
        	 	return false;
        	});

            $('.ftdlink').not('.ftdsellink').hover(
          		function() {
          		       if ($(this).attr('class') != 'ftdlink ftdsellink') {
                			$(this).stop().animate({backgroundColor: '#B5B2B2'}, 100);
          		       }
          		},
          		function() {
          		       if ($(this).attr('class') != 'ftdlink ftdsellink') {
                			$(this).animate({backgroundColor: '#C5C5C5'}, 100);
          		       }
          		}
          	);

          var CollectionRewards = new Object();
          $('img[reward_code]').tooltip($.extend({}, tooltipDefaults, {
          		bodyHandler: function() {
          			return CollectionRewards[$(this).attr('reward_code')];
          		}
          }));

        });


    </script>
    <style type="text/css">
      .tabforge .ftdlink,.ftdsellink{font-weight:bold;cursor:pointer;margin-bottom:5px;border:1px solid #556775;padding:3px;display:block;background:#C5C5C5}
      .tabforge .ftdlink:hover,.ftdsellink:hover{background:#B5B2B2}
      .tabforge .ftdsellink{background:#FFDB90}
      .tabforge .ftdsellink:hover{background:#F1B432}
      .f_items .collection{width:900px}
      .f_items .collection_item_title{width:100px}
      .bimg{cursor:pointer}
      .sadrune td{padding:3px}
    </style>
    <script type="text/javascript">
      $(function() {
      	$('.collection').hover(
      		function() { $(this).stop().css({opacity: 1}); $(".collection").stop().not(this).animate({opacity: .3}, 300);},
      		function() {$(".collection").stop().animate({opacity: 1}, 300);}
      	);
      });
    </script>
<table class="tabforge" style="width: 100%">
                         <tr>
                           <td style="padding-left: 10px; width: 100%;">
                           <?

                              foreach ($collection_data as $k_id => $collection) {

                              $items_name = $collection['items_name'];
                              $items_idname = $collection['items_idname'];

                           ?>
                            <div id="<?= $k_id ?>" class="f_items" <? /*style="<?= ($_COOKIE['cur-tabs-collection']==$k_id?"":"display: none") ?>"*/ ?>>

                                <table class="collection">
                                		<tbody><tr>
                                			<td colspan="8" class="collection_title"><?= $collection['name'] ?></td>
                                		</tr>
                                		<tr>
                                            <? for ($i = 0; $i <= 6; $i++) { ?>
                                			<td class="collection_item_title">
                                				<?= ($user_collection[$k_id]['c'][$i]?$items_name[$i]:"") ?>
                                			</td>
                                            <? } ?>
                                			<td class="collection_collect" rowspan="3">
                                                <? if (!empty($user_collection[$k_id]['fin'])) { ?>
                                				<img alt="" height="80" src="http://img.blutbad.ru/i/collections/<?= $collection['id_name'] ?>_collected.jpg" width="80">
                                				<br><img src="http://img.blutbad.ru/i/quest/rewards/univers_baf.gif" class="tooltip" title="<?= '<div>�����:</div><div>&quot;'.$collection['name'].'&quot;</div>' ?>" alt="">
                                                <? } elseif(count($user_collection[$k_id]['c']) >= 7 && empty($user_collection[$k_id]['fin'])) { ?>
                                                <a href="?cmd=collect_collection&code=<?= $k_id ?>&nd=<?= $user['nd'] ?>" onclick="return confirm('������� ���������?');">
                                				<img alt="" height="80" src="http://img.blutbad.ru/i/collections/<?= $collection['id_name'] ?>_collect.jpg" width="80">
                                                </a>
                                                <? } else { ?>
                                				<img alt="" height="80" src="http://img.blutbad.ru/i/collections/<?= $collection['id_name'] ?>_item_unknown.jpg" width="80">
                                                <? } ?>
                                				<br>
                                			</td>
                                		</tr>
                                		<tr>
                                            <? for ($i = 0; $i <= 6; $i++) { ?>
                                			<td class="collection_item_image">
                                            	<img alt="" height="60" src="http://img.blutbad.ru/i/collections/<?= ($user_collection[$k_id]['c'][$i]?$k_id."_".$items_idname[$i].".gif":$collection['id_name']."_item_unknown.jpg") ?>" width="60">
                                            </td>
                                            <?  } ?>
                                		</tr>
                                		<tr>
                                			<td class="collection_item_count"><?= ($user_collection[$k_id]['c'][0]?"x".abs($user_collection[$k_id]['c'][0]):"&nbsp;") ?></td>
                                			<td class="collection_item_count"><?= ($user_collection[$k_id]['c'][1]?"x".abs($user_collection[$k_id]['c'][1]):"&nbsp;") ?></td>
                                			<td class="collection_item_count"><?= ($user_collection[$k_id]['c'][2]?"x".abs($user_collection[$k_id]['c'][2]):"&nbsp;") ?></td>
                                			<td class="collection_item_count"><?= ($user_collection[$k_id]['c'][3]?"x".abs($user_collection[$k_id]['c'][3]):"&nbsp;") ?></td>
                                			<td class="collection_item_count"><?= ($user_collection[$k_id]['c'][4]?"x".abs($user_collection[$k_id]['c'][4]):"&nbsp;") ?></td>
                                			<td class="collection_item_count"><?= ($user_collection[$k_id]['c'][5]?"x".abs($user_collection[$k_id]['c'][5]):"&nbsp;") ?></td>
                                			<td class="collection_item_count"><?= ($user_collection[$k_id]['c'][6]?"x".abs($user_collection[$k_id]['c'][6]):"&nbsp;") ?></td>
                                		</tr>
                                	</tbody></table>
                            </div>
                            <? } ?>
                           </td>
                         </tr>
                       </table>
                 </div>
            <? } ?>

          </div>
	    </div>
	  </div>
			</td>
			<td class="right-col right-col-small">
				<div class="right-col-inner right-col-inner-small">
					<div class="buttons">
						<input class="xbbutton" onclick="location.reload();" type="button" value="��������">
						<input class="xgbutton" id="return" onclick="location.href='/main.php'" type="button" value="���������">
					</div>
				</div>
			</td>
		</tr>
	</tbody></table>
</body></html>
<? include("a_process_end.php"); ?>