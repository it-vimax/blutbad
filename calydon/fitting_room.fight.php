<?
    error_reporting(E_ALL);
    ini_set('display_errors',true);
    ini_set('html_errors',true);
    ini_set('error_reporting',E_ALL ^ E_NOTICE);

 $mf_krit_def = 0.1;       # ���� � ����� ���������
 $mf_uvorot_def = 0.1;       # ���� � ����� ���������
 $mf_is_krit_max = 0.95; # ����������� ����������� = ���������
 $mf_is_uvor_max = 0.95; # ����������� ����������� = ���������
 $str_factor = 0.5;      # ������� �����
 $z9_factor = 1;         # ���� � �����

/***------------------------------------------
 *  �������� �� ��������� "���� ����"
 **/

 function get_block ($komy, $att, $def, $enemy_is_block) {

# ��� ���������� = $enemy
# ���� $enemy �������� ���� ��� $def
# ���� $enemy ��� ���� ��� $att

  #  �� ����� ������
	/*if($enemy > _BOTSEPARATOR_) {
	  $bots = sql_row('SELECT * FROM `bots` WHERE `id` = '.$enemy.' LIMIT 1;');
      $enemy = $bots['prototype'];
	}*/

     // if($komy=="me"){ $kogochekat = $this->user['id']; } elseif( $komy=="he" ){ $kogochekat = $enemy; }

      if($enemy_is_block){
	     $blocks = array (
						  '1' => array (1,2),
						  '2' => array (2,3),
						  '3' => array (3,4),
						  '4' => array (4,1),
						  '5' => array (4,2),
						  '6' => array (1,3)
						);
       } else {
	     $blocks = array (
						  '1' => array (1),
						  '2' => array (2),
						  '3' => array (3),
						  '4' => array (4)
					     );
       }
				switch ($komy) {
					case "me" :
						if (in_array($att, $blocks[$def])) {
							return true;
						} else {
							return false;
						}
					break;
				   # ���� �������
					case "he" :
						if (in_array($att, $blocks[$def])) {
							return true;
						} else {
							return false;
						}
					break;
				}
 }

/*------------------------------------------------------------------
 ������� ������������
--------------------------------------------------------------------*/
function solve_mf($my_player, $enemy_player, $mf_user_parm) {
 global $mf_krit_def, $mf_uvorot_def, $mf_is_krit_max, $mf_is_uvor_max, $str_factor, $z9_factor;

 $mf = array ();

   $mf_change = function ($mf_user_parm, $u_l = "l", $u_r = "r", $my_player, $enemy_player) {

     $mf_krit_def    = 0.01;  # ���� � ����� ���������
     $mf_uvorot_def  = 0.01;  # ���� � ����� ���������
     $mf_is_krit_max = 0.95; # ����������� ����������� = ���������
     $mf_is_uvor_max = 0.95; # ����������� ����������� = ���������
     $str_factor     = 0.5;  # ������� �����
     $z9_factor      = 1;    # ���� � �����

    #----------------------------------------------------------
    # ������� ����� "1=������, 0=�����"
      $pos_att_my_player    = $my_player['pos'];
      $pos_att_enemy_player = $enemy_player['pos'];

    #----------------------------------------------------------
    # ��� ������� �� �������� ������
      $me_coef = $mf_user_parm[$u_l."_pw"] / $mf_user_parm[$u_l."_pwmax"];
      if($me_coef < 0.1) $me_coef = 0.1;

    #----------------------------------------------------------
    # ��� ������� �� �������� ������
      $he_coef = $mf_user_parm[$u_r."_pw"] / $mf_user_parm[$u_r."_pwmax"];
      if($he_coef < 0.1) $he_coef = 0.1;

    #-------------------------------------
    # �����: ������, �����, �����, ���
      $en_bron = 0;
      switch($my_player['attack']){
       case 1: $en_bron = $enemy_player['bron1']; break;
       case 2: $en_bron = $enemy_player['bron2']; break;
       case 3: $en_bron = $enemy_player['bron3']; break;
       case 4: $en_bron = $enemy_player['bron4']; break;
      }

      $bron = $en_bron; # rand(0, $en_bron);

      // sqrt = � ��� ���� NaN ���� ���� -1 ���������� ������ �� arg ��� ����������� �������� NAN ��� ������������� �����.

    #---------------------------------------
    # ���������: ����, % �����, % �������,
      if (is_nan($my_player['sila']) || is_null($my_player['sila']) || $my_player['sila'] < 0) { $my_player['sila'] = 3; }
      $mod_sila = floor(abs($my_player['sila']));
      $dam   = 0;

    #----------------------
    # ���� ������ ����
      if (empty($my_player['minu'])) { $my_player['minu'] = 0; }
      if (empty($my_player['maxu'])) { $my_player['maxu'] = 0; }

    #----------------------
    # ���� ����� � �����
      if ($pos_att_my_player == 0 && $pos_att_enemy_player == 0) {
        /*$my_player['minu'] *= 1.2;
        $my_player['maxu'] *= 1.2;*/
        $mod_sila *= 1.4;
      }

      if ($mod_sila > 5) {

        $kor_sila = sqrt($mod_sila);
        $kor10_sila = $kor_sila*sqrt($kor_sila);

        $minu  = ($my_player['minu'])+$kor10_sila;
        $maxu  = ($my_player['maxu'])+$kor10_sila+$kor_sila;

        if (is_nan($dam) || is_null($minu) || $minu < 0) { $minu = 0; }
        if (is_nan($dam) || is_null($maxu) || $maxu < 0) { $maxu = 1; }

        $dam  += mt_rand($minu, $maxu)+sqrt($kor_sila);
      } else {
        $dam  += rand(1, 3);
      }

    #-------------------------------------------------
    # ���� ������ ��� 5 �������� ���� ��� +5 �����
     // if ($my_player['inta'] > 15) $dam += 6; elseif ($my_player['inta'] > 12) $dam += 5; elseif ($my_player['inta'] > 9) $dam += 4; else
     // if ($my_player['inta'] > 7)  $dam += 3; elseif ($my_player['inta'] > 6)  $dam += 2; elseif ($my_player['inta'] > 5) $dam += 1;

    #--------------------------
    # �������� ������������
      $x8_min = round(sqrt($mod_sila + $mod_sila));
      if($pos_att_my_player == 1){ $x8_min = round($x8_min * 1.5); }


        $my_uvorot     = $my_player['mfuvorot']     + ($my_player['lovk'] * 10);    # ������
        $enemy_auvorot = $enemy_player['mfauvorot'] + ($enemy_player['lovk'] * 10); # ������ ������
        $my_krit       = $my_player['mfkrit']       + ($my_player['inta'] * 10);    # ����
        $enemy_akrit   = $enemy_player['mfakrit']   + ($enemy_player['inta'] * 10); # ������ ����

        $r_1 = 1.1;
        $r_0 = 1.4;

        if($pos_att_my_player == 0 && $pos_att_enemy_player == 0){
         $r_0  = 1.4;
         $dam *= 1.5;
        }
        if(($pos_att_my_player == 0 && $pos_att_enemy_player == 1) || ($pos_att_my_player == 1 && $pos_att_enemy_player == 0)){
         $r_0  = 1.2;
         $dam *= 1.1;
        }

      #-------------
      # ��� ����
        if($pos_att_my_player == 1){              #-- � ������
           $my_krit       /= float_rand(1, $r_0); # �������� ����������� ����
           $my_uvorot     *= float_rand(1, $r_0); # �������� ���� ����������

           $enemy_akrit   /= float_rand(1, $r_0); # ��������������� ����
           $enemy_auvorot *= float_rand(1, $r_0); # �������� ����������
          // $dam         *= 1.2;
        } elseif($pos_att_my_player == 0){        #-- � �����
           $my_krit       *= float_rand(1, $r_0); # �������� ����������� ����
           $my_uvorot     /= float_rand(1, $r_0); # �������� ���� ����������

           $enemy_akrit   /= float_rand(1, $r_0); # �������� ��������������� ����
           $enemy_auvorot *= float_rand(1, $r_0); # �������� ����������
           $dam           *= 1.2;
        }

      #-------------
      # ��� ����
        if($pos_att_enemy_player == 1){           #-- � ������
           $enemy_akrit   /= float_rand(1, $r_0); # �������� ��������������� ����
           $enemy_auvorot *= float_rand(1, $r_0); # �������� ����������

           $my_krit       /= float_rand(1, $r_0); # �������� ��������������� ����
           $my_uvorot     *= float_rand(1, $r_0); # �������� ����������
        } elseif($pos_att_enemy_player == 0){     #-- � �����
           $enemy_akrit   *= float_rand(1, $r_0); # �������� ����������� ����
           $enemy_auvorot /= float_rand(1, $r_0); # �������� ���� ����������

           $my_krit       /= float_rand(1, $r_0); # ��������������� ����
           $my_uvorot     *= float_rand(1, $r_0); # �������� ����������
          // $dam           *= $r_0;
        }

       // $dam -= mt_rand(sqrt($bron), $bron);
        $dam -= sqrt($bron);

      #--------------------------------------
      # ���� ������ ����� ��� ������ ����
        if($dam < 1) $dam = 1;

      #-------------------
      # ���� ����������
        if ($my_uvorot >= 0 && empty($enemy_auvorot)) $uvorot = $my_uvorot;
        if (rand(1, 100) <= 5) $uvorot = 0;
        else $uvorot = $my_uvorot / $enemy_auvorot;

      #-----------------
      # ���� ���������
        if ($my_krit >= 0 && empty($enemy_akrit)) $krit = $my_krit;
        if (rand(1, 100) <= 5) $krit = 0;
        else $krit = $my_krit / $enemy_akrit;


      #----------------------------
      # �������� ����� �� ������
        if (is_nan($dam) || is_null($dam) || $dam < 0) { $dam = 0; }
      #---------------------------
      # �������� ����� �� ������
        if (is_nan($krit) || is_null($krit) || $krit < 0) { $krit = 0; }
      #------------------------------
      # �������� ������� �� ������
        if (is_nan($uvorot) || is_null($uvorot) || $uvorot < 0) { $uvorot = 0; }

        return array (
              	      'udar'    => floor($dam),
              		  'krit'    => $krit + $mf_krit_def,
              	      'uvorot'  => $uvorot + $mf_uvorot_def,
              		  'x8_min'  => $x8_min,
              		 // 'str1'     => ' mu:'.round($minu, 2).' - mx:'.round($maxu, 2),
              		  'str'    => "",
        );
   };

   $mf = array();
   $mf['me'] = $mf_change($mf_user_parm, "l", "r", $my_player, $enemy_player);
   $mf['he'] = $mf_change($mf_user_parm, "r", "l", $enemy_player, $my_player);

  return $mf;
}

 function razmen_init ($left_user,$right_user, $mf_rl_data) {
 global $mf_krit_def, $mf_uvorot_def, $mf_is_krit_max, $mf_is_uvor_max, $str_factor, $z9_factor;

   if (empty($mf_rl_data['l_pos_left'])) {
     $l_pos_left = rand(0,1);
   } else {
     $l_pos_left = $mf_rl_data['l_pos_left']-1;
   }

  $mfuser = array(
     "attack" => rand(1,4),
     "defend" => rand(1,4),
     "pos" => $l_pos_left,

     "sila" => $left_user[2],
     "lovk" => $left_user[3],
     "inta" => $left_user[4],

     "hp" => $left_user[5],
     "maxhp" => $left_user[5],

     "minu" => $left_user[7],
     "maxu" => $left_user[8],

     "mfkrit" => ($left_user[9]),
     "mfakrit" => ($left_user[10]),

     "mfuvorot" => ($left_user[11]),
     "mfauvorot" => ($left_user[12]),

     "bron1" => $left_user[13],
     "bron2" => $left_user[14],
     "bron3" => $left_user[15],
     "bron4" => $left_user[16]
  );


   if (empty($mf_rl_data['r_pos_left'])) {
     $r_pos_left = rand(0, 1);
   } else {
     $r_pos_left = $mf_rl_data['r_pos_left']-1;
   }

  $mfenemy = array(
     "attack" => rand(1,4),
     "defend" => rand(1,4),
     "pos" => $r_pos_left,

     "sila" => $right_user[2],
     "lovk" => $right_user[3],
     "inta" => $right_user[4],

     "hp" => $right_user[5],
     "maxhp" => $right_user[5],

     "minu" => ($right_user[7]),
     "maxu" => ($right_user[8]),

     "mfkrit" => ($right_user[9]),
     "mfakrit" => ($right_user[10]),

     "mfuvorot" => ($right_user[11]),
     "mfauvorot" => ($right_user[12]),

     "bron1" => $right_user[13],
     "bron2" => $right_user[14],
     "bron3" => $right_user[15],
     "bron4" => $right_user[16]
  );


  $mfddemy = array();
  $mfddemy = $mf_rl_data;

 $mf = solve_mf($mfuser, $mfenemy, $mfddemy);



########################
# ���� �� ���
########################

   $fitting_razmen = function ($mf, $mfuser, $mfenemy, $mfddemy, $us_me = "me", $us_he = "he", $us_l = "l", $us_r = "r"){

                $mf_krit_def = 0.1;     # ���� � ����� ���������
                $mf_uvorot_def = 0.1;   # ���� � ����� ���������
                $mf_is_krit_max = 0.95; # ����������� ����������� = ���������
                $mf_is_uvor_max = 0.95; # ����������� ����������� = ���������
                $str_factor = 0.5;      # ������� �����
                $z9_factor = 1;         # ���� � �����

	            $uvorot_he  = $mf[$us_he]['uvorot'];
	            $krit_he    = $mf[$us_me]['krit'];
                $str        = $mf[$us_me]['str'];

                $con        = 0;
			    $uve        = 0;
                $check_next = true;

                if ($us_l == "l") { $l_login = "uleft";  }
                if ($us_l == "r") { $l_login = "uright"; }
                if ($us_r == "r") { $r_login = "uright"; }
                if ($us_r == "l") { $r_login = "uleft";  }

                if (float_rand(0, $uvorot_he) > $mf_is_uvor_max) $is_uvor = 1; else $is_uvor = 0;
                if (float_rand(0, $krit_he)   > $mf_is_krit_max) $is_krit = 1; else $is_krit = 0;

             # ������
                if($check_next && ($is_uvor/* || rand(1, 100) <= 5*/)){
                  $uve = 1;
                  $check_next = false;
                }

             # ����
                if($check_next && ($is_krit/* || rand(1, 100) <= 5*/)){
                  $uve = 2;
                  $check_next = false;
                }

                       if ($uve == 1) {
							if($con != 1){
							        $mfddemy[$us_r.'_count_uvor'] += 1;
                                    $mfddemy[$us_r."_pw"]         -= round(sqrt($mfenemy["sila"]) * 0.5);
                                    $mf[$us_me]['udar']            = 0;

                                    $mfddemy['log'] .= $l_login." ������ �� ".$r_login." ��������� ".$str." 2br";

							        # � ��������� "$mfenemy + 1 ������"
                                    #----------------------------------
							} else { }
						} elseif($uve == 2)
                        { # START ���� ���������

                            $mfddemy[$us_l."_pw"] -= round(sqrt($mfuser["sila"]) * 0.5);

						        /*if(get_block ($us_me, $mfenemy['attack'], $mfuser['defend'], 0))
							    {    ��������� ����
                                    $mfddemy[$us_r.'_count_block'] += 1;
                                    $newudar = floor($mf[$us_me]['udar']);
                                    if ($mfddemy[$us_r.'_hp'] < $newudar) {   $newudar = $mfddemy[$us_r.'_hp']; }

							    } else*/ {
                                    $newudar = floor($mf[$us_me]['udar'] * 2);
                                   # if ($mfddemy[$us_r.'_hp'] - $newudar <= 0) { $newudar = $mfddemy[$us_r.'_hp']; }
                                }

                                    $mfddemy[$us_r.'_hp']         -= $newudar;
                                    $mfddemy[$us_l.'_count_udar'] += 1;
                                    $mfddemy[$us_l.'_count_uron'] += $newudar;

                                    $mfddemy[$us_l.'_count_krit'] += 1;

                                    $mfddemy['log'] .= $r_login." ������ �� ".$l_login." ����� ���� -".$newudar." [".$mfddemy[$us_r.'_hp']."/".$mfddemy[$us_r.'_maxhp']."] ".$str." 2br";

                                    # ���� ��������� " $mfenemy + 1 ����"
                                    #----------------------------------

                          # END ���� ���������
                        } else/*if(!get_block ($us_me, $mfenemy['attack'], $mfuser['defend'], 0))*/
                        { # START ��������� ����� ���� ����

                                    $mfddemy[$us_l."_pw"] -= round(sqrt($mfuser["sila"]) * 0.5);

                                    $newudar = floor($mf[$us_me]['udar']);
                                   # if ($mfddemy[$us_r.'_hp'] < $newudar) { $newudar = $mfddemy[$us_r.'_hp']; }

                                    $mfddemy[$us_l.'_count_udar'] += 1;
                                    $mfddemy[$us_r.'_hp']         -= $newudar;
                                    $mfddemy[$us_l.'_count_uron'] += $newudar;

                                    $mfddemy['log'] .= $r_login." ������ �� ".$l_login." ����� ���� -".$newudar." [".$mfddemy[$us_r.'_hp']."/".$mfddemy[$us_r.'_maxhp']."] ".$str." 2br";

                                    # ���� ������� " $mfenemy + 1 ����"
                                    #----------------------------------

                          # END ��������� ����� ���� ����
						}/* else
                        { # START ��������� ������

                                    $mfddemy[$us_r.'_count_block'] += 1;
                                    # � �������� ���� " $mfuser + 1 ����"
                                    #----------------------------------
                                    $mfddemy[$us_l."_pw"] -= round(sqrt($mfuser["sila"]) * 0.5);
                          # END ��������� ������
						}*/
        return array($mf, $mfddemy);
   };


  $mfd = $fitting_razmen($mf, $mfuser, $mfenemy, $mfddemy, "me", "he", "l", "r");
  $mf      = $mfd[0];
  $mfddemy = $mfd[1];


  $mfd = $fitting_razmen($mf, $mfenemy, $mfuser, $mfddemy, "he", "me", "r", "l");
  $mf      = $mfd[0];
  $mfddemy = $mfd[1];
  $mfddemy['log'] .= "---------------------------------------------------------- 2br";


/*
echo"<pre>";
print_r($mfuser);
echo"<pre>";
print_r($mfenemy);
die();*/


  if ($mfddemy["l_pw"] <1 ) {$mfddemy["l_pw"] = 0;}
  if ($mfddemy["r_pw"] <1 ) {$mfddemy["r_pw"] = 0;}

 return $mfddemy;
 }

?>