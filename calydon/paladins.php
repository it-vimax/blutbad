<?php
	session_start();
	if ($_SESSION['uid'] == null) header("Location: index.php");
	include "connect.php";
	include "functions.php";

    if ($user['guild'] == 'Paladins') {
    	include "functions/guild/paladins.php";
    } elseif ($user['guild'] == 'Assassins') {
    	include "functions/guild/assassins.php";
    } else {
      header("Location: map.php");
      die();
    }
?>      