<?
  session_start();

  if (empty($_SESSION['uid'])) die();

  if (isset($_POST['is_ajax'])) {
    header('Content-type: application/json');
    include $_SERVER["DOCUMENT_ROOT"]."/connect.php";
    include $_SERVER["DOCUMENT_ROOT"]."/m_game_function.php";

    # ���� �������� �� ����������
    $user_id  = CP1251_to_UTF8($_POST['user_id']?$_POST['user_id']:$_GET['user_id'], false);
    $user_hach  = CP1251_to_UTF8($_POST['user_hach']?$_POST['user_hach']:$_GET['user_hach'], false);

    if ($user_id && $user_hach) {
      $user = sql_row("SELECT * FROM `users` WHERE `id` = '".$user_id."' AND `hach` = '".$user_hach."' LIMIT 1;");
    }
    if (isset($user['id'])) {
       $_SESSION['uid'] = $user['id'];
     # �������� �����
       include($_SERVER["DOCUMENT_ROOT"]."/functions/classes/user.control.class.php");
       $user_my = new UserControl($user);
     # ���������� ���������
       $user = $user_my->user;
    } else {
       include $_SERVER["DOCUMENT_ROOT"]."/m_user_function.php";
    }

    include $_SERVER["DOCUMENT_ROOT"]."/functions/battle/battle.class.php";
  }

/***------------------------------------------
 * �������� �����
 **/

  function view_user_inter_block($data_user, $pas = 0, $battle = 0, $me = 0, $show_pr = 0, $userclass = 0) {

      global $user_my, $rooms, $fbattle, $is_inf, $is_battle, $is_map, $is_main, $is_inventory, $is_npc;

      $view_user_inter_block = '';

      if ($data_user) {
        $isbots_stats = 0;
        $isbot = 0;

       # ���������� �� � �����
         $userpr = $user_my->id_to_user($data_user);

          if ($userpr['bot_id'] != $user_my->user_id) {
            $user_pr = new UserControl($userpr);
            $user_pr->del_cur_mysql = false;

          # ���� ��� �� ��������� ��������� ������
            if (!empty($userpr['bot_id']))  $user_pr->file_inventory_data = $_SERVER["DOCUMENT_ROOT"]."/adata/inventory_data/".$userpr['bot_id'].".txt";
          } else {
            $user_pr = $user_my;
            $user_pr->user_id       = $userpr['id'];
            $user_pr->user['login'] = $userpr['login'];
            $user_pr->user['hp']    = $userpr['hp'];
            $user_pr->user['mana']  = $userpr['mana'];
          }

       # ������ ��� ��� ��� ������� ����
         $isbot = $user_pr->user['bot']?1:0;

       # ������ ���� ��� ���� ��� ��� ������� ��� ������� ������
         if ($user_pr->user['bothidro']) { $isbot = 0; }
         if ($user_pr->user['invis'] || ($userpr['bot'] == 1 && substr($user_pr->user['login'], 0, 4) == '����')) { $isbot = 1; $user_pr->user['shadow'] = 'invisible_new.jpg'; }

       # ���������� ������� �����
         $ubigshadow = 1;

        # ��� ������ ������� ����� ��������
          $tmp_id = $_SESSION['uid'];
          $user_hach  = CP1251_to_UTF8($_POST['user_hach']?$_POST['user_hach']:$_GET['user_hach'], false);
          if($user_hach == $user_pr->user['hach']) $tmp_id = $user_pr->user_id;
          if ($user_pr->user_id == $tmp_id && ($is_main || $is_map || $is_inventory || $is_battle)) {
            if (substr_count($user_pr->user['shadow'], "big_") || get_ismax_obraz($user_pr->user)) {
              $user_pr->user['shadow'] = "0_0_".($user_pr->user['sex']?"M":"F")."000.jpg";
              $ubigshadow = 0;
            }
          }

    #======================================
    # ���� ����� ��������������
      if ((substr_count($user_pr->user['shadow'], "big_") || get_ismax_obraz($user_pr->user))) $isbot = 1;

    #======================================
    # ���� ��� �� �� ��������� ������
      if ($isbot == 0) $user_pr->USER_load_inventory_data();

    #======================================
    # ���� ��� �� �� ��������� ������
      if (($isbot && !$user_pr->user['bothran'])) {
         $vhp = ' title="������� �����: ??/??"';
         $vpw = ' title="������� ������������: ??/??"';
      }

    #======================================
    # ��������� ���� ��� ����
      $view_user_inter_block .= '<div class="nickname" id="leftnickname"></div>';

        $view_user_inter_block .= '<div class="inv-container">
		<div id="inv-overlay">&nbsp;</div>
    	<div class="hp-pw"'.( ($battle > 0)? ' id="hp-pw-'.$user_pr->user_id.'"':'').'>
    		<div class="hp-tooltip tooltip"'.$vhp.'>';
            $curhp = ($user_pr->user['hp'] && $user_pr->user['maxhp']) ? ($user_pr->user['hp'] * 100 / $user_pr->user['maxhp']):"0";
            $curhp = $user_pr->user['invis']?100:$curhp;
            $colorhp = getBgImage($curhp);
            $view_user_inter_block .= '
    	    	<div class="hp" style="background-image: url(http://img.blutbad.ru/i/green'.$colorhp.'.gif); width: '.$curhp.'%;"><!-- --></div>
    		</div>
    		<div class="pw-tooltip tooltip"'.$vpw.'>';
            $curmana = ($user_pr->user['mana'] && $user_pr->user['maxmana'])?($user_pr->user['mana'] * 100 / $user_pr->user['maxmana']):"0";
            $curmana = $user_pr->user['invis']?100:$curmana;
            $colormana = getBgImage($curmana);
            $view_user_inter_block .= '
    	    	<div class="pw" style="background-image: url(http://img.blutbad.ru/i/blue'.$colormana.'.gif); width: '.$curmana.'%;"><!-- --></div>
    		</div>
    	</div>';

        $view_user_inter_block .= '<ul class="inv-items'.(($isbot)? " inv-items-full-size" : "").''.(($is_inf or $is_battle or $battle)? " inv-items-no-gems" : "").'">';

       # ����� ���������
         if ($user_pr->user['invis']) $bavaname = '<i>���������</i>'; else $bavaname = '<b>'.$user_pr->user['login'].'</b> ['.$user_pr->user['level'].']';

        $view_user_inter_block .= '<li class="avatar tooltip" title="'.$bavaname.'"><a href="http://enc.blutbad.ru/obraz/'.$user_pr->user['shadow'].'.html" target="_blank"><img alt="������" src="http://img.blutbad.ru/i/obraz/'.$user_pr->user['shadow'].'"></a></li>';

        # ���������� ��������
          if (!$isbot) {
              # ���������� ��� ����
                $return = true;
                include("functions/classes/view_user_block_view_allitems.class.php");
                $view_user_inter_block .= $view_allitems;
            }

        $view_user_inter_block .= '</ul></div>';

        if($user_pr->user['hp'] > 0) {

          $user_pr_sila  = $user_pr->user['sila'] + $user_pr->user['inv_strength'];
          $user_pr_lovk  = $user_pr->user['lovk'] + $user_pr->user['inv_dexterity'];
          $user_pr_inta  = $user_pr->user['inta'] + $user_pr->user['inv_success'];
          $user_pr_vinos = $user_pr->user['vinos'];
          $user_pr_intel = $user_pr->user['intel'] + $user_pr->user['inv_intelligence'];
          $user_pr_mudra = $user_pr->user['mudra'];

          if ($isbot || $user_pr->user['invis']) {
            $user_pr_sila  = '??';
            $user_pr_lovk  = '??';
            $user_pr_inta  = '??';
            $user_pr_vinos = '??';
            $user_pr_intel = '??';
            $user_pr_mudra = '??';
          }

          $view_user_inter_block .= '
          	<table class="inv-states">
          		<thead>
          			<tr>
          				<th colspan="2">��������������</th>
          			</tr>
          		</thead>
          		<tbody>
          			<tr>
          				<td class="name">����:</td>
          				<td class="value">'.$user_pr_sila.'</td>
          			</tr>
          			<tr>
          				<td class="name lk">��������:</td>
          				<td class="value lk">'.$user_pr_lovk.'</td>
          			</tr>
          			<tr>
          				<td class="name it">��������:</td>
          				<td class="value it">'.$user_pr_inta.'</td>
          			</tr>
          			<tr>
          				<td class="name vs">����������������:</td>
          				<td class="value vs">'.$user_pr_vinos.'</td>
          			</tr>';
                    if ($user_pr->user['invis'] == 0) {
                     if ($user_pr->user['intel'] > 0) {
                      $view_user_inter_block .= '
          			<tr>
          				<td class="name ia">���������:</td>
          				<td class="value ia">'.$user_pr_intel.'</td>
          			</tr>';
                     }
                     if (($user_pr->user['level'] >= 10) && ($user_pr_mudra > 0)) {
                      $view_user_inter_block .= '
          			<tr>
          				<td class="name ma">��������:</td>
          				<td class="value ma">'.$user_pr_mudra.'</td>
          			</tr>';
                     }
                   }
                  $view_user_inter_block .= '
          		</tbody>
          	</table>';
        }

  # �������� ��������
  //  $eff_show = $cur_eff_all($user_pr->user['eff_all']);

  }


# ���� ��� �� �� ��������� ������ �� ��������� �����
  if ($data_user > _BOTSEPARATOR_ || (!empty($data_user['id']) && $data_user['id'] > _BOTSEPARATOR_) ) {
    $user_pr->mod_inventory_data = false;
  }

    return $view_user_inter_block;
  }

/***------------------------------------------
 * ����
 **/

if (isset($_POST['is_ajax']) && $_POST['cmd'] == 'bturn') {

  $D1     = $_POST['D1'];
  $D2     = $_POST['D2'];
  $D3     = $_POST['D3'];
  $D4     = $_POST['D4'];

  $A1     = $_POST['A1'];
  $A2     = $_POST['A2'];
  $A3     = $_POST['A3'];
  $A4     = $_POST['A4'];

  $pos    = $_POST['pos'];

  $enemy  = $_POST['enemy'];
  $attack = $_POST['attack'];
  $defend = $_POST['defend'];

  if ($enemy) { }

  $_POST['act'] = 'bturn';
  $_POST['cmd'] = 'bupd';
}

/***------------------------------------------
 * �������� ���
 **/

if (isset($_POST['is_ajax']) && $_POST['cmd'] == 'bupd') {

     $json_error = "";   # ��� ���������
     $balluron       = 0;
     $ballusdeathn   = 0;
     $btimeout       = 0;
     $blogtext       = '';
     $enemy_id       = abs($_POST['enemy_id']);
     $bshow_att_def  = $_POST['isbot'] ? false : true;

     $bb_t1list      = array();
     $bb_t2list      = array();
     $bcount_user_left  = '0,0';
     $bcount_user_right = '0,0';
     $bpriem          = '';
     $bpriem_affected = '';

     $left_user      = array();
     $right_user     = array();
     $left_block     = array();
     $right_block    = array();
     $right_block_id = 0;
     $btoptools      = '';
     $bstatus        = -1;
     $ismyend        = 0;       # 1 = ����� ����� ��������

     $isbot          = abs($_POST['isbot']);
     $battle_id      = abs($_POST['battle_id']);
     $user_id        = $user_my->user_id;
     $user_hp        = $user_my->user['hp'];

  	 $fbattle        = array();
  	 $fbattle        = @new fbattle($battle_id);

     $btimeout       = $fbattle->battle_data['timeout'];

    # ���� ��� ��� �������� �� ������� ������� ���� �� �������
     if ($user_my->user['battle'] == 0 && !empty($fbattle)) {
       $balluron = abs($fbattle->get_points('damage', $user_my->user_id));
       $ballusdeath = abs($fbattle->get_points('usdeath', $user_my->user_id));
     }

     if ($user_my->user['battle']) {

        $bb_t1list   = $fbattle->t1list;
        $bb_t2list   = $fbattle->t2list;

      #=====================
      # ������ ���������
        $life1 = 0;
        foreach ($bb_t1list as $k_id => $v_data) {
         if ($v_data['h'] > 0) {
           if ($k_id == $user_my->user_id) { $balluron = $bb_t1list[$k_id]['damage'];  $ballusdeath = $bb_t1list[$k_id]['usdeath']; }
           if ($bb_t1list[$k_id]['i'] == 1) {
             if ($k_id != $user_my->user_id) {
                $bb_t1list[$k_id]  = array('id' => $k_id, 'i' => 1, 'tC' => 1);
             }
           } else {
             $bb_t1list[$k_id]['n']  = CP1251_to_UTF8($bb_t1list[$k_id]['n']);
             $bb_t1list[$k_id]['km'] = str_to_spacelower($bb_t1list[$k_id]['k']);
              foreach (array('wep_type', 'shit', 'exp', 'damage', 'karma', 'valor') as $k => $v) unset($bb_t1list[$k_id][$v]);
           }
            if ($k_id == $user_id) $left_user = $bb_t1list[$k_id];
            if ($k_id == $fbattle->enemy) { $right_user = $bb_t1list[$k_id]; }
            $life1++;
         } else {
           unset($bb_t1list[$k_id]);
         }
        }
        $life2 = 0;
        foreach ($bb_t2list as $k_id => $v_data) {
         if ($v_data['h'] > 0) {
           if ($k_id == $user_my->user_id) {$balluron = $bb_t2list[$k_id]['damage']; $ballusdeath = $bb_t2list[$k_id]['usdeath']; }
           if ($bb_t2list[$k_id]['i'] == 1) {
             if ($k_id != $user_my->user_id) {
                $bb_t2list[$k_id]  = array('id' => $k_id, 'i' => 1, 'tC' => 2);
             }
           } else {
             $bb_t2list[$k_id]['n']  = CP1251_to_UTF8($bb_t2list[$k_id]['n']);
             $bb_t2list[$k_id]['km'] = str_to_spacelower($bb_t2list[$k_id]['k']);
             foreach (array('wep_type', 'shit', 'exp', 'damage', 'karma', 'valor') as $k => $v) unset($bb_t2list[$k_id][$v]);
           }
            if ($k_id == $user_id) $left_user = $bb_t2list[$k_id];
            if ($k_id == $fbattle->enemy) { $right_user = $bb_t2list[$k_id]; }
            $life2++;
         } else {
           unset($bb_t2list[$k_id]);
         }
       }

     if ($fbattle->battle_data['win'] == 3) {
       $bcount_user_left  = $life1.','.count($fbattle->t1list);
       $bcount_user_right = $life2.','.count($fbattle->t2list);
     }

        if ($fbattle->return == 1) {

         $btoptools .= '<form action="fbattle.php" method="post" name="bform" id="bform" onKeyUp="set_action();">
                    <input type=hidden value="'.$user_my->user['battle'].'" name=batl>
                    <input type=hidden name="myid" value="'.$user_my->user['id'].'">
                    <input name="cmd" type="hidden" value="turn">
                    <input name="r" type="hidden" value="'.time().'">
                    <input name="to" type="hidden" value="'.$fbattle->enemy.'">
                    <img alt="" class="header" height="15" src="http://img.blutbad.ru/i/battle/battle_form_'.($bshow_att_def == true ? "ad" : "mob").'.gif" width="304">
                    <table class="battle-controls block '.(($bshow_att_def == true)? "battle-controls-main " : "").'" style="font-size: 12px; "><tbody><tr>';
                     if ($bshow_att_def == true) {
                      $btoptools .= '<td class="attack-col"><ul class="checkbox-list radio-list">
    					<li>
                            <span class="hit-stat"><!-- --></span>
    						<span class="container">
    							<input class="checkbox" onclick="checkAttack(this);" id="A1" name="A1" type="checkbox" value="1" >
    							<label class="" for="A1">���� � ������</label>
    						</span>
    					</li>
    					<li>
                            <span class="hit-stat"><!-- --></span>
    						<span class="container">
    							<input class="checkbox" onclick="checkAttack(this);" id="A2" name="A2" type="checkbox" value="1" >
    							<label class="" for="A2">���� � ������</label>
    						</span>
    					</li>
    					<li>
                            <span class="hit-stat"><!-- --></span>
    						<span class="container">
    							<input class="checkbox" onclick="checkAttack(this);" id="A3" name="A3" type="checkbox" value="1" >
    							<label class="" for="A3">���� � ������</label>
    						</span>
    					</li>
    					<li>
                            <span class="hit-stat"><!-- --></span>
    						<span class="container">
    							<input class="checkbox" onclick="checkAttack(this);" id="A4" name="A4" type="checkbox" value="1" >
    							<label class="" for="A4">���� � ������</label>
    						</span>
    					</li>
    					</ul></td>';
                     }
                     $btoptools .= '
          					<td class="center-col"><ul><li><img alt="��������" class="disabled" id="buttonRefresh" src="http://img.blutbad.ru/i/battle/b_refresh.png" title="��������"></li><li>';
                                        if (count($fbattle->t1) + count($fbattle->t2) > 10) {
          								    $btoptools .= '<img alt="" class="disabled" onclick="location.href=\'?cmd=change&nd='.$fbattle->user['nd'].'\'; " src="http://img.blutbad.ru/i/battle/b_change.gif" title="��������� ���� ����������: 0">';
                                        } else {
                                            $btoptools .= '&nbsp;';
                                        }
          							    $btoptools .= '</li><li>';
                                        if($user_my->user['bot_battle_panel'] == 1) {
             								$btoptools .= '<img alt="" id="fight" onclick="document.forms[\'bform\'].submit();" src="http://img.blutbad.ru/i/battle/b_attack.png" title="������!">';
                                        } else {
             								$btoptools .= '<img alt="" id="fight" onclick="var err = checkForm(1); document.getElementById(\'msgbox\').style.display = err ? \'block\':\'none\'; if (!err) document.forms[\'bform\'].submit();" src="http://img.blutbad.ru/i/battle/b_attack.png" title="������!">';
                                        }
                                      $btoptools .= '</li></ul></td>';

                     if ($bshow_att_def == true) {
                     $btoptools .= '
          					<td class="defence-col">
          						<ul class="checkbox-list radio-list">
          							<li>
                                      	<span class="hit-stat"><!-- --></span>
          								<span class="container">
          									<input class="checkbox" onclick="checkDefend(this);" id="D1" name="D1" type="checkbox" value="1" >
          									<label class="" for="D1">���� ������</label>
          								</span>
          							</li>
          							<li>
                                      	<span class="hit-stat"><!-- --></span>
          								<span class="container">
          									<input class="checkbox" onclick="checkDefend(this);" id="D2" name="D2" type="checkbox" value="2" >
          									<label class="" for="D2">���� �������</label>
          								</span>
          							</li>
          							<li>
                                      	<span class="hit-stat"><!-- --></span>
          								<span class="container">
          									<input class="checkbox" onclick="checkDefend(this);" id="D3" name="D3" type="checkbox" value="3">
          									<label class="" for="D3">���� �����</label>
          								</span>
          							</li>
          							<li>
                                      	<span class="hit-stat"><!-- --></span>
          								<span class="container">
          									<input class="checkbox" onclick="checkDefend(this);" id="D4" name="D4" type="checkbox" value="4">
          									<label class="" for="D4">���� ���</label>
          								</span>
          							</li>
          						</ul>
          					</td>';
                     }

                     $btoptools .= '</tr></tbody></table>';

                      $btoptools .= '<img alt="" class="header" height="15" src="http://img.blutbad.ru/i/battle/battle_form_pos.gif" width="304">
                      <table class="battle-controls battle-controls-position block"><tbody><tr>
              			<td class="attack-col"><ul class="radio-list"><li class="position">
    						<span class="hit-stat"><!-- --></span>
    						 <span class="container">
    							<input class="radio" id="A5" name="pos" onclick="checkSubmit(this.form);" type="radio" value="1" '.(($fbattle->b_hint_nbp[$fbattle->user['id']][$fbattle->enemy]['msp']==1)?"checked=\"checked\"":"").' ">
    							<label class="" for="A5">���������</label>
    						</span>
              			</li></ul></td>
              			<td class="center-col"></td><td class="defence-col"><ul class="radio-list"><li class="position">
      						<span class="hit-stat"><!-- --></span>
      						<span class="container">
      							<input class="radio" id="D5" name="pos" onclick="checkSubmit(this.form);" type="radio" value="2" '.(($fbattle->b_hint_nbp[$fbattle->user['id']][$fbattle->enemy]['msp']==2 || $fbattle->b_hint_nbp[$fbattle->user['id']][$fbattle->enemy]['msp']==0)?"checked=\"checked\"":"").' ">
      							<label class="" for="D5">��������</label>
      						</span>
              		   </li></ul></td>
                      </tr></tbody></table>';

                    $btoptools .= '<div class="top-error" id="msgbox" style="'.(!empty($_SESSION['battle_hint'])?"":"display: none;").'"><fieldset><legend>��������!</legend><div class="error-content" id="error">'.(@$_SESSION['battle_hint']).'</div></fieldset></div>';
                    $_SESSION['battle_hint'] = '';

                       if ($bshow_att_def == false) {
                         $a_attack = rand(1, 4);
                         $a_defend = rand(1, 6);
                       } else {
                         $a_attack = 0;
                         $a_defend = 0;
                       }

                    $btoptools .= '
                    <input type="hidden" name="attack" value="'.$a_attack.'" id="attack">
                    <input type="hidden" name="defend" value="'.$a_defend.'" id="defend">
                    <input type="hidden" name="enemy" value="'.$fbattle->enemy.'" id="enemy">
                    </form>';
        } else if ($fbattle->return == 2) {
           if($user_my->user['hp'] <= 0 && $fbattle->battle && !in_array($user_my->user['id'], $fbattle->texit)) {
            $ismyend = 1;
           }
        }

       $bstatus = $fbattle->return;
     }


     if ($fbattle->battle_data['win'] != 3) {

/***------------------------------------------
 * ��������� ���
 **/

                if(empty($enemy) || !$user_my->user['battle']) {
                 # ��������� �� ��������
                   if(empty($fbattle->battle) || !$user_my->user['battle']) {
                	  $data_battle = $fbattle->battle_data;

                      $viktory = 0;
                      $viktory_nich = 0;

                    # ������ ������ ��� ��� ����� �� ���
                      $texit = explode(";", $data_battle['texit']);

                    # ������ ������ �����
                      if (!empty($fbattle->t1list[$user_my->user['id']])) {
                        $tlist = $fbattle->t1list[$user_my->user['id']];
                      } elseif (!empty($fbattle->t2list[$user_my->user['id']])) {
                        $tlist = $fbattle->t2list[$user_my->user['id']];
                      }

                    # ������ ������ ������� �����������
                      if ($data_battle['win'] == 1)       { $winlist = $fbattle->t1;
                      } elseif ($data_battle['win'] == 2) { $winlist = $fbattle->t2; }

                      if (($data_battle['win'] == 1) || ($data_battle['win'] == 2)) {
                         if (in_array ($user_my->user['id'], $winlist)) { $viktory = 1; }
                      }
                      if ($data_battle['win'] == 0) {
                          $viktory = 0;
                          $viktory_nich = 1;
                      }

                 # ����� � �����
                   $battle_exp_percent = empty($fbattle->tools['exp_percent'])?0:$fbattle->tools['exp_percent'];

                   $btoptools .= '<div id="finish">';
                     if (!empty($fbattle->texit) && in_array($user_my->user['id'], $fbattle->texit)) {
                         $btoptools .= '<b>��� ��������. �� ����� �� ���.</b><br><br><input type=hidden value="1" name="end">
                         <input type=submit value="���������" class="xbbutton" name="end" onclick="location.href=\'/'.getisroom().'.php?after_battle=1&win_flag=2\';"><br>';
                     } else {
                       if ($viktory_nich) {
                             $btoptools .= '<b>��� ��������. �����.</b><br>����� ���� �������� �����: <b>'.abs(floor($tlist['damage'])).'</b>. <br>�������� �����: <b>0</b>. �����: <b>'.number_format($tlist['karma'],2,"."," ").'</b>. ��������: <b>'.number_format($tlist['valor'],2,"."," ").'</b>.
                             <br><input type=submit value="���������" class="xbbutton" name="end" onclick="location.href=\'/'.getisroom().'.php?after_battle=1&win_flag=2\';">';
                       } else { if ($viktory) {
                             $btoptools .= '<b>��� ��������. �� ��������.</b><br>����� ���� �������� �����: <b>'.abs(floor($tlist['damage'])).'</b>. <br>�������� �����: <b>'.abs(floor($tlist['exp'])).' ('.(percent_exp($user_my->user, $data_battle) + $battle_exp_percent).'%)</b> �����: <b>'.number_format($tlist['karma'],2,"."," ").'</b>. ��������: <b>'.number_format($tlist['valor'],2,"."," ").'</b>.
                             <br><input type=submit value="���������" class="xbbutton" name="end" onclick="location.href=\'/'.getisroom().'.php?after_battle=1&win_flag=2\';">';
                        } else {
                             $btoptools .= '<b>��� ��������. �� ���������.</b><br>����� ���� �������� �����: <b>'.abs(floor($tlist['damage'])).'</b>. <br>�������� �����: <b>0</b>. �����: <b>0.00</b>. ��������: <b>'.number_format($tlist['valor'],2,"."," ").'</b>.
                             <br><input type=submit value="���������" class="xbbutton" name="end" onclick="location.href=\'/'.getisroom().'.php?after_battle=1&win_flag=2\';">';
                         }
                       }
                     }
                   $btoptools .= '</div>';
                   }
                }

     }

      #=====================
      # ��� ���
        $log_size = filesize($_SERVER["DOCUMENT_ROOT"]."/backup/logs/battle".$battle_id.".txt");
    	$log_file = fopen($_SERVER["DOCUMENT_ROOT"]."/backup/logs/battle".$battle_id.".txt", "r");
    	fseek($log_file, -4256, SEEK_END);
    	$log[0] = fread($log_file, 4256);
    	fclose($log_file);

    	$log = explode("<BR>",$log[0]);
    	$ic  = count($log) - 2;

    	if ($log_size >= 4256) { $max = 1; } else { $max = 0; }

    	for($i = $ic; $i >= 0 + $max; --$i) {
            if(preg_match("/<hr>/i", $log[$i])){
    		   $log[$i] = str_replace("<hr>", "", $log[$i]);
    		   $log[$i] = $log[$i]."<br><img alt class=separator height=8 src=http://img.blutbad.ru/i/battle/hr2.gif>";
            }

            if (substr_count($log[$i], $user_id.";")) $log[$i] = str_replace("<span class=\"date\">", "<span class=\"date3\">", $log[$i]);
            $log[$i] = preg_replace("!<uid>(.*?)</uid>!si", "", $log[$i]);

            if(preg_match("/<hr>/i", $log[$i])) $blogtext .= $log[$i]; else $blogtext .= str_replace('<hr>', writeseparator(), $log[$i])."<BR>";
    	}

        unset($ic);
        unset($log);

      #=====================
      # ������ ����� ���� ���
        if($fbattle->return == 1){
            $right_block = view_user_inter_block($fbattle->enemy,1,1,1,0, $fbattle->userclass($fbattle->enemy, ''));
            $right_block_id = 0;
        } else {
        	if(in_array($user_my->user_id, $fbattle->texit)) $right_block_id = 1; # ����� �� ���
            elseif($viktory_nich && empty($fbattle->battle)) $right_block_id = 2; # �����
        	elseif($viktory)                                 $right_block_id = 3; # ������
        	elseif ($fbattle->return > 1 && $fbattle->battle_data['win'] == 3) $right_block_id = 4; # ������� ���� ����������
        	elseif (empty($fbattle->battle) && (!$viktory))  $right_block_id = 5; # ���������
        	else $right_block_id = 4; # ������� ���� ����������
        }

      #=====================
      # ������
        $actions = unserialize($user_my->user['actions']);
        if (!empty($actions)) {
           foreach ($actions as $k => $row) {
               if ($row['priem']) {
                 if ($row['target'])  $onclick = "onclick=\"openDialogWindow('��� ���������',{ name:'�����' }, { cmd:'priem.target', act:'".$row['priem']."', school:'".$row['school']."', nd:'".$user_my->user['nd']."', user_id:'".$user_my->user['id']."', user_hach:'".$user_my->user['hach']."' }, { action:'?' } ); return false;\"";
                 else $onclick = "onclick=\"location.href='?cmd=priem&act=".$row['priem']."&school=".$row['school']."&nd=".$user_my->user['nd']."&user_id=".$user_my->user['id']."&user_hach=".$user_my->user['hach']."';\"";

                 $bpriem .= '
            	   <li>
            		<img style="'.(is_activete_priem($user_my, $fbattle, $row)?"cursor: pointer;":"opacity: 0.5;").'" alt="" class="tooltip-next" '.((is_activete_priem($user_my, $fbattle, $row))?$onclick:"").' src="http://img.blutbad.ru/i/perks/'.$row['priem'].'.gif">
            		<div class="invisible">
            			<div class="item description">
            				<b class="title">'.$row['name'].'</b>
            				<ul class="properties">'.get_priem_requirements($row).'</ul>
            				<b class="title">��������</b>
            				<ul class="properties">'.$row['actopisan'].'</ul>
            			</div>
            		</div>
            	   </li>';
             }
          }
        }

      #=====================
      # �������� ������
        if (!empty($fbattle->battle_eff[$user_my->user['id']])) {
          foreach ($fbattle->battle_eff[$user_my->user['id']] as $priem => $act_priem) {
             $bpriem_affected .= '<li style="float: left;"><img class="tooltip" alt="'.$act_priem['name'].'" title="'.$act_priem['name'].'<br>�����: '.$act_priem['hod'].'" src="http://img.blutbad.ru/i/perks/'.$priem.'.gif"></li>';
          }
          if ($user_my->user['battle'] && !empty($fbattle->battle_eff[$user_my->user['id']]['salvation'])) {
           $bpriem_affected .= '<li style="float: left;"><img class="tooltip" alt="������������ ��������" title="������������ ��������" src="http://img.blutbad.ru/i/rune/48.gif"></li>';
          }
        }

      #=====================
      # ���� ��� ��
        if (empty($left_user)) {
          $left_user = array();
          $left_user['id'] = $user_my->user_id;
          $left_user['h']  = $user_my->user['hp'];
          $left_user['mh'] = $user_my->user['maxhp'];
          $left_user['p']  = $user_my->user['mana'];
          $left_user['mp'] = $user_my->user['maxmana'];
        }

        print json_encode($result = array(
           'success'           => true,
           'error'             => CP1251_to_UTF8($json_error),
           'balluron'          => $balluron,
           'ballusdeath'       => $ballusdeath,
           'btimeout'          => $btimeout,
           'bteam1'            => $bb_t1list,
           'bteam2'            => $bb_t2list,
           'blogtext'          => CP1251_to_UTF8($blogtext),
           'bpriem_hit'        => $user_my->user['str_mind'].','.$user_my->user['hit_hod'].','.$user_my->user['hit_udar'].','.$user_my->user['hit_uvorot'].','.$user_my->user['hit_krit'].','.$user_my->user['hit_block'],
           'bpriem'            => CP1251_to_UTF8($bpriem),
           'bpriem_affected'   => CP1251_to_UTF8($bpriem_affected),

           'bcount_user_left'  => $bcount_user_left,
           'bcount_user_right' => $bcount_user_right,
           'left_user'         => $left_user,
           'right_user'        => $right_user,
           'left_block'        => CP1251_to_UTF8($left_block),
           'right_block'       => CP1251_to_UTF8($right_block),
           'right_block_id'    => $right_block_id,
           'btoptools'         => CP1251_to_UTF8($btoptools),
           'bstatus'           => $bstatus,   # 2=������� ���� ����������, 3= �������, btoptools
           'ismyend'           => $ismyend,
           'myhpless'          => ($user_hp > $fbattle->get_points('h', $user_id)?($user_hp - abs($fbattle->get_points('h', $user_id))):"0"),
           //'sql'               => str_replace('"', '', str_replace("'", '', $mysql_access_text)),

        ));

 die();
}

?>