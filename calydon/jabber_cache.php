<?php

class Cache {

	private static $redis;

	public static $keys;
	public static $data;

	public static function init() {
		self::$redis = new Redis();

	    self::$redis->connect('127.0.0.1', 6379);
        #self::$redis->auth('5nFxYqe4SE');             # alamut, calydon
	    # self::$redis->connect('148.251.1.170', 6379);
         self::$redis->auth('5g2rKdBZBa');           # damask

	    self::$keys = null;
	    self::$data = null;
	}

	public static function redis() {
		return self::$redis;
	}

	public static function addXml( $xml ) {
//		self::$redis->rpush('xml', iconv('windows-1251', 'utf-8', $xml));
		self::$redis->rpush('xml', iconv('CP1251', 'UTF-8', $xml));
	}

	public static function getAll( $removeKeys = true ) {
		$all = self::$redis->hgetall('chat');

		if(is_array($all)) {
			foreach($all as $key => $value) {
				$keys[] = $key;
				$values[] = $value;
			}
		}

		self::$keys = $keys;

		if($removeKeys === true) self::removeKeys();

		if(is_array($values)) {
			foreach($values as $value) {
				$array = explode(',', iconv('utf-8', 'windows-1251', $value), 9);

				$trash['id'] = $array[0];
				$trash['cityID'] = $array[1];
				$trash['userID'] = $array[2];
				$trash['type'] = $array[3];
				$trash['roomID'] = $array[4];
				$trash['invisible'] = $array[5];
				$trash['login'] = $array[6];
				$trash['jabberLogin'] = $array[7];
				$trash['text'] = code_to_script($array[8]);

				$data[] = $trash;
			}

			self::$data = $data;
		}
	}

	public static function removeKeys( $array = null ) {
		if($array === null) $array = self::$keys;

		if(is_array($array)) {
			array_unshift($array, 'chat');

			return call_user_func_array([self::$redis, 'hdel'], $array);
		}
	}

	/*public static function redis() {
		return self::$redis;
	}*/

}

?>