<html><head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>YoBit.Net</title>
<meta name="robots" content="index, follow">
<meta name="keywords" content="">
<meta name="og:description" content="">
<meta name="description" content="">
<meta charset="utf-8">
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link rel="stylesheet" type="text/css" href="https://yobit.net/style.css?b981e2f1f36bb5caa9c25b0e07dc7013">
<script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/r20171025115245/recaptcha__ru.js"></script><script type="text/javascript" async="" src="https://mc.yandex.ru/metrika/watch.js"></script><script type="text/javascript">
  var locale = 'ru';
  var locale_chat = 'ru';
  var market_base = 'usd';
  var chat_rows_limit = '50';
  var chat_nick = 'blutbad';

  var popup_title_success = '�������!';
  var popup_title_error = '�������� ������';
  var popup_btn_cancel = '������';
  var popup_btn_close = '�������';

  var orders_types = {'1':'BUY', '2':'SELL'};

  var popup_msg_confirm_creating = '�� �������, ��� ������ ������� ��������� �����?';
  var popup_msg_creating_success = '����� ������ �������.';
  var popup_msg_confirm_closing = '�� ������������� ������ ������� �����?';
  var popup_msg_closing_success = '����� ������ �������.';

  var popup_btn_closeorder = '������� �����';
  var popup_btn_createorder = '������� �����';
  var noorders = '��� �������� �������.';
  var popup_btn_lottohistory = '������� �����';
  var popup_btn_dicerules = '������� Dice';
  var dice_rules = '�������� ����������� ������ � ������� Roll < 48 or Roll > 52.<br>�������� ��������� ���� � ������� �� �������� Dice.';
  var pdice_btn_creating = "������...";
  var pdice_btn_win = "�������!";
  var pdice_btn_lost = "��������";
  var pfreecoins_btn_getting = "�������...";
  var pfreecoins_btn_paid = "���������!";
  var pfreecoins_table_statuses_paid_once = "�����������";


  var popup_register_error_default = "�� �� ������ ������ ������������������. ���������� ������� ��� ��������� �� ������� ���������.";
  var popup_register_email_confirmed = "EMAIL �����������<br>��� email �����������.<br>�� �������� ��� <a href='/ru/settings/'>��������</a> ������������� ����������� � ������� ���������.";

  var popup_login_error_default = "�� �� ������ ����� ������. ���������� ������� ��� ��������� �� ������� ���������.";

  var popup_yobicodes_error_default = "�� �� ������ ������������ Yobit ��� ������. ���������� �������.";

</script>
<script type="text/javascript" src="https://yobit.net/js/jquery.js"></script>
<script type="text/javascript" src="https://yobit.net/js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="https://yobit.net/js/script.js?54c089c235db249125aae6a78c9470ae"></script>
<script type="text/javascript" src="https://yobit.net/js/jquery.tinyscrollbar.js"></script>
<script type="text/javascript" src="https://yobit.net/js/jquery.uniform.min.js"></script>
<script type="text/javascript" src="https://yobit.net/js/jstz.min.js"></script>
<script type="text/javascript" src="https://yobit.net/js/jswasort.min.js"></script>
<script type="text/javascript" src="https://yobit.net/js/corex.js?8b5bfb3f9f43d49fdf8d5abcce1bd2e0"></script>
<script type="text/javascript" src="https://yobit.net/js/autobahn.min.js"></script>
<script type="text/javascript" src="https://yobit.net/js/corews.js?66772fb6819fe363c7b883537dacc296"></script>
<script type="text/javascript" src="https://yobit.net/js/messi.min.js?2b87dda3d7f878f59cc16387f9a5853e"></script>
<link rel="stylesheet" href="https://yobit.net/css/messi.min.css?76f39a2fa1e4879d836e09d369a67a4f" type="text/css">
<script type="text/javascript" src="https://yobit.net/js/jquery.jas.min.js"></script>
<script type="text/javascript" src="https://yobit.net/js/jquery.toast.js"></script>
<link rel="stylesheet" href="https://yobit.net/css/jquery.toast.css">
<script type="text/javascript" src="https://yobit.net/js/charts.js"></script>
<script type="text/javascript" src="https://yobit.net/js/soundmanager2-nodebug-jsmin.js"></script>
<script type="text/javascript" src="https://yobit.net/js/jquery.pjax.js"></script>
<link rel="icon" type="image/x-icon" href="/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="https://yobit.net/favicon.ico">
<script src="https://www.google.com/recaptcha/api.js?onload=reCaptchaCallback&amp;render=explicit&amp;hl=ru" async="" defer=""></script>
<script>
    var reCaptchaCallback = function() {
        var elements = document.getElementsByClassName('g-recaptcha');
        for (var i = 0; i < elements.length; i++) {
            var id = elements[i].getAttribute('id');
            var capid = grecaptcha.render(id, {
                'sitekey' : '6LcVxQsTAAAAAA9PM3osPv40xmVMNF7G1YkAgOxM'
            });
            elements[i].setAttribute('capid', capid);
        }
    };
</script>
</head>
<body>
<input type="hidden" id="csrf_token" value="a3d17612aa6b73f23a0b48b0a168d30812ff3f3c855562eb12da14e00811b800adb3169ba36b61ba9edb23bf898e0da3d68e614e4e4c453a69a4ce1affdd7392">
<div class="wrapper">
<link rel="stylesheet" type="text/css" href="https://yobit.net/css/jquery.dataTables.css">
<script type="text/javascript" src="https://yobit.net/js/jquery.dataTables.min.js?c7d793ba3702bb8e5d871dc9eae13c21"></script>
<link rel="stylesheet" type="text/css" href="https://yobit.net/css/jquery.jscrollpane.css">
<script type="text/javascript" src="https://yobit.net/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="https://yobit.net/js/jquery.mousewheel.js"></script>
<script type="text/javascript">

    var pmarket_table_zero_records = '������ �� ������� �� ��������� �������';
    var pmarket_table_search = '�����';
    var popup_title_waring = '��������!';
    var popup_btn_yes = '��';
    var popup_btn_cancel = '������';


  // these two vars for bug correction: dataTable's header width with skined jScrollPane
  var maded = 0;
  var madeh = 0;
  $(document).ready(function() {

    isMouseDown = false;
    $('body').mousedown(function() {
        isMouseDown = true;
    })
    .mouseup(function() {
        isMouseDown = false;
    });

     var mTable = $('#trade_market').dataTable({
        //"destroy": true,
        "bRetrieve": true,
        "dom": '<"search_box_trade2"<"button">f>t<"clear">',
        //"dom": 't<"clear">',
        "info": false,
        "paging": false,
        "language": {
            "zeroRecords": pmarket_table_zero_records,
            "search": '',
            "searchPlaceholder": pmarket_table_search
        },
        "ordering": false,
        "fnDrawCallback": function( oSettings ) {
    	        if(typeof $('.dataTables_scrollBody').data('jsp') === 'object')
                {
                  $('.dataTables_scrollBody').data('jsp').reinitialise();
                }
    	        if(typeof mTable === 'object')
                {
                  //mTable.columns.adjust();
                  if(!maded)
                  {
                    maded = 1;
                    mTable.fnAdjustColumnSizing();
                    madeh = 0;
                  }
                }

        },
        "fnHeaderCallback": function( nHead, aData, iStart, iEnd, aiDisplay ) {
          if(!madeh)
          {
            madeh = 1;
            maded = 0;
          }
        },
        "fnInitComplete": function() {
          //$('.dataTables_scrollBody').jScrollPane();

        }
        //,"sScrollY": calcDataTableHeight()
      });


      $('#coinfilter').on('keyup click', function(){
          $('.search_box_trade2 input[type=search]').val($('#coinfilter').val()).keyup();
      });

      $('#trade_market').on('search.dt', function () {
          var $scrollbar2 = $('#scrollbar2');
          $scrollbar2.tinyscrollbar();
          var scrollbar2 = $scrollbar2.data("plugin_tinyscrollbar");
          scrollbar2.update();
       }).dataTable();

      $(window).smartresize(function(){

         // var $scrollbar2 = $('#scrollbar2');
         // $scrollbar2.tinyscrollbar();
         // var scrollbar2 = $scrollbar2.data("plugin_tinyscrollbar");
         // scrollbar2.update();

        //$('.dataTables_scrollBody').data('jsp').reinitialise();
        //$('.dataTables_scrollBody').css('height',calcDataTableHeight());
        //mTable.fnAdjustColumnSizing();

      } );

  });

  var calcDataTableHeight = function() {
    return $(window).height()-390; //var h = Math.floor($(window).height()*55/100);
  };


</script>
<link rel="stylesheet" type="text/css" href="https://yobit.net/css/jquery.dataTables.css">
<script type="text/javascript" src="https://yobit.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://yobit.net/css/jquery.jscrollpane.css">
<script type="text/javascript" src="https://yobit.net/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="https://yobit.net/js/jquery.mousewheel.js"></script>
<script type="text/javascript">

        var pyobicodes_error_default = "�� �� ������ ������� Yobit ��� ������. ���������� �������.";
        var check_only_mine = parseInt("1");
        var check_only_btc = parseInt("0");

        $(function () {
                $("#check_only_mine").uniform();
                $("#check_only_btc").uniform();
        });

        // these two vars for bug correction: dataTable's header width with skined jScrollPane
        var maded = 0;
        var madeh = 0;
        $(document).ready(function() {
	      prepareLeftAndRightPanels();
          //prepareChat();
          prepareDice();
          //chatStart();
          diceStart();



           var mTable = $('.dice_table').dataTable({
              "dom": '<"top">t<"clear">',
              "info": false,
              "paging": false,
              "language": {
                  "zeroRecords": pmarket_table_zero_records,
                  "search": '',
                  "searchPlaceholder": pmarket_table_search
              },
              "ordering": false,
              //"pageLength": calcDataTableLength(),
              "fnDrawCallback": function( oSettings ) {
          	        if(typeof $('.dataTables_scrollBody').data('jsp') === 'object')
                      {
                        $('.dataTables_scrollBody').data('jsp').reinitialise();
                      }
          	        if(typeof mTable === 'object')
                      {
                        //mTable.columns.adjust();
                        if(!maded)
                        {
                          maded = 1;
                          mTable.fnAdjustColumnSizing();
                          madeh = 0;
                        }
                      }

              },
              "fnHeaderCallback": function( nHead, aData, iStart, iEnd, aiDisplay ) {
                if(!madeh)
                {
                  madeh = 1;
                  maded = 0;
                }
              },
              "fnInitComplete": function() {
                $('.dataTables_scrollBody').jScrollPane();

              }
              ,"sScrollY": calcDataTableHeight()
            });


            $(window).smartresize(function(){
                    $('.dataTables_scrollBody').data('jsp').reinitialise();
                    $('.dataTables_scrollBody').css('height',calcDataTableHeight());
                    mTable.fnAdjustColumnSizing();
            } );


            var cur = $('select[name=currency] option:selected').attr('cur');
            $('.dice_page .currency a').html(cur);

            $('.dice_page input[name=bet]').change(function()
            {
              $(this).val( Number($(this).val()).toFixed(8) );
        	});
            $('.dice_page input[name=bet]').keyup(function(event)
            {
              var bet = $(this).val();
              var prize = floor(+bet*2).toFixed(8);
              $(this).parent().parent().parent().find('.prize').html(prize);
        	});

            $('.dice_page').on('click', 'a.nick', function(event)
            {
              $('#chat-input').addClass("hl").val($(this).html()+', ').focus();
              setTimeout(function(){ $('#chat-input').removeClass("hl"); }, 300);
            });

            $('.dice_page input[name=bet]').val('0.01').keyup().trigger('change');

        } );

        var calcDataTableHeight = function() {
          return $(window).height()-200;
        };


        //var calcDataTableLength = function() {
        //  return Math.floor(calcDataTableHeight()/40); // 50
        //};

        </script>
<div class="window" id="window_register">
<a href="#" class="close"></a>
<form onsubmit="doRegister();return false;" action="/" method="post">
<input type="hidden" name="rtoken" value="vxhOeh0LjsOY20IivpOjUaXWbIhqetpSruax9knUaGG91PtcefJTnHhoD4x4fjHDt3Fyi5TR8ZFl13DD9UAnPDH/vA75b2IuGU/u0Kkt99s4NILl6LtQDWhAaeyNSvFSJ7biovqLEV4/gHhOktO2k162AGDXMoMgyKZcxuyQcHvv5FBMlBfPJq0cwY2rgcB9OWDfztr6bueNAqVmPs1lyfZIXKQZngjgeLHqQk/fegBqO00SBtQw4FC73seGsREEGmPmQ39uin9arovq">
<div class="wincont">
<div class="title">�����������</div>
<div class="ln">
<p>�����</p>
<input name="login" type="text" class="" value="">
</div>
<div class="ln">
<p>Email �����</p>
<input name="email" type="text" class="" value="">
</div>
<div class="ln">
<p>��� ������</p>
<input name="psw1" type="password" class="" value="">
</div>
<div class="ln">
<p>����������� ������</p>
<input name="psw2" type="password" class="" value="">
</div>
<div class="win_meta">
<div class="che">
<label><span class="niceCheck"><div class="checker"><span><input type="checkbox" name="agree"></span></div></span>
<p>� �������� � <a href="/ru/rules/" target="_blank">��������� YoBit.net</a></p></label>
</div>
</div>
<div class="ln">
<div id="recaptcha-1" class="g-recaptcha" capid="0"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LcVxQsTAAAAAA9PM3osPv40xmVMNF7G1YkAgOxM&amp;co=aHR0cHM6Ly95b2JpdC5uZXQ6NDQz&amp;hl=ru&amp;v=r20171025115245&amp;size=normal&amp;cb=uem5gsd4s2ap" title="������ reCAPTCHA" width="304" height="78" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea></div></div>
</div>

<div class="ln win_center"><span id="register_error"></span></div>
<div class="sing_up_bt">
<input type="button" class="clRegister" value="������������������">
<div class="loading"></div>
</div>
<div class="rega_line">
��� ���� �������? <a href="#" class="login_link">�����</a>
</div>
</div>
<input type="submit" style="display:none">
</form>
</div> <div class="window" id="window_login">
<a href="#" class="close"></a>
<form onsubmit="doLogin();return false;" action="/" method="post">
<div class="wincont">
<div class="title">�����������</div>
<div class="ln">
<p>Email</p>
<input name="email" type="text" class="" value="">
</div>
<div class="ln">
<p>������</p>
<input name="psw" type="password" class="" value="">
</div>
<div class="ln auth2fa" style="display:none">
<p>������������������ ��� (�� Google Authenticator)</p>
<input name="code_2fa" type="text" class="" value="">
</div>
<div class="ln authemail" style="display:none">
<p>������������������ ��� (�� Email ������)</p>
<input name="code_email" type="text" class="" value="">
</div>
<div class="ln">
<div id="recaptcha-2" class="g-recaptcha" style="transform:scale(0.9);transform-origin:0 0" capid="1"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LcVxQsTAAAAAA9PM3osPv40xmVMNF7G1YkAgOxM&amp;co=aHR0cHM6Ly95b2JpdC5uZXQ6NDQz&amp;hl=ru&amp;v=r20171025115245&amp;size=normal&amp;cb=f020bxhk10tn" title="������ reCAPTCHA" width="304" height="78" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response-1" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea></div></div>
</div>
<div class="forgot"><a href="#" class="forgot_link">������ ������?</a></div>
<div class="ln win_center"><span id="login_error"></span></div>
<div class="win_meta">
<div class="che">
<label><span class="niceCheck"><div class="checker"><span><input type="checkbox" name="remember"></span></div></span>
<p>���������</p></label>
</div>
<input type="button" class="clLogin" value="�����">
<div class="loading"></div>
</div>
<div class="rega_line">
��� ��������? <a href="#" class="reg_link">�����������������</a>
</div>
</div>
<input type="submit" style="display:none">
</form>
</div>
<div class="window" id="window_forgot">
<a href="#" class="close"></a>
<form onsubmit="doForgotPassword();return false;" action="/" method="post">
<div class="wincont">
<div class="title">������ ������</div>
<div class="ln">
<p>Email</p>
<input name="email" type="text" class="" value="">
</div>
<div class="ln">
<div id="recaptcha-5" class="g-recaptcha" style="transform:scale(0.9);transform-origin:0 0" capid="2"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LcVxQsTAAAAAA9PM3osPv40xmVMNF7G1YkAgOxM&amp;co=aHR0cHM6Ly95b2JpdC5uZXQ6NDQz&amp;hl=ru&amp;v=r20171025115245&amp;size=normal&amp;cb=yh8acrftqtrf" title="������ reCAPTCHA" width="304" height="78" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response-2" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea></div></div>
</div>
<div class="ln win_center"><span id="forgot_error"></span></div>
<div class="sing_up_bt">
<input type="button" class="clForgotPassword" value="�������� ������">
<div class="loading"></div>
</div>
</div>
<input type="submit" style="display:none">
</form>
</div>
<div class="window" id="window_captcha">
<a href="#" class="close"></a>
<form onsubmit="doCheckCaptcha();return false;" action="/" method="post">
<input name="captcha_id" type="hidden" value="0">
<div class="wincont">
<div class="title">����������� ��������</div>
<div class="ln">
<div id="recaptcha-4" class="g-recaptcha" capid="3"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LcVxQsTAAAAAA9PM3osPv40xmVMNF7G1YkAgOxM&amp;co=aHR0cHM6Ly95b2JpdC5uZXQ6NDQz&amp;hl=ru&amp;v=r20171025115245&amp;size=normal&amp;cb=lmuyvye4etqv" title="������ reCAPTCHA" width="304" height="78" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response-3" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea></div></div>
</div>
<div class="ln win_center"><span class="error"></span><span class="success"></span></div>
<div class="sing_up_bt">
<input type="button" class="clCheckCaptcha" onclick="doCheckCaptcha();" value="�����������">
<div class="loading"></div>
</div>
</div>
<input type="submit" style="display:none">
</form>
</div>
<div id="mask"></div>
<header class="header">
<div class="wrap">
<div class="head_left">
<a href="/" class="logo"><img src="/images/logo.png" alt=""></a>
<ul class="top_menu">
<li><a href="/ru/trade/">�����</a></li>
<li><a href="/ru/market/">������</a></li>
<li><a href="/ru/investbox/" class="blue">InvestBox</a></li>
<li><a href="/ru/dice/" class="red active">Dice</a></li>
<li><a href="/ru/freecoins/" class="green">FreeCoins</a></li>
<li><a href="/ru/wallets/">�������</a></li>
<li><a href="/ru/orders/">������</a></li>
<li><a href="/ru/history/">�������</a></li>
<li><a href="/ru/ico/" class="pink">ICO</a></li>
<li><a href="/ru/coinsinfo/">CoinsInfo</a></li>
<li><a href="/ru/addcoin/">AddCoin</a></li>
<li><a href="/ru/support/">���������</a></li>
<li><a href="/ru/pm/">PM</a></li>
</ul>
</div>
<div class="head_right">
<ul class="enter_box">
<li class="user_list"><a class="user" href="#">blutbad</a>
<ul>
<li><a class="settings" href="/ru/settings/">���������</a></li>
<li><a class="orders" href="/ru/affiliate/">���������</a></li>
<li><a class="wallets" href="/ru/yobicodes/">Yobit ����</a></li>
<li><a class="orders" href="/ru/bonus/">Bonus</a></li>
<li><a class="orders" href="/ru/api/keys/">API �����</a></li>
<li><a class="orders" href="/ru/api/">API FAQ</a></li>
<li><a class="orders" href="/ru/fees/">Fees</a></li>
<li><a class="orders" href="/ru/faq/">FAQ</a></li>
<li class="down"><a class="exit2" href="/logout/a3d17612aa6b73f23a0b48b0a168d30812ff3f3c855562eb12da14e00811b800adb3169ba36b61ba9edb23bf898e0da3d68e614e4e4c453a69a4ce1affdd7392">�����</a></li>
</ul>
</li> </ul>
<ul class="lang">
<li><a href="/en/dice/">EN</a></li>
<li><a href="/ru/dice/" class="active">RU</a></li>
<li><a href="/cn/dice/">CN</a></li>
</ul>
</div>
</div>
</header>
<main class="content">
<div class="wrap">
<div class="left_bar">
<div class="balance_box ">
<div class="all_title title">�������</div>
<div class="result">
<table class="balances" width="100%">
<thead>
<tr>
<th width="40%">������</th>
<th width="60%">����������</th>
</tr>
</thead>
</table>
</div>
<div class="scrolling" id="scrollbar1">
<div class="scrollbar" style="height: 154px;"><div class="track" style="height: 154px;"><div class="thumb" style="top: 0px; height: 6.7375px;"></div></div></div>
<div class="viewport">
<div class="overview" style="top: 0px;">
<table class="balances" width="100%">
<tbody>
<tr href="/ru/history/deposits/BTC">
<td width="40%">BTC</td>
<td width="60%" id="balance_1" title="0.01548598 BTC">0.01548598</td>
</tr>
<tr href="/ru/history/bids/BTC/RUR">
<td width="40%">RUR</td>
<td width="60%" id="balance_50003" title="0.00000578 BTC">1.88931162</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<div class="result">
<table class="balances2" width="100%">
<tbody>
<tr>
<td width="40%">Est. BTC:</td>
<td width="60%" id="label_balances_equiv">0.08 BTC</td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="market_box">
<div class="all_title title">������</div>
<div>
<div class="market_base_container">
<a href="javascript:void(0)" value="top" onclick="changeMarketBase('top',0)">TOP</a><a href="javascript:void(0)" value="btc" onclick="changeMarketBase('btc',0)" class="manimr">BTC</a><a href="javascript:void(0)" value="eth" onclick="changeMarketBase('eth',0)" class="manimg">ETH</a><a href="javascript:void(0)" value="doge" onclick="changeMarketBase('doge',0)" class="manimr">DOGE</a><a href="javascript:void(0)" value="waves" onclick="changeMarketBase('waves',0)">WAVES</a><a href="javascript:void(0)" value="usd" onclick="changeMarketBase('usd',0)" class="active manimr">USD</a><a href="javascript:void(0)" value="rur" onclick="changeMarketBase('rur',0)" class="manimr">RUR</a>
</div>
</div>
<div class="result">
<table class="marketes" width="100%">
<thead>
<tr>
<th width="25%" class="first">������</th>
<th width="33%">����</th>
<th width="26%">���.</th>
<th width="16%">��.</th>
</tr>
</thead>
</table>
</div>
<div class="scrolling" id="scrollbar2">
<div class="search_box_trade">
<div class="button"></div>
<div id="trade_market_filter" class="dataTables_filter">
<label>
 <input id="coinfilter" type="search" class="" placeholder="�����" aria-controls="trade_market" onsearch="$('#coinfilter').keyup();">
</label>
</div>
</div>
<div class="scrollbar" style="height: 380px;"><div class="track" style="height: 380px;"><div class="thumb" style="top: 0px; height: 7.09234px;"></div></div></div>
<div class="viewport">
<div class="overview" id="market_base_list" style="top: 0px;">
<div id="trade_market_wrapper" class="dataTables_wrapper no-footer"><div class="search_box_trade2"><div class="button"></div><div id="trade_market_filter" class="dataTables_filter"><label><input type="search" class="" placeholder="�����" aria-controls="trade_market"></label></div></div><table id="trade_market" class="marketes dataTable no-footer" width="100%" role="grid" style="width: 100%;">
<thead style="display:none;">
<tr role="row"><th width="25%" class="first sorting_disabled" rowspan="1" colspan="1" style="width: 0px;">������</th><th width="33%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 0px;">����</th><th width="26%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 0px;">���.</th><th width="16%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 0px;">��.</th><th style="display: none; width: 0px;" class="sorting_disabled" rowspan="1" colspan="1">fullname</th></tr>
</thead>
<tbody>



 <tr class="odd animg" p="50001" c1="1" c2="50001" c1n="BTC" c2n="USD" role="row">
<td width="25%" class="first">BTC</td>
<td width="33%">$5849.00</td>
<td width="26%" class="green">+ 0.4%</td>
<td width="16%" title="34.4 BTC">34.4</td>
<td style="display:none;">Bitcoin</td>
</tr><tr class="even animg" p="50010" c1="40001" c2="50001" c1n="ETH" c2n="USD" role="row">
<td class="first">ETH</td>
<td>$298.43</td>
<td class="green">+ 1.2%</td>
<td title="11 BTC">11</td>
<td style="display:none;">Ethereum</td>
</tr><tr class="odd" p="50022" c1="1015" c2="50001" c1n="BCC" c2n="USD" role="row">
<td class="first">BCC</td>
<td>$460.00</td>
<td class="green">+ 22.6%</td>
<td title="4.77 BTC">4.8</td>
<td style="display:none;">Bitcoin Cash</td>
</tr><tr class="even" p="5981" c1="1077" c2="50001" c1n="REC" c2n="USD" role="row">
<td class="first">REC</td>
<td>$76.90</td>
<td class="red">- 9.6%</td>
<td title="2.08 BTC">2.1</td>
<td style="display:none;">Regal Coin</td>
</tr><tr class="odd" p="6173" c1="1109" c2="50001" c1n="BTG" c2n="USD" role="row">
<td class="first">BTG</td>
<td>$163.84</td>
<td class="green">+ 7.8%</td>
<td title="1.91 BTC">2</td>
<td style="display:none;">Bitcoin Gold</td>
</tr><tr class="even" p="4805" c1="879" c2="50001" c1n="FRST" c2n="USD" role="row">
<td class="first">FRST</td>
<td>$16.25</td>
<td class="green">+ 1%</td>
<td title="1.07 BTC">1.1</td>
<td style="display:none;">FirstCoin</td>
</tr><tr class="odd" p="50004" c1="2" c2="50001" c1n="LTC" c2n="USD" role="row">
<td class="first">LTC</td>
<td>$55.00</td>
<td class="green">+ 0.1%</td>
<td title="1.03 BTC">1.1</td>
<td style="display:none;">Litecoin</td>
</tr><tr class="even" p="4705" c1="767" c2="50001" c1n="ZEC" c2n="USD" role="row">
<td class="first">ZEC</td>
<td>$231.00</td>
<td class="red">- 2.2%</td>
<td title="1.01 BTC">1.1</td>
<td style="display:none;">Zcash</td>
</tr></tbody>
</table><div class="clear"></div></div>
</div>
</div>
</div>
</div>
</div>
<div class="center_box">
<div class="inset_page">
<div class="top_meta_dice">
<h1 class="h1_dice">Dice</h1>
<div class="slogan">
<div class="ch">
<div class="el_chek" style="flow:horizontal;width:0 auto;">
<div style="float:left"><label><div class="checker" id="uniform-check_only_mine"><span class="checked"><input id="check_only_mine" type="checkbox" checked=""></span></div> <strong>���������� ������ ��� ������</strong></label></div>
<div style="float:left;padding-left:20px;"><label><div class="checker" id="uniform-check_only_btc"><span><input id="check_only_btc" type="checkbox"></span></div> <strong>������ BTC</strong></label></div>
</div>
</div> </div>
</div>
<div class="overview">
<div class="dice_page">
<div class="create_new">
<table class="dice_htable" width="100%">
<thead>
<tr>
<td width="250"><span class="history-left">�������� ������</span></td>
<td width="170"><span class="history-left">������</span></td>
<td width="120"><span class="history-left">����</span></td>
<td><span class="history-left">������!</span><a href="/ru/history/dice/" class="history"></a></td>
</tr>
</thead>
<tbody>
<tr>
<td>
<div class="dice_select">
<select name="currency" class="chosen-select-no-single style_hosen1" onchange="var cur = $('select[name=currency] option:selected').attr('cur'); $('.dice_page .currency a').html(cur); $('.dice_page input[name=bet]').val('0.0001').trigger('change'); $(location).attr('href',$('select[name=currency] option:selected').attr('href'));" style="display: none;">
<option href="/ru/dice/BTC" value="1" sum="0.01548598" cur="BTC">BTC (Bitcoin) - 0.01548598 BTC</option>
<option href="/ru/dice/DASH" value="10" sum="0.00000400" cur="DASH">DASH (DASH) - 0.000004 DASH</option>
<option href="/ru/dice/ZEC" value="767" sum="0.00000003" cur="ZEC">ZEC (Zcash) - 0.00000003 ZEC</option>
<option href="/ru/dice/USD" value="50001" sum="64.93205703" cur="USD">USD (USD) - 64.93205703 USD</option>
<option href="/ru/dice/RUR" value="50003" sum="0.00006844" cur="RUR">RUR (RUR) - 0.00006844 RUR</option>
</select><div class="chosen-container chosen-container-single" style="width: 303px;" title=""><a class="chosen-single" tabindex="-1"><span>BTC (Bitcoin) - 0.01548598 BTC</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off"></div><ul class="chosen-results"></ul></div></div>
</div>
 </td>
<td>
<div class="poles"><input class="bet" name="bet" maxlength="25" type="text" value="0.01"><span class="currency"><a href="#" onclick="var sum = $('select[name=currency] option:selected').attr('sum'); $('.dice_page input[name=bet]').val(sum).trigger('change'); return false;">BTC</a></span></div>
</td>
<td><span class="prize green">0.02000000</span></td>
<td>
<input type="button" class="clDicePlay " onclick="doDiceSend(this,0);" origin="Roll < 48" value="Roll < 48">
<input type="button" class="clDicePlay " onclick="doDiceSend(this,1);" origin="Roll > 52" value="Roll > 52">
</td>
</tr>
</tbody>
</table>
<div class="clear"></div>
<div><span class="error"></span><span class="success"></span></div>
</div>
<div class="clear"></div>
<div class="list">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer"><div class="top"></div><div class="dataTables_scroll"><div class="dataTables_scrollHead" style="overflow: hidden; position: relative; border: 0px; width: 100%;"><div class="dataTables_scrollHeadInner" style="box-sizing: content-box; width: 998px; padding-right: 21px;"><table class="dice_table dataTable no-footer" width="100%" role="grid" style="margin-left: 0px; width: 998px;"><thead>
<tr role="row"><td width="10%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px;">����</td><td width="13%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;">�����</td><td width="12%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 120px;">������</td><td width="18%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 180px;">������������</td><td width="13%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px;">������</td><td width="11%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 110px;">����</td><td width="10%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 99px;">�����</td><td width="13%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 129px;">�������</td></tr>
</thead></table></div></div><div class="dataTables_scrollBody jspScrollable" style="position: relative; overflow: hidden; height: 433px; padding: 0px; width: 100%;" tabindex="0"><div class="jspContainer" style="width: 1015px; height: 433px;"><div class="jspPane" style="padding: 0px; top: 0px; left: 0px; width: 998px;"><table class="dice_table dataTable no-footer" width="100%" id="DataTables_Table_0" role="grid" style="width: 100%;"><thead>
<tr role="row" style="height: 0px;"><td width="10%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 100px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">����</div></td><td width="13%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">�����</div></td><td width="12%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 120px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">������</div></td><td width="18%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 180px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">������������</div></td><td width="13%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 130px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">������</div></td><td width="11%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 110px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">����</div></td><td width="10%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 99px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">�����</div></td><td width="13%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 129px; padding-top: 0px; padding-bottom: 0px; border-top-width: 0px; border-bottom-width: 0px; height: 0px;"><div class="dataTables_sizing" style="height:0;overflow:hidden;">�������</div></td></tr>
</thead>

<tbody>






















 <tr role="row" class="odd">
<td width="10%">988438893</td>
<td width="13%">09:25:10</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">1.6</td>
<td width="11%">&gt;52</td>
<td width="10%">90.811</td>
<td width="13%" class="green">+1.60000000</td>
</tr><tr role="row" class="even">
<td width="10%">988438261</td>
<td width="13%">09:25:03</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.8</td>
<td width="11%">&gt;52</td>
<td width="10%">33.978</td>
<td width="13%" class="red">-0.80000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988437839</td>
<td width="13%">09:24:59</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.4</td>
<td width="11%">&gt;52</td>
<td width="10%">9.9095</td>
<td width="13%" class="red">-0.40000000</td>
</tr><tr role="row" class="even">
<td width="10%">988437459</td>
<td width="13%">09:24:55</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.2</td>
<td width="11%">&gt;52</td>
<td width="10%">28.821</td>
<td width="13%" class="red">-0.20000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988437077</td>
<td width="13%">09:24:51</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.1</td>
<td width="11%">&gt;52</td>
<td width="10%">16.3121</td>
<td width="13%" class="red">-0.10000000</td>
</tr><tr role="row" class="even">
<td width="10%">988436146</td>
<td width="13%">09:24:41</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.4</td>
<td width="11%">&gt;52</td>
<td width="10%">78.9282</td>
<td width="13%" class="green">+0.40000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988435824</td>
<td width="13%">09:24:38</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.2</td>
<td width="11%">&gt;52</td>
<td width="10%">42.3787</td>
<td width="13%" class="red">-0.20000000</td>
</tr><tr role="row" class="even">
<td width="10%">988435489</td>
<td width="13%">09:24:35</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.1</td>
<td width="11%">&gt;52</td>
<td width="10%">6.5997</td>
<td width="13%" class="red">-0.10000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988433861</td>
<td width="13%">09:24:18</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.1</td>
<td width="11%">&gt;52</td>
<td width="10%">83.3331</td>
<td width="13%" class="green">+0.10000000</td>
</tr><tr role="row" class="even">
<td width="10%">988429214</td>
<td width="13%">09:23:30</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.8</td>
<td width="11%">&gt;52</td>
<td width="10%">69.9366</td>
<td width="13%" class="green">+0.80000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988428823</td>
<td width="13%">09:23:26</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.4</td>
<td width="11%">&gt;52</td>
<td width="10%">49.4428</td>
<td width="13%" class="red">-0.40000000</td>
</tr><tr role="row" class="even">
<td width="10%">988428317</td>
<td width="13%">09:23:21</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.2</td>
<td width="11%">&lt;48</td>
<td width="10%">92.3527</td>
<td width="13%" class="red">-0.20000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988427758</td>
<td width="13%">09:23:16</td>
 <td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.1</td>
<td width="11%">&gt;52</td>
<td width="10%">22.1747</td>
<td width="13%" class="red">-0.10000000</td>
</tr><tr role="row" class="even">
<td width="10%">988427636</td>
<td width="13%">09:23:14</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.1</td>
<td width="11%">&gt;52</td>
<td width="10%">77.7528</td>
<td width="13%" class="green">+0.10000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988426954</td>
<td width="13%">09:23:08</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.4</td>
<td width="11%">&gt;52</td>
<td width="10%">95.8399</td>
<td width="13%" class="green">+0.40000000</td>
</tr><tr role="row" class="even">
<td width="10%">988426505</td>
<td width="13%">09:23:03</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.2</td>
<td width="11%">&gt;52</td>
<td width="10%">37.9297</td>
<td width="13%" class="red">-0.20000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988426066</td>
<td width="13%">09:22:59</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.1</td>
<td width="11%">&gt;52</td>
<td width="10%">21.3226</td>
<td width="13%" class="red">-0.10000000</td>
</tr><tr role="row" class="even">
<td width="10%">988422012</td>
<td width="13%">09:22:19</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>ETC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">0.1</td>
<td width="11%">&gt;52</td>
<td width="10%">57.5521</td>
<td width="13%" class="green">+0.10000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988420129</td>
<td width="13%">09:21:59</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>PAC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">10</td>
<td width="11%">&gt;52</td>
<td width="10%">92.5866</td>
<td width="13%" class="green">+10.00000000</td>
</tr><tr role="row" class="even">
<td width="10%">988419753</td>
<td width="13%">09:21:55</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>PAC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">8</td>
<td width="11%">&gt;52</td>
<td width="10%">68.2112</td>
<td width="13%" class="green">+8.00000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988419506</td>
<td width="13%">09:21:52</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>PAC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">4</td>
<td width="11%">&gt;52</td>
<td width="10%">1.1156</td>
<td width="13%" class="red">-4.00000000</td>
</tr><tr role="row" class="even">
<td width="10%">988419242</td>
<td width="13%">09:21:50</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>PAC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">2</td>
<td width="11%">&gt;52</td>
<td width="10%">45.6033</td>
<td width="13%" class="red">-2.00000000</td>
</tr><tr role="row" class="odd">
<td width="10%">988418910</td>
<td width="13%">09:21:46</td>
<td width="12%"><img src="/images/cur_0.png" width="18px" height="18px"><span>PAC</span></td>
<td width="18%"><a class="nick">blutbad</a></td>
<td width="13%">1</td>
<td width="11%">&gt;52</td>
<td width="10%">4.8793</td>
<td width="13%" class="red">-1.00000000</td>
</tr></tbody>
</table></div><div class="jspVerticalBar"><div class="jspCap jspCapTop"></div><div class="jspTrack" style="height: 433px;"><div class="jspDrag" style="height: 285px;"><div class="jspDragTop"></div><div class="jspDragBottom"></div></div></div><div class="jspCap jspCapBottom"></div></div></div></div></div><div class="clear"></div></div>
</div>
</div>
</div>
</div>
</div>
<div class="right_bar">
<div class="chat_box">
<div class="inset">
<div class="all_title title">��� <div class="locales">
<a class="" href="javascript:void(0)" value="en" onclick="changeChatLocale('en');">EN</a><a class="active" href="javascript:void(0)" value="ru" onclick="changeChatLocale('ru');">RU</a><a class="" href="javascript:void(0)" value="de" onclick="changeChatLocale('de');">DE</a><a class="" href="javascript:void(0)" value="cn" onclick="changeChatLocale('cn');">CN</a><a class="" href="javascript:void(0)" value="ar" onclick="changeChatLocale('ar');">AR</a> </div>
</div>
<div class="scrolling" id="scrollbar7">
<div class="scrollbar" style="height: 350px;"><div class="track" style="height: 350px;"><div class="thumb" style="top: 288.627px; height: 61.3727px;"></div></div></div>
<div class="viewport">
<div class="overview" id="chat-list" style="top: -1646px;">
<p id="msg5510586" class=""><a class="pm" href="/ru/pm/create/sonzz"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:18:38">sonzz</a>:&nbsp;����� �����</p><p id="msg5510589" class=""><a class="pm" href="/ru/pm/create/reznik"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:18:54">reznik</a>&nbsp;L1:&nbsp;4creator, ���� ������ ��� ATB - � �� �������</p><p id="msg5510590" class=""><a class="pm" href="/ru/pm/create/Arsik"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:18:55">Arsik</a>:&nbsp;Timon88, � �� ������ �� 13?</p><p id="msg5510592" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:18:57">FaxumX</a>:&nbsp;TimurS, �� ��� �� ����� �� ������� ���� � ��� ������ (�) </p><p id="msg5510593" class=""><a class="pm" href="/ru/pm/create/wizard"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:19:01">wizard</a>:&nbsp;YOBITMBOB0PERK���AM4N2SBSQSJCWVFSZFBTPWBTC</p><p id="msg5510594" class=""><a class="pm" href="/ru/pm/create/4creator"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:19:04">4creator</a>:&nbsp;FaxumX, � ��� ��, � ���� 460 USD .. � �� ��� 470</p><p id="msg5510595" class=""><a class="pm" href="/ru/pm/create/wizard"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:19:12">wizard</a>:&nbsp;YOBITF04AZNDJ5YKJVPQ2Z8FUH8PX���ODSAMHGBTC</p><p id="msg5510597" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:19:20">FaxumX</a>:&nbsp;4creator, ��, ��� ���� ������. ���� ����� ����.</p><p id="msg5510598" class=""><a class="pm" href="/ru/pm/create/wizard"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:19:25">wizard</a>:&nbsp;YOBITOONG8VUYNAGZRI141Y1QMZC3WYWUTJO��BTC</p><p id="msg5510601" class=""><a class="pm" href="/ru/pm/create/wizard"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:19:42">wizard</a>:&nbsp;YOBITIAEQLLSI��LOJN0T3ZEU3JKUOUERRQJYZBTC</p><p id="msg5510603" class=""><a class="pm" href="/ru/pm/create/4creator"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:19:52">4creator</a>:&nbsp;reznik, ������ �� ��� �� �����))</p><p id="msg5510604" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:20:05">FaxumX</a>:&nbsp;4creator, ����� ���� ������ )</p><p id="msg5510605" class=""><a class="pm" href="/ru/pm/create/Mbgr1"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:20:27">Mbgr1</a>&nbsp;L1:&nbsp;wizard, +</p><p id="msg5510606" class=""><a class="pm" href="/ru/pm/create/wizard"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:20:31">wizard</a>:&nbsp;YOBITSROOXGXNHCTHXCOBSIYWQYOLY��� ��� ����))MHFYPUDBTC</p><p id="msg5510610" class=""><a class="pm" href="/ru/pm/create/TimurS"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:21:01">TimurS</a>&nbsp;L4:&nbsp;FaxumX, ����� ����� �����, ��� ������</p><p id="msg5510611" class=""><a class="pm" href="/ru/pm/create/4creator"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:21:04">4creator</a>:&nbsp;������ ����� ������� �� ����� �������.. � ����� � ������������ BCC �����</p><p id="msg5510612" class=""><a class="pm" href="/ru/pm/create/kasatkin2017"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:21:21">kasatkin2017</a>&nbsp;L1:&nbsp;wizard, �����������! ������� �� ��� BTC 0.01 ����������. ���� ���� ��� ��� ���� ���������� �������� � ������ ���������. ������������� ��� �� �������.</p><p id="msg5510615" class=""><a class="pm" href="/ru/pm/create/rablik"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:21:50">rablik</a>&nbsp;L1:&nbsp;�� ���� �� ����� ��� ������</p><p id="msg5510617" class=""><a class="pm" href="/ru/pm/create/4creator"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:21:55">4creator</a>:&nbsp;� ��� ���� ����� �� �������� ��� ��������))))) </p><p id="msg5510618" class=""><a class="pm" href="/ru/pm/create/wizard"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:22:01">wizard</a>:&nbsp;kasatkin2017, )</p><p id="msg5510619" class=""><a class="pm" href="/ru/pm/create/ramstein"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:22:11">ramstein</a>:&nbsp;cash razvod, btg razvod, no tol'ko na satoxu vyroslo i vse vdrug zabyi razvod, vspomnyat kak opyat' "razvedut" ?</p><p id="msg5510620" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:22:13">FaxumX</a>:&nbsp;TimurS,�����, � ��� ���������, ��� ����� ������� )</p><p id="msg5510621" class=""><a class="pm" href="/ru/pm/create/Oktogen"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:22:20">Oktogen</a>&nbsp;L1:&nbsp;4creator, � ������� ��������� ������</p><p id="msg5510622" class=""><a class="pm" href="/ru/pm/create/wizard"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:22:21">wizard</a>:&nbsp;rablik, ������� ��� ������)</p><p id="msg5510626" class=""><a class="pm" href="/ru/pm/create/4creator"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:22:39">4creator</a>:&nbsp;�� �� ������� ���, ��������� �� ���������. � ������� ���-���</p><p id="msg5510627" class=""><a class="pm" href="/ru/pm/create/OLDMOLD"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:22:41">OLDMOLD</a>&nbsp;L1:&nbsp;wizard, � �� �� � �� �� �������� �� ������ ����)) ������)</p><p id="msg5510628" class=""><a class="pm" href="/ru/pm/create/TimurS"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:22:46">TimurS</a>&nbsp;L4:&nbsp;FaxumX, �� ���, � ������ � ������</p><p id="msg5510631" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:23:00">FaxumX</a>:&nbsp;TimurS, �� ��� ��� � ����� ����� �� ���� ��������� )))</p><p id="msg5510632" class=""><a class="pm" href="/ru/pm/create/4creator"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:23:09">4creator</a>:&nbsp;����� ��� ����� ����� </p><p id="msg5510634" class=""><a class="pm" href="/ru/pm/create/Drotoverin"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:23:16">Drotoverin</a>:&nbsp;kasatkin2017,��� ���� �� ��� � ���� ������ ��������</p><p id="msg5510636" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:23:21">FaxumX</a>:&nbsp;TimurS, ������ ��� ����!</p><p id="msg5510638" class=""><a class="pm" href="/ru/pm/create/TimurS"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:23:55">TimurS</a>&nbsp;L4:&nbsp;FaxumX, ��������, �� �� �� ������ � � ���� �������� </p><p id="msg5510639" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:24:21">FaxumX</a>:&nbsp;TimurS, �� ���� � ���� � ������ ������� ������. �� ����������� �������, ����� � ��� ��� �������.</p><p id="msg5510640" class=""><a class="pm" href="/ru/pm/create/reznik"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:24:30">reznik</a>&nbsp;L1:&nbsp;4creator, ������ �������� )</p><p id="msg5510641" class=""><a class="pm" href="/ru/pm/create/TimurS"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:24:42">TimurS</a>&nbsp;L4:&nbsp;FaxumX, � ���� ���� ����?)</p><p id="msg5510642" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:24:51">FaxumX</a>:&nbsp;TimurS, � �� ��� �������� �� ������� � ������, � ��������� ����� ������ )</p><p id="msg5510643" class=""><a class="pm" href="/ru/pm/create/menen4"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:24:51">menen4</a>:&nbsp;� �� �� ��� �����?</p><p id="msg5510644" class=""><a class="pm" href="/ru/pm/create/Sergfedya1"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:24:56">Sergfedya1</a>:&nbsp;��� ������</p><p id="msg5510645" class=""><a class="pm" href="/ru/pm/create/33py6"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:25:02">33py6</a>&nbsp;L1:&nbsp;4creator, ���� zec ����� � 2 ���� ������� �� �����? ��� ) ����� PAC ������ ��� ������ �������� ������ �� ������� </p> <p id="msg5510646" class=""><a class="pm" href="/ru/pm/create/FeudMoth"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:25:13">FeudMoth</a>:&nbsp;menen4, ������ ��� � ��� �� 300 ������� :)</p><p id="msg5510647" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:25:20">FaxumX</a>:&nbsp;TimurS, ���� ���� �������, ��� ��� �� ������ ��� �� ����� �� ��� ������� )</p><p id="msg5510648" class=""><a class="pm" href="/ru/pm/create/menen4"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:25:33">menen4</a>:&nbsp;FeudMoth, � ��� �� ������ ������?)</p><p id="msg5510650" class=""><a class="pm" href="/ru/pm/create/FeudMoth"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:25:55">FeudMoth</a>:&nbsp;� ��� �� �����, ��� ��� �� 300 ���� �����, ��� �� 200, � ���� �� 280?</p><p id="msg5510652" class=""><a class="pm" href="/ru/pm/create/TimurS"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:26:00">TimurS</a>&nbsp;L4:&nbsp;FaxumX, � �� � �����) � ��� �� ���� ���� ���� ��� ������)</p><p id="msg5510653" class=""><a class="pm" href="/ru/pm/create/ramstein"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:26:06">ramstein</a>:&nbsp;glavnoe nepokupaite etot bcc, kto to napompil chtob vas slit' pozzhe. dlya doveriya poderzhyt paru dnei cenu i bai bai ?</p><p id="msg5510654" class=""><a class="pm" href="/ru/pm/create/SibTIGR"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:26:21">SibTIGR</a>:&nbsp;ramstein, ����</p><p id="msg5510655" class=""><a class="pm" href="/ru/pm/create/wizard"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:26:26">wizard</a>:&nbsp;YOBITMCA3OCM1M7VIUBA1MUCFGAQFM��EK72MEBTC</p><p id="msg5510656" class=""><a class="pm" href="/ru/pm/create/FaxumX"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:26:32">FaxumX</a>:&nbsp;TimurS, ��� � � �� �������� ���� ������ �� �������. ������ � ������� ���� � ��� � �������� ��� ���, ����� �������� ���������.</p><p id="msg5510657" class=""><a class="pm" href="/ru/pm/create/TimurS"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:26:36">TimurS</a>&nbsp;L4:&nbsp;FaxumX, � ���� � ������ �� �������� �� �����) �������� ������)</p><p id="msg5510659" class=""><a class="pm" href="/ru/pm/create/4creator"><img src="/images/pm.png" width="10" height="10"></a><a class="nick " title="10:26:53">4creator</a>:&nbsp;33py6, ���, ���� ������� �� ������� ������ � ... ������ �� 2-3$ )))</p></div>
</div>
</div>
<div class="fildes_box">
<div class="p_2"><a id="chat-btn" class="button" href="javascript:void(0)">���������</a></div>
<div class="p_1"><span class="p_3"><input id="chat-input" type="text" value=""></span></div>
</div>
</div>
</div>
<div class="play_lotto_box">
<div class="meta">
<div class="all_title title">���� Dice</div>
<a href="javascript:void(0)" onclick="popupDiceRules()" class="question"></a>
<a href="/ru/history/dice/" class="history"></a>
</div>
<div class="selected_box">
<div class="righted">
<div class="vol_4">
<p>&nbsp;</p>
<div class="poles">
<input type="button" class="clDicePlay " onclick="doSmartDiceSend(this,1);" origin="R > 52" value="R > 52">
</div>
</div>
</div>
<div class="lefted">
<div class="inset">
<div class="vol_1">
<p>BTC</p>
<select id="smart_dice_bet" class="chosen-select-no-single style_hosen1" style="display: none;">
<option value="0.001">0.001</option>
<option value="0.01" selected="">0.01</option>
<option value="0.05">0.05</option>
<option value="0.1">0.1</option>
<option value="1">1</option>
<option value="10">10</option>
<option value="100">100</option>
</select><div class="chosen-container chosen-container-single chosen-container-single-nosearch" style="width: 62px;" title="" id="smart_dice_bet_chosen"><a class="chosen-single" tabindex="-1"><span>0.01</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" readonly=""></div><ul class="chosen-results"></ul></div></div>
</div>
<div class="vol_2">
<p>����</p>
<div id="smart_dice_prize" class="prize_ci">0.02</div>
</div>
<div class="vol_3">
<p>&nbsp;</p>
<input type="button" class="clDicePlay " onclick="doSmartDiceSend(this,0);" origin="R < 48" value="R < 48">
</div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
<div class="news_box">
<div class="inset" id="news_twitter">
<iframe class="stream" src="/twits/" frameborder="0" style="border:none;width:100%;"></iframe>
</div>
</div>
</div>
</div></main>
<footer class="footer">
</footer>
</div>

<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter27854913 = new Ya.Metrika({id:27854913,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript>&lt;div&gt;&lt;img src="//mc.yandex.ru/watch/27854913" style="position:absolute; left:-9999px;" alt="" /&gt;&lt;/div&gt;</noscript>

<img src="/lang_detect/" style="position:absolute; left:-9999px;" weight="0" height="0">


<div id="mainLoader"></div>
<div id="loaderFade"></div>
<div style="background-color: #fff; border: 1px solid #ccc; box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.2); position: absolute; left: 0px; top: -10000px; transition: visibility 0s linear 0.3s, opacity 0.3s linear; opacity: 0; visibility: hidden; z-index: 2000000000;"><div style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 2000000000; background-color: #fff; opacity: 0.05;  filter: alpha(opacity=5)"></div><div class="g-recaptcha-bubble-arrow" style="border: 11px solid transparent; width: 0; height: 0; position: absolute; pointer-events: none; margin-top: -11px; z-index: 2000000000;"></div><div class="g-recaptcha-bubble-arrow" style="border: 10px solid transparent; width: 0; height: 0; position: absolute; pointer-events: none; margin-top: -10px; z-index: 2000000000;"></div><div style="z-index: 2000000000; position: relative;"><iframe title="�������� recaptcha" src="https://www.google.com/recaptcha/api2/bframe?hl=ru&amp;v=r20171025115245&amp;k=6LcVxQsTAAAAAA9PM3osPv40xmVMNF7G1YkAgOxM#uyl9x7235nar" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox" name="uyl9x7235nar" style="width: 100%; height: 100%;"></iframe></div></div><div style="background-color: #fff; border: 1px solid #ccc; box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.2); position: absolute; left: 0px; top: -10000px; transition: visibility 0s linear 0.3s, opacity 0.3s linear; opacity: 0; visibility: hidden; z-index: 2000000000;"><div style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 2000000000; background-color: #fff; opacity: 0.05;  filter: alpha(opacity=5)"></div><div class="g-recaptcha-bubble-arrow" style="border: 11px solid transparent; width: 0; height: 0; position: absolute; pointer-events: none; margin-top: -11px; z-index: 2000000000;"></div><div class="g-recaptcha-bubble-arrow" style="border: 10px solid transparent; width: 0; height: 0; position: absolute; pointer-events: none; margin-top: -10px; z-index: 2000000000;"></div><div style="z-index: 2000000000; position: relative;"><iframe title="�������� recaptcha" src="https://www.google.com/recaptcha/api2/bframe?hl=ru&amp;v=r20171025115245&amp;k=6LcVxQsTAAAAAA9PM3osPv40xmVMNF7G1YkAgOxM#ul5y6hdwk6w7" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox" name="ul5y6hdwk6w7" style="width: 100%; height: 100%;"></iframe></div></div><div style="background-color: #fff; border: 1px solid #ccc; box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.2); position: absolute; left: 0px; top: -10000px; transition: visibility 0s linear 0.3s, opacity 0.3s linear; opacity: 0; visibility: hidden; z-index: 2000000000;"><div style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 2000000000; background-color: #fff; opacity: 0.05;  filter: alpha(opacity=5)"></div><div class="g-recaptcha-bubble-arrow" style="border: 11px solid transparent; width: 0; height: 0; position: absolute; pointer-events: none; margin-top: -11px; z-index: 2000000000;"></div><div class="g-recaptcha-bubble-arrow" style="border: 10px solid transparent; width: 0; height: 0; position: absolute; pointer-events: none; margin-top: -10px; z-index: 2000000000;"></div><div style="z-index: 2000000000; position: relative;"><iframe title="�������� recaptcha" src="https://www.google.com/recaptcha/api2/bframe?hl=ru&amp;v=r20171025115245&amp;k=6LcVxQsTAAAAAA9PM3osPv40xmVMNF7G1YkAgOxM#tpjfc9qkvczf" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox" name="tpjfc9qkvczf" style="width: 100%; height: 100%;"></iframe></div></div><div style="background-color: #fff; border: 1px solid #ccc; box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.2); position: absolute; left: 0px; top: -10000px; transition: visibility 0s linear 0.3s, opacity 0.3s linear; opacity: 0; visibility: hidden; z-index: 2000000000;"><div style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 2000000000; background-color: #fff; opacity: 0.05;  filter: alpha(opacity=5)"></div><div class="g-recaptcha-bubble-arrow" style="border: 11px solid transparent; width: 0; height: 0; position: absolute; pointer-events: none; margin-top: -11px; z-index: 2000000000;"></div><div class="g-recaptcha-bubble-arrow" style="border: 10px solid transparent; width: 0; height: 0; position: absolute; pointer-events: none; margin-top: -10px; z-index: 2000000000;"></div><div style="z-index: 2000000000; position: relative;"><iframe title="�������� recaptcha" src="https://www.google.com/recaptcha/api2/bframe?hl=ru&amp;v=r20171025115245&amp;k=6LcVxQsTAAAAAA9PM3osPv40xmVMNF7G1YkAgOxM#ukgwwlwef8eu" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox" name="ukgwwlwef8eu" style="width: 100%; height: 100%;"></iframe></div></div></body></html>