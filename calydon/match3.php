<?php

	session_start();
    if ($_SESSION['uid'] == null) die();
   	include "connect.php";
    include "m_game_function.php";
    $user = sql_row("SELECT * FROM `users` WHERE `id` = '{$_SESSION['uid']}' LIMIT 1;");

    header("Content-Type: text/xml; charset=utf-8");
    echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

    include_once("functions/jewel/match3_matrix.php");

    $jewel_file = 'adata/jewel/'.$user['id'].'.txt';

    $action = @$_GET['action'];
    $action = @$_GET['cmd'];

    $save = 1;

    $max_find_jewel = 300;
    $total_time  = 900;

//if($action != 'save') file_put_contents('adata/jewel/'.$_SESSION['uid'].'_xml_get.txt', serialize($_GET) . "\n", FILE_APPEND);

/***------------------------------------------
 * �������� ������������
 **/

function load_config() {
  echo '<data>';
    echo '<preferences library="http://'.INCITY.'.blutbad.ru/f/match3/res.swf"/>';
  	echo '<scripturl x2="x2" y1="y1" gamestate="/match3.php?cmd=default" move="/match3.php?cmd=step" n_matrix="/match3.php?cmd=n_matrix" isrand="true" y2="y2" qid="qid" x1="x1" />';
  echo '</data>';
}

function load_step() {
 global $jewel_file, $user, $save, $max_find_jewel;

  $drawmatrix = '';
	if(file_exists($jewel_file)) {

      if (is_readable($jewel_file)) {

		$data = unserialize(file_get_contents($jewel_file));
        $olddata = $data['matrix'];
        Game::setMatrix($olddata);
       // $olddata = Game::matrix_revers($olddata);

      //  $sdf = Game::moves();

		$x1 = @$_GET['x1'] -1;
		$y1 = @$_GET['y1'] -1;

		$x2 = @$_GET['x2'] -1;
		$y2 = @$_GET['y2'] -1;

		$d_x = $x1;
		$d_y = $y1;
		$m_x = $x2;
		$m_y = $y2;


           $olddata = Game::onCompleteLoad($olddata);
           Game::loadField($olddata);

           $arg1 = Game::getJewel($d_x, $d_y);
           $arg2 = Game::getJewel($m_x, $m_y);

         # �����������
           Game::sendMove($arg1, $arg2);

         # ����������
           $w_id = 0;
            while (true) {
              if (!Game::collapse()) break;
              $w_id++;
              if ($w_id == 100) break;
            }

          # �������� ��������� ������
            $drawmatrix = Game::get_result_matrix();

            if ($save) {

                  $data_matrix = array();

                  foreach(Game::$jewels as $x => $objects) {
                    foreach($objects as $y => $symbol) {
                      $data_matrix[$x][$y] = $symbol["type"];
                    }
                  }

                # ����� ������� � ������� ���� ������
                  $matrix = array();
                  $matrix = $data;

                #  $matrix['time_end'] = time() + $total_time;

                  $my_collected_stones = Game::$my_close_Jewels;
                  $collected_stones    = Game::$close_Jewels;

          	   	  $matrix['collected_stones'] += $my_collected_stones;
          	   	  $matrix['moves'] += 1;
          	  	  $matrix['matrix'] = $data_matrix;

                # ����� ����
                  if (is_writable($jewel_file)) {

                    if ($matrix['collected_stones'] > $max_find_jewel && empty($matrix['end_game'])) {
                      $matrix['end_game'] = time();
                      sql_query("UPDATE `users` SET `money` = `money` + 10 WHERE `id` = '".$user['id']."' LIMIT 1;");
                      CHAT::chat_attention($user, '��� �� ������� '.$matrix['collected_stones'].' � �������� ������������ ���� 10 �������.', array('addXMPP' => true));
                    }

                   file_put_contents($jewel_file, serialize($matrix), LOCK_EX);
                  } else {
                  # addchp ('<font color=red>��������!</font> �� ���� �������� � ����    ' , "BR");
                  }

            }

		}
	}

   if (empty($matrix['end_game'])) {
    $serv_time = time();
   } else {
    $serv_time = $matrix['end_game'];
   }

  echo "<data time=\"" . time() . "\"><matrix>";
  echo $drawmatrix;
  echo "</matrix></data>";

  die();
}

function load_default() {
	global $jewel_file, $save, $total_time;

    $magic_stones = sql_row("SELECT * FROM `magic_stones` WHERE `owner` = '".$_SESSION['uid']."' LIMIT 1;");

	if(is_file($jewel_file)) {

     $data_m = unserialize(file_get_contents($jewel_file));
     $matrix = array();
     $matrix = $data_m['matrix'];
     $data   = Game::setMatrix($matrix);

   # ������� ����� ������� ���� � ������ ��� �����
     if (!Game::moves()) {

		Game::init();
        $matrix_new = Game::getMatrix();

        $matrix                     = array();
 	   	$matrix['time_end']         = $data_m['time_end'];
	   	$matrix['collected_stones'] = $data_m['collected_stones'];
	   	$matrix['moves']            = $data_m['moves'];
		$matrix['matrix']           = $matrix_new;
        $matrix['end_game']         = $data_m['end_game'];

        if (is_writable($jewel_file)) {
          if ($save) file_put_contents($jewel_file, serialize($matrix), LOCK_EX);
        } else {
         # addchp ('<font color=red>��������!</font> 11111111 ������ ������� ����  ' , "BR");
        }

        $matrix = $matrix_new;

        if (empty($magic_stones['id'])) {
           sql_query("INSERT INTO `magic_stones` (`owner`, `collected_stones`, `moves`, `start`, `incity`, `start_game`, `end_game`) values ('".$_SESSION['uid']."', 0, 0, 1, '".INCITY."', ".time().", ".time().");");
        } else {
           sql_query("UPDATE `magic_stones` SET `start` = 1, `end_game` = ".time()." WHERE `owner` = '".$_SESSION['uid']."' LIMIT 1;");
        }
     }

	} else {

	   	Game::init();
        $data = Game::getMatrix();

        $matrix                     = array();
 		$matrix['time_end']         = time() + $total_time;
		$matrix['collected_stones'] = 0;
		$matrix['moves']            = 0;
		$matrix['matrix']           = $data;
        $matrix['end_game']         = 0;

        if ($save) {
   	      file_put_contents($jewel_file, serialize($matrix), LOCK_EX);
        }

        $matrix                     = $data;

        if (empty($magic_stones['id'])) {
          sql_query("INSERT INTO `magic_stones` (`owner`, `collected_stones`, `moves`, `start`, `incity`, `start_game`, `end_game`) values ('".$_SESSION['uid']."', 0, 0, 1, '".INCITY."', ".time().", ".time().");");
        } else {
          sql_query("UPDATE `magic_stones` SET `start` = 1, `end_game` = ".time()." WHERE `owner` = '".$_SESSION['uid']."' LIMIT 1;");
        }
	}

# ������� �������

  	echo "<data time=\"" . time() . "\"><matrix>";

        foreach($matrix as $x => $objects) {
         $n_x = $x + 1;

         echo "<col x=\"" . $n_x . "\">\n";
          foreach($objects as $y => $symbol) {
            $n_y = $y +1;
            echo "   <el y=\"" . $n_y . "\" type=\"".$symbol."\" />\n";
          }
         echo "</col>\n";
        }

	echo "</matrix></data>";

  die();
}



/***------------------------------------------
 * ������� � ����
 **/

switch($action) {
	case 'config':  load_config();  die(); break;
	case 'step':    load_step();    die(); break;
	case 'default': load_default(); die(); break;
	default:
}



 echo "<data>";
 echo "  <error text=\"����� �� �������\" code=\"0\" />";
 echo "</data>";
 die();

?>