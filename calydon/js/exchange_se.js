
function setSTRBuyBid(name, type, num) {
	var price = Math.round(document.getElementById('itemstr' + type + num).value);
	var quantity = Math.round(document.getElementById('strstr' + type + num).value);

	var total = Math.round(price * quantity * 100) / 100;
	var commission = Math.round(din_commission * quantity * price) / 100;

	if((price > 0) && (quantity > 0)) {
		if ( confirm('�� ������ ������� ������ �� ������� ' + quantity + ' ���. �� ����� ' + price + ' ��. / ���. ?\n\rC ��� ����� �������� ' + total + ' ��. � �������� ' + commission + ' ��. (' + din_commission + '% �� ����� �������)')) {
			document.location.href = '?cmd=exchange.buystr&type=' + type + '&num=' + num + '&price=' + price + '&quantity=' + quantity + '&level=' + current_level + '&nd=' + nd + '&' + Math.random();
		}
	} else {
		alert('�������� �����');
	}
}

function setSTRSellBid(name, entry, madein) {
	var price = Math.round(document.getElementById('item'+madein+entry).value);
	var quantity = Math.round(document.getElementById('stl'+madein+entry).value);

	var total = Math.round(price * quantity * 100) / 100;

	var commission = Math.round(stl_commission*quantity)/100;

	if (( price > 0 ) && ( quantity > 0 )) {
		if ( confirm('�� ������ ������� ������ �� ������� ' + quantity + ' ���. �� ����� ' + price + ' ��. / ���. ?\n\r����� ' + total + ' ��.\n\r����� �������� �������� � ������� ' + stl_commission + '% �� ����� ������� (' + commission + ' ���.)') ) {
			document.location.href = '?cmd=stl.sell&entry=' + entry + '&madein=' + madein + '&price=' + price + '&quantity=' + quantity + '&type=' + type + '&level=' + current_level + '&fname=' + fname + '&nd=' + nd + '&' + Math.random();
		}
	} else {
		alert('�������� �����');
	}
}

function sale(name, txt, n, kr){
	var s = prompt("����� � ������� \""+txt+"\". ������� ����:", kr);
	if ((s != null)&&(s != '')) {
		location.href="exchange.php?sale="+name+"&price="+s+"&n="+n;
	}
}
function chsale(name, txt, id, category, kr)
{
	var s = prompt("������� ���� ��� �������� \""+txt+"\". ������� ����� ����:", kr);
	if ((s != null)&&(s != '')) {
		location.href="exchange.php?unsale="+name+"&id="+id+"&sc="+category+"&price="+s;
	}
}


function setSellBid(name, entry, madein){
	var price = Math.round(parseFloat(document.getElementById('item' + madein + entry).value.replace(',', '.') * 100)) / 100;
	if (price > 0) {
		if (confirm('�� ������ ������� ������ �� ������� �������� \'' + name + '\' �� ' + price + ' ��. ?')) {
			document.location.href="?cmd=items.sell&sale="+name+"&price="+price+"&n="+entry;
		}
	} else {
		alert('�������� �����');
	}
}

function changeSellBid(id, name){
	var price = Math.round(parseFloat(document.getElementById('sellbid' + id).value.replace(',', '.') * 100)) / 100;
	var category = 1;
	if (price > 0) {
		if (confirm('�� ������ �������� ������ �� ������� �������� \'' + name + '\' �� ' + price + ' ��. ?')) {
			document.location.href="?cmd=mybids&unsale=1&id="+id+"&sc="+category+"&price="+price;
		}
	} else {
		alert('�������� �����');
	}
}

function changeSTRSellBid(id, name){
	var price = Math.round(parseFloat(document.getElementById('strsellbid' + id).value.replace(',', '.') * 100)) / 100;
	if (price > 0) {
		if (confirm('�� ������ �������� ���� ������� ��������� ��� ������ ������ �� ' + price + ' ��. / ���. ?')) {
			document.location.href = '?cmd=mybids.stl_change_sell&id=' + id + '&price=' + price + '&type=' + type + '&level=' + current_level + '&fname=' + fname + '&nd=' + nd + '&' + Math.random();
		}
	} else {
		alert('�������� �����');
	}
}

function changeSTRBuyBid(id, name){
	var price = Math.round(parseFloat(document.getElementById('strbuybid' + id).value.replace(',', '.') * 100)) / 100;
	if (price > 0) {
		if (confirm('�� ������ �������� ���� ������� ��������� ��� ������ ������ �� ' + price + ' ��. / ���. ?')) {
			document.location.href = '?cmd=mybids.stl_change_buy&id=' + id + '&price=' + price + '&type=' + type + '&level=' + current_level + '&fname=' + fname + '&nd=' + nd + '&' + Math.random();
		}
	} else {
		alert('�������� �����');
	}
}

function setBuyBid(name, num){
	var price = Math.round(parseFloat($('#item' + num).val().replace(',', '.') * 100)) / 100;

	if (price > 0) {
		if (confirm('�� ������ ������� ������ �� ������� �������� \'' + name + '\' �� ' + price + ' ��. ?')) {
			document.location.href = '?cmd=exchange&type=' + type + '&set=' + num + '&price=' + price + '&sid=' + '&level=' + current_level + '&' + Math.random();
		}
	} else {
		alert('�������� �����');
	}
}

function changeBuyBid(id, name){
	var price = Math.round(parseFloat(document.getElementById('buybid' + id).value.replace(',', '.') * 100)) / 100;
	if (price > 0) {
		if (confirm('�� ������ �������� ������ �� ������� �������� \'' + name + '\' �� ' + price + ' ��. ?')) {
			document.location.href = '?cmd=mybids.change_buy&id=' + id + '&price=' + price + '&type=' + type + '&level=' + current_level + '&fname=' + fname + '&nd=' + nd + '&' + Math.random();
		}
	} else {
		alert('�������� �����');
	}
}

function viewbids(type, num, count){
	if (count > 0) {
		document.write('<a href="#" onclick="this.blur(); javascript:{object=window.open(\'?cmd=exchange.allbids&type=' + type + '&num=' + num + '\',\'exchange_allbids\',\'height=400,width=' + (type == 70 ? '600' : '300') + ',resizable=no,scrollbars=yes,menubar=no,status=no,toolbar=no,top=\'+(window.screen.height-400)/2+\',left=\'+(window.screen.width-' + (type == 70 ? '600' : '300') + ')/2); object.focus();return false;}"><img alt="�������� ��� ������" src="http://img.blutbad.ru/i/b_zoom.gif" title="�������� ��� ������" /></a>');
	} else {
		document.write('<img alt="������ ���" class="disabled" src="http://img.blutbad.ru/i/b_zoom.gif" title="������ ���" />');
	}
}

function viewstats(type, num, count){
	if (count > 0) {
    	document.write('' +
    		'<a href="#" onclick="this.blur(); javascript:{object=window.open(\'?cmd=exchange.allstats&type=' + type + '&num=' + num + '\',\'exchange_allstats\',\'height=500,width=400,resizable=no,scrollbars=yes,menubar=no,status=no,toolbar=no,top=\'+(window.screen.height-500)/2+\',left=\'+(window.screen.width-400)/2); object.focus();return false;}">' +
    			'<img alt="�������� ����������" src="http://img.blutbad.ru/i/b_graph.gif" />' +
    		'</a>');
    } else {
    	document.write('<img alt="���������� ���" class="disabled" src="http://img.blutbad.ru/i/b_graph.gif" />');
    }

}

function STRbids(type, count, min, max){
	var darkcolor = type == 1 ? 'darkred' : 'darkgreen';
	var html = '';

	if (count > 0) {
		html += '' +
			'<table class="no-borders">' +
				'<tbody>' +
					'<tr>' +
						'<td class="nowrap">����������: <b>' + count + '</b></td>' +
					'</tr>' +
					'<tr>' +
						'<td align="right" class="nowrap">' +
							'<b style="color: ' + darkcolor + ';">' + min + '</b> - ' +
							'<b style="color: ' + darkcolor + ';">' + max + '</b>' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>';
	} else {
		html = '��� ������';
	}

	document.write(html);
}