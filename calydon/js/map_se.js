$(function() {
	$('.hover-minimap img').tooltip($.extend({}, tooltipDefaults, {
		bodyHandler: function() {
			return $(this).next('.invisible').html();
		},
		extraClass: 'tooltip-minimap'
	}));

});

if ($.browser.opera && $.browser.version < 10.5) {
	var dblClickTimeout;
	var clickDone = false;

	$(function(){
		$('.context').bind('click', function(event){
			if (!clickDone) {
				var $this = $(this);
				clickDone = true;
				dblClickTimeout = setTimeout(function(){
					var href = $this.attr('href');
					top.frames['main'].document.location.href = href;
					clickDone = false;
				}, 250);
			}

			return false;
		}).bind('dblclick', function(){
			clickDone = false;
			clearTimeout(dblClickTimeout);
		});
	});
}