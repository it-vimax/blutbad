$(function() {
	$('#global-search').click(function() {
		$('.global-search').toggleClass('invisible')
	});

	var loading = '<b>Загрузка&hellip;<b>';
	var options = {
		beforeSubmit: function(arr, $form, options) {
			$form.find('input[type=submit]')[0].disabled = true;
			$('#search-results').html(loading);
		},
		data: {
			is_ajax: 1
		},
		success: function(responseText, statusText, xhr, $form) {
			$form.find('input[type=submit]')[0].disabled = false;
		},
		target: '#search-results'
	};

	$('form').ajaxForm(options);

	$('#pagination a').live('click', function() {
		$('form').ajaxSubmit($.extend({}, options, {
			data: {
				is_ajax: 1,
				page: $(this).data('page')
			}
		}));

		return false;
	});
});
