/**
 * ���������� ������ ������� ������
 *
 * @param {Boolean} dot		���� true, �� ��������� ������� ������ �����
 * @return {String}			���������� ������� � �������
 */
function getDomain(dot) {
	var domain = document.domain;
	var arr = domain.split('.');
	var length = arr.length;

	return (dot ? '.' : '') + arr[length - 2] + '.' + arr[length - 1];
}


/*
 * ���������
 */
/**
 * ������������� �������������� ������������� � ���������
 *
 * @param {Object} el
 */
function initInventoryPropertyExpander(el) {
	jQuery(el ? el : '.properties-expander').click(function() {
		var ul = jQuery(this).nextAll('ul.invisible:first');
		this.src = imgURL + (ul.is(':visible') ? 'sb_plus.png' :'sb_minus.png');
		ul.toggle();
	}).tooltip(jQuery.extend({}, tooltipDefaults, {
		bodyHandler: function() {
			return '<ul>' + jQuery(this).nextAll('ul.invisible:first').html() + '</ul>';
		},
		extraClass: 'properties'
	}));
}
/* end: ��������� */

/**
 * ��������� "�����" � ���������
 */
function showStripes(listItems, start) {
	if(listItems && listItems.length) {
		if(start == undefined) {
			$(listItems).each(function(i) {
				if((i + 1) % 2) {
					$(this).removeClass('even').addClass('odd');
				} else {
					$(this).removeClass('odd').addClass('even');
				}
			});
		} else {
			var parity = ['even', 'odd'];
			$(listItems).each(function() {
				$(this).removeClass(parity[1 - start]).addClass(parity[start]);
				start = 1 - start;
			});
		}
	}
}

/**
 * �������������� ����� (��� ���)
 */
function formatWeight(weight) {
	if(weight != parseInt(weight)) {
		return String(weight.toFixed(3)).replace(/(\.0+|0+)$/, '');
	} else {
		return weight;
	}
}

/**
 * �������� ��������� �������
 *
 * @param {String} id		���������� ����� ��������
 * @param {String} name		�������� ��������
 * @param {String} back		�������� ������� ������� � ���������
 */

 /*
  "one": "�������",
  "two": "�������. ������������",
  "three": "�������. ���������"

  */
function dropitem(el, id, name, back) {
    if (confirm('��������� ������� "' + name + '"?') && confirm('������� ����� ������, �� �������?')) {
		$.post(file_inv, {
			back: back,
			cmd: 'item.drop',
			is_ajax: 1,
			user_id: user_id,
			user_hach: user_hach,
			item: id,
			nd: nd
		}, function(json) {
			var notify = $.pnotify({
				pnotify_title: '��������!',
				pnotify_type: json.error ? 'one' : '�������',
				pnotify_text: json.error ? json.error : json.message
			});
			notify.appendTo('#notifies').click(notify.pnotify_remove);

			if(json.message) {
				$(el).closest('.inventory-list-item').slideUp('fast', function() {
					var listItem = $(this);

					// ���������� ���������
					var weight = Number(listItem.find('.weight').text());
					var $cur_weight = $('.current-weight');
					$cur_weight.text(formatWeight(Number($cur_weight.text()) - weight));

					var $count = $('.total-count');
					$count.text(Number($count.text()) - 1);

					var start = listItem.hasClass('even') ? 0 : 1;
					var nextItems = listItem.nextAll(':not(.inventory-list-item-empty)');

					var list = listItem.parent();

					listItem.remove();

					if(list.children().length > 1) {
						showStripes(nextItems, start);
					} else {
						list.children().show();
					}
				});
			}
		}, 'json');
    }
}

/**
 * ����������� ��������� ��������� �� ���������
 *
 * @param {String} 	type
 * @param {String} 	num
 * @param {Integer} max		����������� ��������� ����� ������������� ���������
 * @param {String} 	name
 * @param {String} 	back	������� �������� ����� ������������ ��������
 */
function multi_drop(type, num, max, name, back, el) {
	var drop = getDialog();
	drop.dialog('option', 'title', '�������� "' + name + '"');

	var form = $.buildForm({
		'fields': [
			{ 'name': 'cmd',	'value': 'items.drop' },
			{ 'name': 'nd',		'value': nd},
			{ 'name': 'type',	'value': type },
			{ 'name': 'back',	'value': back },
			{ 'name': 'name',	'value': name },
			{ 'name': 'num',	'value': num },
			{ 'name': 'user_id',	'value': user_id },
			{ 'name': 'user_hach',	'value': user_hach },
			{
				'label': '����������',
				'group': [
					{
						'name': 'cnt',
						'size': 8,
						'type': 'text'
					}, {
						'click': function() {
							$('#cnt').val(max).focus();
						},
						'type': 'button',
						'value': '���'
					}
				]
			}, {
				'type': 'submit',
				'value': 'OK'
			}
		],
		'url': file_inv
	}).submit(function() {
		var cnt = Number($(this).find('#cnt').val());

		if(cnt != NaN && cnt > 0) {
			if (confirm('�������� ����� �������, �� �������?')) {
				return true;
			} else {
				return false;
			}
		} else {
			cnt = cnt > max ? max : cnt;

			alert('����� ����� �� ���� �!');
			return false;
		}
	});

	form.ajaxForm({
		data: {
			is_ajax: 1
		},
		dataType: 'json',
		success: function(json) {
			drop.dialog('close');

			// �����������
			showNotification(json.message, json.error ? true : false);

            if(json.error == false) {

			// ��������� �������
			var items = json.items;
			var listItem = $(el).closest('.inventory-list-item');
			if(items.length) {
				var durability = listItem.find('.durability');
				var fish_weight = listItem.find('.fish-weight');
				var valid = listItem.find('.valid');
				var weight = listItem.find('.weight');

				var data = {
					'durability': {},
					'fish_weight': {},
					'validdate': {},
					'weight': {}
				};

				var unique_counters = {
					'durability': 0,
					'fish_weight': 0,
					'validdate': 0,
					'weight': 0
				};

				for(var i = 0; i < items.length; i++) {
					var item = items[i];

					for(var key in data) {
						if(item[key]) {
							if(data[key][item[key]]) {
								data[key][item[key]]++;
							} else {
								data[key][item[key]] = 1;
								unique_counters[key]++;
							}
						}
					}
				}

				// ��.
				var max = listItem.find('.max').text();
				listItem.find('.max').text(items.length);
				var old_onclick = listItem.find('.multi_drop').attr("onclick");
				var new_onclick = old_onclick.replace("'"+max+"'","'"+items.length+"'");

				listItem.find('.multi_drop').attr("onclick",new_onclick);
				// �����
				var drop_weight = Number(weight.text());
				var total_weight = 0;
				for(var w in data['weight']) {
					total_weight += Number(w) * Number(data['weight'][w]);
					drop_weight -= Number(w) * Number(data['weight'][w]);
				}
				weight.text(formatWeight(total_weight));

				// ���������� ���������
				var $cur_weight = $('.current-weight');
				$cur_weight.text(formatWeight(Number($cur_weight.text()) - drop_weight));

				var $count = $('.total-count');
				$count.text(Number($count.text()) - (max - items.length));

				// �������������
				durability.children(':not(.label)').remove();

				if(unique_counters['durability'] > 1) { // ����� ������ � ����������� ����������
					var expander  = $('<img />', {
						'class': 'properties-expander',
						'src': imgURL + 'sb_plus.png'
					});
					expander.appendTo(durability);

					// ���������� ������
					var ul = $('<ul />', {
						'class': 'invisible'
					});

					for(var value in data['durability']) {
						$('<li />', {
							'html': data['durability'][value] + ' &times; ' + value
						}).appendTo(ul);
					}
					ul.appendTo(durability);

					initInventoryPropertyExpander(expander);
				} else { // ���� �������� �������� � ���������� ������ ��������
					for(var value in data['durability']) {
						$('<span />', {
							text: value
						}).appendTo(durability);
					}
				}

				// ��� (����)
				if(unique_counters['fish_weight']) {
					fish_weight.children(':not(.label)').remove();

					if(unique_counters['fish_weight'] > 1) {
						// ����� ������ � ����������� ����������
						fish_weight.children('.label').css('font-weight', 'bold');
						var expander  = $('<img />', {
							'class': 'properties-expander',
							'src': imgURL + 'sb_plus.png'
						});

						// ���������� ������
						var ul = $('<ul />', {
							'class': 'invisible'
						});
						var total_fish_weigh = 0;
						for(var mass in data['fish_weight']) {
							$('<li />', {
								'html': data['fish_weight'][mass] + ' &times; ' + mass + ' ��'
							}).appendTo(ul);
							total_fish_weigh += data['fish_weight'][mass] * mass;
						}

						$('<span />', {
							text: formatWeight(total_fish_weigh) + ' �� '
						}).appendTo(fish_weight)
						expander.appendTo(fish_weight);
						ul.appendTo(fish_weight);

						initInventoryPropertyExpander(expander);
					} else {
						// ���� �������� �������� � ���������� ������ ��������
						fish_weight.children('.label').css('font-weight', 'normal');
						for(var mass in data['fish_weight']) {
							$('<span />', {
								text: (Number(mass) * data['fish_weight'][mass]) + ' ��'
							}).appendTo(fish_weight);
						}
					}
				}

				// ����� ��
				valid.children(':not(.label)').remove();
				if(unique_counters['validdate'] > 1) {
					// ����� ������ � ����������� ����������
					valid.children('.label').css('font-weight', 'bold');

					var expander  = $('<img />', {
						'class': 'properties-expander',
						'src': imgURL + 'sb_plus.png'
					});
					expander.appendTo(valid);

					// ���������� ������
					var ul = $('<ul />', {
						'class': 'invisible'
					});

					for(var date in data['validdate']) {
						$('<li />', {
							'html': data['validdate'][date] + ' &times; ' + date
						}).appendTo(ul);
					}
					ul.appendTo(valid);

					initInventoryPropertyExpander(expander);
				} else {
					// ���� �������� �������� � ���������� ������ ��������
					valid.children('.label').css('font-weight', 'normal');
					for(var date in data['validdate']) {
						$('<span />', {
							text: date
						}).appendTo(valid);
					}
				}
			} else {
				listItem.slideUp('fast', function() {
					// ���������� ���������
					var weight = Number(listItem.find('.weight').text());
					var $cur_weight = $('.current-weight');
					$cur_weight.text(formatWeight(Number($cur_weight.text()) - weight));
                    // ���������
					var $count = $('.total-count');
					$count.text(Number($count.text()) - Number(listItem.find('.max').text()));
                    // ���������
					var start = listItem.hasClass('even') ? 0 : 1;
					var nextItems = listItem.nextAll(':not(.inventory-list-item-empty)');

					var list = listItem.parent();

					listItem.remove();

					if(list.children().length > 1) {
						showStripes(nextItems, start);
					} else {
						list.children().show();
					}
				});
			}
	     }
		}
	});

	drop.html(form);
	dialogOpen(drop);

	return false;
}


var groups_nums = {
	'items': 0,
	'runes': 1,
	'zak': 2,
	'el': 3,
	'foods': 4,
	'misc': 5
}

function put_off_error(slot, message) {
	if(message)
		pnotify(message, true);

	overlays.hide();
}
// �����
function put_off_item(img, slot_num) {
	if(IE6) {
		overlays.each(function() {
			$(this).width( $(this).parent().innerWidth() );
			$(this).height( $(this).parent().innerHeight() );
		});
	}
	overlays.show();

	var $img = $(img);

	// ������ ��� AJAX �������
	var url = file_inv;
	var data = {
		back: $('.ui-tabs-selected a').data('group'),
		cmd: 'item.unequip',
		is_ajax: 1,
		user_id: user_id,
		user_hach: user_hach,
		item: slot_num,
		nd: nd
	}

	var type_num = $(img).data('type-num');
	var type = type_num;
   //	var type = type_num.split('-')[0];
	var group;

	if( (type >= 1 && type <= 24) || type == 23 || (type >= 31 && type <= 38)) {
		group = 'items';
	} else if( type == 25 || type == 50 ) {
		group = 'runes';
	} else if( (type >= 36 && type <= 38) || type == 60 || type == 63 || type == 91 || type == 92 ) {
		group = 'stuff';
	}

	var slot = $img.parent();
	if (slot.hasClass("tooltip-next")) {
		slot = slot.parent();
	}

	$.post(url, data, function(json) {
		if(json) {
			if(json.error) {
				put_off_error(slot, json.error);
			} else if(json.message) {
			  	set_stats(json);

				if(group == 'items' || group == 'runes') {
					need_refresh[0] = true;
					need_refresh[1] = true;
				}

				// ��������� ������������� �������
				$group = $('#inventory-' + group);

				if($group.length) {
					if($group.is(':visible')) {
						load_tab(selected_ui, true, 1);
					} else {
						need_refresh[groups_nums[group]] = true;
						overlays.hide();
                        //load_tab(selected_ui, true, 1);
					}
				} else {
					overlays.hide();
				}

				slot.addClass(slot.data('slot-name') + '-empty').empty();

			}
		} else {
			put_off_error(slot, '�������� ����� �������');
		}
	}, 'json');


	return false;

 	location.href = '/inventory.php?cmd=item.unequip&nd=' + nd + '&item=' + slot_num + '&back=' + $('.ui-tabs-selected a').attr('href').replace(/\#([a-z]+)-\d+/, '$1').replace('is_ajax=1', '') + '&' + Math.random();
}

function get_inventory_item(data) {

}

function load_tab(ui, silent, treload) {
	if(!silent)
    	$(ui.panel).empty().append(loader);

    if (treload > 0) {

    	$.post(file_inv, {
    		back: $(ui.tab).data('group'),
    		is_ajax: 1,
    		user_id: user_id,
    		user_hach: user_hach,
    		nd: nd
    	}, function(html) {
    		$(ui.panel).html(html);
    		initInventoryPropertyExpander($(ui.panel).find('.properties-expander'));
    		need_refresh[ui.index] = false;

        	// ���������� ��������� ������������ ���������
            if ($('.inventory-table .tooltip-next').length) {
            	$('td.img').tooltip($.extend({}, tooltipDefaults, {
            		bodyHandler: function(){
            		   return $(this).nextAll('.invisible').html();
            		}
            	}));
            }

    		overlays.hide();
    	});
    } else {
      overlays.hide();
    }
}

function load_item(ui, el) {
	$.post(file_inv, {
		back: $(ui.tabs).data('group'),
		is_ajax: 1,
		user_id: user_id,
		user_hach: user_hach,
		nd: nd
	}, function(html) {
		var id = el[0].id;
		var item = $(html).find('#' + id);
		if(item.length) {
			el.html(item.html());
			initInventoryPropertyExpander(el.find('.properties-expander'));
		}
		need_refresh[ui.index] = false;

		el.slideDown('fast')

	   overlays.hide();
	});
}

function put_on_error(el, message) {
	pnotify(message, true);

	var start = el.hasClass('even') ? 0 : 1;
	showStripes(el.nextAll(':not(.inventory-list-item-empty)'), 1 - start);

	el.slideDown('fast');

	overlays.hide();
}

/*
 * AJAX ��������� ���������
 */
function put_on_item(item_id, slot_type, link) {
	// ���� �������� �� ���������
	var char_slot = $('ul.inv-items > .' + slot_type + ':empty:first');
	// ���� ������� ����� ��� - ���� ������
	if(!char_slot.length)
		char_slot = $('ul.inv-items > .' + slot_type + ':first');

	if(char_slot.length) {

		//char_slot.addClass('loading');
		if(IE6) {
			overlays.each(function() {
				var $this = $(this);
				$this.width($this.parent().innerWidth());
				$this.height($this.parent().innerHeight());
			});
		}

		overlays.show();
		// ������� ������ ��������� li
		var el = $(link).closest('.inventory-list-item');
		var is_stack = Number(el.data('stack'));
		var count = 1;
		if(is_stack)
			count = Number(el.find('.number .max').text());

		function process() {
			// ����� �������������� "�����" ��� ������
			var start = el.hasClass('even') ? 0 : 1;
			showStripes(el.nextAll(':not(.inventory-list-item-empty)'), start);

			// ���������� ������ �� ������
			$.post(file_inv, {
				back: selected_group,
				cmd: 'item.equip',
				is_ajax: 1,
				user_id: user_id,
				user_hach: user_hach,
				is_stack: is_stack,
				item: item_id,
				nd: nd
			}, function(json) {

				put_on_item_callback(json, el, char_slot, is_stack, count);
                // ������� ��������� �������� � ������
                $('.airpredmet').html("");
			}, 'json');
		}

		if(count == 1) {
			el.slideUp(process);
		} else {
			process();
		}

		return false;
	}
}

function put_on_item_callback(json, el, char_slot, is_stack, count) {
	//char_slot.removeClass('loading');
	if(json) {
		// ������� ������ ������ �� �����-�� �������
		if(json.error) {
			put_on_error(el, json.error);
		} else if(json.message) {
			char_slot.removeClass(char_slot.data('slot-name') + '-empty').empty();

			// ���� � ���������
			var slot = is_stack ? el.find('.slot').clone() : el.find('.slot');
			// �������� �������� � ���������
			var description = is_stack ? el.find('.item-description').clone() : el.find('.item-description');
			if(is_stack) {
				var durability = description.find('li.durability');
				if(durability.length && durability.children('.properties-expander').length) {
					durability.children(':not(.label)').remove();
					$('<span />', {
						text: json.items[0].durability
					}).appendTo(durability);
				}

				var valid = description.find('li.valid');
				if(valid.length && valid.children('.properties-expander').length) {
					valid.children(':not(.label)').remove();
					$('<span />', {
						text: json.items[0].validdate
					}).appendTo(valid);
				}
			}

			set_stats(json);

			// �������� �������� �������� ��� ���������
			var hidden_desc = $('<div />', {
				"class": "invisible",
				"html": description
			});

			// ��������� ������� � �������� � ����
			char_slot.append(slot.contents()).append(hidden_desc);
            char_slot.hide().fadeIn("500");

			// ���������� ���������
			char_slot.children('img.thumb').addClass('active').tooltip($.extend({}, tooltipDefaults, {
				bodyHandler: function(){
					return $(this).nextAll('.invisible').html();
				}
			})).removeAttr('onclick').click(function() {
				put_off_item(this, char_slot.data('slot'));
			});

			var list = el.parent();

			// ������� �������
			if(!is_stack || count == 1)
				el.remove();

			if(list.children().length == 1) {
				list.children().show();
			}

			if(selected_group == 'items') {
				need_refresh[0] = true;
				need_refresh[1] = true;
			}
			need_refresh[inventory.tabs('option', 'selected')] = true;

			// ������������ ������� �������
			 load_tab(selected_ui, true, json.item_n);
		} else { // ����������� ������
			put_on_error(el, json.error);
		}

		if(json.tutorial && top.currentCity != undefined) {
			top.highlightAllTutorialElements(json.tutorial);
		}
	} else { // ����������� ������
		put_on_error(el, json.error);
	}
}

function get_sign(value) {
	return value > 0 ? '+' : value < 0 ? '-' : '';
}

function set_stats( json ) {
	if(json.exp_kf >=0) {
		$('#exp_kf').text(json.exp_kf + '%');
	}

	var hppw = json.hppw;

	$('.hp-pw').HpPw({
		hp: {
			current: hppw.chp,
			max: hppw.mhp,
			speed: hppw.shp
		},
		pw: {
			current: hppw.cpw,
			max: hppw.mpw,
			speed: hppw.spw
		}
	});

	// ������ ������������ ���
	$('.max-weight').text(json.max_weight);

	var stats = json.allstats;
	var stats_ids = [ 'str', 'dex', 'suc', 'end', 'int', 'wis' ];
	for(var i in stats_ids) {
		var stat_id = stats_ids[i];

		$('#' + stat_id).text(stats[stat_id]);

		//stat_items = stats[stat_id + '_items'];
		//stat_tr = stats[stat_id + '_tr'];

		var add = stats[stat_id] - stats[stat_id + '_base'];

		if(add) {
			$('#' + stat_id + '_base').text(stats[stat_id + '_base']);

			//var add = stat_items - stat_tr;
			$('#' + stat_id + '_add').text(get_sign(add) + ' ' + Math.abs(add));

			$('#' + stat_id + '_info').removeClass('invisible');
		} else {
			$('#' + stat_id + '_info').addClass('invisible');
		}
	}

	var mf = json.allmf;
	for(var id in mf) {
		var value = mf[id];
		var text = Math.round(Math.abs(value) * 100) / 100;

		if(id != 'uron_min' && id != 'uron_max' && id != 'absorbs_uron' && id != 'absorbs_krit') {
			text = get_sign(value) + text;
		}
        if (id == 'absorbs_uron' || id == 'absorbs_krit') {
         text = (value ? "-" : "") + text + '%';
        }
        if (id == 'power_uron' || id == 'power_krit') {
         text = text + '%';
        }
		$('#' + id).text(text);
	}

	var actives = json.allactives;

}

var overlays;
var IE6 = $.browser.msie && $.browser.versionX == 6;

$(function(){
	overlays = $('#inv-overlay, #inventory-overlay');

	var kitDialog = $('#kit-dialog');
	if (kitDialog.length) {
		// ������ - �������� ���������
		var kit = $('#kit');

		// �������������� ���������� ���� ���������� ���������
		kitDialog.dialog({
			autoOpen: false,
			minHeight: 0,
			title: '��������� ��������',
			open: function(event, ui){
				kit.focus();
			},
			width: 'auto'
		});

		// ������� �� ����� �� ������ � ���������� ���������
		$('#save-kit-link').click(function(){

			dialogOpen(kitDialog);
			return false;
		});

		// AJAX-���������� ���������
		kitDialog.find('form').ajaxForm({
			data: {
				ajax: true,
				is_ajax: 1,
				user_id: user_id,
				user_hach: user_hach
			},
			dataType: 'json',
			resetForm: true,
			success: function(json) {
				showNotification(json.message, json.error ? true : false);

				if(!json.error) {
					kitDialog.dialog('close');

					var list = $('.stat.kits tbody ul');

					var isKitExists = false;

					list.find('.dress-up span').each(function() {
						if($(this).text() == json.name) {
							isKitExists = true;
						}
					});

					if(!isKitExists) {
						li = '' +
							'<li>' +
								'<img alt="������� ��������" class="delete" onclick="dropcomp(' + json.kit + ', \'' + json.name + '\', this);" src="' + imgURL + 'x.gif' + '" title="������� ��������" /> ' +
								'<a class="dress-up" href="'+file_inv+'?cmd=complect.equip&amp;nd=' + json.nd + '&amp;kit=' + json.kit + '&amp;user_id=' + user_id + '&amp;user_hach=' + user_hach + '&amp;' + Math.random() + '">' +
									'������ &laquo;<span>' + json.name + '</span>&raquo;' +
								'</a>' +
							'</li>';
						list.append(li);
						list.children(':first').addClass('invisible');
					}
				}
			}
		});
	}




if ($('.characteristics').length && $.ui && $.ui.sortable) {
		$('.characteristics').sortable({
			axis: 'y',
			cancel: '.toggle',
			handle: '.block',
			items: '.sortable',
			update: function(event, ui){
				var data = {
					cmd: 'block.shuffle',
					is_ajax: 1,
					user_id: user_id,
					user_hach: user_hach,
					nd: nd
				}

				$('.characteristics .sortable').each(function(i){
					data[ $(this).data('type') ] = i + 1;
				});

				$.post(file_inv, data);
			}
		});

    }

	//toggle message_body
	$(".message_head, .block").click(function(){

			var $this = $(this);
			var table = $this.closest('div.stat');
			//var blockName = table.find('.block img.move').attr('data');
			var blockName = table.parent().data('type');

			var imgToggle = table.find('.block img.toggle');
			var spanToggle = table.find('.block span.toggle');
			var blockToggle = table.find('.block');

			var closed = table.children('.message_body').css('display') == 'none' ? 1 : 0;

			$.post(file_inv, {
				block: blockName,
				cmd: 'block.toggle',
				toggle: 1 - closed
			});

	       	$(this).next(".message_body").slideToggle(500);

			if (closed) {
				imgToggle[0].src = imgURL + 'sb_minus.png';
				imgToggle[0].title = '��������';
                blockToggle.addClass("selblock");
			} else {
				imgToggle[0].src = imgURL + 'sb_plus.png';
				imgToggle[0].title = '����������';
                blockToggle.removeClass("selblock");
			}

		return false;
	});


 	initInventoryPropertyExpander();


   // ����� ��������� � ���������

   $("#search_item").keyup(function(){
     var text = $(this).val();

     if (text == "") {
        $("#clear_si").fadeOut("slow");
        $("#text_si").html("");
     } else {
        $("#clear_si").fadeIn("slow");
     }

     var items_list = $(".inventory-list");
     var items_� = 0;

        $('.inventory-list .inventory-list-item').each(function(i, elem) {

            if ($(this).hasClass("invisible")) {
        		//return false;
        	} else {
        	    $(elem).show();
        	}

            var alt = $(elem).find('.inventory-table .img .slot img').attr('alt');

            if (text && alt && alt.toLowerCase().indexOf(text.toLowerCase()) + 1) {
              console.info(alt + " = " + text);
              items_�++;
              $("#text_si").html("������� " + items_� + " " + suffix(items_�, "�������", "��������", "���������"));

            } else if(text != ""){
              $(elem).hide();
            }

        });

   });

   $("#search_item").submit(function(e){
    return false;
   });

   $("#clear_si").click(function(){
    $("#search_item").val("");
    $("#search_item").keyup();
    $("#text_si").html("");
    $(this).fadeOut("slow");
   });

   function resize_search_item() {
     if ($( window ).width() < 980) {
       $("#search_item").hide();
       $(".total-items").css({"left" : "50%"});
     } else if ($( window ).width() < 1150) {
       $("#search_item").show();
       $(".total-items").css({"left" : "70%"});
       $("#search_item").css({"width" : "100px"});
     } else if ($( window ).width() < 1350) {
       $("#search_item").show();
       $(".total-items").css({"left" : "70%"});
       $("#search_item").css({"width" : "200px"});
     } else {
       $("#search_item").show();
       $(".total-items").css({"left" : "50%"});
       $("#search_item").css({"width" : "200px"});
     }
   }

   resize_search_item();

   $( window ).resize(function() {
     resize_search_item();
   });


   $(".ui-state-default").click(function(){
    $("#search_item").keyup();
   });



    $('.ilink_put').live('click', function() {

      var pad = 20;

      if ($('.airpredmet').length == 0) {
       $('body').append('<div class="airpredmet">airpredmet</div>');
      }

      $('.airpredmet').html("");

      var block_img = $(this).parent().parent();
      var predm_data = $(this).parent().parent().parent().attr('data');

      if (predm_data == 25 || predm_data == undefined) {
        predm_data = 12;
      }
      if (predm_data == 20) {
        predm_data = 37;
      }

      $('.airpredmet').append(block_img.find(".slot").html());
      var im_top = block_img.offset().top + pad;
      var im_left = block_img.offset().left + pad;

      $('.airpredmet').css({'top': im_top, 'left': im_left, 'opacity': 1});

      $('.airpredmet').animate({
         opacity: 0,
         top: $('.inv-items .slot[data-slot="' + predm_data + '"]').offset().top + 'px',
         left: $('.inv-items .slot[data-slot="' + predm_data + '"]').offset().left + 'px'
      }, 700, function() {
         $('.airpredmet').html("");
      });

    });

    $('.inv-items .slot').live('click', function() {

      var pad = 20;

      if ($('.airpredmet').length == 0) {
       $('body').append('<div class="airpredmet">airpredmet</div>');
      }

      $('.airpredmet').html("");

      var block_img = $(this).find('img');
      $(this).find('.indicator').hide();


      $('.airpredmet').append(block_img);
      var im_top = $(this).offset().top + pad;
      var im_left = $(this).offset().left + pad;

      $('.airpredmet').css({'top': im_top, 'left': im_left, 'opacity': 1});

      $('.airpredmet').animate({
         opacity: 0,
         top: '100px',
         left: '550px'
      }, 700, function() {
        $('.airpredmet').html("");
      });

    });
});
