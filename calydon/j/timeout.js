var CurSecurityTimeout = 0;
var SecurityTimeoutEl = null;
var SecurityTimeoutBeg = Math.round(((new Date()).getTime()) / 1000);
var OnFinishTimeout = null;

function FormatSecurityTimeout(){
	var date = new Date();
	var timeout = CurSecurityTimeout - (Math.round(date.getTime() / 1000) - SecurityTimeoutBeg);
	if (timeout <= 0) 
		return 0;
	var hours = Math.floor(timeout / 3600);
	var minutes = Math.floor(timeout / 60 - hours * 60);
	var seconds = Math.floor(timeout - minutes * 60 - hours * 3600);
	
	var str = "";
	if (hours > 0) {
		if (hours > 0 && (hours - 1) % 10 == 0 && hours != 11) 
			str += hours + " ��� ";
		else if (hours > 1 && (hours - 2) % 10 == 0 && hours != 12) 
			str += hours + " ���� ";
		else if (hours > 2 && (hours - 3) % 10 == 0 && hours != 13) 
			str += hours + " ���� ";
		else if (hours > 3 && (hours - 4) % 10 == 0 && hours != 14) 
			str += hours + " ���� ";
		else str += hours + " ����� ";
	}
	if (minutes > 0) {
		if (minutes > 0 && (minutes - 1) % 10 == 0 && minutes != 11) 
			str += minutes + " ������ ";
		else if (minutes > 1 && (minutes - 2) % 10 == 0 && minutes != 12) 
			str += minutes + " ������ ";
		else if (minutes > 2 && (minutes - 3) % 10 == 0 && minutes != 13) 
			str += minutes + " ������ ";
		else if (minutes > 3 && (minutes - 4) % 10 == 0 && minutes != 14) 
			str += minutes + " ������ ";
		else str += minutes + " ����� ";
	}
	if (seconds > 0) {
		if (seconds > 0 && (seconds - 1) % 10 == 0 && seconds != 11) 
			str += seconds + " ������� ";
		else if (seconds > 1 && (seconds - 2) % 10 == 0 && seconds != 12) 
			str += seconds + " ������� ";
		else if (seconds > 2 && (seconds - 3) % 10 == 0 && seconds != 13) 
			str += seconds + " ������� ";
		else if (seconds > 3 && (seconds - 4) % 10 == 0 && seconds != 14) 
			str += seconds + " ������� ";
		else str += seconds + " ������ ";
	}
	if (!str) 
		str = "0 ������ ";
	str = str.slice(0, str.length - 1);
	SecurityTimeoutEl.data = str;
	return 1;
}

function UpdateSecurityTimeout(){
	if (FormatSecurityTimeout()) 
		window.setTimeout("UpdateSecurityTimeout();", 1000);
	else if (onFinishTimeout) 
		onFinishTimeout();
}

function InitSecurityTimeout(timeout, element, onfinishtimeout){
	SecurityTimeoutEl = document.getElementById(element);
	if (!SecurityTimeoutEl) 
		return;
	SecurityTimeoutEl = SecurityTimeoutEl.firstChild
	CurSecurityTimeout = timeout;
	onFinishTimeout = onfinishtimeout || null;
	UpdateSecurityTimeout();
}

function SimpleTimeout(timeout, element, onfinishtimeout) {
	this.element = document.getElementById(element);
	if (!this.element) return;
	
	this.element = this.element.firstChild
	this.timeout = timeout;
	this.onFinishTimeout = onfinishtimeout || null;
	this.timeout_beg = Math.round(((new Date()).getTime()) / 1000);
	this.UpdateSimpleTimeout();
}

SimpleTimeout.prototype.UpdateSimpleTimeout = function() {
	var t_this = this;
	if (this.ProgressSimpleTimeout()) window.setTimeout(function() { t_this.UpdateSimpleTimeout() }, 1000);
	else if (this.onFinishTimeout) this.onFinishTimeout();
};

SimpleTimeout.prototype.ProgressSimpleTimeout = function() {
	var date = new Date();
	var timeout = this.timeout - (Math.round(date.getTime() / 1000) - this.timeout_beg);
	if (timeout <= 0) return 0;
	this.element.data = timeout;
	return 1;
}
