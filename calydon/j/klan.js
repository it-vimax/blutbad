var members_counter = 0;
var members_online = 0;
var members = new Array();
var allied = new Array();
var allied_common;
var allied_wnd;
var abilities = new Array();

function big(type, num) {
	window.open("http://enc.blutbad.ru/" + type + "/" + num + ".html", "displayWindow", "height=600,width=540,resizable=yes,scrollbars=yes,menubar=no,status=no,toolbar=no,directories=no,location=no");
}

function all_private() {
	var priv = '';
	var too_long = false;
	
	if (members.length > 0) {
		for (var i = 0; i < members.length; i++) {
			if (priv.length + members[i].length + top.frames['down'].document.forms[0].text.value.length <= 239) {
				priv += 'private [' + members[i] + '] ';
			}
			else {
				too_long = true;
			}
		}
		top.frames['down'].window.document.F1.text.focus();
		top.frames['down'].document.forms[0].text.value = priv + top.frames['down'].document.forms[0].text.value;
		if (too_long) {
			alert("��������� ������������ ����� ���������. ����� ����������� ��� �� �������");
		}
	}
	else {
		alert("��� �������� � ������ ������ ����������");
	}
	return true;
}

function contact_hl(el, hl) {
	if (el.hasChildNodes()) {
		var cells = el.childNodes;
		for (i = 0; i < cells.length; i++) {
			if (hl == 1) {
				cells[i].style.background = "#cfd5df";
			}
			else {
				cells[i].style.background = "#dee2e7";
			}
		}
		return true;
	}
	return false;
}

function check_cups_form() {
	if (document.change_cups_form.change_cups.selectedIndex == 0) {
		alert("�������� �� �������");
		return false;
	}
	return true;
}

function close_children() {
	if ((allied_wnd) && (!allied_wnd.closed)) {
		allied_wnd.close();
	}
	
	return true;
}

function allied_show(idto) {
	var need_open = 0;
	if (!allied_wnd) {
		need_open = 1;
	}
	else {
		if (allied_wnd.closed) {
			need_open = 1
		}
		else {
			allied_wnd.focus();
		}
	}
	if (need_open) {
		allied_wnd = window.open('/klan.php?cmd=allied.list&klanid=' + idto, 'unions', 'width=400,height=500,toolbar=no,location=no,scrollbars=yes,resizable=yes');
	}
}

function klan_leave() {
	if (confirm("�� � ����� ���� ������ ����� �� �����?")) {
		if (confirm("�� �������?")) {
			if (confirm("��������� ��� ���, �� ������������� ������� �����?")) {
				document.relicts_form.cmd.value = "relicts.leave";
				document.relicts_form.submit();
				return true;
			}
		}
	}
	return false;
}

function klan_exclude(user, nd) {
	if (confirm("�� � ����� ���� ������ ��������� ��������� �� �����?")) {
		document.location.href = "/klan.php?cmd=relicts.exclude&area=members&exclude=" + user + "&nd=" + nd;
		return true;
	}
}

function klan_hall_enter(nd) {
	if (confirm("����� � �������� ���?")) {
		document.location.href = "/klan.php?cmd=relicts.klan_hall&nd=" + nd;
		return true;		
	}
}

function drop_ability(code, nd) {
	if (confirm("������������� ������� ��� �����������?\n�������� ������ ������������ �� �����!")) {
		document.location.href="/klan.php?cmd=drop_ability&code="+code+"&nd="+nd;
	}
}

function pause_ability(code, nd) {
	if (confirm("������������� ������������� ��� �����������?\n��� ������������� ����������� �������� ������� ���������!")) {
		document.location.href="/klan.php?cmd=pause_ability&code="+code+"&nd="+nd;
	}
}

function resume_ability(code, nd, money) {
	if (confirm("������������� ����������� ��� �����������?\n��� ������������� ����������� �������� ������� ��������� � "+money+" ������!")) {
		document.location.href="/klan.php?cmd=resume_ability&code="+code+"&nd="+nd;
	}
}

function increase_ability(code, nd) {
	if (confirm("������������� ��������� ����� ������� �� 1000 ������?")) {
		document.location.href="/klan.php?cmd=increase_ability&code="+code+"&nd="+nd;
	}
}

function confirm_s(text, loc){
	if (confirm(text)) {
			return  window.location = loc;
	} else {
		return false;
	}
}

function decrease_ability(code,nd) {
	if (confirm("������������� ��������� ����� ������� �� 1000 ������? �� ������������� ���� ����� ��������� ���������� �� �����.")) {
		document.location.href="/klan.php?cmd=decrease_ability&code="+code+"&nd="+nd;
	}
}

function access_remove(user_id,access,user,desc,nd) {
	if (confirm("������������� ������ ������ '"+desc+"' � ������������ "+user+"?")) {
		document.location.href="/klan.php?cmd=access.remove&user_id="+user_id+"&access="+access+"&nd="+nd;
	}
}

function confirm_cup2dinar(price) {
	var val = $("#cup2flor_input").val();
	val = val.replace(',','.');
	return confirm("������������� �������� "+$("#cup2flor_input").val()+" ������ �� "+(price*val)+" �������?");
}

function confirm_cup2war(price) {
	return confirm("������������� �������� " + price + " ������ �� ����� ���������� �����?");
}

function confirm_change_cups() {
	var cur_ability = $("#exchange-cups").val();
	var cur_price = 0;
	var cur_currency = "������";
	for (var i=0;i<abilities.length;i++) {
		if (abilities[i].code == cur_ability) {
			cur_price = abilities[i].price;
			if (abilities[i].currency && abilities[i].currency == "dinar") {
				cur_currency = "�������";
			} else if (abilities[i].currency && abilities[i].currency == "sterling") {
				cur_currency = "����������";
			}
			break;
		}
	}
	return confirm("������������� �������� ������ �� "+cur_price+" "+cur_currency+"?");
}

function acceptDialog(nd,can_bank) {
	var dialog = getDialog();

	if(!dialog) return false;

	var html = '';
	html += '' +
		'<form action="klan.php?' + Math.random() + '" class="nowrap" method="post">';

	html += '' +
		'<input name="cmd" type="hidden" value="relicts.accept" />' +
		'<input name="nd" type="hidden" value="'+nd+'" />';

	html += '' +
			'<label for="accept">�����:</label> ' +
			'<input class="text paste-nick" id="accept" name="accept" type="text" /> ';

	html += '' +
			'<select name="s">' +
				'<option value="2">�� ���� �������</option>' +
				'<option value="1">�� ��� ����</option>' +
				(can_bank ? '<option value="3">�� ���� �����</option>' : '') +
			'</select> ';

	html += '<input class="xgbutton" type="submit" value="OK" />' +
		'</form>';

	dialog.html(html);
	dialog.dialog('option', 'title', '�������');

	var input = $('#accept');
	// ������ ����� �� ���� ����� ��� ������ ����
	dialog.unbind('dialogopen').bind('dialogopen', function(event, ui) {
		input.focus();
	});
	if(input.is(':visible')) {
		input.focus();
	}

	dialogOpen(dialog);
}

function klan_private() {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.writeToEditor( '!���� ' );
	}
	return true;
}

function excludeDialog(nd,can_bank) {
	var dialog = getDialog();
	
	if(!dialog) return false;
	
	var html = '';
	html += '' +
		'<form action="/klan.php?' + Math.random() + '" class="nowrap" method="post">';
	
	html += '' +
		'<input name="cmd" type="hidden" value="relicts.exclude" />' +
		'<input name="nd" type="hidden" value="'+nd+'" />';
	
	html += '' +
			'<label for="accept">�����:</label> ' +
			'<input class="text paste-nick" id="exclude" name="exclude" type="text" /> ';

	if (can_bank) {
		html += '' +
			'<select name="s">' +
				'<option value="1">�� ��� ����</option>' +
				(can_bank ? '<option value="2">�� ���� �����</option>' : '') +
			'</select> ';
	}
	html += '<input class="xgbutton" type="submit" value="OK" />' +
		'</form>';

    Hint3Name = 'exclude';
	dialog.html(html);
	dialog.dialog('option', 'title', '���������');
	
	var input = $('#exclude');
	// ������ ����� �� ���� ����� ��� ������ ����
	dialog.unbind('dialogopen').bind('dialogopen', function(event, ui) {
		input.focus();
	});
	if(input.is(':visible')) {
		input.focus();
	}
	
	dialogOpen(dialog);	
}

$(function() {
	$("tr.stroks").hover(function() {
		$(this).addClass("hover");
	}, function() {
		$(this).removeClass("hover");
	});
	
	$('#edit-topic').click(function() {
		$('#topic-form .invisible, #topic').toggle();
		$('#topic-form input[name=topic]:visible').focus();
	});
	
	$('#delete-topic').click(function() {
	   //	$('#topic-form input.text').val('');
        $('#topic-form #cmd').val('topic.del');
		$('#topic-form').submit();
	});
	
	$('.elapsed-time').removeClass('invisible').each(function(){
		$(this).countdown({
			format: 'hms',
			layout: '{h<}{hn} {hl} {h>}{m<}{mn} {ml} {m>}{sn} {sl}',
			until: $(this).text()
		})
	});
	
	if(pages > 1) {
		$('.pagination-container').pagination(pages, $.extend({}, paginationDefaults, {
			current_page: current_page,
			link_to: link_to
		}));
	}
	
	var dates = jQuery('#date1, #date2').datepicker({
		maxDate: new Date(),
		minDate: new Date(2004, 1-1, 1),
		onSelect: function(selectedDate){
			var option = this.id == "date1" ? "minDate" : "maxDate";
			var instance = jQuery(this).data("datepicker");
			var date = jQuery.datepicker.parseDate(instance.settings.dateFormat || jQuery.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	});	
});
