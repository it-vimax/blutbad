var timePhrases = {
	0: '����������',
	15: '15 ���.',
	30: '30 ���.',
	60: '1 ���',
	180: '3 ����',
	360: '6 �����',
	720: '12 �����',
	1440: '1 �����',
	4320: '3 ���',
	10080: '1 ������',
	20160: '2 ������',
	43200: '1 �����',
	86400: '2 ������',
	129600: '3 ������',
	256200: '6 �������',
	525600: '1 ���'
}


function periodDialog(url, title, params) {
	var period = getDialog();

	if(!period) return false;

	var html = '';

	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="period-dialog-form" method="post">' +
			'<input name="cmd" type="hidden" value=' + params.cmd + ' />' +
            '<input type=hidden name="use" value="'+params.use+'">'+
			(params.nd ? '<input name="nd" type="hidden" value="' + params.nd + '" />' : '') +

			'<table>' +
				'<tbody>' +
					'<tr>' +
						'<td class="label">' +
							'<label for="' + params.nick + '">�����:</label>' +
						'</td>' +
						'<td class="input">' +
							'<input class="text paste-nick" type="text" id="' + params.nick + '" name="' + params.nick + '" />' +
						'</td>' +
					'</tr>';

	if(params.periods) {
		html +=		'<tr>' +
						'<td class="label">' +
							'<label for="period-time">����:</label>' +
						'</td>' +
						'<td class="input">' +
							'<select id="period-time" name="time">';

		for(var i in params.periods) {
			html +=				'<option value="' + params.periods[i] +'">' + timePhrases[params.periods[i]] + '</option>';
		}

		html +=				'</select>' +
						'</td>' +
					'</tr>';
	}

	html +=			'<tr>' +
						'<td class="label">' +
							'<label for="period-desc">�������:</label>' +
						'</td>' +
						'<td class="input" colspan="3">' +
							'<input class="text" id="period-desc" name="desc" type="text" />' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td></td>' +
						'<td>' +
							'<input class="xgbutton" type="submit" value="OK" />' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>' +
		'</form>';

	Hint3Name = params.nick;

	period.html(html);
	period.find('form').submit(function() {
		if ($('#period-desc').val() || confirm('�� ������������� �� ������ ��������� �������?s')) {
			return true;
		} else {
			return false;
		}
	});
	period.dialog('option', 'title', title);
	dialogOpen(period);
}

var adminranktimePhrases = {
	1.1:  '1 ����. (�������)',
	1.2:  '2 ����. (��������� ������)',
	1.3:  '3 ����. (��������)',
	1.4:  '4 ����. (����������)',
	1.5:  '5 ����. (���������� ���)',
	1.6:  '6 ����. (IT �����)'
}

function adminrankDialog(url, title, params) {
	var period = getDialog();

	if(!period) return false;

	var html = '';

	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="period-dialog-form" method="post">' +
			'<input name="cmd" type="hidden" value=' + params.cmd + ' />' +
            '<input type=hidden name="use" value="'+params.use+'">'+
			(params.nd ? '<input name="nd" type="hidden" value="' + params.nd + '" />' : '') +

			'<table>' +
				'<tbody>' +
					'<tr>' +
						'<td class="label">' +
							'<label for="' + params.nick + '">�����:</label>' +
						'</td>' +
						'<td class="input">' +
							'<input class="text paste-nick" type="text" id="' + params.nick + '" name="' + params.nick + '" />' +
						'</td>' +
					'</tr>';

	if(params.periods) {
		html +=		'<tr>' +
						'<td class="label">' +
							'<label for="period-rang">����:</label>' +
						'</td>' +
						'<td class="input">' +
							'<select id="period-rang" name="rang">';

		for(var i in params.periods) {
			html +=				'<option value="' + params.periods[i] +'">' + adminranktimePhrases[params.periods[i]] + '</option>';
		}

		html +=				'</select>' +
						'</td>' +
					'</tr>';
	}

	html +=			'<tr>' +
						'<td class="label">' +
							'<label for="period-desc">�������:</label>' +
						'</td>' +
						'<td class="input" colspan="3">' +
							'<input class="text" id="period-desc" name="desc" type="text" />' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td></td>' +
						'<td>' +
							'<input class="xgbutton" type="submit" value="OK" />' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>' +
		'</form>';

	Hint3Name = params.nick;

	period.html(html);
	period.find('form').submit(function() {
		if ($('#period-desc').val() || confirm('�� ������������� �� ������ ��������� �������?s')) {
			return true;
		} else {
			return false;
		}
	});
	period.dialog('option', 'title', title);
	dialogOpen(period);
}


function inputsDialog(url, title, params) {
	var select = getDialog();

	if(!select) return false;

	var html = '';
	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="select-dialog-form" method="post">' +
			'<input name="cmd" type="hidden" value=' + params.cmd + ' />' +
			(params.nd ? '<input name="nd" type="hidden" value="' + params.nd + '" />' : '') +

			'<table>' +
				'<tbody>' +
					'<tr>' +
						'<td class="label">' +
							'<label for="' + params.nick + '">�����:</label>' +
						'</td>' +
						'<td class="input">' +
							'<input class="text paste-nick" type="text" id="' + params.nick + '" name="' + params.nick + '" />' +
						'</td>' +
					'</tr>';

	if(params.comment) {
		html += 	'<tr>' +
						'<td></td>' +
						'<td>' + params.comment + '</td>' +
					'</tr>';
	}

	for(var i in params.inputs) {
		html +=		'<tr>' +
						'<td class="label">' +
							'<label for="' + params.inputs[i].name + '">' + params.inputs[i].label + ':</label>' +
						'</td>' +
						'<td class="input">' +
							'<input class="text" id="' + params.inputs[i].name + '" name="' + params.inputs[i].name + '" type="text" />' +
						'</td>' +
					'</tr>';
		if(params.inputs[i].comment) {
			html +=	'<tr>' +
						'<td></td>' +
						'<td>' + params.inputs[i].comment + '</td>' +
					'</tr>';
		}
	}

	html +=			'<tr>' +
						'<td></td>' +
						'<td>' +
							'<input class="xgbutton" type="submit" value="OK" />' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>' +
		'</form>';
	Hint3Name = params.nick;
	select.html(html);
	select.dialog('option', 'title', title);
	dialogOpen(select);
}



$(function() {
	var dates = $('.date-input');

	if (dates.length) {
		dates.datepicker({
			minDate: new Date(2013, 1 - 1, 1)
		});

		$('#changeuserbirthday').datepicker('option', 'minDate', new Date(1910, 1 - 1, 1));
		$('#changeuserbirthday').datepicker('option', 'maxDate', new Date());
		$('#klanratingdisabledate').datepicker('option', 'minDate', new Date(1910, 1 - 1, 1));
		$('#klanratingdisabledate').datepicker('option', 'maxDate', new Date());


		$('#report_date1, #report_date2').datepicker('option', 'onSelect', function(selectedDate){
			var option = this.id == 'report_date1' ? 'minDate' : 'maxDate';
			var instance = jQuery(this).data('datepicker');
			var date = jQuery.datepicker.parseDate(instance.settings.dateFormat || jQuery.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);

			var date_since = $('#report_date1');
			var date_till = $('#report_date2');
			var since = date_since.datepicker('getDate');
			var till = date_till.datepicker('getDate');
			var ten_days = 10 * 86400000;

			if(date_since.val() && date_till.val() && (till - since > ten_days)) {
				if(this.id == 'report_date1') {
					date_till.datepicker('setDate', new Date( since.getTime() + ten_days ) );
				} else {
					date_since.datepicker('setDate', new Date( till.getTime() - ten_days ) );
				}
			}
		});

		$('#log_date1, #log_date2').datepicker('option', 'onSelect', function(selectedDate){
			var option = this.id == 'log_date1' ? 'minDate' : 'maxDate';
			var instance = jQuery(this).data("datepicker");
			var date = jQuery.datepicker.parseDate(instance.settings.dateFormat || jQuery.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker('option', option, date);

			//if($('#enter-log:checked').length) {
			var date_since = $('#log_date1');
			var date_till = $('#log_date2');
			var since = date_since.datepicker('getDate');
			var till = date_till.datepicker('getDate');
			var year_days = 365 * 86400000;

			if(date_since.val() && date_till.val() && (till - since > year_days)) {
				if(this.id == 'log_date1') {
					date_till.datepicker('setDate', new Date( since.getTime() + year_days ) );
				} else {
					date_since.datepicker('setDate', new Date( till.getTime() - year_days ) );
				}
			}
			//}
		});
	}

	var form = $('form[name = adminfunctions]');
	if (form.length) {
		var allCheckbox = form.find('#all');
		var groups = form.find('input[id^=all]:not(#all)');
		var checkboxes = form.find('input[type=checkbox][name^=f]');

		function checkGroups() {
			if(groups.length == groups.filter(':checked').length) {
				allCheckbox[0].checked = true;
			} else {
				allCheckbox[0].checked = false;
			}
		}

		// ��������� ��������� ���
		$('form[name=adminfunctions] ul.group-block').each(function(){
			var checkboxes = $(this).find('input[type=checkbox]:not([id^=all])');
			if (checkboxes.filter(':checked').length == checkboxes.length) {
				$(this).find('input[type=checkbox][id^=all]')[0].checked = true;
			}
		});

		// ��������� ��������� ��� ��������� ���������
		if(!$('ul.group-block input[type=checkbox][id^=all]:not(:checked)').length) {
			$('#all')[0].checked = true;
		}

		// ��� ��������� ���������
		checkboxes.change(function() {
			var group = $(this).closest('ul').find('input[id^=all]');
			var groupCheckboxes = $(this).closest('ul').find('input[type=checkbox][name^=f]');
			if(groupCheckboxes.length == groupCheckboxes.filter(':checked').length) {
				group[0].checked = true;
			} else {
				group[0].checked = false;
			}
			checkGroups();
		});

		// ��� ��������� ���������
		groups.change(function(){
			var checked = this.checked;
			$(this).closest('ul').find('input[type=checkbox]').each(function() {
				this.checked = checked;
			})

			checkGroups();
		});

		// ��� ������ ��������
		allCheckbox.change(function() {
			var checked = this.checked;
			groups.each(function() {
				this.checked = checked;
			});
			checkboxes.each(function() {
				this.checked = checked;
			});
		});
	}
});
