var show_items = new Array();

function setSelect(name, value){
    var s = document.all[name];
    for (var i = 0; i < s.options.length; i++) {
        if (s.options[i].value == value) {
            s.selectedIndex = i;
            break;
        }
    }
}

function tavernaOut(number, taverna_main, taverna_property, taverna_request, count){
    taverna_main.durability0 = taverna_main.durability;
    taverna_main.durability = 0;
    var s = '<tr bgcolor=#dee2e7>';
    s += '<td valign=top align=center>';
    if (taverna_main.big == 1) {
        s += '<a target="_blank" href="' + taverna_main.image_full + '"><img src="' + taverna_main.image_preview + '" onmouseout="hh()" onmouseover="show_item(' + number + ',event)" /></a>'
    } else {
        s += '<img src="' + taverna_main.image_preview + '" onmouseout="hh()" onmouseover="show_item(' + number + ',event)" />'
    }
    s += '</td><td class="nowrap" align="left" valign="top" style="padding-left: 8px;">';
    s += description(taverna_main, taverna_property, taverna_request, count);
    s += '</td>';
    s += '<td class="nowrap" align="right" style="font-size: 8pt" valign="top">';
    s += '<table><tr>';
    var val = (count == 0) ? 0 : 1;
    s += '<td class="nowrap"><input style="text-align:right; background-color: #dee2e7;" name="buy_' + taverna_main.type + '_' + taverna_main.num + '" type="text" size="6" value="' + val + '"> ��.</td>'
    s += '<td valign="middle"><img src="http://img.blutbad.ru/i/baccept.gif"';
    if (val > 0) {
        s += ' style="cursor:hand;" alt="������" onclick="setBuy(\'' + taverna_main.name + '\',\'' + taverna_main.type + '\',\'' + taverna_main.num + '\');"'
    } else {
        s += ' class="gray"'
    }
    s += ' /></td>';
    s += '</tr></table></td>';
    s += '</tr>';
    document.write(s);
    show_items[number] = {
        'main': taverna_main,
        'property': taverna_property,
        'request': taverna_request
    };
}

function descriptionAlt(item_main, item_property, item_request){
    var st = 'style="color: red; background: url(http://img.blutbad.ru/i/bull_small_red.gif) 0px 2px no-repeat;"';
    var s = '';
    s = '<b>' + item_main.name + '</b> ';
    if (user.maxWeight < (user.itemsWeight + item_main.weight)) {
        s += '<font color="red">(�����: ' + item_main.weight + ')</font><br>'
    } else {
        s += '(�����: ' + item_main.weight + ')<br>'
    }
    s += descriptionMain(item_main, item_property, item_request, st);
    return s;
}

function description(item_main, item_property, item_request, count){
    var st = 'style="color: red; background: url(http://img.blutbad.ru/i/bull_small_red.gif) 0px 4px no-repeat;"';
    var s = '';
    if (count) {
        if (count == 0) {
            s += '<font color="red">����������: <i>' + count + '</font></i> (<a href="#" onclick="this.blur(); javascript:{object=window.open(\'?cmd=item.where&type=' + item_main.type + '&num=' + item_main.num + '\',\'show_where\',\'height=200,width=300,resizable=no,scrollbars=yes,menubar=no,status=no,toolbar=no,top=\'+(window.screen.height-200)/2+\',left=\'+(window.screen.width-300)/2); object.focus();return false;}">��� ��� ����� ����� ���� �������?</a>)<br>'
        } else {
            s += '����������: <i>' + count + '</i><br>'
        }
    }
    s += '<b>' + item_main.name + '</b> ';
    if (user.maxWeight < (user.itemsWeight + item_main.weight)) {
        s += '<font color="red">(�����: ' + item_main.weight + ')</font>'
    } else {
        s += '(�����: ' + item_main.weight + ')'
    }
    if (item_main.old == 1) {
        s += ' <img src="http://img.blutbad.ru/i/old.gif" alt="������� �������" />'
    }
    if (item_main.soulbound == 1) {
        s += ' <img src="http://img.blutbad.ru/i/soulbound.gif" alt="������� �������" />'
    }
    if (item_main.rolling == 1) {
        s += ' <img src="http://img.blutbad.ru/i/rolling.gif" alt="������� ���� � ������" />'
    }
    if (item_main.gift == 1) {
        s += ' <img src="http://img.blutbad.ru/i/present.gif" alt="������� �� ' + item_main.fromuser + '" />'
    }
    s += '<br>';
    s += descriptionMain(item_main, item_property, item_request, st);
    if (item_main.text) {
        s += '<b>�����:</b><br />' + item_main.text + '<br />'
    }
    return s;
}

function descriptionMain(item_main, item_property, item_request, st){
    var s = '';
    if (user.money < item_main.price) {
        s += '<font color="red"><i>����: <b>' + item_main.price + '</b>'+(item_main.old_price > 0 ? '(<b style="text-decoration:line-through">'+item_main.old_price+'</b>)' : '') +' ��.</i></font><br>'
    } else {
        s += '<i>����: <b>' + item_main.price + '</b>'+(item_main.old_price > 0 ? '(<b style="text-decoration:line-through">'+item_main.old_price+'</b>)' : '') +' ��.</i><br>'
    }
    s += '�������������: ' + item_main.durability + '/' + item_main.durability0 + '<br />';
    if (item_main.valid > 0) {
        s += '���� ��������: ' + item_main.valid + ' ��.<br />'
    }
    var s1 = '';
    if (item_request.level > 0) {
        if (user.level < item_request.level) {
            s1 += '<span ' + st + '>�������: ' + item_request.level + '</span>'
        } else {
            s1 += '<span>�������: ' + item_request.level + '</span>'
        }
    }
    if (item_request.str > 0) {
        if (user.strength < item_request.str) {
            s1 += '<span ' + st + '>����: ' + item_request.str + '</span>'
        } else {
            s1 += '<span>����: ' + item_request.str + '</span>'
        }
    }
    if (item_request.dex > 0) {
        if (user.dexterity < item_request.dex) {
            s1 += '<span ' + st + '>��������: ' + item_request.dex + '</span>'
        } else {
            s1 += '<span>��������: ' + item_request.dex + '</span>'
        }
    }
    if (item_request.suc > 0) {
        if (user.success < item_request.suc) {
            s1 += '<span ' + st + '>��������: ' + item_request.suc + '</span>'
        } else {
            s1 += '<span>��������: ' + item_request.suc + '</span>'
        }
    }
    if (item_request.end > 0) {
        if (user.endurance < item_request.end) {
            s1 += '<span ' + st + '>����������������: ' + item_request.end + '</span>'
        } else {
            s1 += '<span>����������������: ' + item_request.end + '</span>'
        }
    }
    if (item_request.intel > 0) {
        if (user.intelligence < item_request.intel) {
            s1 += '<span ' + st + '>���������: ' + item_request.intel + '</span>'
        } else {
            s1 += '<span>���������: ' + item_request.intel + '</span>'
        }
    }
    if (item_request.lic_war == 1) {
        if (user.warrior == 0) {
            s1 += '<span ' + st + '>�������� ��������</span>'
        } else {
            s1 += '<span>�������� ��������</span>'
        }
    }
    if (item_request.type == 43) {
        if (user.age < 18) {
            s1 += '<span ' + st + '>�������: �� 18 ���</span>'
        } else {
            s1 += '<span>�������: �� 18 ���</span>'
        }
    }
    if (s1) {
        s1 = '<b>����������:</b><br />' + s1
    }
    s += s1;
    var s0 = '';
    if (item_property.strength > 0) {
        s0 += '<span>����: +' + item_property.strength + '</span>'
    }
    if (item_property.dexterity > 0) {
        s0 += '<span>��������: +' + item_property.dexterity + '</span>'
    }
    if (item_property.success > 0) {
        s0 += '<span>��������: +' + item_property.success + '</span>'
    }
    if (item_property.endurance > 0) {
        s0 += '<span>������� �����: +' + item_property.endurance + '</span>'
    }
    if (item_property.weariness > 0) {
        s0 += '<span>������������: +' + item_property.weariness + '</span>'
    }
    if (item_property.intelligence > 0) {
        s0 += '<span>���������: +' + item_property.intelligence + '</span>'
    }
    if (item_property.krit > 0) {
        s0 += '<span>����������� ����: +' + item_property.krit + '</span>'
    }
    if (item_property.ukrit > 0) {
        s0 += '<span>������ ������������ �����: +' + item_property.ukrit + '</span>'
    }
    if (item_property.uvor > 0) {
        s0 += '<span>�����������: +' + item_property.uvor + '</span>'
    }
    if (item_property.uuvor > 0) {
        s0 += '<span>������ �����������: +' + item_property.uuvor + '</span>'
    }
    if (item_property.uronmin > 0) {
        s0 += '<span>����������� ����: +' + item_property.uronmin + '</span>'
    }
    if (item_property.uronmax > 0) {
        s0 += '<span>������������ ����: +' + item_property.uronmax + '</span>'
    }
    if (item_property.b1 > 0) {
        s0 += '<span>����� ������: +' + item_property.b1 + '</span>'
    }
    if (item_property.b2 > 0) {
        s0 += '<span>����� �������: +' + item_property.b2 + '</span>'
    }
    if (item_property.b3 > 0) {
        s0 += '<span>����� �����: +' + item_property.b3 + '</span>'
    }
    if (item_property.b4 > 0) {
        s0 += '<span>����� ���: +' + item_property.b4 + '</span>'
    }
    if (item_property.abrasion > 0) {
        s0 += '<span>����������� ������������: ' + item_property.abrasion + '%</span>'
    }
    if (item_property.duration > 0) {
        s0 += '<span>����������������� ��������: ' + item_property.duration + ' ���.</span>'
    }
    if (s0) {
        s0 = '<b>��������:</b><br />' + s0;
    }
    s += s0;
    if (item_main.action) {
        s += '<b>��������:</b><br>' + item_main.action;
    }
    if (item_main.descr) {
        s += '<b>��������:</b><br>' + item_main.descr + '<br />';
    }
    return s;
}

function movehint(ev){
    if (!ev)
        ev = window.event;
    var hint1 = document.getElementById('hint1');
    if (hint1 && hint1.style.visibility == 'visible') {
        setpos(ev);
    }
    return (true);
}

function big(type, num){
    window.open("http://enc.blutbad.ru/" + type + "/" + num + ".html", "displayWindow", "height=600,width=540,resizable=yes,scrollbars=yes,menubar=no,status=no,toolbar=no,directories=no,location=no")
}

function show_item(number, event){
    if (!event) {
		event = window.event;
	}

    var hint1 = document.getElementById('hint1');
    var da = '';
    var img = show_items[number];
    da += '<table border="0" cellspacing="0" cellpadding="0" class="hintspan"><tr>';
    da += '<td valign="top" align="left">';
    da += descriptionAlt(img.main, img.property, img.request) + '</td>';
    da += '</tr></table>';
    hint1.innerHTML = da;
    setpos(event);
    hint1.style.visibility = 'visible';
    document.onmousemove = movehint;
}

function setpos(event){
    var x, y;
    if (!event) {
		event = window.event;
	}

    var hint1 = document.getElementById('hint1');
    if (event.clientX + hint1.clientWidth + 20 >= document.body.clientWidth) {
        x = document.body.scrollLeft + event.clientX - hint1.clientWidth - 10;
    } else {
        x = event.clientX + document.body.scrollLeft + 10;
    }
    if (event.clientY + hint1.clientHeight + 20 >= document.body.clientHeight) {
        y = document.body.scrollTop + document.body.clientHeight - hint1.clientHeight - 20;
    } else {
        y = event.clientY + document.body.scrollTop + 20;
    }
    hint1.style.left = x;
    hint1.style.top = y;
}

function hh(){
    document.getElementById('hint1').style.visibility = 'hidden'
}

function setBuy(name, type, num){
    var currentInputName = 'buy_' + type + '_' + num;
    var count = Math.round(parseFloat(document.forms['tavern'].elements[currentInputName].value));
    if (isNaN(count) || count == 0) {
        alert("�������� ���������� ������!");
        document.forms['tavern'].elements[currentInputName].value = 1;
    } else {
        if (confirm("��������� �������������:\n ��� ����� - " + count + " ��. \"" + name + "\" ?")){ //+ "\" �� " + count+" ��. ?"))
            document.forms['tavern'].elements['cmd'].value = 'taverna.buy';
            document.forms['tavern'].elements['type'].value = type;
            document.forms['tavern'].elements['num'].value = num;
            document.forms['tavern'].elements['count'].value = count;
            document.forms['tavern'].submit();
        }
    }
}


$(function() {
	var tavernDialog = $('#tavern-dialog');
	if (tavernDialog.length) {
		tavernDialog.dialog({
			autoOpen: false,
			height: 'auto',
			hide: 'fade',
			minHeight: 0,
			open: function(event, ui) {
				$('#target').focus();
			},
			show: 'fade',
			width: 'auto'
		});

		$('.give-food').click(function() {
			tavernDialog.find('input[name=food_id]').val($(this).attr('data'));
			dialogOpen(tavernDialog);
		});
	}
});
