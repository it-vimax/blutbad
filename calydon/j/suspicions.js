function GoToPage(targetPage) {
	return '?cmd=suspicions_show&page=' + targetPage + 
		'&f_user=' + options.f_user + 
		'&f_city=' + options.f_city + 
		'&f_curcity=' + options.f_curcity + 
		'&f_type=' + options.f_type + 
		'&f_status=' + options.f_status + 
		'&f_date=' + options.f_date + 
		'&f_date2=' + options.f_date2 +
		'&rows_on_page=' + options.rows_on_page + 
		'&rand=' + Math.random();
}

function ShowPages(){
	if (pages > 1) {
		var html = '��������: ';
		if (page > 0) {
			html += '<a href="' + GoToPage(page - 1) + '" title="���������� ��������">&larr;</a> ';
		}
		
		var f = 0;
		
		for (var i = 0; i < pages; i++) {
			if (i > 0 && i < page - 10) {
				if (f < 1) {
					html += ", ...";
					f = 1;
				}
				continue;
			}
			
			if (i < pages - 1 && i > page + 10) {
				if (f < 2) {
					html += ", ...";
					f = 2;
				}
				continue;
			}
			
			if (i > 0) {
				html += ', ';
			}
			
			if (i == page) {
				html += '<b>' + (i + 1) + '</b>';
			} else {
				html += '<a href="' + GoToPage(i) + '">' + (i + 1) + '</a>';
			}
		}
		
		if (page < (pages - 1)) {
			html += ' <a href="' + GoToPage(page + 1) + '" title="��������� ��������">&rarr;</a>';
		}
		
		return html;
	} else {
		return '';
	}
}

function CheckWin(title, id, status, comment) {	
	var value = '<form action="suspicions.php?' + Math.random() + '" id="suspicion-form" method="post">';
	
	value += '<label for="comment">�����������:</label> <input class="text" id="comment" name="comment" size="60" type="text" />';
	
	value += '<input type="hidden" name="nd" value="' + options.nd + '" />';
	
	if (id == "selected") {
		value += '<input type="hidden" name="cmd" value="suspicions.status_selected" />';
		
		var ids = new Array();
		for (var i = 0; i < suspicion_ids.length; i++) {
			var checkbox_element = document.getElementById("select" + suspicion_ids[i]);
			if (checkbox_element && checkbox_element.checked) {
				ids.push(suspicion_ids[i]);
			}
		}
		
		if (ids.length == 0) {
			alert("�������������� �������� �� �������!");
			return false;
		}
		value += '<input type="hidden" name="ids" value="' + ids.join(',') + '" />';
	} else {
		value += '' +
			'<input type="hidden" name="cmd" value="suspicions.status" />' +
			'<input type="hidden" name="id" value="' + id + '" />';
	}
	
	value += '' +
			'<input type="hidden" name="page" value="' + page + '" />' +
			'<input type="hidden" name="status" value="'+status+'" />' +
			'<input type="hidden" name="f_user" value="' + options.f_user +'" />' +
			'<input type="hidden" name="f_city" value="' + options.f_city + '" />' +
			'<input type="hidden" name="f_curcity" value="' + options.f_curcity + '" />' +
			'<input type="hidden" name="f_type" value="' + options.f_type + '" />' +
			'<input type="hidden" name="f_status" value="' + options.f_status + '" />' +
			'<input type="hidden" name="f_date" value="' + options.f_date + '" />' +
			'<input type="hidden" name="f_date2" value="' + options.f_date2 + '" />' +
			'<input type="hidden" name="rows_on_page" value="' + options.rows_on_page + '" />' +
			
			' <input class="button" id="checkwin_button" type="submit" value="OK" />' +
		'</form>';
	
	suspicionsDialog.html(value);
	suspicionsDialog.dialog('option', 'title', title);
	if ($.browser.msie && $.browser.versionX < 8) {
		suspicionsDialog.dialog('option', 'width', 530);
	}
	dialogOpen(suspicionsDialog);
}

function ShowCommentEdit(id) {
	var comment = document.getElementById("comment_" + id);
	comment.style.display = "none";
	var comment_edit_href = document.getElementById("comment_edit_href_" + id);
	comment_edit_href.style.display = "none";
	var comment_edit_form = document.getElementById("comment_edit_form_" + id);
	comment_edit_form.style.display = "";
	var comment_edit_input = document.getElementById("comment_edit_input_" + id);
	comment_edit_input.focus();
}

function HideCommentEdit(id){
	var comment = document.getElementById("comment_" + id);
	comment.style.display = "";
	var comment_edit_href = document.getElementById("comment_edit_href_" + id);
	comment_edit_href.style.display = "";
	var comment_edit_form = document.getElementById("comment_edit_form_" + id);
	comment_edit_form.style.display = "none";
}

function selectall(/*el*/){
	var el = document.getElementById("selectall_checkbox");
	
	for (var i = 0; i < suspicion_ids.length; i++) {
		var susp_el = document.getElementById("select" + suspicion_ids[i]);
		if (!susp_el) {
			continue;
		}
		susp_el.checked = el.checked;
	}
	// �� �������
	/*if(el.checked) {
		$('.date-time-col input:checkbox').attr('checked', 'checked');
	} else {
		$('.date-time-col input:checkbox').removeAttr('checked');
	}*/
}

function filter(title) {
	$('input[name=f_user]').val(title)
	$('#filter_form').submit();
}

function ShowHistory(id){
	$("#history" + id).toggle();
}

var suspicion_ids = new Array();

var suspicionsDialog;

$(function() {
	/*if(typeof pages != 'undefined' && $('.pagination').length) {
		$('.pagination').html(ShowPages());
	}*/
	
	if(typeof pages != 'undefined' && pages > 1) {
		$('.pagination-container').pagination(pages, {
			callback: function(event, container) {
				container.prepend('��������:');
			},
			current_page: current_page,
			items_per_page: 1,
			link_to: link_to,
			next_text: '&rarr;',
			num_edge_entries: 1,
			next_show_always: false,
			prev_show_always: false,
			prev_text: '&larr;'
		});
	}
	
	var dates = $('#fdate, #fdate2').datepicker({
		maxDate: new Date()
	});
	
	$('#fdate').datepicker('option', 'onSelect', function(selectedDate){
		var instance = jQuery(this).data('datepicker');
		var date = jQuery.datepicker.parseDate(instance.settings.dateFormat || jQuery.datepicker._defaults.dateFormat, selectedDate, instance.settings);
		var till = $('#fdate2').datepicker('getDate');
		
		if(!till || till < date) {
			$('#fdate2').datepicker('setDate', date);
		}
	});
	
	$('#fdate2').datepicker('option', 'onSelect', function(selectedDate){
		var instance = jQuery(this).data('datepicker');
		var date = jQuery.datepicker.parseDate(instance.settings.dateFormat || jQuery.datepicker._defaults.dateFormat, selectedDate, instance.settings);
		var since = $('#fdate').datepicker('getDate');
		
		if(!since || since > date) {
			$('#fdate').datepicker('setDate', date);
		}
	});
	
	suspicionsDialog = $('#suspicions-dialog');
	if (suspicionsDialog.length) {
		suspicionsDialog.dialog({
			autoOpen: false,
			height: 'auto',
			minHeight: 0,
			open: function(event, ui) {
				setTimeout(function() { // ��� IE
					var comment = $('#comment');
					if (comment.length) {
						comment.focus();
					} else {
						$('#checkwin_button').focus();
					}
				}, 1);
			},
			width: 'auto'
		});
	}
});
