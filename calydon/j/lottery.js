var selected=0;

function selectElement(id) {
	var el=document.getElementById(id);
	if(el.className == 'selected') {
		el.className='released';
		selected--;
		if (0==selected) {
			document.getElementById('clearlink').style.display='none';
		}

		document.getElementById('buybutton').disabled=true;
	} else {
		if(selected<5){
			el.className='selected';
			selected++;
			document.getElementById('clearlink').style.display='inline';
		}
		if (5==selected) {
			document.getElementById('buybutton').disabled=false;
		}
	}
}

function clearIt() {
	for(i=1; i<26; i++) {
		var el=document.getElementById('C'+i);
		el.className='released';
		document.getElementById('buybutton').disabled=true;
		document.getElementById('clearlink').style.display='none';
	}
	selected=0;
}

function buyIt(form) {
	var nums = [];
	$('#new-lottery tbody td').each(function(i) {
		if($(this).hasClass('selected')) {
			nums.push(i + 1);
		}
	});

	form.ticket.value = nums.join(',');
	form.submit();
}

function clearSelected() {
	$('#new-lottery tbody td').removeClass('selected');
	$('#buybutton')[0].disabled = true;
	$('#reset-link').hide();

	return false;
}

$(function() {
	$('#new-lottery tbody td').click(function() {
		var $this = $(this);
		var selected = $('#new-lottery .selected');

		if($this.hasClass('selected') || selected.length < 5) {
			$this.toggleClass('selected');
		}

		var count = $('#new-lottery .selected').length;
		if(count) {
			$('#reset-link').show();
		} else {
			$('#reset-link').hide();
		}

		$('#buybutton')[0].disabled = (count == 5) ? false : true;
	});

	$('#form-buy-ticket').submit(function() {
		var nums = [];
		$('#new-lottery tbody td').each(function(i) {
			if($(this).hasClass('selected')) {
				nums.push(i + 1);
			}
		});

		$(this).find('input[name=ticket]').val(nums.join(','));

		return true;
	});
});