var ratio = 326 / 723;

function resizeFlash() {
	var flash = $('object.resizable');
	var container = flash.parent();

	flash.width(0);
	flash.height(0);

	var width = container.width();
	var height = $('html').height() - flash.offset().top - 22;

	if(width * ratio > height) {
		width = height / ratio;
	}

	if(width > 700) {
		if (width > 1700) {
			width = 1700;
		}

		flash.width(width);
		flash.height(width * ratio);
	} else {
		flash.width(700);
		flash.height(700 * ratio);
	}
}

function embedSWFCallback() {
	resizeFlash();
	$(window).resize(resizeFlash);
	if ($.browser.msie && $.browser.versionX < 8) {
		$(top.frames['chat'].window).resize(resizeFlash); // IE 6,7
	}
}