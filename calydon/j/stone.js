function send_klan(fid, kid){
    f = document.getElementById(fid);
    f.klan.value = kid;
    f.submit();

    return true;
}

function reject_union(fid, kid){
    f = document.getElementById(fid);
    f.reject.value = kid;

    return send_klan(fid, kid);
}

function cancel_union(fid, kid){
    if (confirm('��������� ����?'))
        return send_klan(fid, kid);

    return false;
}

$(function() {
	$('img.align:not([title])').each(function() {
		if (!this.title) {
			var res = this.src.toString().match(/align(\d{2})\.gif$/);
			if (res) {
				var code = res[1];
				var align = getAlignName(code);
				this.title = align;
			}
		}
	});
});
