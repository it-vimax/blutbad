if (!top.CheckFrame) {
	top.window.location = '/';
}

function formatTime(minutes, html){	
	var dCases = {
		nom: '����',
		gen: '���',
		plu: '����'
	}
	
	var hCases = {
		nom: '���',
		gen: '����',
		plu: '�����'
	}
	
	var mCases = {
		nom: '������',
		gen: '������',
		plu: '�����'
	}
	
	var time = '';
	
	var days = Math.floor(minutes / (24 * 60));
	if(days) {
		time += (html ? '<span class="nowrap">' : '') + days + ' ' + units(days, dCases) + (html ? '</span>' : '') + ' ';
		minutes -= days * (24 * 60);
	}
	
	var hours = Math.floor(minutes / 60);
	if(hours) {
		time += (html ? '<span class="nowrap">' : '') + hours + ' ' + units(hours, hCases) + (html ? '</span>' : '') + ' ';
		minutes -= hours * 60;
	}
	
	if (minutes) {
		time += (html ? '<span class="nowrap">' : '') + minutes + ' ' + units(minutes, mCases) + (html ? '</span>' : '') + ' ';
	}
	
	return time;
}

$(function(){
	var cities = $('.cities-list .city');
	if (cities.length) {
		cities.hover(
			function(){
				$(this).addClass('city-hover');
			}, function(){
				$(this).removeClass('city-hover');
			}
		).click(function(){
			cities.removeClass('city-selected');
			$(this).addClass('city-selected');
		});
	}
})
