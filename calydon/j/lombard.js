if (!top.CheckFrame) {
	location.href = '/';
}

var show_items = new Array();

function topawnOut(number, item_main, item_property, item_request, item_price){
    var s = '';
	s += '' +
		'<tr>' +
    		'<th>' + number + '</th>' +
    		'<td class="img">';
    if (item_main.big == 1) {
        s += 	'<a target="_blank" href="' + item_main.image_full + '">' +
					'<img src="' + item_main.image_preview + '" onmouseout="hh()" onmouseover="show_item(' + number + ',event)" />' +
				'</a>';
    } else {
        s += 	'<img src="' + item_main.image_preview + '" onmouseout="hh()" onmouseover="show_item(' + number + ',event)" />';
    }
    s += 	'</td>' +
			'<td class="lomb">' +
    			description(item_main, item_property, item_request) +
    		'</td>' +
    		'<td>' +
				'<ul class="actions">' +
					'<li>' +
						'<span>' + item_price + ' ��.</span> ' +
						'<img alt="��������" height="16" src="http://img.blutbad.ru/i/baccept.gif" onclick="setTopawn(\'' + item_main.name + '\',\'' + item_main.entry + '\',\'' + item_main.madein + '\',' + item_price + ');" title="��������" width="16" />' +
					'</li>' +
					'<li>' +
						'<span><i>� ����� ' + item_main.priceday + ' ��.</i></span> ' +
					'</li>' +
				'</ul>' +
			'</td>' +
    	'</tr>';

    document.write(s);

    show_items[number] = {
        'main': item_main,
        'property': item_property,
        'request': item_request
    };
}

function outpawnOut(number, item_main, item_property, item_request, item_price){
    var s = '';
	s += '' +
		'<tr>' +
    		'<th>' + number + '</th>' +
    		'<td class="img">';
    if (item_main.big == 1) {
        s += 	'<a target="_blank" href="' + item_main.image_full + '">' +
					'<img src="' + item_main.image_preview + '" onmouseout="hh()" onmouseover="show_item(' + number + ',event)" />' +
				'</a>';
    } else {
        s += 	'<img src="' + item_main.image_preview + '" onmouseout="hh()" onmouseover="show_item(' + number + ',event)" />';
    }
    s += 	'</td>' +
			'<td class="tal vat lomb"  style="padding-left: 8px;">' +
    			description(item_main, item_property, item_request) +
    		'</td>' +
    		'<td>' +
				'<ul class="actions">' +
					'<li>' +
						'<span>' + item_price + ' ��.</span> ' +
						'<img alt="��������" height="16" onclick="setOutpawn(\'' + item_main.name + '\',\'' + item_main.entry + '\',\'' + item_main.madein + '\',' + item_price + ');" src="http://img.blutbad.ru/i/baccept.gif" title="��������" width="16" />' +
					'</li>' +
					'<li>' +
						'<span><i>� ����� ' + item_main.priceday + ' ��.</i></span> ' +
					'</li>' +
				'</ul>' +
			'</td>' +
    	'</tr>';

    document.write(s);

    show_items[number] = {
        'main': item_main,
        'property': item_property,
        'request': item_request
    };
}

function descriptionAlt(item_main, item_property, item_request){
    var st = 'style="color: red;"';
    var s = '';
    s = '<b>' + item_main.name + '</b> ';
    if (user.maxWeight < (user.itemsWeight + item_main.weight)) {
        s += '<span class="purered">(�����: ' + item_main.weight + ')</span><br />'
    } else {
        s += '(�����: ' + item_main.weight + ')<br />'
    }
    s += descriptionMain(item_main, item_property, item_request, st);
    return s;
}

function description(item_main, item_property, item_request){
    var st = 'style="color: red;"';
    var s = '<b>' + item_main.name + '</b> ';
    if (user.maxWeight < (user.itemsWeight + item_main.weight)) {
        s += '<span class="purered">(�����: ' + item_main.weight + ')</span>'
    } else {
        s += '(�����: ' + item_main.weight + ')'
    }
    if (item_main.old == 1) {
        s += ' <img src="http://img.blutbad.ru/i/old.gif" alt="������� �������" title="������� �������" class="vam"/>'
    }
    if (item_main.soulbound == 1) {
        s += ' <img src="http://img.blutbad.ru/i/soulbound.gif" alt="������� �������" title="������� �������" class="vam"/>'
    }
    if (item_main.rolling == 1) {
        s += ' <img src="http://img.blutbad.ru/i/rolling.gif" alt="������� ���� � ������" title="������� ���� � ������" class="vam"/>'
    }
    if (item_main.gift == 1) {
        s += ' <img src="http://img.blutbad.ru/i/present.gif" alt="������� �� ' + item_main.fromuser + '" title="������� �� ' + item_main.fromuser + '" class="vam"/>'
    }
    s += '<br />';
    s += descriptionMain(item_main, item_property, item_request, st);
    if (item_main.text) {
        s += '<b>�����:</b><br />' + item_main.text + '<br />'
    }
    return s;
}

function descriptionMain(item_main, item_property, item_request, st){
    var s = '';
    if (user.money < item_main.price) {
        s += '<span style="color: #FF0000;">����: <b>' + item_main.price + '</b> ��.</span><br />';
    } else {
        s += '<i>����: <b>' + item_main.price + '</b> ��.</i><br />'
    }
    s += '�������������: ' + item_main.durability + '/' + item_main.durability0 + '<br />';
    if (item_main.valid > 0) {
        s += '���� ��������: ' + item_main.valid + ' ��.<br />'
    }
    var s1 = '';
	s1 += '<ul class="item-description-list">';
    if (item_request.level > 0) {
        if (user.level < item_request.level) {
            s1 += '<li ' + st + '>�������: ' + item_request.level + '</li>'
        } else {
            s1 += '<li>�������: ' + item_request.level + '</li>'
        }
    }
    if (item_request.str > 0) {
        if (user.strength < item_request.str) {
            s1 += '<li ' + st + '>����: ' + item_request.str + '</li>'
        } else {
            s1 += '<li>����: ' + item_request.str + '</li>'
        }
    }
    if (item_request.dex > 0) {
        if (user.dexterity < item_request.dex) {
            s1 += '<li ' + st + '>��������: ' + item_request.dex + '</li>'
        } else {
            s1 += '<li>��������: ' + item_request.dex + '</li>'
        }
    }
    if (item_request.suc > 0) {
        if (user.success < item_request.suc) {
            s1 += '<li ' + st + '>��������: ' + item_request.suc + '</li>'
        } else {
            s1 += '<li>��������: ' + item_request.suc + '</li>'
        }
    }
    if (item_request.end > 0) {
        if (user.endurance < item_request.end) {
            s1 += '<li ' + st + '>����������������: ' + item_request.end + '</li>'
        } else {
            s1 += '<li>����������������: ' + item_request.end + '</li>'
        }
    }
    if (item_request.intel > 0) {
        if (user.intelligence < item_request.intel) {
            s1 += '<li ' + st + '>���������: ' + item_request.intel + '</li>'
        } else {
            s1 += '<li>���������: ' + item_request.intel + '</li>'
        }
    }
    if (item_request.lic_war == 1) {
        if (user.warrior == 0) {
            s1 += '<li ' + st + '>�������� ��������</li>'
        } else {
            s1 += '<li>�������� ��������</li>'
        }
    }
	s1 += '</ul>';

    if (s1) {
        s1 = '<b>����������:</b>' + s1;
    }
    s += s1;
    var s0 = '';
    if (item_property.strength > 0) {
        s0 += '<li>����: +' + item_property.strength + '</li>';
    }
    if (item_property.dexterity > 0) {
        s0 += '<li>��������: +' + item_property.dexterity + '</li>';
    }
    if (item_property.success > 0) {
        s0 += '<li>��������: +' + item_property.success + '</li>';
    }
    if (item_property.endurance > 0) {
        s0 += '<li>������� �����: +' + item_property.endurance + '</li>';
    }
    if (item_property.weariness > 0) {
        s0 += '<li>������������: +' + item_property.weariness + '</li>';
    }
    if (item_property.intelligence > 0) {
        s0 += '<li>���������: +' + item_property.intelligence + '</li>';
    }
    if (item_property.krit > 0) {
        s0 += '<li>����������� ����: +' + item_property.krit + '</li>';
    }
    if (item_property.ukrit > 0) {
        s0 += '<li>������ ������������ �����: +' + item_property.ukrit + '</li>';
    }
    if (item_property.uvor > 0) {
        s0 += '<li>�����������: +' + item_property.uvor + '</li>';
    }
    if (item_property.uuvor > 0) {
        s0 += '<li>������ �����������: +' + item_property.uuvor + '</li>';
    }
    if (item_property.uronmin > 0) {
        s0 += '<li>����������� ����: +' + item_property.uronmin + '</li>';
    }
    if (item_property.uronmax > 0) {
        s0 += '<li>������������ ����: +' + item_property.uronmax + '</li>';
    }
    if (item_property.b1 > 0) {
        s0 += '<li>����� ������: +' + item_property.b1 + '</li>';
    }
    if (item_property.b2 > 0) {
        s0 += '<li>����� �������: +' + item_property.b2 + '</li>';
    }
    if (item_property.b3 > 0) {
        s0 += '<li>����� �����: +' + item_property.b3 + '</li>';
    }
    if (item_property.b4 > 0) {
        s0 += '<li>����� ���: +' + item_property.b4 + '</li>';
    }
    if (item_property.abrasion > 0) {
        s0 += '<li>����������� ������������: ' + item_property.abrasion + '%</li>';
    }
    if (item_property.duration > 0) {
        s0 += '<li>����������������� ��������: ' + item_property.duration + ' ���.</li>';
    }

    if (s0) {
        s0 = '<b>��������:</b><br />' + '<ul class="item-description-list">' + s0 + '</ul>';
    }

    s += s0;

    if (item_main.action) {
        s += '<b>��������:</b><br />' + item_main.action;
    }
    if (item_main.descr) {
        s += '<b>��������:</b><br />' + item_main.descr + '<br />';
    }
    return s;
}

function movehint(ev){
    if (!ev)
        ev = window.event;
    var hint1 = document.getElementById('hint1');
    if (hint1 && hint1.style.visibility == 'visible') {
        setpos(ev)
    }
    return (true);
}

function big(type, num){
    window.open("http://enc.blutbad.ru/" + type + "/" + num + ".html", "displayWindow", "height=600,width=540,resizable=yes,scrollbars=yes,menubar=no,status=no,toolbar=no,directories=no,location=no")
}

function show_item(number, event){
    if (!event)
        event = window.event;
    var da = '';
    var hint1 = document.getElementById('hint1');
    var img = show_items[number];
    da += '<table class="hintspan null"><tr>';
    da += '<td class="vat tal">';
    da += descriptionAlt(img.main, img.property, img.request) + '</td>';
    da += '</tr></table>';
    hint1.innerHTML = da;
    setpos(event);
    hint1.style.visibility = 'visible';
    document.onmousemove = movehint;
}

function setpos(event){
    var x, y;
    var hint1 = document.getElementById('hint1');
    if (event.clientX + hint1.clientWidth + 20 >= document.documentElement.clientWidth) {
        x = document.documentElement.scrollLeft + event.clientX - hint1.clientWidth - 10
    } else {
        x = event.clientX + document.documentElement.scrollLeft + 10
    }
    if (event.clientY + hint1.clientHeight + 20 >= document.documentElement.clientHeight) {
        y = document.documentElement.scrollTop + document.documentElement.clientHeight - hint1.clientHeight - 20
    } else {
        y = event.clientY + document.documentElement.scrollTop + 20;
    }
    hint1.style.left = x + 'px';
    hint1.style.top = y + 'px';
}

function hh(){
    document.getElementById('hint1').style.visibility = 'hidden';
}