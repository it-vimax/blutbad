

function alertBeforeCraft() {
	var oDialog = getDialog();

	oDialog.html('��������. ���������� ������ ��������� �����.');

	oDialog.dialog('option','title','������ ��������� �����');
	oDialog.dialog('open');

	$.get('forge.php?' + Math.random(), {
		is_ajax: 1,
		'nd' : nd,
		'cmd' : 'calc.pay',
		'recipe' : document.forms['craft'].recipe.value,
		'organic' : document.forms['craft'].organic.value,
		'crystal' : document.forms['craft'].crystal.value
	}, function (data) {
		var oDiv = $('<div />',{
			'css' : {
				'text-align' : 'center'
			}
		});

		if (RegExp(/^[\.\d]+$/).test(data)) {
			var message = '��� ����� ������ ��� ' + data + ' ';

			message = message + suffix(data,'�����','������','�������');
			message = message + '. ����������?';
		} else {
			message = data
		}

		oDiv.html(message + '<br /><br />');

		if (RegExp(/^[\.\d]+$/).test(data)) {
			$('<input />',{
				'type' : 'button',
				'value' : '����������',
				'class' : 'xbbutton',
				'click' : function() {
					document.forms['craft'].submit()
				}
			}).appendTo(oDiv);
		}

		oDialog.html(oDiv);
	})
}

function buyoutCurrentWork(id,timestart,timefinish,timerest,sterlinge,dinare) {
	if ($('#work_'+id+'_buyout').hasClass("disabled")) {
		return;
	}
	if (!dinare) dinare = 0;
	if (!sterlinge) sterlinge = 0;
	dinare = Number(dinare);
	sterlinge = Number(sterlinge);
	var diffTime = Number(timerest) - ((new Date()).getTime() - beginTime) / 1000;
	if (diffTime < 0) diffTime = 0;
	var diffTotalTime = Number(timefinish) - Number(timestart);

	if ((dinare == 0 && sterlinge == 0) || $('#work_'+id+'_elapsed_time').text() == "0 ���." || diffTime == 0) {
		return;
	}

	if (sterlinge != 0) {
		sterlinge = Number(sterlinge * (diffTime / diffTotalTime)).toFixed(2);
	}
	if (dinare != 0) {
		dinare = Number(dinare * (diffTime / diffTotalTime)).toFixed(2);
	}
	var price_text = '';
	if (sterlinge != 0) price_text += sterlinge+' ���.';
	if (dinare != 0) {
		if (price_text != '') price_text += ', ';
		price_text += dinare+' ��.';
	}
	if (confirm("������ ��� �� ���������. ��������� �� ��������� ���������� "+price_text+"?")) {
		document.location.href = '?cmd=work.get&cid='+id+'&fast_work=1&nd='+nd+'&'+rand;
	}
}

function getCurrentWork(id) {
	if ($('#work_'+id+'_get').hasClass("disabled")) {
		return;
	}
	document.location.href='?cmd=work.get&cid='+id+'&nd='+nd+'&'+rand;
}

function checkWorks() {
	for (var i=0; i < works.length; i++) {
		var work = works[i];
		var disable_get = false;
		var disable_buyout = false;
		var elapsed_time_el = $("#work_"+work.id+"_elapsed_time");
		var get_el = $("#work_"+work.id+"_get");
		var buyout_el = $("#work_"+work.id+"_buyout");

		if (elapsed_time_el.text() != "0 ���.") {
		   	disable_get = true;
		} else {
			disable_buyout = true;
		}

		if (disable_get) {
			if (
				(Number(work.fast_price.sterlinge) == 0 && Number(work.fast_price.dinare) == 0) ||
				Number(work.timerest) - ((new Date()).getTime() - beginTime) / 1000 <= 0
			) {
				disable_buyout = true;
			}
		}

		if (get_el.hasClass("disabled") != disable_get) {
			if (disable_get) get_el.addClass("disabled");
			else get_el.removeClass("disabled");
		}

		if (buyout_el.hasClass("disabled") != disable_buyout) {
			if (disable_buyout) buyout_el.addClass("disabled");
			else buyout_el.removeClass("disabled");
		}
	}

	setTimeout(checkWorks,1000);
}

$(function() {
	$('.elapsed-time').removeClass('invisible').each(function(){
		$(this).countdown({
			format: 'yodhms',
			layout: '{y<}{yn} {yl} {y>}{o<}{on} {ol} {o>}{d<}{dn} {dl} {d>}{h<}{hn} {hl} {h>}{m<}{mn} {ml} {m>}{sn} {sl}',
			until: $(this).text()
		})
	});

	checkWorks();
});