function onkeyup(e){
	// Let's remember about poor Explorer.
	// It does not pass e as an argument
	
	if (!e) {
		e = window.event;
	}
	
	var handler;
	
	if (window.top.frames['main'] && window.top.frames['main'].current_inventory_uid) {
		var uid = window.top.frames['main'].current_inventory_uid;
		if (window.top.frames['main'].shortkey_handler) {
			handler = window.top.frames['main'].shortkey_handler[uid]
		}
	}

	var code = e.keyCode;
	
	if (e.altKey || e.ctrlKey) {
		// short keys
		if (code == 49) {
			if (handler) {
				handler('items');
			} else {
				window.top.frames['main'].location = '/inventory.php?back=items'
			}
		} else if (code == 50) {
			if (handler) {
				handler('runes');
			} else {
				window.top.frames['main'].location = '/inventory.php?back=runes'
			}
		} else if (code == 51) {
			if (handler) {
				handler('animals');
			} else {
				window.top.frames['main'].location = '/inventory.php?back=animals'
			}
		} else if (code == 52) {
			if (handler) {
				handler('stuff');
			} else {
				window.top.frames['main'].location = '/inventory.php?back=stuff'
			}
		} else if (code == 53) {
			if (handler) {
				handler('misc');
			} else {
				window.top.frames['main'].location = '/inventory.php?back=misc'
			}
		} else if (code == 192) {
			window.top.frames['main'].document.location = '/map.php?' + Math.random();
		} else if (code == 81) {
			window.top.document.location = '/main.php?cmd=exit&' + Math.random();
		}/* else if (code == 70) {
			window.open('/forum.php');
		}*/
	}
}

document.onkeyup = onkeyup;