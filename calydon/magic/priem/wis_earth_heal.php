<?

/***--------------------------
 ****
 * ������ ����� "��� �������"
 *
 * ��� ������� ��������������� ��� 12% �� ������ ������ � 5-�� ���������� ����� ������� �� 2-10% ������ ������.
 ****
 **/

  if ($_SESSION['uid'] == null) die();

    $gn_hod    = abs($priem['n_hod']);
    $gn_hp     = abs($priem['n_hp']);
    $gn_udar   = abs($priem['n_udar']);
    $gn_block  = abs($priem['n_block']);
    $gn_uvorot = abs($priem['n_uvorot']);
    $gn_krit   = abs($priem['n_krit']);
    $gn_str_mind = abs($priem['n_str_mind']);

    $hp_pers_rand = rand(2, 10);
    $hp_me_rand   = 12;

    $my_hp_rand = $user_my->user['hp'];
    if ($user_my->user['maxhp'] > 0) {
      $my_hp_rand = $user_my->user['hp'] + ($user_my->user['maxhp'] * $hp_me_rand / 100);
      $my_hp_ad   = $user_my->user['maxhp'] * $hp_me_rand / 100;

      if (($user_my->user['hp'] + $my_hp_ad) > $user_my->user['maxhp']) {
        $my_hp_rand = $user_my->user['hp'] + ($user_my->user['maxhp'] - $user_my->user['hp']);
        $my_hp_ad   = $user_my->user['maxhp'] - $user_my->user['hp'];
      }
    }

    $my_hp_rand = floor($my_hp_rand);
    $my_hp_ad   = floor($my_hp_ad);

      $this->set_points('h', $my_hp_rand, $user_my->user['id']);
      $user_my->user['hp'] += $my_hp_ad;


        if (($gn_hod || $gn_hp || $gn_udar || $gn_block || $gn_uvorot || $gn_krit || $gn_str_mind) || $my_hp_ad) {
          sql_query("UPDATE `users` SET `hp` = `hp` + '".$my_hp_ad."', `hp_precise` = `hp_precise` + '".$my_hp_ad."',
                                        `hit_hod`    = `hit_hod` - ".$gn_hod.",
                                        `hit_hp`     = `hit_hp` - ".$gn_hp.",
                                        `hit_udar`   = `hit_udar` - ".$gn_udar.",
                                        `hit_block`  = `hit_block` - ".$gn_block.",
                                        `hit_uvorot` = `hit_uvorot` - ".$gn_uvorot.",
                                        `hit_krit`   = `hit_krit` - ".$gn_krit.",
                                        `str_mind`   = `str_mind` - ".$gn_str_mind."
                    WHERE `id` = '".$user_my->user['id']."' LIMIT 1;");

          $user_my->user['hit_hod']    -= $gn_hod;
          $user_my->user['hit_hp']     -= $gn_hp;
          $user_my->user['hit_udar']   -= $gn_udar;
          $user_my->user['hit_block']  -= $gn_block;
          $user_my->user['hit_uvorot'] -= $gn_uvorot;
          $user_my->user['hit_krit']   -= $gn_krit;
          $user_my->user['str_mind']   -= $gn_str_mind;
        }

      $sex = $user_my->user['sex']?"":"�";

        if ($user_my->user['invis'] == '1') {
          $my_hp_ad         = '??';
          $my_hp_rand       = '??';
          $user_my->user['maxhp'] = '??';
        }

    	$this->write_log();
        $parm = array('my_id' => $user_my->user['id'], 'he_id' => 0);
        $this->add_log(nick_team($user_my->user, $this->userclass($user_my->user['id'])).' �������'.$sex.' � ��� <b>��� �������</b>', $parm);

        if ($my_hp_ad > 0) {
          $this->write_log();
          $this->add_log('<b>��� �������</b> ���������� '.nick_team($user_my->user, $this->userclass($user_my->user['id'])).' ������� ����� <b class="bplus">+'.$my_hp_ad.'</b> ['.($my_hp_rand).'/'.$user_my->user['maxhp'].']', $parm);
        }

        $ico = 0;

       foreach ($this->team_mine as $k => $v_id) {
        if ($v_id != $user_my->user['id']) {
         if ($ico < 5) {
          $hp_ad = 0;
          if($v_id > _BOTSEPARATOR_) {
      	   $hp_bots = $this->bots[$v_id];
      	   $hp_user = sql_row("SELECT * FROM `users` WHERE id = '".$hp_bots['prototype']."' LIMIT 1;");

            if (!empty($hp_user['id'])) {

          	  $hp_user['id']     = $hp_bots['id'];
          	  $hp_user['hp']     = $hp_bots['hp'];
          	  $hp_user['login']  = $hp_bots['name'];
              $hp_user['bot_id'] = $hp_user['id'];

                 if ($hp_user['hp'] > 0 && $hp_user['maxhp'] > 0) {

                    $hp_rand = $hp_user['hp'] + ($hp_user['maxhp'] * $hp_pers_rand / 100);
                    $hp_ad   = $hp_user['maxhp'] * $hp_pers_rand / 100;

                    if (($hp_user['hp'] + $hp_ad) > $hp_user['maxhp']) {
                      $hp_rand = $hp_user['maxhp'];
                      $hp_ad   = $hp_user['maxhp'] - $hp_user['hp'];
                    }

                   $hp_rand = floor(abs($hp_rand));
                   $hp_ad   = floor(abs($hp_ad));

                   sql_query("UPDATE `bots` SET `hp` = `hp` + ".$hp_ad." WHERE `id` = '".$hp_user['id']."';");
                   $this->set_points('h', $hp_rand, $hp_user['id']);

                   $ico++;
                 }
            }
          } else {
    	      $hp_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$v_id."' LIMIT 1;");
              $hp_user['bot_id'] = $hp_user['id'];

               if ($hp_user['hp'] > 0 && $hp_user['maxhp'] > 0) {
                  $hp_rand = $hp_user['hp'] + ($hp_user['maxhp'] * $hp_pers_rand / 100);
                  $hp_ad   = $hp_user['maxhp'] * $hp_pers_rand / 100;

                  if (($hp_user['hp'] + $hp_ad) > $hp_user['maxhp']) {
                    $hp_rand = $hp_user['maxhp'];
                    $hp_ad   = $hp_user['maxhp'] - $hp_user['hp'];
                  }

                 $hp_rand = floor(abs($hp_rand));
                 $hp_ad   = floor(abs($hp_ad));

                 sql_query("UPDATE `users` SET `hp` = `hp` + '".$hp_ad."', `hp_precise` = `hp_precise` + '".$hp_ad."' WHERE `id` = '".$hp_user['id']."';");
                 $this->set_points('h', $hp_rand, $hp_user['id']);

                 $ico++;
               }
          }
           if ($hp_ad) {

            if ($hp_user['invis'] == '1') {
              $hp_ad            = '??';
              $hp_rand          = '??';
              $hp_user['maxhp'] = '??';
            }

            $this->add_log('<b>��� �������</b> ���������� '.nick_team($hp_user, $this->userclass($hp_user['id'])).' ������� ����� <b class="bplus">+'.$hp_ad.'</b> ['.($hp_rand).'/'.$hp_user['maxhp'].']', $parm);
           }
          }
         }
       }

       if ($ico) {
        $this->write_log();
       }
?>