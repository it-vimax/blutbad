<?

/***--------------------------
 ****
 * ������ ����� "�������� �����"
 *
 * ������� 8�� ���������� �������� ����� ������� 8% ������ ������ � 10% ������ ������������.
 ****
 **/

  if ($_SESSION['uid'] == null) die();

    $gn_hod    = abs($priem['n_hod']);
    $gn_hp     = abs($priem['n_hp']);
    $gn_udar   = abs($priem['n_udar']);
    $gn_block  = abs($priem['n_block']);
    $gn_uvorot = abs($priem['n_uvorot']);
    $gn_krit   = abs($priem['n_krit']);
    $gn_str_mind = abs($priem['n_str_mind']);

    $hp_pers_rand = rand(2, 10);
    $hp_me_rand   = 12;

    $hp_ad  = 0;
    $hp_cur = $user_my->user['hp'];
    if ($user_my->user['maxhp'] > 0) {
      $hp_ad = floor($user_my->user['maxhp'] * $hp_me_rand / 100);

      if (($user_my->user['hp'] + $hp_ad) > $user_my->user['maxhp']) {
        $hp_ad = floor($user_my->user['maxhp'] - $user_my->user['hp']);
      }
    }
      if ($hp_ad > 0) {
        sql_query("UPDATE `users` SET `hp` = `hp` + '".$hp_ad."', `hp_precise` = `hp_precise` + '".$hp_ad."' WHERE `id` = '".$user_my->user['id']."' LIMIT 1;");
        $this->set_points('h', $hp_cur + $hp_ad, $user_my->user['id']);

        $user_my->user['hp'] = $hp_cur + $hp_ad;
      }

  	  $this->write_log ();
      $parm = array('my_id' => $user_my->user['id'], 'he_id' => 0);
      $this->add_log(nick_team($user_my->user, $this->userclass($user_my->user['id'])).' �������� <b>�������� �����</b>', $parm);

      $ico          = 0;
      $ico_c        = 8;
      $hp_pers_rand = 8;
      $pw_pers_rand = 10;

     foreach ($this->team_enemy as $k => $v) {
      if ($v != $user_my->user['id']) {
       if ($ico < $ico_c) {
        $hp_ad        = 0;

        if($v > _BOTSEPARATOR_) {
    	  $hp_bots = sql_row("SELECT * FROM `bots` WHERE id = '".$v."' LIMIT 1;");
    	  $pr_user = sql_row("SELECT * FROM `users` WHERE id = '".$hp_bots['prototype']."' LIMIT 1;");
    	  $pr_user['id']      = $hp_bots['id'];
    	  $pr_user['hp']      = $hp_bots['hp'];
    	  $pr_user['maxhp']   = $hp_bots['maxhp'];
    	  $pr_user['mana']    = $hp_bots['mana'];
    	  $pr_user['maxmana'] = $hp_bots['maxmana'];
    	  $pr_user['login']   = $hp_bots['name'];
          $pr_user['bot_id']  = $pr_user['id'];

           if ($pr_user['hp'] > 1 && $pr_user['maxhp'] > 1 && $pr_user['mana'] > 1 && $pr_user['maxmana'] > 1) {

           #---------------------
           # �����
                $hp_m   = floor($pr_user['maxhp'] * $hp_pers_rand / 100);
                $hp_cur = floor($pr_user['hp'] - $hp_m);

                if ($hp_cur <= 0) {
                  $hp_cur = 1;
                  $hp_m   = $pr_user['hp'] - 1; # ���. 1 HP �� �������
                }

           #---------------------
           # ������������

                $pw_m   = floor($pr_user['maxmana'] * $pw_pers_rand / 100);
                $pw_cur = floor($pr_user['mana'] - $pw_m);

                if ($pw_cur <= 0) {
                  $pw_cur = 1;
                  $pw_m   = $pr_user['mana'] - 1; # ���. 1 PW �� �������
                }

              # ���� ������ ��� ���� ������
                if ($pw_cur > $pr_user['maxmana']) {
                  $pw_cur = $pr_user['maxmana'];
                  $pw_m   = 0;
                }

              # �������� ����� � �����
                sql_query("UPDATE `bots` SET `hp` = '".$hp_cur."', `mana` = '".$pw_cur."' WHERE `id` = '".$pr_user['id']."' LIMIT 1;");
                $this->set_points('h', $hp_cur, $pr_user['id']);
                $this->set_points('p', $pw_cur, $pr_user['id']);
           }
        } else {

  	      $pr_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$v."' LIMIT 1;");
          $pr_user['bot_id'] = $pr_user['id'];
          if ($pr_user['hp'] > 1 && $pr_user['maxhp'] > 1 && $pr_user['mana'] > 1 && $pr_user['maxmana'] > 1) {

           #---------------------
           # �����
                $hp_m   = floor($pr_user['maxhp'] * $hp_pers_rand / 100);
                $hp_cur = floor($pr_user['hp'] - $hp_m);

                if ($hp_cur <= 0) {
                  $hp_cur = 1;
                  $hp_m   = $pr_user['hp'] - 1; # ���. 1 HP �� �������
                }

           #---------------------
           # ������������

                $pw_m   = floor($pr_user['maxmana'] * $pw_pers_rand / 100);
                $pw_cur = floor($pr_user['mana'] - $pw_m);

                if ($pw_cur <= 0) {
                  $pw_cur = 1;
                  $pw_m   = $pr_user['mana'] - 1; # ���. 1 PW �� �������
                }

           # �������� ����� � �����
             sql_query("UPDATE `users` SET `hp` = '".$hp_cur."', `hp_precise` = '".$hp_cur."', `mana` = '".$pw_cur."', `pw_precise` = '".$pw_cur."' WHERE `id` = '".$pr_user['id']."' LIMIT 1;");
             $this->set_points('h', $hp_cur, $pr_user['id']);
             $this->set_points('p', $pw_cur, $pr_user['id']);

           }
        }
         if ($hp_m) {

          if ($pr_user['invis'] == '1') {
            $hp_m               = '??';
            $hp_cur             = '??';
            $pr_user['maxhp']   = '??';
            $pw_m               = '??';
            $pw_cur             = '??';
            $pr_user['maxmana'] = '??';
          }

          $this->add_log(nick_team($pr_user, $this->userclass($pr_user['id'])).' �������'.($pr_user['sex']?"":"a").' ���� �� <b>�������� �����</b> � �������'.($pr_user['sex']?"":"a").' ������� ������ <b>-'.$hp_m.'</b> ['.($hp_cur).'/'.$pr_user['maxhp'].']', $parm);
          $this->add_log(nick_team($pr_user, $this->userclass($pr_user['id'])).' �������'.($pr_user['sex']?"":"a").' ���� �� <b>�������� �����</b> � �������'.($pr_user['sex']?"":"a").' ������� ������������ <b>-'.$pw_m.'</b> ['.($pw_cur).'/'.$pr_user['maxmana'].']', $parm);
          //$this->fast_death_to($pr_user);
          $ico++;
         }
        }
       }
     }

      $this->write_log ();

    if ($gn_hod || $gn_hp || $gn_udar || $gn_block || $gn_uvorot || $gn_krit || $gn_str_mind) {
      sql_query("UPDATE `users` SET
                                    `hit_hod`    = `hit_hod` - ".$gn_hod.",
                                    `hit_hp`     = `hit_hp` - ".$gn_hp.",
                                    `hit_udar`   = `hit_udar` - ".$gn_udar.",
                                    `hit_block`  = `hit_block` - ".$gn_block.",
                                    `hit_uvorot` = `hit_uvorot` - ".$gn_uvorot.",
                                    `hit_krit`   = `hit_krit` - ".$gn_krit.",
                                    `str_mind`   = `str_mind` - ".$gn_str_mind."
                WHERE `id` = '".$user_my->user['id']."' LIMIT 1;");

      $user_my->user['hit_hod']    -= $gn_hod;
      $user_my->user['hit_hp']     -= $gn_hp;
      $user_my->user['hit_udar']   -= $gn_udar;
      $user_my->user['hit_block']  -= $gn_block;
      $user_my->user['hit_uvorot'] -= $gn_uvorot;
      $user_my->user['hit_krit']   -= $gn_krit;
      $user_my->user['str_mind']   -= $gn_str_mind;
    }

?>