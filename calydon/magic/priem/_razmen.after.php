<?

 $after_razmen = function ($mf, $r_me_user_id, $r_he_user_id, $enemy_id, $start_attacks){

    if (!empty($this->battle_eff[$r_me_user_id])) {

         $use_me_user['id']    = $this->get_points('id', $r_me_user_id);
         $use_me_user['login'] = $this->get_points('n',  $r_me_user_id);
         $use_me_user['invis'] = $this->get_points('i',  $r_me_user_id);
         $use_me_user['sex']   = $this->get_points('s',  $r_me_user_id);

         $use_he_user['id']    = $this->get_points('id', $r_he_user_id);
         $use_he_user['login'] = $this->get_points('n',  $r_he_user_id);
         $use_he_user['invis'] = $this->get_points('i',  $r_he_user_id);
         $use_he_user['sex']   = $this->get_points('s',  $r_he_user_id);

      foreach ($this->battle_eff[$r_me_user_id] as $priem => $act_priem) {

           switch($priem){

          #------------------------
          # ����������� ����
            case 'krit_crush': # ��� ��������� ���� ������� ������� ����.
             # ���� ���� ���� ��� ���� �� ���
              if (!empty($mf[$r_me_user_id]['user_udar']) || !empty($mf[$r_me_user_id]['user_krit'])) {
               # ������ ������� ����
                 $mf[$r_me_user_id]['udar'] *= 2;

               # ����� � ��� ���
                 $this->add_log(nick_team($use_me_user, $this->userclass($use_me_user['id'])).' ��������'.($use_me_user['sex']?"":"a").' ����� <b>"'.$act_priem['name'].'"</b> � �����'.($use_me_user['sex']?"":"�a").' ������� ����');

               # �������� ��� � ������� ����� � �������� ��� �������������
                 $this->battle_eff[$r_me_user_id][$priem]['hod']      -= 1;
                 if ($this->battle_eff[$r_me_user_id][$priem]['hod'] <= 0) unset($this->battle_eff[$r_me_user_id][$priem]);
               }
            break;

          #------------------------
          # ������ ������
            case 'block_fullshield': # �������� ���� ������� ��� �� ����� 1 �����������
             # ���� ���� ���� ��� ���� �� ���
              if (!empty($mf[$r_he_user_id]['user_udar']) || !empty($mf[$r_he_user_id]['user_krit'])) {

             # ������ ������� ����
               $mf[$r_he_user_id]['udar'] = 1;

             # ����� � ��� ���
               $this->add_log(nick_team($use_me_user, $this->userclass($use_me_user['id'])).' ��������'.($use_me_user['sex']?"":"a").' ����� <b>"'.$act_priem['name'].'"</b> � �������������'.($use_me_user['sex']?"":"a").' ����');

             # �������� ��� � ������� ����� � �������� ��� �������������
               $this->battle_eff[$r_me_user_id][$priem]['hod']      -= 1;
               if ($this->battle_eff[$r_me_user_id][$priem]['hod'] <= 0) unset($this->battle_eff[$r_me_user_id][$priem]);
              }
            break;

          #------------------------
          # �������� �����
            case 'krit_blooddrink': # ��������� ����������� ���� �� ��� ����� ��� ������ ����������� �����. ������� ��o�� ������� ��� ����.
              if ($mf[$r_he_user_id]['user_krit']) {

               $pr_new_uron      = floor($mf[$r_me_user_id]['udar']);

               $enemyhar_hp   = $this->get_points('h',  $r_me_user_id);
               $enemyhar_mhp  = $this->get_points('mh', $r_me_user_id);
               $enemyhar_pw   = $this->get_points('p',  $r_me_user_id);
               $enemyhar_mpw  = $this->get_points('mp', $r_me_user_id);

               if ($enemyhar_hp - $pr_new_uron <= 0) { $pr_new_uron = $enemyhar_hp; }

               $hp_add = floor($enemyhar_hp * 5 / 100);
               $pw_add = floor($enemyhar_pw * 5 / 100);

               if ($hp_add + $enemyhar_hp > $enemyhar_mhp) { $hp_add = $enemyhar_mhp - $enemyhar_hp; }
               if ($pw_add + $enemyhar_pw > $enemyhar_mpw) { $pw_add = $enemyhar_mpw - $enemyhar_pw; }

               if ($hp_add > 0 || $pw_add > 0) {
               # ���� ��
                 sql_query("UPDATE `users` SET `hp` = `hp` + '".$hp_add."', `hp_precise` = `hp_precise` + '".$hp_add."', `mana` = `mana` + '".$pw_add."', `pw_precise` = `pw_precise` + '".$pw_add."' WHERE `id` = '".$r_me_user_id."' LIMIT 1;");
                 $this->add_log(nick_team($use_me_user, $this->userclass($use_me_user['id'])).' ��������'.($use_me_user['sex']?"":"a").' ����� <b>"'.$act_priem['name'].'"</b>');
                 if ($pw_add > 0) {
                   $this->add_log(nick_team($use_me_user, $this->userclass($use_me_user['id'])).' ����������� ������� ������������ �� <b>+'.$pw_add.'</b> ['.($enemyhar_pw + $pw_add).'/'.$enemyhar_mpw.']');
                   $this->set_points('p', ($enemyhar_pw + $pw_add), $r_me_user_id);
                 }
                 if ($hp_add > 0) {
                   $this->add_log(nick_team($use_me_user, $this->userclass($use_me_user['id'])).' ����������� ������� ������ �� <b>+'.$hp_add.'</b> ['.($enemyhar_hp + $hp_add).'/'.$enemyhar_mhp.']');
                   $this->set_points('h', ($enemyhar_hp + $hp_add), $r_me_user_id);
                 }

               # �������� ��� � ������� ����� � �������� ��� �������������
                 $this->battle_eff[$r_me_user_id][$priem]['hod']      -= 1;
                 if ($this->battle_eff[$r_me_user_id][$priem]['hod'] <= 0) unset($this->battle_eff[$r_me_user_id][$priem]);
                }
               }
            break;

          #------------------------
          # �������� ����
            case 'multi_cowardshift': # ��������� ���� ���������� ��������� �� ����, ������ ���.
              $my_uron = floor($mf[$r_me_user_id]['udar']); # ��� ���� �� ����

              if (($mf[$r_he_user_id]['user_udar']/* || $mf[$r_he_user_id]['user_krit']*/)/* && $enemy_id == $r_me_user_id*/) {

               if (empty($mf[$r_he_user_id]['user_krit'])) { $u_ur = 1; } else { $u_ur = 2; }

                   $uenem['hp']    = $mf[$r_he_user_id]['user_hp'];//$this->get_points('h', $r_he_user_id);
                   $uenem['maxhp'] = $this->get_points('mh', $r_he_user_id);

                   $pr_new_uron = floor($mf[$r_he_user_id]['udar'] * $u_ur); # ��� ���� �� ����

                   if (/*$my_uron < $uenem['hp'] &&*/ $pr_new_uron > 0 && $u_ur == 1) {

                     if ($pr_new_uron >= $uenem['hp']) {
                      $pr_new_uron = $uenem['hp']; }

                     $hp_ad = floor($uenem['hp'] - $pr_new_uron);
                     $s_hp_ad = $hp_ad;

                   # ���. �� �� �������
                     if ($hp_ad <= 0) { # $hp_ad = 1; $pr_new_uron -= 1;
                        if ($start_attacks == 1 && $enemy_id == $r_he_user_id) {
                            $pr_new_uron -= $mf[$r_me_user_id]['udar'] - 1;
                            $hp_ad       += $mf[$r_me_user_id]['udar'] + 1;
                            $s_hp_ad      = $hp_ad;
                           // $hp_ad = 1;
                        } else {
                            $pr_new_uron -= $mf[$r_me_user_id]['udar'] - 1;
                            $hp_ad       += $mf[$r_me_user_id]['udar'] + 1;
                            $s_hp_ad      = 1;
                        }
                     }

                     if ($hp_ad < 0) {
                      $hp_ad = 0;
                     }

                   # �������� ��
                     $this->set_points('h', $hp_ad, $r_he_user_id);
                     if ($r_he_user_id > _BOTSEPARATOR_) {
                         sql_query("UPDATE `bots` SET `hp` = '".$hp_ad."' WHERE `id` = '".$r_he_user_id."' LIMIT 1;");
                     } else {
                         sql_query("UPDATE `users` SET `hp` = '".$hp_ad."', `hp_precise` = '".$hp_ad."' WHERE `id` = '".$r_he_user_id."' LIMIT 1;");
                     }

                   # �������� �� � �������
                     $mf[$r_he_user_id]['user_hp'] = $hp_ad;


                     if ($use_he_user['invis']) {
                       $pr_new_uron = "??";
                       $uenem['maxhp'] = "??";
                     }

                     if ($u_ur == 1) {
                       $pr_new_uron = '<b>-'.$pr_new_uron.'</b>';
                     } else {
                       $pr_new_uron = '<font color="red"><b>-'.$pr_new_uron.'</b></font>';
                     }


                     $this->add_log(nick_team($use_me_user, $this->userclass($use_me_user['id'])).' ��������'.($use_me_user['sex']?"":"a").' ����� <b>"'.$act_priem['name'].'"</b> � '.nick_team($use_he_user, $this->userclass($use_he_user['id'])).' �����'.($use_he_user['sex']?"":"�a").' ���� ����������� '.$pr_new_uron.' ['.$s_hp_ad.'/'.$uenem['maxhp'].']');

                   # ��� ���� �� ���
                     $mf[$r_he_user_id]['udar'] = 0;

                   # �������� ��� � ������� ����� � �������� ��� �������������
                     $this->battle_eff[$r_me_user_id][$priem]['hod']      -= 1;
                     if ($this->battle_eff[$r_me_user_id][$priem]['hod'] <= 0) unset($this->battle_eff[$r_me_user_id][$priem]);
                   }
              }
            break;


           }
      }
    }
  return $mf;
 };

# ������ ������� ��� ����
 $mf = $after_razmen($mf, $me_user_id, $he_user_id, $enemy_id, $start_attacks);

# ������ �� � �����
 if (isset($mf[$he_user_id]['user_hp']) && $enemy_id == $he_user_id) {
  // addchp ('<b>�������� 2222222222</b>!!! <b>'.$this_user['hp'].'</b> = <b>'.$he_user_id.'</b> = '.$mf[$he_user_id]['user_hp'].'  ', "BR");
  # $this_user['hp'] = $mf[$he_user_id]['user_hp'];
 }

# ������ ������� ��� ����
 $mf = $after_razmen($mf, $he_user_id, $me_user_id, $enemy_id, $start_attacks);

# ������ �� � �����
 if (isset($mf[$me_user_id]['user_hp']) && $enemy_id == $me_user_id) {
  // addchp ('<b>�������� 2222222222</b>!!! <b>'.$this_user['hp'].'</b> = <b>'.$me_user_id.'</b> = '.$mf[$me_user_id]['user_hp'].'  ', "BR");
  # $this_user['hp'] = $mf[$me_user_id]['user_hp'];
 }

?>