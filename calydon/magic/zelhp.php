<?php

global $caverooms, $canalrooms;

if ($user['battle'] > 0) {
    $_SESSION['rune_hint'] = "�� � ���...";
} elseif (($row['name'] == '����������� ��������' || $row['name'] == '����������� �������' || $row['name'] == '��������� ����' || $row['name'] == '������� ����') && !in_array($user['room'], $canalrooms) && !in_array($user['room'], $caverooms)) {
    $_SESSION['rune_hint'] = "����� ������������ ������ � ������� ����������.";
} else {
  $deltahp = 0;
  $deltamana = 0;
  $need = 0;

  if($row['name']=='����� �����' || $row['name']=='����� �����') $deltahp=100;
  if($row['name']=='������� �����') $deltahp=250;
  if($row['name']=='����� ����') $deltamana=200;
  if($row['name']=='������� ����') $deltamana=500;
  if($row['name']=='����������� ��������') $deltahp=150;
  if($row['name']=='����������� �������') $deltahp=300;
  if($row['name']=='��������� ����') $deltamana=250;
  if($row['name']=='������� ����') $deltamana=500;
  if($row['prototype'] == 2461 || $row['name']=='������ ������ �����') $deltahp=600;
  //getadditdata($user);
  if ($deltahp) {
    $deltahp=floor($deltahp*(addictval($user["hp"])*0.04+1));
    $need=$user["hp"]<$user["maxhp"];
  }
  if ($deltamana) {
    $deltamana=floor($deltamana*(addictval($user["mana"])*0.04+1));
    $need=$user["mana"]<$user["maxmana"];
  }
  if ($need) {
    mq("update users set ".($deltahp?"hp=if(hp+$deltahp>maxhp, maxhp, hp+$deltahp), `fullhptime` = ".time()."":"")." ".($deltamana?"mana=if(mana+$deltamana>maxmana, maxmana, mana+$deltamana), `fullmptime` = ".time()."":"")." where id='$user[id]'");

    if ($deltahp) $_SESSION['rune_hint'] = "����� ������� &quot;".$row['name']."&quot;. ������� ������������� ".min($deltahp,$user["maxhp"]-$user["hp"])." ��. ������ �����.";
    if ($deltamana) $_SESSION['rune_hint'] = "����� ������� &quot;".$row['name']."&quot;. ������� ������������� ".min($deltamana,$user["maxmana"]-$user["mana"])." ��. ������ ������������.";

    $user["hp"]=min($user["hp"]+$deltahp, $user["maxhp"]);
    $user["mana"]=min($user["mana"]+$deltamana, $user["maxmana"]);

    $bet=1;
  } elseif ($deltahp) $_SESSION['rune_hint'] = "�� � ��� �������.";
  elseif ($deltamana) $_SESSION['rune_hint'] = "���� ������������ � ��� ������.";
}

?>