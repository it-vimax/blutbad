﻿<?
 /*
Составляем списки друзей
Составляем списки друзей
#Распределение нагрузки, начало в 13мс
	/1_s.php?cmd=appserv3  загрузилось норм
	Время ответа от сервера: 140мс
#1 Обработка параметров, начало в 154мс
	ничего не загружалось, все ОК
Загадываем слова
Загадываем слова
#2 Загрузка прелоадера, начало в 155мс
LoaderGraphic.swf?v=5, [HTTPStatusEvent type="httpStatus" bubbles=false cancelable=false eventPhase=2 status=404 redirected=false responseURL=null]
	LoaderGraphic.swf?v=5  не загрузилось
<a href="LoaderGraphic.swf?v=5" target="blank" >Открыть в новом окне</a>
	LoaderGraphic.swf?v=5  не загрузилось
<a href="LoaderGraphic.swf?v=5" target="blank" >Открыть в новом окне</a>
Ошибка при Загрузка прелоадера:[IOErrorEvent type="ioError" bubbles=false cancelable=false eventPhase=2 text="Error #2036"]
	попробованы все варианты загрузки статики
Информация о системе
	Операционная система - Windows 8
	Тип флеш плеера - PlugIn
	Версия флеш плеера - WIN 20,0,0,286


*/

if (@$_GET['cmd'] == "tournaments") {
 die('
[{
	"tour_count": 6,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0418\u0433\u0440\u0443\u0448\u0435\u0447\u043d\u044b\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 1,
	"prizeRatio": 0.3,
	"id": 1,
	"totalPlaces": 4,
	"img": {
		"main": "1.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "1\/4.png?2",
			"2": "1\/4.png?1",
			"3": "1\/4.png?1",
			"4": "1\/3.png?1",
			"5": "1\/2.png?1",
			"6": "1\/1.png?1"
		}
	},
	"tournamentworktime": 720,
	"admComm": 30,
	"prizeTourCount": 3
}, {
	"tour_count": 6,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0421\u043b\u0430\u0434\u043a\u0438\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"prizeRatio": 0.3,
	"id": 2,
	"totalPlaces": 4,
	"img": {
		"main": "2.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "2\/4.png?2",
			"2": "2\/4.png?1",
			"3": "2\/4.png?1",
			"4": "2\/3.png?1",
			"5": "2\/2.png?1",
			"6": "2\/1.png?1"
		}
	},
	"tournamentworktime": 960,
	"admComm": 30,
	"prizeTourCount": 3
}, {
	"tour_count": 7,
	"cost": 150,
	"exitcost": 15,
	"required_rating": 110,
	"title": "\u0420\u043e\u043c\u0430\u043d\u0442\u0438\u0447\u0435\u0441\u043a\u0438\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 1,
	"prizeRatio": 0.3,
	"id": 3,
	"totalPlaces": 12,
	"img": {
		"main": "3.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "3\/4.png?2",
			"2": "3\/4.png?1",
			"3": "3\/4.png?1",
			"4": "3\/4.png?1",
			"5": "3\/3.png?1",
			"6": "3\/2.png?1",
			"7": "3\/1.png?1"
		}
	},
	"tournamentworktime": 840,
	"admComm": 30
}, {
	"tour_count": 8,
	"cost": 100,
	"exitcost": 10,
	"required_rating": 110,
	"title": "\u0426\u0432\u0435\u0442\u043e\u0447\u043d\u044b\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"prizeRatio": 0.4,
	"id": 4,
	"totalPlaces": 6,
	"img": {
		"main": "4.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "4\/4.png?1",
			"2": "4\/4.png?1",
			"3": "4\/4.png?1",
			"4": "4\/4.png?1",
			"5": "4\/4.png?1",
			"6": "4\/3.png?1",
			"7": "4\/2.png?1",
			"8": "4\/1.png?1"
		}
	},
	"tournamentworktime": 960,
	"admComm": 30,
	"prizeTourCount": 3
}, {
	"tour_count": 9,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0410\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044c\u043d\u044b\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 1,
	"prizeRatio": 0.5,
	"id": 5,
	"totalPlaces": 6,
	"img": {
		"main": "5.png?3",
		"blank": "blank.png?1",
		"tours": {
			"1": "5\/4.png?1",
			"2": "5\/4.png?1",
			"3": "5\/4.png?1",
			"4": "5\/4.png?1",
			"5": "5\/4.png?1",
			"6": "5\/4.png?1",
			"7": "5\/3.png?1",
			"8": "5\/2.png?2",
			"9": "5\/1.png?1"
		}
	},
	"tournamentworktime": 1080,
	"admComm": 30,
	"prizeTourCount": 3
}, {
	"tour_count": 10,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 0,
	"img": {
		"main": "6.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "6\/4.png?2",
			"2": "6\/4.png?1",
			"3": "6\/4.png?1",
			"4": "6\/4.png?1",
			"5": "6\/4.png?1",
			"6": "6\/4.png?1",
			"7": "6\/4.png?1",
			"8": "6\/3.png?1",
			"9": "6\/2.png?1",
			"10": "6\/1.png?1"
		}
	},
	"title": "\u041d\u043e\u0432\u043e\u0433\u043e\u0434\u043d\u0438\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"prizeRatio": 0.6,
	"id": 6,
	"totalPlaces": 6,
	"tournamentworktime": 1200,
	"admComm": 30,
	"prizeTourCount": 3
}, {
	"tour_count": 4,
	"cost": 20,
	"exitcost": 2,
	"required_rating": 0,
	"title": "\u0422\u0435\u0441\u0442\u043e\u0432\u044b\u0439 \u0434\u043b\u044f \u043e\u0434\u043d\u043e\u0433\u043e",
	"enable": 1,
	"prizeRatio": 0.6,
	"id": 7,
	"tournamentworktime": 600,
	"img": {
		"main": "7.png?3",
		"blank": "blank.png?1",
		"tours": {
			"1": "7\/4.png?1",
			"2": "7\/3.png?1",
			"3": "7\/2.png?1",
			"4": "7\/1.png?1"
		}
	},
	"totalPlaces": 1,
	"admComm": 30
}, {
	"tour_count": 6,
	"cost": 20,
	"exitcost": 2,
	"required_rating": 0,
	"title": "\u0422\u0435\u0441\u0442\u043e\u0432\u044b\u0439 \u0434\u043b\u044f \u0434\u0432\u0443\u0445",
	"enable": 0,
	"prizeRatio": 0.6,
	"id": 8,
	"tournamentworktime": 600,
	"img": {
		"main": "8.png?4",
		"blank": "blank.png?1",
		"tours": {
			"1": "8\/4.png?1",
			"2": "8\/4.png?1",
			"3": "8\/4.png?1",
			"4": "8\/3.png?1",
			"5": "8\/2.png?1",
			"6": "8\/1.png?1"
		}
	},
	"totalPlaces": 2,
	"admComm": 30
}, {
	"tour_count": 4,
	"cost": 20,
	"exitcost": 5,
	"required_rating": 0,
	"title": "\u0414\u0443\u044d\u043b\u044c \u0442\u0435\u0441\u0442\u043e\u0432\u0430\u044f",
	"enable": 0,
	"id": 9,
	"tournamentworktime": 480,
	"totalPlaces": 2,
	"img": {
		"main": "9.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "9\/3.png?2",
			"2": "9\/2.png?2",
			"3": "9\/1.png?2"
		}
	},
	"admComm": 0,
	"prizeTourCount": 3
}, {
	"tour_count": 4,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 0,
	"title": "\u0422\u0443\u0440\u043d\u0438\u0440 \u043d\u0430 \u0434\u0432\u043e\u0438\u0445",
	"enable": 1,
	"id": 10,
	"tournamentworktime": 480,
	"totalPlaces": 2,
	"img": {
		"main": "10.png?3",
		"blank": "blank.png?1",
		"tours": {
			"1": "10\/4.png?2",
			"2": "10\/3.png?2",
			"3": "10\/2.png?3",
			"4": "10\/1.png?4"
		}
	},
	"admComm": 20
}, {
	"id": 11,
	"admComm": 30,
	"cost": 50,
	"enable": 1,
	"exitcost": 5,
	"img": {
		"main": "11.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "11\/10.png?2",
			"2": "11\/9.png?2",
			"3": "11\/8.png?2",
			"4": "11\/7.png?1",
			"5": "11\/6.png?1",
			"6": "11\/5.png?4",
			"7": "11\/4.png?1",
			"8": "11\/3.png?2",
			"9": "11\/2.png?3",
			"10": "11\/1.png?3"
		}
	},
	"required_rating": 105,
	"title": "\u041e\u043b\u0438\u043c\u043f\u0438\u0439\u0441\u043a\u0438\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"totalPlaces": 6,
	"tour_count": 10,
	"tournamentworktime": 1200
}, {
	"tour_count": 10,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0422\u0443\u0440\u043d\u0438\u0440 \u0432\u043b\u044e\u0431\u043b\u0435\u043d\u043d\u044b\u0445",
	"enable": 1,
	"id": 12,
	"tournamentworktime": 1200,
	"totalPlaces": 6,
	"img": {
		"main": "12.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "12\/10.png?1",
			"2": "12\/9.png?1",
			"3": "12\/8.png?1",
			"4": "12\/7.png?2",
			"5": "12\/6.png?1",
			"6": "12\/5.png?2",
			"7": "12\/4.png?2",
			"8": "12\/3.png?3",
			"9": "12\/2.png?1",
			"10": "12\/1.png?1"
		}
	},
	"admComm": 20,
	"prizeTourCount": 3
}, {
	"tour_count": 10,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0422\u0443\u0440\u043d\u0438\u0440 \u0437\u0430\u0449\u0438\u0442\u043d\u0438\u043a\u043e\u0432",
	"enable": 0,
	"id": 13,
	"tournamentworktime": 1200,
	"totalPlaces": 6,
	"prizeTourCount": 3,
	"img": {
		"main": "13.png?2",
		"blank": "blank.png?1",
		"tours": {
			"1": "13\/10.png?1",
			"2": "13\/9.png?1",
			"3": "13\/8.png?2",
			"4": "13\/7.png?2",
			"5": "13\/6.png?1",
			"6": "13\/5.png?1",
			"7": "13\/4.png?2",
			"8": "13\/3.png?3",
			"9": "13\/2.png?2",
			"10": "13\/1.png?2"
		}
	},
	"admComm": 20
}, {
	"tour_count": 8,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0422\u0443\u0440\u043d\u0438\u0440 \u0417\u0430 \u043c\u0438\u043b\u044b\u0445 \u0434\u0430\u043c!",
	"enable": 0,
	"id": 14,
	"tournamentworktime": 960,
	"totalPlaces": 6,
	"prizeTourCount": 3,
	"img": {
		"main": "14.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "14\/8.png?2",
			"2": "14\/7.png?2",
			"3": "14\/6.png?1",
			"4": "14\/5.png?1",
			"5": "14\/4.png?2",
			"6": "14\/3.png?2",
			"7": "14\/2.png?1",
			"8": "14\/1.png?2"
		}
	},
	"admComm": 30
}, {
	"tour_count": 7,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0422\u0443\u0440\u043d\u0438\u0440 \u043c\u0430\u0441\u043b\u0435\u043d\u0438\u0446\u0430",
	"enable": 0,
	"id": 15,
	"tournamentworktime": 840,
	"totalPlaces": 6,
	"prizeTourCount": 3,
	"img": {
		"main": "15.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "15\/7.png?1",
			"2": "15\/6.png?2",
			"3": "15\/5.png?1",
			"4": "15\/4.png?1",
			"5": "15\/3.png?2",
			"6": "15\/2.png?2",
			"7": "15\/1.png?2"
		}
	},
	"admComm": 20
}, {
	"tour_count": 6,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0412\u0435\u0441\u0435\u043d\u043d\u0438\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 1,
	"id": 16,
	"tournamentworktime": 720,
	"totalPlaces": 4,
	"prizeTourCount": 3,
	"img": {
		"main": "16.png?2",
		"blank": "blank.png?1",
		"tours": {
			"1": "16\/6.png?1",
			"2": "16\/5.png?2",
			"3": "16\/4.png?2",
			"4": "16\/3.png?1",
			"5": "16\/2.png?3",
			"6": "16\/1.png?2"
		}
	},
	"admComm": 30
}, {
	"tour_count": 10,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u041c\u043e\u0440\u0441\u043a\u043e\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"id": 17,
	"tournamentworktime": 1200,
	"totalPlaces": 6,
	"img": {
		"main": "17.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "17\/10.png?2",
			"2": "17\/9.png?1",
			"3": "17\/8.png?2",
			"4": "17\/7.png?1",
			"5": "17\/6.png?1",
			"6": "17\/5.png?2",
			"7": "17\/4.png?2",
			"8": "17\/3.png?2",
			"9": "17\/2.png?1",
			"10": "17\/1.png?2"
		}
	},
	"admComm": 30,
	"prizeTourCount": 3
}, {
	"tour_count": 10,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0422\u0440\u043e\u043f\u0438\u0447\u0435\u0441\u043a\u0438\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"id": 18,
	"tournamentworktime": 1200,
	"totalPlaces": 4,
	"prizeTourCount": 3,
	"img": {
		"main": "18.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "18\/10.png?1",
			"2": "18\/9.png?2",
			"3": "18\/8.png?1",
			"4": "18\/7.png?2",
			"5": "18\/6.png?2",
			"6": "18\/5.png?1",
			"7": "18\/4.png?2",
			"8": "18\/3.png?1",
			"9": "18\/2.png?2",
			"10": "18\/1.png?1"
		}
	},
	"admComm": 30
}, {
	"tour_count": 8,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0412\u043e\u0441\u0442\u043e\u0447\u043d\u044b\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"id": 19,
	"tournamentworktime": 960,
	"totalPlaces": 6,
	"prizeTourCount": 3,
	"img": {
		"main": "19.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "19\/8.png?2",
			"2": "19\/7.png?1",
			"3": "19\/6.png?1",
			"4": "19\/5.png?2",
			"5": "19\/4.png?2",
			"6": "19\/3.png?2",
			"7": "19\/2.png?1",
			"8": "19\/1.png?2"
		}
	},
	"admComm": 45
}, {
	"tour_count": 9,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0422\u0443\u0440\u043d\u0438\u0440 \u041f\u043e\u0431\u0435\u0434\u044b",
	"enable": 0,
	"id": 20,
	"tournamentworktime": 1080,
	"totalPlaces": 6,
	"prizeTourCount": 3,
	"img": {
		"main": "20.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "20\/9.png?2",
			"2": "20\/8.png?1",
			"3": "20\/7.png?2",
			"4": "20\/6.png?1",
			"5": "20\/5.png?1",
			"6": "20\/4.png?1",
			"7": "20\/3.png?2",
			"8": "20\/2.png?1",
			"9": "20\/1.png?1"
		}
	},
	"admComm": 45
}, {
	"tour_count": 7,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u041f\u0430\u0441\u0445\u0430\u043b\u044c\u043d\u044b\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"id": 21,
	"tournamentworktime": 840,
	"totalPlaces": 6,
	"prizeTourCount": 3,
	"img": {
		"main": "21.png?3",
		"blank": "blank.png?1",
		"tours": {
			"1": "21\/7.png?2",
			"2": "21\/6.png?1",
			"3": "21\/5.png?1",
			"4": "21\/4.png?1",
			"5": "21\/3.png?1",
			"6": "21\/2.png?1",
			"7": "21\/1.png?2"
		}
	},
	"admComm": 45
}, {
	"tour_count": 6,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u0422\u0443\u0440\u043d\u0438\u0440 \u00ab1 \u041c\u0430\u044f\u00bb",
	"enable": 0,
	"id": 22,
	"tournamentworktime": 720,
	"totalPlaces": 6,
	"prizeTourCount": 3,
	"img": {
		"main": "22.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "22\/6.png?2",
			"2": "22\/5.png?1",
			"3": "22\/4.png?2",
			"4": "22\/3.png?1",
			"5": "22\/2.png?2",
			"6": "22\/1.png?1"
		}
	},
	"admComm": 45
}, {
	"tour_count": 6,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u041b\u0435\u0442\u043d\u0438\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"id": 23,
	"tournamentworktime": 720,
	"totalPlaces": 4,
	"prizeTourCount": 3,
	"img": {
		"main": "23.png?2",
		"blank": "blank.png?1",
		"tours": {
			"1": "23\/6.png?1",
			"2": "23\/5.png?1",
			"3": "23\/4.png?1",
			"4": "23\/3.png?1",
			"5": "23\/2.png?1",
			"6": "23\/1.png?1"
		}
	},
	"admComm": 30
}, {
	"tour_count": 6,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 105,
	"title": "\u041e\u0441\u0435\u043d\u043d\u0438\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"id": 24,
	"tournamentworktime": 720,
	"totalPlaces": 4,
	"prizeTourCount": 3,
	"img": {
		"main": "24.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "24\/6.png?2",
			"2": "24\/5.png?1",
			"3": "24\/4.png?1",
			"4": "24\/3.png?2",
			"5": "24\/2.png?1",
			"6": "24\/1.png?1"
		}
	},
	"admComm": 30
}, {
	"tour_count": 6,
	"cost": 50,
	"exitcost": 5,
	"required_rating": 10,
	"title": "\u0417\u0438\u043c\u043d\u0438\u0439 \u0442\u0443\u0440\u043d\u0438\u0440",
	"enable": 0,
	"id": 25,
	"tournamentworktime": 720,
	"totalPlaces": 4,
	"prizeTourCount": 3,
	"img": {
		"main": "25.png?1",
		"blank": "blank.png?1",
		"tours": {
			"1": "25\/6.png?1",
			"2": "25\/5.png?2",
			"3": "25\/4.png?1",
			"4": "25\/3.png?1",
			"5": "25\/2.png?1",
			"6": "25\/1.png?2"
		}
	},
	"admComm": 30
}]
 ');
}

//if (@$_GET['cmd'] == "appserv") {
if (@$_GET['cmd'] == "appserv3") {
 $Tournaments = "calydon.blutbad.ru/1_s.php?cmd=tournaments"; # ps.k12-slovo.ru/tournaments/
 $AppServer = "calydon.blutbad.ru"; # app7.k12-slovo.ru
 $SlovoPhotoPath = "photo2.k12-talk.ru/imgtag/"; #
 $FlashStat = "calydon.blutbad.ru/f/slovo"; # fstat.k12-talk.ru/flashstat/SLOVO-
 $SnOnline = "app15on.k12-slovo.ru"; #
 $Ratings = "ps.k12-slovo.ru/ratings_day/"; #
 $PermanentConnection = "62.213.107.135:1531"; #

 $StaticServer = "calydon.blutbad.ru/f/slovo";

// die("Ratings=".$Ratings.";SnOnline=".$SnOnline.";StaticServer=".$StaticServer.";FlashStat=".$FlashStat.";PermanentConnection=".$PermanentConnection.";SlovoPhotoPath=".$SlovoPhotoPath.";AppServer=".$AppServer.";Version=5;PseudoStaticServer=;Tournaments=".$Tournaments."");
 die("Ratings=ps.k12-slovo.ru/ratings_day/;SnOnline=app15on.k12-slovo.ru;StaticServer=;FlashStat=fstat.k12-talk.ru/flashstat/SLOVO-;PermanentConnection=62.213.107.135:1531;SlovoPhotoPath=photo2.k12-talk.ru/imgtag/;AppServer=app7.k12-slovo.ru;Version=5;PseudoStaticServer=;Tournaments=ps.k12-slovo.ru/tournaments/");
}


$auth_key = "6917d0099c3f86c9a4f740656afe4093";
$partner_url = "";
$orbiterUrl = "/1_s.php?cmd=appserv";      # http://app5.k12-slovo.ru/appserv/
$cityURL = "http://calydon.blutbad.ru/"; # https://city.k12-company.ru/

?>


<object type="application/x-shockwave-flash" data="SlovoLoader.swf?148" width="740" height="540" id="slovoFlashContainer" style="visibility: visible;">
  <param name="menu" value="false">
  <param name="wmode" value="window">
  <param name="allowFullScreen" value="true">
  <param name="scale" value="noscale">
  <param name="allowScriptAccess" value="always">
  <param name="bgcolor" value="#ffffff">
  <param name="flashvars" value="socialNetId=mamba&socnet_type=03&refId=undefined&app_id=844&auth_key=<?= $auth_key ?>&sid=app_id=844lang=ruoid=1598702883partner_url=http://mamba.ua/sid=e29fe7f9e4c8b80748d6b8e297dc2e7b&partner_url=http://mamba.ua/&lang=ru&viewer_id=159870288303&session_key=app_id=844lang=ruoid=1598702883partner_url=http://mamba.ua/sid=e29fe7f9e4c8b80748d6b8e297dc2e7b&error=&socialAdapterUrl=bin/adapters/MB.swf?v=4&orbiterUrl=<?= $orbiterUrl ?>&cityURL=<?= $cityURL ?>&appVersion=0.2.165&flashCacheVersion=8&loaderVersion=48&versionVKadapter=10&versionOKadapter=7&versionMBadapter=4&versionMWadapter=5&ruTextsVersion=10&versionButtons=15&versionDifferent=15&versionBackground=14&versionIndicators=7&versionRoulette=10&versionSounds=8&versionCSS=2&versionChat=11&versionWords=17&versionFonts=1&formVersion=82&AOSVersion=26&currentStateName=app">
</object>

<?
/*
<param name="flashvars" value="socialNetId=mamba&amp;socnet_type=03&amp;refId=undefined&amp;app_id=844&amp;auth_key=6917d0099c3f86c9a4f740656afe4093&amp;sid=app_id=844lang=ruoid=1598702883partner_url=http://mamba.ua/sid=e29fe7f9e4c8b80748d6b8e297dc2e7b&amp;partner_url=http://mamba.ua/&amp;lang=ru&amp;viewer_id=159870288303&amp;session_key=app_id=844lang=ruoid=1598702883partner_url=http://mamba.ua/sid=e29fe7f9e4c8b80748d6b8e297dc2e7b&amp;error=&amp;socialAdapterUrl=bin/adapters/MB.swf?v=4&amp;orbiterUrl=http://app5.k12-slovo.ru/appserv/&amp;cityURL=https://city.k12-company.ru/&amp;appVersion=0.2.165&amp;flashCacheVersion=8&amp;loaderVersion=48&amp;versionVKadapter=10&amp;versionOKadapter=7&amp;versionMBadapter=4&amp;versionMWadapter=5&amp;ruTextsVersion=10&amp;versionButtons=15&amp;versionDifferent=15&amp;versionBackground=14&amp;versionIndicators=7&amp;versionRoulette=10&amp;versionSounds=8&amp;versionCSS=2&amp;versionChat=11&amp;versionWords=17&amp;versionFonts=1&amp;formVersion=82&amp;AOSVersion=26&amp;currentStateName=app">

 */
?>