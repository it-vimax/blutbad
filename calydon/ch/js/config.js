// �������
var CMD_CHANGE_ROOM   = 'change_room';
var CMD_JOIN_CHANNEL  = 'join_channel';
var CMD_LEAVE_CHANNEL = 'leave_channel';
var CMD_MESSAGE       = 'message';
var CMD_PARAMS        = 'params';
var CMD_OPTIONS       = 'options';
var CMD_BUTTONS       = 'buttons';
var CMD_INBOX         = 'inbox';
var CMD_SOUND         = 'sound';
var CMD_DEAL          = 'deal';
var CMD_REFRESH       = 'refresh';
var CMD_HELP_HINT     = 'help_hint';
var CMD_TRADE_CHANNEL = 'trade_channel';
var CMD_TUTORIAL      = 'tutorial';
var CMD_RELOAD_PAGE   = 'reload_page';
var CMD_BURN_FIRE     = 'burn_fire';
var CMD_BATTLE_LOG    = 'battle_log';
var CMD_RELOAD_PANEL  = 'reload_panel';

// ������������� ������
var CH_MAIN        = '�������';
var CH_TRADE       = '��������';
var CH_SYSTEM      = '���������';
var CH_BATTLE_LOGS = '���� ����';
var CH_BATTLE_LOG  = '��� ���';

// ������� ������������� �������
var CHANNEL_ORDERS = new Array();
CHANNEL_ORDERS[ CH_MAIN ]        = 0;
CHANNEL_ORDERS[ CH_TRADE ]       = 1;
CHANNEL_ORDERS[ CH_SYSTEM ]      = 2;
CHANNEL_ORDERS[ CH_BATTLE_LOGS ] = 3;

// ������� ����
var FT_ALL     = 0;
var FT_PRIVATE = 1;
var FT_OFF     = 2;

// ��������
var TR_OFF = 0;
var TR_ON  = 1;

// �������� �����������
var SND_OFF     = 0;
var SND_VOLUME1 = 1;
var SND_VOLUME2 = 2;

// div, ���������� ����
var SND_CONTAINER = 'Sound';

// ������������ ����� ��������� �� ���� ���������
var MAX_SMILES = 3;

// ������������ ����� ���������
var MAX_MESSAGE_LENGTH = 250;

// ������������ ����� (��-���������) ������������ ��������� � ������
var DEFAULT_MAX_ROWS = 300;

// ����� � ������������, ����� ������� ���������� ���������� ������ � ������ �������������
var AUTOREFRESH_INTERVAL = 15000;

// ����� �������� � ������������ ��� ���������� �������������� � �������
var WAIT_PRESENCE_INTERVAL = 500;

// ������������ ����� �������� �������� ������
var MAX_USER_CHANNEL_NAME_LENGTH = 20;

// ������������ ����� ������ �������� ������
var MAX_USER_CHANNEL_PASSWORD_LENGTH = 10;

// ���� ������� ���������
var MT_PRIVATE          = 'private';
var MT_ROOM             = 'room';
var MT_CLAN             = 'clan';
var MT_GUILD            = 'guild';
var MT_GROUP            = 'group';
var MT_POKER            = 'poker';
var MT_ALIENSPEECH      = 'alienspeech';
var MT_BATTLE_LOG       = 'battle_log';
var MT_SYS_ATTENTION    = 'attention';
var MT_SYS_COMMON       = 'system';
var MT_COMMERCE         = 'commerce';
var MT_CITY_SHOUT       = 'city_shout';
var MT_CITY_SHOUT2      = 'city_shout2';
var MT_TELEPATHY        = 'telepathy';
var MT_CITY_ALIENSPEECH = 'city_alienspeech';

// ���� ��������� XMPP
var XMPP_MT_PRIVATE = 'chat';
var XMPP_MT_ROOM    = 'groupchat';
var XMPP_MT_ERROR   = 'error';

// ���� ������� � ������ ���������
var PRIVATE_NICK = 'private_nick';
var TO_NICK      = 'to_nick';

// ������������� �������� ������
var MAIN_ID = 'main#room';

// ������
var CHAT_BUTTONS = [ 'say', 'clear', 'filter', 'filter_log', 'smiles', 'trans', 'sound', 'inv', 'deals', 'contacts', 'align', 'clan', 'guild', 'mentor', 'pupil', 'admin', 'quests', 'help' ];
var CHAT_BUTTON_PARAMS = {
	clear : { image : 'cm_clear.gif', width : 30, height : 30, title : '��������', onclick : 'processClear();', oncontextmenu : '' },
	filter_log : { image : 'cm_log_all.gif', width : 30, height : 30, title : '������ ����� ����: �������� ���� ����������', onclick : 'changeNextFilterLog();', oncontextmenu : '' },
	smiles : { image : 'cm_smile.gif', width : 30, height : 30, title : '��������', onclick : 'showSmiles(this);', oncontextmenu : '' },
	trans : { image : 'cm_trans_off.gif', width : 30, height : 30, title : '�������� (���������)', onclick : 'switchTrans();', oncontextmenu : '' },
	sound : { image : 'cm_sound_off.gif', width : 30, height : 30, title : '�������� ����������� (���������)', onclick : 'changeNextSound();', oncontextmenu : '' },
	contacts : { image : 'cm_friends.gif', width : 30, height : 30, title : '��������/�������', onclick : 'processContacts();', oncontextmenu : 'processNotes();' },
	align : { image : 'cm_align.gif', width : 30, height : 30, title : '����������', onclick : 'processAlign();', oncontextmenu : '' },
	clan : { image : 'cm_klan.gif', width : 30, height : 30, title : '����/�������', onclick : 'processClan();', oncontextmenu : 'processClanRelicts();' },
	guild : { image : 'cm_guild.gif', width : 30, height : 30, title : '�������/�������', onclick : 'processGuild();', oncontextmenu : 'processGuildRelicts();' },
	mentor : { image : 'cm_mentor.gif', width : 30, height : 30, title : '��������', onclick : 'processMentor();', oncontextmenu : '' },
	pupil : { image : 'cm_study.gif', width : 30, height : 30, title : '��������', onclick : 'processPupil();', oncontextmenu : '' },
	admin : { image : 'cm_admin.gif', width : 30, height : 30, title : '�����������������', onclick : 'processAdmin();', oncontextmenu : '' },
	help : { image : 'cm_help.gif', width : 30, height : 30, title : '������', onclick : 'processHelp();', oncontextmenu : '' }

};

var CHAT_FILTERS = [
	{ type : FT_ALL, image : 'http://img.blutbad.ru/i/button/new/cm_mess_all.gif', title : '������ ����: ��� ���������' },
	{ type : FT_PRIVATE, image : 'http://img.blutbad.ru/i/button/new/cm_mess_priv.gif', title : '������ ����: ������ ������ ���������' },
	{ type : FT_OFF, image : 'http://img.blutbad.ru/i/button/new/cm_mess_off.gif', title : '������ ����: �� ���������� ���������' }

];

var CHAT_FILTERS_LOG = [
	{ type : FT_ALL, image : 'http://img.blutbad.ru/i/button/new/cm_log_all.gif', title : '������ ����� ����: ��� ��������' },
	{ type : FT_PRIVATE, image : 'http://img.blutbad.ru/i/button/new/cm_log_me.gif', title : '������ ����� ����: ������ �������� �� ����' },
	{ type : FT_OFF, image : 'http://img.blutbad.ru/i/button/new/cm_log_off.gif', title : '������ ����� ����: �� ���������� ��������' }

];

var CHAT_TRANSES = [
	{ type : TR_OFF, image : 'http://img.blutbad.ru/i/button/new/cm_trans_off.gif', title : '�������� (���������)' },
	{ type : TR_ON, image : 'http://img.blutbad.ru/i/button/new/cm_trans_on.gif', title : '�������� (��������)' }

];

var CHAT_SOUNDS = [
	{ type : SND_OFF, image : 'http://img.blutbad.ru/i/button/new/cm_sound_off.gif', title : '�������� ����������� (���������)' },
	{ type : SND_VOLUME1, image : 'http://img.blutbad.ru/i/button/new/cm_sound_on2.gif', title : '�������� ����������� ���� (��������)' },
	{ type : SND_VOLUME2, image : 'http://img.blutbad.ru/i/button/new/cm_sound_on.gif', title : '�������� ����������� ������ (��������)' }

];

// ���� �������� � XMPP �������
var Q_DESTROY_CHANNEL = 'destroy';
var Q_KICK_USER       = 'kick';
var Q_BAN_USER        = 'ban';
var Q_UNBAN_USER      = 'unban';
var Q_GRANT_ADMIN     = 'grant_admin';
var Q_REVOKE_ADMIN    = 'revoke_admin';
var Q_USER_CHANNEL    = 'user_channel';

// �������������� ��������
var ID_DESTROY_CHANNEL         = 'user-destroy-channel|';
var ID_KICK_USER               = 'user-kick|';
var ID_BAN_USER                = 'user-ban|';
var ID_UNBAN_USER              = 'user-unban|';
var ID_GRANT_ADMIN             = 'user-grant-admin|';
var ID_REVOKE_ADMIN            = 'user-revoke-admin|';
var ID_OWNER_CHANNEL_LIST      = 'user-owner-channel-list|';
var ID_CHANNEL_LIST            = 'user-channel-list|';
var ID_CHANNELS_ADMIN_LIST     = 'user-channels-admin-list|';
var ID_ADMIN_CHANNEL_LIST      = 'user-admin-channel-list|';
var ID_BANNED_CHANNEL_LIST     = 'user-banned-channel-list|';
var ID_CHANNELS_BROADCAST_LIST = 'user-channels-broadcast-list|';
var ID_JOIN_CHANNELS           = 'user-join-channels|';
var ID_TAB_CHANNEL             = 'user-tab-channel|';
var ID_UNTAB_CHANNEL           = 'user-untab-channel|';
var ID_REMOVE_BROADCAST        = 'user-remove-broadcast|';

// ���� ����������� �������
var CODE_KICKED = 307;
var CODE_BANNED = 301;

// ���� ������
var ECODE_PASSWORD       = 401;
var ECODE_FORBIDDEN_C    = 403;
var ECODE_NOT_FOUND_C    = 404;
var ECODE_FORBIDDEN_C2   = 405;
var ECODE_NOT_ACCEPTABLE = 406;
var ECODE_ADMIN          = 490;
var ECODE_SELF           = 491;
var ECODE_IGNORED        = 492;
var ECODE_BLIND          = 493;
var ECODE_SILENCE        = 494;
var ECODE_SELF_ADMIN     = 496;
var ECODE_LIMIT_REACHED  = 498;
var ECODE_ALREADY_EXISTS = 499;
var ECODE_NOT_FOUND      = 503;

// ������ ������
var ETEXT_BANNED = 'You have been banned from this room';

// ���������
var ADMIN_BASENAME         = 'admin';
var HIDDEN_BASENAME        = 'hidden';
var HIDDEN_REALNAME        = '���������';
var BOT_NAME               = '������';
var BASE_USER_CHANNEL_NAME = 'user#';

// ���������� ��������
var R_OK                = 'ok';
var E_ERROR             = 'error';
var E_NOT_ALLOWED       = 'not-allowed';
var E_NOT_FOUND         = 'not-found';
var E_NOT_IN_CLAN       = 'not-in-clan';
var E_NOT_IN_GUILD      = 'not-in-guild';
var E_BAD_NAME          = 'bad-name';
var E_LEVEL_NOT_ALLOWED = 'level-not-allowed';
var E_RESERVED_NAME     = 'reserved-name';
var E_BAD_PASSWORD      = 'bad-password';
var E_LIMIT_REACHED     = 'limit-reached';
var E_ALREADY_EXISTS    = 'already-exists';
var E_HAS_TIMEOUT       = 'has-timeout';

// ���������
var MSG_SENT_TO_ALLY    = '���� ��������� �������� ����� ����������';
var MSG_SPAM_REPORT     = '���� ��������� � ����� ����������������';
var MSG_COMPLAIN_REPORT = '���� ������ ����������������';

// ��������� �� �������
var EMSG_SEND_NOT_ALLOWED            = '�� �� ������ ��������� ��� ���������';
var EMSG_TRADE_NOT_ALLOWED           = '��������� �� ����������. �� ������ ��������� ��������� ��������� ����� ';
var EMSG_SILENCE_ON                  = EMSG_SEND_NOT_ALLOWED + '. �� ��� �������� ��������';
var EMSG_NOT_IN_CLAN                 = '�� �� �������� � �����';
var EMSG_NOT_ALLY_CLAN               = '������� ���� �� ������';
var EMSG_NOT_IN_GUILD                = '�� �� �������� � �������';
var EMSG_NOT_IN_GROUP                = '�� �� �������� � ������';
var EMSG_NOT_IN_POKER                = '�� �� ���������� �� �������� ������';
var EMSG_CHANNEL_OFF                 = '� ��� ���� ����� ��������';
var EMSG_DISCONNECT                  = '��� � ������ ������ �� ��������. �������������� ������� �����������...';
var EMSG_FAILED_TO_CONNECT           = '�� ������� ������������ � ����';
var EMSG_NOT_AUTHENTICATED           = '�� ������� �������������� � ����. ���������� ��������� � ����';
var EMSG_UNABLE_FILTER               = '�� ������� �������� ������ ����';
var EMSG_UNABLE_FILTER_LOG           = '�� ������� �������� ������ ��� ����� ����';
var EMSG_UNABLE_TRANS                = '�� ������� �������� �������� ����';
var EMSG_UNABLE_SOUND                = '�� ������� �������� �������� ����������� ����';
var EMSG_UNABLE_PRESENCEOFF          = '�� ������� �������� ��������� �����������';
var EMSG_UNABLE_CREATE_CHANNEL       = '�� ������� ������� �����';
var EMSG_WRONG_CHANNEL_NAME          = '������������ �������� ������ (�� ����� 20 �������� �������� ��� ����������� ��������� ��� ����)';
var EMSG_CHANNEL_LEVEL_NOT_ALLOWED   = '��� ������� �� ��������� ��������� ������� ������';
var EMSG_RESERVED_CHANNEL_NAME       = '�� �� ������ ������� ����� � ����� ������';
var EMSG_WRONG_CHANNEL_PASSWORD      = '����������� ������ ������ (�� ����� 10 �������� ����������� �������� ��� ����)';
var EMSG_CREATE_LIMIT_REACHED        = '����� �� ������. ���� ���������� ������������ ���������� ��������� �������';
var EMSG_CHANNEL_ALREAY_EXISTS       = '����� ����� ��� ����������';
var EMSG_BAD_PASSWORD                = '������������ ������ ��� ������ ';
var EMSG_OPERATION_NOT_ALLOWED       = '� ��� ��� ���� �� ������ ��������';
var EMSG_NO_CHANNEL_NAME             = '�� ������� ��� ������';
var EMSG_CHANNEL_NOT_FOUND           = '����� �� ������';
var EMSG_CHANNEL_NOT_ACCEPTABLE      = '�� �� ���������� � ������ ';
var EMSG_UNABLE_DESTROY_CHANNEL      = '�� ������� ������� �����';
var EMSG_UNABLE_SET_PASSWORD_CHANNEL = '�� ������� ���������� ������';
var EMSG_NO_OWNER_CHANNELS           = '� ��� ��� ����� ������� �������';
var EMSG_NO_CONNECTED_CHANNELS       = '�� �� ���������� �� � ������ �������� ������';
var EMSG_NO_CHANNELS_ADMIN           = '�� �� ��������� ��������������� �� ������ �������� ������';
var EMSG_NO_CHANNELS_BROADCAST       = '� ��� ��� �� ������ ������������ �������� ������';
var EMSG_NO_OTHER_CONNECTED          = '������ �� ���������� � ������ ';
var EMSG_CONNEC_LIMIT_REACHED        = '�� ������� ������������ � ������. ���������� ������������ ���������� �����������';
var EMSG_CREATE_TIMEOUT              = '����� �� ������. ��������� ���� ������� ����� �� �������� ������ ������';
var EMSG_CONN_CHANNEL_NOT_FOUND      = '����� �� ������ ����� ������������';
var EMSG_USER_NOT_FOUND              = '�������� �� ������';
var EMSG_ADMIN_LIMIT_REACHED         = '������������� �� ����������. ���������� ������������ ���������� ��������������� ��� ������ ';
var EMSG_ALREADY_OWNER               = '�� ��������� ���������� ������ ';
var EMSG_NOT_OWNER                   = '�� �� ��������� ���������� ������ ';
var EMSG_CHANNEL_SET_STYLES          = '� ��� ����������� ����� ��� ��������� ������� �������: ';
var EMSG_NO_CHANNEL_SET_STYLES       = '� ��� ��� ������������� ������ ��� ������� �������';
var EMSG_UNABLE_DELETE_CHANNEL_STYLE = '�� ������� ������� ����� ��� �������� ������';
var EMSG_BAD_CHARS                   = '��������� �������� ������������ �������';
var EMSG_BANNED                      = '�� �������� �� ������';
var EMSG_NOT_SELF_ALLOWED            = '������ �������� ������ ��������� �� ����';
var EMSG_NOT_ADMIN_ALLOWED           = '������ �������� ������ ��������� �� ��������� ��� �������������� ������';

// ��������� ��� XMPP-����������
//var XMPP_URL      = '/http-bind';
var XMPP_URL      = 'http://damask.blutbad.ru/http-bind';

//var XMPP_SERVER   = 'calydon.blutbad.ru';
//var XMPP_DOMAIN   = 'calydon.blutbad.ru';
var XMPP_SERVER   = 'localhost';
var XMPP_DOMAIN   = 'localhost';
var XMPP_RESOURCE = 'Blutbad';
var XMPP_MUC      = 'conference';

// NS
var NS_MUC          = 'http://jabber.org/protocol/muc';
var NS_MUC_OWNER    = 'http://jabber.org/protocol/muc#owner';
var NS_MUC_ADMIN    = 'http://jabber.org/protocol/muc#admin';
var NS_USER_CHANNEL = 'jabber:iq:user-channel';

// ����������������� �����
var RESERVED_NAMES = new Array( 'to', 'p', 'private', 'w', 'whisper', '�', '�������', '�������', 'c', 'clan', 'k', 'klan', '�', '����', 'a', 'ally', '�', '����', 'guild', '�������', 'g', 'group', '�', '������', 'poker', '�����', 'ch', 'channel', '�����', 'b', 'broadcast', 'bg', 'broadcastglobal', 'bc', 'broadcastcities', 'debug', 'help', '������', 'create', '�������', 'cm', 'createmain', '��', '��������������', 'password', '������', 'j', 'join', '�', '������������', 'jm', 'joinmain', '��', '�������������������', 'l', 'leave', '�', '�����������', 'destroy', '�������', 'adestroy', '��������', 'mychannels', '���������', 'channels', '������', 'adminchannels', '�����������', 'admins', '������', '��������������', 'banned', 'banlist', '�������', 'bch', 'broadcastchannels', '��', '�����������������', 'list', '������', 'cs', 'channelstyles', '��', '������������', 't', 'tab', '�', '�������', 'ut', 'untab', '��', '��������', 'kick', '���������', 'ban', '���', 'unban', '������', 'grant', 'admin', '�����', '�������������', 'revoke', 'deladmin', '�������', '��������������������', 'rb', 'removebroadcast', '��������', '�����������������', 'ds', 'deletestyle', '��', '������������', 'pon', 'presenceon', '����', '��������������', 'poff', 'presenceoff', '�����', '���������������', '�������', '��������', '���������' );

var HELP_TEXT = '<br><span class="room-name">=== ������ ===</span><br>' +
'<span class="chat-head">�������� �������� ������</span><br>' +
'�������� ������ ������ �������� �� �� ����� 20 �������� �������� ��� ����������� ��������� ��� ����.<br>' +
'������ ������ �������� �� �� ����� 10 �������� ����������� �������� ��� ����.<br><br>' +
'� ��������� �������:<br>' +
'<strong>!������� ������ qwerty</strong> - ��������� ����� "������" � ������� "qwerty"<br>' +
'<strong>.create �����</strong> - ��������� ����� "�����" ��� ������<br>' +
'<strong>/create �����</strong> - ��������� ����� "�����" ��� ������<br><br>' +
'�� ������� �������� ������:<br>' +
'<strong>/�������������� ������ qwerty</strong><br>' +
'<strong>/createmain �����</strong><br>' +
'<strong>/�� ������ qwerty</strong><br>' +
'<strong>/cm �����</strong><br><br>' +
'<span class="chat-head">�������� �������� ������</span><br>' +
'<strong>/������� ���������</strong><br>' +
'<strong>/destroy ���������</strong><br><br>' +
'<span class="chat-head">�����������</span><br>' +
'� ��������� �������:<br>' +
'<strong>/������������ ���������</strong><br>' +
'<strong>/join ���������</strong><br>' +
'<strong>/� ���������</strong><br>' +
'<strong>/j ���������</strong><br><br>' +
'�� ������� �������� ������:<br>' +
'<strong>/������������������� ���������</strong><br>' +
'<strong>/joinmain ���������</strong><br>' +
'<strong>/�� ���������</strong><br>' +
'<strong>/jm ���������</strong><br><br>' +
'<span class="chat-head">����������</span><br>' +
'<strong>/����������� ���������</strong><br>' +
'<strong>/leave ���������</strong><br>' +
'<strong>/� ���������</strong><br>' +
'<strong>/l ���������</strong><br><br>' +
'<span class="chat-head">�������� ���������</span><br>' +
'<strong>/����� ��������� ��������� � �����</strong><br>' +
'<strong>/channel ��������� ��������� � �����</strong><br>' +
'<strong>/ch ��������� ��������� � �����</strong><br><br>' +
'���, ��������:<br>' +
'<strong>/����� ���� ������</strong> - ���������� ����� "���� ������" � ����� "�����"<br><br>' +
'<span class="chat-head">������ � �������� ��������</span><br>' +
'��������� ������ ����� �������:<br>' +
'<strong>/���������</strong><br>' +
'<strong>/mychannels</strong><br><br>' +
'��������� ������ ������������ �������:<br>' +
'<strong>/������</strong><br>' +
'<strong>/channels</strong><br><br>' +
'��������� ������ �������, � ������� ��� �������������:<br>' +
'<strong>/�����������</strong><br>' +
'<strong>/adminchannels</strong><br><br>' +
'��������� ������ ��������������� ������ ������:<br>' +
'<strong>/�������������� ���������</strong><br>' +
'<strong>/������ ���������</strong><br>' +
'<strong>/admins ���������</strong><br><br>' +
'��������� ������ ���������� �� ������:<br>' +
'<strong>/������� ���������</strong><br>' +
'<strong>/banned ���������</strong><br>' +
'<strong>/banlist ���������</strong><br><br>' +
'��������� ������ ����������� �������:<br>' +
'<strong>/�����������������</strong><br>' +
'<strong>/broadcastchannels</strong><br>' +
'<strong>/��</strong><br>' +
'<strong>/bch</strong><br><br>' +
'��������� ������ ������������ � �������� ������ �������:<br>' +
'<strong>/������ ���������</strong><br>' +
'<strong>/list ���������</strong><br><br>' +
'��������� ������ ������ ������� �������:<br>' +
'<strong>/������������</strong><br>' +
'<strong>/channelstyles</strong><br>' +
'<strong>/��</strong><br>' +
'<strong>/cs</strong><br><br>' +
'�������� ������� ��� ������:<br>' +
'<strong>/������� ���������</strong><br>' +
'<strong>/tab ���������</strong><br>' +
'<strong>/� ���������</strong><br>' +
'<strong>/t ���������</strong><br><br>' +
'������� ������� � ������� �����:<br>' +
'<strong>/�������� ���������</strong><br>' +
'<strong>/untab ���������</strong><br>' +
'<strong>/�� ���������</strong><br>' +
'<strong>/ut ���������</strong><br><br>' +
'������� ������� ����������� ������� �����:<br>' +
'<strong>/����������������� ���������</strong><br>' +
'<strong>/removebroadcast ���������</strong><br>' +
'<strong>/�������� ���������</strong><br>' +
'<strong>/rb ���������</strong><br><br>' +
'�������� ����� �������� ������:<br>' +
'<strong>/������������ ���������</strong><br>' +
'<strong>/deletestyle ���������</strong><br>' +
'<strong>/�� ���������</strong><br>' +
'<strong>/ds ���������</strong><br><br>' +
'��������� ����������� � ����������� ������� � ������� �������:<br>' +
'<strong>/��������������</strong><br>' +
'<strong>/presenceon</strong><br>' +
'<strong>/����</strong><br>' +
'<strong>/pon</strong><br><br>' +
'���������� ����������� � ����������� ������� � ������� �������:<br>' +
'<strong>/���������������</strong><br>' +
'<strong>/presenceoff</strong><br>' +
'<strong>/�����</strong><br>' +
'<strong>/poff</strong><br><br>' +
'<span class="chat-head">���������� ������ �� ������</span><br>' +
'��������� ������ ����� ������ ������������ �������� ������ ��� ��� ��������������.<br><br>' +
'<strong>/��������� ��������� ��� ���������</strong><br>' +
'<strong>/kick ��������� ��� ���������</strong><br><br>' +
'<span class="chat-head">��� ������ �� ������</span><br>' +
'�������� ������ ����� ������ �������� ������ ��� ��� ��������������.<br><br>' +
'<strong>/��� ��������� ��� ���������</strong><br>' +
'<strong>/ban ��������� ��� ���������</strong><br><br>' +
'<span class="chat-head">������ ���� � ������ �� ������</span><br>' +
'������� ��� � ������� ����� ������ �������� ������ ��� ��� ��������������.<br><br>' +
'<strong>/������ ��������� ��� ���������</strong><br>' +
'<strong>/unban ��������� ��� ���������</strong><br><br>' +
'<span class="chat-head">��������� ������ �� ������� �����</span><br>' +
'������������� ������ ����� ������ �������� ������ ��� ��� ��������������.<br><br>' +
'<strong>/������ ���������</strong> - ����� ������<br>' +
'<strong>/password ��������� qwerty</strong> - ����������� ������ "qwerty"<br><br>' +
'<span class="chat-head">��������� �������������� �������� ������</span><br>' +
'������������� ����� ������ �������� ������.<br>' +
'�������������� ����� ����� ��������� ������� �� ������, � ����� ������ ������ �� ���� � �����.<br><br>' +
'<strong>/������������� ��������� ��� ���������</strong><br>' +
'<strong>/����� ��������� ��� ���������</strong><br>' +
'<strong>/admin ��������� ��� ���������</strong><br>' +
'<strong>/grant ��������� ��� ���������</strong><br><br>' +
'<span class="chat-head">�������� �������������� �������� ������</span><br>' +
'������� ����� ������ �������� ������.<br><br>' +
'<strong>/�������������������� ��������� ��� ���������</strong><br>' +
'<strong>/������� ��������� ��� ���������</strong><br>' +
'<strong>/deladmin ��������� ��� ���������</strong><br>' +
'<strong>/revoke ��������� ��� ���������</strong><br>' +
'<span class="room-name">=======</span><br><br>';
