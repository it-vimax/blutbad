var forbiddenUrls = new Array( 'rcarnageru', 'cwars', 'cwarsru', 'dwarru', 'pwonlineru', 'combatscom', 'combatsru', 'hlonlineru', 'kmailru', 'otherworldsru', 'kingdomru', 'nurabiz', 'starquakeru', 'ganjawarsru', 'mistgameru', 'heroeswm', 'travianru', 'lineagerru', 'rfonlineru', 'ronlineru', 'geonlineru', 'raggameru', 'rzonlineru', 'requiemonlineru', 'piratiaru', 'eveonlineru', 'eveonlinecom', 'ucozru', 'tormentru', 'alakoru', 'refowebnet', 'mantunclansu', 'chaosroadcom', 'peredovayaru', 'fdworldsnet', 'worldofchaosru', 'outexporg', 'gladiatorsru', 'wgladscom', 'tsemeroru', 'pvpepoharu', 'traviancomua', 'latransru', 'hostlandsu', 'goldenturu', 'erealityru', 'tmchatru', 'gamaxagilityhostercom', 'fragoriamailru', 'lleoaharu', 'crackworldstslandru', 'boomru', 'holmru', 'houa', 'instinctsru', 'tironlinenet', 'botvaru', 'chaosageru', 'starlordsru', 'epohaus', 'klanzru', 'rusonlainzbordru', 'gameowru', 'aloneislandsru', 'timezeroru', 'bloodyworldcom', 'netwarsru', 'eofgru', 'granmirovorg', 'abkclubcom', 'buxto', 'magicru', 'rulezzru', 'narodru', 'phpruinfo', 'kolotibablocom', 'mycarnagedoam', 'studentsu', 'sestercehutru', 'wgladscom', 'carnagehdd1', 'fotocarnageru', 'hiderorgua', 'ucozua', 'fcarnageru', 'carnagefotoru', 'acreshoperu', 'revengonline', 'emailsu', 'kentanru', 'emodlabcom', 'flyws', 'carnagenews', 'magicsword', 'feesnet', 'imperienet', 'xaknet', 'oldcombats', 'pollisru', 'kentanru', 'petsdoam', 'fdworldsnet', 'magegameru', '�������', 'vestnikk', 'carnage', 'ravag', 'ravaged', 'memories', 'memories-game' );
var forbiddenUrlsRus = new Array('�����','�����','�������');
var forbiddenDigs = new Array('7458');
var allowedUrls = new Array( 'http://[a-z]{2,}.blutbad.ru/', 'http://www.blutbad.ru/', 'http://www.sterling.ru/' );
var CENSORED = '&lt;�������� ��������&gt;';


function processAntiurl( message ) {
	var re = new RegExp( /([a-zA-Z0-9\.\-\_]+?\@[a-zA-Z0-9\.\-\_]+)/ig );
	message = message.replace( re, '<a href="mailto:$1">$1</a>' );

	var re = new RegExp( /\br\.blutbad\.ru\S*/ig );
	message = message.replace( re, CENSORED );

	var re = new RegExp( /\S+reminder\.php\S*/ig );
	message = message.replace( re, CENSORED );

	if ( _check( message ) ) {
		message = CENSORED;
	} else if ( _check2 (message ) ) {
		message = CENSORED;
	} else if ( _check3 (message)) {
		message = CENSORED;
	} else {
		var re = new RegExp( /((http|https):\/\/([a-zA-Z0-9]+\.)*(blutbad|sterling)\.ru\/\S*)/ig );

		if ( re.test( message ) ) {
			message = message.replace( re, '<a href="$1" target="_blank">$1</a>' );
		} else {
			var re = new RegExp( /\.ru[^\<\"]|\.com[^\<\"]|www\.|\.php|\.htm|http:|ftp:|\.asp/i );

			if ( re.test( message ) ) {
				message = CENSORED;
			}

		}

	}

	return message;

}

function _check( message ) {
	for ( var i = 0; i <= allowedUrls.length - 1; i++ ) {
		var re = new RegExp( allowedUrls[ i ], 'g' );
		message = message.replace( re, '' );
	}

	message = message.toLowerCase();
	message = message.replace(/\&amp\;/g, '');
	message = message.replace(/\&quot\;/g, '');
	message = message.replace(/\&gt\;/g, '');
	message = message.replace(/\&lt\;/g, '');
	message = message.replace( /[^a-z]/g, '' );

	for ( var i = 0; i <= forbiddenUrls.length - 1; i++ ) {
		var re = new RegExp( forbiddenUrls[ i ] );
		if ( re.test( message ) ) {
			return true;
		}
	}
	return false;
}

function _check2( message ) {
    var c_message = message;
    c_message = c_message.replace( /[^0-9]/g, '' );
    for ( var i = 0; i <= forbiddenDigs.length - 1; i++ ) {
        var re = new RegExp( forbiddenDigs [i] );
	if ( re.test( c_message ) ) {
	    return true;
        }
    }
    return false;
}

function _check3( message ) {
	message = message.toLowerCase();
	message = message.replace( /[^�-��]/g, '' );
	for ( var i = 0; i <= forbiddenUrlsRus.length - 1; i++ ) {
		var re = new RegExp( forbiddenUrlsRus[ i ] );
		if ( re.test( message ) ) {
			return true;
		}
	}
	return false;
}
