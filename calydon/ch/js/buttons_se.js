function processKeyUp( event ) {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat && top.frames[ 'chat' ][ 'chat2' ].oChat.connected() ) {
		var oSrc = event.srcElement ? event.srcElement : event.target;

		if ( event.keyCode == 13 ) {
			if ( top.frames[ 'chat' ][ 'chat2' ].oChat.sendMessage( oSrc.value ) ) {
				oSrc.value = '';
			}

		}

		if ( event.keyCode == 27 ) {
           document.getElementById( 'editor' ).value = '';
		}

	}

}
function processSay() {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat && top.frames[ 'chat' ][ 'chat2' ].oChat.connected() ) {
		if ( top.frames[ 'chat' ][ 'chat2' ].oChat.sendMessage( document.getElementById( 'editor' ).value ) ) {
			document.getElementById( 'editor' ).value = '';
		}

	}

}


function processClear() {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.clear();
	}

}


function changeNextFilter() {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.changeNextFilter();
	}

}


function changeNextFilterLog() {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.changeNextFilterLog();
	}

}


function showSmiles() {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.showSmiles();
	}

}


function addSmile( smile ) {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.addSmile( smile );
	}

}


function switchTrans() {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.switchTrans();
	}

}


function changeNextSound() {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.changeNextSound();
	}

}


function processDeals() {
	top.frames[ 'main' ].location = '/transfer.php?' + Math.random();
}


function processContacts() {
	top.frames[ 'main' ].location = '/friends.php?' + Math.random();
}


function processNotes() {
	top.frames[ 'main' ].location = '/friends.php?cmd=memo.show&' + Math.random();
}


function processAlign() {
	top.frames[ 'main' ].location = '/align.php?' + Math.random();
}


function processClan() {
	top.frames[ 'main' ].location = '/klan.php?' + Math.random();
}


function processClanRelicts() {
	top.frames[ 'main' ].location = '/klan.php?cmd=relicts&' + Math.random();
}


function processGuild() {
	top.frames[ 'main' ].location = '/paladins.php?' + Math.random();
}


function processGuildRelicts() {
	top.frames[ 'main' ].location = '/paladins.php?cmd=relicts&' + Math.random();
}


function processMentor() {
	top.frames[ 'main' ].location = '/mentor.php?' + Math.random();
}


function processPupil() {
	top.frames[ 'main' ].location = '/mentor.php?' + Math.random();
}


function processAdmin() {
	top.frames[ 'main' ].location = '/angel.php?' + Math.random();
}


function processHelp() {
	top.frames[ 'main' ].location = '/help.php?' + Math.random();
}
