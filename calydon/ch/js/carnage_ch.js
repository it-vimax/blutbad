/*****************************************************************************
* CarnageChat
*/
function CarnageChat( chatParams, chatOptions ) {
	Inherited( this, BaseChat, chatParams, chatOptions );

	this.userRealName = this.chatParams.userRealName;
	this.editorEnabled = false;
	this.isPlaySound = false;
	this.commonChannelOrders = new Array;

	this._userChannels = {};
	this._ignored = chatParams.ignored;
	this._editorDisabledByChannel = false;
	this._editorDisabledByFilter = false;
	this._isChangingRoom = false;
	this._isListCleaned = true;
	this._roomMemberCount = 0;
	this._requests = new Array();
	this._queries = new Array();
	this._tutorial = chatParams.tutorial;
	this._lastPrivateTime = '';
	this._lastPrivateMessage = '';
	this._lastUserChannelIndex = 100;
	this._lastOrderBattleLog = 0;

	this.htmlStuff = {
		channels : 'chat-channels',
		tabId : 'channel-tabs',
		tabPrefix : 'tab-',
		userNameTag : 'SPAN',
		messageTag : 'SPAN',
		channelListPopup : 'channel-list-popup',
		channelListPopup2 : 'channel-list-popup2',
		channelPopupItemPrefix : 'channel-popup-item-',
		channelPopupItemPrefix2 : 'channel-popup-item2-',
		channelHelper : 'channel-helper',
		onlineAutorefreshButton : 'auto',
		onlineRoomName : 'online-room-name',
		onlineListContainer : 'online-list-container',
		onlineList : 'online-list',
		onlinePrefix : 'online-user-',
		onlineUsernamePrefix : 'span',
		onlineListPopup : 'online-list-popup',
		onlinePopupItemPrefix : 'online-popup-item-',
		editorIdHtml : 'editor',
		filterButton : 'filter_img',
		filterLogButton : 'filter_log_img',
		smilesButton : 'smiles_img',
		transButton : 'trans_img',
		soundButton : 'sound_img',
		userContextMenuClasses : { 'user-from-private' : 1, 'user-from-room' : 1, 'user-to-private' : 1, 'user-from-private-hidden' : 1, 'user-from-room-hidden' : 1 },
		messageContextMenuClasses : { 'time-private' : 1, 'time-private-me' : 1, 'time-private-self' : 1, 'time-private-self-me' : 1, 'time-room' : 1, 'time-room-me' : 1, 'time-room-self' : 1, 'time-room-self-me' : 1, 'time-system' : 1, 'time-system-attention' : 1 },
		userPrivateClass : 'user-to-private'

	};

}


Inheritance( CarnageChat, BaseChat );

/*****************************************************************************
* ����������� ������ ����
*****************************************************************************/
CarnageChat.prototype.cmdBurnFire = function( data ) {
	if ( data ) {
		if(top.frames["main"].burnReady){
			top.frames["main"].startSalut(data['type']);
		}
	}
}

CarnageChat.prototype.cmdJoinChannel = function( channel ) {
	if ( channel ) {
		var owner = CH_MAIN;

		if ( this.isGroupChannel( channel[ 'channel' ] ) ) {
			this.chatParams.groupId = channel[ 'channel' ];
		} else if ( this.isPokerChannel( channel[ 'channel' ] ) ) {
			this.chatParams.pokerId = channel[ 'channel' ];
		} else if ( this.isBattleLogChannel( channel[ 'channel' ] ) ) {
			this.chatParams.battleLogId = channel[ 'channel' ];
			owner = CH_BATTLE_LOGS;

		}

		if ( this._needJoinChannel( channel[ 'channel' ] ) ) {
			this.joinChannel( channel[ 'channel' ], channel[ 'channel_name' ], owner );
		}

	}

}


CarnageChat.prototype.cmdLeaveChannel = function( channel ) {
	if ( channel ) {
		if ( this.isGroupChannel( channel[ 'channel' ] ) ) {
			this.chatParams.groupId = '';
		} else if ( this.isPokerChannel( channel[ 'channel' ] ) ) {
			this.chatParams.pokerId = '';
		} else if ( this.isBattleLogChannel( channel[ 'channel' ] ) ) {
			this.chatParams.battleLogId = '';
		}

		this.leaveChannel( channel[ 'channel' ] );

	}

}


CarnageChat.prototype.cmdApplyParams = function( params ) {
	for ( var prop in params ) {
		this.chatParams[ prop ] = params[ prop ];
	}

}


CarnageChat.prototype.cmdApplyOptions = function( options ) {
	for ( var prop in options ) {
		this.chatOptions[ prop ] = options[ prop ];
	}

	if ( this.chatOptions.tradeChannelOff != 0 ) {
		if ( this.channelExists( CH_TRADE ) ) {
			this.removeChannelByName( CH_TRADE );
		}

	} else {
		if ( ! this.channelExists( CH_TRADE ) ) {
			this.addChannel( this.chatParams.tradeRoomId, CH_TRADE ).sendPresence();
		}

	}

	if ( this.chatOptions.systemChannelOff != 0 ) {
		if ( this.channelExists( CH_SYSTEM ) ) {
			this.removeChannelByName( CH_SYSTEM );
		}

	} else {
		if ( ! this.channelExists( CH_SYSTEM ) ) {
			this.addChannel( this.chatParams.systemRoomId, CH_SYSTEM ).sendPresence();
			this.getChannelByName( CH_SYSTEM ).appendText( '<span class="room-name">=== ' + this.roomName + ' ===</span>' );

		}

	}

	if ( this.chatOptions.clanChannelOff != 0 ) {
		if ( this.chatParams.clan > 0 ) {
			this.leaveChannel( 'c' + this.chatParams.clan );
		}

		if ( this.chatParams.guild > 0 ) {
			this.leaveChannel( 'g' + this.chatParams.guild );
		}

	} else {
		if ( this.chatParams.clan > 0 ) {
			this.joinChannel( 'c' + this.chatParams.clan, '��������', CH_MAIN );
		}

		if ( this.chatParams.guild > 0 ) {
			this.joinChannel( 'g' + this.chatParams.guild, '�����������', CH_MAIN );
		}

	}

	if ( this.chatOptions.groupChannelOff != 0 ) {
		if ( this.chatParams.groupId.length > 0 ) {
			this.leaveChannel( this.chatParams.groupId );
		}

	} else {
		if ( this.chatParams.groupId.length > 0 ) {
			this.joinChannel( this.chatParams.groupId, '���������', CH_MAIN );
		}

	}

	if ( this.chatOptions.pokerChannelOff != 0 ) {
		if ( this.chatParams.pokerId.length > 0 ) {
			this.leaveChannel( this.chatParams.pokerId );
		}

	} else {
		if ( this.chatParams.pokerId.length > 0 ) {
			this.joinChannel( this.chatParams.pokerId, '��������', CH_MAIN );
		}

	}

	if ( this.chatOptions.battleLogChannelOff != 0 ) {
		this.removeButton( 'filter_log' );

		if ( this.chatParams.battleLogId.length > 0 ) {
			this.leaveChannel( this.chatParams.battleLogId );
		}

		if ( this.channelExists( CH_BATTLE_LOGS ) ) {
			this.removeChannelByName( CH_BATTLE_LOGS );
		}

	} else {
		if ( ! this._EditorDocument.getElementById( 'filter_log' ) ) {
			this.addButton( 'filter_log' );
			this.setFilterLog();

		}

		if ( ! this.channelExists( CH_BATTLE_LOGS ) ) {
			this.addChannel( this.chatParams.battleLogRoomId, CH_BATTLE_LOGS );
		}

		if ( this.chatParams.battleLogId.length > 0 ) {
			this.joinChannel( this.chatParams.battleLogId, CH_BATTLE_LOG + ' ' + this.chatParams.battleLogId, CH_BATTLE_LOGS );
		}

	}

	if ( this.chatOptions.autoscrollOff == 1 ) {
		for ( var channel in this._channels ) {
			this._channels[ channel ].lastScrollTop = this._channels[ channel ].htmlContainer.scrollTop;
		}

	}

	this._checkChannelList();

}


CarnageChat.prototype.cmdApplyHiddenButtons = function( buttons ) {
	if (this._tutorial) return;

	for ( var button in buttons ) {
		if ( buttons[ button ] != 0 ) {
			this.removeButton( button );
		} else {
			if ( ! this._EditorDocument.getElementById( button ) ) {
				this.addButton( button );
			}

		}

	}

}


CarnageChat.prototype.cmdChangeRoom = function( room ) {
	if ( room ) {
		this.changeRoom( room[ 'room' ], room[ 'room_name' ] );
	}

}


CarnageChat.prototype.cmdInbox = function( data ) {
	if ( data ) {
		top.refreshInbox( data[ 'count' ] );
	}

}


CarnageChat.prototype.cmdSound = function( data ) {
	if ( data ) {
		this.playSound( data[ 'type' ] );
	}

}


CarnageChat.prototype.cmdDeal = function( data ) {
	if (this._tutorial) return;
	if ( data ) {
		top.refreshDeals( data[ 'state' ] );
		if (data['mentor'] && !top.frames[ 'main' ].deal) {
			top.frames["main"].mentorDealDialog(data[ 'username' ],data[ 'entry' ],UserND);
		}
	}

}


CarnageChat.prototype.cmdRefresh = function( data ) {
	if ( data ) {
		if ( ! data[ 'deals' ] || ( data[ 'deals' ] > 0 && top.frames[ 'main' ].deal ) ) {
			top.FrameRefresh( data[ 'url' ], data[ 'frame' ] );
		}

	}

}


CarnageChat.prototype.cmdHelpHint = function( data ) {
	if ( data ) {
		top.showHint( _filterCommandText( data[ 'message' ] ) );
	}

}


CarnageChat.prototype.cmdTutorial = function( data ) {
	if ( data ) {
		top.setTutorialState( data[ 'event' ] );
	}

}

CarnageChat.prototype.cmdReloadPage = function( data ) {
	window.location.href = '/blutbad.php?'+Math.random();
}

CarnageChat.prototype.cmdTradeChannel = function( data ) {
	if ( data ) {
		var cooldown = this.getTimeDiffStr( data[ 'diff' ] );

		if ( this.channelExists( CH_TRADE ) ) {
			this.getChannelByName( CH_TRADE ).appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_TRADE_NOT_ALLOWED + cooldown );
		}

	}

}


CarnageChat.prototype.cmdBattleLog = function( data ) {
	if ( data && data[ 'rows' ] && this.chatOptions.filterLog < 2 ) {
		var rows = _filterMessage( data[ 'rows' ], 2, 1 ).replace( /&#166;/g, '|' ).split( '||' );

		for ( var i = 0; i < rows.length; i++ ) {
			var params = rows[ i ].split( '|' );

			if ( this.chatOptions.filterLog == 1 && params[ 2 ] != this.chatParams.userId && params[ 3 ] != this.chatParams.userId ) {
				continue;
			}

			var message = params[ 4 ];
			var re = new RegExp( /(\[\[(.*?)\]\])/ );

			while ( re.test( message ) ) {
				var token = RegExp.$1;
				var code = RegExp.$2;

				if ( code ) {
					try {
						code = eval( code );
					} catch( e ) {
						code = '';
					}

				} else {
					code = '';
				}

				message = message.replace( token, code );

			}

			var oChannel = this.getChannelById( this.chatParams.battleLogId );

			if ( oChannel ) {
				if ( ! this._lastOrderBattleLog ) {
					this._lastOrderBattleLog = params[ 1 ];
				}

				if ( this._lastOrderBattleLog != params[ 1 ] ) {
					oChannel.appendText( '<img alt="" class="separator" src="http://img.blutbad.ru/i/battle/hr3.gif" />' );
				}

				this._lastOrderBattleLog = params[ 1 ];

				oChannel.appendMessage( '', MT_BATTLE_LOG, params[ 0 ], params[ 2 ], params[ 3 ], message, '', 1 );

			}

		}

	}

}


CarnageChat.prototype.cmdReloadPanel = function( data ) {
	if ( top.frames[ 'panel' ] ) {
		top.frames[ 'panel' ].reload_panel();
	}

}
/*****************************************************************************/


CarnageChat.prototype.run = function() {
	this.parent.run.call( this );
	this._MainDocumentHtml = this._MainDocument.documentElement || this._MainDocument;
	this._MainDocumentBody = this._MainDocumentHtml.getElementsByTagName( 'BODY' )[ 0 ];

	if (!this._tutorial) {
		this._OnlineWindow = top.frames[ 'online' ];
		this._OnlineDocument = this._OnlineWindow.document;
		this._OnlineDocumentHtml = this._OnlineDocument.documentElement || this._OnlineDocument;
		this._OnlineDocumentBody = this._OnlineDocumentHtml.getElementsByTagName( 'BODY' )[ 0 ];

		this._EditorWindow = top.frames[ 'bottom' ];
		this._EditorDocument = this._EditorWindow.document;
		this._EditorDocumentHtml = this._EditorDocument.documentElement || this._EditorDocument;
		this._EditorDocumentBody = this._EditorDocumentHtml.getElementsByTagName( 'BODY' )[ 0 ];
	}


	var oChannels = this._MainDocument.createElement( 'DIV' );
	oChannels.setAttribute( 'id', this.htmlStuff.channels );
	var oUl = this._MainDocument.createElement( 'UL' );
	oUl.id = this.htmlStuff.tabId;
	oChannels.appendChild( oUl );

	this._MainDocumentBody.insertBefore( oChannels, this._MainDocumentBody.firstChild );
	this.tabs = this._MainDocument.getElementById( this.htmlStuff.tabId );

	jQuery( '#' + oChat.htmlStuff.channels ).tabs();
	jQuery( '#' + oChat.htmlStuff.channels ).tabs( 'option', 'panelTemplate', '<div style="line-height:110%; overflow:auto;"></div>' );
	jQuery( '#' + oChat.htmlStuff.channels ).bind( 'tabsselect', function( event, ui ) {
			oChat._changeChannelByName( jQuery( ui.panel ).data( 'name' ) );
			jQuery( ui.tab.parentNode ).removeClass( 'ui-state-new' );

		}

	);

	this.channelListPopup = this._MainDocument.getElementById( this.htmlStuff.channelListPopup );
	this.channelListPopup2 = this._MainDocument.getElementById( this.htmlStuff.channelListPopup2 );
	this.addChannel( MAIN_ID, CH_MAIN );
	this.mainChannel = this.getChannelByName( CH_MAIN );
	this.currentChannelName = CH_MAIN;
	this.onlineRoomName = this._tutorial ? null : this._OnlineDocument.getElementById( this.htmlStuff.onlineRoomName );
	this.onlineList = this._tutorial ? null : this._OnlineDocument.getElementById( this.htmlStuff.onlineList );
	this.onlineListPopup = this._tutorial ? null : this._OnlineDocument.getElementById( this.htmlStuff.onlineListPopup );
	this.editor = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.editorIdHtml );
	this.channelHelper = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.channelHelper );

	var oOnlineList = this.onlineList;
	var oChannelHelper = this.channelHelper;
	var oldOpera = jQuery.browser.opera && jQuery.browser.version < 10.5;

	if ( ! this._tutorial ) {
		jQuery( this._OnlineDocument ).ready( function() {
			jQuery( oOnlineList ).jeegoocontext( oChat.onlineListPopup, {
					hoverClass : '',
					operaEvent : oldOpera ? 'dblclick' : 'contextmenu',
					onShow : oChat._onOnlinePopupShow.bind( oChat ),
					onAfterShow : oChat._onOnlinePopupAfterShow.bind( oChat ),
					onSelect : oChat._onOnlinePopupItemClick.bind( oChat ),
					oWindow : oChat._OnlineWindow

				}

			);

			jQuery( oOnlineList ).click( oChat._onOnlineListClick.bind( oChat ) );
			jQuery( oChannelHelper ).jeegoocontext( oChat.channelListPopup2, {
					event : 'click',
					heightOverflowOffset: 1,
					onShow : oChat._onChannelPopupShow.bind( oChat ),
					onSelect : oChat._onChannelPopupItemClick.bind( oChat ),
					x : -1,
					y : 10000,
					oWindow : oChat._MainWindow

				}

			);

		} );

	}

	for ( var button in CHAT_BUTTON_PARAMS ) {
		if ( ! this.chatOptions.hiddenButtons[ button ] || this.chatOptions.hiddenButtons[ button ] == 0 ) {
			this.addButton( button );
		}

	}

	this.getUsersInRoom();

	var oCheckbox = this._tutorial ? null : this._OnlineDocument.getElementById( this.htmlStuff.onlineAutorefreshButton );

	if ( oCheckbox ) {
		this.setAutorefreshOnlineList( oCheckbox.checked );
	}

}


CarnageChat.prototype.sendMessage = function( message ) {
	if ( ! this.connected() || ! this.editorEnabled || ! message ) {
		return false;
	}

	if ( this.isInJail() ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_SEND_NOT_ALLOWED );

		return false;

	} else if ( this.chatParams.isSilence == 1 ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_SILENCE_ON );

		return false;

	} else {
		var regPrivate = new RegExp( /^[\/\.\!\,](p|private|w|whisper|�|�������|�������)\s+([^:]*?)\s*:\s*(.+)$/i );
		var regClan = new RegExp( /^[\/\.\!\,](c|clan|k|klan|�|����)\s+(.+)$/i );
		var regClanAlly = new RegExp( /^[\/\.\!\,](a|ally|�|����)(\d+)\s+(.+)$/i );
		var regGuild = new RegExp( /^[\/\.\!\,](guild|�������)\s+(.+)$/i );
		var regGroup = new RegExp( /^[\/\.\!\,](g|group|�|������)\s+(.+)$/i );
		var regPoker = new RegExp( /^[\/\.\!\,](poker|�����)\s+(.+)$/i );
		var regChannel = new RegExp( /^[\/\.\!\,](ch|channel|�����)\s+(.+?)\s+(.+)$/i );
		var regBroadcast = new RegExp( /^[\/\.\!\,](b|broadcast)\s+(.+)$/i );
		var regBroadcastGlobal = new RegExp( /^[\/\.\!\,](bg|broadcastglobal)\s+(.+)$/i );
		var regBroadcastCities = new RegExp( /^[\/\.\!\,](bc|broadcastcities)\s+(.+?)\s+(.+)$/i );
		var regDebug = new RegExp( /^[\/\.\!\,](debug)\s+(.+)$/i );
		var regHelp = new RegExp( /^[\/\.\!\,](help|������)$/i );
		var regCreateUserChannel = new RegExp( /^[\/\.\!\,](create|�������)\s+(.+?)(?:\s+(.*))?$/i );
		var regCreateUserChannelMain = new RegExp( /^[\/\.\!\,](cm|createmain|��|��������������)\s+(.+?)(?:\s+(.*))?$/i );
		var regSetPasswordUserChannel = new RegExp( /^[\/\.\!\,](password|������)\s+(.+?)(?:\s+(.*))?$/i );
		var regJoinUserChannel = new RegExp( /^[\/\.\!\,](j|join|�|������������)\s+(.+?)(?:\s+(.*))?$/i );
		var regJoinUserChannelMain = new RegExp( /^[\/\.\!\,](jm|joinmain|��|�������������������)\s+(.+?)(?:\s+(.*))?$/i );
		var regLeaveUserChannel = new RegExp( /^[\/\.\!\,](l|leave|�|�����������)(?:\s+(.*))?$/i );
		var regDestroyUserChannel = new RegExp( /^[\/\.\!\,](destroy|�������)(?:\s+(.*))?$/i );
		var regAdminDestroyUserChannel = new RegExp( /^[\/\.\!\,](adestroy|��������)\s+(.+)$/i );
		var regListUserOwnerChannels = new RegExp( /^[\/\.\!\,](mychannels|���������)$/i );
		var regListUserChannels = new RegExp( /^[\/\.\!\,](channels|������)$/i );
		var regListUserChannelsAdmin = new RegExp( /^[\/\.\!\,](adminchannels|�����������)$/i );
		var regListUserAdminsChannel = new RegExp( /^[\/\.\!\,](admins|������|��������������)(?:\s+(.*))?$/i );
		var regListUserBannedChannel = new RegExp( /^[\/\.\!\,](banned|banlist|�������)(?:\s+(.*))?$/i );
		var regListUserChannelsBroadcast = new RegExp( /^[\/\.\!\,](bch|broadcastchannels|��|�����������������)$/i );
		var regListUserConnectedChannel = new RegExp( /^[\/\.\!\,](list|������)(?:\s+(.*))?$/i );
		var regListUserChannelStyles = new RegExp( /^[\/\.\!\,](cs|channelstyles|��|������������)$/i );
		var regTabUserChannel = new RegExp( /^[\/\.\!\,](t|tab|�|�������)\s+(.+)$/i );
		var regUntabUserChannel = new RegExp( /^[\/\.\!\,](ut|untab|��|��������)(?:\s+(.*))?$/i );
		var regKickUserChannel = new RegExp( /^[\/\.\!\,](kick|���������)\s+(.+?)\s+(.+)$/i );
		var regBanUserChannel = new RegExp( /^[\/\.\!\,](ban|���)\s+(.+?)\s+(.+)$/i );
		var regUnbanUserChannel = new RegExp( /^[\/\.\!\,](unban|������)\s+(.+?)\s+(.+)$/i );
		var regGrantUserChannel = new RegExp( /^[\/\.\!\,](grant|admin|�����|�������������)\s+(.+?)\s+(.+)$/i );
		var regRevokeUserChannel = new RegExp( /^[\/\.\!\,](revoke|deladmin|�������|��������������������)\s+(.+?)\s+(.+)$/i );
		var regRemoveBroadcastUserChannel = new RegExp( /^[\/\.\!\,](rb|removebroadcast|��������|�����������������)(?:\s+(.*))?$/i );
		var regDeleteUserChannelStyle = new RegExp( /^[\/\.\!\,](ds|deletestyle|��|������������)(?:\s+(.*))?$/i );
		var regUserChannelsPresenceOn = new RegExp( /^[\/\.\!\,](pon|presenceon|����|��������������)$/i );
		var regUserChannelsPresenceOff = new RegExp( /^[\/\.\!\,](poff|presenceoff|�����|���������������)$/i );
		var regToMessage = new RegExp( /^[\/\.\!\,](to)\s+(.+)$/i );
		var regChannelMessage = new RegExp( /^[\/\.\!\,](.+?)\s+(.+)$/i );

		if ( regPrivate.test( message ) ) {
			var userTo = RegExp.$2;

			if ( this.chatParams.level > 0 || ( userTo.toLowerCase() == this.chatParams.mentorName.toLowerCase() ) ) {
				return this.sendPrivateMessage( userTo, RegExp.$3 );
			} else {
				return this.sendRoomMessage( '/to ' + userTo + ': ' + RegExp.$3 );
			}

		} else if ( regClan.test( message ) ) {
			return this.sendClanMessage( RegExp.$2 );
		} else if ( regClanAlly.test( message ) ) {
			return this.sendClanAllyMessage( RegExp.$2, RegExp.$3 );
		} else if ( regGuild.test( message ) ) {
			return this.sendGuildMessage( RegExp.$2 );
		} else if ( regGroup.test( message ) ) {
			return this.sendGroupMessage( RegExp.$2 );
		} else if ( regPoker.test( message ) ) {
			return this.sendPokerMessage( RegExp.$2 );
		} else if ( regChannel.test( message ) ) {
			return this.sendChannelMessage( RegExp.$2, RegExp.$3 );
		} else if ( regBroadcast.test( message ) ) {
			return this.sendBroadcastMessage( RegExp.$2 );
		} else if ( regBroadcastGlobal.test( message ) ) {
			return this.sendBroadcastMessage( RegExp.$2, 1 );
		} else if ( regBroadcastCities.test( message ) ) {
			return this.sendBroadcastMessage( RegExp.$3, 0, RegExp.$2 );
		} else if ( regDebug.test( message ) ) {
			return this.sendDebugStatus( RegExp.$2 );
		} else if ( regHelp.test( message ) ) {
			return this.showHelp();
		} else if ( regCreateUserChannel.test( message ) ) {
			return this.createUserChannel( RegExp.$2, RegExp.$3 );
		} else if ( regCreateUserChannelMain.test( message ) ) {
			return this.createUserChannel( RegExp.$2, RegExp.$3, CH_MAIN );
		} else if ( regSetPasswordUserChannel.test( message ) ) {
			return this.setPasswordUserChannel( RegExp.$2, RegExp.$3 );
		} else if ( regJoinUserChannel.test( message ) ) {
			return this.joinUserChannel( RegExp.$2, RegExp.$3 );
		} else if ( regJoinUserChannelMain.test( message ) ) {
			return this.joinUserChannel( RegExp.$2, RegExp.$3, CH_MAIN );
		} else if ( regLeaveUserChannel.test( message ) ) {
			return this.leaveUserChannel( RegExp.$2 );
		} else if ( regDestroyUserChannel.test( message ) ) {
			return this.destroyUserChannel( RegExp.$2 );
		} else if ( regAdminDestroyUserChannel.test( message ) ) {
			return this.adminDestroyUserChannel( RegExp.$2 );
		} else if ( regListUserOwnerChannels.test( message ) ) {
			return this.listUserOwnerChannels();
		} else if ( regListUserChannels.test( message ) ) {
			return this.listUserChannels();
		} else if ( regListUserChannelsAdmin.test( message ) ) {
			return this.listUserChannelsAdmin();
		} else if ( regListUserAdminsChannel.test( message ) ) {
			return this.listUserAdminsChannel( RegExp.$2 );
		} else if ( regListUserBannedChannel.test( message ) ) {
			return this.listUserBannedChannel( RegExp.$2 );
		} else if ( regListUserChannelsBroadcast.test( message ) ) {
			return this.listUserChannelsBroadcast();
		} else if ( regListUserConnectedChannel.test( message ) ) {
			return this.listUserConnectedChannel( RegExp.$2 );
		} else if ( regListUserChannelStyles.test( message ) ) {
			return this.listUserChannelStyles();
		} else if ( regTabUserChannel.test( message ) ) {
			return this.tabUserChannel( RegExp.$2 );
		} else if ( regUntabUserChannel.test( message ) ) {
			return this.untabUserChannel( RegExp.$2 );
		} else if ( regKickUserChannel.test( message ) ) {
			return this.kickUserChannel( RegExp.$2, RegExp.$3 );
		} else if ( regBanUserChannel.test( message ) ) {
			return this.banUserChannel( RegExp.$2, RegExp.$3 );
		} else if ( regUnbanUserChannel.test( message ) ) {
			return this.unbanUserChannel( RegExp.$2, RegExp.$3 );
		} else if ( regGrantUserChannel.test( message ) ) {
			return this.grantUserChannel( RegExp.$2, RegExp.$3 );
		} else if ( regRevokeUserChannel.test( message ) ) {
			return this.revokeUserChannel( RegExp.$2, RegExp.$3 );
		} else if ( regRemoveBroadcastUserChannel.test( message ) ) {
			return this.removeBroadcastUserChannel( RegExp.$2 );
		} else if ( regDeleteUserChannelStyle.test( message ) ) {
			return this.deleteUserChannelStyle( RegExp.$2 );
		} else if ( regUserChannelsPresenceOn.test( message ) ) {
			return this.changePresenceoff( 0 );
		} else if ( regUserChannelsPresenceOff.test( message ) ) {
			return this.changePresenceoff( 1 );
		} else if ( regToMessage.test( message ) ) {
			return this.sendRoomMessage( message );
		} else if ( regChannelMessage.test( message ) ) {
			return this.sendChannelMessage( RegExp.$1, RegExp.$2 );
		} else {
			return this.sendRoomMessage( message );
		}

	}

}


CarnageChat.prototype.sendRoomMessage = function( message ) {
	if ( ! this.connected() || ! this.editorEnabled || _isStringEmpty( message ) ) {
		return false;
	} else if ( _hasBadChars( message ) ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_CHARS );

		return false;

	}

	if ( this.chatOptions.trans == TR_ON ) {
		message = this.translate( message );
	}

	message = _filterMessage( message );

	var oSend = $msg( { to : this.currentChannelId + '@' + XMPP_MUC + '.' + this._domain, from : this.userName + '@' + this._domain + '/' + XMPP_RESOURCE, type : 'groupchat' } ).c( 'body' ).t( message );
	this.send( oSend.tree() );
	this._checkToBot( message );

	return true;

}


CarnageChat.prototype.sendClanMessage = function( message ) {
	if ( ! this.connected() || ! this.editorEnabled || _isStringEmpty( message ) ) {
		return false;
	} else if ( _hasBadChars( message ) ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_CHARS );

		return false;

	}

	if ( this.chatOptions.clanChannelOff == 0 && this.chatParams.clan > 0 ) {
		if ( this.chatOptions.trans == TR_ON ) {
			message = this.translate( message );
		}

		message = _filterMessage( message );
		var oSend = $msg( { to : 'c' + this.chatParams.clan + '@' + XMPP_MUC + '.' + this._domain, from : this.userName + '@' + this._domain + '/' + XMPP_RESOURCE, type : 'groupchat' } ).c( 'body' ).t( message );
		this.send( oSend.tree() );

		return true;

	} else {
		if ( this.chatOptions.clanChannelOff == 1 ) {
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_OFF );
		} else if ( this.chatParams.clan == 0 ) {
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_IN_CLAN );
		}

		return false;

	}

}


CarnageChat.prototype.sendClanAllyMessage = function( clanId, message ) {
	if ( ! this.connected() || ! this.editorEnabled || _isStringEmpty( message ) ) {
		return false;
	} else if ( _hasBadChars( message ) ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_CHARS );

		return false;

	}

	if ( this.chatOptions.trans == TR_ON ) {
		message = this.translate( message );
	}

	message = _filterMessage( message );
	this.loadData( 'user.send_clan_ally', { 'clan' : clanId, 'message' : _escapeEx( message ) }, this._getSendClanAllyResult );

	return true;

}


CarnageChat.prototype.sendGuildMessage = function( message ) {
	if ( ! this.connected() || ! this.editorEnabled || _isStringEmpty( message ) ) {
		return false;
	} else if ( _hasBadChars( message ) ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_CHARS );

		return false;

	}

	if ( this.chatOptions.clanChannelOff == 0 && this.chatParams.guild > 0 ) {
		if ( this.chatOptions.trans == TR_ON ) {
			message = this.translate( message );
		}

		message = _filterMessage( message );
		var oSend = $msg( { to : 'g' + this.chatParams.guild + '@' + XMPP_MUC + '.' + this._domain, from : this.userName + '@' + this._domain + '/' + XMPP_RESOURCE, type : 'groupchat' } ).c( 'body' ).t( message );
		this.send( oSend.tree() );

		return true;

	} else {
		if ( this.chatOptions.clanChannelOff == 1 ) {
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_OFF );
		} else if ( this.chatParams.guild == 0 ) {
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_IN_GUILD );
		}

		return false;

	}

}


CarnageChat.prototype.sendGroupMessage = function( message ) {
	if ( ! this.connected() || ! this.editorEnabled || _isStringEmpty( message ) ) {
		return false;
	} else if ( _hasBadChars( message ) ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_CHARS );

		return false;

	}

	if ( this.chatOptions.groupChannelOff == 0 && this.chatParams.groupId.length > 0 ) {
		if ( this.chatOptions.trans == TR_ON ) {
			message = this.translate( message );
		}

		message = _filterMessage( message );
		var oSend = $msg( { to : this.chatParams.groupId + '@' + XMPP_MUC + '.' + this._domain, from : this.userName + '@' + this._domain + '/' + XMPP_RESOURCE, type : 'groupchat' } ).c( 'body' ).t( message );
		this.send( oSend.tree() );

		return true;

	} else {
		if ( this.chatOptions.groupChannelOff == 1 ) {
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_OFF );
		} else if ( this.chatParams.groupId.length == 0 ) {
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_IN_GROUP );
		}

		return false;

	}

}


CarnageChat.prototype.sendPokerMessage = function( message ) {
	if ( ! this.connected() || ! this.editorEnabled || _isStringEmpty( message ) ) {
		return false;
	} else if ( _hasBadChars( message ) ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_CHARS );

		return false;

	}

	if ( this.chatOptions.pokerChannelOff == 0 && this.chatParams.pokerId.length > 0 ) {
		if ( this.chatOptions.trans == TR_ON ) {
			message = this.translate( message );
		}

		message = _filterMessage( message );
		var oSend = $msg( { to : this.chatParams.pokerId + '@' + XMPP_MUC + '.' + this._domain, from : this.userName + '@' + this._domain + '/' + XMPP_RESOURCE, type : 'groupchat' } ).c( 'body' ).t( message );
		this.send( oSend.tree() );

		return true;

	} else {
		if ( this.chatOptions.pokerChannelOff == 1 ) {
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_OFF );
		} else if ( this.chatParams.pokerId.length == 0 ) {
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_IN_POKER );
		}

		return false;

	}

}


CarnageChat.prototype.sendChannelMessage = function( channel, message ) {
	if ( ! this.connected() || ! this.editorEnabled || _isStringEmpty( channel ) || _isStringEmpty( message ) ) {
		return false;
	} else if ( _hasBadChars( message ) ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_CHARS );

		return false;

	}

	if ( ! _checkUserChannel( channel ) ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	} else {
		if ( this.chatOptions.trans == TR_ON ) {
			message = this.translate( message );
		}

		message = _filterMessage( message );
		var oSend = $msg( { to : BASE_USER_CHANNEL_NAME + channel + '@' + XMPP_MUC + '.' + this._domain, from : this.userName + '@' + this._domain + '/' + XMPP_RESOURCE, type : 'groupchat' } ).c( 'body' ).t( message );
		this.send( oSend.tree() );

		return true;

	}

}


CarnageChat.prototype.sendBroadcastMessage = function( message, global, cities ) {
	if ( ! this.connected() || ! this.editorEnabled || _isStringEmpty( message ) ) {
		return false;
	} else if ( _hasBadChars( message ) ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_CHARS );

		return false;

	}

	if ( this.chatOptions.trans == TR_ON ) {
		message = this.translate( message );
	}

	message = _filterMessage( message );
	global = global ? 1 : 0;
	this.loadData( 'user.send_broadcast', { 'message' : _escapeEx( message ), 'global' : global, 'cities' : cities }, this._getSendBroadcastResult );

	return true;

}


CarnageChat.prototype.sendDebugStatus = function( status ) {
	if ( ! this.connected() || ! this.editorEnabled || _isStringEmpty( status ) ) {
		return false;
	}

	if ( isNaN( status ) || status < 0 ) {
		status = 0;
	} else {
		status = parseInt( status );
	}

	this.loadData( 'user.send_debugstatus', { 'status' : status }, this._getSendDebugStatusResult );

	return true;

}


CarnageChat.prototype.sendToBot = function( type, name, message ) {
	if ( ! this.connected() || ! this.editorEnabled || ! message ) {
		return false;
	} else if ( _hasBadChars( message ) ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_CHARS );

		return false;

	}

	if ( ! _isStringEmpty( message ) ) {
		if ( this.chatOptions.trans == TR_ON ) {
			message = this.translate( message );
		}

		message = _filterMessage( message );
		this.loadData( 'user.send_to_bot', { 'type' : type, 'name' : _escapeEx( name ), 'message' : _escapeEx( message ) }, this._getSendToBotResult );

		return true;

	} else {
		return false;
	}

}


CarnageChat.prototype.showHelp = function() {
	var oChannel = this.getChannelByName( this.currentChannelName );

	if ( oChannel ) {
		oChannel.appendText( HELP_TEXT );
	}

	return true;

}


CarnageChat.prototype.clear = function() {
	var oChannel = this.getChannelByName( this.currentChannelName );

	if ( oChannel ) {
		oChannel.clear();
	}

}


CarnageChat.prototype.changeNextFilter = function() {
	if ( ! this.connected() ) {
		return false;
	}

	var oImage = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.filterButton );

	if ( oImage ) {
		var filter = ( this.chatOptions.filter + 1 ) % 3;
		this.loadData( 'user.change_filter', { 'filter' : filter }, this._getChangeFilterResult );

	}

}


CarnageChat.prototype.setFilter = function() {
	var oImage = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.filterButton );

	if ( oImage ) {
		oImage.src = CHAT_FILTERS[ this.chatOptions.filter ].image;
		oImage.alt = CHAT_FILTERS[ this.chatOptions.filter ].title;
		oImage.title = CHAT_FILTERS[ this.chatOptions.filter ].title;

		if ( this.chatOptions.filter == FT_OFF ) {
			this._editorDisabledByFilter = true;
		} else {
			this._editorDisabledByFilter = false;
		}

		this.checkEditor();

	}

}


CarnageChat.prototype.changeNextFilterLog = function() {
	if ( ! this.connected() ) {
		return false;
	}

	var oImage = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.filterLogButton );

	if ( oImage ) {
		var filterLog = ( this.chatOptions.filterLog + 1 ) % 3;
		this.loadData( 'user.change_filter_log', { 'filter_log' : filterLog }, this._getChangeFilterLogResult );

	}

}


CarnageChat.prototype.setFilterLog = function() {
	var oImage = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.filterLogButton );

	if ( oImage ) {
		oImage.src = CHAT_FILTERS_LOG[ this.chatOptions.filterLog ].image;
		oImage.alt = CHAT_FILTERS_LOG[ this.chatOptions.filterLog ].title;
		oImage.title = CHAT_FILTERS_LOG[ this.chatOptions.filterLog ].title;

	}

}


CarnageChat.prototype.switchTrans = function() {
	var oImage = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.transButton );

	if ( oImage ) {
		var trans = ( this.chatOptions.trans + 1 ) % 2;
		oImage.src = CHAT_TRANSES[ trans ].image;
		oImage.alt = CHAT_TRANSES[ trans ].title;
		oImage.title = CHAT_TRANSES[ trans ].title;
		this.loadData( 'user.switch_trans', { 'trans' : trans }, this._getSwitchTransResult );

	}

}


CarnageChat.prototype.setTrans = function() {
	var oImage = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.transButton );

	if ( oImage ) {
		oImage.src = CHAT_TRANSES[ this.chatOptions.trans ].image;
		oImage.alt = CHAT_TRANSES[ this.chatOptions.trans ].title;
		oImage.title = CHAT_TRANSES[ this.chatOptions.trans ].title;

	}

}


CarnageChat.prototype.changeNextSound = function() {
	var oImage = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.soundButton );

	if ( oImage ) {
		var sound = ( this.chatOptions.sound + 1 ) % 3;
		oImage.src = CHAT_SOUNDS[ sound ].image;
		oImage.alt = CHAT_SOUNDS[ sound ].title;
		oImage.title = CHAT_SOUNDS[ sound ].title;
		this.loadData( 'user.change_sound', { 'sound' : sound }, this._getChangeSoundResult );

	}

}


CarnageChat.prototype.setSound = function() {
	var oImage = this._tutorial ? null : this._EditorDocument.getElementById( this.htmlStuff.soundButton );

	if ( oImage ) {
		oImage.src = CHAT_SOUNDS[ this.chatOptions.sound ].image;
		oImage.alt = CHAT_SOUNDS[ this.chatOptions.sound ].title;
		oImage.title = CHAT_SOUNDS[ this.chatOptions.sound ].title;

	}

}


CarnageChat.prototype.changePresenceoff = function( value ) {
	value = value ? 1 : 0;
	this.loadData( 'user.change_presenceoff', { 'value' : value }, this._getChangePresenceoffResult );

	return true;

}


CarnageChat.prototype.writeToEditor = function( text, insertType ) {
	if (this._tutorial) return;

	if ( ! this.editorEnabled ) {
		this.selectChannelByName( CH_MAIN );
	}

	if ( this.editorEnabled ) {
		this.editor.focus();
		var newValue = text + this.editor.value;

		if ( insertType == TO_NICK ) {
			newValue = this._addToList( text, this.editor.value, TO_NICK );
		} else if ( insertType == PRIVATE_NICK ) {
			newValue = this._addToList( text, this.editor.value, PRIVATE_NICK );
		}

		this.editor.value = newValue;

	}

}


CarnageChat.prototype.writeToEditorTo = function( userRealName ) {
	this.writeToEditor( userRealName, TO_NICK );
}


CarnageChat.prototype.writeToEditorPrivate = function( userRealName ) {
	this.writeToEditor( userRealName, PRIVATE_NICK );
}


CarnageChat.prototype.processCommand = function( command ) {
	if ( ! command ) {
		return;
	}

	var cmd = command[ 'name' ];
	delete( command[ 'name' ] );

	switch ( cmd ) {
		case CMD_CHANGE_ROOM:
			this.cmdChangeRoom( command );
		break;

		case CMD_JOIN_CHANNEL:
			this.cmdJoinChannel( command );
		break;

		case CMD_LEAVE_CHANNEL:
			this.cmdLeaveChannel( command );
		break;

		case CMD_PARAMS:
			this.cmdApplyParams( command );
		break;

		case CMD_OPTIONS:
			this.cmdApplyOptions( command );
		break;

		case CMD_BUTTONS:
			this.cmdApplyHiddenButtons( command );
		break;

		case CMD_INBOX:
			this.cmdInbox( command );
		break;

		case CMD_SOUND:
			this.cmdSound( command );
		break;

		case CMD_DEAL:
			this.cmdDeal( command );
		break;

		case CMD_REFRESH:
			this.cmdRefresh( command );
		break;

		case CMD_HELP_HINT:
			this.cmdHelpHint( command );
		break;

		case CMD_TRADE_CHANNEL:
			this.cmdTradeChannel( command );
		break;

		case CMD_TUTORIAL:
			this.cmdTutorial( command );
		break;
		
		case CMD_RELOAD_PAGE:
			this.cmdReloadPage( command );
		break;

		case CMD_BURN_FIRE:
			this.cmdBurnFire( command );
		break;

		case CMD_BATTLE_LOG:
			this.cmdBattleLog( command );
		break;

		case CMD_RELOAD_PANEL:
			this.cmdReloadPanel( command );
		break;

	}

}


CarnageChat.prototype.formatMessage = function( messageId, type, time, from, to, text, style, special, oChannel ) {
	var originalText = text;
	var messageLink = '';

	if ( messageId > 1 ) {
		messageLink = ' id="' + messageId + '" style="cursor:default;"';
	}

	var reg = new RegExp( /^\[(.+?)\]\s?(.*)$/ );
	var fromReal = from;
	var fromHidden = false;

	if ( reg.test( text ) ) {
		var _realName = RegExp.$1;
		var _text = RegExp.$2;

		if ( this.userName == from || this.getChatUsername( _realName ) == from || this.isNameAdmin( from ) ) {
			fromReal = _realName;
			text = _text;

			var invRe = new RegExp( /^hidden\|(.+)$/ );

			if ( invRe.test( fromReal ) ) {
				fromHidden = true;
				fromReal = RegExp.$1;

			}

		}

	}

	if ( this.isNameAdmin( fromReal ) ) {
		fromReal = '';
	}

	fromReal = this.getRealUsername( fromReal );
	var attrFromReal = fromReal;

	if ( fromReal.length > 0 && this.isUserIgnored( fromReal ) ) {
		return '';
	}

	if ( this.isNameHidden( fromReal ) ) {
		fromReal = '<i>' + HIDDEN_REALNAME + '</i>';
		attrFromReal = '';

	}

	/*
	if ( this.chatOptions.antimatOff == 0 ) {
		text = processAntimat( text );
	}
	*/

	text = this.preProcessSmiles( text );

	if ( ! special ) {
		var regPrivate = new RegExp( /^private\s+\[(.+?)\]\s?(.*)$/ );
		var toStr = this.userRealName;
		var isPrivate = false;

		if ( regPrivate.test( text ) ) {
			isPrivate = true;
			toStr = RegExp.$1;
			text = RegExp.$2;

		}

		text = processAntiurl( text );

		if ( _isStringEmpty( text ) ) {
			return '';
		}

		if ( isPrivate ) {
			text = 'private [' + toStr + '] ' + text;
		}

	}

	text = this.processSmiles( text, MAX_SMILES );
	text = this.processNames( text );

	if ( _isStringEmpty( text ) ) {
		return '';
	}

	var self = ( from == this.userName || fromReal == this.userRealName ) ? '-self' : '';
	var toMe = to == this.userRealName ? '-me' : '';
	fromHidden = fromHidden ? '-hidden' : '';

	if ( type == MT_PRIVATE || type == MT_TELEPATHY ) {
		var regPrivate = new RegExp( /^private\s+\[(.+?)\]\s?(.*)$/ );
		var toStr = this.userRealName;

		if ( regPrivate.test( text ) ) {
			toStr = RegExp.$1;
			text = RegExp.$2;

		}

		var attrToStr = toStr;

		if ( ! self ) {
			attrToStr = _replaceElement( this.userRealName, attrFromReal, toStr );
		}

		tos = this._createPrivateList( toStr, attrToStr );
		text = this.setTextStyle( text, style );

		if ( toMe.length > 0 ) {
			if ( self.length > 0 ) {
				if ( messageId > 1 ) {
					self = '';
					this.isPlaySound = true;

				}

			} else {
				this.isPlaySound = true;
			}

		}

		var telepathy = '';

		if ( type == MT_TELEPATHY ) {
			telepathy = '<img src="http://img.blutbad.ru/i/telepathy.gif"> ';
		}

		var openTag = '';
		var closeTag = '';

		if ( fromReal == this.chatParams.mentorName ) {
			openTag = '<span class="mentor">';
			closeTag = '</span>';

		} else if ( fromReal.toLowerCase() == BOT_NAME ) {
			openTag = '<span class="bot">';
			closeTag = '</span>';

		}

		return '<span class="time-private' + self + toMe + '"' + messageLink + '>' + time + '</span> ' + telepathy + openTag + '[<span class="user-from-private' + fromHidden + '" name="' + attrFromReal + '">' + fromReal + '</span>] <span class="user-to-private" data="' + attrToStr + '">private [</span>' + tos + '<span class="user-to-private" data="' + attrToStr + '">]</span> ' + text + closeTag;

	} else if ( type == MT_ROOM || type == MT_CLAN || type == MT_GUILD || type == MT_GROUP || type == MT_POKER || type == MT_ALIENSPEECH ) {
		text = text.replace( /[\/\.\!\,]to\s+([^:]*?)\s*:\s*/g, 'to [$1] ' );
		var regTo = new RegExp( /(to\s+\[([^\[\]]+)\])/ );

		while ( regTo.test( text ) ) {
			var capture = RegExp.$1;
			var userList = _getList( RegExp.$2 );
			var toStr = 'to&nbsp;[' + userList.join( ', ' ) + ']';

			if ( _findUser( this.userRealName, userList ) ) {
				toMe = '-me';
				text = text.replace( capture, '<span class="user-to-room-me">' + toStr + '</span>' );

				if ( self == '' ) {
					this.isPlaySound = true;
				}

			} else {
				text = text.replace( capture, toStr );
			}

		}

		if ( type == MT_ROOM ) {
			text = this.setTextStyle( text, style );

			var openTag = '';
			var closeTag = '';

			if ( fromReal.toLowerCase() == BOT_NAME ) {
				openTag = '<span class="bot">';
				closeTag = '</span>';

			}

			var fromCity = '';
			var userChannel = '';
			var userChannelStyle = '';
			var openTagUser = '';
			var closeTagUser = '';

			if ( this.isUserChannel( oChannel.id ) ) {
				if ( ! oChannel.selfOutput ) {
					userChannel = '<span class="user-channel">[' + this.getUserChannelName( oChannel.id ) + ']</span> ';
					userChannelStyle = this.getUserChannelStyle( oChannel );

					if ( userChannelStyle ) {
						openTagUser = '<span style="' + userChannelStyle + '">';
						closeTagUser = '</span>';

					}

				}

				var regCity = new RegExp( /^\d+\.(\d+)$/ );

				if ( regCity.test( '' + messageId ) ) {
					var city = RegExp.$1;

					if ( CITIES[ city ] && CITIES[ city ] != this.chatParams.cityName ) {
						fromCity = '.' + CITIES[ city ];
					}

				}

			}

			return '<span class="time-room' + self + toMe + '"' + messageLink + '>' + time + '</span> ' + openTagUser + userChannel + openTag + ( fromReal.length > 0 ? '[<span class="user-from-room' + fromHidden + '" name="' + attrFromReal + '">' + fromReal + fromCity + '</span>] ' : '' ) + text + closeTag + closeTagUser;

		} else {
			if ( ! messageId ) {
				self = '-self';
			}

			return '<span class="time-room' + self + toMe + '"' + messageLink + '>' + time + '</span> <span class="' + type + '">' + ( fromReal.length > 0 ? '[<span class="user-from-room' + fromHidden + '" name="' + attrFromReal + '">' + fromReal + '</span>] ' : '' ) + text + '</span>';

		}

	} else if ( type == MT_BATTLE_LOG ) {
		var me = '';

		if ( from == this.chatParams.userId || to == this.chatParams.userId ) {
			me = '-me';
		}

		return '<span class="time-battle-log' + me + '">' + time + '</span> ' + text;

	} else if ( type == MT_SYS_ATTENTION ) {
		return ( time ? '<span class="time-system-attention"' + messageLink + '>' + time + '</span> ' : '' ) + '<span class="system-attention">��������!</span> ' + text;
	} else if ( type == MT_SYS_COMMON ) {
		return '<span class="time-system"' + messageLink + '>' + time + '</span> <span class="system">' + ( fromReal.length > 0 ? '[<span class="user">' + fromReal + '</span>] ' : '' ) + text + '</span>';
	} else if ( type == MT_COMMERCE ) {
		return '<span class="time-system-attention"' + messageLink + '>' + time + '</span> <span class="system-attention">��������!</span> <span class="cityshout"><span class="user-from-room" name="' + attrFromReal + '">' + fromReal + '</span>: ' + text + '</span>';
	} else if ( type == MT_CITY_SHOUT ) {
		return '<span class="time-system-attention"' + messageLink + '>' + time + '</span> <span class="system-attention">��������!</span> <span class="cityshout"><span class="user-from-room" name="' + attrFromReal + '">' + fromReal + '</span>: ' + text + '</span>';
	} else if ( type == MT_CITY_SHOUT2 ) {
		return '<span class="time-system-attention"' + messageLink + '>' + time + '</span> <span class="system-attention">��������!</span> <span class="cityshout2"><span class="user-from-room" name="' + attrFromReal + '">' + fromReal + '</span>: ' + text + '</span>';
	} else if ( type == MT_CITY_ALIENSPEECH ) {
		return '<span class="time-system-attention"' + messageLink + '>' + time + '</span> <span class="system-attention">��������!</span> <span class="city_alienspeech"><span class="user-from-room" name="' + attrFromReal + '">' + fromReal + '</span>: ' + text + '</span>';
	} else {
		return '';
	}

}


CarnageChat.prototype.changeRoom = function( roomId, roomName ) {
	if ( roomId ) {
		if ( roomId != this.roomId ) {
			var oPres = $pres( { to : this.roomId + '@' + XMPP_MUC + '.' + this._domain, type : 'unavailable' } );
			this.send( oPres.tree() );

		}

		if ( ! this._tutorial ) {
			var oPres = $pres( { to : roomId + '@' + XMPP_MUC + '.' + this._domain + '/' + this.userName } );
			this.send( oPres.tree() );
			this.roomId = roomId;
			this.roomName = roomName;

			if ( this.currentChannelName == CH_MAIN ) {
				this.currentChannelId = this.roomId;
			}

		} else {
			this.mainChannel.appendText( '<span class="room-name">=== �������� ===</span>' );
		}

		this.getUsersInRoom();

	}

}


CarnageChat.prototype.joinChannel = function( channelId, name, outputName, password ) {
	if ( ! this.connected() ) {
		return;
	}

	if ( this.channelExists( name ) ) {
		this.removeChannelByName( name, 1 );
	}

	this.addChannel( channelId, name, outputName, 1 ).sendPresence( password );

}


CarnageChat.prototype.leaveChannel = function( channelId ) {
	if ( ! this.connected() ) {
		return;
	}

	if ( this.channelExists( channelId ) ) {
		var oPres = $pres( { to : channelId + '@' + XMPP_MUC + '.' + this._domain, type : 'unavailable' } );
		this.send( oPres.tree() );
		this.removeChannelById( channelId, true );

	}

}


CarnageChat.prototype.createUserChannel = function( name, password, outputName ) {
	name = '' + name;

	if ( typeof( password ) != 'undefined' ) {
		password = '' + password;
	} else {
		password = '';
	}

	if ( typeof( outputName ) != 'undefined' ) {
		outputName = '' + outputName;
	} else {
		outputName = '';
	}

	var checkName = _checkUserChannel( name );
	var checkReserved = ! _isReservedName( name );
	var checkPassword = _checkUserChannelPassword( password );

	if ( checkName && checkReserved && checkPassword ) {
		this.loadData( 'user.create_channel', { 'name' : _escapeEx( name.toLowerCase() ), 'password' : _escapeEx( password ), 'output' : _escapeEx( outputName ) }, this._getCreateChannelResult );

		return true;

	} else if ( ! checkName ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	} else if ( ! checkReserved ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_RESERVED_CHANNEL_NAME );

		return false;

	} else if ( ! checkPassword ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_PASSWORD );

		return false;

	}

}


CarnageChat.prototype.setPasswordUserChannel = function( name, password ) {
	name = '' + name;

	if ( typeof( password ) != 'undefined' ) {
		password = '' + password;
	} else {
		password = '';
	}

	var checkName = _checkUserChannel( name );
	var checkPassword = _checkUserChannelPassword( password );

	if ( checkName && checkPassword ) {
		this.loadData( 'user.set_password_channel', { 'name' : _escapeEx( name.toLowerCase() ), 'password' : _escapeEx( password ) }, this._getSetPasswordChannelResult );

		return true;

	} else if ( ! checkName ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	} else if ( ! checkPassword ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_PASSWORD );

		return false;

	}

}


CarnageChat.prototype.joinUserChannel = function( name, password, outputName ) {
	name = '' + name;

	if ( typeof( password ) != 'undefined' ) {
		password = '' + password;
	} else {
		password = '';
	}

	if ( typeof( outputName ) != 'undefined' ) {
		outputName = '' + outputName;
	} else {
		outputName = '';
	}

	var checkName = _checkUserChannel( name );
	var checkPassword = _checkUserChannelPassword( password );

	if ( checkName && checkPassword ) {
		var oPres = $pres( { to : BASE_USER_CHANNEL_NAME + name + '@' + XMPP_MUC + '.' + this._domain + '/' + this.userName, self : 1, output : outputName } );

		if ( password.length > 0 ) {
			oPres.c( 'x', { xmlns : NS_MUC } ).c( 'password' ).t( password );
		}

		this.send( oPres.tree() );

		return true;

	} else if ( ! checkName ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	} else if ( ! checkPassword ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_PASSWORD );

		return false;

	}

}


CarnageChat.prototype.leaveUserChannel = function( name ) {
	name = this.takeUserChannelName( name );

	if ( name == '' ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNEL_NAME );

		return false;

	} else if ( _checkUserChannel( name ) ) {
		var oPres = $pres( { to : BASE_USER_CHANNEL_NAME + name + '@' + XMPP_MUC + '.' + this._domain + '/' + this.userName, type : 'unavailable', self : 1 } );
		this.send( oPres.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.destroyUserChannel = function( name ) {
	name = this.takeUserChannelName( name );

	if ( name == '' ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNEL_NAME );

		return false;

	} else if ( _checkUserChannel( name ) ) {
		var oQuery = new CarnageQuery( Q_DESTROY_CHANNEL, ID_DESTROY_CHANNEL + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { name : name, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : BASE_USER_CHANNEL_NAME + name + '@' + XMPP_MUC + '.' + this._domain, type : 'set' } ).c( 'query', { xmlns : NS_MUC_OWNER } ).c( 'destroy', { jid : BASE_USER_CHANNEL_NAME + name + '@' + XMPP_MUC + '.' + this._domain } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.adminDestroyUserChannel = function( name ) {
	name = ( '' + name ).toLowerCase();

	if ( _checkUserChannel( name ) ) {
		this.loadData( 'user.destroy_channel', { 'name' : _escapeEx( name ) }, this._getDestroyChannelResult );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.listUserOwnerChannels = function() {
	var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_OWNER_CHANNEL_LIST + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { userName : this.userName } );
	this._queries.push( oQuery );
	var oIq = $iq( { id : oQuery.id, to : this.userName + '@' + this._domain, type : 'get' } ).c( 'query', { xmlns : NS_USER_CHANNEL } ).c( 'item', { type : 'owner' } );
	this.send( oIq.tree() );

	return true;

}


CarnageChat.prototype.listUserChannels = function() {
	var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_CHANNEL_LIST + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { userName : this.userName } );
	this._queries.push( oQuery );
	var oIq = $iq( { id : oQuery.id, to : this.userName + '@' + this._domain, type : 'get' } ).c( 'query', { xmlns : NS_USER_CHANNEL } ).c( 'item', { type : 'connected' } );
	this.send( oIq.tree() );

	return true;

}


CarnageChat.prototype.listUserChannelsAdmin = function() {
	var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_CHANNELS_ADMIN_LIST + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { userName : this.userName } );
	this._queries.push( oQuery );
	var oIq = $iq( { id : oQuery.id, to : this.userName + '@' + this._domain, type : 'get' } ).c( 'query', { xmlns : NS_USER_CHANNEL } ).c( 'item', { type : 'channelsadmin' } );
	this.send( oIq.tree() );

	return true;

}


CarnageChat.prototype.listUserAdminsChannel = function( name ) {
	name = this.takeUserChannelName( name );

	if ( name == '' ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNEL_NAME );

		return false;

	} else if ( _checkUserChannel( name ) ) {
		var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_ADMIN_CHANNEL_LIST + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { name : name, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : this.userName + '@' + this._domain, type : 'get' } ).c( 'query', { xmlns : NS_USER_CHANNEL } ).c( 'item', { type : 'admin', channel : name } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.listUserBannedChannel = function( name ) {
	name = this.takeUserChannelName( name );

	if ( name == '' ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNEL_NAME );

		return false;

	} else if ( _checkUserChannel( name ) ) {
		var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_BANNED_CHANNEL_LIST + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { name : name, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : BASE_USER_CHANNEL_NAME + name + '@' + XMPP_MUC + '.' + this._domain, type : 'get' } ).c( 'query', { xmlns : NS_MUC_ADMIN } ).c( 'item', { affiliation : 'outcast' } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.listUserChannelsBroadcast = function() {
	var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_CHANNELS_BROADCAST_LIST + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { userName : this.userName } );
	this._queries.push( oQuery );
	var oIq = $iq( { id : oQuery.id, to : this.userName + '@' + this._domain, type : 'get' } ).c( 'query', { xmlns : NS_USER_CHANNEL } ).c( 'item', { type : 'channelsbroadcast' } );
	this.send( oIq.tree() );

	return true;

}


CarnageChat.prototype.listUserConnectedChannel = function( name ) {
	name = this.takeUserChannelName( name );

	if ( name == '' ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNEL_NAME );

		return false;

	} else if ( _checkUserChannel( name ) ) {
		if ( this.channelExists( name ) ) {
			var users = new Array();

			for ( var realUser in this._userChannels[ name ] ) {
				users.push( realUser );
			}

			var oChannel = this.getChannelByName( this.currentChannelName );

			if ( ! oChannel ) {
				oChannel = this.mainChannel;
			}

			if ( users.length > 0 ) {
				users = _sortLex( users );

				for ( var i = 0; i < users.length; i++ ) {
					users[ i ] = '{[' + users[ i ] + ']}';
				}

				oChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '� ������ <b>' + name + '</b> ����������: ' + users.join( ', ' ) );

			} else {
				oChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_OTHER_CONNECTED + '<b>' + name + '</b>' );
			}

		} else {
			this.selectChannelByName( CH_MAIN );
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_ACCEPTABLE + '<b>' + name + '</b>' );

		}

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.listUserChannelStyles = function() {
	var listStr = '';

	for ( var name in this.chatParams.userChannelStyles ) {
		listStr += '<p>����� <b>' + name + '</b>: ' + '<span style="' + this.chatParams.userChannelStyles[ name ] + '">����� ��� ���� ������� ����������� �������</span></p>';
	}

	if ( listStr ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_SET_STYLES + '<br>' + listStr + '<br>' );

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNEL_SET_STYLES );

	}

	return true;

}


CarnageChat.prototype.tabUserChannel = function( name ) {
	name = ( '' + name ).toLowerCase();

	if ( _checkUserChannel( name ) ) {
		var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_TAB_CHANNEL + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { name : name, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : this.userName + '@' + this._domain, type : 'set' } ).c( 'query', { xmlns : NS_USER_CHANNEL } ).c( 'item', { action : 'tab', channel : name } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.untabUserChannel = function( name ) {
	name = this.takeUserChannelName( name );

	if ( name == '' ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNEL_NAME );

		return false;

	} else if ( _checkUserChannel( name ) ) {
		var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_UNTAB_CHANNEL + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { name : name, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : this.userName + '@' + this._domain, type : 'set' } ).c( 'query', { xmlns : NS_USER_CHANNEL } ).c( 'item', { action : 'untab', channel : name } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.kickUserChannel = function( channel, user ) {
	channel = ( '' + channel ).toLowerCase();

	if ( _checkUserChannel( channel ) ) {
		var oQuery = new CarnageQuery( Q_KICK_USER, ID_KICK_USER + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { channel : channel, user : user, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : BASE_USER_CHANNEL_NAME + channel + '@' + XMPP_MUC + '.' + this._domain, type : 'set' } ).c( 'query', { xmlns : NS_MUC_ADMIN } ).c( 'item', { nick : this.getChatUsername( user ), role : 'none' } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.banUserChannel = function( channel, user ) {
	channel = ( '' + channel ).toLowerCase();

	if ( _checkUserChannel( channel ) ) {
		var oQuery = new CarnageQuery( Q_BAN_USER, ID_BAN_USER + Math.floor( Math.random() * 4294967295 ) + '|' + this.getChatUsername( user ), { channel : channel, user : user, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : BASE_USER_CHANNEL_NAME + channel + '@' + XMPP_MUC + '.' + this._domain, type : 'set' } ).c( 'query', { xmlns : NS_MUC_ADMIN } ).c( 'item', { jid : this.getChatUsername( user ) + '@' + this._domain, affiliation : 'outcast' } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.unbanUserChannel = function( channel, user ) {
	channel = ( '' + channel ).toLowerCase();

	if ( _checkUserChannel( channel ) ) {
		var oQuery = new CarnageQuery( Q_UNBAN_USER, ID_UNBAN_USER + Math.floor( Math.random() * 4294967295 ) + '|' + this.getChatUsername( user ), { channel : channel, user : user, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : BASE_USER_CHANNEL_NAME + channel + '@' + XMPP_MUC + '.' + this._domain, type : 'set' } ).c( 'query', { xmlns : NS_MUC_ADMIN } ).c( 'item', { jid : this.getChatUsername( user ) + '@' + this._domain, affiliation : 'none' } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.grantUserChannel = function( channel, user ) {
	channel = ( '' + channel ).toLowerCase();

	if ( _checkUserChannel( channel ) ) {
		var oQuery = new CarnageQuery( Q_GRANT_ADMIN, ID_GRANT_ADMIN + Math.floor( Math.random() * 4294967295 ) + '|' + this.getChatUsername( user ), { channel : channel, user : user, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : BASE_USER_CHANNEL_NAME + channel + '@' + XMPP_MUC + '.' + this._domain, type : 'set' } ).c( 'query', { xmlns : NS_MUC_ADMIN } ).c( 'item', { jid : this.getChatUsername( user ) + '@' + this._domain, affiliation : 'owner' } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.revokeUserChannel = function( channel, user ) {
	channel = ( '' + channel ).toLowerCase();

	if ( _checkUserChannel( channel ) ) {
		var oQuery = new CarnageQuery( Q_REVOKE_ADMIN, ID_REVOKE_ADMIN + Math.floor( Math.random() * 4294967295 ) + '|' + this.getChatUsername( user ), { channel : channel, user : user, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : BASE_USER_CHANNEL_NAME + channel + '@' + XMPP_MUC + '.' + this._domain, type : 'set' } ).c( 'query', { xmlns : NS_MUC_ADMIN } ).c( 'item', { jid : this.getChatUsername( user ) + '@' + this._domain, affiliation : 'none' } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.removeBroadcastUserChannel = function( name ) {
	name = this.takeUserChannelName( name );

	if ( name == '' ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNEL_NAME );

		return false;

	} else if ( _checkUserChannel( name ) ) {
		var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_REMOVE_BROADCAST + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { name : name, userName : this.userName } );
		this._queries.push( oQuery );
		var oIq = $iq( { id : oQuery.id, to : this.userName + '@' + this._domain, type : 'set' } ).c( 'query', { xmlns : NS_USER_CHANNEL } ).c( 'item', { action : 'removebroadcast', channel : name } );
		this.send( oIq.tree() );

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.deleteUserChannelStyle = function( name ) {
	name = this.takeUserChannelName( name );

	if ( name == '' ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNEL_NAME );

		return false;

	} else if ( _checkUserChannel( name ) ) {
		if ( this.chatParams.userChannelStyles[ name ] ) {
			this.loadData( 'user.delete_user_channel_style', { 'name' : _escapeEx( name ) }, this._getDeleteUserChannelStyleResult );
		} else {
			this.selectChannelByName( CH_MAIN );
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ������ ����� ��� �������� ������ <b>' + name + '</b>' );

		}

		return true;

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

		return false;

	}

}


CarnageChat.prototype.joinUserChannels = function() {
	var oQuery = new CarnageQuery( Q_USER_CHANNEL, ID_JOIN_CHANNELS + Math.floor( Math.random() * 4294967295 ) + '|' + this.userName, { userName : this.userName } );
	this._queries.push( oQuery );
	var oIq = $iq( { id : oQuery.id, to : this.userName + '@' + this._domain, type : 'get' } ).c( 'query', { xmlns : NS_USER_CHANNEL } ).c( 'item', { type : 'join' } );
	this.send( oIq.tree() );

}


CarnageChat.prototype.addUserToIgnore = function( realName ) {
	if ( realName ) {
		this._ignored[ realName.toLowerCase() ] = true;
	}

}


CarnageChat.prototype.removeUserFromIgnore = function( realName ) {
	if ( realName ) {
		delete( this._ignored[ realName.toLowerCase() ] );
	}

}


CarnageChat.prototype.addUserChannelStyle = function( name, style ) {
	if ( name ) {
		this.chatParams.userChannelStyles[ name ] = style;
	}

}


CarnageChat.prototype.removeUserChannelStyle = function( name ) {
	if ( name ) {
		delete( this.chatParams.userChannelStyles[ name ] );
	}

}


CarnageChat.prototype.addSmile = function( smile ) {
	if ( !this._tutorial && smile && this.editorEnabled ) {
		this.editor.focus();
		this.editor.value += ':' + smile + ':';

	}

}


CarnageChat.prototype.preProcessSmiles = function( text ) {
	if ( this.chatOptions.smilesOff == 1 ) {
		return text;
	}

	var re = new RegExp( /(:([0-9a-z�-�]+):)/ );

	while ( re.test( text ) ) {
		var smile = RegExp.$1;
		var smileName = RegExp.$2;
		var replaceText = '';

		if ( smileyCodes[ smileName ] ) {
			replaceText = ' -|-' + smileyCodes[ smileName ] + '-|- ';
		} else {
			replaceText = '-|-' + smileName + ':';
		}

		text = text.replace( smile, replaceText );

	}

	text = text.replace( /\-\|\-/g, ':' );

	return text;

}


CarnageChat.prototype.processSmiles = function( text, maxSmiles ) {
	if ( this.chatOptions.smilesOff == 1 ) {
		return text;
	}

	var re = new RegExp( /(:(\w+):)/ );
	var smileCount = 0;

	while ( re.test( text ) ) {
		var smile = RegExp.$1;
		var smileName = RegExp.$2;
		var replaceText = '';

		if ( smileCount < maxSmiles ) {
			if ( ! smilies[ smileName ] ) {
				replaceText = '&#058;' + smileName + '&#058;';
			} else {
				smileCount++;
				replaceText = '<img border="0" title="&#058;' + smileName + '&#058;" alt="&#058;' + smileName + '&#058;" src="http://img.blutbad.ru/i/sm/' + smileName + '.gif" style="cursor:pointer;" width="' + smilies[ smileName ].width + '" height="' + smilies[ smileName ].height + '" onclick="oChat.addSmile( \'' + smileName + '\' )"/>';

			}

		}

		text = text.replace( smile, replaceText );

	}

	return text;

}


CarnageChat.prototype.processNames = function( text ) {
	var re = new RegExp( /\{\[(.*?)\]\}/ );

	while ( re.test( text ) ) {
		var name = RegExp.$1;
		var attrName = name;
		var realName = name;
		var hiddenName = HIDDEN_REALNAME;
		var isHidden = this.isNameHidden( name );
		var invRe = new RegExp( /^hidden(b?)(\|?)(.*)$/ );

		if ( invRe.test( name ) ) {
			attrName = HIDDEN_BASENAME;
			isBold = RegExp.$1;

			if ( RegExp.$3 ) {
				hiddenName = RegExp.$3;
			}

			if ( isBold == 'b' ) {
				hiddenName = '<b>' + hiddenName + '</b>';
			}

			hiddenName = '<i>' + hiddenName + '</i>';
			isHidden = true;

		}

		var replaceText = '<span class="user-from-room" name="' + attrName + '">' + ( isHidden ? hiddenName : realName ) + '</span>';
		text = text.replace( '{[' + name + ']}', replaceText );

	}

	return text;

}


CarnageChat.prototype.translate = function( text ) {
	if ( ! text ) {
		return '';
	}

	text = text.replace( /[\/\.\!\,]to\s+([^:]*?)\s*:\s*/g, 'to [$1] ' );
	var tos = {};
	var count = 0;

	var regTo = new RegExp( /(to\s+\[[^\[\]]+\])/ );
	while ( regTo.test( text ) ) {
		count++;
		var key = '{{{' + count + '}}}';
		var capture = RegExp.$1;
		tos[ key ] = capture;
		text = text.replace( capture, key );
	}

	var regURL = new RegExp( /((?:http|https|ftp):\/\/(?:[a-z0-9_\.\-\@]+?\.)*(?:[a-z0-9]+?)\.[a-z]+\/\S*)/ig );
	while ( regURL.test(text) ) {
		count++;
		var key = '{{{' + count + '}}}';
		var capture = RegExp.$1;
		tos[ key ] = capture;
		text = text.replace( capture, key );
	}

	var regEMail = new RegExp( /([a-zA-Z0-9\.\-\_]+?\@[a-zA-Z0-9\.\-\_]+)/ig );
	while ( regEMail.test(text) ) {
		count++;
		var key = '{{{' + count + '}}}';
		var capture = RegExp.$1;
		tos[ key ] = capture;
		text = text.replace( capture, key );
	}

	text = transliterate( text );

	for ( var key in tos ) {
		text = text.replace( key, tos[ key ] );
	}

	return text;

}


CarnageChat.prototype.playSound = function( type ) {
	var oSound = this._MainDocument[ SND_CONTAINER ] ? this._MainDocument[ SND_CONTAINER ] : this._MainWindow[ SND_CONTAINER ];

	try {
		oSound.SetVariable( 'Volume', this.chatOptions.sound * 50 );
		oSound.SetVariable( 'php', type );

	} catch( e ) {
		this.onError( e.message );
	}

}


CarnageChat.prototype.setTextStyle = function( text, style ) {
	if ( style.length > 0 ) {
		text = '<span style="' + style + ';">' + text + '</span>';
	}

	return text;

}


CarnageChat.prototype.checkConnection = function() {
	if ( ! this.connected() ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_FAILED_TO_CONNECT );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_DISCONNECT );

		if ( this._needConnect() ) {
			this.connect();
		} else {
			setTimeout( function() { oChat.checkConnection(); }, 60000 );
		}

	}

}


CarnageChat.prototype.refreshOnlineList = function() {
	this.getUsersInRoom();
}


CarnageChat.prototype.setAutorefreshOnlineList = function( refresh ) {
	if (this._tutorial) return;

	if ( refresh ) {
		this._autorefreshOnlineList = setInterval( function() {
				oChat.refreshOnlineList();
			}, AUTOREFRESH_INTERVAL

		);

	} else {
		clearInterval( this._autorefreshOnlineList );
	}

	top.frames[ 'online' ].setAutorefreshState( refresh );

}


CarnageChat.prototype.generateUserId = function() {
	var id = this.parent.generateUserId.call( this );

	return this.htmlStuff.onlinePrefix + id;

}


CarnageChat.prototype.processResize = function() {
	if ( this._channels[ MAIN_ID ] && this._channels[ MAIN_ID ].htmlContainer ) {
		var height = jQuery( window ).height() - jQuery( '#channel-tabs' ).outerHeight();

		if ( height > 0 ) {
			for ( var channel in this._channels ) {
				jQuery( this._channels[ channel ].htmlContainer ).height( height - 3 );
			}

		}

	}

	var oDiv = this._tutorial ? null : this._OnlineDocument.getElementById( this.htmlStuff.onlineListContainer );

	if ( oDiv ) {
		var deltaY = _getPosY( oDiv );
		var deltaX = _getPosX( oDiv );
		var newHeight = this._OnlineDocumentHtml.clientHeight - deltaY;
		var newWidth = this._OnlineDocumentHtml.clientWidth - deltaX;
		
		oDiv.style.height = (newHeight < 0 ? 0 : newHeight) + 'px';
		oDiv.style.width = newWidth + 'px';

	}

}


CarnageChat.prototype.loadData = function( cmd, params, handler ) {
	this._requests.push( new CarnageRequest( cmd, params, this._onRequestStateChange.bind( this ).prependArg( handler.bind( this ) ) ) );
}


CarnageChat.prototype.loadDataUri = function( uri, handler ) {
	this._requests.push( new CarnageRequestUri( uri, this._onRequestStateChange.bind( this ).prependArg( handler.bind( this ) ) ) );
}


CarnageChat.prototype.addButton = function( button ) {
	if (this._tutorial) return;

	var lastButtonId = '';
	var i = 0;

	while ( CHAT_BUTTONS[ i ] != button && i < CHAT_BUTTONS.length ) {
		if ( this._EditorDocument.getElementById( CHAT_BUTTONS[ i ] ) ) {
			lastButtonId = CHAT_BUTTONS[ i ];
		}

		i++;

	}

	if ( lastButtonId != '' ) {
		var oTd = this._EditorDocument.getElementById( lastButtonId );

		if ( oTd ) {
			var oNewTd = this._EditorDocument.createElement( 'TD' );
			oNewTd.id = button;
			var src = CHAT_BUTTON_PARAMS[ button ].image;

			if ( button == 'align' ) {
				src = 'ch_align' + this.chatParams.align + '.gif';
			}

			oNewTd.innerHTML = '<img id="' + button + '_img" src="http://img.blutbad.ru/i/button/new/' + src + '" width="' + CHAT_BUTTON_PARAMS[ button ].width + '" height="' + CHAT_BUTTON_PARAMS[ button ].height + '" alt="' + CHAT_BUTTON_PARAMS[ button ].title + '" title="' + CHAT_BUTTON_PARAMS[ button ].title + '" style="cursor:pointer" onclick="' + CHAT_BUTTON_PARAMS[ button ].onclick + '" oncontextmenu="' + CHAT_BUTTON_PARAMS[ button ].oncontextmenu + ' return false;">';
			oTd.parentNode.insertBefore( oNewTd, oTd.nextSibling );
			this.chatOptions.hiddenButtons[ button ] = 0;

		}

	}

}


CarnageChat.prototype.removeButton = function( button ) {
	if (this._tutorial) return;

	var oTd = this._EditorDocument.getElementById( button );

	if ( oTd ) {
		oTd.parentNode.removeChild( oTd );
		this.chatOptions.hiddenButtons[ button ] = 1;

	}

	oTd = null;

}


CarnageChat.prototype.checkEditor = function() {
	if (this._tutorial) return;

	this.editor.disabled = this._editorDisabledByChannel || this._editorDisabledByFilter;
	this.editorEnabled = ! this.editor.disabled;

}


CarnageChat.prototype.addChannel = function( id, name, outputName, isCustom ) {
	var oChannel = this.parent.addChannel.apply( this, [ id, name, outputName, isCustom ] );
	this.processResize();

	return oChannel;

}


CarnageChat.prototype.generateChannelId = function() {
	var id = this.parent.generateChannelId.call( this );

	return this.htmlStuff.tabPrefix + id;

}


CarnageChat.prototype.selectChannelByName = function( name ) {
	var flag = true;
	var i = 0;

	while ( flag ) {
		if ( this.commonChannelOrders[ i ] == name ) {
			flag = false;
		} else if ( i >= this.commonChannelOrders.length ) {
			i = 0;
			flag = false;

		} else {
			i++;
		}

	}

	jQuery( '#' + oChat.htmlStuff.channels ).tabs( 'select', i );

}


CarnageChat.prototype.getRealRoom = function( roomId ) {
	if ( ! roomId ) {
		return '';
	}

	var room = roomId.replace( /^\d+_/, '' );

	return room;

}


CarnageChat.prototype.getUserChannelName = function( channelId ) {
	if ( ! channelId ) {
		return '';
	}

	var name = channelId.replace( /^user#/, '' );

	return name;

}


CarnageChat.prototype.getChatUsername = function( userRealName ) {
	userRealName = '' + userRealName;
	var reg = new RegExp( /^[\&\;\:\|\#\_\s\-A-Za-z�����Ũ����������������������������������������������������������\d]+$/ );

	if ( ! reg.test( userRealName ) ) {
		return '';
	}

	var str = userRealName.replace( /\s/g, '_' );
	str = str.replace( /\&/g, '#amp' );
	str = str.toLowerCase();

	return str;

}


CarnageChat.prototype.getRealUsername = function( userName ) {
	if ( ! userName ) {
		return '';
	}

	var str = userName.replace( /_/g, ' ' );
	str = str.replace( /#amp/g, '&' );

	return str;

}


CarnageChat.prototype.getUserChannelStyle = function( oChannel ) {
	if ( this.chatParams.userChannelStyles[ oChannel.name ] ) {
		return this.chatParams.userChannelStyles[ oChannel.name ];
	} else {
		return '';
	}

}


CarnageChat.prototype.getUsersInRoom = function() {
	this.loadDataUri( '/online_list/' + this.roomId + '/?' + Math.random(), this._getUserInfo );
}


CarnageChat.prototype.getTimeDiffStr = function( diff ) {
	diff = parseInt( diff );
	var cooldown = '';
	var minutes = parseInt( diff / 60 );

	if ( minutes > 0 ) {
		cooldown = minutes + ' ���. ' + ( diff - 60 * minutes ) + ' ���.';
	} else {
		cooldown = diff + ' ���.';
	}

	return cooldown;

}


CarnageChat.prototype.getClanIdFromChannel = function( channelId ) {
	var reg = new RegExp( /^c(\d+)$/ );

	if ( reg.test( channelId ) ) {
		return RegExp.$1;
	} else {
		return 0;
	}

}


CarnageChat.prototype.getGuildIdFromChannel = function( channelId ) {
	var reg = new RegExp( /^g(\d+)$/ );

	if ( reg.test( channelId ) ) {
		return RegExp.$1;
	} else {
		return 0;
	}

}


CarnageChat.prototype.getBattleIdFromChannel = function( channelId ) {
	var reg = new RegExp( /^\d+\_l(\d+)$/ );

	if ( reg.test( channelId ) ) {
		return RegExp.$1;
	} else {
		return 0;
	}

}


CarnageChat.prototype.isClanChannel = function( channelId ) {
	if ( channelId.match( /^c\d+$/ ) ) {
		return true;
	} else {
		return false;
	}

}


CarnageChat.prototype.isGuildChannel = function( channelId ) {
	if ( channelId.match( /^g\d+$/ ) ) {
		return true;
	} else {
		return false;
	}

}


CarnageChat.prototype.isGroupChannel = function( channelId ) {
	if ( channelId.match( /^\d+\_b\d+\_\d$/ ) ) {
		return true;
	} else {
		return false;
	}

}


CarnageChat.prototype.isPokerChannel = function( channelId ) {
	if ( channelId.match( /^\d+\_p\d+$/ ) ) {
		return true;
	} else {
		return false;
	}

}


CarnageChat.prototype.isBattleLogChannel = function( channelId ) {
	if ( channelId.match( /^\d+\_l\d+$/ ) ) {
		return true;
	} else {
		return false;
	}

}


CarnageChat.prototype.isUserChannel = function( channelId ) {
	if ( channelId.match( /^user#/ ) ) {
		return true;
	} else {
		return false;
	}

}


CarnageChat.prototype.isRoom = function( roomId ) {
	if ( roomId.match( /^\d+\_\d+$/ ) ) {
		return true;
	} else {
		return false;
	}

}


CarnageChat.prototype.isUserIgnored = function( realName ) {
	if ( this._ignored[ realName.toLowerCase() ] ) {
		return true;
	} else {
		return false;
	}

}


CarnageChat.prototype.isNameHidden = function( name ) {
	var reg = new RegExp( '^' + HIDDEN_BASENAME );

	return reg.test( name );

}


CarnageChat.prototype.isNameAdmin = function( name ) {
	var reg = new RegExp( '^' + ADMIN_BASENAME );

	return reg.test( name );

}


CarnageChat.prototype.isInJail = function() {
	var room = this.getRealRoom( this.roomId );

	return ( room == 151 || room == 152 );

}


// ���������� true, ���� oUser1 "������" oUser2, ����� false
CarnageChat.prototype.compareUsers = function( oUser1, oUser2 ) {
	var result = true;

	if ( ! oUser2 || ! oUser2.userInfo ) {
		return false;
	}

	if ( oUser1.userInfo[ 'speaking' ] > oUser2.userInfo[ 'speaking' ] ) {
		result = true;
	} else if ( oUser1.userInfo[ 'speaking' ] < oUser2.userInfo[ 'speaking' ] ) {
		result = false;
	} else {
		if ( oUser1.userInfo[ 'isBot' ] > oUser2.userInfo[ 'isBot' ] ) {
			result = true;
		} else if ( oUser1.userInfo[ 'isBot' ] < oUser2.userInfo[ 'isBot' ] ) {
			result = false;
		} else {
			result = ( oUser1.realName < oUser2.realName );
		}

	}

	return result;

}


CarnageChat.prototype.send = function( stanza ) {
	if ( ! this.connected() ) {
		return;
	}

	this._ChatClient.send( stanza );

}


CarnageChat.prototype.throwThing = function( userRealName, type ) {
	var phrase = ''

	if ( type == 'cake' ) {
		phrase = ' ������� � ������� ����';
	} else if ( type == 'snowball' ) {
		phrase = ' ��������� ������';
	}

	if ( this.currentChannelName != CH_MAIN ) {
		this.selectChannelByName( CH_MAIN );
	}

	this.sendRoomMessage( '������� � ' + userRealName + phrase );

}


CarnageChat.prototype.getCurrentTime = function() {
	return this.getTime();
}


CarnageChat.prototype.getTime = function( oDate ) {
	if ( ! oDate ) {
		oDate = new Date();
	}

	var hours = oDate.getHours();
	hours = ( hours < 10 ? '0' + hours : hours );
	var minutes = oDate.getMinutes();
	minutes = ( minutes < 10 ? '0' + minutes : minutes );
	var seconds = oDate.getSeconds();
	seconds = ( seconds < 10 ? '0' + seconds : seconds );
	var time = hours + ':' + minutes + ':' + seconds;

	return time;

}


CarnageChat.prototype.takeUserChannelName = function( name ) {
	if ( typeof( name ) != 'undefined' && name != '' ) {
		name = ( '' + name ).toLowerCase();
	} else {
		var oChannel = this.getChannelByName( this.currentChannelName );

		if ( oChannel && this.isUserChannel( oChannel.id ) ) {
			name = this.currentChannelName;
		}

	}

	return name;

}


CarnageChat.prototype.setRoomName = function( name ) {
	if (this._tutorial) return;

	this.onlineRoomName.innerHTML = name;
	this.onlineRoomName.title = name;

}


CarnageChat.prototype.debugMessage = function( message, level ) {
	message = '' + message;

	if ( ! level || level < 1 ) {
		level = 1;
	}

	if ( this.chatParams.debug && this.chatParams.debug != 0 && level >= this.chatParams.debug ) {
		var time = this.getCurrentTime();
		message = message.replace( /&/g, '&amp;' );
		message = message.replace( />/g, '&gt;' );
		message = message.replace( /</g, '&lt;' );
		message = message.replace( /"/g, '&quot;' );
		message = message.replace( /'/g, '&#39;' );
		this.mainChannel.appendText( '<span class="debug">' + time + ' ' + message + '</span>' );

	}

}


CarnageChat.prototype.hidePopup = function( oWindow ) {
	jQuery( oWindow.document ).trigger( 'click.jeegoocontext' );
	
	if(oWindow.zeroClip) {
		oWindow.jQuery(oWindow.zeroClip.div).css({
			left: '-100px',
			top: '-100px'
		});
	}
}


CarnageChat.prototype.reloadOptionsFrame = function() {
	if ( top.frames[ 'main' ] && top.frames[ 'main' ].userChannels ) {
		top.frames[ 'main' ].location.reload();
	}

}


CarnageChat.prototype.onEnter = function() {
	this.mainChannel.appendText( '<span class="chat-message">����� ���������� � ����� ' + this.chatParams.cityName + ' ���� &laquo;Blutbad&raquo;!</span>' );

	var oPres = $pres();
	this.send( oPres.tree() );
	var oPres = $pres( { to : this.roomId + '@' + XMPP_MUC + '.' + this._domain + '/' + this.userName } );
	this.send( oPres.tree() );

	if ( this._reconnecting ) {
		this._reconnecting = false;

		if ( this.channelExists( CH_TRADE ) ) {
			this.getChannelByName( CH_TRADE ).sendPresence();
		}

		if ( this.channelExists( CH_SYSTEM ) ) {
			this.getChannelByName( CH_SYSTEM ).sendPresence();
		}

	}

	this.joinUserChannels();

	var login = 0;

	if ( top.getCookie( 'login' ) == 1 ) {
		login = 1;
	}

	this.loadData( 'user.onenter', { 'login' : login }, this._getSendOnEnterResult );

	var date = new Date();
	date.setDate( date.getDate() + 365 );
	top.setCookie( 'login', 0, date.toGMTString(), '/', top.getSubDomains() );

	this.setFilter();
	this.setFilterLog();
	this.setTrans();
	this.setSound();
	this.checkEditor();

}


// ������� �� XMPP �������
CarnageChat.prototype.onAuthenticate = function() {
	this.onEnter();
}


CarnageChat.prototype.onConnect = function() {
}


CarnageChat.prototype.onDisconnect = function() {
	if ( ! this._disconnectType ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_DISCONNECT );
		this.checkEditor();
		this._reconnecting = true;
		this.connect();

	}

}


CarnageChat.prototype.onError = function( error ) {
	this.debugMessage( 'Error: ' + error, 100 );
}


CarnageChat.prototype.onMessage = function( message ) {
	this.debugMessage( Strophe.serialize( message ), 2 );

	var elems = message.getElementsByTagName( 'body' );

	if ( ! elems || elems.length == 0 ) {
		return;
	}

	var type = message.getAttribute( 'type' );
	var jid = message.getAttribute( 'from' );
	var body = elems[ 0 ];
	var messageId = body.getAttribute( 'mid' );
	var style = body.getAttribute( 'style' );
	var decode = 1;

	if ( body.getAttribute( 'decode' ) ) {
		decode = parseInt( body.getAttribute( 'decode' ) );
	}

	if ( ! style ) {
		style = '';
	}

	var time = body.getAttribute( 'time' ) || '';
	var tutorial = body.getAttribute( 'tutorial' ) ? parseInt( body.getAttribute( 'tutorial' ) ) : 0;

	if ( type == XMPP_MT_PRIVATE ) {
		var from = Strophe.getNodeFromJid( jid );

		if ( this.isNameAdmin( from ) ) {
			var oCommand = this._parseCommand( Strophe.getText( body ) );

			if ( oCommand[ 'name' ] == CMD_MESSAGE ) {
				if (this._tutorial && !tutorial) return;
				if (!this._tutorial && tutorial) return;

				if ( this.chatOptions.filter != FT_OFF ) {
					var special = 0;

					if ( body.getAttribute( 'special' ) ) {
						special = body.getAttribute( 'special' );
					}

					if ( body.getAttribute( 'self' ) ) {
						from = this.userName;

						if ( this._lastPrivateTime == time && this._lastPrivateMessage == oCommand[ 'source_message' ] ) {
							return;
						} else {
							this._lastPrivateTime = time;
							this._lastPrivateMessage = oCommand[ 'source_message' ];

						}

					} else if ( oCommand[ 'from' ] ) {
						from = oCommand[ 'from' ];
					}

					var messageType = body.getAttribute( 'telepathy' ) ? MT_TELEPATHY : oCommand[ 'type' ];
					var fromCity = body.getAttribute( 'from_city' );
					messageId = ( fromCity && messageId > 0 ) ? messageId + '.' + fromCity : messageId;
					this.mainChannel.appendMessage( messageId, messageType, time, from, this.userRealName, _filterMessage( oCommand[ 'message' ], decode, special ), style, special );

				}

			} else {
				var xs = message.getElementsByTagName( 'x' );

				if ( ! xs || xs.length == 0 ) { // ���������� ������� �� �����������
					this.processCommand( oCommand );
				}

			}

		} else if ( this.chatOptions.filter != FT_OFF ) {
			if (this._tutorial) return;
		
			if ( from && from.length > 0 ) {
				var messageType = body.getAttribute( 'telepathy' ) ? MT_TELEPATHY : MT_PRIVATE;
				var fromCity = body.getAttribute( 'from_city' );
				messageId = ( fromCity && messageId > 0 ) ? messageId + '.' + fromCity : messageId;
				this.mainChannel.appendMessage( messageId, messageType, time, from, this.userRealName, _filterMessage( Strophe.getText( body ), decode ), style );

			} else if ( jid == this._domain ) {
				var special = 0;
				var messageType = MT_SYS_ATTENTION;

				if ( body.getAttribute( 'subtype' ) ) {
					messageType = body.getAttribute( 'subtype' );
				}

				if ( body.getAttribute( 'special' ) ) {
					special = body.getAttribute( 'special' );
				}

				var fromCity = body.getAttribute( 'from_city' );
				messageId = ( fromCity && messageId > 0 ) ? messageId + '.' + fromCity : messageId;
				this.mainChannel.appendMessage( messageId, messageType, time, ADMIN_BASENAME, '', _filterMessage( Strophe.getText( body ), decode, special ), style, special );

			}

		}

	} else if ( type == XMPP_MT_ROOM ) {
		if (this._tutorial) return;
	
		var from = Strophe.getResourceFromJid( jid );

		if ( ! from ) {
			from = ADMIN_BASENAME;
		}

		if ( this.isNameAdmin( from ) ) {
			var oCommand = this._parseCommand( Strophe.getText( body ) );

			if ( oCommand[ 'name' ] == CMD_MESSAGE ) {
				if ( this.chatOptions.filter != FT_OFF && ( this.channelExists( oCommand[ 'room' ] ) || ( this.isRoom( oCommand[ 'room' ] ) && oCommand[ 'room' ] == this.roomId ) ) ) {
					var special = 0;

					if ( body.getAttribute( 'special' ) ) {
						special = body.getAttribute( 'special' );
					}

					var oChannel = null;

					if ( this.isRoom( oCommand[ 'room' ] ) ) {
						oChannel = this.mainChannel;
					} else {
						oChannel = this.getChannelById( oCommand[ 'room' ] );
					}

					if ( oChannel ) {
						var type = oCommand[ 'subtype' ] ? oCommand[ 'subtype' ] : oCommand[ 'type' ];
						var messageType = this._defineMessageType( oCommand[ 'room' ], type );
						var fromCity = body.getAttribute( 'from_city' );
						messageId = ( fromCity && messageId > 0 ) ? messageId + '.' + fromCity : messageId;
						oChannel.appendMessage( messageId, messageType, time, ADMIN_BASENAME, '', _filterMessage( oCommand[ 'message' ], decode, special ), style, special );

					}

				}

			} else {
				var xs = message.getElementsByTagName( 'x' );

				if ( ! xs || xs.length == 0 ) { // ���������� ������� �� �����������
					this.processCommand( oCommand );
				}

			}

		} else if ( this.chatOptions.filter != FT_OFF ) {
			var channelId = Strophe.getNodeFromJid( jid );

			if ( this.chatOptions.filter != FT_PRIVATE || from == this.userName || ! this.isRoom( channelId ) ) {
				var oChannel = null;

				if ( this.isRoom( channelId ) ) {
					oChannel = this.mainChannel;
				} else {
					oChannel = this.getChannelById( channelId );
				}

				if ( oChannel ) {
					var messageType = this._defineMessageType( channelId );
					var fromCity = body.getAttribute( 'from_city' );
					messageId = ( fromCity && messageId > 0 ) ? messageId + '.' + fromCity : messageId;
					oChannel.appendMessage( messageId, messageType, time, from, '', _filterMessage( Strophe.getText( body ), decode ), style );

				}

			}

		}

	} else if ( type == XMPP_MT_ERROR ) {
		if ( this.chatOptions.filter != FT_OFF ) {
			var from = Strophe.getNodeFromJid( jid );
			var elems = message.getElementsByTagName( 'error' );

			if ( elems.length == 0 ) {
				return;
			}

			var error = elems[ 0 ];
			var code = error.getAttribute( 'code' );

			if ( code == ECODE_NOT_FOUND || code == ECODE_IGNORED || code == ECODE_BLIND || code == ECODE_SILENCE ) {
				from = this.getRealUsername( from );
				var regPrivate = new RegExp( /private\s+\[(.+?)\]\s?(.*)$/ );

				if ( regPrivate.test( Strophe.getText( body ) ) ) {
					var userNames = RegExp.$1;
					var userName = _extractElement( userNames, from );

					if ( userName ) {
						var errorMessage = '';

						if ( code == ECODE_NOT_FOUND ) {
							errorMessage = '�������� "' + userName + '" �� ������';
						} else if ( code == ECODE_IGNORED ) {
							errorMessage = '��������� �� ���������� ��������� "' + userName + '". �� ��� ����������';
						} else if ( code == ECODE_BLIND ) {
							errorMessage = '�������� "' + userName + '" �� ����� ���������, �� ���� �������� �������� �������';
						} else if ( code == ECODE_SILENCE ) {
							errorMessage = '�������� "' + userName + '" �� ������ ��� ������ ��������, �� ���� �������� �������� ��������';
						}

						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, time, '', '', errorMessage );

					}

				}

			} else if ( code == ECODE_NOT_FOUND_C ) {
				if ( this.isUserChannel( from ) ) {
					this.selectChannelByName( CH_MAIN );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_FOUND + ': <b>' + this.getUserChannelName( from ) + '</b>' );

				}

			} else if ( code == ECODE_NOT_ACCEPTABLE ) {
				if ( this.isUserChannel( from ) ) {
					this.selectChannelByName( CH_MAIN );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_ACCEPTABLE + '<b>' + this.getUserChannelName( from ) + '</b>' );

				}

			}

		}

	}

}


CarnageChat.prototype.onPresence = function( presence ) {
	this.debugMessage( Strophe.serialize( presence ), 1 );

	var jid = presence.getAttribute( 'from' );
	var userName = Strophe.getResourceFromJid( jid );

	if ( userName ) {
		userName = userName.toLowerCase();
	} else {
		userName = '';
	}

	var type = presence.getAttribute( 'type' );
	var node = Strophe.getNodeFromJid( jid );

	if ( this.isClanChannel( node ) && userName == this.userName ) {
		var statuses = presence.getElementsByTagName( 'status' );

		if ( type == 'unavailable' && ( ! statuses || statuses.length == 0 ) ) {
			var xs = presence.getElementsByTagName( 'x' );

			if ( xs && xs.length > 0 ) {
				var x = xs[ 0 ];
				var destroy = x.getElementsByTagName( 'destroy' );

				if ( destroy && destroy.length > 0 && this.channelExists( node ) ) {
					this.removeChannelById( node, true );
					this.chatParams.clan = 0;

				}

			}

		} else if ( ! type ) {
			this.chatParams.clan = this.getClanIdFromChannel( node );
		}

		this._checkChannelList();

	} else if ( this.isGuildChannel( node ) && userName == this.userName ) {
		var statuses = presence.getElementsByTagName( 'status' );

		if ( type == 'unavailable' && ( ! statuses || statuses.length == 0 ) ) {
			var xs = presence.getElementsByTagName( 'x' );

			if ( xs && xs.length > 0 ) {
				var x = xs[ 0 ];
				var destroy = x.getElementsByTagName( 'destroy' );

				if ( destroy && destroy.length > 0 && this.channelExists( node ) ) {
					this.removeChannelById( node, true );
					this.chatParams.guild = 0;

				}

			}

		} else if ( ! type ) {
			this.chatParams.guild = this.getGuildIdFromChannel( node );
		}

		this._checkChannelList();

	} else if ( this.isGroupChannel( node ) && userName == this.userName ) {
		var statuses = presence.getElementsByTagName( 'status' );

		if ( type == 'unavailable' && ( ! statuses || statuses.length == 0 ) ) {
			var xs = presence.getElementsByTagName( 'x' );

			if ( xs && xs.length > 0 ) {
				var x = xs[ 0 ];
				var destroys = x.getElementsByTagName( 'destroy' );

				if ( destroys && destroys.length > 0 && this.channelExists( node ) ) {
					this.removeChannelById( node, true );
					this.chatParams.groupId = '';

				}

			}

		} else if ( ! type ) {
			this.chatParams.groupId = node;
		}

		this._checkChannelList();

	} else if ( this.isPokerChannel( node ) && userName == this.userName ) {
		var statuses = presence.getElementsByTagName( 'status' );

		if ( type == 'unavailable' && ( ! statuses || statuses.length == 0 ) ) {
			var xs = presence.getElementsByTagName( 'x' );

			if ( xs && xs.length > 0 ) {
				var x = xs[ 0 ];
				var destroys = x.getElementsByTagName( 'destroy' );

				if ( destroys && destroys.length > 0 && this.channelExists( node ) ) {
					this.removeChannelById( node, true );
					this.chatParams.pokerId = '';

				}

			}

		} else if ( ! type ) {
			this.chatParams.pokerId = node;
		}

		this._checkChannelList();

	} else if ( this.isBattleLogChannel( node ) && userName == this.userName ) {
		var statuses = presence.getElementsByTagName( 'status' );

		if ( type == 'unavailable' && ( ! statuses || statuses.length == 0 ) ) {
			var xs = presence.getElementsByTagName( 'x' );

			if ( xs && xs.length > 0 ) {
				var x = xs[ 0 ];
				var destroys = x.getElementsByTagName( 'destroy' );

				if ( destroys && destroys.length > 0 && this.channelExists( node ) ) {
					this.removeChannelById( node, true );

					if ( this.chatParams.battleLogId == node ) {
						this.chatParams.battleLogId = '';
					}

				}

			}

		} else if ( ! type ) {
			this.chatParams.battleLogId = node;
			var oChannel = this.getChannelById( this.chatParams.battleLogId );

			if ( oChannel ) {
				oChannel.appendText( '&nbsp;' );
				oChannel.appendText( '<span class="room-name">=== ��� ��� ' + this.getBattleIdFromChannel( this.chatParams.battleLogId ) + ' ===</span>' );
				this._lastOrderBattleLog = 0;

			}

		}

		this._checkChannelList();

	} else if ( this.isUserChannel( node ) ) {
		var statuses = presence.getElementsByTagName( 'status' );

		if ( type == 'error' && ( ! statuses || statuses.length == 0 ) ) {
			var error = presence.getElementsByTagName( 'error' );

			if ( error && error[ 0 ] ) {
				var code = error[ 0 ].getAttribute( 'code' );

				if ( code == ECODE_PASSWORD ) {
					if ( this.channelExists( node ) ) {
						this.removeChannelById( node, true );
						this._removePopupChannel( this.getUserChannelName( node ) );

					}

					this.selectChannelByName( CH_MAIN );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BAD_PASSWORD + '<b>' + this.getUserChannelName( node ) + '</b>' );

				} else if ( code == ECODE_FORBIDDEN_C || code == ECODE_NOT_FOUND_C ) {
					if ( this.channelExists( node ) ) {
						this.removeChannelById( node, true );
						this._removePopupChannel( this.getUserChannelName( node ) );

					}

					this.selectChannelByName( CH_MAIN );
					var text = error[ 0 ].getElementsByTagName( 'text' );

					if ( text && text[ 0 ] && Strophe.getText( text[ 0 ] ) == ETEXT_BANNED ) {
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_BANNED + ' <b>' + this.getUserChannelName( node ) + '</b>' );
					} else {
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_FOUND + ': <b>' + this.getUserChannelName( node ) + '</b>' );
					}

				} else if ( code == ECODE_LIMIT_REACHED ) {
					if ( this.channelExists( node ) ) {
						this.removeChannelById( node, true );
						this._removePopupChannel( this.getUserChannelName( node ) );

					}

					this.selectChannelByName( CH_MAIN );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CONNEC_LIMIT_REACHED );

				}

			}

		} else if ( type == 'unavailable' && ( ! statuses || statuses.length == 0 ) ) {
			var xs = presence.getElementsByTagName( 'x' );

			if ( xs && xs.length > 0 ) {
				var x = xs[ 0 ];
				var destroys = x.getElementsByTagName( 'destroy' );

				if ( destroys && destroys.length > 0 && this.channelExists( node ) ) {
					var name = this.getUserChannelName( node );

					if ( this.currentChannelName == name ) {
						this.selectChannelByName( CH_MAIN );
					}

					this.removeChannelById( node, true );
					this._removePopupChannel( name );
					delete( this._userChannels[ name ] );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '����� <b>' + name + '</b> ��� ������' );
					this.reloadOptionsFrame();

				} else if ( this.channelExists( node ) ) {
					if ( userName == this.userName ) {
						var name = this.getUserChannelName( node );
						this.removeChannelById( node, true );
						this._removePopupChannel( name );
						delete( this._userChannels[ name ] );
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ����������� �� ������ <b>' + name + '</b>' );
						this.reloadOptionsFrame();

					} else {
						var name = this.getUserChannelName( node );

						if ( this._userChannels[ name ] ) {
							var oChannel = this.getChannelByName( name );
							var realName = presence.getAttribute( 'user' ) ? presence.getAttribute( 'user' ) : this.getRealUsername( userName );

							if ( oChannel && this.chatOptions.presenceoff == 0 ) {
								var channelName = '';

								if ( ! oChannel.selfOutput ) {
									channelName = ' <b>' + name + '</b>';
								}

								oChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�������� {[' + realName + ']} ���������� �� ������' + channelName );

							}

							delete( this._userChannels[ name ][ realName ] );

						}

					}

				}

			}

		} else if ( type == 'unavailable' && statuses && statuses.length > 0 ) {
			if ( this.channelExists( node ) ) {
				if ( userName == this.userName && statuses[ 0 ].getAttribute( 'code' ) == CODE_KICKED ) {
					var name = this.getUserChannelName( node );
					this.removeChannelById( node, true );
					this._removePopupChannel( name );
					this.selectChannelByName( CH_MAIN );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '��� ��������� �� ������ <b>' + name + '</b>' );
					this.reloadOptionsFrame();

				} else if ( userName == this.userName && statuses[ 0 ].getAttribute( 'code' ) == CODE_BANNED ) {
					var name = this.getUserChannelName( node );
					this.removeChannelById( node, true );
					this._removePopupChannel( name );
					this.selectChannelByName( CH_MAIN );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '��� �������� �� ������ <b>' + name + '</b>' );
					this.reloadOptionsFrame();

				} else if ( userName != this.userName ) {
					var name = this.getUserChannelName( node );

					if ( this._userChannels[ name ] ) {
						var oChannel = this.getChannelByName( name );
						var realName = presence.getAttribute( 'user' ) ? presence.getAttribute( 'user' ) : this.getRealUsername( userName );

						if ( oChannel && this.chatOptions.presenceoff == 0 ) {
							var channelName = '';

							if ( ! oChannel.selfOutput ) {
								channelName = ' <b>' + name + '</b>';
							}

							oChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�������� {[' + realName + ']} ���������� �� ������' + channelName );

						}

						delete( this._userChannels[ name ][ realName ] );

					}

				}

			}

		} else if ( ! type ) {
			if ( userName == this.userName ) {
				if ( ! this.channelExists( node ) ) {
					var name = this.getUserChannelName( node );
					var output = presence.getAttribute( 'output' );

					if ( output == null || output.length == 0 ) {
						output = name;
					}

					this.addChannel( node, name, output, 1 );
					this._addPopupChannel( name );
					this.selectChannelByName( CH_MAIN );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ������������ � ������ <b>' + name + '</b>' );
					this.reloadOptionsFrame();

				} else if ( presence.getAttribute( 'self' ) ) {
					this.selectChannelByName( CH_MAIN );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ��� ���������� � ������ <b>' + this.getUserChannelName( node ) + '</b>' );

				}

			} else {
				var name = this.getUserChannelName( node );
				var realName = presence.getAttribute( 'user' ) ? presence.getAttribute( 'user' ) : this.getRealUsername( userName );

				if ( ! this._userChannels[ name ] ) {
					this._userChannels[ name ] = {};
				}

				var oChannel = this.getChannelByName( name );

				if ( oChannel && this.chatOptions.presenceoff == 0 ) {
					var channelName = '';

					if ( ! oChannel.selfOutput ) {
						channelName = ' <b>' + name + '</b>';
					}

					oChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�������� {[' + realName + ']} ����������� � ������' + channelName );

				}

				this._userChannels[ name ][ realName ] = 1;

			}

		}

		this._checkChannelList();

	} else if ( this.isRoom( node ) && userName == this.userName ) {
		var statuses = presence.getElementsByTagName( 'status' );

		if ( ! type && ( ! statuses || statuses.length == 0 ) ) {
			this.mainChannel.appendText( '<span class="room-name">=== ' + this.roomName + ' ===</span>' );

			if ( this.channelExists( CH_SYSTEM ) ) {
				this.getChannelByName( CH_SYSTEM ).appendText( '<span class="room-name">=== ' + this.roomName + ' ===</span>' );
			}

		}

	}

}


CarnageChat.prototype.onQuery = function( query ) {
	this.debugMessage( Strophe.serialize( query ), 2 );

	var id = query.getAttribute( 'id' );
	var oQuery = this._findQueryById( id );

	if ( oQuery && oQuery.id ) {
		var type = query.getAttribute( 'type' );

		if ( type == 'result' ) {
			if ( oQuery.type == Q_DESTROY_CHANNEL ) {
				this.selectChannelByName( CH_MAIN );
				this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ������� ����� <b>' + oQuery.params.name + '</b>' );
				this.reloadOptionsFrame();

			} else if ( oQuery.type == Q_KICK_USER ) {
				this.selectChannelByName( CH_MAIN );
				this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ��������� ��������� <b>' + oQuery.params.user + '</b> �� ������ <b>' + oQuery.params.channel + '</b>' );

			} else if ( oQuery.type == Q_BAN_USER ) {
				this.selectChannelByName( CH_MAIN );
				this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� �������� ��������� <b>' + oQuery.params.user + '</b> �� ������ <b>' + oQuery.params.channel + '</b>' );

			} else if ( oQuery.type == Q_UNBAN_USER ) {
				this.selectChannelByName( CH_MAIN );
				this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ����� ��� � ��������� <b>' + oQuery.params.user + '</b> �� ������ <b>' + oQuery.params.channel + '</b>' );

			} else if ( oQuery.type == Q_GRANT_ADMIN ) {
				this.selectChannelByName( CH_MAIN );
				this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ������� ��������� <b>' + oQuery.params.user + '</b> ��������������� ������ <b>' + oQuery.params.channel + '</b>' );
				this.reloadOptionsFrame();

			} else if ( oQuery.type == Q_REVOKE_ADMIN ) {
				this.selectChannelByName( CH_MAIN );
				this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ������� � ��������� <b>' + oQuery.params.user + '</b> ����� �������������� ������ <b>' + oQuery.params.channel + '</b>' );
				this.reloadOptionsFrame();

			} else if ( oQuery.type == Q_USER_CHANNEL ) {
				var identifier = oQuery.getBaseId();

				if ( identifier == ID_OWNER_CHANNEL_LIST ) {
					this.selectChannelByName( CH_MAIN );
					var items = query.getElementsByTagName( 'item' );

					if ( items && items.length > 0 ) {
						var channels = _getChannelList( items );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '���� ������� ������� ������: ' + channels );

					} else {
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_OWNER_CHANNELS );
					}

				} else if ( identifier == ID_CHANNEL_LIST ) {
					this.selectChannelByName( CH_MAIN );
					var items = query.getElementsByTagName( 'item' );

					if ( items && items.length > 0 ) {
						var channels = _getChannelList( items );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ���������� � ������� �������: ' + channels );

					} else {
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CONNECTED_CHANNELS );
					}

				} else if ( identifier == ID_CHANNELS_ADMIN_LIST ) {
					this.selectChannelByName( CH_MAIN );
					var items = query.getElementsByTagName( 'item' );

					if ( items && items.length > 0 ) {
						var channels = _getChannelList( items );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ��������� ��������������� ������� �������: ' + channels );

					} else {
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNELS_ADMIN );
					}

				} else if ( identifier == ID_ADMIN_CHANNEL_LIST ) {
					this.selectChannelByName( CH_MAIN );
					var items = query.getElementsByTagName( 'item' );

					if ( items && items.length > 0 ) {
						var users = _getUserList( items );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�������������� ������ <b>' + oQuery.params.name + '</b>: ' + users );

					} else {
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '����� <b>' + oQuery.params.name + '</b> �� ����� ���������������' );
					}

				} else if ( identifier == ID_BANNED_CHANNEL_LIST ) {
					this.selectChannelByName( CH_MAIN );
					var items = query.getElementsByTagName( 'item' );

					if ( items && items.length > 0 ) {
						var users = _getUserList( items );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�������� �� ������ <b>' + oQuery.params.name + '</b>: ' + users );

					} else {
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '����� <b>' + oQuery.params.name + '</b> �� ����� ���������� �������' );
					}

				} else if ( identifier == ID_CHANNELS_BROADCAST_LIST ) {
					this.selectChannelByName( CH_MAIN );
					var items = query.getElementsByTagName( 'item' );

					if ( items && items.length > 0 ) {
						var channels = _getChannelList( items );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '���� ����������� ������� ������: ' + channels );

					} else {
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NO_CHANNELS_BROADCAST );
					}

				} else if ( identifier == ID_JOIN_CHANNELS ) {
					var items = query.getElementsByTagName( 'item' );

					if ( items ) {
						for ( var i = 0; i < items.length; i++ ) {
							this.joinUserChannel( items[ i ].getAttribute( 'name' ), items[ i ].getAttribute( 'password' ), items[ i ].getAttribute( 'output' ) );
						}

					}

				} else if ( identifier == ID_TAB_CHANNEL ) {
					var oChannel = this.getChannelByName( oQuery.params.name );

					if ( oChannel ) {
						oChannel.tab();
					}

					this.reloadOptionsFrame();

				} else if ( identifier == ID_UNTAB_CHANNEL ) {
					var oChannel = this.getChannelByName( oQuery.params.name );

					if ( oChannel ) {
						oChannel.untab();
					}

					this.reloadOptionsFrame();

				} else if ( identifier == ID_REMOVE_BROADCAST ) {
					this.selectChannelByName( CH_MAIN );
					this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ������� ������� ����� <b>' + oQuery.params.name + '</b>' );
					this.loadData( 'user.remove_broadcast_user_channel', { 'name' : _escapeEx( oQuery.params.name ) }, this._getRemoveBroadcastUserChannelResult );

				}

			}

		} else if ( type == 'error' ) {
			var error = query.getElementsByTagName( 'error' );

			if ( error && error[ 0 ] ) {
				if ( oQuery.type == Q_DESTROY_CHANNEL ) {
					var code = error[ 0 ].getAttribute( 'code' );

					if ( code == ECODE_FORBIDDEN_C || code == ECODE_FORBIDDEN_C2 ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_OPERATION_NOT_ALLOWED );

					} else if ( code == ECODE_NOT_FOUND_C ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_FOUND + ': <b>' + oQuery.params.name + '</b>' );

					}

				} else if ( oQuery.type == Q_KICK_USER ) {
					var code = error[ 0 ].getAttribute( 'code' );

					if ( code == ECODE_NOT_FOUND_C ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_FOUND + ': <b>' + oQuery.params.channel + '</b>' );

					} else if ( code == ECODE_FORBIDDEN_C || code == ECODE_FORBIDDEN_C2 ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_OPERATION_NOT_ALLOWED );

					} else if ( code == ECODE_NOT_ACCEPTABLE ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�������� <b>' + oQuery.params.user + '</b> �� ��������� � ������ <b>' + oQuery.params.channel + '</b>' );

					}

				} else if ( oQuery.type == Q_BAN_USER || oQuery.type == Q_UNBAN_USER ) {
					var code = error[ 0 ].getAttribute( 'code' );

					if ( code == ECODE_NOT_FOUND_C ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_FOUND + ': <b>' + oQuery.params.channel + '</b>' );

					} else if ( code == ECODE_NOT_ACCEPTABLE ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_USER_NOT_FOUND + ': <b>' + oQuery.params.user + '</b>' );

					} else if ( code == ECODE_FORBIDDEN_C || code == ECODE_FORBIDDEN_C2 ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_OPERATION_NOT_ALLOWED );

					} else if ( code == ECODE_SELF ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_SELF_ALLOWED );

					} else if ( code == ECODE_ADMIN ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_ADMIN_ALLOWED );

					}

				} else if ( oQuery.type == Q_GRANT_ADMIN ) {
					var code = error[ 0 ].getAttribute( 'code' );

					if ( code == ECODE_NOT_FOUND_C ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_FOUND + ': <b>' + oQuery.params.channel + '</b>' );

					} else if ( code == ECODE_NOT_ACCEPTABLE ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_USER_NOT_FOUND + ': <b>' + oQuery.params.user + '</b>' );

					} else if ( code == ECODE_FORBIDDEN_C || code == ECODE_FORBIDDEN_C2 ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_OPERATION_NOT_ALLOWED );

					} else if ( code == ECODE_LIMIT_REACHED ) {
						this.selectChannelByName( CH_MAIN );

						if ( Strophe.getText( error[ 0 ] ) == 'Limit reached' ) {
							this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_ADMIN_LIMIT_REACHED + '<b>' + oQuery.params.channel + '</b>' );
						} else if ( Strophe.getText( error[ 0 ] ) == 'Administration limit reached' ) {
							this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '������������� �� ����������. �������� <b>' + oQuery.params.user + '</b> �������� ��������������� ������������� ���������� �������' );
						}

					} else if ( code == ECODE_SELF_ADMIN ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_ALREADY_OWNER + '<b>' + oQuery.params.channel + '</b>' );

					}

				} else if ( oQuery.type == Q_REVOKE_ADMIN ) {
					var code = error[ 0 ].getAttribute( 'code' );

					if ( code == ECODE_NOT_FOUND_C ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_FOUND + ': <b>' + oQuery.params.channel + '</b>' );

					} else if ( code == ECODE_NOT_ACCEPTABLE ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�������� <b>' + oQuery.params.user + '</b> �� �������� ��������������� ������ <b>' + oQuery.params.channel + '</b>' );

					} else if ( code == ECODE_FORBIDDEN_C || code == ECODE_FORBIDDEN_C2 ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_OPERATION_NOT_ALLOWED );

					} else if ( code == ECODE_SELF_ADMIN ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_ALREADY_OWNER + '<b>' + oQuery.params.channel + '</b>' );

					}

				} else if ( oQuery.type == Q_USER_CHANNEL ) {
					var code = error[ 0 ].getAttribute( 'code' );
					var identifier = oQuery.getBaseId();

					if ( code == ECODE_NOT_FOUND_C ) {
						if ( identifier == ID_TAB_CHANNEL || identifier == ID_UNTAB_CHANNEL ) {
							this.selectChannelByName( CH_MAIN );
							this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CONN_CHANNEL_NOT_FOUND + ': <b>' + oQuery.params.name + '</b>' );

						} else if ( identifier == ID_ADMIN_CHANNEL_LIST ) {
							this.selectChannelByName( CH_MAIN );
							this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_OWNER + '<b>' + oQuery.params.name + '</b>' );

						} else if ( identifier == ID_BANNED_CHANNEL_LIST ) {
							this.selectChannelByName( CH_MAIN );
							this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_OWNER + '<b>' + oQuery.params.name + '</b>' );

						}

					} else if ( code == ECODE_FORBIDDEN_C || code == ECODE_FORBIDDEN_C2 ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_OPERATION_NOT_ALLOWED );

					}

				} else {
					var code = error[ 0 ].getAttribute( 'code' );

					if ( code == ECODE_FORBIDDEN_C || code == ECODE_FORBIDDEN_C2 ) {
						this.selectChannelByName( CH_MAIN );
						this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_OPERATION_NOT_ALLOWED );

					}

				}

			}

		}

		this._removeQueryById( id );

	}

}


// ���������� ������
CarnageChat.prototype._createUser = function( name ) {
	return new User( name, this );
}


CarnageChat.prototype._removeUser = function( oUser ) {
	var oLi = this._tutorial ? null : this._OnlineDocument.getElementById( oUser.userId );

	if ( oLi ) {
		this.onlineList.removeChild( oLi );
		this._roomMemberCount--;

		if ( this._roomMemberCount < 0 ) {
			this._roomMemberCount = 0;
		}

	}

}


CarnageChat.prototype._updateUser = function( oUser, userInfo ) {
	if (this._tutorial) return;

	oUser.userInfo = userInfo;
	oUser.id = oUser.userInfo[ 'id' ];
	oUser.name = oUser.userInfo[ 'username' ];
	oUser.name = oUser.name.toLowerCase();
	oUser.realName = oUser.userInfo[ 'realName' ];

	var oLi = this._OnlineDocument.getElementById( oUser.userId );

	if ( ! oLi ) {
		oLi = this._addUserToList( oUser );
	}

	if ( ! oLi ) {
		return;
	}

	var options = {};

	if ( oUser.userInfo[ 'isBot' ] == 0 ) {
		options.privateMess = true;
	}

	if ( oUser.userInfo[ 'dungeon_x' ] > 0 && oUser.userInfo[ 'dungeon_y' ] > 0 ) {
		options.dungeon = {};
		options.dungeon.x = oUser.userInfo[ 'dungeon_x' ];
		options.dungeon.y = oUser.userInfo[ 'dungeon_y' ];

		if ( oUser.userInfo[ 'hp' ] && oUser.userInfo[ 'maxHP' ] > 0 ) {
			options.dungeon.hp = oUser.userInfo[ 'hp' ];
			options.dungeon.maxhp = oUser.userInfo[ 'maxHP' ];

		}

	}

	if ( oUser.userInfo[ 'zside' ] > 0 ) {
		options.eventSide = oUser.userInfo[ 'zside' ];
	}

	if ( oUser.userInfo[ 'zimmune' ] > 0 ) {
		options.zimmune = oUser.userInfo[ 'zimmune' ];
	}

	if ( oUser.userInfo[ 'align' ] > 0 ) {
		options.align = { code : oUser.userInfo[ 'align' ] };
	}

	if ( oUser.userInfo[ 'clan' ] && oUser.userInfo[ 'clan' ].length > 0 ) {
		options.clan = { img : oUser.userInfo[ 'clan' ], name : oUser.userInfo[ 'clanTitle' ] };
	}

	if ( oUser.userInfo[ 'guild' ] && oUser.userInfo[ 'guild' ].length > 0 ) {
		options.guild = { img : oUser.userInfo[ 'guild' ], name : oUser.userInfo[ 'guildTitle' ] };
	}
	
	if ( oUser.userInfo[ 'topicon' ]) {
		options.topicon = 1;
	}

	if ( oUser.userInfo[ 'mentorId' ] > 0 ) {
		options.mentor = true;
	} else {
		options.mentor = false;
	}

	if ( this.chatParams.isMentor == 1 ) {
		options.isMentor = true;
	} else {
		options.isMentor = false;
	}

	options.name = oUser.userInfo[ 'realName' ];
	options.sex = oUser.userInfo[ 'sex' ];
	options.level = oUser.userInfo[ 'level' ];

	if ( oUser.userInfo[ 'battle' ] > 0 ) {
		options.battle = oUser.userInfo[ 'battle' ];
	}

	if ( oUser.userInfo[ 'isBot' ] > 0 ) {
		options.bot = {};

		if ( oUser.userInfo[ 'speaking' ] > 0 ) {
			options.bot.speak = true;
		}

		if ( oUser.userInfo[ 'count' ] > 0 ) {
			options.bot.count = oUser.userInfo[ 'count' ];
		}

	}

	if ( this.isUserIgnored( oUser.realName ) ) {
		options.ignore = true;
	}

	options.alcohol = oUser.userInfo[ 'alcohol' ];

	if ( oUser.userInfo[ 'silence' ] > 0 ) {
		options.silence = true;
	}

	if ( oUser.userInfo[ 'blind' ] > 0 ) {
		options.blind = true;
	}

	if ( oUser.userInfo[ 'hobble' ] > 0 ) {
		options.hobble = true;
	}

	if ( oUser.userInfo[ 'hurt' ] > 0 ) {
		options.hurt = true;
	}

	options.showEmpty = true;
	options.ownerDocument = this._OnlineDocument;
	jQuery( oLi ).empty().append( getNick( options ) );
	oLi.style.display = '';
	this.setRoomName( this.roomName + ' (' + this._roomMemberCount + ')' );

}


CarnageChat.prototype._addUserToList = function ( oUser ) {
	if (this._tutorial) return;

	var oLi = this._OnlineDocument.createElement( 'LI' );
	oLi.setAttribute( 'id', oUser.userId );
	oLi.setAttribute( 'name', oUser.name );
	oLi.innerHTML = oUser.name;
	oLi.style.display = 'none';

	this.onlineList.appendChild( oLi );
	this._roomMemberCount++;

	return oLi;

}


CarnageChat.prototype._addPopupChannel = function ( name ) {
	var oLi = this._MainDocument.createElement( 'LI' );
	oLi.className = 'simple';
	oLi.id = this.htmlStuff.channelPopupItemPrefix2 + 'user-' + name;
	var oA = this._MainDocument.createElement( 'A' );
	oA.setAttribute( 'href', '#user-' + name );
	oA.innerHTML = '����� ' + name;
	oLi.appendChild( oA );
	this.channelListPopup2.insertBefore( oLi, this.channelListPopup2.lastChild );
	var oChannelHelper = this.channelHelper;

	jQuery( oChannelHelper ).jeegoocontext( oChat.channelListPopup2, {
			event : 'click',
			heightOverflowOffset: 1,
			onShow : oChat._onChannelPopupShow.bind( oChat ),
			onSelect : oChat._onChannelPopupItemClick.bind( oChat ),
			x : -1,
			y : 10000,
			oWindow : oChat._MainWindow

		}

	);

}


CarnageChat.prototype._removePopupChannel = function ( name ) {
	var oLi = this._MainDocument.getElementById( this.htmlStuff.channelPopupItemPrefix2 + 'user-' + name );

	if ( oLi ) {
		oLi.parentNode.removeChild( oLi );
	}

}


CarnageChat.prototype._getUserInfo = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving user info' );

		return;

	}

	this.cleanUserList();
	var room = response.getElementsByTagName( 'room' );

	if ( room && room[ 0 ] ) {
		this.roomName = Strophe.getText( room[ 0 ] );
		this.setRoomName( this.roomName );

	}

	var data = response.getElementsByTagName( 'data' );

	for ( var i = 0; i < data.length; i++ ) {
		var params = Strophe.getText( data[ i ] ).split( ',' );

		var userName = params[ 0 ].toLowerCase();
		this.addUser( userName );
		var oUser = this.getUserByName( userName );

		if ( oUser ) {
			var userInfo = {};
			userInfo[ 'username' ] = params[ 0 ];
			userInfo[ 'id' ] = params[ 1 ];
			userInfo[ 'realName' ] = params[ 2 ];
			userInfo[ 'align' ] = params[ 3 ];
			userInfo[ 'sex' ] = params[ 4 ];
			userInfo[ 'mentorId' ] = params[ 5 ];
			userInfo[ 'clan' ] = params[ 6 ];
			userInfo[ 'clanTitle' ] = params[ 7 ];
			userInfo[ 'guild' ] = params[ 8 ];
			userInfo[ 'guildTitle' ] = params[ 9 ];
			userInfo[ 'silence' ] = params[ 10 ];
			userInfo[ 'hurt' ] = params[ 11 ];
			userInfo[ 'blind' ] = params[ 12 ];
			userInfo[ 'hobble' ] = params[ 13 ];
			userInfo[ 'battle' ] = params[ 14 ];
			userInfo[ 'alcohol' ] = params[ 15 ];
			userInfo[ 'zside' ] = params[ 16 ];
			userInfo[ 'dungeon' ] = params[ 17 ];
			userInfo[ 'hp' ] = params[ 18 ];
			userInfo[ 'maxHP' ] = params[ 19 ];
			userInfo[ 'dungeon_x' ] = params[ 20 ];
			userInfo[ 'dungeon_y' ] = params[ 21 ];
			userInfo[ 'isBot' ] = params[ 22 ];
			userInfo[ 'speaking' ] = params[ 23 ];
			userInfo[ 'level' ] = params[ 24 ];
			userInfo[ 'attack' ] = params[ 25 ];
			userInfo[ 'meddle' ] = params[ 26 ];
			userInfo[ 'count' ] = params[ 27 ];
			userInfo[ 'zimmune' ] = params[ 28 ];
			userInfo[ 'topicon' ] = params[ 29 ] == 1 ? 1 : 0;

			this._updateUser( oUser, userInfo );

		} else {
			this.removeUser( userName );
		}

	}

}


CarnageChat.prototype._getSendClanAllyResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving ally clan message send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result == E_NOT_ALLOWED ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_SEND_NOT_ALLOWED );
	} else if ( result == E_NOT_FOUND ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_ALLY_CLAN );
	}

}


CarnageChat.prototype._getSendBroadcastResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving broadcast message send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result == E_NOT_ALLOWED ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_SEND_NOT_ALLOWED );
	}

}


CarnageChat.prototype._getSendDebugStatusResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving debug status send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	this.chatParams.debug = Strophe.getText( data[ 0 ] );

	if ( this.chatParams.debug >= 0 ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', 'debug is ' + this.chatParams.debug );
	} else {
		this.chatParams.debug = 0;
	}

}


CarnageChat.prototype._getSendToBotResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving message to bot send result' );

		return;

	}

}


CarnageChat.prototype._getSendSpamReportResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving spam report result' );

		return;

	}

}


CarnageChat.prototype._getSendComplainResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving complain send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result == R_OK ) {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', MSG_COMPLAIN_REPORT );
	} else {
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', result );
	}

}


CarnageChat.prototype._getChangeFilterResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving change filter send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result != E_ERROR ) {
		this.chatOptions.filter = parseInt( result );
		this.setFilter();

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_UNABLE_FILTER );

	}

}


CarnageChat.prototype._getChangeFilterLogResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving change filter log send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result != E_ERROR ) {
		this.chatOptions.filterLog = parseInt( result );
		this.setFilterLog();

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_UNABLE_FILTER_LOG );

	}

}


CarnageChat.prototype._getSwitchTransResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving switch tranlit send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result != E_ERROR ) {
		this.chatOptions.trans = parseInt( result );

		if ( this.chatOptions.trans ) {
			top.ChatTrans = true;
		} else {
			top.ChatTrans = false;
		}

		this.setTrans();

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_UNABLE_TRANS );

	}

}


CarnageChat.prototype._getChangeSoundResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving change sound send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result != E_ERROR ) {
		this.chatOptions.sound = parseInt( result );
		this.setSound();

	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_UNABLE_SOUND );

	}

}


CarnageChat.prototype._getChangePresenceoffResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving change presenceoff send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result != E_ERROR ) {
		this.chatOptions.presenceoff = parseInt( result );
	} else {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_UNABLE_PRESENCEOFF );

	}

}


CarnageChat.prototype._getSendOnEnterResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving onenter message send result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var items = data[ 0 ].getElementsByTagName( 'item' );

	if ( items && items.length > 0 ) {
		for ( var i = 0; i < items.length; i++ ) {
			this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', _filterMessage( Strophe.getText( items[ i ] ), 1, 1 ) );
		}

	}

}


CarnageChat.prototype._getCreateChannelResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving create channel result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result == E_ERROR ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_UNABLE_CREATE_CHANNEL );

	} else if ( result == E_BAD_NAME ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

	} else if ( result == E_LEVEL_NOT_ALLOWED ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_LEVEL_NOT_ALLOWED );

	} else if ( result == E_RESERVED_NAME ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_RESERVED_CHANNEL_NAME );

	} else if ( result == E_BAD_PASSWORD ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_PASSWORD );

	} else if ( result == E_LIMIT_REACHED ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CREATE_LIMIT_REACHED );

	} else if ( result == E_ALREADY_EXISTS ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_ALREAY_EXISTS );

	} else if ( result == E_HAS_TIMEOUT ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CREATE_TIMEOUT );

	} else if ( result == R_OK ) {
		var name = response.getElementsByTagName( 'name' );
		name = Strophe.getText( name[ 0 ] );
		var password = response.getElementsByTagName( 'password' );
		password = Strophe.getText( password[ 0 ] );
		var output = response.getElementsByTagName( 'output' );
		output = Strophe.getText( output[ 0 ] );

		if ( output.length == 0 ) {
			output = name;
		}

		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ������� ����� <b>' + name + '</b>' );
		this.reloadOptionsFrame();
		this.joinChannel( BASE_USER_CHANNEL_NAME + name, name, output, password );
		this._addPopupChannel( name );

	}

}


CarnageChat.prototype._getSetPasswordChannelResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving create channel result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result == E_ERROR ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_UNABLE_SET_PASSWORD_CHANNEL );

	} else if ( result == E_BAD_NAME ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_NAME );

	} else if ( result == E_BAD_PASSWORD ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_WRONG_CHANNEL_PASSWORD );

	} else if ( result == E_NOT_FOUND ) {
		var name = response.getElementsByTagName( 'name' );
		name = Strophe.getText( name[ 0 ] );
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_FOUND + ': <b>' + name + '</b>' );

	} else if ( result == E_NOT_ALLOWED ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_OPERATION_NOT_ALLOWED );

	} else if ( result == R_OK ) {
		var name = response.getElementsByTagName( 'name' );
		name = Strophe.getText( name[ 0 ] );
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '������ ���������� ��� ������ <b>' + name + '</b>' );

	}

}


CarnageChat.prototype._getDestroyChannelResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving destroy channel result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result == E_ERROR ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_UNABLE_DESTROY_CHANNEL );

	} else if ( result == E_NOT_FOUND ) {
		var name = response.getElementsByTagName( 'name' );
		name = Strophe.getText( name[ 0 ] );

		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_CHANNEL_NOT_FOUND + ': <b>' + name + '</b>' );

	} else if ( result == E_NOT_ALLOWED ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_OPERATION_NOT_ALLOWED );

	} else if ( result == R_OK ) {
		var name = response.getElementsByTagName( 'name' );
		name = Strophe.getText( name[ 0 ] );

		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ������� ����� <b>' + name + '</b>' );

	}

}


CarnageChat.prototype._getDeleteUserChannelStyleResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving delete user channel style result' );

		return;

	}

	var data = response.getElementsByTagName( 'data' );
	var result = Strophe.getText( data[ 0 ] );

	if ( result == E_ERROR ) {
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_UNABLE_DELETE_CHANNEL_STYLE );

	} else if ( result == R_OK ) {
		var name = response.getElementsByTagName( 'name' );
		name = Strophe.getText( name[ 0 ] );

		this.removeUserChannelStyle( name );
		this.selectChannelByName( CH_MAIN );
		this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', '�� ������� ����� ��� �������� ������ <b>' + name + '</b>' );
		this.reloadOptionsFrame();

	}

}


CarnageChat.prototype._getRemoveBroadcastUserChannelResult = function( response ) {
	if ( ! response ) {
		this.onError( 'Error receiving remove broadcast user channel result' );

		return;

	}

	this.reloadOptionsFrame();

}


CarnageChat.prototype._getRealNameFromElement = function( element ) {
	var userRealName = null;

	if ( element.tagName == this.htmlStuff.userNameTag && this.htmlStuff.userContextMenuClasses[ element.className ] ) {
		userRealName = element.getAttribute( 'name' );
	}

	return userRealName;

}


CarnageChat.prototype._getDataFromElement = function( element ) {
	var data = null;

	if ( element.tagName == this.htmlStuff.userNameTag && this.htmlStuff.userContextMenuClasses[ element.className ] ) {
		data = element.getAttribute( 'data' );
	}

	return data;

}


CarnageChat.prototype._getMessageId = function( element ) {
	var messageId = null;

	if ( element.tagName == this.htmlStuff.messageTag && this.htmlStuff.messageContextMenuClasses[ element.className ] ) {
		messageId = element.getAttribute( 'id' );
	}

	return messageId;

}


CarnageChat.prototype._createChannel = function( id, name, outputName, isCustom ) {
	return new Channel( id, name, this, outputName, isCustom );
}


CarnageChat.prototype._deleteChannel = function( oChannel, forced ) {
	if ( ! forced ) {
		var oPres = $pres( { to : oChannel.id + '@' + XMPP_MUC + '.' + this._domain, type : 'unavailable' } );
		this.send( oPres.tree() );

	}

	oChannel.free();

}


CarnageChat.prototype._changeChannelByName = function( name ) {
	this.previousChannelName = this.currentChannelName;
	this.currentChannelName = name;

	if ( this.currentChannelName == CH_MAIN ) {
		this.currentChannelId = this.roomId;
	} else {
		this.currentChannelId = this.getChannelByName( name ).id;
	}

	if ( this.currentChannelName == CH_SYSTEM || this.currentChannelName == CH_BATTLE_LOGS ) {
		this._editorDisabledByChannel = true;
	} else {
		this._editorDisabledByChannel = false;
	}

	var oChannel = this.getChannelByName( this.previousChannelName );

	if ( oChannel ) {
		oChannel.lastScrollTop = oChannel.htmlContainer.scrollTop;

		if ( this.chatOptions.autoscrollOff == 0 && oChannel.htmlContainer.scrollTop + parseInt( oChannel.htmlContainer.style.height ) >= oChannel.htmlContainer.scrollHeight ) {
			oChannel.lastScrollTop = -1;
		}

		setTimeout( function() {
				oChat.getChannelByName( oChat.currentChannelName ).scrollTo( oChat.getChannelByName( oChat.currentChannelName ).lastScrollTop );
			}, 50

		);

	}

	this.checkEditor();

	if ( this.editorEnabled ) {
		this.editor.focus();
	}

}


CarnageChat.prototype._onChannelClick = function( event ) {
	if ( jQuery.browser.opera && jQuery.browser.version < 10.5 ) {
		if ( this._channelClick ) {
			clearTimeout( this._channelClick );
			this._channelClick = false;
			this._wasChannelClick = false;

			return false;

		} else {
			if ( ! this._wasChannelClick ) {
				this._wasChannelClick = true;
				this._channelClick = setTimeout( function() {
						clearTimeout( oChat._channelClick );
						oChat._channelClick = false;
						oChat._onChannelClick( event );

					}, 250

				);

				return false;

			} else {
				this._wasChannelClick = false;
			}

		}

	}

	var userRealName = null;

	if ( event && event.target ) {
		var userRealName = this._getRealNameFromElement( event.target );

		if ( event.ctrlKey && userRealName ) {
			this._showUserInfo( userRealName );
		} else if ( event.target.className == this.htmlStuff.userPrivateClass ) {
			var private = this._getDataFromElement( event.target );

			if ( private ) {
				this.writeToEditorPrivate( private );
			}

		} else {
			if ( userRealName ) {
				var flag = true;

				if ( top.frames[ 'main' ].pasteNick ) {
					flag = top.frames[ 'main' ].pasteNick( userRealName, true );
				}

				if ( flag ) {
					this.writeToEditorTo( userRealName );
				}

			}

		}

	}

}


CarnageChat.prototype._onChannelPopupShow = function( event, context ) {
	if ( event && event.target ) {
		var userRealName = null;
		var messageId = null;

		if ( userRealName = this._getRealNameFromElement( event.target ) ) {
			var visibleItems = this._getChannelPopupVisibleItems( userRealName );

			return this._displayVisibleItems( visibleItems, this.htmlStuff.channelPopupItemPrefix, this.channelListPopup );

		} else if ( messageId = this._getMessageId( event.target ) ) {
			var visibleItems = this._getMessageVisibleItems();

			return this._displayVisibleItems( visibleItems, this.htmlStuff.channelPopupItemPrefix, this.channelListPopup );

		} else if ( event.target.id == this.htmlStuff.channelHelper ) {
			var visibleItems = this._getChannelVisibleItems();

			return this._displayVisibleItems( visibleItems, this.htmlStuff.channelPopupItemPrefix2, this.channelListPopup2 );

		} else {
			return false;
		}

	} else {
		return false;
	}

}


CarnageChat.prototype._onChannelPopupAfterShow = function( event, context ) {
	var userRealName = null;

	if ( userRealName = this._getRealNameFromElement( event.target ) ) {
		this._MainWindow.assignZeroClip( userRealName );
	}
	
}


CarnageChat.prototype._onChannelPopupItemClick = function( target, context, element ) {
	if ( target ) {
		var userRealName = null;
		var messageId = null
		var action = element.firstChild.getAttribute( 'href' );
		action = action.replace( /^.*#/, '' );

		if ( userRealName = this._getRealNameFromElement( target ) ) {
			switch ( action ) {
				case 'to':
					this.writeToEditorTo( userRealName );
					break;

				case 'private':
					this.writeToEditorPrivate( userRealName );
					break;

				case 'info':
					this._showUserInfo( userRealName );
					break;

				case 'cake':
					this.throwThing( userRealName, 'cake' );
					break;

				case 'snowball':
					this.throwThing( userRealName, 'snowball' );
					break;

				case 'contact':
					this._addUserToContacts( userRealName );
					break;

				case 'ignore':
					this._ignoreUser( userRealName );
					break;

				case 'unignore':
					this._unignoreUser( userRealName );
					break;

			}

		} else if ( messageId = this._getMessageId( target ) ) {
			switch ( action ) {
				case 'spam':
					this._sendSpamReport( messageId );
					break;

				case 'complain':
					this._sendComplain( messageId );
					break;

			}

		} else if ( target.id == this.htmlStuff.channelHelper ) {
			switch ( action ) {
				case 'clan':
					this.writeToEditor( '!���� ' );
					break;

				case 'guild':
					this.writeToEditor( '!������� ' );
					break;

				case 'group':
					this.writeToEditor( '!������ ' );
					break;

				case 'poker':
					this.writeToEditor( '!����� ' );
					break;

				default:
					if ( action.substr( 0, 5 ) == 'user-' ) {
						var channel = action.substr( 5 );
						this.writeToEditor( '!' + channel + ' ' );

					}

			}

		}

	}

}


CarnageChat.prototype._getChannelPopupVisibleItems = function( userRealName ) {
	var monthTypes = new Array( 'winter', 'winter', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'winter' );
	var type = monthTypes[ ( new Date() ).getMonth() ];
	var items = {};
	var throwType = '';

	if ( type == 'summer' ) {
		throwType = 'cake';
	} else {
		throwType = 'snowball';
	}

	items[ 'to' ] = 1;
	items[ 'private' ] = 1;
	items[ 'info' ] = 1;
	items[ 'copy' ] = 1;
	items[ throwType ] = 1;
	items[ 'contact' ] = 1;

	if ( this.isUserIgnored( userRealName ) ) {
		items[ 'unignore' ] = 1;
	} else {
		items[ 'ignore' ] = 1;
	}

	items[ 'close' ] = 1;

	return items;

}


CarnageChat.prototype._getMessageVisibleItems = function() {
	var items = {};
	items[ 'spam' ] = 1;
	items[ 'complain' ] = 1;
	items[ 'close' ] = 1;

	return items;

}


CarnageChat.prototype._getChannelVisibleItems = function() {
	var items = {};
	var channelCount = 0;

	if ( this.chatOptions.clanChannelOff == 0 && this.chatParams.clan > 0 ) {
		items[ 'clan' ] = 1;
		channelCount++;

	}

	if ( this.chatOptions.clanChannelOff == 0 && this.chatParams.guild > 0 ) {
		items[ 'guild' ] = 1;
		channelCount++;

	}

	if ( this.chatOptions.groupChannelOff == 0 && this.chatParams.groupId.length > 0 ) {
		items[ 'group' ] = 1;
		channelCount++;

	}

	if ( this.chatOptions.pokerChannelOff == 0 && this.chatParams.pokerId.length > 0 ) {
		items[ 'poker' ] = 1;
		channelCount++;

	}

	for ( var channel in this._channels ) {
		if ( this.isUserChannel( this._channels[ channel ].id ) ) {
			items[ 'user-' + this.getUserChannelName( this._channels[ channel ].id ) ] = 1;
			channelCount++;

		}

	}

	if ( channelCount > 0 ) {
		items[ 'close' ] = 1;
	}

	return items;

}


CarnageChat.prototype._checkChannelList = function() {
	var channelCount = 0;

	if ( this.chatOptions.clanChannelOff == 0 && this.chatParams.clan > 0 ) {
		channelCount++;
	}

	if ( this.chatOptions.clanChannelOff == 0 && this.chatParams.guild > 0 ) {
		channelCount++;
	}

	if ( this.chatOptions.groupChannelOff == 0 && this.chatParams.groupId.length > 0 ) {
		channelCount++;
	}

	if ( this.chatOptions.pokerChannelOff == 0 && this.chatParams.pokerId.length > 0 ) {
		channelCount++;
	}

	for ( var channel in this._channels ) {
		if ( this.isUserChannel( this._channels[ channel ].id ) ) {
			channelCount++;
		}

	}

	if (this.channelHelper) {
		if ( channelCount > 0 ) {
			this.channelHelper.style.display = 'block';
		} else {
			this.channelHelper.style.display = 'none';
		}
	}

	return channelCount;

}


CarnageChat.prototype._checkToBot = function( message ) {
	var regTo = new RegExp( /^[\/\.\!\,]to\s+([^:]*?)\s*:\s*(.+)$/ );

	if ( regTo.test( message ) ) {
		var toStr = RegExp.$1;
		message = RegExp.$2;

		if ( this.getChatUsername( toStr ) == BOT_NAME ) {
			setTimeout( function() {
					oChat.sendToBot( 'room', BOT_NAME, message );
				}, 500

			);

		}

	}

}


CarnageChat.prototype._onOnlineListClick = function( event ) {
	if (this._tutorial) return;

	if ( jQuery.browser.opera && jQuery.browser.version < 10.5 ) {
		if ( this._onlineListClick ) {
			clearTimeout( this._onlineListClick );
			this._onlineListClick = false;
			this._wasOnlineClick = false;

			return false;

		} else {
			if ( ! this._wasOnlineClick ) {
				this._wasOnlineClick = true;
				this._onlineListClick = setTimeout( function() {
						clearTimeout( oChat._onlineListClick );
						oChat._onlineListClick = false;
						oChat._onOnlineListClick( event );

					}, 250

				);

				return false;

			} else {
				this._wasOnlineClick = false;
			}

		}

	}

	if ( event && event.target && event.target.parentNode && event.target.parentNode.parentNode ) {
		var oUser = null;
		var oLi = null;
		var actionType = '';

		if ( event.target.parentNode.parentNode.tagName == 'LI' ) {
			oLi = event.target.parentNode.parentNode;
			oUser = this.getUserByName( oLi.getAttribute( 'name' ) );

			if ( oUser ) {
				if ( jQuery( event.target ).is( '.name' ) ) {
					if ( event.ctrlKey ) {
						this._showUserInfo( oUser.realName );
					} else {
						var flag = true;

						if ( top.frames[ 'main' ].pasteNick ) {
							flag = top.frames[ 'main' ].pasteNick( oUser.realName, true );
						}

						if ( flag ) {
							this.writeToEditorTo( oUser.realName );
						}

					}

				} else if ( event.target.className == 'private' ) {
					this.writeToEditorPrivate( oUser.realName );
				} else if ( event.target.className == 'info' ) {
					this._showUserInfo( oUser.realName );
				}

			}

		}

	}

}


CarnageChat.prototype._onOnlinePopupShow = function( event, context ) {
	if (this._tutorial) return;

	var oUser = null;

	if ( event && event.target && event.target.parentNode && event.target.parentNode.parentNode && event.target.parentNode.parentNode.getAttribute && ( oUser = this.getUserByName( event.target.parentNode.parentNode.getAttribute( 'name' ) ) ) ) {
		var visibleItems = this._getOnlinePopupVisibleItems( oUser );

		return this._displayVisibleItems( visibleItems, this.htmlStuff.onlinePopupItemPrefix, this.onlineListPopup );

	} else {
		return false;
	}

}


CarnageChat.prototype._onOnlinePopupAfterShow = function( event, context ) {
	if (this._tutorial) return;

	var oUser = null;

	if ( event && event.target && event.target.parentNode && event.target.parentNode.parentNode && event.target.parentNode.parentNode.getAttribute && ( oUser = this.getUserByName( event.target.parentNode.parentNode.getAttribute( 'name' ) ) ) ) {
		this._OnlineWindow.assignZeroClip( oUser.realName );
	}

}


CarnageChat.prototype._onOnlinePopupItemClick = function( target, context, element ) {
	if (this._tutorial) return;

	var oUser = null;

	if ( target && target.parentNode && target.parentNode.parentNode && target.parentNode.parentNode.getAttribute && ( oUser = this.getUserByName( target.parentNode.parentNode.getAttribute( 'name' ) ) ) ) {
		var action = element.firstChild.getAttribute( 'href' );
		action = action.replace( /^.*#/, '' );

		switch ( action ) {
			case 'talk':
				this._talkTo( oUser.id );
				break;

			case 'to':
				this.writeToEditorTo( oUser.realName );
				break;

			case 'private':
				this.writeToEditorPrivate( oUser.realName );
				break;

			case 'info':
				this._showUserInfo( oUser.realName );
				break;

			case 'copy':
				this._copyUserName( oUser.realName );
				break;

			case 'cake':
				this.throwThing( oUser.realName, 'cake' );
				break;

			case 'snowball':
				this.throwThing( oUser.realName, 'snowball' );
				break;

			case 'meddle':
				this._meddleBot( oUser.id );
				break;

			case 'attack':
				this._attackBot( oUser.id );
				break;

			case 'contact':
				this._addUserToContacts( oUser.realName );
				break;

			case 'ignore':
				this._ignoreUser( oUser.realName );
				break;

			case 'unignore':
				this._unignoreUser( oUser.realName );
				break;

			case 'viewbattle':
				this._viewBattle( oUser.userInfo[ 'battle' ] );
				break;

			case 'refresh':
				this.getUsersInRoom();
				break;

		}

	}

}


CarnageChat.prototype._getOnlinePopupVisibleItems = function( oUser ) {
	if (this._tutorial) return {};

	var monthTypes = new Array( 'winter', 'winter', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'summer', 'winter' );
	var type = monthTypes[ ( new Date() ).getMonth() ];
	var items = {};
	var throwType = '';

	if ( type == 'summer' ) {
		throwType = 'cake';
	} else {
		throwType = 'snowball';
	}

	if ( oUser.userInfo[ 'isBot' ] > 0 ) {
		if ( oUser.userInfo[ 'speaking' ] > 0 ) {
			items[ 'talk' ] = 1;
		}

		items[ 'info' ] = 1;
		//items[ 'copy' ] = 1;
		items[ throwType ] = 1;

		if ( oUser.userInfo[ 'battle' ] > 0 ) {
			items[ 'viewbattle' ] = 1;

			if ( oUser.userInfo[ 'meddle' ] > 0 ) {
				items[ 'meddle' ] = 1;
			}

		}

		if ( oUser.userInfo[ 'attack' ] > 0 ) {
			items[ 'attack' ] = 1;
		}

	} else {
		items[ 'to' ]      = 1;
		items[ 'private' ] = 1;
		items[ 'info' ]    = 1;
		items[ 'copy' ]    = 1;
		items[ throwType ] = 1;
		items[ 'contact' ] = 1;

		if ( this.isUserIgnored( oUser.realName ) ) {
			items[ 'unignore' ] = 1;
		} else {
			items[ 'ignore' ] = 1;
		}

		if ( oUser.userInfo[ 'battle' ] > 0 ) {
			items[ 'viewbattle' ] = 1;
		}

	}
	
	items[ 'refresh' ] = 1;
	items[ 'close' ] = 1;

	return items;

}


CarnageChat.prototype._displayVisibleItems = function( visibleItems, prefix, oList ) {
	var visibleItemsHtml = {};

	for ( var prop in visibleItems ) {
		visibleItemsHtml[ prefix + prop ] = visibleItems[ prop ];
	}

	var items = oList.getElementsByTagName( 'LI' );
	var showCount = 0;

	for ( var i = 0; i < items.length; i++ ) {
		if ( visibleItemsHtml[ items[ i ].id ] ) {
			items[ i ].style.display = '';
			showCount++;

			if ( items[ i ].id == 'online-popup-item-close' ) {
				top.copyNick = items[ i ].firstChild;
			}

		} else {
			items[ i ].style.display = 'none';
		}

	}

	if ( showCount > 0 ) {
		return true;
	} else {
		return false;
	}

}


CarnageChat.prototype._talkTo = function( id ) {
	top.frames[ 'main' ].location = '/npc.php?cmd=npc&nid=' + id + '&' + Math.random();
}


CarnageChat.prototype._showUserInfo = function( realName ) {
	top.open( '/inf=' + realName );
}


CarnageChat.prototype._copyUserName = function( realName ) {
	// ���� ������ ������
}


CarnageChat.prototype._meddleBot = function( id ) {
	top.frames[ 'main' ].location = '/main.php?cmd=meddle_bot&bot=' + id + '&' + Math.random();
}


CarnageChat.prototype._attackBot = function( id ) {
	top.frames[ 'main' ].location = '/main.php?cmd=attack_bot&bot=' + id + '&' + Math.random();
}


CarnageChat.prototype._addUserToContacts = function( realName ) {
	top.frames[ 'main' ].location = '/friends.php?cmd=contacts.show&suser=' + realName + '&' + Math.random();
}


CarnageChat.prototype._ignoreUser = function( realName ) {
	top.frames[ 'main' ].location = '/friends.php?cmd=ignored&suser=' + realName + '&' + Math.random();
}


CarnageChat.prototype._unignoreUser = function( realName ) {
	top.frames[ 'main' ].location = '/friends.php?cmd=ignored&duser=' + realName + '&' + Math.random();
}


CarnageChat.prototype._viewBattle = function( battle ) {
	top.open( '/log=' + battle );
}


CarnageChat.prototype._addToList = function( userRealName, currentText, insertType ) {
	var regPrivate = new RegExp( /^([\/\.\!\,])(p|private|w|whisper|�|�������|�������)\s+([^:]*?)\s*:\s*(.+)$/ );
	var regTo = new RegExp( /^([\/\.\!\,])(to)\s+([^:]*?)\s*:\s*(.+)$/ );
	var reg1 = regPrivate;
	var reg2 = regTo;
	var prefix = 'private ';

	if ( insertType == TO_NICK ) {
		reg1 = regTo;
		reg2 = regPrivate;
		prefix = 'to ';

	}

	var test1 = reg1.test( currentText );
	var test2 = reg2.test( currentText );
	var currentListStr = '';
	var changePrefix = false;

	if ( test1 || test2 ) {
		currentText = RegExp.$4;
		currentListStr = RegExp.$3;

		if ( test1 ) {
			prefix = RegExp.$1 + RegExp.$2 + ' ';
			changePrefix = true;

		} else {
			prefix = RegExp.$1 + prefix;
		}

		currentText = currentText.replace( /^\s+/, '' );
		currentText = currentText.replace( /\s+$/, '' );
		currentListStr = currentListStr.replace( /^\s+/, '' );
		currentListStr = currentListStr.replace( /\s+$/, '' );

	} else {
		prefix = '/' + prefix;
	}

	userRealNames = userRealName.split( ',' );

	for ( var i = userRealNames.length - 1; i >= 0; i-- ) {
		userRealNames[ i ] = userRealNames[ i ].replace( /^\s+/, '' );
		userRealNames[ i ] = userRealNames[ i ].replace( /\s+$/, '' );

	}

	var currentList = new Array();

	if ( currentListStr != '' ) {
		currentList = currentListStr.split( ',' );
	}

	for ( var j = 0; j < userRealNames.length; j++ ) {
		var exists = false;

		for ( var i = currentList.length - 1; i >= 0; i-- ) {
			currentList[ i ] = currentList[ i ].replace( /^\s+/, '' );
			currentList[ i ] = currentList[ i ].replace( /\s+$/, '' );

			if ( currentList[ i ] == '' ) {
				currentList.splice( i, 1 );
			} else {
				if ( currentList[ i ].toLowerCase() == userRealNames[ j ].toLowerCase() ) {
					exists = true;
				}

			}

		}

		if ( ! exists ) {
			currentList.push( userRealNames[ j ] );
		} else {
			if ( insertType == TO_NICK && changePrefix ) {
				prefix = '/private ';
			}

		}

	}

	return prefix + currentList.join( ', ' ) + ': ' + currentText;

}


CarnageChat.prototype._sendSpamReport = function( messageId ) {
	if ( ! this.connected() || ! this.editorEnabled ) {
		return false;
	}

	this.loadData( 'user.send_spam_report', { 'message_id' : messageId }, this._getSendSpamReportResult );
	this.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', MSG_SPAM_REPORT );

	return true;

}


CarnageChat.prototype._sendComplain = function( messageId ) {
	if ( ! this.connected() || ! this.editorEnabled ) {
		return false;
	}

	this.loadData( 'user.send_complain', { 'message_id' : messageId }, this._getSendComplainResult );

	return true;

}


CarnageChat.prototype._getChatClient = function() {
	return new ChatClient( this._url );
}


CarnageChat.prototype._onRequestStateChange = function( handler, req ) {
	if ( req.request.readyState == 4 ) {
		if ( req.request.status == 200 && req.request.responseXML ) {
			handler( req.request.responseXML.documentElement );
		} else {
			this.onError( 'Response error' );
		}

		this._removeRequest( req );

	}

}


CarnageChat.prototype._removeRequest = function( req ) {
	for ( var i = this._requests.length - 1; i >= 0; i-- ) {
		if ( req == this._requests[ i ] ) {
			this._requests.splice( i, 1 );
		}

	}

}


CarnageChat.prototype._findQueryById = function( id ) {
	for ( var i = this._queries.length - 1; i >= 0; i-- ) {
		if ( this._queries[ i ].id == id ) {
			return this._queries[ i ];
		}

	}

	return null;

}


CarnageChat.prototype._removeQueryById = function( id ) {
	for ( var i = this._queries.length - 1; i >= 0; i-- ) {
		if ( this._queries[ i ].id == id ) {
			this._queries.splice( i, 1 );
		}

	}

}


CarnageChat.prototype._parseCommand = function( text ) {
	var oCommand = {};
	var reg = new RegExp( /^(.+?)\|(.*)$/ );

	if ( reg.test( text ) ) {
		oCommand[ 'name' ] = RegExp.$1;
		var params = RegExp.$2;
		var pairs = params.split( '|' );

		for ( var i = 0; i < pairs.length; i++ ) {
			if ( pairs[ i ] ) {
				var regPair = new RegExp( /^(.+?)\=(.*)$/ );

				if ( regPair.test( pairs[ i ] ) ) {
					var name = RegExp.$1;
					var value = RegExp.$2;

					if ( name == 'user' ) {
						value = value.replace( /&#166;/g, '|' );
					}

					oCommand[ name ] = value;

				}

			}

		}

	}

	return oCommand;

}


CarnageChat.prototype._prepareTextForOutput = function( text ) {
	return text;
}


CarnageChat.prototype._needJoinChannel = function( channelId ) {
	return ( ! this.isGroupChannel( channelId ) || this.chatOptions.groupChannelOff == 0 ) &&
	( ! this.isPokerChannel( channelId ) || this.chatOptions.pokerChannelOff == 0 ) &&
	( ! this.isBattleLogChannel( channelId ) || this.chatOptions.battleLogChannelOff == 0 );

}


CarnageChat.prototype._createPrivateList = function( listStr, attrToStr ) {
	var list = listStr.split( ',' );

	for ( var i = list.length - 1; i >= 0; i-- ) {
		list[ i ] = list[ i ].replace( /^\s+/, '' );
		list[ i ] = list[ i ].replace( /\s+$/, '' );

		if ( _isStringEmpty( list[ i ] ) ) {
			list.splice( i, 1 );
		} else {
			list[ i ] = '<span class="user-to-private" name="' + list[ i ] + '" data="' + attrToStr + '">' + list[ i ] + '</span>';
		}

	}

	return list.join( '<span class="user-to-private" data="' + attrToStr + '">, </span>' );

}


CarnageChat.prototype._defineMessageType = function( channelId, typeMessage ) {
	var type = MT_ROOM;

	if ( this.isClanChannel( channelId ) ) {
		type = MT_CLAN;
	} else if ( this.isGuildChannel( channelId ) ) {
		type = MT_GUILD;
	} else if ( this.isGroupChannel( channelId ) ) {
		type = MT_GROUP;
	} else if ( this.isPokerChannel( channelId ) ) {
		type = MT_POKER;
	} else if ( typeMessage ) {
		type = typeMessage;
	}

	return type;

}


CarnageChat.prototype._getUrl = function() {
	return XMPP_URL;
}


CarnageChat.prototype._getServer = function() {
	return XMPP_SERVER;
}


CarnageChat.prototype._getDomain = function() {
	return XMPP_DOMAIN;
}


CarnageChat.prototype._connect = function() {
	this._ChatClient.connect( Utf8.encode( this.userName ) + '@' + this._server + '/' + XMPP_RESOURCE, this.password );
}


CarnageChat.prototype._disconnect = function( type ) {
	this._disconnectType = type;
	this._ChatClient.disconnect();

}


CarnageChat.prototype._needConnect = function() {
	return this._ChatClient._client.errors > 4;
}


CarnageChat.prototype._attachOnAuthenticate = function() {
}


CarnageChat.prototype._attachOnConnect = function() {
}


CarnageChat.prototype._attachOnDisconnect = function() {
}


CarnageChat.prototype._attachOnError = function() {
}


CarnageChat.prototype._attachOnMessage = function() {
	this._ChatClient._attachOnMessage( this._onMessage );
}


CarnageChat.prototype._attachOnPresence = function() {
	this._ChatClient._attachOnPresence();
}


CarnageChat.prototype._attachOnQuery = function() {
	this._ChatClient._attachOnQuery();
}





/*****************************************************************************
* ChatClient
*****************************************************************************/
function ChatClient( url ) {
	this._client = new Strophe.Connection( url );
}


ChatClient.prototype.connect = function( userName, password ) {
	this._client.connect( userName, password, this._callback.bind( this ) );
}


ChatClient.prototype.disconnect = function() {
	this._client.disconnect();
}


ChatClient.prototype.reset = function() {
	this._client.reset();
}


ChatClient.prototype.send = function( stanza ) {
	this._client.send( stanza );
}


ChatClient.prototype._attachOnMessage = function( handler ) {
	this._onMessageHandler = handler;
	this._client.addHandler( this._onMessage.bind( this ), null, 'message', null, null, null );

}


ChatClient.prototype._attachOnPresence = function() {
	this._client.addHandler( this._onPresence.bind( this ), null, 'presence', null, null, null );
}


ChatClient.prototype._attachOnQuery = function() {
	this._client.addHandler( this._onQuery.bind( this ), null, 'iq', null, null, null );
}


ChatClient.prototype._callback = function( status ) {
	if ( status == Strophe.Status.ERROR ) {
		this.owner._onError( 'Error from Strophe.' );
	} else if ( status == Strophe.Status.CONNECTING ) {
		setTimeout( function() { oChat.checkConnection(); }, 60000 );
	} else if ( status == Strophe.Status.CONNFAIL ) {
		this.owner.selectChannelByName( CH_MAIN );
		this.owner.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_FAILED_TO_CONNECT );

	} else if ( status == Strophe.Status.AUTHENTICATING ) {
		this.owner._onConnect();
	} else if ( status == Strophe.Status.AUTHFAIL ) {
		this.owner.selectChannelByName( CH_MAIN );
		this.owner.mainChannel.appendMessage( '', MT_SYS_ATTENTION, '', '', '', EMSG_NOT_AUTHENTICATED );

	} else if ( status == Strophe.Status.DISCONNECTED ) {
		this.owner._onDisconnect();
	} else if ( status == Strophe.Status.CONNECTED ) {
		this.owner._onAuthenticate();
	}

}


ChatClient.prototype._onMessage = function( message ) {
	this._onMessageHandler.call( this.owner, message );

	return true;

}


ChatClient.prototype._onPresence = function( presence ) {
	this.owner.onPresence( presence );

	return true;

}


ChatClient.prototype._onQuery = function( query ) {
	this.owner.onQuery( query );

	return true;

}





/*****************************************************************************
* Channel
*****************************************************************************/
function Channel( id, name, owner, outputName, isCustom ) {
	Inherited( this, BaseChannel, id, name, owner, outputName, isCustom );
	
	if ( this.name == this.outputName ) {
		// ������� �������
		this.tab();

	} else {
		this.selfOutput = false;
		this.htmlContainer = this.owner.getChannelByName( this.outputName ).htmlContainer;

	}

}


Inheritance( Channel, BaseChannel );


Channel.prototype.sendPresence = function( password ) {
	if ( typeof( password ) != 'undefined' ) {
		password = '' + password;
	} else {
		password = '';
	}

	var output = '';

	if ( this.owner.isUserChannel( this.id ) && ! this.selfOutput ) {
		output = this.outputName;
	}

	var oPres = $pres( { to : this.id + '@' + XMPP_MUC + '.' + this.owner._domain + '/' + this.owner.userName, output : output } );

	if ( password.length > 0 ) {
		oPres.c( 'x', { xmlns : NS_MUC } ).c( 'password' ).t( password );
	}

	this.owner.send( oPres.tree() );

}


Channel.prototype.appendText = function( text ) {
	var needScrollBottom = this.owner.chatOptions.autoscrollOff == 0 && this.htmlContainer.scrollTop + parseInt( this.htmlContainer.style.height ) + 20 >= this.htmlContainer.scrollHeight;

	if ( this.htmlContainer.childNodes.length >= DEFAULT_MAX_ROWS + 1 ) {
		var scrollHeight = this.htmlContainer.scrollHeight;
		this.htmlContainer.removeChild( this.htmlContainer.firstChild );
		this.htmlContainer.scrollTop -= scrollHeight - this.htmlContainer.scrollHeight;

	}

	var oRow = this.owner._MainDocument.createElement( 'P' );
	oRow.innerHTML = this.owner._prepareTextForOutput( text );
	this.htmlContainer.insertBefore( oRow, this.htmlContainer.lastChild );

	if ( needScrollBottom ) {
		this.scrollBottom();
	}

	if ( this.owner.isPlaySound ) {
		this.owner.isPlaySound = false;
		this.owner.playSound( 2 );

	}

}


Channel.prototype.tab = function() {
	if ( ! this.selfOutput ) {
		this.lastScrollTop = 0;
		var index = 100;
		var name = this.outputName ? this.outputName : this.name;

		if ( typeof( CHANNEL_ORDERS[ name ] ) != 'undefined' ) { // ���� ���� �� ������������� �������
			index = 0;
			var flag = true;

			// ���������� ������ �������
			while ( flag ) {
				var channelName = this.owner.commonChannelOrders[ index ];

				if ( ! channelName || CHANNEL_ORDERS[ name ] < CHANNEL_ORDERS[ channelName ] ) {
					// ��������� ����� ����� � ������ ������� ������������� �������
					this.owner.commonChannelOrders.splice( index, 0, name );
					flag = false;

				} else {
					index++;
				}

			}

		}

		var channelId = this.channelId;
		var name = this.name;
		var oldOpera = jQuery.browser.opera && jQuery.browser.version < 10.5;

		jQuery( '#' + oChat.htmlStuff.channels ).tabs( 'add', '#' + channelId, name, index );
		jQuery( '#' + channelId ).jeegoocontext( oChat.channelListPopup, {
				heightOverflowOffset: 1,
				hoverClass : '',
				operaEvent : oldOpera ? 'dblclick' : 'contextmenu',
				onShow : oChat._onChannelPopupShow.bind( oChat ),
				onAfterShow : oChat._onChannelPopupAfterShow.bind( oChat ),
				onSelect : oChat._onChannelPopupItemClick.bind( oChat ),
				oWindow : oChat._MainWindow

			}

		);

		jQuery( '#' + channelId ).click( oChat._onChannelClick.bind( oChat ) );

		this.htmlContainer = this.owner._MainDocument.getElementById( this.channelId );
		this._clearContainer();

		if ( this.owner.chatOptions.autoscrollOff == 0 ) {
			this.lastScrollTop = -1;
		}

		jQuery( this.htmlContainer ).data( 'name', name );
		this.selfOutput = true;
		this.outputName = this.name;

	}

}


Channel.prototype.untab = function() {
	if ( this.selfOutput && this.id != MAIN_ID ) {
		this.selfOutput = false;
		this.outputName = CH_MAIN;

		if ( typeof( CHANNEL_ORDERS[ this.name ] ) != 'undefined' ) { // ���� ���� �� ������������� �������
			var flag = true;
			var i = 0;

			while ( flag ) {
				if ( this.owner.commonChannelOrders[ i ] == this.name ) {
					// ������� ����� �� ������ ������� ������������� �������
					this.owner.commonChannelOrders.splice( i, 1 );
					flag = false;

				} else if ( i >= this.owner.commonChannelOrders.length ) {
					flag = false;
				} else {
					i++;
				}

			}

		}

		var channelId = this.channelId;
		jQuery( '#' + channelId ).unbind( '.jeegoocontext' );
		var index = jQuery( this.htmlContainer ).index() - 1;
		jQuery( '#' + oChat.htmlStuff.channels ).tabs( 'remove', index );
		this.htmlContainer = this.owner.getChannelByName( this.outputName ).htmlContainer;

	}

}


Channel.prototype.clear = function() {
	if (this.owner._tutorial) return;

	if ( this.owner.editor.value.length == 0 ) {
		if ( confirm( '�������� ���� ����?' ) ) {
			this.htmlContainer.innerHTML = '';
			this._clearContainer();

			if ( this.owner.editorEnabled ) {
				this.owner.editor.focus();
			}

		}

	} else {
		this.owner.editor.value = '';

		if ( this.owner.editorEnabled ) {
			this.owner.editor.focus();
		}

	}

}


Channel.prototype.scrollBottom = function() {
	this.htmlContainer.offsetHeight;
	this.htmlContainer.offsetWidth;
	this.htmlContainer.scrollTop = this.htmlContainer.scrollHeight;

}


Channel.prototype.scrollTo = function( value ) {
	if ( value == -1 ) {
		this.scrollBottom();
	} else {
		this.htmlContainer.scrollTop = value;
	}

}


Channel.prototype.free = function() {
	this.untab();
	this.htmlContainer = null;
	this.owner = null;

}


Channel.prototype._clearContainer = function() {
	var oDiv = this.owner._MainDocument.createElement( 'DIV' );
	oDiv.style.padding = '2px';
	this.htmlContainer.appendChild( oDiv );

}


Channel.prototype._afterAppendMessage = function() {
	if ( this.owner.currentChannelName != this.name && this.owner.currentChannelName != this.outputName ) {
		jQuery( '#' + this.owner.htmlStuff.tabId + ' li:has(a[href="#' + this.htmlContainer.id + '"])' ).addClass( 'ui-state-new' );
	}

}





/*****************************************************************************
* User
*****************************************************************************/
function User( name, owner ) {
	Inherited( this, BaseUser, name, owner );
}


Inheritance( User, BaseUser );





/*****************************************************************************
* CarnageRequest
*****************************************************************************/
CarnageRequest = function( cmd, params, callback ) {
	this.cmd = cmd;
	this.params = params;
	this.callback = callback;

	this.rId = Math.floor( Math.random() * 4294967295 );
	this.url = 'http://' + window.location.hostname + '/chr.php';

	var paramStr = 'cmd=' + this.cmd + '&rid=' + this.rId + '&s=' + Math.random();

	for ( var key in this.params ) {
		paramStr += '&' + key + '=' + this.params[ key ];
	}

	if ( window.XMLHttpRequest ) {
		// branch for native XMLHttpRequest object
		this.request = new XMLHttpRequest();

	} else if ( window.ActiveXObject ) {
		// branch for IE/Windows ActiveX version
		this.request = new ActiveXObject( 'Microsoft.XMLHTTP' );

	}

	if ( this.request ) {
		this.request.onreadystatechange = this.callback.prependArg( this );
		this.request.open( 'POST', this.url );
		this.request.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
		this.request.send( paramStr );

	}

}





/*****************************************************************************
* CarnageRequestUri
*****************************************************************************/
CarnageRequestUri = function( uri, callback ) {
	this.uri = uri;
	this.callback = callback;

	this.rId = Math.floor( Math.random() * 4294967295 );
	this.url = 'http://' + window.location.hostname + this.uri;

	if ( window.XMLHttpRequest ) {
		// branch for native XMLHttpRequest object
		this.request = new XMLHttpRequest();

	} else if ( window.ActiveXObject ) {
		// branch for IE/Windows ActiveX version
		this.request = new ActiveXObject( 'Microsoft.XMLHTTP' );

	}

	if ( this.request ) {
		this.request.onreadystatechange = this.callback.prependArg( this );
		this.request.open( 'GET', this.url, true );
		this.request.send( null );

	}

}





/*****************************************************************************
* CarnageQuery
*****************************************************************************/
CarnageQuery = function( type, id, params ) {
	this.type = type;
	this.params = params;
	this.id = id;

}


CarnageQuery.prototype.getBaseId = function() {
	var result = this.id;

	return result.replace( /\d.*/, '' );

}





/*****************************************************************************
* ��������������� �������
*****************************************************************************/

// ���������� ���������� �� ��� X �������� input
function _getPosX( obj ) {
	var currentX = 0;

	while ( obj.offsetParent != null ) {
		currentX += obj.offsetLeft;
		obj = obj.offsetParent;

	}

	return currentX + obj.offsetLeft;

}


// ���������� ���������� �� ��� Y �������� input
function _getPosY( obj ) {
	var currentY = 0;

	while ( obj.offsetParent != null ) {
		currentY += obj.offsetTop;
		obj = obj.offsetParent;

	}

	return currentY + obj.offsetTop;

}


// �������� ������ ��� ��������
function _escapeEx( str ) {
	if ( ! str ) {
		return ''
	}

	var ret = '';

	for ( i = 0; i < str.length; i++ ) {
		var n = str.charCodeAt( i );

		if ( n >= 0x410 && n <= 0x44F ) {
			n -= 0x350;
		} else if ( n == 0x451 ) {
			n = 0xB8;
		} else if ( n == 0x401 ) {
			n = 0xA8;
		}

		if ( ( n < 65 || n > 90 ) && ( n < 97 || n > 122 ) && n < 256 ) {
			if ( n < 16 ) {
				ret += '%0' + n.toString( 16 );
			} else {
				ret += '%'+ n.toString( 16 );
			}

		} else {
			ret += String.fromCharCode( n );
		}

	}

	return ret;

}


// ���������� ������
function _filterMessage( message, decode, special ) {
	if ( _isStringEmpty( message ) ) {
		return '';
	}

	message = message.replace( /\s+$/, '' );

	if ( special != 1 ) {
		if ( decode > 0 ) {
			message = message.slice( 0, MAX_MESSAGE_LENGTH * 8 );
		} else {
			message = message.slice( 0, MAX_MESSAGE_LENGTH );
		}

	}

	if ( decode > 0 ) {
		message = message.replace( /&#166;/g, '|' );
		message = message.replace( /&#61;/g, '=' );

		if ( decode == 1 ) {
			message = message.replace( /&/g, '&amp;' );
		}

		message = message.replace( />/g, '&gt;' );
		message = message.replace( /</g, '&lt;' );

		if ( special == 1 ) {
			message = message.replace( /\{\{/g, '<' );
			message = message.replace( /\}\}/g, '>' );

		} else {
			message = message.replace( /"/g, '&quot;' );
			message = message.replace( /'/g, '&#39;' );
			message = message.replace( /\(\{/g, '&lt;' );
			message = message.replace( /\}\)/g, '&gt;' );

		}

		var reg1 = new RegExp( /\S{81}/ );
		var reg2 = new RegExp( /(\S{80})(\S)/ );

		while ( reg1.test( message ) ) {
			reg2.test( message );
			var part1 = RegExp.$1;
			var part2 = RegExp.$2;
			message = message.replace( /\S{81}/, part1 + ' ' + part2 );

		}

		if ( _hasBadChars( message ) ) {
			message = '';
		}

	} else {
		message = message.replace( /\|/g, '&#166;' );
		message = message.replace( /\=/g, '&#61;' );

		var re = new RegExp( /(:([0-9�-�]+):)/ );

		while ( re.test( message ) ) {
			var smile = RegExp.$1;
			var smileName = RegExp.$2;
			var replaceText = '';

			if ( smileyCodes[ smileName ] ) {
				replaceText = '-|-' + smileyCodes[ smileName ] + '-|-';
			} else {
				replaceText = '-|-' + smileName + '-|-';
			}

			message = message.replace( smile, replaceText );

		}

		message = message.replace( /\-\|\-/g, ':' );

	}

	return message;

}


function _filterCommandText( text ) {
	if ( _isStringEmpty( text ) ) {
		return '';
	}

	text = text.replace( /&#166;/g, '|' );
	text = text.replace( /&#61;/g, '=' );
	text = text.replace( /\{\{/g, '<' );
	text = text.replace( /\}\}/g, '>' );

	return text;

}


function _getList( text ) {
	var list = new Array();

	if ( text && text != '' ) {
		list = text.split( ',' );
		var uniqueItems = new Array();

		for ( var i = list.length - 1; i >= 0; i-- ) {
			list[ i ] = list[ i ].replace( /^\s+/, '' );
			list[ i ] = list[ i ].replace( /\s+$/, '' );

			if ( list[ i ] == '' || uniqueItems[ list[ i ] ] ) {
				list.splice( i, 1 );
			} else {
				uniqueItems[ list[ i ] ] = true;
			}

		}

	}

	return list;

}


function _getChannelList( items ) {
	var channels = new Array();

	for ( var i = 0; i < items.length; i++ ) {
		channels.push( '<b>' + items[ i ].getAttribute( 'name' ) + '</b>' );
	}

	return channels.join( ', ' );

}


function _getUserList( items ) {
	var users = new Array();

	for ( var i = 0; i < items.length; i++ ) {
		var name = items[ i ].getAttribute( 'name' );

		if ( ! _isStringEmpty( name ) ) {
			users.push( '{[' + name + ']}' );
		}
	}

	return users.join( ', ' );

}


function _findUser( userName, list ) {
	for ( var i = list.length - 1; i >= 0; i-- ) {
		if ( list[ i ] == userName ) {
			return true;
		}

	}

	return false;

}


function _isStringEmpty( message ) {
	if ( ! message ) {
		return true;
	}

	message = message.replace( /^\s+/, '' );

	if ( message.length > 0 ) {
		return false;
	} else {
		return true;
	}

}


function _replaceElement( what, newElement, listStr ) {
	var list = listStr.split( ',' );

	for ( var i = list.length - 1; i >= 0; i-- ) {
		list[ i ] = list[ i ].replace( /^\s+/, '' );
		list[ i ] = list[ i ].replace( /\s+$/, '' );

		if ( _isStringEmpty( list[ i ] ) || list[ i ].toLowerCase() == what.toLowerCase() ) {
			list.splice( i, 1 );
		}

	}

	if ( ! _isStringEmpty( newElement ) ) {
		list.push( newElement );
	}

	return list.join( ', ' );

}


function _extractElement( listStr, element ) {
	var list = listStr.split( ',' );
	var result = '';

	for ( var i = list.length - 1; i >= 0; i-- ) {
		list[ i ] = list[ i ].replace( /^\s+/, '' );
		list[ i ] = list[ i ].replace( /\s+$/, '' );

		if ( _isStringEmpty( list[ i ] ) ) {
			list.splice( i, 1 );
		} else {
			if ( list[ i ].toLowerCase() == element.toLowerCase() ) {
				result = list[ i ];
				break;

			}

		}

	}

	return result;

}


function _sortLex( elements ) {
	elements.sort( function( a, b ) {
		var itemA = ( '' + a ).toLowerCase();
		var itemB = ( '' + b ).toLowerCase();

		if ( itemA < itemB ) {
			return -1;
		} else if ( itemA > itemB ) {
			return 1;
		} else {
			return 0;
		}

	} );

	return elements;

}


function _checkUserChannel( name ) {
	var reg = new RegExp( /^[A-Za-z�����Ũ����������������������������������������������������������\d]+$/ );

	return ( reg.test( name ) && name.length <= MAX_USER_CHANNEL_NAME_LENGTH );

}


function _checkUserChannelPassword( password ) {
	var reg = new RegExp( /^[A-Za-z\d]*$/ );

	return ( reg.test( password ) && password.length <= MAX_USER_CHANNEL_PASSWORD_LENGTH );

}


function _isReservedName( name ) {
	name = ( '' + name ).toLowerCase();
	var i = 0;
	var count = RESERVED_NAMES.length;
	var found = false;

	while ( ! found && i < count ) {
		if ( name == RESERVED_NAMES[ i ] ) {
			found = true;
		} else {
			i++;
		}

	}

	return found;

}


var en_ru = [["shh","SHH","sh","SH","ch","CH","jo","JO","zh","ZH","je","JE","ju","JU","ja","JA","sx","SX","j/o","J/O","j/e","J/E","a","A","b","B","v","V","g","G","d","D","e","E","z","Z","i","I","j","J","k","K","l","L","m","M","n","N","o","O","p","P","r","R","s","S","t","T","u","U","f","F","x","X","h","H","c","C","w","W","##","#","y","Y","''","'"],["�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","��","��","��","��","��","��","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�"]];

// �����������
function _convert( s ) {
	if ( ! s ) {
		return '';
	}

	for ( var i = 0; i < en_ru[ 0 ].length; ++i ) {
		while ( s.indexOf( en_ru[ 0 ][ i ] ) >= 0 ) {
			s = s.replace( en_ru[ 0 ][ i ], en_ru[ 1 ][ i ] );
		}

	}

	return s;

}


function _hasBadChars( s ) {
	var re = new RegExp( /^[A-Za-z0-9�-��-���~`!@#\$%\^&\*\(\)"�;:\?\-_\+=\/\\\|\s\[\]\{\}<>\'\.,\s]+$/ );

	if ( ! re.test( s ) ) {
		return true;
	} else {
		return false;
	}
}
