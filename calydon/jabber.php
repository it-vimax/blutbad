<?php

require_once('xmpp/_xmpp.php');
require_once('jabber_cache.php');

set_time_limit(0);

$login = 'admin';
$password = '123456';

$server = 'localhost';
$conference = 'conference';

XMPP::connect($login, $password, $server, $conference);

Cache::init();

$previous_xml = '';

while(true) {
	/*if(Cache::get('new') == 1) { */
	 //	Cache::set('new', 0);
    /*$len = Cache::length();

    if($len > 0) {
        $xml = "";
		for($i = 0; $i < $len; $i++) $xml .= Cache::getXml();

		XMPP::send($xml);

		$xml = "";
	}*/

    /*$len = Cache::length();

	if($len > 0) {
		for($i = 0; $i < $len; $i++) $xml .= Cache::getXml();

		$status = XMPP::send($xml);

        Cache::redis()->rpush('xmpp', $status ? $status : 'false');

        Cache::redis()->rpush('data', $xml);

		if(!$status) {
            //XMPP::off();

			XMPP::connect($login, $password, $server, $conference);

			XMPP::send($previous_xml);
			XMPP::send($xml);
		}

		$previous_xml = $xml;

		$xml = '';
	}*/

    $len = Cache::length();

    if($len > 0) {
		for($i = 0; $i < $len; $i++) {
          $xml = Cache::getXml();

          $status = XMPP::send($xml);

          Cache::redis()->rpush('xmpp', $status ? $status : 'false');

          Cache::redis()->rpush('data', $xml);

    		if(!$status) {
              //XMPP::off();

    			XMPP::connect($login, $password, $server, $conference);
                if ($previous_xml) {
                  XMPP::send($previous_xml);
                }

    			XMPP::send($xml);

                $previous_xml = '';

                continue;
    		}

    		$previous_xml = $xml;
		}
	}
	
	if(Cache::get('reconnect') == 1) {
		Cache::set('reconnect', 0);

		//XMPP::off();

		XMPP::connect($login, $password, $server, $conference);
	}
	
	if(Cache::get('stop') == 1) {
		Cache::set('stop', 0);

		XMPP::off();

		break;
	}

   	/*if(Cache::length() == 0)*/ usleep(200000);
}

?>