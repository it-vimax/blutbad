<?

?>
<? if ($_GET['cmd']=='abilities.show') { ?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<title>������� ������������� ����� � FAQ</title>

	<meta http-equiv="content-type" content="text/html; charset=windows-1251">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">

	<link href="/css/main.css" rel="stylesheet" type="text/css">

	<script src="/j/jquery/jquery.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery-ui.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.browser.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.cookie.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.tooltip.mod.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.pnotify.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.placeholder.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.countdown.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.jeegoocontext.mod.js" type="text/javascript"></script>

    <script type="text/javascript">
  		var isBattle = Number(<?= $user['battle'] ?>);
  		var isBid = Number(<?= $user['zayavka'] ?>);
    </script>

	<script src="/js/iner.js" type="text/javascript"></script>

	<style type="text/css">
		.block {
			margin: 0 auto;
			padding: 1em;
			width: 500px;
		}

		h1 {
			font-size: 1.1em;
			text-align: center;
		}

		.option-num {
			font-weight: bold;
			margin: 1em 0 .3em;
		}

		td.value {
			text-align: center;
		}

		p {
			margin-top: 1em;
		}
	</style>
</head>
<body class="main-content">
	<div class="block">
		<h1>������� ������������� �����:</h1>

		<div class="option-num">������� 1</div>
		<table class="table-list">
			<thead>
				<tr>
					<th>��������������</th>
					<th>��������</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>����</td>
					<td class="value">4</td>
				</tr>
				<tr>
					<td>��������</td>
					<td class="value">4</td>
				</tr>
				<tr>
					<td>��������</td>
					<td class="value">3</td>
				</tr>
				<tr>
					<td>����������������</td>
					<td class="value">4</td>
				</tr>
			</tbody>
		</table>
		<div class="option-num">������� 2</div>
		<table class="table-list">
			<thead>
				<tr>
					<th>��������������</th>
					<th>��������</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>����</td>
					<td class="value">4</td>
				</tr>
				<tr>
					<td>��������</td>
					<td class="value">4</td>
				</tr>
				<tr>
					<td>��������</td>
					<td class="value">4</td>
				</tr>
				<tr>
					<td>����������������</td>
					<td class="value">3</td>
				</tr>
			</tbody>
		</table>
		<div class="option-num">������� 3</div>
		<table class="table-list">
			<thead>
				<tr>
					<th>��������������</th>
					<th>��������</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>����</td>
					<td class="value">5</td>
				</tr>
				<tr>
					<td>��������</td>
					<td class="value">3</td>
				</tr>
				<tr>
					<td>��������</td>
					<td class="value">3</td>
				</tr>
				<tr>
					<td>����������������</td>
					<td class="value">4</td>
				</tr>
			</tbody>
		</table>

		<p>
			����&nbsp;� ����� ������ ��������. ��� ���� ���� ����, ��� ������� ���������� ����� ��&nbsp;������� ������ �&nbsp;����� �������. ����� ����, ��&nbsp;���� ������� ��������� ����� ���� �&nbsp;������� ������������.
		</p>
		<p>
			�������� ������ ��&nbsp;��, ��� ����� ��&nbsp;������� ������������� ��&nbsp;������ �����������, �&nbsp;����� ��������� ����� ��&nbsp;������ ��&nbsp;��� ��������.
		</p>
		<p>
			�������� ����������� ���� ������������ �����, ���������� ����� ������ �����, �&nbsp;����� ���� �������� ��� ��&nbsp;������� �����������.
		</p>
		<p>
			���������������� ��&nbsp;����� �����&nbsp;� ��� ���� ����������������, ��� ���� �&nbsp;���� ������� �����.
		</p>
	</div>

</body></html>
<? } ?>