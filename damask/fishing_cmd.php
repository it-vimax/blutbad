<?php
  session_start();
  if ($_SESSION['uid'] == null) die();
  include "connect_curbd.php";
  include "connect.php";
  include "functions.php";
//  include "m_game_function.php";
 # ���������� ���������
//   $user = sql_row("SELECT * FROM `users` WHERE `id` = '{$_SESSION['uid']}' LIMIT 1;");

  header("Content-Type: text/xml; charset=utf-8");
  echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

  $file = 'adata/fishing/fishing.txt';

# ����e���� ������ = ���� ��� ����
  if((int)date("H") > 8 && (int)date("H") < 20) { $day_night = 'day'; } else { $day_night = 'night'; }

  $error_text = array(
    1 => '',
    3 => '�� �� ���������� � ������� � �������� ��� �����.',
    11 => '����� ������. �������� ������.',
    12 => '�� �� ��� ������. ��� �������.',
    22 => '��� ��� ����.',
    24 => '�� �������, ��� �����������?',
    30 => '���. ������ �������.',
    10 => ''
  );

# ������
  $ar_rod = array(
    '������ �������' => array('������������� ������'),
    '���������� ������' => array('������'),
    '��������������� ������' => array('������', '������'),
    '���������� ������' => array('������'),
    '��������� ������' => array('������', '������', '������', '������ �������'),
    '��������� ������' => array('������� ���')
  );

# ����
  $ar_fish = array(
    1 => '���������',
    2 => '������',
    3 => '�������',
    4 => '���',
    5 => '����',
    6 => '�����',
    7 => '��������� �����',
    8 => '����',
    9 => '���',
    10 => '����',
    11 => '�������',
    12 => '���',
    13 => '�����',
    14 => '�����',
    15 => '������� �����'
  );

# ����
  $ar_catch = array(
    '������������� ������' => array('������'),
    '������'               => array('���������', '������', '�������', '������', '�������', '�������', '������', '�������'),
    '������'               => array('���������', '���', '����', '�����', '��������� �����'),
    '������'               => array('���������', '���', '����', '�����', '�����'),
    '������� �������'      => array('���������', '���', '���', '����'),
    '������ �������'       => array('���������', '����', '�����', '���', '��������� �����', '���'),
    '������� ���'          => array('���������', '���������', '���������', '���������', '���������', '������� �����')
  );

# �������
  $ar_bait = array(
    1 => '������',
    2 => '������',
    3 => '������'
  );


/***------------------------------------------
 * ��������� ������� �������
 **/

function is_bait($user, $name_tackle){
 global $ar_rod, $user_my;

 $bait = array();
 $bait['name'] = '';
 $bait['use']  = 0;

   if ($name_tackle) {
    $cur_name_bait = '';
    $inv_bait      = array();
    $inv_bait[]    = $user_my->user['m1'];
    $inv_bait[]    = $user_my->user['m2'];
    $inv_bait[]    = $user_my->user['m3'];
    $inv_bait[]    = $user_my->user['m4'];
    $inv_bait[]    = $user_my->user['m5'];
    $inv_bait[]    = $user_my->user['m6'];

     foreach ($inv_bait as $k => $b_id) {
      if ($b_id > 0) {
       $bait_it = sql_row("SELECT `duration`, `maxdur`, `name` FROM `inventory` WHERE `id` = '".$b_id."' AND `owner` = '".$user_my->user['id']."' AND `dressed` = 1 AND otdel = 91 AND `duration` < `maxdur` LIMIT 1;");
         if (empty($cur_name_bait) || $cur_name_bait == $bait_it['name']) {
           if (in_array($bait_it['name'], $ar_rod[$name_tackle])) {
             $bait['name']   = $bait_it['name'];
             $bait['use']   += $bait_it['maxdur'] - $bait_it['duration'];
             $cur_name_bait  = $bait_it['name'];
           }
         }
      }
     }
   }

  if (empty($bait)) {
     return false;
  } else {
     return $bait;
  }
}

/***------------------------------------------
 * start
 **/

if (@$_GET['cmd'] == "start") { # INIT
  send_user_letter(1, '[�������������]', $_GET['cmd'], serialize($_GET));
  die("start");
}

/***------------------------------------------
 * �������� ������
 **/

if (@$_GET['cmd'] == "launch") { # LAUNCH

  $launch = ''; # ��������� ������
  $is_tackle = false;
  $is_bait = false;
  $messages = '';
  $service_tackle = '';
  $service_bait = '';
  $name_tackle = '';
  $bait_use = 0;
  $launch_it = array();

  $m_x = (@$_GET['x']);
  $m_y = (@$_GET['y']);

  $fishingDuration = rand(25, 35);                   # ����� ��������� ������� (�������� ��������� ������ � ��������)
  $bitingDuration  = rand(3, $fishingDuration);      # ����� ����� ����� �������
  $bitingTime      = $bitingDuration + rand(1, 10);  # ����� �������� � ��������
  $elapsedTime     = 0;                              # ����� ������ ������� �������
  $randTime        = rand(111111, 999999);           # ����

# ���� ������
  if ($user_my->user['weap']) {
   $launch_it = sql_row("SELECT `id`, `duration`, `maxdur`, `name` FROM `inventory` WHERE `owner` = '".$user_my->user['id']."' AND `dressed` = 1 AND `otdel` = 18 LIMIT 1;");
    if (!empty($launch_it['id'])) {
      if ($launch_it['duration'] < $launch_it['maxdur']) {
        $name_tackle = $launch_it['name'];

        $launch = "  <launch bait=\"1\" rod=\"/f/fishing/".INCITY."/udilo.png\" value=\"".$fishingDuration.",".$bitingDuration.",".$bitingTime.",".$elapsedTime.",".$randTime.",22\" x=\"".$m_x."\" y=\"".$m_y."\" />\n";
        $text_tackle = @iconv('CP1251', 'UTF-8', $name_tackle." (".$launch_it['duration']."/".$launch_it['maxdur'].")");
        $service_tackle = "     <service code=\"0\" text=\"".$text_tackle."\" type=\"tackle\" />\n";
        $is_tackle = true;
      }
    }
  }

# ���� �������
  if ($bait = is_bait($user, $name_tackle)) {
    if ($bait['use'] > 0) {
       $bait_use = $bait['use'];
       $text_bait = @iconv('CP1251', 'UTF-8', $bait['name']." (".$bait_use.")");
       $service_bait = "     <service code=\"0\" text=\"".$text_bait."\" type=\"bait\" />\n";
       $is_bait = true;
    }
  }

  if (empty($is_tackle)) {
    $messages .= "     <message code=\"11\" text=\"".@iconv('CP1251', 'UTF-8', $error_text[11])."\" type=\"error\" />\n";
  }
  if (empty($is_bait) && $is_tackle) {
    $messages .= "     <message code=\"12\" text=\"".@iconv('CP1251', 'UTF-8', $error_text[12])."\" type=\"error\" />\n";
    $launch = "";

  }

  if ($service_tackle) { $messages .= $service_tackle; }
  if ($service_bait)   { $messages .= $service_bait;   }


# ��������� ������ ������
  if ($launch && $bait && $launch_it) {
    if (is_file($file)) {
      $fishing = unserialize(file_get_contents($file));      # ��������� ����������
    } else {
      $fishing['users'][$user_my->user['id']] = array();
    }

     $fishing['users'][$user_my->user['id']]['fishing'] = $fishingDuration.','.$bitingDuration.','.$bitingTime;
     $fishing['users'][$user_my->user['id']]['launch'] = $launch_it;
     $fishing['users'][$user_my->user['id']]['bait'] = $bait;
     $fishing['users'][$user_my->user['id']]['start_fish'] = time();
     $fishing['users'][$user_my->user['id']]['end_fish'] = time() + $fishingDuration;

     file_put_contents($file, serialize($fishing), LOCK_EX);    # ��������� ����������


   # �������� ���������
     $user_my->USER_load_inventory_data();

   # ������� 1 ������������� � �������
     $item_bait = sql_row("SELECT `id`, `name`, `maxdur`, `duration`, `type`, `magic` FROM `inventory` WHERE `duration` < `maxdur` AND `name` = '".$bait['name']."' AND `owner` = '".$user_my->user['id']."' AND `dressed` = 1 AND `otdel` = 91 LIMIT 1;");
      if (!empty($item_bait['id'])) {
       sql_query("UPDATE `inventory` SET `duration` = `duration` + 1 WHERE `owner` = '".$user_my->user['id']."' AND `id` = '".$item_bait['id']."' LIMIT 1;");
        if ($user_my->inventory_data[type_dreass_item($item_bait, $user)]['id'] == $item_bait['id']) {
         $user_my->inventory_data[type_dreass_item($item_bait, $user)]['duration'] = $item_bait['duration'] + 1;
         if ($user_my->inventory_data[type_dreass_item($item_bait, $user)]['duration'] >= $item_bait['maxdur']) {

                 if ($item_bait['id'] == $user_my->user['m1']) { $rem_slot = 12;
           } elseif ($item_bait['id'] == $user_my->user['m2']) { $rem_slot = 13;
           } elseif ($item_bait['id'] == $user_my->user['m3']) { $rem_slot = 14;
           } elseif ($item_bait['id'] == $user_my->user['m4']) { $rem_slot = 15;
           } elseif ($item_bait['id'] == $user_my->user['m5']) { $rem_slot = 16;
           } elseif ($item_bait['id'] == $user_my->user['m6']) { $rem_slot = 17;
           } else { $rem_slot   = 0; }

           if ($rem_slot) $user_my->USER_remove_item($rem_slot);

         }
         $user_my->mod_inventory_data = true;
        }
      }

   # ������� 1 ������������� � ������
     if (rand(1, 100) >= 90) {
       sql_query("UPDATE `inventory` SET `duration` = `duration` + 1 WHERE `duration` < `maxdur` AND `name` = '".$name_tackle."' AND `owner` = '".$user_my->user['id']."' AND `dressed` = 1 AND `otdel` = 18 LIMIT 1;");

        if (!empty($user_my->inventory_data['weap']['id'])) {
          $user_my->inventory_data['weap']['duration'] += 1;
          if ($user_my->inventory_data['weap']['duration'] >= $user_my->inventory_data['weap']['maxdur']) {
             $user_my->USER_remove_item(3); # ������� ������
          }
          $user_my->mod_inventory_data = true;
        }
     }

  }

# ������� ������
  echo "<data datetime=\"".$day_night."\">\n".$launch."";
  echo "  <messages>\n";
  echo $messages;
  echo "  </messages>\n";
  echo "</data>";

 die("");
}

/***------------------------------------------
 * �������� ��� ������� ��������
 **/

if (@$_GET['cmd'] == "hooking") { # HOOK

  $cur_time     = time();
  $messages     = '';
  $text_error   = '';
  $is_timebite  = 0;
  $result       = 0; # 0 = ��� �����, 1 = ���������, 2 = ���� ����
  $toBattle     = '';

# ��������� ������ ������
    if (is_file($file)) {
      $fishing = unserialize(file_get_contents($file));      # ��������� ����������

      $a_fishing    = explode(",", $fishing['users'][$user_my->user['id']]['fishing']);

      $fishingDuration = $a_fishing[0];     # ����� ��������� ������� (�������� ��������� ������ � ��������)
      $bitingDuration  = $a_fishing[1];     # ����� ����� ����� �������
      $bitingTime      = $a_fishing[2];     # ����� �������� � ��������

      $a_launch     = $fishing['users'][$user_my->user['id']]['launch'];
      $a_bait       = $fishing['users'][$user_my->user['id']]['bait'];
      $a_start_fish = $fishing['users'][$user_my->user['id']]['start_fish'];
      $a_end_fish   = $fishing['users'][$user_my->user['id']]['end_fish'];


    # ������ ��������
      if ($cur_time >= ($a_start_fish + $bitingDuration) && $cur_time <= ($a_start_fish + $bitingDuration + $bitingTime)) {

       # �������� ����������� ���� ��� �� ����� � �������� ���� ����� ������� ������ 100
         if ($cur_time >= (($a_start_fish + $bitingDuration + $bitingTime) - floor($bitingTime / 3)) || (rand(1, 3) == 1 && $user_my->user['skill_fishing'] < 100)) {
          $is_timebite = 1;
         } else {
          $is_timebite = 2;
         }

      }

          # ��� ����� "��� ��� ����."
            if ($is_timebite == 0) {
              $text_error = "     <message code=\"22\" text=\"".@iconv('CP1251', 'UTF-8', $error_text[22])."\" type=\"error\" />";
              $result = 0;
            }


          # C�������� "�� �� ���������� � ������� � �������� ��� �����."
            if ($is_timebite == 1) {
              $n_error_text = array('�� �������. ���� ��������� � ������.', '�� �� ���������� � ������� � �������� ��� �����.');
              $text_error = "     <message code=\"3\" text=\"".@iconv('CP1251', 'UTF-8', $n_error_text[rand(0, count($n_error_text)-1)])."\" type=\"error\" />";
              $result = 1;
            }

          # �������� �����������
            if ($is_timebite == 2) {

             # ������� �����
               if (rand(1, 100) >= 98) {

                 # 23727 ��� �� ������ ��������� "�������"

				   $bot_waterman_id = 0;

                 # ���� ��������� �������
                   $user_waterman = sql_row("SELECT * FROM `users` WHERE `id` = 23727 LIMIT 1;");
                   if (!empty($user_waterman['id'])) {

                        $user_waterman['level']   = $user_my->user['level'];
                        $user_waterman['login']   = "���� �������";

                        $user_waterman['no_user'] = 1;
                        $user_waterman['guild']   = "";
                        $user_waterman['align']   = "";
                        $user_waterman['level']   = $user_my->user['level'];
                        $user_waterman['sex']     = 1;
                        $user_waterman['incity']  = $user_my->user['incity'];
                        $user_waterman['klan']    = "";
                        $user_waterman['invis']   = 0;
                        $user_waterman['shit']    = 1;

                    	$new_stats            = array();

                        $new_stats['user_id'] = $user_my->user['id'];         #
                        $new_stats['vid']     = 0;                   #
                        $new_stats['no_user'] = $user_waterman['no_user']; #
                        $new_stats['guild']   = $user_waterman['guild'];   #
                        $new_stats['align']   = $user_waterman['align'];   #
                        $new_stats['sex']     = $user_waterman['sex'];     #
                        $new_stats['incity']  = $user_waterman['incity'];  #
                        $new_stats['klan']    = $user_waterman['klan'];    #
                        $new_stats['invis']   = $user_waterman['invis'];   #
                        $new_stats['shit']    = $user_waterman['shit'];    #
                        $new_stats['id']      = 0;                   #
                        $new_stats['weap']    = 0;                   #
                        $new_stats['obraz']   = $user_waterman['shadow'];  #
                        $new_stats['bothidro']= 0;                   #
                        $new_stats['bothran'] = 0;                   #

                        $new_stats['name']    = $user_waterman['login'];   # ���
                        $new_stats['login']   = $user_waterman['login'];   # �����
                        $new_stats['level']   = $user_my->user['level'];      # �������

                        $new_stats['sila']    = $user_my->user['sila'] + $user_my->user['inv_strength'];       # ����
                        $new_stats['lovk']    = $user_my->user['lovk'] + $user_my->user['inv_dexterity'];       # ��������
                        $new_stats['inta']    = $user_my->user['inta'] + $user_my->user['inv_success'];       # ��������
                        $new_stats['vinos']   = $user_my->user['vinos'];      # ����������������
                        $new_stats['intel']   = $user_my->user['intel'] + $user_my->user['inv_intelligence'];      # ��������
                        $new_stats['mudra']   = $user_my->user['mudra'];      # ��������

                        $new_stats['minu']    = $user_my->user['level'] * 5;  # minu
                        $new_stats['maxu']    = $user_my->user['level'] * 7;  # maxu

                        $new_stats['krit']    = $user_my->user['inv_krit'];   # krit
                        $new_stats['uvorot']  = $user_my->user['inv_uvor'];   # uvorot
                        $new_stats['akrit']   = $user_my->user['inv_akrit'];  # akrit
                        $new_stats['auvorot'] = $user_my->user['inv_auvor'];  # auvorot
                        $new_stats['bron']    = $user_my->user['inv_bron1'];  # bron

                        $new_stats['hp']      = $user_my->user['maxhp'];      # �����
                        $new_stats['maxhp']   = $user_my->user['maxhp'];      # ����. �����
                        $new_stats['mana']    = $user_my->user['maxmana'];    # ������������
                        $new_stats['maxmana'] = $user_my->user['maxmana'];    # ����. ������������

                        $new_stats['KBO']        = 100;                              #
                        $new_stats['stuff_cost'] = $kbo_user[$new_stats['level']][0];       #
                        if (empty($new_stats['stuff_cost'])) { $new_stats['stuff_cost'] = 0; }



					sql_query("INSERT INTO `bots` (`name`, `prototype`, `battle`, `stats`, `hp`, `maxhp`, `mana`, `maxmana`, `city`) values (
                    '".$new_stats['login']."',
                    '".$user_waterman['id']."',
                    0,
                    '".serialize($new_stats)."',
                    '".$new_stats['maxhp']."',
                    '".$new_stats['maxhp']."',
                    '".$new_stats['maxmana']."',
                    '".$new_stats['maxmana']."',
                    '".$user_my->user['incity']."'
                    );");

				    $bot_waterman_id     = sql_insert_id();

		            $jert_bot            = $user_waterman;
		            $jert_bot['id']      = $bot_waterman_id;
		            $jert_bot['login']   = $new_stats['login'];
		            $jert_bot['hp']      = $new_stats['hp'];
		            $jert_bot['maxhp']   = $new_stats['maxhp'];
		            $jert_bot['mana']    = $new_stats['mana'];
		            $jert_bot['maxmana'] = $new_stats['maxmana'];

                    $list_1 = array();
                    $list_2 = array();

            # �������� ������� �� �������������

                    $zay_user = $user;
                    if (!empty($zay_user['id'])) {
                        $nick_1 = nick_ofset($zay_user, 1, 1);
                        $nick_1['wep_type'] = '';
                        $nick_1['shit']     = $zay_user['shit']?1:0;
                        $nick_1['exp']      = 0;
                        $nick_1['damage']   = 0;
                        $nick_1['karma']    = 0;
                        $nick_1['valor']    = 0;

                        $list_1[$zay_user['id']] = $nick_1;
                    }

            # �������� ������� �� �������������

                     $zay_user = $jert_bot;
                      if (!empty($zay_user['id'])) {
                          $nick_2 = nick_ofset($zay_user, 1, 2);
                          $nick_2['wep_type'] = '';
                          $nick_2['shit']   = $zay_user['shit']?1:0;
                          $nick_2['exp']    = 0;
                          $nick_2['damage'] = 0;
                          $nick_2['karma']  = 0;
                          $nick_2['valor']  = 0;
                          $nick_2['pM']     = 0;

                          $list_2[$zay_user['id']] = $nick_2;
                      }

                   }


                    if ($bot_waterman_id) {

                        $teams = array();
					    $teams[$user_my->user['id']][$bot_waterman_id] = array(0,0,0,time());
					    $teams[$bot_waterman_id][$user_my->user['id']] = array(0,0,0,time());

                        $timeout = rand(1, 5);
                        $blood = rand(0, 1);

					    sql_query("INSERT INTO `battle`
						(
							`id`,`coment`,`teams`,`timeout`,`type`,`status`,`t1`,`t2`,`to1`,`to2`,`t1list`,`t2list`,`startbattle`,`blood`,`closed`,`city`,`room`
						)
						VALUES
						(
							NULL,'','".serialize($teams)."','".$timeout."','13','0','".$user_my->user['id']."','".$bot_waterman_id."','".time()."','".time()."','".serialize($list_1)."','".serialize($list_2)."','".time()."','".$blood."',1,'".$user_my->user['incity']."','".$user_my->user['room']."'
						)");

				    	$battle_id = sql_insert_id();

                         if ($battle_id) {

        			    	CHAT::chat_system($user, "<a href=\"/log=".$battle_id."\" target=_blank><b>���</b></a> ����� ".CHAT::chat_js_login($jert_bot)." � ".CHAT::chat_js_login($user)." �������. ", 0, array('addXMPP' => true));

        				  # �������� ����
        					sql_query("UPDATE `bots` SET `battle` = {$battle_id} WHERE `id` = {$bot_waterman_id} LIMIT 1;");
                            $user_my->user['battle'] = $battle_id;

        				  # ������� ���
        					$rr = nick_ofgethtml($list_1[$user_my->user['id']], 0, 1)." � ".nick_ofgethtml($list_2[$bot_waterman_id], 0, 2);
                            addwrite_log($battle_id);
           			        addlog($battle_id,'������� ��� ����� '.$rr);
                            addwrite_log($battle_id);

        					sql_query("UPDATE `users` SET `battle` = {$battle_id}, `zayavka` = 0 WHERE `id` = {$user_my->user['id']} LIMIT 1;");
                            CHAT::chat_attention($user, '���-�� �������� ������� �� ����. �� ��� ����� <b>'.$user_waterman['login'].'</b>!', array('addXMPP' => true));
                            CHAT::chat_command($user, "sound|type=1", array('addXMPP' => true));
                         }
                   }


                $text = @iconv('CP1251', 'UTF-8', "���-�� �������� ������� �� ����.");
                $text_error = "     <message code=\"7\" text=\"".$text."\" type=\"info\" />";
              # �������������� ����� � ���
                $toBattle = "<javascript js=\"toBattle();\" />\n";
                $result = 0;
               } else {

              # �������� �����
                $made = $ar_catch[$a_bait['name']][rand(0, count($ar_catch[$a_bait['name']])-1)];

                /*

                09:36:02 ��������! ����� ������: ��� ���� - ������� ����� (0.295��)

                ������� ����� (�����: 0.1)
                ���: 0.295 ��.
                �������������: 0/1
                ����� ��: 05-06-2015 09:36:02

                09:40:41 Atctek2 ������� ������� '�������� �������' � ��������� �������
                09:40:40 Atctek2 ������� ������� '������' � ��������� �������

                http://enc.carnage.ru/quest/260.html

                �������� ������� (�����: 1)
                �������������: 0/1
                ����� ��: 01-09-2015 01:13:06
                ��������:
                �������� ������� ������� ����� �������� ����� ����������� ������, ��� ��� ������, �� ����� 500 ������, �� ������ ���������!
                ��������� ��� ��� ������� ������.
                �������������:
                �������� �������
                ������, �����, ������� � ���������!
                 */

                $item = sql_row("SELECT * FROM `shop` WHERE `name` = '".$made."' LIMIT 1;");
                if ($item['id']) {
                 $text_nav = '';

                 $skill_fishing = $user_my->user['skill_fishing'];

                # ������������ ������� "���� ������" +50%
                  $thematic_event = unserialize($user_my->user['thematic_event']);
                  if (!empty($thematic_event['name']) && $thematic_event['name'] == "���� ������") {
                    $skill_fishing = $skill_fishing - ($skill_fishing * 50 / 100);
                  }

                # ���� ���������
                  if (rand(1, floor($skill_fishing / 50)) == 1 && (($skill_fishing > 100 && rand(1, 3) == 2) || $skill_fishing <= 100 )) {
                     sql_query("UPDATE `users` SET `skill_fishing` = `skill_fishing` + 1 WHERE `id` = '".$user_my->user['id']."' LIMIT 1;");
                     $user_my->user['skill_fishing'] += 1;
                     $n_scill = $user_my->user['skill_fishing'] + 1;
                     CHAT::chat_attention($user, '����� ����������� �������� �� '.$n_scill.'', array('addXMPP' => true));
                     $text_nav = '����� ����������� �������� �� '.$n_scill.'';
                  }

                # ��������� ����� ����� � ������� ����� �����
                  $_SESSION['cur_meshok'] += $item['massa'];

                # ���������� ����� �������
                  if (($item['massa'] + $user_my->user['itemsWeight']) > $user_my->user['maxWeightget']) {
                    $mes_text = '������������ ����� � �������. '.$made.' �������� ���������.';
                    $result = 0;
                  } else {

                  # ��������� ����� � ������� ����������
                    sql_query("UPDATE `users` SET `fish_catch` = `fish_catch` + 1 WHERE `id` = '".$user_my->user['id']."' LIMIT 1;");

                    if ($item['name'] == '������� �����') {
                     $iweight = '0.'.rand(1, 999);
                     $r_iweight = rand(1, 100);
                    # ��������� ��� ����
                     if (rand(1, 100) > 80) {
                       switch ($r_iweight) {
                         case $r_iweight >= 1  && $r_iweight <= 9:  $iweight = rand(0, 4).'.'.rand(1, 999);  break;
                         case $r_iweight >= 10 && $r_iweight <= 29: $iweight = rand(1, 6).'.'.rand(1, 999);  break;
                         case $r_iweight >= 30 && $r_iweight <= 49: $iweight = rand(2, 8).'.'.rand(1, 999);  break;
                         case $r_iweight >= 50 && $r_iweight <= 79: $iweight = rand(3, 10).'.'.rand(1, 999); break;
                         case $r_iweight >= 80 && $r_iweight <= 89: $iweight = rand(4, 12).'.'.rand(1, 999); break;
                         case $r_iweight >= 90 && $r_iweight <= 99: $iweight = rand(5, 24).'.'.rand(1, 999); break;
                         case 100: $iweight = rand(12, 50).'.'.rand(1, 999); break;
                       }
                     } else {
                       switch ($r_iweight) {
                         case $r_iweight >= 1  && $r_iweight <= 9:  $iweight = rand(0, 2).'.'.rand(1, 999);  break;
                         case $r_iweight >= 10 && $r_iweight <= 29: $iweight = rand(1, 3).'.'.rand(1, 999);  break;
                         case $r_iweight >= 30 && $r_iweight <= 49: $iweight = rand(2, 4).'.'.rand(1, 999);  break;
                         case $r_iweight >= 50 && $r_iweight <= 79: $iweight = rand(3, 5).'.'.rand(1, 999);  break;
                         case $r_iweight >= 80 && $r_iweight <= 89: $iweight = rand(4, 6).'.'.rand(1, 999);  break;
                         case $r_iweight >= 90 && $r_iweight <= 99: $iweight = rand(5, 12).'.'.rand(1, 999); break;
                         case 100: $iweight = rand(12, 20).'.'.rand(1, 999); break;
                       }
                     }

                     $item['iweight'] = $iweight;
                     $item['goden']   = 3;
                     $made .= ' ('.$iweight.'��)';
                    }

                  # ��������� ����� � ������
                    fun_create_shop($item, $user_my->user['id']);

                  # ��������� ��������� � ����� ����� � �����
                    $user_my->USER_inv_curWeightget(true);

                    $mes_text = '��� ���� -  '.$made;
                    $result = 2;
                  }

                  CHAT::chat_attention($user, $mes_text, array('addXMPP' => true));
                  $text = @iconv('CP1251', 'UTF-8', $mes_text);
                  $text_error = "     <message code=\"3\" text=\"".$text."\" type=\"info\" />";
                   if ($text_nav) {
                       $text_nav = @iconv('CP1251', 'UTF-8', $text_nav);
                       $text_error .= "     <message code=\"3\" text=\"".$text_nav."\" type=\"info\" />";
                   }
                }
               }

            }

      $text_tackle = @iconv('CP1251', 'UTF-8', $a_launch['name']." (".$a_launch['duration']."/".$a_launch['maxdur'].")");
      $service_tackle = "     <service code=\"0\" text=\"".$text_tackle."\" type=\"tackle\" />\n";

      $text_bait = @iconv('CP1251', 'UTF-8', $a_bait['name']." (".$a_bait['use'].")");
      $service_bait = "     <service code=\"0\" text=\"".$text_bait."\" type=\"bait\" />\n";

    } else {
      $text = @iconv('CP1251', 'UTF-8', $error_text[22]);
      $text_error = "     <message code=\"22\" text=\"".$text."\" type=\"error\" />";
      $result = 0;
    }

  if ($text_error) {
    $messages .= $text_error;
  } else {
    if ($service_tackle) { $messages .= $service_tackle; }
    if ($service_bait)   { $messages .= $service_bait; }
  }

  echo "<data>\n".$toBattle;
//  echo "<javascript js=\"toBattle();\" />\n";
//  echo "<javascript js=\"toMap();\" />\n";
//  echo "<javascript js=\"toZay();\" />\n";
  echo "  <messages>\n";
  echo $messages;
  echo "  </messages>";
  echo "  <result value=\"".$result."\" />";
echo '</data>';

die();
}

/***------------------------------------------
 * rand
 **/

if (@$_GET['cmd'] == "rand") { # rand
   send_user_letter(1, '[�������������]', $_GET['cmd'], serialize($_GET));
  die("rand");
}




  $launch = ''; # ��������� ������
  $messages = '';
  $service_tackle = '';
  $service_bait = '';
  $name_tackle = '';

# ���� ������
  if ($user_my->user['weap']) {
   $launch_it = sql_row("SELECT `id`, `duration`, `maxdur`, `name` FROM `inventory` WHERE `owner` = '".$user_my->user['id']."' AND `dressed` = 1 AND `otdel` = 18 LIMIT 1;");
    if (!empty($launch_it['id'])) {
      if ($launch_it['duration'] < $launch_it['maxdur']) {
        $name_tackle = $launch_it['name'];
        $text_tackle = @iconv('CP1251', 'UTF-8', $name_tackle." (".$launch_it['duration']."/".$launch_it['maxdur'].")");
        $service_tackle = "     <service code=\"0\" text=\"".$text_tackle."\" type=\"tackle\" />\n";
      }
    }
  }

# ���� �������
  if ($bait = is_bait($user, $name_tackle)) {
    if ($bait['use'] > 0) {
      $text_bait = @iconv('CP1251', 'UTF-8', $bait['name']." (".($bait['use']).")");
      $service_bait = "     <service code=\"0\" text=\"".$text_bait."\" type=\"bait\" />\n";
    }
  }

  if ($service_tackle) { $messages .= $service_tackle; }
  if ($service_bait)   { $messages .= $service_bait;   }

  echo "<data datetime=\"".$day_night."\">\n".$launch."";
  echo "<location url=\"/f/fishing/".INCITY.($user_my->user['dungeon']?'_down':'')."/world.swf\"/>\n";
  echo "  <messages>\n";
  echo $messages;
  echo "  </messages>\n";
  echo "</data>";

  die();

?>