<?php

 include_once("functions/jewel/FieldWayFinder.php");

class Game {

	private static $matrix          = array(); // ����� � ������; (y, x)
	private static $new_matrix      = array(); // ����� � ������ ���������; (x, y)

	private static $my_clear_matrix = array(); // ����� � ����������� ���� ��������� (x, y)

	private static $symbols         = 6; // ������� �������
	private static $DIMENSION       = 7; // ���� �� �������

# ������� ������������ ��������

	private static $array1          = array(array(-2, 0), array(-1, -1), array(-1, 1), array(2, -1), array(2, 1), array(3, 0));
	private static $array2          = array(array(1, -1), array(1, 1));
	private static $array3          = array(array(0, -2), array(-1, -1), array(1, -1), array(-1, 2), array(1, 2), array(0, 3));
	private static $array4          = array(array(-1, 1), array(1, 1));


	public static $jewels           = array(); //
	private static $closeJewels     = Array(); //
	private static $DY              = 37; //
	private static $DX              = 37; //
	private static $gameEnded       = false; //
	private static $crashPoints     = 0; //


    public static function getJewel($arg1, $arg2)
        {
            if (self::$jewels == null)
            {
                return null;
            }
            if (self::$jewels[$arg1] == null || self::$jewels[$arg1] == "")
            {
                return null;
            }
            if (self::$jewels[$arg1][$arg2] == null || self::$jewels[$arg1][$arg2] == "")
            {
                return null;
            }

            $jewel['type']    = self::$jewels[$arg1][$arg2]['type'];
            $jewel['wasHere'] = false;
            $jewel['hided']   = true;
            $jewel['I']       = $arg1;
            $jewel['J']       = $arg2;

            return $jewel;
        }

    public static function refreshMatrix()
        {
            $loc1 = 0;
            $loc2 = 0;
            while ($loc2 < self::$DIMENSION)
            {
                $loc1 = 0;
                while ($loc1 < self::$DIMENSION)
                {
                    if (self::getJewel($loc2, $loc1) != null)
                    {
                        unset(self::$jewels[$loc2][$loc1]);   // �������
                       // self::getJewel($loc2, $loc1).animateHide();

                    }
                    ++$loc1;
                }
                ++$loc2;
            }
           // self::dataLoader.load();
           // ��������� �����
            return;
        }
        public static function clearJewel($arg1, $arg2)
        {
            $loc1 = self::getJewel($arg1, $arg2);
            self::$jewels[$loc1["I"]][$loc1["J"]] = null;

            return;
        }
        public static function hidecloseJewels()
        {
            $loc4 = "";
            $loc1 = null;
            $columnDownPoints = Array();
            $loc2 = false;
           // $closeJewels = SorterByJ.sort(closeJewels);
            $loc3 = 0;
            while ($loc3 < count(self::$closeJewels))
            {
                $loc1 = self::$closeJewels[$loc3];
                if ($loc1.hided == false)
                {
                    self::clearJewel($loc1["I"], $loc1["J"]);
                    $loc4 = $crashPoints + 1;
                    $crashPoints = $loc4;
                    $columnDown($loc1.I, $loc1.J);

                    $loc2 = true;
                }
                $loc1.type = -1;
                ++$loc3;
            }
            return;
        }

        public static function collapse()
        {
            $loc1 = 0;
            $crashPoints = 0;
            $closeJewels = Array();
            $loc2 = false;
            $loc3 = 0;
            while ($loc3 < self::$DIMENSION)
            {
                $loc1 = (self::$DIMENSION - 1);
                while ($loc1 >= 0)
                {
                    if (self::selfCollapseOne(self::getJewel($loc3, $loc1)))
                    {
						$loc2 = true;
                    }
                    --$loc1;
                }
                ++$loc3;
            }
        // �������� ����������
          self::hidecloseJewels();
          return $loc2;
        }

     public static function selfCollapseOne($arg1 = array())
        {
            if ($arg1 == null)
            {
                return false;
            }
			// ���� �� ����������� ��� �������
            self::$closeJewels[] = (FieldWayFinder::checkPointHor($arg1["I"], $arg1["J"]));
			// ���� �� ���������� ��� �������
            self::$closeJewels[] = (FieldWayFinder::checkPointVer($arg1["I"], $arg1["J"]));

            $loc1 = false;
            if (count(self::$closeJewels) != 0)
            {
                $loc1 = true;
            }
            return $loc1;
        }

    public static function addJewel($arg1, $arg2, $arg3)
        {
            $jewel = "";

            $jewel['type']    = @$arg1;
            $jewel['wasHere'] = false;
            $jewel['hided']   = true;
            $jewel['I']       = $arg2;// * self::$DX;
            $jewel['J']       = $arg3;// * self::$DY;

           //($jewel = new ViewJewel(Math.floor($arg1), $arg2, $arg3)).x = $arg2 * self::$DX;
            self::$jewels[$arg2][$arg3] = $jewel;
            return;
        }

    public static function sendMove($arg1, $arg2, $arg3, $arg4)
        {
           // moveLoader.load($arg1, $arg2, $arg3, $arg4);
            return;
        }

    public static function sendMove_change($arg1, $arg2)
        {
            self::$jewels[$arg1["I"]][$arg1["J"]] = $arg2;
            self::$jewels[$arg2["I"]][$arg2["J"]] = $arg1;
			return;
        }

    public static function loadField($arg1 = Array())
        {
            $loc1   = 0;
            $jewels = Array();
            $loc2   = 0;
            while ($loc2 < count($arg1))
            {
                $jewels[$loc2] = Array();
                $loc1 = 0;
                while ($loc1 < count($arg1[$loc2]))
                {
                    self::addJewel($arg1[$loc2][$loc1], $loc2, $loc1);
                    ++$loc1;
                }
                ++$loc2;
            }
            return;
        }



















/***------------------------------------------
 * ����� ������
 **/

	public static function init() {
		while(true) {
			self::matrix();

			if(count(self::matches()) != 0) continue;
			if(self::moves()) break;
		}
	}

	private static function matrix() {
		$matrix = array();

		for($y = 0; $y < self::$DIMENSION; $y++) {
			for($x = 0; $x < self::$DIMENSION; $x++) {
				$matrix[$y][$x] = mt_rand(1, self::$symbols);
			}
		}

		self::$matrix =  $matrix;
	}

	private static function matches() {
		$matches = array();

		for($y = 0; $y < self::$DIMENSION; $y++) {
			for($x = 0; $x < self::$DIMENSION - 2; $x++) {
				$match = self::horizontal($x, $y);
				$n = count($match);

				if($n > 2) {
					$matches[] = $match;
					$x += $n - 1;
				}
			}
		}

		for($x = 0; $x < self::$DIMENSION; $x++) {
			for($y = 0; $y < self::$DIMENSION - 2; $y++) {
				$match = self::vertical($x, $y);
				$n = count($match);

				if($n > 2) {
					$matches[] = $match;
					$y += $n - 1;
				}
			}
		}

		return $matches;
	}

	private static function horizontal( $x, $y ) {
		$match[] = array($y, $x);

		for($i = 1; $x + $i < self::$DIMENSION; $i++) {
			if(self::$matrix[$y][$x] == self::$matrix[$y][$x + $i]) $match[] = array($y, $x + $i);
			else return $match;
		}

		return $match;
	}

	private static function vertical( $x, $y ) {
		$match[] = array($y, $x);

		for($i = 1; $y + $i < self::$DIMENSION; $i++) {
			if(self::$matrix[$y][$x] == self::$matrix[$y + $i][$x]) $match[] = array($y + $i, $x);
			else return $match;
		}

		return $match;
	}

	private static function moves() {
		for($i = 0; $i < self::$DIMENSION; $i++) {
			for($j = 0; $j < self::$DIMENSION; $j++) {
				if(self::m_patern($i, $j, array(array(1, 0)), self::$array1)) return true;
				if(self::m_patern($i, $j, array(array(2, 0)), self::$array2)) return true;
				if(self::m_patern($i, $j, array(array(0, 1)), self::$array3)) return true;
				if(self::m_patern($i, $j, array(array(0, 2)), self::$array4)) return true;
			}
		}
		return false;
	}

	public static function set_move( $d_x, $d_y, $m_x, $m_y ) {

		self::m_swap($d_x, $d_y, $m_x, $m_y);

		if(count(self::matches()) == 0) {
			self::m_swap($d_x, $d_y, $m_x, $m_y);

			return false;
		} else return true;
	}

	private static function remove( $matches, $is_my = false ) {

		for($i = 0; $i < count($matches); $i++) {
			for($j = 0; $j < count($matches[$i]); $j++) {
				$m1 = $matches[$i][$j][0];
				$m2 = $matches[$i][$j][1];

                if ($is_my) {
                  self::$my_clear_matrix[] = ($m1+0).'_'.($m2+0);
                }

				self::$matrix[$m1][$m2] = -1;

				self::above($m1, $m2);
			}
		}

		self::add();

		if(count($matches) == 0)
			if(!self::moves()) return false;

		return true;
	}

	private static function add() {
		$objects = self::$new_matrix;

		for($i = 0; $i < self::$DIMENSION; $i++) {
			for($j = self::$DIMENSION - 1; $j >= 0; $j--) {
				if(self::$matrix[$j][$i] == -1) {
					self::$matrix[$j][$i] = mt_rand(1, self::$symbols);

					if(@$objects[$i][0] != '') {

						$objects[$i][] = self::$matrix[$j][$i];
					} else $objects[$i][0] = self::$matrix[$j][$i];

				}
			}
		}

		self::$new_matrix = $objects;
	}

# ����������� �������
	public static function matix_revers($matrix) {
		$matr = array();
		for($i = 0; $i < self::$DIMENSION; $i++) {
			for($j = 0; $j < self::$DIMENSION; $j++) {
			  if (!empty(self::$matrix[$j][$i])) {
                $matr[$i][$j] = self::$matrix[$j][$i];
			  }
			}
		}
		return $matr;
	}

# �������� ���� �������
	private static function above( $x, $_y ) {
		for($y = $x - 1; $y >= 0; $y--) {
			if(self::$matrix[$y][$_y] != -1) {
				self::$matrix[$y + 1][$_y] = self::$matrix[$y][$_y];
				self::$matrix[$y][$_y]     = -1;
			}
		}
	}

	private static function m_swap( $x1, $y1, $x2, $y2 ) {
		$symbol = self::$matrix[$y1][$x1];
		self::$matrix[$y1][$x1] = self::$matrix[$y2][$x2];
		self::$matrix[$y2][$x2] = $symbol;
	}

	private static function m_patern( $x, $y, $must, $need ) {
		$symbol = self::$matrix[$y][$x];

		for($i = 0; $i < count($must); $i++) if(!self::m_type($x + $must[$i][0], $y + $must[$i][1], $symbol)) return false;
		for($i = 0; $i < count($need); $i++) if(self::m_type($x + $need[$i][0], $y + $need[$i][1], $symbol)) return true;

		return false;
	}

	private static function m_type( $x, $y, $symbol ) {
		if(($x < 0) || ($x > self::$DIMENSION) || ($y < 0) || ($y > self::$DIMENSION)) return false;

		return (self::$matrix[$y][$x] == $symbol);
	}

	public static function step() {
		$matches = self::matches();

		self::remove($matches, true);

		while($matches = self::matches()) self::remove($matches, false);

		return true;
	}

	public static function new_matrix() {
		if(count(self::$new_matrix)) return self::$new_matrix;
		else return false;
	}

	public static function setMatrix( $matrix ) {
		self::$matrix = $matrix;
	}

	public static function getMatrix() {
		return self::$matrix;
	}

	public static function getMy_clear_matrix() {
		return self::$my_clear_matrix;
	}


# ����� �������

	public static function get_result_matrix() {
      $draw_matrix     = "";
      $matrix          = self::$matrix;
      $new_matrix      = self::$new_matrix;
      $my_clear_matrix = self::$my_clear_matrix;
      $my_matrix       = array();


       $hhnew_matrix = self::matix_revers($new_matrix);



          foreach($new_matrix as $k => $objects) {

            foreach($objects as $y => $symbol) {
              $n_y = $y + 1;
              $my_matrix[0][] = $n_y.'-'.$symbol;
            }


          }


        foreach($my_matrix as $x => $objects) {
         $n_x = $x + 1;

         $draw_matrix .= "<col x=\"" . $n_x . "\"  xxx=''>\n";
          foreach($objects as $y => $symbol) {
            $n_y = $y + 1;

            $m_xy_s = explode("-", $symbol);

            $draw_matrix .= "   <el y=\"" . ($m_xy_s[0]) . "\" type=\"".$m_xy_s[1]."\" type1=\"".$m_xy_s[1]."\" />\n";

          }
         $draw_matrix .= "</col>\n";
        }


       /*
       $new_matrix = self::matix_revers($new_matrix);*/

        foreach($new_matrix as $x => $objects) {
         $n_x = $x + 1;

         $draw_matrix .= "<col x=\"" . $n_x . "\">\n";
          foreach($objects as $y => $symbol) {
            $n_y = $y +1;
            $draw_matrix .= "   <el y=\"" . $n_y . "\" type=\"".$symbol."\" />\n";
          }
         $draw_matrix .= "</col>\n";
        }

	   return $draw_matrix;
	}

}

?>