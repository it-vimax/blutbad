<?php

 include_once("functions/jewel/FieldCollapser.php");
 include_once("functions/jewel/FieldWayFinder.php");

class ViewField {

	private $jewels;

	private $count = 0;
	private $DIMENSION = 7;
	private $crashPoints = 0;
	private $gameEnded = fa;

   	private $collapserCrash;
   	private $vfield = array();
   //	private $wayFinder = 7; FieldWayFinder

	public function __construct( $arg1 ) {
		/*$this->closeJewels = [];
		$this->columnDownPoints = [];

		$this->wayFinder = new FieldWayFinder($arg1);*/
		$this->vfield = $arg1;
		$this->collapserCrash = new FieldCollapser($this);
	}

    public function getJewel($arg1, $arg2)
        {
            if (empty($this->jewels) || $this->jewels == null)
            {
                return null;
            }
            if (empty($this->jewels[$arg1]) || $this->jewels[$arg1] == null || $this->jewels[$arg1] == "")
            {
                return null;
            }
            if (empty($this->jewels[$arg1][$arg2]) || $this->jewels[$arg1][$arg2] == null || $this->jewels[$arg1][$arg2] == "")
            {
                return null;
            }

            $jewel['type']    = $this->jewels[$arg1][$arg2]['type'];
            $jewel['wasHere'] = false;
            $jewel['hided']   = false;
            $jewel['I']       = $arg1;
            $jewel['J']       = $arg2;

            return $jewel;
        }


	public function setJewel( $arg1, $arg2, $type, $val ) {
            $Jewel = $this->getJewel($arg1, $arg2);
              if ($Jewel) {
                  $Jewel[$type] = $val;
                  return $this->jewels[$arg1][$arg2] = $Jewel;
              }
            return false;
	}

    public function addJewel($arg1, $arg2, $arg3)
        {
            $jewel            = array();
            $jewel['type']    = $arg1;
            $jewel['wasHere'] = false;
            $jewel['hided']   = false;
            $jewel['I']       = $arg2;
            $jewel['J']       = $arg3;

            $this->jewels[$arg2][$arg3] = $jewel;
            return true;
        }

    public function clearJewel($arg1, $arg2)
    {
        $loc1 = $this->getJewel($arg1, $arg2);
        $this->jewels[$arg1][$arg2] = null;

        if ($loc1 != null)
        {
            $this->setJewel($arg1, $arg2, "hided", true);
            return true;
        }

        return false;
    }

    public function placeJewelDelay($arg1, $arg2, $arg3)
    {
        $this->jewels[$arg2][$arg3] = $arg1;
        $arg_1 = $arg1;
       // echo serialize($arg1)."<br>";

       if ($arg1 == null) {
        return false;
       }

        $this->jewels[$arg1["I"]][$arg1["J"]] = null;

       // self::clear_closeJewels($arg1["I"], $arg1["J"]);

       // unset(self::jewels[$arg2][$arg3]);
       // echo "placeJewelDelay ".$arg2."===".$arg3."<br>";

       // echo "---------------------<br>";
       // echo $arg1["I"]."-".$arg1["J"]."-".$arg1["type"]."<br>";

       // $arg1 = $arg_1;
        $arg1["I"] = $arg2;
        $arg1["J"] = $arg3;
        $this->jewels[$arg1["I"]][$arg1["J"]] = $arg1;

       // echo $arg2."-".$arg3."<br>";
       // echo $arg1["I"]."-".$arg1["J"]."-".$arg1["type"]."<br>";
       // echo "---------------------<br>";

        //return false;
       // $arg1.moveToXYDelay($arg2 * $DX, $arg3 * $DY);
       return true;
    }


    public function loadField($arg1 = array())
        {
            $loc1   = 0;
            $this->jewels = array();
            $loc2   = 0;
            while ($loc2 < count($arg1)) {
                $this->jewels[$loc2] = array();
                $loc1 = 0;
                while ($loc1 < count($arg1[$loc2])) {
                    $this->addJewel($arg1[$loc2][$loc1], $loc2, $loc1);
                    ++$loc1;
                }
                ++$loc2;
            }

            while ($this->collapserCrash->collapse())
            {
                $this->collapserCrash->collapse();
            }

            return true;
        }
}

?>