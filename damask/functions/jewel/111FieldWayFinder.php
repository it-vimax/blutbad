<?

        function clearJewel($arg1, $arg2)
        {
            $loc1 = getJewel($arg1, $arg2);
            $jewels[$loc1.I][$loc1.J] = null;
            return;
        }

        function compareJewels($arg1, $arg2)
        {
            return $arg1["J"] <= $arg2["J"];
        }

        function SorterByJ_sort($arg1)
        {
            return STDSort.sortObjects(arg1, compareJewels);
        }

        function hidecloseJewels()
        {
            $loc4 = "";
            $loc1 = null;
            $columnDownPoints = Array();
            $loc2  = false;
          // $closeJewels = SorterByJ_sort($closeJewels);
            $loc3 = 0;
            while ($loc3 < count($closeJewels))
            {
                $loc1 = $closeJewels[$loc3];
                if ($loc1['hided'] == false)
                {
                    $vfield.clearJewel($loc1.I, $loc1.J);
                    $loc4 = $crashPoints + 1;
                    $crashPoints = $loc4;
                    $columnDown($loc1.I, $loc1.J);

                    $loc2 = true;
                }
                $loc1['type'] = -1;
                ++$loc3;
            }
          return;
        }

class FieldWayFinder {

	private static $vfield          = array(); //
	private static $count           = 0; //

        /*public static function FieldWayFinder($arg1)
        {
            self::$vfield = $arg1;
            return;
        }*/

        public static function checkPointCHor($arg1, $arg2, $arg3, $arg4 = Array())
        {
            if (Game::getJewel($arg1, $arg2) == null)
            {
                return;
            }
            $loc1 = Game::getJewel($arg1, $arg2);
            if (!($loc1['type'] == $arg3) || $loc1['wasHere'])
            {
				return;
            }

            array_push($arg4, $loc1);

            $loc1['wasHere'] = true;
            $loc2  = $count + 1;
            $count = $loc2;
            self::checkPointCHor($arg1 + 1, $arg2, $arg3, $arg4);
            self::checkPointCHor(($arg1 - 1), $arg2, $arg3, $arg4);
            $loc1['wasHere'] = false;
            return $count;
        }

        public static function checkPointHor($arg1, $arg2)
        {
            $loc1  = 0;
            $count = 0;
            $loc2  = Array();
            if ( Game::getJewel($arg1, $arg2) == null)
            {
                return $loc2;
            }
            $loc1 = Game::getJewel($arg1, $arg2);
            $count = self::checkPointCHor($arg1, $arg2, $loc1["type"], $loc2);
            if ($count <= 2) // ����� 3 �� �����������
            {
                $loc2 = Array();
            }
            return $loc2;
        }

       public static function checkPointCVer($arg1, $arg2, $arg3, $arg4 = Array())
        {
            if (Game::getJewel($arg1, $arg2) == null)
            {
                return;
            }
            $loc1 = Game::getJewel($arg1, $arg2);
            if (!($loc1['type'] == $arg3) || $loc1['wasHere'])
            {
                return;
            }

            array_push($arg4, $loc1);

            $loc1["wasHere"] = true;
            $loc2 = $count + 1;
            $count = $loc2;
            self::checkPointCVer($arg1, $arg2 + 1, $arg3, $arg4);
            self::checkPointCVer($arg1, ($arg2 - 1), $arg3, $arg4);
            $loc1["wasHere"] = false;
            return;
        }

        public static function checkPointVer($arg1, $arg2)
        {
            $loc1 = 0;
            $count = 0;
            $loc2 = Array();
            if (Game::getJewel($arg1, $arg2) == null)
            {
                return $loc2;
            }
            $loc1 = Game::getJewel($arg1, $arg2);
            self::checkPointCVer($arg1, $arg2, $loc1["type"], $loc2);
            if ($count <= 2) // ����� 3 �� ���������
            {
                $loc2 = Array();
            }
            return $loc2;
        }

        public static function getJewel($arg1, $arg2)
        {
            return Game::getJewel($arg1, $arg2);
           // return $vfield.getJewel($arg1, $arg2);
        }
}
?>