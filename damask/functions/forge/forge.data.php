<?

   $matr_it = array(
    "����� �� 3-� ������� ������"  => 3,
    "����� �� 5-� ������� ������"  => 5,
    "����� �� 7-� ������� ������"  => 7,
    "����� �� 9-� ������� ������"  => 9,
    "����� �� 21-� ������� ������" => 21,

    "�������� ��������"  => 1,
    "�������� ���������" => 2,
    "�������� ������"    => 3,
    "�������� ��������"  => 4,
    "�������� �������"   => 5,
    "�������� ������"    => 6,

    "������ ��������� +100" => 1,
    "������ ��������� +250" => 2,
    "������ ������� ��������� +500" => 3,

    "������ ������������� +100" => 4,
    "������ ������������� +250" => 5,
   );

   $a_recipe = array(
    1 => array('name' => "������ ��������� +100", 'img' => "recipe", 'num' => '1', 'time' => 500, 'craft' => '0', 'money' => '10'),
    2 => array('name' => "������ ��������� +250", 'img' => "recipe", 'num' => '2', 'time' => 1500, 'craft' => '50', 'money' => '25'),
    3 => array('name' => "������ ������� ��������� +500", 'img' => "recipe", 'num' => '3', 'time' => 3000, 'craft' => '100', 'money' => '50'),

    4 => array('name' => "������ ������������� +100", 'img' => "recipe", 'num' => '4', 'time' => 500, 'craft' => '100', 'money' => '10'),
    5 => array('name' => "������ ������������� +250", 'img' => "recipe", 'num' => '5', 'time' => 1500, 'craft' => '150', 'money' => '25'),
   );

   $a_organic = array(
    3  => array('name' => "����� �� 3-� ������� ������", 'img' => "organic", 'num' => '3', 'time' => 3600, 'craft' => '0'),
    5  => array('name' => "����� �� 5-� ������� ������", 'img' => "organic", 'num' => '5', 'time' => 7200, 'craft' => '20'),
    7  => array('name' => "����� �� 7-� ������� ������", 'img' => "organic", 'num' => '7', 'time' => 10800, 'craft' => '70'),
    9  => array('name' => "����� �� 9-� ������� ������", 'img' => "organic", 'num' => '9', 'time' => 14400, 'craft' => '150'),
    21 => array('name' => "����� �� 21-� ������� ������", 'img' => "organic", 'num' => '21', 'time' => 22000, 'craft' => '300'),
   );

   $a_crystal = array(
    1 => array('name' => "�������� ��������", 'img' => "crystal", 'num' => '1', 'time' => 1000, 'craft' => '0'),
    2 => array('name' => "�������� ���������", 'img' => "crystal", 'num' => '2', 'time' => 2000, 'craft' => '0'),
    3 => array('name' => "�������� ������", 'img' => "crystal", 'num' => '3', 'time' => 3000, 'craft' => '0'),
    4 => array('name' => "�������� ��������", 'img' => "crystal", 'num' => '4', 'time' => 4000, 'craft' => '0'),
    5 => array('name' => "�������� �������", 'img' => "crystal", 'num' => '5', 'time' => 5000, 'craft' => '0'),
    6 => array('name' => "�������� ������", 'img' => "crystal", 'num' => '6', 'time' => 6000, 'craft' => '0'),
   );
?>