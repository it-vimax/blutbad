<?

 # ������� ������ � ��������
   defined('_CRON') or die();
   $debug_test_s = "";
   $debug_test_s .= "equalizer<br>";

   $debug_test = false;
   $intervene_log = true;

 # ���� �������� "�������� ������������"
   $file_equalizer = 'adata/bot_equalizer.txt';

 # ��������� ���������
   $file_save = false;

   define ("MAX_VALOR", 25); # ������������ ���������� ��������
   define ("EXITTIME_BATTLE", 500); # 500 ���. �������� �� ��� �����
   define ("SHANCE_BATTLE", 35); # ���� ��������� � ���

 # �������� ��������
   if (is_writable($file_equalizer)) {
     $data_equalizer = unserialize(file_get_contents($file_equalizer)); # ���������
   } else {
     $data_equalizer = array(
          11 => array(
                      'id'             => 0,
                      'login'          => "Equalizer eleven",
                      'level'          => 11,
                      'battle'         => 0,           # �� ���
                      'no_battles'     => "",          # ��� ���� � ������� �� ����� �����������
                      'intervene_time' => 0,           # ���� ������ � ���
                      'intervene_battle' => 0,         # ��������� �� ���
                      'battle_rune'    => true,        # ������������� ����
                      'incity'         => INCITY,      # �����
                ),
          12 => array(
                      'id'             => 0,
                      'login'          => "Equalizer twelve",
                      'level'          => 12,
                      'battle'         => 0,
                      'no_battles'     => "",
                      'intervene_time' => 0,
                      'intervene_battle' => 0,         # ��������� �� ���
                      'battle_rune'    => true,        # ������������� ����
                      'incity'         => INCITY,
                ),
          13 => array(
                      'id'             => 0,
                      'login'          => "Equalizer thirteen",
                      'level'          => 13,
                      'battle'         => 0,
                      'no_battles'     => "",
                      'intervene_time' => 0,
                      'intervene_battle' => 0,         # ��������� �� ���
                      'battle_rune'    => true,        # ������������� ����
                      'incity'         => INCITY,
                ),
     );
     $file_save = true;
   }


   /***------------------------------------------
    * ������ �� ���� ����� ���������
    **/

   function user_interference($d1, $d2, $no_id) {
       $us = false;

       if (isset($d1[$no_id])) $d = $d2; else $d = $d1;

       foreach ($d as $k => $v) {
        if ($v['h'] > 0 && $v['i'] == 0) {
          if ($v['id'] > _BOTSEPARATOR_) {
            $us = sql_row("SELECT * FROM `bots` WHERE `id` = '".$v['id']."' LIMIT 1;");
            $us['login'] = $us['name'];
          } else {
            $us = sql_row("SELECT * FROM `users` WHERE `id` = '".$v['id']."' LIMIT 1;");
          }

          break;
        }
       }
       return $us;
   }

   if ($debug_test) {
     //bprint($data_equalizer);
   }

   // ���� ��� � ����������� � ���� "����������� ��� � ���������"
   $batls = sql_query("SELECT id, teams, statis, t1, t2, t1list, t2list, room FROM `battle` WHERE (`type` = 5 OR `type` = 20) AND `win` = 3 AND `closed` = 0 AND `city` = '".INCITY."';");
   while ($row = fetch_array($batls)) {
     # $upd_battle = unserialize($row['teams']);
     # bprint($upd_battle);

   # ������ ������ �������
     $t1 = explode(";", $row['t1']);

     $t1list = unserialize($row['t1list']);
     $t2list = unserialize($row['t2list']);

     $tlist = array_merge($t1list, $t2list);

     // ������ ������ ��������� ���
     foreach ($tlist as $k => $t_us) {

         // ���������� ���� ������ 10-�� ������
         if ($t_us['l'] < 10) {
           continue;
         }

         if ($debug_test) {
           if ($t_us['valor'] > 0.001) {
                $debug_test_s .= "===> bid=".$row['id'].", valor=".$t_us['valor'].", level=".$t_us['l'].", login=".$t_us['n']."<br>";
           }
         }

       // ��� ��������� ������� ������� ����� 500 ��������
       if (floor($t_us['valor']) >= MAX_VALOR) {

         $eq_t_us = $data_equalizer[$t_us['l']];

         //----------------------------------
         // �������� ����������� "equalizer"
         if (isset($eq_t_us)) {

              // ���������� ���� ��� � ������ "no_battles"
              if (substr_count($eq_t_us['no_battles'], $row['id'])) {
                continue;
              }

              if ($debug_test) {
                $debug_test_s .= "===> �������� ����������� equalizer OK<br>";
              }

              //----------------
              // � ������ � 75%
              if (rand(1, 100) > SHANCE_BATTLE) {



                // �������� ��� "equalizer" ������� � ���
                if (isset($eq_t_us) && $eq_t_us['battle'] != 0 && time() > $eq_t_us['intervene_time'] + EXITTIME_BATTLE) {

                  // ���������� � ��� "equalizer"
                  $old_battle_equalizer = sql_row("SELECT `id` FROM `battle` WHERE `id` = '".$eq_t_us['battle']."' AND `win` != 3 LIMIT 1;");
                  if (isset($old_battle_equalizer['id'])) {
                    $eq_t_us['id']         = 0;
                    $eq_t_us['battle']     = 0;
                    $eq_t_us['no_battles'] = "";

                    $data_equalizer[$t_us['l']] = $eq_t_us;
                    $file_save = true;
                    if ($debug_test) {
                      $debug_test_s .= "===> ���������� � ��� equalizer OK<br>";
                    }
                    continue;
                  }

                } # end if


              //----------------
              // �������� � ���
                if (isset($eq_t_us) && $eq_t_us['battle'] != 0) {

                    if ($eq_t_us['battle_rune'] == true) {

                      // ���� ������ "equalizer" � ���
                      if (in_array ($eq_t_us['id'], $t1)) {
                        $eq_hilbot = $t1list[$eq_t_us['id']];
                        $ttt = 1;
                      } else {
                        $eq_hilbot = $t2list[$eq_t_us['id']];
                        $ttt = 2;
                      }

                      // ������
                      if (isset($eq_hilbot)) {
                        if (rand(1, 100) > 70) {
                          if ($eq_hilbot['mh'] - $eq_hilbot['h'] > floor($eq_hilbot['mh'] * 0.1)) {

                              $hil = array(250, 500, 1000);
                              $hpadd = $hil[rand(0, count($hil) - 1)];
                              $hp = $eq_hilbot['h'] + $hpadd;
                              if ($hp > $eq_hilbot['mh']) $hp = $eq_hilbot['mh'];
                              $eq_hilbot['h'] = $hp;

                              // ������ ������ "equalizer" � ��� � ��������� SQL ������
                              if (in_array ($eq_t_us['id'], $t1)) {
                                $t1list[$eq_t_us['id']] = $eq_hilbot;
                                $hil_tlist = ", `t1list` = '".serialize($t1list)."'";
                              } else {
                                $t2list[$eq_t_us['id']] = $eq_hilbot;
                                $hil_tlist = ", `t2list` = '".serialize($t2list)."'";
                              }

                              // ��������� ���
               		          sql_query("UPDATE `battle` SET `to1` = '".time()."', `to2` = '".time()."' ".$hil_tlist." WHERE `id` = '".$row['id']."' LIMIT 1;");
                              // ��������� ����
               		          sql_query("UPDATE `bots` SET `hp` = '".$hp."' WHERE `id` = '".$eq_t_us['id']."' LIMIT 1;");

                              $eq_t_us['invis'] = 1;

                            # ����� � ���
                 			  addlog($row['id'], nick_team($eq_t_us, "B".$ttt).' ����������� ������� ����� <b class="bplus">+??</b> [??/??]');
                              addwrite_log($row['id']);

                              if ($debug_test) {
                                $debug_test_s .= "===> ��� � ��� �� +".$hpadd." hp equalizer OK<br>";
                              }

                          } else {
                            if ($debug_test) {
                              $debug_test_s .= "===> ��� ��� ������ �� equalizer OK<br>".$eq_hilbot['h']."=".$eq_hilbot['mh'];
                            }
                          }
                        }
                      }

                      if ($debug_test) {
                        $debug_test_s .= "===> ��� � ��� equalizer OK<br>";
                      }

                      // ��������� ��� � ������� ���
                      if (rand(1, 100) > 70) {
                        $eq_t_us['battle_rune'] = false;
                        $data_equalizer[$t_us['l']] = $eq_t_us;
                        $file_save = true;
                      }

                    } else {

                      if ($debug_test) {
                        $debug_test_s .= "===> ������ ��� � ��� battle_rune=".$eq_t_us['battle_rune']." equalizer OK<br>";
                      }

                    }
                    continue;
                }

                // �������� ��� "equalizer" ������� �� ���
                if (isset($eq_t_us) && $eq_t_us['battle'] == 0) {

                  // �������� � ���


                  // ���������
                  $eq_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$t_us['id']."' LIMIT 1;");

                  // �� ���� �����������
                  $intervention_user = user_interference($t1list, $t2list, $t_us['id']);

                  if ($debug_test) {
                    $debug_test_s .= "===> eq_user id =  ".$eq_user['id']." <br>";
                    $debug_test_s .= "===> intervention_user id =  ".$intervention_user['id']." <br>";
                  }

                  // ���� ��� ��������� �� ����������
                  if (empty($eq_user['id']) || empty($intervention_user['id'])) continue;


                # ������� ����� ���������� "equalizer"
                  $eq_stats = array();

                  $eq_stats['no_user']    = 1;
                  $eq_stats['guild']      = '';
                  $eq_stats['align']      = 0;
                  $eq_stats['sex']        = 1;
                  $eq_stats['incity']     = INCITY;
                  $eq_stats['klan']       = "";
                  $eq_stats['invis']      = 1; # 1
                  $eq_stats['shit']       = 1;
                  $eq_stats['id']         = 0;
                  $eq_stats['weap']       = -1; # ��� ������ ��� ���������� ��������
                  $eq_stats['obraz']      = "invisible_new.jpg";
                  $eq_stats['bothidro']   = 0;
                  $eq_stats['bothran']    = 0;
                  $eq_stats['top']        = 0;

                  $eq_stats['name']       = $eq_t_us['login'];       # ���
                  $eq_stats['login']      = $eq_t_us['login'];       # ���
                  $eq_stats['level']      = $eq_t_us['level'];       # �������

                  $eq_stats['sila']       = $eq_user['sila'] + $eq_user['inv_strength'] + floor(float_rand($gip * $eq_user['inv_strength']));   # ����
                  $eq_stats['lovk']       = $eq_user['lovk'] + $eq_user['inv_dexterity'] + floor(float_rand($gip * $eq_user['inv_dexterity']));  # ��������
                  $eq_stats['inta']       = $eq_user['inta'] + $eq_user['inv_success'] + floor(float_rand($gip * $eq_user['inv_success']));    # ��������
                  $eq_stats['vinos']      = 0;                       # ����������������
                  $eq_stats['intel']      = 0;                       # ��������
                  $eq_stats['mudra']      = 0;                       # ��������

                  $wep_type = array("topor", "dubina", "molot", "posoh");
                  $eq_stats['wep_type']   = $wep_type[rand(0, count($wep_type) - 1)];      # ��� ������

                  $gip = rand(0.01, 0.5);

                  $eq_stats['minu']       = $eq_user['inv_minu'] + floor(float_rand($gip * $eq_user['inv_minu']));    # ���  ����
                  $eq_stats['maxu']       = $eq_user['inv_maxu'] + floor(float_rand($gip * $eq_user['inv_maxu']));    # ���� ����
                  $eq_stats['krit']       = $eq_user['inv_krit'] + floor(float_rand($gip * $eq_user['inv_krit']));    # krit
                  $eq_stats['akrit']      = $eq_user['inv_akrit'] + floor(float_rand($gip * $eq_user['inv_akrit']));   # akrit
                  $eq_stats['uvorot']     = $eq_user['inv_uvor'] + floor(float_rand($gip * $eq_user['inv_uvor']));    # uvorot
                  $eq_stats['auvorot']    = $eq_user['inv_auvor'] + floor(float_rand($gip * $eq_user['inv_auvor']));   # auvorot
                  $eq_stats['inv_absorbs_uron'] = $eq_user['inv_absorbs_uron'] + floor(float_rand($gip * $eq_user['inv_absorbs_uron'])); # inv_absorbs_uron
                  $eq_stats['inv_absorbs_krit'] = $eq_user['inv_absorbs_krit'] + floor(float_rand($gip * $eq_user['inv_absorbs_krit'])); # inv_absorbs_krit
                  $eq_stats['inv_power_uron']   = $eq_user['inv_power_uron'] + floor(float_rand($gip * $eq_user['inv_power_uron']));   # inv_power_uron
                  $eq_stats['inv_power_krit']   = $eq_user['inv_power_krit'] + floor(float_rand($gip * $eq_user['inv_power_krit']));   # inv_power_krit
                  $eq_stats['bron']       = floor(($eq_user['inv_bron1'] + $eq_user['inv_bron2'] + $eq_user['inv_bron3'] + $eq_user['inv_bron4']) / 4);    # bron
                  $eq_stats['hp']         = $eq_user['maxhp'] + floor(float_rand($gip * $eq_user['maxhp']));      # �����
                  $eq_stats['maxhp']      = $eq_user['maxhp'] + floor(float_rand($gip * $eq_user['maxhp']));      # ����. �����
                  $eq_stats['mana']       = $eq_user['maxmana'] + floor(float_rand($gip * $eq_user['maxmana']));    # ������������
                  $eq_stats['maxmana']    = $eq_user['maxmana'] + floor(float_rand($gip * $eq_user['maxmana']));    # ����. ������������

                  $eq_stats['KBO']        = 100;
                  $eq_stats['stuff_cost'] = $kbo_user[$eq_stats['level']][0];


  	            sql_query("INSERT INTO `bots` (`name`,`prototype`,`battle`,`is_pet`,`stats`,`hp`,`maxhp`,`mana`,`maxmana`,`city`) values (
                  '".$eq_stats['login']."',
                  '0',
                  '".$eq_user['battle']."',
                  1,
                  '".serialize($eq_stats)."',
                  '".$eq_stats['maxhp']."',
                  '".$eq_stats['maxhp']."',
                  '".$eq_stats['maxmana']."',
                  '".$eq_stats['maxmana']."',
                  '".INCITY."');");

           		 $eq_bot_id = sql_insert_id();

                   if (!empty($eq_bot_id)) {
            	    	$upd_battle = unserialize($row['teams']);
                        $statis     = unserialize($row['statis']);

                   	    $upd_battle[$eq_bot_id] = $upd_battle[$intervention_user['id']];

                		foreach($upd_battle[$eq_bot_id] as $k => $v) {
                		  $upd_battle[$k][$eq_bot_id] = array(0,0,0,time());
                		  $upd_battle[$eq_bot_id][$k] = array(0,0,0,time());
                		}

                      # ����������� ���-���    $eq_user
                		if (in_array ($intervention_user['id'], $t1)) {
                		    $ttt = 1;

                            # $user_pr = user_to_bot($eq_bot_id);
                            # $user_pr['invis'] = $user['invis'];
                            $user_pr['id']     = $eq_bot_id;
                            $user_pr['login']  = $eq_stats['login'];
                            $user_pr['guild']  = $eq_stats['guild'];
                            $user_pr['align']  = $eq_stats['align'];
                            $user_pr['level']  = $eq_stats['level'];
                            $user_pr['sex']    = $eq_stats['sex'];

                            $user_pr['top']    = 0;
                            $user_pr['hp']     = $eq_stats['hp'];
                            $user_pr['maxhp']  = $eq_stats['maxhp'];
                            $user_pr['mana']   = $eq_stats['mana'];
                            $user_pr['maxmana']= $eq_stats['maxmana'];

                            $user_pr['incity'] = $eq_stats['incity'];
                            $user_pr['klan']   = $eq_stats['klan'];
                            $user_pr['invis']  = $eq_stats['invis'];
                            $user_pr['shit']   = $eq_stats['shit'];

                          $nick_l = nick_ofset($user_pr, 0, 1);
                          $nick_l['weap']      = $eq_stats['weap'];
                          $nick_l['wep_type']  = $eq_stats['wep_type'];
                          $nick_l['shit']      = $user_pr['shit']?1:0;
                          $nick_l['exp']       = 0;
                          $nick_l['damage']    = 0;
                          $nick_l['karma']     = 0;
                          $nick_l['valor']     = 0;
                          $nick_l['pM']        = 0;

                          $t1list[$eq_bot_id]  = $nick_l;
                          $tlist = ", `t1list` = '".serialize($t1list)."'";
                        } else {
                            $ttt = 2;

                            # $user_pr = user_to_bot($eq_bot_id);
                            # $user_pr['invis'] = $user['invis'];
                            $user_pr['id']     = $eq_bot_id;
                            $user_pr['login']  = $eq_stats['login'];
                            $user_pr['guild']  = $eq_stats['guild'];
                            $user_pr['align']  = $eq_stats['align'];
                            $user_pr['level']  = $eq_stats['level'];
                            $user_pr['sex']    = $eq_stats['sex'];

                            $user_pr['top']    = 0;
                            $user_pr['hp']     = $eq_stats['hp'];
                            $user_pr['maxhp']  = $eq_stats['maxhp'];
                            $user_pr['mana']   = $eq_stats['mana'];
                            $user_pr['maxmana']= $eq_stats['maxmana'];

                            $user_pr['incity'] = $eq_stats['incity'];
                            $user_pr['klan']   = $eq_stats['klan'];
                            $user_pr['invis']  = $eq_stats['invis'];
                            $user_pr['shit']   = $eq_stats['shit'];

                          $nick_l = nick_ofset($user_pr, 0, 2);
                          $nick_l['weap']      = $eq_stats['weap'];
                          $nick_l['wep_type']  = $eq_stats['wep_type'];
                          $nick_l['shit']      = $user_pr['shit']?1:0;
                          $nick_l['exp']       = 0;
                          $nick_l['damage']    = 0;
                          $nick_l['karma']     = 0;
                          $nick_l['valor']     = 0;
                          $nick_l['pM']        = 0;

                          $t2list[$eq_bot_id] = $nick_l;
                          $tlist = ", `t2list` = '".serialize($t2list)."'";
                        }

                          $sexus = $eq_user['sex']?"":"a";
                          $action = $text_intervened[$eq_stats['sex']];

                        # ��� ����
                          $user_pr['invis'] = 1;
                          $bot_nick = " ".nick_team($user_pr, "B".$ttt);

                        # ����� � ���
             			  addlog($row['id'], nick_team($eq_stats, "B".$ttt).' �������� � ���!');
                          addwrite_log($row['id']);

                          //$att_intervened = array('login' => $eq_stats['login'], 'invis' => 1);
                          //CHAT::chat_system(array(), CHAT::chat_js_login($att_intervened).' '.$action.' � <a href="/log='.$row['id'].'" target=_blank><b>���</b></a>. �� '.CHAT::chat_js_login($text_intervened).'', $row['room'], array('addXMPP' => true));

                        # ����������
                          $statis[$eq_bot_id] = fun_new_statis($eq_bot_id, $ttt);

            		      sql_query('UPDATE `battle` SET
                              `teams` = \''.serialize($upd_battle).'\', `statis` = \''.serialize($statis).'\',
                              `t'.$ttt.'` = CONCAT(`t'.$ttt.'`,\';'.$eq_bot_id.'\'),
                              `to1` = \''.time().'\', `to2` = \''.time().'\' '.$tlist.'
                               WHERE `id` = '.$row['id'].' LIMIT 1;');


                       $eq_t_us['id']               = $eq_bot_id;
                       $eq_t_us['intervene_time']   = time();
                       $eq_t_us['intervene_battle'] = $row['id'];
                       $eq_t_us['battle_rune']      = true;
                       $eq_t_us['battle']           = $row['id'];
                       $eq_t_us['no_battles']       = "";
                       $data_equalizer[$t_us['l']]  = $eq_t_us;

                       $file_save = true;

                       if ($debug_test) {
                        $debug_test_s .= "===> ������ � ��� = ".$row['id']." OK<br>";
                       }

                       if ($intervene_log) {
                         # ��������� �����
                           $text = "��� <a href=\"/log=".$row['id']."\" target=_blank><b>���</b></a> ������ �� ".$intervention_user['login']."<br>���������: ".$eq_user['login'];
                           send_user_letter(1, '[equalizer]', '����� ��� '.INCITY, $text);
                       }

                      // ���� ����
                      break;

                   } # end if
                } # end if

                // ���� ����
                continue;
              } else {

                  // ��������� ������ � ���
                  if ($eq_t_us['battle'] == 0) {
                    $eq_t_us['no_battles'] = $eq_t_us['no_battles'] ? ",".$eq_t_us['no_battles'] : $row['id'];
                    $data_equalizer[$t_us['l']] = $eq_t_us;
                    $file_save = true;

                    if ($intervene_log) {
                       # ��������� �����
                         $text = "��� <a href=\"/log=".$row['id']."\" target=_blank><b>���</b></a> <b>���������</b> ������";
                         send_user_letter(1, '[equalizer]', '����� ��� '.INCITY, $text);
                    }

                    if ($debug_test) {
                      $debug_test_s .= "===> ��������� ������ � ��� = ".$row['id']." OK<br>";
                    }
                  } else {
                    if ($debug_test) {
                      $debug_test_s .= "===> ������ ��� ((( = ".$row['id']." OK<br>";
                    }
                  }

              }

         } # end if
       } # end if
     } # end foreach
   } # end while


 # ��������� ���������
   if ($file_save) {
     if ($debug_test) {
      $debug_test_s .= "===> ��������� ��������� = equalizer OK<br>";
     }
     file_put_contents($file_equalizer, serialize($data_equalizer), LOCK_EX);  # ���������
   }

   if ($debug_test_s) {
    //echo $debug_test_s;
   }

?>