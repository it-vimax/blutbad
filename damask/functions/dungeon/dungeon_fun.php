<?

/***------------------------------------------
 * �������� ����� ��������
 **/

function world_data_load(){
global $my_dungeon_id;
$world = array();
 if ($my_dungeon_id) {
  $world_db = sql_row("SELECT * FROM `dungeon_in` WHERE `id` = '".$my_dungeon_id."' LIMIT 1;");

   $world['ways']        = unserialize($world_db['ways']);         # ���������
   $world['users']       = unserialize($world_db['users']);        # ���������
   $world['subjects']    = unserialize($world_db['subjects']);     # ���������
   $world['interactive'] = unserialize($world_db['interactive']);  # ���������
   $world['doors']       = unserialize($world_db['doors']);        # ���������
   $world['grating']     = unserialize($world_db['grating']);      # ���������
   $world['items']       = unserialize($world_db['items']);        # ���������
   $world['battles']     = unserialize($world_db['battles']);      # ���������
   $world['statics']     = unserialize($world_db['statics']);      # ���������

   $world['map_name']    = $world_db['map_name'];                  # ���������
   $world['dir_name']    = $world_db['dir_name'];                  # ���������
   $world['start_room']  = $world_db['start_room'];                # ���������
 }

 return $world;
}

/***------------------------------------------
 * ���������� ����� ��������
 **/

function world_data_save($new_world, $saved = array()){
global $world, $my_dungeon_id;
 if ($my_dungeon_id) {
  if (empty($world['battles']))   $world['battles'] = array();

  $parms = "";

  if (in_array("ways", $saved)        || empty($saved)) { $parms .= ", `ways` = '".serialize($new_world['ways'])."'"; }
  if (in_array("users", $saved)       || empty($saved)) { $parms .= ", `users` = '".serialize($new_world['users'])."'"; }
  if (in_array("subjects", $saved)    || empty($saved)) { $parms .= ", `subjects` = '".serialize($new_world['subjects'])."'"; }
  if (in_array("interactive", $saved) || empty($saved)) { $parms .= ", `interactive` = '".serialize($new_world['interactive'])."'"; }
  if (in_array("doors", $saved)       || empty($saved)) { $parms .= ", `doors` = '".serialize($new_world['doors'])."'"; }
  if (in_array("grating", $saved)     || empty($saved)) { $parms .= ", `grating` = '".serialize($new_world['grating'])."'"; }
  if (in_array("items", $saved)       || empty($saved)) { $parms .= ", `items` = '".serialize($new_world['items'])."'"; }
  if (in_array("battles", $saved)     || empty($saved)) { $parms .= ", battles = '".serialize($new_world['battles'])."'"; }
  if (in_array("statics", $saved)     || empty($saved)) { $parms .= ", `statics` = '".serialize($new_world['statics'])."'"; }

  sql_query("UPDATE `dungeon_in` SET `active` = ".time()." ".$parms." WHERE `id` = '".$my_dungeon_id."' LIMIT 1;");
 }

 return $new_world;
}

# ������� ���� �����
  function matrix_to_mypos($x, $y, $type = "") {
   global $dungeon_mapsize, $name_bung;
    $matr = array();

    $m_map_size = $dungeon_mapsize[$name_bung]['width'];
    $h_map_size = $dungeon_mapsize[$name_bung]['height'];

    $start_x = $x - 4;
    $start_y = $y - 4;

    $max_width  = $x + 4;
    $max_height = $y + 4;

    if ($type == "matrix_mini_map") {
      $max_width  += 1;
      $max_height += 1;

     # ���� ������ ���� �� �������������
       if ($start_x < 0) { $start_x = 0; }
       if ($start_y < 0) { $start_y = 0; }

     # ���� ������ ��� ����� �� �������������
       if ($max_width > $m_map_size)  { $max_width = $m_map_size+1; }
       if ($max_height > $h_map_size) { $max_height = $h_map_size+1; }
    }

     for ($d_x = $start_x; $d_x < $max_width; $d_x++) {
       for ($d_y = $start_y; $d_y < $max_height; $d_y++) {
         $matr[$d_x][$d_y] = 1;
       }
     }

   return $matr;
  }

# ��������� ��������� �� �����
  function world_ways($world, $hero_x, $hero_y){
   global $my_id;

    $WIDTH_MINIMAP       = 9;
    $HEIGHT_MINIMAP      = 9;

    $loc_x = $hero_x;
    $loc_y = $hero_y;

    if (empty($loc_x)) { $loc_x = 0; }
    if (empty($loc_y)) { $loc_y = 0; }

    $w_x = $loc_x - floor($WIDTH_MINIMAP / 2);
    $h_y = $loc_y - floor($HEIGHT_MINIMAP / 2);

     $matrix_wals = array();
     $matrix = array();
     for ($d_x = 0; $d_x < $WIDTH_MINIMAP; $d_x++) {
       $d_y = 0;
       for ($d_y = 0; $d_y < $HEIGHT_MINIMAP; $d_y++) {
         $matrix[$d_x][$d_y] = 1;
       }
     }

   $ways = "";
     foreach ($matrix as $y => $x_val) {
       $y += $h_y;
       foreach ($x_val as $x => $y_val) {
         $x += $w_x;
         if (is_worlds($world, $x, $y)) {
           $v_ways['type'] = "cell";
           $ways .= "    <position x=\"".$x."\" y=\"".$y."\" type=\"".$v_ways['type']."\" />\n";
         }
       }
     }

   return $ways;
  }

# ������� ��������� � ����������
  function create_material($world, $dir_name){
    global $user;

   $materials = array();
   $id = 1;

   if ($dir_name == 'dense_forest') { # �������� ���

     $matr = array(
      1 => array('name' => "������� �����", 'img' => "present", 'num' => '35'),
      2 => array('name' => "������� �����", 'img' => "present", 'num' => '35'),
      3 => array('name' => "������� ������� �����", 'img' => "present", 'num' => '36'),
      4 => array('name' => "������� ������� �����", 'img' => "present", 'num' => '36'),
      5 => array('name' => "������� ����� � �������", 'img' => "present", 'num' => '37'),
     );

   } else {

     $matr = array(
      1 => array('name' => "����� �� 3-� ������� ������", 'img' => "organic", 'num' => '3'),
      2 => array('name' => "����� �� 5-� ������� ������", 'img' => "organic", 'num' => '5'),
      3 => array('name' => "����� �� 7-� ������� ������", 'img' => "organic", 'num' => '7'),
      4 => array('name' => "����� �� 9-� ������� ������", 'img' => "organic", 'num' => '9'),
      5 => array('name' => "����� �� 21-� ������� ������", 'img' => "organic", 'num' => '21'),

      6 => array('name' => "�������� ��������", 'img' => "crystal", 'num' => '1'),
      7 => array('name' => "�������� ���������", 'img' => "crystal", 'num' => '2'),
      8 => array('name' => "�������� ������", 'img' => "crystal", 'num' => '3'),
      9 => array('name' => "�������� ��������", 'img' => "crystal", 'num' => '4'),
      10 => array('name' => "�������� �������", 'img' => "crystal", 'num' => '5'),
  //    11 => array('name' => "�������� �������", 'img' => "crystal", 'num' => '6'),
      11 => array('name' => "�������� ������", 'img' => "crystal", 'num' => '6'),
  //    13 => array('name' => "�������� ����������", 'img' => "crystal", 'num' => '8'),
  //    14 => array('name' => "�������� �������", 'img' => "crystal", 'num' => '9'),
  //    15 => array('name' => "�������� ��������", 'img' => "crystal", 'num' => '10'),

      12 => array('name' => "������ ��������� +100", 'img' => "recipe", 'num' => '1'),
      13 => array('name' => "������ ��������� +250", 'img' => "recipe", 'num' => '2'),
      14 => array('name' => "������ ������� ��������� +500", 'img' => "recipe", 'num' => '3'),

      15 => array('name' => "������ ������������� +100", 'img' => "recipe", 'num' => '4'),
      16 => array('name' => "������ ������������� +250", 'img' => "recipe", 'num' => '5'),
     );
   }


     $kr_lapa = 0;

     foreach ($world['ways'] as $y => $x_val) {
       foreach ($x_val as $x => $y_val) {

        switch ($dir_name) {
          case "dense_forest":
          case "kanal": $is_m = mt_rand(1, 50); break;
          default:      $is_m = mt_rand(1, 30);
        }

       # ��������
        if ($is_m == 16) {
          $id++;

          if ($kr_lapa <= 3 && substr_count($user['quest_all'], '10013_3')) {

            $kr_lapa++;
            $item["id"]   = $id;
            $item["name"] = "�������� �����";
            $item["img"]  = "quest/31.gif";
            $item["num"]  = "31";
            $materials[$y][$x] = $item;

          } else {
          # ����������� ��������� ���������
            if ($dir_name == 'dense_forest') {
             $num = mt_rand(1, 4);
             if ($num == 4) $num = mt_rand(1, 5);   # �������������� ����������
            } else {
             $num = mt_rand(1, 16);
             if ($num >= 12) $num = mt_rand(1, 16); # �������������� ����������
            }

            $item["id"]   = $id;
            $item["name"] = $matr[$num]["name"];
            $item["img"]  = $matr[$num]["img"]."/".$matr[$num]["num"].".gif";
            $item["num"]  = $matr[$num]["num"];
            $materials[$y][$x] = $item;
          }
        }
       }
     }
   return $materials;
  }

# ��������� ���� � �������
  function dungeon_addchp($text){
   global $world, $my_id;
    if (count($world['users']) > 1) {
      foreach ($world['users'] as $k => $v) {
        if ($v['id'] != $my_id) {
         $v['login_jaber'] = jabber_format_login($v['login']);
         CHAT::chat_attention($v, $text, array('addXMPP' => true));
        }
      }
    }
  }

# �������� ����� ��� ���
  function is_statics_wall($world, $x, $y){
   global $world;
    if (empty($world['statics'][$x][$y])) return false; else return true;
  }

# �������� ����� ��� ��� ��� ���� �����
  function is_matrix_mini_map($world, $x, $y, $matrix_mini_map){

    if (empty($matrix_mini_map[$x][$y]))
        return false;
    else {
      if (empty($world['ways'][$x][$y]))
         return true;
      else
         return false;
    }
  }

# �������� user ��� ���
  function is_user_object($world, $x, $y){
   global $world;
     foreach ($world['users'] as $k_id => $v_user) {
       if ($v_user['position']['x'] == $x && $v_user['position']['y'] == $y) {
         return true;
       }
     }
  }

# �������� ������� ��� ���
  function is_grating_object($world, $x, $y){
   global $world;
    if (empty($world['grating'][$x][$y])) return false; else return true;
  }

# �������� �������� ������� ��� ���
  function is_close_grating_object($world, $x, $y){
   global $world;
         if (empty($world['grating'][$x][$y])) return false;
    else if (substr_count($world['grating'][$x][$y]['type_value'], "close")) return true; else return false;
  }

# �������� ����� ��� ���
  function is_doors_object($world, $x, $y){
   global $world;
    if (empty($world['doors'][$x][$y])) return false; else return true;
  }

# �������� �������� ������ ��� ���
  function is_close_doors_object($world, $x, $y){
   global $world;
         if (empty($world['doors'][$x][$y])) return false;
    else if (substr_count($world['doors'][$x][$y]['type_value'], "close")) return true; else return false;
  }

# �������� ����� ��� ���
  function is_interactive_object($world, $x, $y){
   global $world;
    if (substr_count($world['interactive'][$x][$y]['type_value'], 'obj_tree')) return false;
    if (empty($world['interactive'][$x][$y])) return false; else return true;

  }

# �������� ��� ��� ���
  function is_subjects_object($world, $x, $y){
   global $world;
    if (!empty($world['subjects'])) {
     foreach ($world['subjects'] as $k_id => $v_ways) {
       if ($v_ways['x'] == $x && $v_ways['y'] == $y) {
         return true;
       }
     }
    }

    return false;
  }

# �������� ����� ��� ���
  function is_worlds($world, $x, $y){
   global $world;
    if (empty($world['ways'][$x][$y])) return false; else return true;
  }


# ����� ����� � ���
  function find_battle_user($world, $find_id) {
   global $world, $my_dungeon_id, $name_bung;

   $return     = 0;
   $world_save = 0;

    if (!empty($world["battles"]))
         foreach ($world['battles'] as $batl_id => $batl_users) {  # �� ���
          $count_in_battle = 0;
             foreach ($batl_users as $user_id => $batl_id_user) {

                if (!empty($world['users'][$user_id]) && $world['users'][$user_id]['battle']) {

                 # ����
                   if ($find_id == $user_id) {
                     $return = $batl_id;
                   }

                  $count_in_battle++;
                } elseif (substr($user_id, 0, 3) == "sub") {

                 # ���
                   if ($find_id == $user_id) {
                     $return = $batl_id;
                   }

                }

             }

           # ������� ��� ���� ��� ��� ������
             if (empty($count_in_battle) && !empty($world['battles'][$batl_id])) {
              unset($world['battles'][$batl_id]);
              $world_save  = 1;
              $return      = 0;
             }

         }

   # ��������� ����� ���������
     if ($world_save) {
      # ��������� ���������
        world_data_save($world);
     }

    if (empty($count_in_battle))
         return 0;
    else return $return;
  }

# ������� ����� ������
  function add_interactive_object($world, $x, $y, $type){
   global $world;
    $world['interactive'][$x][$y]['id']         = "sub".(mt_rand(200, 299) . (mt_rand(1, 10000) * mt_rand(2, 100)));;
    $world['interactive'][$x][$y]['type_value'] = $type;
    return $world;
  }

# ������ ��� � ��������
  function get_world_object($world, $type, $x, $y){

    if (empty($world[$type][$x][$y]) && $type != "subjects" && $type != "users") {
      return false;
    } else {
      if ($type == "subjects") {
         foreach ($world[$type] as $k_id => $v_ways) {
           if ($v_ways['x'] == $x && $v_ways['y'] == $y) {
             return $v_ways;
           }
         }
         return false;
      } elseif ($type == "users") {

         foreach ($world["users"] as $k_id => $v_ways) {
           if ($v_ways['position']['x'] == $x && $v_ways['position']['y'] == $y) {
             return $v_ways;
           }
         }

         return false;
      } else {
         return $world[$type][$x][$y];
      }

    }

    return false;
  }

# ����� ��������� �� ���
  function find_world_object_toid($world, $type, $id){
   global $world;

   $pos      = array();
   $pos['x'] = 0;
   $pos['y'] = 0;

    if (!empty($world[$type]))
      foreach ($world[$type] as $k_x => $v) {

       if ($type == "users") {
         if ($k_x == $id) {
               $pos['x'] = @$world["users"][$k_x]['position']['x'];
               $pos['y'] = @$world["users"][$k_x]['position']['y'];
             return $pos;
         }
       } elseif ($type == "subjects") {

         foreach ($world[$type] as $k_id => $v_ways) {
           if ($v_ways['id'] == $id) {
               $pos['x'] = $v_ways['x'];
               $pos['y'] = $v_ways['y'];
             return $pos;
           }
         }

         if ($k_x == $id) {
               $pos['x'] = @$world["users"][$k_x]['position']['x'];
               $pos['y'] = @$world["users"][$k_x]['position']['y'];
             return $pos;
         }
       } else {
          foreach ($v as $k_y => $v_type) {
            if (@$world[$type][$k_x][$k_y]['id'] == $id) {
               $pos['x'] = $k_x;
               $pos['y'] = $k_y;
             return $pos;
            }
          }
       }

      }

    return false;
  }

# ������� ������
  function world_interactive_object($my_visual_map){
   global $world, $my_id, $my_dungeon_id;

   # $my_visual_map = matrix_to_mypos(@$world["users"][$my_id]['position']['x'], @$world["users"][$my_id]['position']['y']);

     $interactive = "";
       $interactive .= "";
        if (!empty($world['interactive']))
         foreach ($world['interactive'] as $k_x => $v) {
          foreach ($v as $k_y => $v_ways) {
           if (!empty($my_visual_map[$k_x][$k_y])) {
            $interactive .= "<object>\n";
            $interactive .= "    <position x=\"".$k_x."\" y=\"".$k_y."\" />\n";
            $interactive .= "    <id value=\"".$v_ways['id']."\" />\n";
            $interactive .= "    <type value=\"".$v_ways['type_value']."\" />\n";
            $interactive .= "</object>\n";
           }
          }
         }
       $interactive .= "";
     return $interactive;
  }

# �������
function world_grating_object($my_visual_map){
   global $world, $my_id, $my_dungeon_id;

    # $my_visual_map = matrix_to_mypos(@$world["users"][$my_id]['position']['x'], @$world["users"][$my_id]['position']['y']);

     $grating = "";
       $grating .= "";
        if (!empty($world['grating']))
         foreach ($world['grating'] as $k_x => $v) {
          foreach ($v as $k_y => $v_ways) {
           if (!empty($my_visual_map[$k_x][$k_y])) {
            if (empty($v_ways['id'])) {
             $v_ways['id'] = "sub".(mt_rand(300, 399) . (mt_rand(1, 10000) * mt_rand(2, 100)));
            }

            $grating .= "<object>\n";

            $grating .= "    <position x=\"".$k_x."\" y=\"".$k_y."\" />\n";
            $grating .= "    <id value=\"".$v_ways['id']."\" />\n";
            $grating .= "    <type value=\"".$v_ways['type_value']."\" />\n";
            $grating .= "</object>\n";
           }
          }
         }
       $grating .= "";
     return $grating;

}

# �����
function world_doors_object($world){
   global $file, $world, $my_dungeon_id;

     $doors = "";
       $doors .= "";
        if (!empty($world['doors']))
         foreach ($world['doors'] as $k_x => $v) {
          foreach ($v as $k_y => $v_ways) {
            if (empty($v_ways['id'])) {
             $v_ways['id'] = "sub".(mt_rand(300, 399) . (mt_rand(1, 10000) * mt_rand(2, 100)));
            }

            $doors .= "<object>\n";

            $doors .= "    <position x=\"".$k_x."\" y=\"".$k_y."\" />\n";
            $doors .= "    <id value=\"".$v_ways['id']."\" />\n";
            $doors .= "    <type value=\"".$v_ways['type_value']."\" />\n";
            $doors .= "    <hint value=\"".CP1251_to_UTF8($v_ways['hint_value'])."\" />\n";
            $doors .= "    <header value=\"".CP1251_to_UTF8($v_ways['hint_value'])."\" />\n";
            $doors .= "    <miniheader value=\"".CP1251_to_UTF8($v_ways['hint_value'])."\" />\n";
            $doors .= "</object>\n";
          }
         }
       $doors .= "";
     return $doors;

}

# ������� ������
  function world_battles_object($world){
   global $file, $world, $my_dungeon_id;

     $world_save   = 0;
     $battles      = "";
     $battle_user  = "";

       $battles .= "";
        if (!empty($world['battles']))
         foreach ($world['battles'] as $batl_id => $batl_users) {  # �� ���

          $count_in_battle = 0;
          $battle_user  .= "<battle id=\"".$batl_id."\">\n";      # �� �����

             foreach ($batl_users as $user_id => $batl_id_user) {  # �� ���

                if (!empty($world['users'][$user_id]) && $world['users'][$user_id]['battle']) {
                 $battle_user .= "    <user id=\"".@$user_id."\"/>\n";
                 $count_in_battle++;
                } elseif (substr($user_id, 0, 3) == "sub") {
                 $battle_user .= "    <user id=\"".$user_id."\"/>\n";
                }
             }

          $battle_user  .= "</battle>\n";

         # ������� ��� ���� ��� ��� ������
           if (empty($count_in_battle)) {
            unset($world['battles'][$batl_id]);
            $battle_user = "\n";
            $world_save  = 1;
           }

          $battles .= $battle_user;
         }

       $battles .= "\n";

     if ($world_save) {
      # ��������� ���������
        world_data_save($world);

      # ������� ���� �� ������� ����� � ����
        $my_dungeon = sql_row("SELECT `id`, `battles` FROM `dungeon_in` WHERE `id` = '".$my_dungeon_id."' LIMIT 1;");
        if (!empty($my_dungeon['id'])) {
         # ��������� ���������
           world_data_save($world);
        }
     }

     return $battles;
  }

# ������� ����� ����
  function add_subjects_object($world, $x, $y, $type, $name, $level, $hp, $pw){
   global $world;
      $world['subjects'][$x][$y]['id']    = "sub".(mt_rand(300, 399) . (mt_rand(1, 10000) * mt_rand(2, 100)));
      $world['subjects'][$x][$y]['type_value']  = $type;
      $world['subjects'][$x][$y]['name_value']  = CP1251_to_UTF8($name);
      $world['subjects'][$x][$y]['level_value'] = $level;
      $world['subjects'][$x][$y]['hp']          = $hp;
      $world['subjects'][$x][$y]['pw']          = $pw;
    return $world;
  }

# ������� ����
  function world_subjects_object($my_visual_map){
   global $world, $name_bung, $my_dungeon_id, $my_id;

     $world = world_data_load();         # ���������

    # $my_visual_map = matrix_to_mypos(@$world["users"][$my_id]['position']['x'], @$world["users"][$my_id]['position']['y']);

     $subjects = "";
       $subjects .= "";
        if (!empty($world['subjects']))
         foreach ($world['subjects'] as $k_id => $v_ways) {
           if (!empty($my_visual_map[$v_ways['x']][$v_ways['y']])) {
            $subjects .= "<object>\n";
            $subjects .= "    <position x=\"".$v_ways['x']."\" y=\"".$v_ways['y']."\" />\n";
            $subjects .= "    <id value=\"".$v_ways['id']."\" />\n";
            $subjects .= "    <type value=\"".@$v_ways['type_value']."\" />\n";
            $subjects .= "    <name value=\"".CP1251_to_UTF8($v_ways['name_value'])."\" />\n";
            if (empty($v_ways['npc'])) {
              $subjects .= "    <level value=\"".$v_ways['level_value']."\" />\n";
              $subjects .= "    <features>\n";
              $subjects .= "      <feature id=\"1\" value=\"".$v_ways['hp']."/".$v_ways['hp']."\" />\n";
              $subjects .= "      <feature id=\"2\" value=\"".$v_ways['pw']."/".$v_ways['pw']."\" />\n";
              $subjects .= "    </features>\n";
            }
            $subjects .= "</object>\n";
           }
         }


       $subjects .= "\n";

     return $subjects;
  }

# ������� ������
  function world_users_object(){
   global  $world, $my_dungeon_id;

   $world = world_data_load();         # ���������

     $users = "";
       $users .= "";
        if (!empty($world['users']) && count($world['users']) > 1)
         foreach ($world['users'] as $k => $v_user) {
          if ($v_user['id']) {
            $users .= "<user>\n";
            $users .= "    <position x=\"".$v_user['position']['x']."\" y=\"".$v_user['position']['y']."\" />\n";
            $users .= "    <id value=\"".$v_user['id']."\" />\n";
            $users .= "    <type value=\"".CP1251_to_UTF8($v_user['login'])."\" />\n";
            $users .= "    <feature id=\"1\" value=\"".$v_user['hp']."\" />\n";
            $users .= "    <feature id=\"2\" value=\"".$v_user['pw']."\" />\n";
            $users .= "</user>\n";
          }
         }
       $users .= "";

     return $users;
  }

# ��������� ���� �� �����
  function world_statics_wall($world, $hero_x, $hero_y){
   global $world, $my_id, $my_dungeon_id;

    $WIDTH_MINIMAP       = 9;
    $HEIGHT_MINIMAP      = 9;

    if (empty($hero_x)) { $hero_x = 0; }
    if (empty($hero_y)) { $hero_y = 0; }

    $w_x = $hero_x - floor($WIDTH_MINIMAP / 2);
    $h_y = $hero_y - floor($HEIGHT_MINIMAP / 2);
    $d_x = 0;
    $d_y = 0;

   # ������� �������
     $matrix = array();
     for ($d_x = 0; $d_x < $WIDTH_MINIMAP; $d_x++) {
       $d_y = 0;
       for ($d_y = 0; $d_y < $HEIGHT_MINIMAP; $d_y++) {
         $matrix[$d_x][$d_y] = 1;
       }
     }

    # ������ ������� ����
      $matrix_wall = array();

    # ������� ���� �����
      $matrix_mini_map = matrix_to_mypos($hero_x, $hero_y, 'matrix_mini_map');

     foreach ($matrix as $y => $x_val) {
       $y += $h_y;

        foreach ($x_val as $x => $y_val) {
           $x += $w_x;
           if ($x > 0 && $y > 0 && is_matrix_mini_map($world, $x, $y, $matrix_mini_map)) {
             $object               = array();
             $object['id']         = "st".(mt_rand(100, 199) . (mt_rand(1, 10000) * mt_rand(2, 100)));
             $object['type_value'] = "wall";
             $matrix_wall[$x][$y]  = $object;
           }
        }

     }

   $wall = "";
     $wall .= "";
      if (!empty($matrix_wall))
       foreach ($matrix_wall as $k_x => $v) {
        foreach ($v as $k_y => $v_ways) {
          $wall .= "        <object>\n";
          $wall .= "            <position x=\"".$k_x."\" y=\"".$k_y."\" />\n";
          $wall .= "            <id value=\"".$v_ways['id']."\" />\n";
          $wall .= "            <type value=\"".$v_ways['type_value']."\" />\n";
          $wall .= "        </object>\n";
        }
       }
     $wall .= "";

   return $wall;
  }

/***------------------------------------------
 * ���������� ������ � �����
 **/

function update_hero_data($hero_id, $hero_data){

  $hero                       = array();
  $hero['id']                 = $hero_data['id'];
  $hero['login']              = $hero_data['login'];
  $hero['level']              = $hero_data['level'];
  $hero['klan']               = $hero_data['klan'];
  $hero['align']              = $hero_data['align'];
  $hero['guild']              = $hero_data['guild'];
  $hero['incity']             = $hero_data['incity'];
  $hero['sex']                = $hero_data['sex'];
  $hero['top']                = $hero_data['top'];
  $hero['hp']                 = $hero_data['hp'];
  $hero['maxhp']              = $hero_data['maxhp'];
  $hero['pw']                 = $hero_data['mana'];
  $hero['eff_silence']        = $hero_data['eff_silence'];
  $hero['eff_chain']          = $hero_data['eff_chain'];
  $hero['shadow']             = $hero_data['shadow'];
  $hero['battle']             = $hero_data['battle'];

  $hero['position']['x']      = 5;
  $hero['position']['y']      = 12;
  $hero['nd']                 = $hero_data['nd'];
  $hero['direction']          = "n";
  $hero['delay']              = 5;
  $hero['last_movement']      = time();

  return $hero;

}

/***------------------------------------------
 * ����������� ��j��� �� �����
 **/

function set_move_bots($world, $hero_x, $hero_y){
 global $world, $my_id, $user;
   $move = "";
   $world_dave = false;

     $my_visual_map = matrix_to_mypos($hero_x, $hero_y);

        if (!empty($world['subjects']))
         foreach ($world['subjects'] as $k_id => $v_ways) {
           if (!empty($my_visual_map[$v_ways['x']][$v_ways['y']])) {

            $old_bor_pos = $world['subjects'][$k_id];
            $old_x       = $old_bor_pos['x'];
            $old_y       = $old_bor_pos['y'];


            /*if (!empty($old_bor_pos['battle'])) {
              $data_battle = sql_row("SELECT * FROM `battle` WHERE `id` = '".$old_bor_pos['battle']."' AND win = 3 LIMIT 1;");
              if (empty($data_battle['id'])) {
                 $old_bor_pos['battle'] = 0;
                 $world['subjects'][$k_id]['battle'] = 0;
              }
            }*/
              // CHAT::chat_attention($user, '<b>'.$old_bor_pos['name_value'].'</b> ���� '.@$old_bor_pos['battle'].'', array('addXMPP' => true));

            /*if (
               ($hero_x == $old_x && $hero_y == $old_y)
            || ($hero_x+1 == $old_x && $hero_y == $old_y)
            || ($hero_x == $old_x+1 && $hero_y == $old_y)
            || ($hero_x == $old_x && $hero_y+1 == $old_y)
            || ($hero_x == $old_x && $hero_y == $old_y+1)
            || ($hero_x-1 == $old_x && $hero_y == $old_y)
            || ($hero_x == $old_x-1 && $hero_y == $old_y)
            || ($hero_x == $old_x && $hero_y-1 == $old_y)
            || ($hero_x == $old_x && $hero_y == $old_y-1)
            ) {
                CHAT::chat_attention($user, '<b>'.$old_bor_pos['name_value'].'</b> 2222222222222222222 '.@$old_bor_pos['battle'].'', array('addXMPP' => true));
            }*/

            if (empty($old_bor_pos['last_movement'])) { $old_bor_pos['last_movement'] = time(); }

          # ����� �������������
            $is_pos      = rand(0, 1);
            $time_stop   = 7;

             if ($is_pos && empty($old_bor_pos['npc']) && empty($old_bor_pos['stop_move']) && empty($old_bor_pos['battle']) /*&& ($old_bor_pos['last_movement'] + $time_stop) < time()*/) {

              # ��������� �����������
                $pos = rand(1, 4);
              //  $pos = 3;

                 $pos_x = $old_x;
                 $pos_y = $old_y;

                 switch ($pos) {
                   case 1: $pos_x = $old_x + 1; break;
                   case 2: $pos_x = $old_x - 1; break;
                   case 3: $pos_y = $old_y + 1; break;
                   case 4: $pos_y = $old_y - 1; break;
                 }

                     if (is_statics_wall($world, $pos_x, $pos_y)         # �����
                      || !is_worlds($world, $pos_x, $pos_y)              # ������
                      || is_interactive_object($world, $pos_x, $pos_y)   # �����
                      /*|| is_subjects_object($world, $pos_x, $pos_y)      # ��� */
                      || is_close_grating_object($world, $pos_x, $pos_y) # �������
                      || is_user_object($world, $pos_x, $pos_y)          # ��������
                      || is_close_doors_object($world, $pos_x, $pos_y)   # �����
                      || ($pos_x == $hero_x && $pos_y == $hero_y)        # � ���� ���
                      ) {

                          if (is_user_object($world, $pos_x, $pos_y) && $user['battle'] == 0) {

                            $sr_user = get_world_object($world, "users", $pos_x, $pos_y);

                            # ��������
                              if ($sr_user['id'] && empty($sr_user['battle'])) {
                                if ($world['map_name'] != '������ ���������� ������' && !empty($old_bor_pos['id'])) {

                                  if ($sr_user['id'] == $user['id']) {
                                    $sr_user = $user;
                                  } else {
                                    $sr_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$sr_user['id']."' LIMIT 1;");
                                  }

                                 # �������� ���� ������ 0 ��
                                  if ($sr_user['hp'] > 0 && $my_id && $sr_user['id'] == $my_id) {

                                   $dungeon_att = dungeon_attack($world, $sr_user, $old_bor_pos['id'], $old_bor_pos['type_value']);

                                    if ($dungeon_att) {
                                      $world = $dungeon_att;
                                      $world_dave = true;
                                     // addchp(' <b>'.$old_bor_pos['name_value'].'</b> �������� �� '.$sr_user['login'].' sss '.$old_bor_pos['id'].' ddd '.$old_bor_pos['type_value'].'', "BR");
                                    }

                                  }

                                }
                                //  addchp('<font color=red>��������!</font>dddddddddd ��������� <b>'.$sr_user['login'].'</b> ��������� �� '.@$bot_fight['name'].'', "BR");
                              } else {

                                 # ��������� �� ������ ����

                                   $bot_fight = sql_row("SELECT * FROM `bots` WHERE `hp` > 0 AND `battle` = '".$sr_user['battle']."' LIMIT 1;");

                                      if (!empty($bot_fight['name'])) {

                                        $kto_user = sql_row("SELECT * FROM `users` WHERE `login` = '".$old_bor_pos['name_value']."' LIMIT 1;");
                                        $kto_user['id_bot_object'] = $old_bor_pos['id'];
                                        $kto_user_bot = sql_row("SELECT * FROM `bots` WHERE `name` = '".$kto_user['login']."' AND `battle` = '".$sr_user['battle']."' LIMIT 1;");
                                        $bot_fight['login'] = $bot_fight['name'];
                                        $zakogo_user = $bot_fight;    # ���� �� �����

                                        $data_battle = sql_row("SELECT * FROM `battle` WHERE `id` = '".$bot_fight['battle']."' AND win = 3 LIMIT 1;");

                                        if ($kto_user['id'] && $zakogo_user['id'] && $data_battle['id'] && empty($kto_user_bot['id'])) {
                                          $dungeon_med = dungeon_meddle($world, $kto_user, $zakogo_user, $data_battle);
                                            if ($dungeon_med) {
                                              $world = $dungeon_med;
                                              $world_dave = true;
                                            // addchp('<b>'.$old_bor_pos['name_value'].'</b> ��������� �� '.@$bot_fight['name'].'', "BR");
                                            }
                                        }

                                      }



                              }
                          }
                        # ����
                          $pos_x = $old_x;
                          $pos_y = $old_y;

                     } else {
                       if (rand(0, 2) == 2) {
                        $old_bor_pos['x']             = $pos_x;
                        $old_bor_pos['y']             = $pos_y;
                        $old_bor_pos['last_movement'] = time();

                      # ����� ���������� ����
                        unset($world['subjects'][$k_id]);
                        $world['subjects'][$k_id] = $old_bor_pos;
                        $world_dave = true;
                       }
                     }

                     # ��������� ���������
                       if ($world_dave) {
                         world_data_save($world);
                       }

             }
           }
         }

  return $move;
}

/***------------------------------------------
 * ������
 **/

function get_obraz($name_obr, $sex){
 global $obr_all;

 if (empty($name_obr)) { $name_obr = "0_0_M000.png";  }

 $name_obr = str_replace(".jpg", "", $name_obr);

 if (in_array($name_obr, $obr_all)) {
  return $name_obr.".png";
 } else {
  return "0_0_".($sex?"M":"F")."000.png";
 }

}

/***------------------------------------------
 * ���������
 **/

function dungeon_attack($world, $kto_user, $id_bot_object, $type_bot){
 global $world, $my_dungeon_id, $main_bot, $params, $user, $kbo_user, $DUNGEON_BOTS;

//   $kto_user
//   $nakogo_user
  // $kto_user = $user;
   $my_id = $kto_user['id'];

    if (empty($kto_user['battle'])) {

             // 23:00:58 ��p����� �������o ���� �� ������.
               $login_header = $main_bot[$type_bot]['header'];

               $bot_attack = sql_row("SELECT id, login, level, hp, maxhp, mana, maxmana, sex, klan, guild, top, align, shadow, sila, lovk, inta, vinos, invis, bot, bothidro, battle, room, incity, dungeon FROM `users` WHERE `login` = '".$login_header."' LIMIT 1;");

             # ��������� ���
               $tools = array();

               if (empty($bot_attack['id']) && $type_bot == "bot_klan_monster" && $kto_user['klan']) {

                 // �������� ���
                 include("dungeon_fun_klan.php");

                 $tools["no_ng_rune"]        = 1;       # ��� �� ���
                 $tools["no_ability"]        = 1;       # ��� ������
                 $tools["no_klan_ability"]   = 1;       # ��� �������� ������
                 $tools["no_valor"]          = 1;       # ��� ��������
                 $tools["no_ps"]             = 1;       # ��� ������� �� �������� �����

               } else {

                 $bot_data = $DUNGEON_BOTS[$bot_attack['login']];
                 $bot_stats = array();

                 if (!empty($bot_data)) {

                   $bot_attack['maxhp']   = $bot_data['maxhp'];
                   $bot_attack['maxmana'] = $bot_data['maxmana'];
                   $bot_attack['sex']     = $bot_data['sex'];
                   $bot_stats             = $bot_data;
                 }

               # id ����
                 $bot_attack_id = $bot_attack['id'];

               # ������ ���������� ������
                 if ($world['map_name'] == '������ ���������� ������') {
                   include("dungeon_fun_att_lair.php");
                   $bot_attack['maxmana'] = $bot_stats['maxmana'];
                   $bot_attack['maxhp']   = $bot_stats['maxhp'];
                   $bot_attack['level']   = $bot_stats['level'];

                   $bot_attack_id = -1;

                   $tools["no_ng_rune"]        = 1;       # ��� �� ���
                   $tools["no_ability"]        = 1;       # ��� ������
                   $tools["no_klan_ability"]   = 1;       # ��� �������� ������
                   $tools["no_valor"]          = 1;       # ��� ��������
                   $tools["no_ps"]             = 1;       # ��� ������� �� �������� �����
                 }

                 $level = $bot_attack['level'];

    	            sql_query("INSERT INTO `bots` (`name`,`prototype`,`battle`,`stats`,`hp`,`maxhp`,`mana`,`maxmana`,`city`) values (
                    '".$bot_attack['login']."',
                    '".$bot_attack_id."',
                    '".$kto_user['battle']."',
                    '".serialize($bot_stats)."',
                    '".$bot_attack['maxhp']."',
                    '".$bot_attack['maxhp']."',
                    '".$bot_attack['maxmana']."',
                    '".$bot_attack['maxmana']."',
                    '".$kto_user['incity']."');");

    		        $jert_id = sql_insert_id();
               }



                if ($bot_attack['id']) {

                    $list_1 = array();
                    $list_2 = array();

            # �������� ������� �� �������������

                    $zay_user = $kto_user;
                    if (!empty($zay_user['id'])) {
                        $nick_1               = nick_ofset($zay_user, 1, 1);
                        $nick_1['wep_type']   = '';
                        $nick_1['shit']       = $zay_user['shit']?1:0;
                        $nick_1['exp']        = 0;
                        $nick_1['damage']     = 0;
                        $nick_1['karma']      = 0;
                        $nick_1['valor']      = 0;

                        $list_1[$zay_user['id']] = $nick_1;
                    }

            # �������� ������� �� �������������

                     $zay_user = $bot_attack;
                      if (!empty($zay_user['id'])) {
                          $bot_attack['id']   = $jert_id;
                          $zay_user['hp']     = $zay_user['maxhp'];
                          $zay_user['mana']   = $zay_user['maxmana'];
                          $zay_user['id']     = $jert_id;
                          $nick_2             = nick_ofset($zay_user, 1, 2);
                          $nick_2['wep_type'] = '';
                          $nick_2['shit']     = $zay_user['shit']?1:0;
                          $nick_2['exp']      = 0;
                          $nick_2['damage']   = 0;
                          $nick_2['karma']    = 0;
                          $nick_2['valor']    = 0;
                          $nick_2['pM']       = 0;

                          $list_2[$bot_attack['id']] = $nick_2;
                      }


				    $teams = array();
                    if ($jert_id > 0 && $my_id > 0) {
    					$teams[$my_id][$jert_id] = array(0,0,0,time());
    					$teams[$jert_id][$my_id] = array(0,0,0,time());
    					$sv = array(3,5,7);
                    }

                    if (isset($teams[$my_id][$jert_id]) && isset($teams[$jert_id][$my_id]) && !empty($teams[$jert_id])) {



					sql_query("INSERT INTO `battle`
						(
							`teams`,`teams_old`,`timeout`,`type`,`status`,`t1`,`t2`,`to1`,`to2`,`t1list`,`t2list`,`blood`,`closed`,`room`, `startbattle`, `log_channel`, `tools`, `city`
						)
						VALUES
						(
							'".serialize($teams)."','".serialize($teams)."','".$sv[rand(0,2)]."','23','0', '".$my_id."', '".$jert_id."', '".time()."', '".time()."','".serialize($list_1)."','".serialize($list_2)."', 0, 0,'".$kto_user['room']."','".time()."','l','".serialize($tools)."','".$kto_user['incity']."'
						)");

    					$battle_id = sql_insert_id();
                        if ($battle_id) {

        				    sql_query("UPDATE `bots` SET `battle` = '".$battle_id."' WHERE `id` = '".$jert_id."' LIMIT 1;");

        				  # ������� ���
                            $action = $bot_attack['sex']?"�����":"������";
        	   		        CHAT::chat_system($kto_user, "<a href=\"/log=".$battle_id."\" target=_blank><b>���</b></a> ����� ".CHAT::chat_js_login($kto_user)." � ".CHAT::chat_js_login($bot_attack)." �������.", $kto_user['room'], array('addXMPP' => true));

        				  # ����� ���� ��� �������� ���
                            dungeon_addchp ("������� <a href=\"/log=".$battle_id."\" target=_blank><b>���</b></a> ����� ".CHAT::chat_js_login($kto_user)." � <b>".$bot_attack['login']."</b>");

        					sql_query("UPDATE `users` SET `battle` = '".$battle_id."', `zayavka` = 0 WHERE `id` = '".$my_id."' LIMIT 1;");

        					$rr = nick_ofgethtml($list_1[$my_id], 0, 1)." � ".nick_ofgethtml($list_2[$bot_attack['id']], 0, 2);
                            addwrite_log($battle_id);
           			        addlog($battle_id,'������� ��� ����� '.$rr);
                            addwrite_log($battle_id);

                          # ���������� ���� � ������ � ���� ���
                            CHAT::chat_battle_c_glb($battle_id, array('addXMPP' => true, 'joinuser' => [['j_login' => $kto_user['login_jaber'], 'side' => 1]]));
                            $battle_log = date("H:i:s")."|".rand(1, 100)."|0|0|".""."������� ��� ����� ".$rr;
                            CHAT::chat_battle_add_lb($battle_log, $battle_id, array('addXMPP' => true));

                          # ������ ���� � ��� � �����
                            $world['users'][$my_id]['battle']             = $battle_id;
                            $world['subjects'][$id_bot_object]['battle']  = $battle_id;

                            $world['battles'][$battle_id][$my_id]         = $battle_id;
                            $world['battles'][$battle_id][$id_bot_object] = $battle_id;

                          # ��������� ���������
                            world_data_save($world);

                         $params['battle'] = 1;
                        }
                    } else {
                       sql_query("DELETE FROM `bots` WHERE `id` = '".$jert_id."' LIMIT 1;");
                    }

                 return $world;
              } else return false;
    }
  return false;
}

/***------------------------------------------
 * �������������
 **/

function dungeon_meddle($world, $kto_user, $zakogo_user, $data_battle){
 global $world, $my_dungeon_id, $text_intervened, $user;

  if ($kto_user['bot']) {

       sql_query("INSERT INTO `bots` (`name`,`prototype`,`battle`,`hp`,`maxhp`,`mana`,`maxmana`,`city`) values (
          '".$kto_user['login']."',
          '".$kto_user['id']."',
          '".$zakogo_user['battle']."',
          '".$kto_user['maxhp']."',
          '".$kto_user['maxhp']."',
          '".$kto_user['maxmana']."',
          '".$kto_user['maxmana']."',
          '".$kto_user['incity']."');");

    $my_id = sql_insert_id();
    $kto_user['id'] = $my_id;

  } else {
    $my_id = $kto_user['id'];
  }


  $jert = $zakogo_user;
  $battle_id = $jert['battle'];

  # ���� ������
    $upd_battle = unserialize($data_battle['teams']);
    $statis     = unserialize($data_battle['statis']);

  # ����������� �����
    $upd_battle[$my_id] = $upd_battle[$jert['id']];
    foreach($upd_battle[$my_id] as $k => $v) {
      $upd_battle[$my_id][$k] = array(0,0,0,time());
      $upd_battle[$k][$my_id] = array(0,0,0,time());
    }

    $t1 = explode(";",$data_battle['t1']);

       # ����������� ���-���
  	     if (in_array ($jert['id'], $t1)) {
  		    $ttt = 1;

            $t1list = unserialize($data_battle['t1list']);

            $user_pr = $kto_user;
            $nick_l = nick_ofset($user_pr, 1, 1);
            $nick_l['wep_type'] = '';
            $nick_l['shit']     = $user_pr['shit']?1:0;
            $nick_l['exp']      = 0;
            $nick_l['damage']   = 0;
            $nick_l['karma']    = 0;
            $nick_l['valor']    = 0;
            $nick_l['pM']       = 0;

            $t1list[$my_id] = $nick_l;
            $tlist = ", `t1list` = '".serialize($t1list)."'";
  	     } else {
            $ttt = 2;

            $t2list = unserialize($data_battle['t2list']);

            $user_pr = $kto_user;
            $nick_l = nick_ofset($user_pr, 1, 2);
            $nick_l['wep_type'] = '';
            $nick_l['shit']     = $user_pr['shit']?1:0;
            $nick_l['exp']      = 0;
            $nick_l['damage']   = 0;
            $nick_l['karma']    = 0;
            $nick_l['valor']    = 0;
            $nick_l['pM']       = 0;

            $t2list[$my_id] = $nick_l;
            $tlist = ", `t2list` = '".serialize($t2list)."'";
  	     }

       # ����� � ��������
         $action = $text_intervened[$kto_user['sex']];
         CHAT::chat_system($user, CHAT::chat_js_login($kto_user).' '.$action.' � <a href="/log='.$battle_id.'" target=_blank><b>���</b></a>. �� '.CHAT::chat_js_login($jert).'', $user['room'], array('addXMPP' => true));

       # ����� � ���
         $log_text = nick_ofgethtml($nick_l, 0, $ttt).' '.$action.' � ���!';
         addlog($battle_id, $log_text);
         addwrite_log($battle_id);

       # ����������
         $statis[$my_id] = fun_new_statis($kto_user, $ttt);

       # ��������� ���������
         sql_query('UPDATE `battle` SET
                 `teams` = \''.serialize($upd_battle).'\', `statis` = \''.serialize($statis).'\',
                 `t'.$ttt.'` = CONCAT(`t'.$ttt.'`,\';'.$my_id.'\'),
                 `to1` = \''.time().'\', `to2` = \''.time().'\' '.$tlist.'
                  WHERE `id` = '.$battle_id.' LIMIT 1;');

       # ����� ��� � ��� � ���
         sql_query("UPDATE users SET `battle` = ".$battle_id.", `zayavka` = 0 WHERE `id` = ".$my_id."; ");
         $kto_user['battle'] = $battle_id;

      # ���� ��� � �� ���� ��������
	    CHAT::chat_attention($jert, '�� ��� � ��� '.$action.' '.CHAT::chat_js_login($kto_user).' ', array('addXMPP' => true));

      # ���������� ���� � ������ � ���� ���
        if ($data_battle['log_channel']) {
          CHAT::chat_battle_jc_glb($kto_user, $battle_id, array('side' => 1));
          $battle_log = date("H:i:s")."|".rand(1, 100)."|".$kto_user['id']."|0|"."".$log_text;
          CHAT::chat_battle_add_lb($battle_log, $battle_id, array('addXMPP' => true));
        }

      # ������ ���� � ���

         if ($kto_user['bot']) {
            $world['subjects'][$kto_user['id_bot_object']]['battle']  = $battle_id;
            $world['battles'][$battle_id][$kto_user['id_bot_object']] = $battle_id;
        } else {
            $world['users'][$my_id]['battle']     = $battle_id;
            $world['battles'][$battle_id][$my_id] = $battle_id;
         }

   return $world;
}


?>