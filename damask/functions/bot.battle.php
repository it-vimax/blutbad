<?php

# ������� ������ � ��������
  defined('_CRON') or die();

//include("a_process_start.php");

 #--------------------------------
 # ��������� ������� �� ����� ����������
   include 'functions/battle/battle.lock_tables.php';

/***--------------------------
 * ������� ��� �����
 **/

function bot_percent_exp($user_p, $data_battle_p) {
 global $eff_obraz;

 $stand_percent = 100;            # ������� ������� �����
 $percent       = $stand_percent; # ������� ������� �����

 if (!empty($user_p['id']) || !empty($data_battle_p['id'])) {

   $eff_bonus = sql_row("SELECT sum(total) as total_sum FROM `effects` WHERE `owner` = '".$user_p['id']."' AND (`type` = 1039 OR `type` = 1034);");

   if (!empty($eff_bonus['total_sum'])) { $percent += $eff_bonus['total_sum']; }

   if ($data_battle_p['id']) {
     # ��������� ����� �����������
       if ($data_battle_p['type'] == 25) { $percent += 100; }

    # �� ������
      if(date("w")<6 && date("w")>0){
      # ����� � 1 �� 8 ����
      	if (date("G")>=1 && date("G")<=8){
      		$percent += 20;
      	}
      } else {
      # �� ��������
        $percent += 50;
      }

    # ����� ������ ������� � �����
      if (isset($eff_obraz[$user_p['shadow']]['persent_exp'])) {
        $percent += $eff_obraz[$user_p['shadow']]['persent_exp'];
      }

      if ($user_p['klan']) {
       $klan_user_p = sql_row("SELECT clanlevel, name, place_count FROM `clans` WHERE `name` = '{$user_p['klan']}' LIMIT 1;");
       $klan_user_maxlimit = klan_user_maxlimit($klan_user_p, 'count');
            if ($klan_user_maxlimit >= floor($klan_user_p['place_count']))      { $percent += 20; }
        elseif ($klan_user_maxlimit >= floor($klan_user_p['place_count']*0.85)) { $percent += 10; }
        elseif ($klan_user_maxlimit >= floor($klan_user_p['place_count']*0.7))  { $percent += 5; }
      }
    # ��� ��������
      if($data_battle_p['blood']) { $percent += 50; }

    # �� ����� ��� ���� +100% � �����
      if(date("m") == 1 && (date("d") == 1 || date("d") == 2 || date("d") == 3)) { $percent += 100; }

      if ($data_battle_p['id']) {
        # ���� ��� ���������
        switch ($data_battle_p['status_battle']) {
          case 1: $percent += 10;  break; # ������ �������!
          case 2: $percent += 30;  break; # ������ ������������
          case 3: $percent += 50;  break; # ��������� �����!
          case 4: $percent += 75;  break; # �������������� ��������!
          case 5: $percent += 100; break; # ������������ ��������!
        }
      }
   }
 }
  return $percent;
}

 $text_death  = array(" ���������.", " ��������.");
 $text_atakt  = array("������", "�����", "�����", "����");
 $text_isudar = array("�����", "�������");

 $text_salvation = array(" ��������.", " ������.", " �����������.", " ����������.", " ����� ��������.", " ����� ������.");

/***-------------------------------
 * ������ ������������ ���� �����
 **/

	class fbattle_bot {
			public $log_channel    = '';        # ������ ��������
			public $status         = integer;   # ������ ��������  ---- 0 - ��� �����; 1 - ���� �����
			public $battle         = array();   # ������ � ������������
			public $battle_eff     = array();   # ������ � ��������� ���
			public $b_hint_nbp     = array();   # � ������� ���

			public $battle_data    = array();   # ������ �� �����
			public $enemy          = null;      # ������������� ����������

			public $tintervention  = array();   # ������ � ���������� � ���
			public $t1             = array();   # ������ �������
			public $t2             = array();   # ������ �������
			public $t1list         = array();   # ������ ������ �������
			public $t2list         = array();   # ������ ������ �������
			public $tools          = array();   # ��������� ���

			public $texit          = array();   # ������� ��������
			public $team_enemy     = array();   # ������� ���������� (������ �� �����)
			public $team_mine      = array();   # ���� �������
  			public $user           = array();   # ���� �� ������
			public $enemyhar       = array();   # ���� �� ����������
			public $enemy_dress    = array();   # ���� �����
			public $user_dress     = array();   # ����  ����
			public $en_class, $my_class;        # ����� ��� ����
			public $bots           = array ();
			public $log            = "";        # ���������� ����
			public $battle_log     = "";        # ���������� ����
			public $battle_log_id  = 0;         # ���������� ���� ��
			public $to1;                        # ������� ������ �������
            public $to2;                        # ������� ������ �������
            public $upd_required   = 0;         # ��������� ����������
			public $exp            = array();   # �����
			public $statis         = array();   # ���������� ��� �������

			public $battle_mod     = false;     # ���� ��������� � ��� ��� ����������

    /***-------------------------------------
     * �������� ������ � ���� �������� ����
     **/

	function fbattle_bot($battle_data) {
     global $bot_user_attacking;

	    $this->user   = $bot_user_attacking;
	    $upd_battle   = '';
        $battle_id    = $battle_data['id'];
        $cur_battleid = $battle_id;

	    # ���������� ��������
	      if ($battle_id > 0 && $bot_user_attacking['id']) {

              $this->battle_log_id  = date("His").rand(1, 10); # ���������� ���� ��

			# ��������� �����������
			  $this->battle_data = $battle_data;
              $this->log_channel = $this->battle_data['log_channel'];

            # ����� � ����� ������� ��� ����
			  if($this->log_channel == 'l') { $upd_battle .= "`log_channel` = 'l".$battle_id."', "; }

            # ����� � ����� ������� ��� ����
			  if($this->battle_data['room'] <= 0) { $upd_battle .= "`room` = '".$bot_user_attacking['room']."', "; }

		    # ��������� ���
		   	  $this->tools = unserialize($this->battle_data['tools']);

            # ������ ������� ���
              $this->t1list = unserialize($this->battle_data['t1list']);
              $this->t2list = unserialize($this->battle_data['t2list']);

			  $this->sort_teams($bot_attacking['id']);

            # ��� ���������?
		      $this->battle = unserialize($this->battle_data['teams']);

    		# ������ � ��������� ���
    		  $this->battle_eff = unserialize($this->battle_data['battle_eff']);
    		# ������ � ������� ���
    		  $this->b_hint_nbp = unserialize($this->battle_data['b_hint_nbp']);

           # ��������� ���
             if ($upd_battle) {
                sql_query("UPDATE `battle` SET $upd_battle `to1` = '".time()."', `to2` = '".time()."' WHERE `id` = '".$battle_id."' LIMIT 1;");
             }

		   # �������� ���������� ���
		     $this->statis = unserialize($this->battle_data['statis']);
		   # ���� ����
			 $this->to1 = $this->battle_data['to1'];
			 $this->to2 = $this->battle_data['to2'];

	      } else {
		    # ������ ������ ����� �� "��� �����"
	      }

	}

          /***--------------------------
           * �������� �����������
           **/

            function select_enemy() {
             global $bot_attacking;
              if(!empty($bot_attacking['id']) && !empty($this->battle_data['id']) && $this->battle_data['win'] == 3 && ($bot_attacking['hp'] > 0 && $bot_attacking['battle'] > 0) && !empty($this->battle[$bot_attacking['id']])) {
          	   foreach($this->battle[$bot_attacking['id']] as $k => $v) {
          		 if (empty($this->battle[$bot_attacking['id']][$k][0])) {
                     if($k > _BOTSEPARATOR_) {
          			     $enemys[] = $k;
                     }
          		 }
          	   }
                   if (!empty($enemys)) {
          	        return $enemys[rand(0,count($enemys)-1)];
                   } else {
          		    return 0;
                   }

              } else {
          	  return 0;
              }
            }

/***--------------------------
 * ���� �� ���� � ��������� �����?
 **/

			function get_timeout () {
				if(!empty($this->battle_data['id']) && $this->battle_data['win']==3 && !empty($this->battle)) {

					if ($this->my_class == 'B1') {
						if($this->to2 <= $this->to1) {

                            if ($this->battle_data['status_battle']) {
                              return ((time() - $this->to2) > (($this->battle_data['timeout'] * 60)));
                            } else {
                              return ((time() - $this->to2) > (($this->battle_data['timeout'] * 60)));
                            }

						} else {
							return false;
						}
					} else {
						if($this->to2 >= $this->to1) {

                            if ($this->battle_data['status_battle']) {
                              return ((time() - $this->to1) > (($this->battle_data['timeout'] * 60)));
                            } else {
                              return ((time() - $this->to1) > (($this->battle_data['timeout'] * 60)));
                            }

						} else {
							return false;
						}
					}
				}
			}



function get_count_team($team = 1){
	$tlife = 0;

  if ($team == 1) {
  # ��������� �������� ������ �������
	foreach ($this->t1 as $k => $v) {
		if (in_array($v, array_keys($this->battle))) {
		  if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���
			$tlife++;
	      }
		}
	}
  } elseif ($team == 2) {
  # ��������� �������� ������ �������
	foreach ($this->t2 as $k => $v) {
		if (in_array($v, array_keys($this->battle))) {
		  if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���
			$tlife++;
          }
		}
	}
  }
 return $tlife;
}

function upd_battle_err($battle_err) {
//  sql_query("UPDATE `battle` SET `battle_err` = '".serialize($battle_err)."' WHERE `id` = '".$this->battle_data['id']."' LIMIT 1;");
}

/***--------------------------
 * �������� � ���������� ���
 **/

    function battle_end ($battle_id = 0) {
     global $bot_attacking, $bot_user_attacking, $bot_defender, $bot_user_defender,
            $caverooms, $base_mods, $lim_exp, $baseexp;

      if (empty($battle_id) && $bot_user_attacking['battle'] > 0) { $battle_id = $bot_user_attacking['battle']; }

       if ($battle_id) {

      # ��������� ����������

         # ������ �����
    	   $fighters = @array_keys($this->battle);

            $flag = 2;
        	$t1life = 0;
        	$t2life = 0;
        	$allvragdead = 1;

    		  # ��������� �������� ������ �������
    			foreach ($this->t1 as $k => $v) {
                    # �� ��� ����� �� ���
    				  if (!in_array($v, $this->texit) && $this->t1list[$v]['h'] > 0) { $t1life++; }
    			}

    		  # ��������� �������� ������ �������
    			foreach ($this->t2 as $k => $v) {
    		        # �� ��� ����� �� ���
    				  if (!in_array($v, $this->texit) && $this->t2list[$v]['h'] > 0) { $t2life++; }
    			}
                                                       $statusbattle_end  = 0;   # ��� ��� ����
               if ($t1life > 0 && $t2life <= 0)      { $statusbattle_end  = 1; } # �������� ������ �������
               elseif ($t2life > 0 && $t1life <= 0)  { $statusbattle_end  = 2; } # �������� ������ �������
               elseif ($t2life <= 0 && $t1life <= 0) { $statusbattle_end  = 3; } # �����

                    switch($statusbattle_end){

                     case 0: # ��� ���������� ��� ��� ����
                     break;

                     case 1: # �������� 1� �������
                        $win_team  = $this->t1;
                        $lose_team = $this->t2;
                     break;

                     case 2: # �������� 2� �������
                        $win_team  = $this->t2;
                        $lose_team = $this->t1;
                     break;

                     case 3: # �����
                        $win_team  = $this->t2;
                        $lose_team = $this->t1;
                        $nich_team  = array_merge($this->t1, $this->t2);
                     break;
                    }

      /*
       battle_end = 0 = ��� ����
       battle_end = 1 = ��� ������ ���������
       battle_end = 2 = ��� ���� ���
       battle_end = 3 = ��� ��������
      */

     # �������� ������ ��� ������ ��� ����� �������
       if ($statusbattle_end == 1 || $statusbattle_end == 2 || $statusbattle_end == 3) {

        $charge_win = sql_row('SELECT `id`, `win`, `battle_end` FROM `battle` WHERE `id` = '.$this->battle_data['id'].' LIMIT 1;');

           if ($allvragdead && !empty($charge_win['id']) && $charge_win['win'] == 3 && ($charge_win['battle_end'] == 0 || $charge_win['battle_end'] == 2)) {

        	 if (sql_query("UPDATE `battle` SET `battle_end` = 1 WHERE `id` = '".$this->battle_data['id']."' AND `battle_end` != 3 LIMIT 1;")) {
               $charge_win['battle_end'] = 1;
             }

            if ($charge_win['battle_end'] == 1) {

             $battle_err = array();
             $battle_err['win'] = $statusbattle_end;
             $battle_err['status_start']      = 'error';
             $battle_err['status_start_1']    = 'error';
             $battle_err['status_start_2']    = 'error';
             $battle_err['status_modif_1'] = 'error';
             $battle_err['status_modif_2'] = 'error';
             $battle_err['status_win_status'] = 'error';
             $battle_err['status_lose_team'] = 'error';
             $battle_err['status_ends1'] = 'error';

             $this->upd_battle_err($battle_err);

                $winers = '';
        	    $flag   = $statusbattle_end;

                $team1  = array();
                $nks1   = array();

                $team2  = array();
                $nks2   = array();

                # ������ ����������� � ������ � ��� ���������� ��� ��� ������ �������
                  foreach ($this->t1 as $k => $v) {
                   if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���
                     $party_user['id']    = $this->get_points('id', $v);
                     $party_user['invis'] = $this->get_points('i', $v);
                     $party_user['login'] = $this->get_points('n', $v);

                     $team1[$k] = nick_team($party_user, "B1");   # nick5($v, 'B1');
                     $nks1[]    = $party_user['invis']?"���������":$this->get_points('n', $v); # n = login or ���������;
                   }
                  }

                # ������ ����������� � ������ � ��� ���������� ��� ��� ������ �������
                  foreach ($this->t2 as $k => $v) {
                   if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���
                     $party_user['id']    = $this->get_points('id', $v);
                     $party_user['invis'] = $this->get_points('i', $v);
                     $party_user['login'] = $this->get_points('n', $v);

                     $team2[$k] = nick_team($party_user, "B2");  # nick5($v, 'B1');
                     $nks2[]    = $party_user['invis']?"���������":$this->get_points('n', $v); # n = login or ���������;
                   }
                  }

             $battle_err['status_start'] = 'ok';
             $this->upd_battle_err($battle_err);

            if ($statusbattle_end == 1 || $statusbattle_end == 2) { # �������� ������ ��� ������ �������

           ###########################################################
           #  ������� �����������
           ###########################################################

              foreach ($win_team as $k => $v) { # ����������

        	   $flag = $statusbattle_end;

               if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���

        	     sql_query('UPDATE `battle` SET `win` = '.$statusbattle_end.' WHERE `id` = "'.$this->battle_data['id'].'" LIMIT 1;');
                 $this->battle_data['win'] = $statusbattle_end;

                  #------------------------------------
                  # ��� ��� ����������
                    if ($this->battle_data['type'] == 12 && $v > _BOTSEPARATOR_) {
                      include("functions/battle/fbattle.endhran.php");
                    }

                    if($v < _BOTSEPARATOR_) {

        			  $user_win = sql_row("SELECT `id`, `level`, `login`, `animals`, `incity`, `klan`, `ps`, `room`, `sex`, `invis`, `battle`, `battles_today`, `win`, `win_next`, `dungeon`, `achievements_finish`, `thematic_event`, `rep`, `zside`, `zarnica`, `eff_all`, `shadow`, `quest_all` FROM `users` WHERE `id` = '".$v."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");

                      if (!empty($user_win['id'])) {

                       #---------------------------
                       # ������� % ����� �� ���
                         $battle_exp_percent = empty($this->tools['exp_percent'])?0:$this->tools['exp_percent'];
                         $exp_percent = bot_percent_exp($user_win, $this->battle_data) + $battle_exp_percent;

                       #---------------------------
                       # ���������s�� ���� �� ���
                         $earned_damage = floor($this->get_points('damage', $user_win['id']));
                         if (empty($earned_damage)) { $earned_damage = 0; }

                       #---------------------------
                       # ����������� ���� �� ���
                         $earned_exp = abs(floor($this->get_points('exp', $user_win['id']) / 100 * $exp_percent));
                         $this->set_points('exp', $earned_exp, $user_win['id']);
                         if (empty($earned_exp) || $earned_exp < 0) { $earned_exp = 0; $this->set_points('exp', 0, $user_win['id']); }

                       #---------------------------
                       # ����������� ����� �� ���
                         $earned_karma = conv_hundredths($this->get_points('karma', $user_win['id']), 2);
                         $this->set_points('karma', $earned_karma, $user_win['id']);
                         if (empty($earned_karma)) { $earned_karma = 0; }

                       #---------------------------
                       # ������������ ������� "���� ������"
                         $thematic_event = unserialize($user_win['thematic_event']);
                         if (!empty($thematic_event['name']) && $thematic_event['name'] == "���� ������") {
                          $t_earned_valor = $this->get_points('valor', $user_win['id']);
                          $this->set_points('valor', ($t_earned_valor + ($t_earned_valor * 50 / 100)), $user_win['id']);
                         }

             $battle_err['status_start_1'] = 'ok';
             $this->upd_battle_err($battle_err);

                       #---------------------------
                       # ����������� �������� �� ���

                       # � ��������� ��� ���� ������ ��������
                         if ($this->battle_data['hideb'] == 1) $hideb = 2; else $hideb = 1;

                         $earned_valor = round((($this->get_points('valor', $user_win['id']) / 100 * $exp_percent) * $hideb), 2);

                       # ������� ������� ���� ������ ��������
                              if ($user_win['id'] <= 7)  $earned_valor *= 3;
                         else if ($user_win['id'] <= 10) $earned_valor *= 2;

                         $this->set_points('valor', $earned_valor, $user_win['id']);
                         if (empty($earned_valor)) { $earned_valor = 0; }

                       # ��� ��������
                         if (!empty($this->tools['no_valor']) || $user_win['dungeon']) {
                          $earned_valor = 0;
                          $this->set_points('valor', 0, $user_win['id']);
                         }

                       #---------------------------
                       # �������� ������ ��������
                         if ($earned_valor > 500) {
                           $earned_valor = 500;
                           $this->set_points('valor', 500, $user_win['id']);
                           send_user_letter(1, '[���������� ��������]', $user_win['login'].' ����� ����� �������', 'battle_id '.$this->battle_data['id'].', earned_valor '.$earned_valor.', valor <b>'.$this->valor[$user_win['id']].'</b> ');
                         }

                              if (
                                 $this->battle_data['type'] != 20    # ����� ��� "���������"
                              && $this->battle_data['type'] != 25    # ����� ��� "������ �����������"
                              ) {
                                # ���� �������� ������ ��� ��������� ����� �� ������ �����
                                  if($earned_exp > $lim_exp[$user_win['level']]){
                                    $earned_exp = $lim_exp[$user_win['level']];
                                    $this->set_points('exp', $earned_exp, $v);
                                  }
                              }

                            # ��� ������� ������� ������ ��������� 30 ����� �� ���
                              if ($user_win['level'] == 0) {
                                $earned_exp = 30;
                                $this->set_points('exp', $earned_exp, $v);
                              }

                            # �������� �����
                              if ($this->battle_data['type'] == 14 || $this->battle_data['type'] == 17) {
                               # ���� �� �������� ��� �����
                                 $earned_exp = 0;
                                 $this->set_points('exp', $earned_exp, $v);
                              }

                                #-----------------------------
                                # ��� ������ = ��� ��������
                                  include ("functions/battle/specialwin.php");

             $battle_err['status_start_2'] = 'ok';
             $this->upd_battle_err($battle_err);

                                #---------------------------------------------
                                # ����� � ��� ��� ���
            			          CHAT::chat_attention($user_win, '<a href="http://'.$user_win['incity'].'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>���</b></a> ��������. ����� ���� �������� �����: <b>'.$earned_damage.'</b>. �������� �����: <b>'.$earned_exp.' ('.$exp_percent.'%).</b> �����: <b>'.$earned_karma.'</b>. ��������: <b>'.$earned_valor.'</b>.', array('addXMPP' => true));

                                  $reit_klan_iswar = "";

                                #---------------------------------------------
                                # ���� �������� � ����� �� ����� � ��� ��� � ���� �������� �����
                                  if ($user_win['klan']) {
                                   if ($earned_valor != 0) {
                                     $klan_valor = $earned_valor;
                                     CHAT::chat_attention($user_win, '�������� �������� ��������:  <b>'.$klan_valor.'</b>.', array('addXMPP' => true));
        						     sql_query("UPDATE `clans` SET `clanexp` = `clanexp` + ".$klan_valor." WHERE `name` = '".$user_win['klan']."' LIMIT 1;");
                                   }

                                   # ����� ���������� ���� ��� ��� ��������
                                     if ($this->battle_data['type'] == 14) {
                                       $reit_klan_iswar = ' `reit_klan_war_wins` = `reit_klan_war_wins` + 1, ';
                                     }
                                  }

                                #---------------------------------------------
                                # ���������� "���������� �����"
                                  if (!in_array($user_win["room"], $caverooms) && !$user_win["dungeon"]) {
                                    if (!in_array("winner_25000", explode(";", $user_win['achievements_finish']))) {
                                      achievements($user_win, array('winner_total' => array(1 => 1)));
                                    }
                                  }

                                #---------------------------------------------
                                # ����� ��������
                                  sql_query("UPDATE `enterlog` SET

                                    $reit_klan_iswar

                                        `reit_exp` = `reit_exp` + ".$earned_exp.",
                                        `reit_karma` = `reit_karma` + ".$earned_karma.",
                                        `reit_valor` = `reit_valor` + ".$earned_valor."

                                  WHERE `owner` = '".$user_win['id']."' AND `type` = 0 LIMIT 1;");

                                    #-----------------------------------------------
        			                # ���� ���� ����� �� ����� � ��� � ���� ��� ����
                                     if (!empty($user_win['animals']) && $user_win['animals'] != 'a:0:{}') {
        			                  if($earned_exp > 0) {
                                         $animals = unserialize($user_win['animals']);

                                           foreach ($animals as $k => $v_anim) {
                                              if ($v_anim['war'] && @$v_anim['battle'] == $user_win['battle']) {

                      			                 $my_animal_exp = floor($earned_exp / rand(3, 5));
                        			             if($my_animal_exp)
                                                   $animals[$v_anim['name']]['battle'] = 0;
                                                   $animals[$v_anim['name']]['exp']   += $my_animal_exp;

                      			                   CHAT::chat_attention($user_win, '��� �����'.(($v_anim['animal_login'] != $v_anim['name'])?" �� ������ <b>".$v_anim['animal_login'].'</b>':" <b>".$v_anim['name']."</b>").' ������� <b>'.$my_animal_exp.'</b> �����.', array('addXMPP' => true));
                                              }
                                           }
                        			     sql_query("UPDATE `users` SET `animals` = '".serialize($animals)."' WHERE `id` = '".$user_win['id']."' LIMIT 1;");
        			                  }
                                     }

             $battle_err['status_modif_1'] = 'ok';
             $this->upd_battle_err($battle_err);

                                    #-----------------------------
                                    # ��������� ����
                                      include("functions/battle/fbattle.holiday.php");

                                    #-----------------------------
                                    # ��� ������� ��������
                                	  include ("functions/battle/battle.drop_object.php");

                                    #-----------------------------
                                    # ������ ������ � ������ � ���
                                      add_chronicle_in_battle($user_win);

                                    #-------------------------------
                                    # �������� �����
                                     if ($user_win['ps'] > 0) {
                                       $ps = '`ps` = `ps` + 1, ';
                                     } elseif ($user_win['ps'] <= 0) {
                                       $ps = '`ps` = 1, ';
                                     }

                                    # ��� �������� �����
                                      if (!empty($this->tools['no_ps'])) { $ps = ''; }

                                    #-------------------------------
                                    # ���� ���������� ����� �� ������ � ���
                                      $chances_wheel = '';
                                      if ($user_win['battles_today'] == 0) {
                                         $chances_wheel = '`chances_wheel` = `chances_wheel` + 1, ';
              			                 CHAT::chat_attention($user_win, '�� ������ � ������ ��� �� ������� �� ��������� �������������� ����� ���������� �������������� ������� "������ �����"!', array('addXMPP' => true));
                                      }

                                   #-------------------------------
                                   # ���������� �������� ��� ������
                                   # �������� ���� �� ���
                                   # �������� �� ���
        				             sql_query("UPDATE `users` SET $ps $chances_wheel
                                     `hit_hod` = 0, `hit_hp` = 0, `hit_udar` = 0, `hit_block` = 0, `hit_uvorot` = 0, `hit_krit` = 0,
                                     `win` = `win` + 1, `fullhptime` = ".time().", `fullmptime` = ".time().",
                                     `exp` = `exp` + '".$earned_exp."', `karma` = `karma` + '".$earned_karma."', `valor` = `valor` + '".$earned_valor."',
                                     `battle` = 0, `battles_today` = `battles_today` + 1
                                      WHERE `id` = '".$user_win['id']."' LIMIT 1;");

                                      # if ($user['id'] == $user_win['id']) { $user['battle'] = 0; }

             $battle_err['status_modif_2'] = 'ok';
             $this->upd_battle_err($battle_err);

                                     }
                                   } else {

                                      # ��� ��� �����
                                         $bot_user_attacking['battle'] = 0;
                                         $bot_attacking['battle']      = 0;

                                         $bot_user_defender['battle']  = 0;
                                         $bot_defender['battle']       = 0;

                                        $user_bot = sql_row("SELECT `name`, `prototype`, `is_hbot`, `is_hranbot` FROM `bots` WHERE id = '".$v."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                                        if (!empty($user_bot['prototype'])) {
                                          # �������� �� ���
                                            if ($user_bot['is_hbot'] || $user_bot['is_hranbot']) {
        			                          sql_query("UPDATE `users` SET
                                              `win` = `win` + 1, `fullhptime` = ".time().", `fullmptime` = ".time().",
                                              `battle` = 0
                                               WHERE id = '".$user_bot['prototype']."' LIMIT 1;");
                                            }
                                        }

                                   }
               }
              }

        /***-------------------------------
         * 1. �������� �����
         * 2. ����� ��������� ����� "��� ����������� ��������"
         **/

             include("functions/battle/fbattle.klan_war.php");

        /***-------------------------------
         * 1. �������
         * 2. ����� ��������� ���
         **/

            if ($this->battle_data['type'] == 28) {
             include("functions/battle/fbattle.zarnica.php");
            }

        /***-------------------------------
         * 1. ��������� ����
         **/

            if ($this->battle_data['type'] == 35) {
             include("functions/battle/fbattle.distribution.php");
            }

             $this->write_log();

             $endlog = "��� ��������, ������ �� ";

              # ��������� ��������� ��� � ���� ������� ����� � ���
                if (!empty($win_team) && $this->battle_data['status_battle'] > 0) {
                  $win_exp = array();
                   foreach ($win_team as $k => $v) {
                     $win_exp[$v] = $this->get_points('exp', $v);
                   }

                     $end_str_boy = '';
                     $end_str_sysboy = '';
                     $m_money1 = 10; $m_money2 = 5; $m_money3 = 3;

                       $made = '��� ����������';
                       switch($this->battle_data['status_battle']){
                         case 1: $m_stl_money1 = 0;    $m_stl_money2 = 0;    $m_stl_money3 = 0;    $m_money1 = 10;   $m_money2 = 6;   $m_money3 = 3;   $made = '������ ������� �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>������ �������</b></a> �����������'; break; # ������ �������!
                         case 2: $m_stl_money1 = 300;  $m_stl_money2 = 150;  $m_stl_money3 = 75;   $m_money1 = 100;  $m_money2 = 50;  $m_money3 = 25;  $made = '������ ������������ �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>������ ������������</b></a> �����������'; break; # ������ ������������
                         case 3: $m_stl_money1 = 700;  $m_stl_money2 = 750;  $m_stl_money3 = 175;  $m_money1 = 200;  $m_money2 = 100; $m_money3 = 50;  $made = '��������� ����� �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>��������� �����</b></a> �����������'; break; # ��������� �����!
                         case 4: $m_stl_money1 = 1500; $m_stl_money2 = 750;  $m_stl_money3 = 375;  $m_money1 = 500;  $m_money2 = 250; $m_money3 = 125; $made = '�������������� �������� �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>�������������� ��������</b></a> �����������'; break; # �������������� ��������!
                         case 5: $m_stl_money1 = 4000; $m_stl_money2 = 2000; $m_stl_money3 = 1000; $m_money1 = 1000; $m_money2 = 500; $m_money3 = 250; $made = '������������ �������� �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>������������ ��������</b></a> �����������'; break; # ������������ ��������!
                       }

                     $end_str_bla = '��������� ������ ������� �����';

                     # ������  �����
                       $max_uron = null;
                       $win_user = null;
                       $maxkey = 0;
                       foreach ($win_exp as $key_id => $val_ur) {  if ($val_ur > $max_uron || is_null($max_uron)) { $max_uron = $val_ur; $maxkey = $key_id; } }
                       $pervi_max_uron = $max_uron;

                       if ($maxkey > _BOTSEPARATOR_) {

                            if (empty($this->bots[$maxkey]))
                                 $win_bot = sql_row('SELECT `id`, `name`, `prototype` FROM `bots` WHERE `id` = "'.$maxkey.'" LIMIT 1;');
                            else $win_bot = $this->bots[$maxkey];

                         $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex, achievements_finish, rewards FROM `users` WHERE `id` = '{$win_bot['prototype']}' LIMIT 1;");
                         $win_user['id'] = $win_bot['id'];
                         $win_user['login'] = $win_bot['name'];
                         $win_user['battle'] = $battle_id;
                         $isus = 0;
                       } else { $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex, achievements_finish, rewards FROM `users` WHERE `id` = '{$maxkey}' LIMIT 1;"); $isus = 1; }

                       #
                         if ($m_baseexp[$win_user['level']] > $pervi_max_uron) { # ������ ������ ������ �������� �����
                            $pervi_max_uron = 0;
                         } else {

                           if ($win_user['id']) {
                             if ($win_user['invis']==1) {
                                $end_str_boy .= "<i class='user".($this->userclass($win_user['id'], '')+2)."'>���������</i>";
                             } else {
                                //$end_str_boy .= "<script type=\"text/javascript\">u(\"".$win_user['login']."\",\"".$win_user['level']."\",\"".($win_user['guild']?"":$win_user['align'])."\",\"".($win_user['klan']?str_to_spacelower($win_user['klan']):"")."\",\"".$win_user['klan']."\",\"".($win_user['guild']?'paladins'.$win_user['align']:"")."\",\"".$win_user['guild']."\",\"".($win_user['sex']?"M":"F")."\",\"".$this->userclass($win_user['id'], '')."\",\"%alt%\",0,0,1,".$win_user['top'].");</script>";
                                $end_str_boy .= nick_ofgethtml(nick_ofset($win_user, 0, $this->userclass($win_user['id'], '')), 0, 0);
                             }

                            $end_str_sysboy = _GetUserChatName($win_user);
                              if ($isus) {
                               $sql_rewards = "";

                                #-------------------------------------
                                # ���������� "������ �� ������"
                                  $status_battle = $this->battle_data['status_battle'];
                                  $hideb = $this->battle_data['hideb'];
                                  $no_foodbatl = $this->battle_data['type'] == 20 ? false : true; # ��� ���������

                                  $statused = "";

                                   switch($status_battle){
                                     case 1: $statused = "������ �� ������ - ������ �������";          break; # ������ �������!
                                     case 2: $statused = "������ �� ������ - ������ ������������";     break; # ������ ������������
                                     case 3: $statused = "������ �� ������ - ��������� �����";         break; # ��������� �����!
                                     case 4: $statused = "������ �� ������ - �������������� ��������"; break; # �������������� ��������!
                                     case 5: $statused = "������ �� ������ - ������������ ��������";   break; # ������������ ��������!
                                   }

                                   if ($statused) {
                                     if (!in_array("fight_".$status_battle, explode(";", $win_user['achievements_finish']))) {
                                       achievements($win_user, array('statused_'.$status_battle => array(1 => 1)));

                                       #---------------------------------------------
                                       # ���������� "����������� ����"
                                         achievements($win_user, array('statused_6' => array(1 => array($statused))));
                                     }

                                   # ������ ������������
                                     if ($status_battle == 2) {
                                      $rewards = unserialize($win_user['rewards']);

                                      $n_rewards['name'] = '������ ������������!';
                                      $n_rewards['type'] = 'military_clashes';
                                      $n_rewards['date_create'] = time();

                                      $rewards[] = $n_rewards;
                                      $sql_rewards = ", `rewards` = '".serialize($rewards)."'";
                                      send_user_letter($win_user['id'], '[�������������]', "������� �� ������ ����� � ������ ������������", "��� ���� ������ ������� ��� ������� ����� � <b>������ ������������</b>");
                                     }

                                   # ��������� �����
                                     if ($status_battle == 3) {
                                      $rewards = unserialize($win_user['rewards']);

                                      $n_rewards['name'] = '��������� �����!';
                                      $n_rewards['type'] = 'local_battle';
                                      $n_rewards['date_create'] = time();

                                      $rewards[] = $n_rewards;
                                      $sql_rewards = ", `rewards` = '".serialize($rewards)."'";
                                      send_user_letter($win_user['id'], '[�������������]', "������� �� ������ ����� � ��������� �����", "��� ���� ������ ������� ��� ������� ����� � <b>��������� �����</b>");
                                     }

                                   # ������� �� ������� ����� � ��������� ���
                                     if ($hideb == 1 && isset($m_stl_money1) && $no_foodbatl) {
                                        $sql_rewards = ", `ekr` = `ekr` + '".$m_stl_money1."'";
                                     }
                                   }

                               sql_query("UPDATE `users` SET `money` = `money` + '".$m_money1."' ".$sql_rewards." WHERE `id` = '".$win_user['id']."' LIMIT 1;");
                               send_user_letter($win_user['id'], '[�������������]', "�� ������ ������ �������� ����� ����� ������ � ���", $madesys." ������� ��������� ������ �������, ������� �������� ������� <b>".$m_money1."</b> ��.".(($hideb == 1 && isset($m_stl_money1) && $no_foodbatl)?", ".$m_stl_money1." ���.":""));
                               add_private_matter($win_user['id'], 0, '�� ������ ������ �������� ����� ����� ������ � ���'. $madesys.' ������� ��������� ������ �������, ������� �������� ������� <b>'.$m_money1.'</b> ��.'.(($hideb == 1 && isset($m_stl_money1) && $no_foodbatl)?", ".$m_stl_money1." ���.":""), '[���]');

                              }
                           }
                         }

                     # ������ �����
                       $sql_rewards = "";
                       $win_user = null;
                       $max_uron = null;
                       $maxkey = 0;
                       foreach ($win_exp as $key_id => $val_ur) { if ($pervi_max_uron != $val_ur && ($val_ur > $max_uron || is_null($max_uron))) { $max_uron = $val_ur; $maxkey = $key_id; } }
                       $vtoroy_max_uron = $max_uron;
                       $vtoroy_persent = 0;

                        if ($vtoroy_max_uron > 0 && $pervi_max_uron > 0) {
                         $vtoroy_persent = floor($vtoroy_max_uron * 100 / $pervi_max_uron);
                        }

                         if ($vtoroy_persent >= 90) {

                           if ($maxkey > _BOTSEPARATOR_) {

                            if (empty($this->bots[$maxkey]))
                                  $win_bot = sql_row('SELECT id, name, prototype FROM `bots` WHERE `id` = "'.$maxkey.'" LIMIT 1;');
                            else  $win_bot = $this->bots[$maxkey];

                             $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex FROM `users` WHERE `id` = '{$win_bot['prototype']}' LIMIT 1;");
                             $win_user['id'] = $win_bot['id'];
                             $win_user['login'] = $win_bot['name'];
                             $win_user['battle'] = $battle_id;
                             $isus = 0;
                           } else { $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex FROM `users` WHERE `id` = '{$maxkey}' LIMIT 1;"); $isus = 1;}

                             if ($m_baseexp[$win_user['level']] > $vtoroy_max_uron) { # ������ ������ ������ �������� �����
                                $vtoroy_max_uron = 0;
                             } else {
                                 if ($win_user['id']) {

                                   if ($win_user['invis']) {
                                      $end_str_boy .= ", <i class='user".($this->userclass($win_user['id'], '')+2)."'>���������</i>";
                                   } else {
                                     // $end_str_boy .= ", <script type=\"text/javascript\">u(\"".$win_user['login']."\",\"".$win_user['level']."\",\"".($win_user['guild']?"":$win_user['align'])."\",\"".($win_user['klan']?str_to_spacelower($win_user['klan']):"")."\",\"".$win_user['klan']."\",\"".($win_user['guild']?'paladins'.$win_user['align']:"")."\",\"".$win_user['guild']."\",\"".($win_user['sex']?"M":"F")."\",\"".$this->userclass($win_user['id'], '')."\",\"%alt%\",0,0,1, ".$win_user['top'].");</script>";
                                      $end_str_boy .= ", ".nick_ofgethtml(nick_ofset($win_user, 0, $this->userclass($win_user['id'], '')), 0, 0);
                                   }

                                  $end_str_sysboy .= ', '._GetUserChatName($win_user);
                                  $end_str_bla = '��������� ������� ������ ������';

                                # ������� �� ������� ����� � ��������� ���
                                  if ($hideb == 1 && isset($m_stl_money2) && $no_foodbatl) {
                                     $sql_rewards = ", `ekr` = `ekr` + '".$m_stl_money2."'";
                                  }

                                  if ($isus) {
                                   sql_query("UPDATE `users` SET `money` = `money` + ".$m_money2." ".$sql_rewards." WHERE `id` = '".$win_user['id']."' LIMIT 1;");
                                   send_user_letter($win_user['id'], '[�������������]', "�� ������ ������ ����� ����� ������ � ���", $madesys." ������� ��������� ������ �������, ������� �������� ������� <b>".$m_money2."</b> ��.".(($hideb == 1 && isset($m_stl_money2) && $no_foodbatl)?", ".$m_stl_money2." ���.":""));
                                   add_private_matter($win_user['id'], 0, '�� ������ ������ ����� ����� ������ � ���', $madesys.' ������� ��������� ������ �������, ������� �������� ������� <b>'.$m_money2.'</b> ��.'.(($hideb == 1 && isset($m_stl_money2) && $no_foodbatl)?", ".$m_stl_money2." ���.":""), '[���]');
                                  }
                                 }
                             }
                         }

                     # ������ �����
                       $sql_rewards = "";
                       $win_user = null;
                       $max_uron = null;
                       $maxkey = 0;
                       foreach ($win_exp as $key_id => $val_ur) { if ($pervi_max_uron != $val_ur && $vtoroy_max_uron != $val_ur && ($val_ur > $max_uron || is_null($max_uron))) { $max_uron = $val_ur; $maxkey = $key_id; } }
                       $tretiy_max_uron = $max_uron;
                       $tretiy_persent = 0;

                        if ($vtoroy_max_uron > 0 && $pervi_max_uron > 0) {
                         $tretiy_persent = floor($tretiy_max_uron * 100 / $pervi_max_uron);
                        }

                         if ($tretiy_persent >= 80) {

                         if ($maxkey > _BOTSEPARATOR_) {

                            if (empty($this->bots[$maxkey]))
                                  $win_bot = sql_row('SELECT id, name, prototype FROM `bots` WHERE `id` = "'.$maxkey.'" LIMIT 1;');
                            else  $win_bot = $this->bots[$maxkey];


                           $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex FROM `users` WHERE `id` = '{$win_bot['prototype']}' LIMIT 1;");
                           $win_user['id'] = $win_bot['id'];
                           $win_user['login'] = $win_bot['name'];
                           $win_user['battle'] = $battle_id;
                           $isus = 0;
                         } else { $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex FROM `users` WHERE `id` = '{$maxkey}' LIMIT 1;"); $isus = 1;}

                           if ($m_baseexp[$win_user['level']] > $tretiy_persent) { # ������ ������ ������ �������� �����
                              $tretiy_persent = 0;
                           } else {
                               if ($win_user['id']) {

                                   if ($win_user['invis']) {
                                      $end_str_boy .= ", <i class='user".($this->userclass($win_user['id'], '')+2)."'>���������</i>";
                                   } else {
                                     // $end_str_boy .= ", <script type=\"text/javascript\">u(\"".$win_user['login']."\",\"".$win_user['level']."\",\"".($win_user['guild']?"":$win_user['align'])."\",\"".($win_user['klan']?str_to_spacelower($win_user['klan']):"")."\",\"".$win_user['klan']."\",\"".($win_user['guild']?'paladins'.$win_user['align']:"")."\",\"".$win_user['guild']."\",\"".($win_user['sex']?"M":"F")."\",\"".$this->userclass($win_user['id'], '')."\",\"%alt%\",0,0,1, ".$win_user['top'].");</script>";
                                      $end_str_boy .= ", ".nick_ofgethtml(nick_ofset($win_user, 0, $this->userclass($win_user['id'], '')), 0, 0);
                                   }

                                $end_str_sysboy .= ', '._GetUserChatName($win_user);
                                $end_str_bla = '��������� ������� ������ ������';

                              # ������� �� ������� ����� � ��������� ���
                                if ($hideb == 1 && isset($m_stl_money3) && $no_foodbatl) {
                                   $sql_rewards = ", `ekr` = `ekr` + '".$m_stl_money3."'";
                                }

                                if ($isus) {
                                 sql_query("UPDATE `users` SET `money` = `money` + ".$m_money3." ".$sql_rewards." WHERE `id` = '".$win_user['id']."' LIMIT 1;");
                                 send_user_letter($win_user['id'], '[�������������]', "�� ������ ������ ����� ����� ������ � ���", $madesys." ������� ��������� ������ �������, ������� �������� ������� <b>".$m_money3."</b> ��.".(($hideb == 1 && isset($m_stl_money3) && $no_foodbatl)?", ".$m_stl_money3." ���.":""));
                                 add_private_matter($win_user['id'], 0, '�� ������ ������ ����� ����� ������ � ���', $madesys.' ������� ��������� ������ �������, ������� �������� ������� <b>'.$m_money3.'</b> ��.'.(($hideb == 1 && isset($m_stl_money3) && $no_foodbatl)?", ".$m_stl_money3." ���.":""), '[���]');
                                }
                               }
                           }
                         }

                   if (!empty($end_str_bla) && !empty($end_str_boy) && !empty($made)) {
                     $endlog = " ��� ��������. ".$end_str_bla." ".$end_str_boy." ".$made." ������� ";
                     if (!empty($madesys) && !empty($end_str_sysboy)) {
                       CHAT::chat_city_attention (false, $madesys.' �������! '.$end_str_bla.' '.$end_str_sysboy, array('addXMPP' => true));
                     }
                   }

                }

             $battle_err['status_win_status'] = 'ok';
             $this->upd_battle_err($battle_err);

           ###########################################################
           #  ������� �����������
           ###########################################################

              foreach ($lose_team as $k => $v) { # �����������

               #-----------------------------------------
        	   # ��� ��� ��� �������� ������ ���� ���...

        		  if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���
                    if($v < _BOTSEPARATOR_) {
                       $user_v_lomka = sql_row("SELECT * FROM `users` WHERE `id` = '".$v."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                        if (!empty($user_v_lomka['id'])) {

                         #-------------------------------
                         # ������������ �������
                           $thematic_event = unserialize($user_v_lomka['thematic_event']);

                         #-------------------------------
                         # ������� % ����� �� ���
                           $exp_percent = bot_percent_exp($user_v_lomka, $this->battle_data);

                         #-------------------------------
                         # ���������s�� ���� �� ���
                           $earned_damage = 0 + $this->get_points('damage', $user_v_lomka['id']);

                         #-------------------------------
                         # ����������� ���� �� ���
                           $old_earned_exp = $this->get_points('exp', $user_v_lomka['id']);
                           $this->set_points('exp', 0, $user_v_lomka['id']);
                           $earned_exp = 0;

                         #-------------------------------
                         # ��� ��������� ���� ��������� ��������
                           $old_valor = $this->get_points('valor', $user_v_lomka['id']);
                           $earned_valor  = 0;

                         # ������������ ������� "���� ������"
                           if (!empty($thematic_event['name']) && $thematic_event['name'] == "���� ������") {
                             $earned_valor -= round(sqrt((1 + $old_valor) * sqrt(1 + $old_earned_exp)), 2);
                           } else {
                         # � ��������� ��� �������� ������ ��������
                             $earned_valor -= round((sqrt((1 + $old_valor) * sqrt(1 + $old_earned_exp))) / ($this->battle_data['hideb'] == 1 ? 3 : 1), 2);
                           }

                           $this->set_points('valor', $earned_valor, $user_v_lomka['id']);

                         # ��� ��������
                           if (!empty($this->tools['no_valor'])) {
                            $earned_valor = 0;
                            $this->set_points('valor', 0, $user_v_lomka['id']);
                           }

                         #-------------------------------------------
                         # ������ ���� �������� � �����
                         # ���� ���� ����� ����� �� ����� = ��� ������ �������

                          if (empty($this->tools["break_items"]) && !substr_count($user_v_lomka['eff_all'], "1033;")) {  # ���� ��� ��������

                            $dur_ids = '';

                          # ������������ ������� "���� ������"
                            if (!empty($thematic_event['name']) && $thematic_event['name'] == "���� ������") {
                              $user_item_lomka = sql_query("SELECT `id`, `name`, `maxdur`, `duration`, `type`, `magic` FROM `inventory` WHERE `dressed` = 1 AND (`type` <> 25) AND `type` != 20 AND `owner` = '".$user_v_lomka['id']."';");
                            } else {
                              $user_item_lomka = sql_query("SELECT `id`, `name`, `maxdur`, `duration`, `type`, `magic` FROM `inventory` WHERE `dressed` = 1 AND (`type` <> 25) AND `owner` = '".$user_v_lomka['id']."';");
                            }

          				    while ($r_broken = fetch_array($user_item_lomka)) {
                               if (rand(1, 2) == 1 && (/*$r_broken['type'] != 20 && */$r_broken['type'] != 39 && $r_broken['type'] != 40 && $r_broken['type'] != 41 && $r_broken['type'] != 42)){

                                 $file = "adata/inventory_data/".$user_v_lomka['id'].".txt";
                                 if (is_file($file)) {
                                  global $itemtype_to_name;
                                  $battle_inventory = unserialize(file_get_contents($file));       # ���������
                                   if ($battle_inventory[type_dreass_item($r_broken, $user_v_lomka)]['id'] == $r_broken['id']) {
                                    $battle_inventory[type_dreass_item($r_broken, $user_v_lomka)]['duration'] = $r_broken['duration'] + 1;
                                    file_put_contents($file, serialize($battle_inventory), LOCK_EX); # ���������
                                   }
                                 }

                                 if ($dur_ids) $coma = ','; else $coma = '';
                                 $dur_ids .= $coma.$r_broken['id'];

                                 $r_broken['duration'] += 1;

              				     if (fun_broken_item($r_broken) && $r_broken['type'] != 20){
              					  	$this->add_log('� '.nick_team($user_v_lomka, $this->userclass($v)).' ������� "'.$r_broken['name'].'" � ����������� ���������!');
                                	$this->write_log();
              					 }
                               }
          				    }

                             if ($dur_ids) {
                              sql_query("UPDATE `inventory` SET `duration` = `duration` + 1 WHERE `owner` = '".$user_v_lomka['id']."' AND `id` in(".$dur_ids.") ;");
                             }
                          }

                         #-------------------------------------------
                         # ����������� ��� ������ ������ �����������
                           if($this->battle_data['blood'] == 1) { $tr = settravma($user_v_lomka, 75); } else { $tr = settravma($user_v_lomka, 10); }
                           if ($tr) {
                             $this->add_log(nick_team($user_v_lomka, $this->userclass($v)).$tr);
                             $this->write_log();
                           }

                         #-------------------------------
                         # ����� � ��� "���������"
        				   CHAT::chat_attention($user_v_lomka, '<a href="http://'.$user_v_lomka['incity'].'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>���</b></a> ��������. ����� ���� �������� �����: <b>'.conv_hundredths($earned_damage, 0).'</b>. �������� �����: <b>0</b>. �����: <b>0.00</b>. ��������: <b>'.$earned_valor.'</b>.', array('addXMPP' => true));

                         #-------------------------------
                         # �������� �����
                          if ($user_v_lomka['ps'] >= 0) {
                            $ps = '`ps` = -1, ';
                          } elseif ($user_v_lomka['ps'] < 0) {
                            $ps = '`ps` = `ps` - 1, ';
                          }

                         # ��� �������� �����
                           if (!empty($this->tools['no_ps'])) { $ps = ''; }

                         #-------------------------------
                         # �������� ���� �� ���
                         # ���� �������� + ��� - � ���� ���������
                         # ������ �� � ����� �� 0
                         # �������� �� ���
                           sql_query("UPDATE `users` SET $ps
                           `hit_hod` = 0, `hit_hp` = 0, `hit_udar` = 0, `hit_block` = 0, `hit_uvorot` = 0, `hit_krit` = 0,
                           `valor` = `valor` + ".$earned_valor.", `lose` = `lose` + 1,
                           `hp` = 0, `hp_precise` = 0, `mana` = 0, `pw_precise` = 0, `fullhptime` = ".time().", `fullmptime` = ".time().",
                           `battle` = 0
                           WHERE `id` = '".$user_v_lomka['id']."' LIMIT 1;");

                           /*if ($user['id'] == $user_v_lomka['id']) {
                             $user['hp'] = 0;
                             $user['mana'] = 0;
                             $user['battle'] = 0;
                           }*/

                         #---------------------------------------------
                         # ���� �������� � ����� �� ����� � ��� ��� � ������� �����
                           if ($user_v_lomka['klan']) {
                             $klan_valor = $earned_valor;
                             CHAT::chat_attention($user_v_lomka, '�������� �������� ��������:  <b>'.$klan_valor.'</b>.', array('addXMPP' => true));
                             sql_query("UPDATE `clans` SET `clanexp` = `clanexp` + '".$klan_valor."' WHERE `name` = '".$user_v_lomka['klan']."' LIMIT 1;");

                           # ����� ���������� ���� ��� ��� �������� "��� �����"
                             if ($this->battle_data['type'] == 14) {
        				       sql_query("UPDATE `enterlog` SET
                               `klan_losewar_isday` = `klan_losewar_isday` + 1,
                               `klan_iswar_isday` = `klan_iswar_isday` + 1,
                               `klan_valor_isday` = `klan_valor_isday` + ".$klan_valor."
                               WHERE `owner` = '".$user_v_lomka['id']."' AND `type` = 0 LIMIT 1;");
                             } else {
                              sql_query("UPDATE `enterlog` SET `klan_valor_isday` = `klan_valor_isday` + ".$klan_valor." WHERE `owner` = '".$user_v_lomka['id']."' AND `type` = 0 LIMIT 1;");
                             }
                           }


                         #-----------------------------
                         # ��� ��������� = ��� �������
                           include("functions/battle/speciallose.php");

                        }

                    } else {
                      $user_bot = sql_row("SELECT `name`, `prototype`, `is_hbot`, `is_hranbot` FROM `bots` WHERE `id` = '".$v."' LIMIT 1;");
                      if (!empty($user_bot['prototype'])) {

                        if ($user_bot['is_hbot'] || $user_bot['is_hranbot']) {
                          sql_query("UPDATE `users` SET `battle` = 0, `lose` = `lose` + 1 WHERE `id` = '".$user_bot['prototype']."' LIMIT 1;");
                        }

                      }

                    }
        		  }
              }

             $battle_err['status_lose_team'] = 'ok';
             $this->upd_battle_err($battle_err);


          # ������ �����������
            if ($statusbattle_end == 1) {
               $winers .= implode(", ", $team1);
            } else {
               $winers .= implode(", ", $team2);
            }

              # ��� ������� ��������
        	  # include ("functions/battle/battle.drop_object.php");

                $this->add_log($endlog.$winers);
                $this->write_log();

          #-----------------------------
          # ������ � ��������
            if (in_array($this->battle_data['type'], array(19, 20, 22, 29)) || $this->battle_data['hideb'] == 1) {
               include($_SERVER["DOCUMENT_ROOT"]."/functions/battle/ratings.php");
            }

            } elseif ($statusbattle_end == 3) {

           /***------------------------------------------
            * �����
            **/

             $battle_err['nich_start'] = 'ok';
             $this->upd_battle_err($battle_err);

             $this->add_log('��� ��������. <b>�����</b>.');
             $this->write_log();

             $battle_err['nich_write_log'] = 'ok';
             $this->upd_battle_err($battle_err);

              # ������� ������ � ������
        	    foreach ($nich_team as $v => $k) {
        		 if (!in_array($k, $this->texit)) { # �� ��� ����� �� ���
                  if($k > _BOTSEPARATOR_) {

                      $user_bot = sql_row("SELECT `prototype`, `is_hbot`, `is_hranbot` FROM `bots` WHERE `id` = '".$k."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1 ;");
                      $user_nich = $user_bot;
                        if ($user_nich['is_hbot'] || $user_nich['is_hranbot']) {
                          sql_query("UPDATE `users` SET `battle` = 0, `nich` = `nich` + 1, `hp` = 0, `hp_precise` = 0, `fullhptime` = ".time().", `mana` = 0, `pw_precise` = 0, `fullmptime` = ".time()." WHERE `id` = '".$user_bot['prototype']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                        }
                      sql_query('UPDATE `bots` SET `battle` = 0, `hp` = 0 WHERE `id` = "'.$k.'" LIMIT 1;');
                  } else {
                      $user_nich = sql_row("SELECT * FROM `users` WHERE id = '".$k."' LIMIT 1;");

                      $user_draw = new UserControl($user_nich);
                      $user_draw->USER_load_inventory_data();
                      $user_draw->del_cur_mysql = false;

                    #-------------------------------------------
                    # ������������ �������
                      $thematic_event = unserialize($user_nich['thematic_event']);

                    #-------------------------------------------
                    # ������������ ���� �� ���
                      $earned_damage = conv_hundredths($this->get_points('damage', $user_nich['id']), 0);
                      $this->set_points('damage', $earned_damage, $user_nich['id']);

                    #-------------------------------------------
                    # ����������� ����� �� ���
                      $earned_karma = conv_hundredths($this->get_points('karma', $user_nich['id']), 2);
                      $this->set_points('karma', $earned_karma, $user_nich['id']);

                    #-------------------------------------------
                    # �������� ���� �� ���
                      sql_query('UPDATE `users` SET
                      `hit_hod` = 0, `hit_hp` = 0, `hit_udar` = 0, `hit_block` = 0, `hit_uvorot` = 0, `hit_krit` = 0,
                      `battle` = 0, `battles_today` = `battles_today` + 1, `nich` = `nich` + 1,
                      `hp` = 0, `hp_precise` = 0, `fullhptime` = '.time().',
                      `mana` = 0, `pw_precise` = 0, `fullmptime` = '.time().',
                      `karma` = `karma` + '.$earned_karma.'
                       WHERE `id` = "'.$user_nich['id'].'" LIMIT 1;');

                       /*if ($user['id'] == $user_nich['id']) {
                          $user['hp'] = 0;
                          $user['mana'] = 0;
                          $user['battle'] = 0;
                       }*/

                     #-------------------------------------------
                     # ������ ���� �������� � �����
                     # ���� ���� ����� ����� �� ����� = ��� ������ �������

                      if (empty($this->tools["break_items"]) && !substr_count($user_nich['eff_all'], "1033;")) {  # ���� ��� ��������

                        $dur_ids = '';

                      #-------------------------------
                      # ������������ ������� "���� ������"
                        if (!empty($thematic_event['name']) && $thematic_event['name'] == "���� ������") {
                          $user_item_lomka = sql_query("SELECT `id`, `name`, `maxdur`, `duration`, `type`, `magic` FROM `inventory` WHERE `dressed` = 1 AND (`type` <> 25) AND `type` != 20 AND `owner` = '".$user_nich['id']."';");
                        } else {
                          $user_item_lomka = sql_query("SELECT `id`, `name`, `maxdur`, `duration`, `type`, `magic` FROM `inventory` WHERE `dressed` = 1 AND (`type` <> 25) AND `owner` = '".$user_nich['id']."';");
                        }

      				    while ($r_broken = fetch_array($user_item_lomka)) {
                           if (rand(1, 2) == 1 && (/*$r_broken['type'] != 20 && */$r_broken['type'] != 39 && $r_broken['type'] != 40 && $r_broken['type'] != 41 && $r_broken['type'] != 42)){

                               if ($user_draw->inventory_data[type_dreass_item($r_broken, $user_nich)]['id'] == $r_broken['id']) {
                                $user_draw->inventory_data[type_dreass_item($r_broken, $user_nich)]['duration'] = $r_broken['duration'] + 1;
                                $user_draw->mod_inventory_data = true;
                                $user_draw->USER_save_inventory_data();
                               }

                             if ($dur_ids) $coma = ','; else $coma = '';
                             $dur_ids .= $coma.$r_broken['id'];

                             $r_broken['duration'] += 1;

          				     if (fun_broken_item($r_broken) && $r_broken['type'] != 20){
          					  	$this->add_log('� '.nick_team($user_nich, $this->userclass($v)).' ������� "'.$r_broken['name'].'" � ����������� ���������!');
                            	$this->write_log();
          					 }
                           }
      				    }

                         if ($dur_ids) {
                          sql_query("UPDATE `inventory` SET `duration` = `duration` + 1 WHERE `owner` = '".$user_nich['id']."' AND `id` in(".$dur_ids.") ;");
                         }
                      }

                    #-------------------------------
                    # ����� ���������� ���� ��� ��� ��������
                      if ($user_nich['klan']) {
                       if ($this->battle_data['type'] == 14) {
                         sql_query("UPDATE `enterlog` SET
                         `klan_losewar_isday` = `klan_losewar_isday` + 1,
                         `klan_iswar_isday` = `klan_iswar_isday` + 1
                          WHERE `owner` = '".$user_nich['id']."' AND `type` = 0 LIMIT 1;");
                       }
                      }

                    # ����� � ���
                      CHAT::chat_attention($user_nich, '<a href="http://'.$this->battle_data['city'].'.blutbad.ru/log='.$battle_id.'" target="_blank"><b>���</b></a> ��������. ����� ���� �������� �����: <b>'.$earned_damage.'</b>. �������� �����: <b>0</b>. �����: <b>'.$earned_karma.'</b>. ��������: <b>0.00</b>.', array('addXMPP' => true));

                    # ��� ������ = ��� �������
                      include("functions/battle/specialnich.php");
                  }

                  $this->set_points('exp', 0, $k);     # �������� �����
                  $this->set_points('valor', 0, $k);   # �������� ��������

        		 }
        	    }

             $battle_err['nich_t1'] = 'ok';
             $this->upd_battle_err($battle_err);

             # C����� ����� ��� ���
          	   sql_query("UPDATE `battle` SET `win` = 0 WHERE `id` = '".$this->battle_data['id']."';");
               $this->battle_data['win'] = 0;

             #--> �������� ���� ���� � �������� �� 0
             #	$this->exp = array();
      	     #	$this->valor = array();

            }

             $battle_err['status_ends1'] = 'ok';
             $this->upd_battle_err($battle_err);

           # ���������� ������ ������� ��� ����
             $rr = "";
        	 if ($flag == 1 || $flag == 2 || $flag == 3) {
              $t1count = 0;
              $t2count = 0;

               $rr_e = "";
               $rr .= "<span>";
            	   foreach ($nks1 as $k => $v) {
            	     $t1count++;
            	     $dcoma = "";
            	     if ($k > 0) {$dcoma = ", "; }
                     $rr .= $dcoma."<b>".$v."</b>";
            	   }
               $rr .= "</span>";
               if ($flag == 1) $rr .= " <img src=http://img.blutbad.ru/i/flag.gif>";
               $rr .= " � ";
               $rr .= "<span>";
            	   foreach ($nks2 as $k => $v) {
            	     $t2count++;
            	     $dcoma = "";
            	     if ($k > 0) {$dcoma = ", "; }
                     $rr .= $dcoma."<b>".$v."</b>";
            	   }
               $rr .= "</span>";

               if ($flag == 2) $rr .= " <img src=http://img.blutbad.ru/i/flag.gif>";

               if ($flag != 3 && $t1count > 1 && $t2count > 1) {
                 if ($flag == 1) {
                   $rr_e = " �������� <b style=\" color: #BB5500; \">�������</b> �������";
                 } elseif ($flag == 2) {
                   $rr_e = " �������� <b style=\" color: #0055BB; \">�����</b> �������";
                 }
               }
        	 } else {
        		$rr = implode("</b>, <b>", $nks1)."</b> � <b>".implode("</b>, <b>",$nks2);
                $rr_e = " <b>�����</b>";
        	 }

           # �������� ���� �� ���
             $idbattle   = $battle_id;
             $roombattle = $this->battle_data['room'];
         	 sql_query("UPDATE `battle` SET `teams` = '', `t1list` = '".serialize($this->t1list)."', `t2list` = '".serialize($this->t2list)."', `b_hint_nbp` = '', `battle_end` = 3 WHERE `id` = '".$idbattle."' LIMIT 1;");

           	 sql_query("DELETE FROM `bots` WHERE `battle` = '".$idbattle."' OR `battle` = 0 ;");

             if (empty($this->tools['b_rooms'])) {
      	        CHAT::chat_system ($this->user, "<a href=\"http://".$this->battle_data['city'].".blutbad.ru/log=".$idbattle."\" target=_blank><b>���</b></a> ����� ".$rr." ��������. ".$rr_e, $roombattle, array('addXMPP' => true));
             } else {
                $b_rooms = $this->tools['b_rooms'];
                foreach ($b_rooms as $k => $b_room) {
      	          CHAT::chat_system ($this->user, "<a href=\"http://".$this->battle_data['city'].".blutbad.ru/log=".$idbattle."\" target=_blank><b>���</b></a> ����� ".$rr." ��������. ".$rr_e, $b_room, array('addXMPP' => true));
                }
             }

             $id_city = get_city(INCITY, 1);
             XMPP::battleRoomManager($id_city, $idbattle, 2);
        	 XMPP::groupRoomManager($id_city, $idbattle, 1, 2);
        	 XMPP::groupRoomManager($id_city, $idbattle, 2, 2);
             Cache::addXml(XMPP::xml());

             unset($this->battle);

             $this->battle_data['teams'] = '';
             $this->battle_data['battle_eff'] = $this->battle_eff;
             $this->battle_data['b_hint_nbp'] = '';
             $this->battle_data['battle_end'] = 3;

             $this->return = 0;
           return true;
          }
         }
       }
      return false;
     }
    }

/***--------------------------
 * �������� ���������� �������� � ���
 * (�� ��� ���������� ����� �� ����)
 **/
    function spare_no($is_spare) {
      global $data_battle;

	  $is_spare_batl = $data_battle;

	  if ($is_spare_batl['id'] && $this->get_timeout() && ($is_spare_batl['win'] == 3 && $is_spare_batl['battle_end'] == 0))  {
         $this->sort_teams($is_spare['id']);
         sql_query("UPDATE `battle` SET `battle_end` = 2, us_nich = '".$is_spare['id']."' WHERE `id` = '".$this->battle_data['id']."' AND us_nich = '' LIMIT 1;");
       //  sql_query("UPDATE `battle` SET  us_nich = '".$is_spare['id']."' WHERE `id` = '".$this->battle_data['id']."' AND us_nich = '' LIMIT 1;");
         $this->battle_data['battle_end'] = 2;

       # ���� �����
         $this->add_points('karma', -1, $is_spare['id']);

       # ����� � ���
         $is_spare['login'] = $is_spare['name'];
         $is_spare['invis'] = $is_spare['invis'] == 1 ? 1 : 0;
         //$is_spare['invis'] = 0;

         $this->add_log(nick_team($is_spare, $this->userclass($is_spare['id']))." ��������".isusersex($is_spare['prototype'], 2)." ��� �� ��������.");

        # ������� �����������
          foreach ($this->team_enemy as $v => $k) {
             if (!in_array($k, $this->texit)) { # �� ��� ����� �� ���
                if($k > _BOTSEPARATOR_) {
                    $user_bot = sql_row("SELECT `id`, `prototype`, `hp`, `name`, `is_hbot`, `is_hranbot` FROM `bots` WHERE id = '".$k."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                    if ($user_bot['is_hbot'] || $user_bot['is_hranbot']) {
                      sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0, `fullhptime` = ".time().", `mana` = 0, `pw_precise` = 0, `fullmptime` = ".time()." WHERE `id` = '".$user_bot['prototype']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                    }

                    if (sql_query('UPDATE `bots` SET `hp` = 0 WHERE `id` = "'.$k.'" AND `hp` > 0 LIMIT 1;')) {
                       $death_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$user_bot['prototype']."' LIMIT 1;");
                       $death_user['bot_id']  = $user_bot['id'];
                       $death_user['login']   = $user_bot['name'];
                       $death_user['hp']     = 0;
                       $this->fast_death_to($death_user, 1);
                    }
                } else {
                     $death_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$k."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");

                     if (!empty($death_user['id'])) {

                       $death_user['bot_id'] = $death_user['id'];
                       if (sql_query('UPDATE `users` SET `hp` = 0, `hp_precise` = 0, `fullhptime` = '.time().', `mana` = 0, `pw_precise` = 0, `fullmptime` = '.time().' WHERE `id` = "'.$k.'" AND `hp` > 0 LIMIT 1;')) {
                          $death_user['hp']   = 0;
                          $death_user['mana'] = 0;
                          $this->fast_death_to($death_user, 1);
                       }

                       if(!$this->battle_data['blood']) { # ���� ��� �� ��������
                          $tr = settravma($k, 75);
                          if (!empty($tr)) {
                            $this->add_log(nick_team($death_user, $this->userclass($k)).$tr);
                          }
                       }
                     }
                }
             }
          }

        $this->fast_team_death(2);

      # �������� �����
       // $this->update_battle();
       // $this->battle_end($is_spare['battle']);
       // $this->write_log ();
      //  header ("Location:fbattle.php"); die("");
	  }
    }

/***------------------------------------------
 * ��������:
 **/

    function add_points($type, $value = 0, $us_id) {

     if ($type == 'karma') {
       $value = $value;      # ������������� ���������� ������� ����� number_format($value, 4,".","");
     } elseif ($type == 'h' && $value < 0) {
       $value = 0;           # ���� �� ������ ���� �� ������ ����
     } else {
       $value = abs(number_format($value, 4,".",""));
     }

      if (!empty($this->t1list[$us_id])) {
         if (empty($this->t1list[$us_id][$type])) {
          $this->t1list[$us_id][$type] = $value;
         } else {
          $this->t1list[$us_id][$type] += $value;
         }
      } else {
         if (empty($this->t2list[$us_id][$type])) {
          $this->t2list[$us_id][$type] = $value;
         } else {
          $this->t2list[$us_id][$type] += $value;
         }
      }
    }

/***------------------------------------------
 * ��������:
 **/

    function set_points($type, $value = 0, $us_id) {
      if (!empty($this->t1list[$us_id])) {
        $this->t1list[$us_id][$type] = $value;
      } elseif (!empty($this->t2list[$us_id])) {
        $this->t2list[$us_id][$type] = $value;
      } else {
        //
      }
    }

/***------------------------------------------
 * ����������:
 **/

    function get_points($type, $us_id) {

      if (!empty($this->t1list[$us_id])) {

        if (empty($this->t1list[$us_id][$type])) {
         return 0;
        } else {
         return $this->t1list[$us_id][$type];
        }

      } elseif (!empty($this->t2list[$us_id])) {

        if (empty($this->t2list[$us_id][$type])) {
         return 0;
        } else {
         return $this->t2list[$us_id][$type];
        }

      } else {

        return 0;
      }
    }

/***------------------------------------------
 * ����� � ��� ���� ���������
 **/

    function add_write_drop($winer_user, $itemname, $bots, $itimg = 0, $img = '') {
      if ($itimg) {
       $wrimg = "'".$itemname."'";
      } else {
       $wrimg = '<img alt="'.$itemname.'" class="tooltip" src="http://img.blutbad.ru/i/'.$img.'" title="'.$itemname.'" style=" vertical-align: middle; ">';
      }

      if($winer_user['sex'] == 0) {$sex="a";}else{$sex="";}
      $sdrop = nick_team($winer_user, $this->userclass($winer_user['id']))." �������".$sex." ������� ".$wrimg."";

      $this->add_log($sdrop.' � <font class="user'.$this->userclass($bots['id']).'">'.$bots['name'].'</font> ');
      $this->write_log ();
      CHAT::chat_attention($winer_user, '�� �������� ������� "'.$itemname.'" � <b>'.$bots['name'].'</b>', array('addXMPP' => true));
    }

    function add_drop($itn, $itemrnd, $winer_user, $bots) {

      $rnd = rand(1, 100);
      switch($itn){
       case 1: # ��������� +25
       case 2: # �������������� +25
       case 3: # ��������� +50
       case 4: # �������������� +50
       case 5: # ��������� +100
       case 6: # �������������� +100
       case 7: # ��������� +250

       $made = '';
          switch($itn){
           case 1: $made = '��������� +25'; break;
           case 2: $made = '�������������� +25'; break;
           case 3: $made = '��������� +50'; break;
           case 4: $made = '�������������� +50'; break;
           case 5: $made = '��������� +100'; break;
           case 6: $made = '�������������� +100'; break;
           case 7: $made = '��������� +250'; break;
          }

        if (!empty($made) && $rnd <= $itemrnd) {
          $item = sql_row("SELECT * FROM `shop` WHERE `name` = '".$made."' LIMIT 1;");
          if ($item['id']) {
            $item['podzem'] = 1;
            $item['cost'] = 0;
            $item['foronetrip'] = 1;
            fun_create_shop($item, $winer_user['id']);
            $this->add_write_drop($winer_user, $made, $bots, 0, $item['img']);
          }
        }
       break;

       case 8: # ������� -������� �������-
       case 9: # ������� -������-
       case 10: # ������� -����-
       case 11: # ������� -�����-
       case 12: # ������� -���-
       case 13: # ������� -�������-
       case 14: # ������� -������-

       case 24: # ������� -��������-
       case 25: # ������ -����-
       case 26: # ������� -������-
       case 27: # ������� -�����-
       case 28: # ������� -����� ������-
       case 29: # ������� -�������-
       case 30: # ������� -�������-
       case 31: # ������� -����� ������-
       case 32: # ������� -�����-
       case 33: # ������� -�������-
       case 34: # ������� -���-
       case 35: # ������� �� ��� ����
          $made = '';
          switch($itn){
           case 8: $made = '������� -������� �������-'; break;
           case 9: $made = '������� -������-'; break;
           case 10: $made = '������� -�������-'; break;
           case 11: $made = '������� -�����-'; break;
           case 12: $made = '������� -���-'; break;
           case 13: $made = '������� -�������-'; break;
           case 14: $made = '������� -������-'; break;

           case 24: $made = '������� -��������-'; break;
           case 25: $made = 'C������ -����-'; break;
           case 26: $made = '������� -������-'; break;
           case 27: $made = '������� -�����-'; break;
           case 28: $made = '������� -����� ������-'; break;
           case 29: $made = '������� -�������-'; break;
           case 30: $made = '������� -�������-'; break;
           case 31: $made = '������� -����� ������-'; break;
           case 32: $made = '������� -�����-'; break;
           case 33: $made = '������� -�������-'; break;
           case 34: $made = '������� -���-'; break;
           case 35: $made = '������� �� ��� ����'; break;
          }

        if (!empty($made) && $rnd <= $itemrnd) {
          $item = sql_row("SELECT * FROM `shop` WHERE `name` = '".$made."' LIMIT 1;");
          if ($item['id']) {
            $item['cost'] = 0;
            $item['foronetrip'] = 1;
            fun_create_shop($item, $winer_user);
            $this->add_write_drop($winer_user, $made, $bots, 1, $item['img']);
          }
        }
       break;

       case 15: # ������������
       case 16: # ��������� +100
       case 17: # ��������� +250
       case 18: # ��������� +100 ����� �� ���
       case 19: # ������� ��������� +500
       case 20: # ����� ����
       case 21: # ���� ����
       case 22: # ����� ����
       case 23: # ���� ����
          $made = '';
          switch($itn){
           case 15: $made = '������������'; break;
           case 16: $made = '��������� +100'; break;
           case 17: $made = '��������� +250'; break;
           case 18: $made = '��������� +100'; break;
           case 19: $made = '������� ��������� +500'; break;
           case 20: $made = '����� ����'; break;
           case 21: $made = '���� ����'; break;
           case 22: $made = '����� ����'; break;
           case 23: $made = '���� ����'; break;
          }

        if (!empty($made) && $rnd <= $itemrnd) {
          $item = sql_row("SELECT * FROM `berezka` WHERE `name` = '".$made."' LIMIT 1;");
          if ($item['id']) {
           $item['ecost'] = 0;
           $item['foronetrip'] = 1;

           if (in_array($itn, array(20, 21, 22, 23)) ) { $item['maxdur'] = 2; }

           fun_create_shop_exclusive($item, $winer_user['id']);
           $this->add_write_drop($winer_user, $made, $bots, 0, $item['img']);
          }
        }
       break;

      }
    }


    function add_drop_item($itn_name, $itemrnd, $winer_user, $bots, $ex = 0) {

      $rnd = rand(1, 100);
      switch($itn_name){
       case '��������� +25':
       case '�������������� +25':
       case '��������� +50':
       case '�������������� +50':
       case '��������� +100':
       case '�������������� +100':
       case '��������� +250':
       case '�����������':

       case '��������� � ��������':
       case '��������� � ��������':

       case '������������':
       case '��������� +100':
       case '��������� +250':
       case '����� �� ���':
       case '������� ��������� +500':
       case '����� ����':
       case '���� ����':
       case '����� ����':
       case '���� ����':

     # ex

       case '��������� +100':
       case '������� ��������� +500':
       case '�������������� +100':
       case '��������� +250':
       case '�������������� +250':
       case '������������':

            if (!empty($itn_name) && $rnd <= $itemrnd) {

             if ($ex) {

                $item = sql_row("SELECT * FROM `berezka` WHERE `name` = '".$itn_name."' LIMIT 1;");
                if ($item['id']) {
                  $item['podzem'] = 1;
                  $item['ecost'] = 0;
                  $item['foronetrip'] = 1;

                  fun_create_shop_exclusive($item, $winer_user['id']);
                  $this->add_write_drop($winer_user, $itn_name, $bots, 0, $item['img']);
                }

             } else {

                $item = sql_row("SELECT * FROM `shop` WHERE `name` = '".$itn_name."' LIMIT 1;");
                if ($item['id']) {
                  $item['podzem'] = 1;
                  $item['cost'] = 0;
                  $item['foronetrip'] = 1;
                  fun_create_shop($item, $winer_user['id']);
                  $this->add_write_drop($winer_user, $itn_name, $bots, 0, $item['img']);
                }

             }

            }

       break;

       case '������� -������� �������-':
       case '������� -������-':
       case '������� -����-':
       case '������� -�����-':
       case '������� -���-':
       case '������� -�������-':
       case '������� -������-':
       case '������� -��������-':
       case '������� -����-':
       case '������� -������-':
       case '������� -�����-':
       case '������� -����� ������-':
       case '������� -�������-':
       case '������� -�������-':
       case '������� -����� ������-':
       case '������� -�����-':
       case '������� -�������-':
       case '������� -���-':
       case '������� �� ��� ����':

            if (!empty($itn_name) && $rnd <= $itemrnd) {
              $item = sql_row("SELECT * FROM `shop` WHERE `name` = '".$itn_name."' LIMIT 1;");
              if ($item['id']) {
                $item['cost'] = 0;
                $item['foronetrip'] = 1;
                fun_create_shop($item, $winer_user);
                $this->add_write_drop($winer_user, $itn_name, $bots, 1, $item['img']);
              }
            }

       break;

      }
    }


/***--------------------------
 * ��������� ������ � ���
 **/

    function getudarintlog($pos, $type) {
    # $pos == 1 - �����
    # $pos == 2 - ������
    $eee = '.';
        if ($pos == 1) { # ������� �����
            switch($type){
             case 'udar': $eee = '2';break;
             case 'uvorot': $eee = '1';break;
             case 'block': $eee = '0';break;
             case 'krit': $eee = '3';break;
             case 'krita': $eee = '4';break;
            }
        }
        if ($pos == 2) { # ������� ������
            switch($type){
             case 'udar': $eee = '7';break;
             case 'uvorot': $eee = '6';break;
             case 'block': $eee = '5';break;
             case 'krit': $eee = '8';break;
             case 'krita': $eee = '9';break;
            }
        }
        if ($pos == 0) { # ��� �������
            switch($type){
             case 'udar': $eee = 'c';break;
             case 'uvorot': $eee = 'b';break;
             case 'block': $eee = 'a';break;
             case 'krit': $eee = 'd';break;
             case 'krita': $eee = 'e';break;
            }
        }
     return $eee;
    }

/***--------------------------
 * ������ �� ��������� ��������
 *
 * $type = ����� ���� ����� (������ ���� ���)
 * $uron = �� ������� �������
 * $udar_in = ���� ����
 * $udar_pos = ������� �����
 **/

    function set_user_statis($us_stat, $type, $hetype, $uron=0, $ataka=0, $pos=0, $defend=0){

     $us_id = $us_stat['id'];

    if (empty($this->statis[$us_id])) { $this->statis[$us_id] = fun_new_statis($us_stat); }

      if ($ataka && $type) {
        $udar_in1 = '.';$udar_in2 = '.';$udar_in3 = '.';$udar_in4 = '.';
        $udar_protection1 = '.';$udar_protection2 = '.';$udar_protection3 = '.';$udar_protection4 = '.';

           switch($ataka){
               case 1:
                  $udar_in1 = $this->getudarintlog($pos, $type); # udars = ���������� ������
               break;
               case 2:
                  $udar_in2 = $this->getudarintlog($pos, $type); # udars = ���������� ������
               break;
               case 3:
                  $udar_in3 = $this->getudarintlog($pos, $type); # udars = ���������� ������
               break;
               case 4:
                  $udar_in4 = $this->getudarintlog($pos, $type); # udars = ���������� ������
               break;
          }
                  $this->statis[$us_id][1][0] = $this->statis[$us_id][1][0].$udar_in1; # udars = ���������� ������
                  $this->statis[$us_id][1][1] = $this->statis[$us_id][1][1].$udar_in2; # udars = ���������� ������
                  $this->statis[$us_id][1][2] = $this->statis[$us_id][1][2].$udar_in3; # udars = ���������� ������
                  $this->statis[$us_id][1][3] = $this->statis[$us_id][1][3].$udar_in4; # udars = ���������� ������

                  if($this->get_points('shit', $us_stat['id'])) {
                       if ($defend==1 || $defend==4 || $defend==6) {$udar_protection1 = $this->getudarintlog($pos, $hetype);}
                       if ($defend==1 || $defend==2 || $defend==5) {$udar_protection2 = $this->getudarintlog($pos, $hetype);}
                       if ($defend==2 || $defend==3 || $defend==6) {$udar_protection3 = $this->getudarintlog($pos, $hetype);}
                       if ($defend==3 || $defend==4 || $defend==5) {$udar_protection4 = $this->getudarintlog($pos, $hetype);}
                  } else {
                       if ($defend==1) {$udar_protection1 = $this->getudarintlog($pos, $hetype);}
                       if ($defend==1) {$udar_protection2 = $this->getudarintlog($pos, $hetype);}
                       if ($defend==2) {$udar_protection3 = $this->getudarintlog($pos, $hetype);}
                       if ($defend==3) {$udar_protection4 = $this->getudarintlog($pos, $hetype);}
                  }

                  $this->statis[$us_id][2][0] = $this->statis[$us_id][2][0].$udar_protection1; # udars = ���������� ������
                  $this->statis[$us_id][2][1] = $this->statis[$us_id][2][1].$udar_protection2; # udars = ���������� ������
                  $this->statis[$us_id][2][2] = $this->statis[$us_id][2][2].$udar_protection3; # udars = ���������� ������
                  $this->statis[$us_id][2][3] = $this->statis[$us_id][2][3].$udar_protection4; # udars = ���������� ������
      }

        # $this->statis[$us_id][0][1] = $duser; # �����

         /*$this->statis[$us_id][0][3] = $duser['hp']; # hp
         $this->statis[$us_id][0][4] = $duser['maxhp']; # maxhp
         $this->statis[$us_id][0][5] = $duser['mana']; # mana
         $this->statis[$us_id][0][6] = $duser['maxmana']; # maxmana*/

         if ($uron) {
             $this->statis[$us_id][5][0] += $uron; # uron
              if ($type == 'krit' || $type == 'krita') {
                 $this->statis[$us_id][5][1] += $uron; # krituron
              }
         }
    }


/***--------------------------------------
 * ��������� �������� ����� �� ��������
 **/

      function fast_death_def($defeated_id, $bot_defeated) {
  	   # ������� ������

  	     if($this->battle) {

            # ���� ���� ���� � �� �� � ��� �� �������� ��� �� ����
  	    	  if(($defeated_id > _BOTSEPARATOR_ && empty($bot_defeated['hp'])) || ($bot_defeated['id'] && ($bot_defeated['battle'] != $this->battle_data['id'] || $bot_defeated['hp'] <= 0))) {
               # ������� �� ������
  		         unset($this->battle[$defeated_id]);

               # ������� ���� �������
                 unset($this->b_hint_nbp[$defeated_id]);

  		       	foreach ($this->battle as $kak => $vav) {
  		   	      unset($this->battle[$kak][$defeated_id]);
  			    }

                $this->update_battle("");
                return true;
  	    	  }

  	     }
         return false;
      }

/***--------------------------
 * �������� �������� ����� �� ��������
 **/

      function fast_death_to($death_user, $type = 0) {  # $type=1 �������� ��������
  	   global $text_death, $text_salvation, $bot_user_attacking;

  	  # ������� ������
  		if($this->battle) {

          if (!empty($death_user['bot_id'])) {

          # ������ ��� ��� ���
    		if($death_user['bot_id'] > _BOTSEPARATOR_) $is_defeated_bot = 1; else $is_defeated_bot = 0;
          # id �����������
            $is_defeated_id  = $death_user['bot_id'];

            # ���������� ���� ������������ ���� "��������"
              if (empty($type) && (int)$death_user['hp'] <= 0 && $is_defeated_id && !empty($this->battle_eff[$is_defeated_id]['salvation'])) {

                # ��������� + 10% ������
                  $ran_pw = floor($death_user['mana'] + ($death_user['maxmana'] * 0.1));
                  if ($ran_pw > $death_user['maxmana']) $ran_pw = $death_user['maxmana'];

                  if ($is_defeated_bot) {
                      $ran_salvation = 0;

                      if (($death_user['bothran'] || $death_user['bothidro'])) {

                          sql_query("UPDATE `users` SET
                            `hp` = `maxhp`, `hp_precise` = `maxhp`,
                            `mana` = ".$ran_pw.", `pw_precise` = ".$ran_pw."
                          WHERE id = '".$is_defeated_id."' LIMIT 1;");

                          $this->set_points("h", $death_user['maxhp'], $is_defeated_id);
                          $this->set_points("p", $ran_pw, $is_defeated_id);
                          $death_user['hp'] = $death_user['maxhp'];

                          $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');
                          $this->write_log ();
                          $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_salvation[$ran_salvation + $death_user['sex']].'');
                      } elseif (sql_query("UPDATE `bots` SET `hp` = `maxhp`, `mana` = ".$ran_pw." WHERE `id` = '".$is_defeated_id."' LIMIT 1;")) {

                          $this->set_points("h", $death_user['maxhp'], $is_defeated_id);
                          $this->set_points("p", $ran_pw, $is_defeated_id);
                          $death_user['hp'] = $death_user['maxhp'];

                          $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');
                          $this->write_log ();
                          $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_salvation[$ran_salvation + $death_user['sex']].'');
                      }
                  } else {
                      if (sql_query("UPDATE `users` SET `hp` = `maxhp`, `hp_precise` = `maxhp`, `mana` = ".$ran_pw.", `pw_precise` = ".$ran_pw." WHERE `id` = '".$is_defeated_id."' LIMIT 1;")) {
                        $ran_salvation = 0;

                      # ������ HP � PW �����
                        if ($bot_user_attacking['id'] == $is_defeated_id || $bot_attacking['id'] == $is_defeated_id) {
                          $botr_attacking['hp']       = $bot_attacking['maxhp'];
                          $bot_user_attacking['hp']   = $bot_user_attacking['maxhp'];
                          $bot_user_attacking['mana'] = $ran_pw;
                        }

                        $death_user['hp'] = $death_user['maxhp'];
                        $this->set_points("h", $death_user['maxhp'], $is_defeated_id);
                        $this->set_points("p", $ran_pw, $is_defeated_id);

                        $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');
                        $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_salvation[$ran_salvation + $death_user['sex']].'');
                      }
                  }

                 $this->battle_eff[$is_defeated_id]['salvation'] = 0;

            # ������� �� ���
              } elseif($death_user['bot_id'] && (int)$death_user['hp'] <= 0 && $is_defeated_id) {

              # ������� �� ������ ���
                unset($this->battle[$is_defeated_id]);
              # ������� ���� �������
                unset($this->b_hint_nbp[$is_defeated_id]);

                  if ($is_defeated_bot && $is_defeated_id) {
                      if (isset($death_user['bothran']) || isset($death_user['bothidro'])) {
                         sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0 WHERE `id` = '".$is_defeated_id."' LIMIT 1;");
                      } else {
                        # sql_query("UPDATE `bots` SET `hp` = 0 WHERE `id` = '".$is_defeated_id."' LIMIT 1;");
                      }

                      $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');

                    # ������� ����
                      unset($this->bots[$is_defeated_id]);

                      $this->set_points("h", 0, $is_defeated_id);
                  } else {
          	          if (sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0 WHERE `id` = '".$is_defeated_id."' LIMIT 1;")) {

                      # ������ HP � PW �����
                        if ($bot_user_attacking['id'] == $is_defeated_id) $bot_user_attacking['hp'] = 0;

                        $this->set_points("h", 0, $is_defeated_id);
                        $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');
                      }
                  }

                # ������� �� ������ ��� ����� �� ����
  				  foreach ($this->battle as $pers_id => $vav) {
  					unset($this->battle[$pers_id][$is_defeated_id]);
  				  }

              }

            # ���� ���� ���� � �� �� � ��� �� �������� ��� �� ����
  	    	  if($death_user['bot_id'] && ($death_user['battle'] != $this->battle_data['id'] || $death_user['hp'] <= 0)) {
  	          # ������� �� ������
  		        unset($this->battle[$is_defeated_id]);

              # ������� ���� �������
                unset($this->b_hint_nbp[$is_defeated_id]);

  		       	foreach ($this->battle as $pers_id => $vav) {
  		   	      unset($this->battle[$pers_id][$is_defeated_id]);
  			    }
  	    	  }

  		    unset($death_user);
          }
  		}
      }

/***--------------------------------------
 * ��������� �������� ������ �� ��������
 **/

			function fast_death() {
			  //
			}

/***--------------------------------------
 * ��������� �������� �� ��������
 **/

      function fast_team_death($no_upd = 0) {

  	  # ������� ������
  		if($this->battle && $no_upd == 21) {
          foreach($this->battle as $k => $v) {
            if ($k) {
  				if($k > _BOTSEPARATOR_) {
  					$bots_defeated = $this->bots[$k];

                   # ���� ��� ����� � ���
                     if (empty($bots_defeated['id'])) {
    					$bots_defeated['id'] = $k;
    					$bots_defeated['hp'] = 0;
    					$bots_defeated['battle'] = $this->battle_data['battle'];
                     }

                    $is_defeated_bot = 1;
                    $is_defeated_id  = $k;
                    $user_defeated   = $bots_defeated;
  				} else {
                    $is_defeated_bot = 0;
                    $is_defeated_id  = $k;
  					$user_defeated   = sql_row("SELECT `id`, `hp`, `battle` FROM `users` WHERE `id` = '".$k."' LIMIT 1;");
  				}

                if($user_defeated['id'] && (int)$user_defeated['hp'] <= 0 && $is_defeated_id) {
  					unset($this->battle[$is_defeated_id]);

                      if ($is_defeated_bot && $is_defeated_id) {

  	        	        $bots = $user_defeated;
                         if (($bots['is_hbot'] || $bots['is_hranbot'])) {
                           sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0 WHERE `id` = '".$bots['prototype']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                         } elseif (sql_query("UPDATE `bots` SET `hp` = 0 WHERE `id` = '".$is_defeated_id."' LIMIT 1;")) {
                           $no_upd = 1;
                         }

                       } else {
  				         if (sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0, `fullhptime` = ".time()." WHERE `id` = '".$is_defeated_id."' LIMIT 1;")) {
                           $no_upd = 1;
                         }
                       }

                    $this->set_points("h", 0, $is_defeated_id);

  					foreach ($this->battle as $kak => $vav) {
  					 unset($this->battle[$kak][$is_defeated_id]);
  					}

                }

    	    	if($user_defeated['id'] && $user_defeated['battle'] == 0) {
    		      unset($this->battle[$is_defeated_id]);
    		    	foreach ($this->battle as $kak => $vav) {
    		   	        unset($this->battle[$kak][$is_defeated_id]);
    			    }
    	    	}

             # �������� �����
  		       if (empty($no_upd)) {  $this->update_battle (); }
  			   unset($user_defeated);
            }
          }
  		}
      }

/***--------------------------
 * �������� ��� ������
 **/

      function get_wep_type($idwep, $id_us) {

        if ($this->get_points("wep_type", $id_us)) {
         return $this->get_points("wep_type", $id_us);
	    } elseif ($idwep == 0 || $idwep == null || $idwep == '') {
         return "kulak";
        } else {
	     $wep = sql_row('SELECT `otdel` FROM `inventory` WHERE `id` = '.$idwep.' LIMIT 1;');
         $name_wep = '';

          switch ($wep['otdel']) {
            case '1':   $name_wep = "noj";    break;
            case '11':  $name_wep = "topor";  break;
            case '12':  $name_wep = "dubina"; break;
            case '13':  $name_wep = "mech";   break;
            case '14':  $name_wep = "molot";  break;
            case '15':  $name_wep = "kastet"; break;
            case '52':  $name_wep = "posoh";  break;
            case '18':  $name_wep = "tackle"; break;
            default:    $name_wep = "kulak";  break;
          }

         $this->set_points("wep_type", $name_wep, $id_us);
         return $name_wep;
        }
      }

/***----------------------------------------------
 * ���������� �������, � �����������/���������
 **/

      function sort_teams($cus = 0) {

    	  # ������ ������� � ��� ��� ����� �� ���
    		$this->t1 = explode(";",$this->battle_data['t1']);
    		$this->t2 = explode(";",$this->battle_data['t2']);
    		$this->texit = explode(";",$this->battle_data['texit']);

    	  # ����������� ���-���
    		if (in_array ($cus, $this->t1)) {
    			$this->my_class   = "B1";
    			$this->en_class   = "B2";
    			$this->team_mine  = $this->t1;
    			$this->team_enemy = $this->t2;
    		} else {
    			$this->my_class   = "B2";
    			$this->en_class   = "B1";
    			$this->team_mine  = $this->t2;
    			$this->team_enemy = $this->t1;
    		}
      }

/***--------------------------
 * ��������� ������
 **/

      function razmen_log($type,$kuda,$chem,$uron,$kto,$c1,$pokomy,$c2,$hp,$maxhp,$mepos,$hepos, $isusertimeout = 0) {
        global $bot_attacking, $bot_user_attacking, $bot_defender, $bot_user_defender;

              /*echo "<pre>";
              print_r($kto);
              echo "</pre>";*/

               $uronex = abs(floor($uron));
               $hp = abs(floor($hp));
               if($hp < 0) { $hp = 0; }
               $maxhp = abs(floor($maxhp));

               $us_kto = $kto;
               if ($kto['bot_id']) { $kto_id = $kto['bot_id']; } else { $kto_id = $kto['id']; }

               $us_pokomy = $pokomy;
               if ($pokomy['bot_id']) { $us_pokomy_id = $pokomy['bot_id']; } else { $us_pokomy_id = $pokomy['id']; }

               if($us_pokomy['invis']) {
            	 $uron = '??';
                 $hp = '??';
                 $maxhp= '??';
            	 $uronex = '??';
               }

               if ($isusertimeout) {
                 if ($kto_id == $isusertimeout) {
                  $user_timeout = $us_kto;
                 } else if($us_pokomy_id == $isusertimeout) {
                  $user_timeout = $us_pokomy;
                 } else {
                  $user_timeout = sql_row("SELECT `id`, `login`, `invis` FROM `users` WHERE `id` = '".$isusertimeout."' LIMIT 1;");
                 }
               }

                   #--> ��������� ��� ������� �����
                        if (!in_array($mepos, array(0, 1, 2))) { $mepos = 0; }
                   #--> ��������� ������� ����� ����������
                        if (!in_array($hepos, array(0, 1, 2))) { $hepos = 0; }

                        $sex1 = false;
                        $sex2 = false;

            			if ($this->enemyhar['sex']  && $kto_id    == $this->enemyhar['id']) { $sex1 = false; }
            			if (!$this->enemyhar['sex'] && $kto_id    == $this->enemyhar['id']) { $sex1 = true;  }
            			if ($this->enemyhar['sex']  && $us_pokomy_id == $this->enemyhar['id']) { $sex2 = false; }
            			if (!$this->enemyhar['sex'] && $us_pokomy_id == $this->enemyhar['id']) { $sex2 = true;  }

            			if ($this->user['sex']  && $kto_id    == $this->user['id']) { $sex1 = false; }
            			if (!$this->user['sex'] && $kto_id    == $this->user['id']) { $sex1 = true;  }
            			if ($this->user['sex']  && $us_pokomy_id == $this->user['id']) { $sex2 = false; }
            			if (!$this->user['sex'] && $us_pokomy_id == $this->user['id']) { $sex2 = true;  }

            		  # ��� ����
            			$textchem = array (
  							"kulak"   => array("","",""),
  							"noj"     => array("�����","�����","�����"),
  							"posoh"   => array("������","������","������"),
  							"dubina"  => array("�������","�������","�������"),
  							"topor"   => array("�������","�������","�������"),
  							"mech"    => array("�����","�����","�����"),
  							"molot"   => array("�������","�������","�������"),
  							"buket"   => array("������� ������","�������","�������","���������","������","�������","��������","�������",),
  							"kastet"  => array("��������","��������","��������")
  						);

            		   $textchem = $textchem[$chem];

            		  # ���� ����
            			$udars = array(
            				1 => array ('� ���','� ����','� �������','�� �������','� ������'),
            				2 => array ('� �����','� ��������� ���������','�� �����','� ������'),
            				3 => array ('� �����','� ������','� �����','� ������','� �����','�� ������'),
            				4 => array ('�� �����','� ������','�� ���������','�� �����','�� ���������')
            			);

                     # ���� ������
                       if ($kuda) {
                           $kuda = $udars[$kuda][rand(0,count($udars[$kuda])-1)];
                       } else $kuda = '';

            	      # ����� �� ������������
            			if (!$sex1) {
            			   if ($mepos==1) { $textfail = array ('��������'); } elseif ($mepos==0 || $mepos==2) { $textfail = array ('���������'); }
            			} else {
                           if ($mepos==1) { $textfail = array ('���������'); } elseif ($mepos==0 || $mepos==2) { $textfail = array ('����������'); }
            			}

            			if (!$sex2) {
            			  if ($hepos==1) { $textud = array ('��������');} elseif ($hepos==0 || $hepos==2) { $textud = array ('���������');}
            			} else {
            			  if ($hepos==1) { $textud = array ('���������');} elseif ($hepos==0 || $hepos==2) { $textud = array ('����������');}
            			}
            			if (!$sex2) {
            			  if ($mepos==1) { $textudk = array ('������');} elseif ($mepos==0 || $mepos==2) { $textudk = array ('���������');}
            			} else {
            			  if ($mepos==1) { $textudk = array ('������');} elseif ($mepos==0 || $mepos==2) { $textudk = array ('���������');}
            			}

                        if (!empty($isusertimeout) && $isusertimeout == 'usertimeout') {
              			 if (!$sex2) {
              			   if ($hepos==1) { $textud = array ('�����������');} elseif ($hepos==0 || $hepos==2) { $textud = array ('�����������');}
              			 } else {
              			   if ($hepos==1) { $textud = array ('���������');} elseif ($hepos==0 || $hepos==2) { $textud = array ('����������');}
              			 }
                        }

                        $str = '';

        				switch ($type) {
        				  # ������
        					case "uvorot":
        						if ($sex2) { $textuvorot = array (", ���������� �� ����� "); }
        					      	  else { $textuvorot = array (", ��������� �� ����� "); }

            					if (!$sex2) {
            					  if ($hepos==1) {$textudk = array ('������'); } elseif ($hepos==0 || $hepos==2) { $textudk = array ('���������');}
            					} else {
            					  if ($hepos==1) {$textudk = array ('������'); } elseif ($hepos==0 || $hepos==2) { $textudk = array ('���������');}
            					}

                               if (!empty($isusertimeout) && $isusertimeout == 'usertimeout') {
            					if (!$sex2) {
            					  if ($hepos==1) { $textudk = array ('����������� �');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('����������� �');}
            					} else {
            					  if ($hepos==1) { $textudk = array ('����������� �');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('����������� �');}
            					}
                               }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
        					     return $str.nick_team($us_kto, $c1).' '.$textfail[rand(0,count($textfail)-1)].', �� '.nick_team($us_pokomy, $c2).' '.$textudk[rand(0,count($textud)-1)].''.$textuvorot[rand(0,count($textuvorot)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.'.';
                               }
        					break;
        				  # ����
        					case "block":
        						if ($sex2) { $textblock = array (", ������������� ���� "); }
        					          else { $textblock = array (", ������������ ���� "); }

            					if (!$sex2) {
            					  if ($hepos==1) {$textudk = array ('������');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('���������');}
            					} else {
            					  if ($hepos==1) {$textudk = array ('������');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('���������');}
            					}

                               if (!empty($isusertimeout) && $isusertimeout == 'usertimeout') {
            					if (!$sex2) {
            					  if ($hepos==1) {$textudk = array ('����������� �');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('����������� �');}
            					} else {
            					  if ($hepos==1) {$textudk = array ('����������� �');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('����������� �');}
            					}
                               }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
        					     return $str.nick_team($us_kto, $c1).' '.$textfail[rand(0,count($textfail)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_pokomy, $c2).' '.$textudk[rand(0,count($textudk)-1)].''.$textblock[rand(0,count($textblock)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.'.';
                               }
        					break;
        				  # ����
        					case "krit":
        						if ($sex1) { $textkrit = array (", ������� ����������� ����"); }
        					          else { $textkrit = array (", ����� ����������� ����"); }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
        						 return $str.nick_team($us_pokomy, $c2).' '.$textud[rand(0,count($textud)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_kto, $c1).' '.$textudk[rand(0,count($textud)-1)].''.$textkrit[rand(0,count($textkrit)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.' <font color="red"><b>-'.$uronex.'</b></font> ['.$hp.'/'.$maxhp.']'.'';
                               }
        					break;
        				  # ���� ������ ����
        					case "krita":
        						if ($sex1) { $textkrit = array (", ������� ����"); }
        					          else { $textkrit = array (", ������ ����"); }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
        						 return $str.nick_team($us_pokomy, $c2).' '.$textud[rand(0,count($textud)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_kto, $c1).' '.$textudk[rand(0,count($textud)-1)].''.$textkrit[rand(0,count($textkrit)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.' <font color="red"><b>-'.$uronex.'</b></font> ['.$hp.'/'.$maxhp.']'.'';
                               }
        					break;
        				  # ���������
        					case "udar":
        						if ($sex1) { $textudar = array(", �������"); }
                                      else { $textudar = array(", ������"); }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
         						 return $str.nick_team($us_pokomy, $c2).' '.$textud[rand(0,count($textud)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_kto, $c1).' '.$textudk[rand(0,count($textud)-1)].''.$textudar[rand(0,count($textudar)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.' <b>-'.$uronex.'</b> ['.$hp.'/'.$maxhp.']'.' ';
                              }

        					break;
        				}
      }


/***--------------------------
 * ������� ������������
 **/

function solve_mf($attaka_pos_bot1, $attaka_pos_bot2) {
  global $bot_attacking, $bot_user_attacking, $bot_defender, $bot_user_defender;

     $mf = array ();
     $mf_z9_def = 0.1;
     $mf_x1_def = 0.1;
     $mf_is_krit_max = 0.95;
     $mf_is_uvor_max = 0.95;
     $str_factor = 0.5;
     $z9_factor = 1;


    # ���������
     $this->user                     = array();
     $this->user                     = $bot_user_attacking;
     $enemy_at_id                    = $this->user['bot_id'];
     $items_bot_user_attacking       = $this->user;
     $items_bot_user_attacking['id'] = $this->user['id'];

     $this->user_u1[0] = 0;
     $this->user_u1[1] = 0;

    # ������������
  	 $this->enemyhar                 = array();
  	 $this->enemyhar                 = $bot_user_defender;
     $enemy_def_id                   = $this->enemyhar['bot_id'];
     $items_bot_user_defender        = $this->enemyhar;
     $items_bot_user_defender['id']  = $this->enemyhar['id'];

     if ($this->user['id'] && $this->enemyhar['id']) {

      # ���� ���� ���� �����
          if (empty($this->user['bot_stats']) || $this->user['bot_stats'] == 'a:0:{}') {
            # ����������� � ������������ ����� ��� ����
              $enemy_at       = array();
              $enemy_at['id'] = $bot_user_attacking['id'];
              $enemy_at['incity'] = $bot_user_attacking['incity'];

              //$this->user_dress = get_user_items_dress($bot_user_attacking);

              if ($bot_user_attacking['inv_strength']) $this->user['sila']  += $bot_user_attacking['inv_strength'];
              if ($bot_user_attacking['inv_dexterity']) $this->user['lovk']  += $bot_user_attacking['inv_dexterity'];
              if ($bot_user_attacking['inv_success']) $this->user['inta']  += $bot_user_attacking['inv_success'];

           } else {
      	      $new_stats = array();
      	      $new_stats = unserialize($this->user['bot_stats']);

              $this->user['sex']    = $new_stats['sex'];
              $this->user['shit']   = $new_stats['shit'];
              $this->user['invis']  = $new_stats['invis'];
              $this->user['id']     = 0;
              $this->user['weap']   = empty($this->user['weap'])?0:$this->user['weap'];

              $this->user['sila']  = $new_stats['sila'];
              $this->user['lovk']  = $new_stats['lovk'];
              $this->user['inta']  = $new_stats['inta'];
              $this->user['vinos'] = $new_stats['vinos'];

              $bot_user_attacking['inv_minu']     = empty($new_stats['minu'])?0:$new_stats['minu'];
              $bot_user_attacking['inv_maxu']     = empty($new_stats['maxu'])?0:$new_stats['maxu'];

              $bot_user_attacking['inv_krit']     = empty($new_stats['krit'])?0:$new_stats['krit'];
              $bot_user_attacking['inv_akrit']    = empty($new_stats['akrit'])?0:$new_stats['akrit'];
              $bot_user_attacking['inv_uvor']     = empty($new_stats['uvorot'])?0:$new_stats['uvorot'];
              $bot_user_attacking['inv_auvor']    = empty($new_stats['auvorot'])?0:$new_stats['auvorot'];

              $bot_user_attacking['inv_bron1']    = empty($new_stats['bron'])?0:$new_stats['bron'];
              $bot_user_attacking['inv_bron2']    = empty($new_stats['bron'])?0:$new_stats['bron'];
              $bot_user_attacking['inv_bron3']    = empty($new_stats['bron'])?0:$new_stats['bron'];
              $bot_user_attacking['inv_bron4']    = empty($new_stats['bron'])?0:$new_stats['bron'];

              if ($new_stats['login']) { $this->user['login']  = $new_stats['login']; }
              if ($new_stats['invis']) { $this->user['invis']  = $new_stats['invis']; }

              $this->user['level']   = $new_stats['level'];
              $this->user['maxhp']   = $new_stats['maxhp'];
              $this->user['maxmana'] = $new_stats['maxmana'];
           }

      # ���� ���� ���� �����
          if (empty($this->enemyhar['bot_stats']) || $this->enemyhar['bot_stats'] == 'a:0:{}') {
            # ����������� � ������������ ����� ��� �����
              $enemy_def       = array();
              $enemy_def['id'] = $bot_user_defender['id'];
              $enemy_def['incity'] = $bot_user_defender['incity'];

              //$this->enemy_dress = get_user_items_dress($bot_user_defender);

              if ($bot_user_defender['inv_strength']) $this->enemyhar['sila']  += $bot_user_defender['inv_strength'];
              if ($bot_user_defender['inv_dexterity']) $this->enemyhar['lovk']  += $bot_user_defender['inv_dexterity'];
              if ($bot_user_defender['inv_success']) $this->enemyhar['inta']  += $bot_user_defender['inv_success'];

          } else {
      	      $new_stats = array();
      	      $new_stats = unserialize($this->enemyhar['bot_stats']);

              $this->enemyhar['sex']    = $new_stats['sex'];
              $this->enemyhar['shit']   = $new_stats['shit'];
              $this->enemyhar['invis']  = $new_stats['invis'];
              $this->enemyhar['id']     = 0;
              $this->enemyhar['weap']   = empty($this->enemyhar['weap'])?0:$this->enemyhar['weap'];

              $this->enemyhar['sila']  = $new_stats['sila'];
              $this->enemyhar['lovk']  = $new_stats['lovk'];
              $this->enemyhar['inta']  = $new_stats['inta'];
              $this->enemyhar['vinos'] = $new_stats['vinos'];

              $bot_user_defender['inv_minu']     = empty($new_stats['minu'])?0:$new_stats['minu'];
              $bot_user_defender['inv_maxu']     = empty($new_stats['maxu'])?0:$new_stats['maxu'];

              $bot_user_defender['inv_krit']     = empty($new_stats['krit'])?0:$new_stats['krit'];
              $bot_user_defender['inv_akrit']    = empty($new_stats['akrit'])?0:$new_stats['akrit'];
              $bot_user_defender['inv_uvor']     = empty($new_stats['uvorot'])?0:$new_stats['uvorot'];
              $bot_user_defender['inv_auvor']    = empty($new_stats['auvorot'])?0:$new_stats['auvorot'];

              $bot_user_defender['inv_bron1']    = empty($new_stats['bron'])?0:$new_stats['bron'];
              $bot_user_defender['inv_bron2']    = empty($new_stats['bron'])?0:$new_stats['bron'];
              $bot_user_defender['inv_bron3']    = empty($new_stats['bron'])?0:$new_stats['bron'];
              $bot_user_defender['inv_bron4']    = empty($new_stats['bron'])?0:$new_stats['bron'];

              if ($new_stats['login']) { $this->enemyhar['login']  = $new_stats['login']; }
              if ($new_stats['invis']) { $this->enemyhar['invis']  = $new_stats['invis']; }

              $this->enemyhar['level']   = $new_stats['level'];
              $this->enemyhar['maxhp']   = $new_stats['maxhp'];
              $this->enemyhar['maxmana'] = $new_stats['maxmana'];
          }
    }

    /*
      0-minu1         = ����������� ����
      1-maxu1         = ������������ ����
      2-mfkrit        = ���������� �����
      3-mfakrit       = ���������� ���� �����
      4-mfuvorot      = ���������� �������
      5-mfauvorot     = ���������� ���� �������
      6-bron1         = ����� ������
      7-bron2         = ����� �������
      8-bron2         = ����� �����
      9-bron3         = ����� ���
      10-bron4        = ����� ���
    */


              # $this->user['id']            = $user_left['bot_id'];
               $this->user['inv_minu']      = $bot_user_attacking['inv_minu'];
               $this->user['inv_maxu']      = $bot_user_attacking['inv_maxu'];
               $this->user['mfkrit']        = $bot_user_attacking['inv_krit'];
               $this->user['mfakrit']       = $bot_user_attacking['inv_akrit'];
               $this->user['mfuvorot']      = $bot_user_attacking['inv_uvor'];
               $this->user['mfauvorot']     = $bot_user_attacking['inv_auvor'];
               $this->user['bron1']         = $bot_user_attacking['inv_bron1'];
               $this->user['bron2']         = $bot_user_attacking['inv_bron2'];
               $this->user['bron3']         = $bot_user_attacking['inv_bron3'];
               $this->user['bron4']         = $bot_user_attacking['inv_bron4'];

              # $this->enemyhar['id']        = $user_right['bot_id'];
               $this->enemyhar['inv_minu']  = $bot_user_defender['inv_minu'];
               $this->enemyhar['inv_maxu']  = $bot_user_defender['inv_maxu'];
               $this->enemyhar['mfkrit']    = $bot_user_defender['inv_krit'];
               $this->enemyhar['mfakrit']   = $bot_user_defender['inv_akrit'];
               $this->enemyhar['mfuvorot']  = $bot_user_defender['inv_uvor'];
               $this->enemyhar['mfauvorot'] = $bot_user_defender['inv_auvor'];
               $this->enemyhar['bron1']     = $bot_user_defender['inv_bron1'];
               $this->enemyhar['bron2']     = $bot_user_defender['inv_bron2'];
               $this->enemyhar['bron3']     = $bot_user_defender['inv_bron3'];
               $this->enemyhar['bron4']     = $bot_user_defender['inv_bron4'];

              # ������
                $mf_change = function ($my_player, $enemy_player, $pos_att = 0, $m_me = "me",  $m_he = "he") {

                   $mf_krit_def    = 0.01;  # ���� � ����� ���������
                   $mf_uvorot_def  = 0.01;  # ���� � ����� ���������
                   $mf_is_krit_max = 0.95; # ����������� ����������� = ���������
                   $mf_is_uvor_max = 0.95; # ����������� ����������� = ���������
                   $str_factor     = 0.5;  # ������� �����
                   $z9_factor      = 1;    # ���� � �����

                    #----------------------------------------------------------
                    # ������� ����� "2=������, 1=�����, 0=��� �������"
                      $pos_att_my_player    = $this->battle[$my_player['bot_id']][$enemy_player['bot_id']][2];
                      $pos_att_enemy_player = $this->battle[$enemy_player['bot_id']][$my_player['bot_id']][2];


                    #----------------------------------------------------------
                    # ��� ������� �� �������� ������
                      $me_coef = $my_player["mana"] / $my_player["maxmana"];
                      if($me_coef < 0.1) $me_coef = 0.1;

                    #----------------------------------------------------------
                    # ��� ������� �� �������� ������
                      $he_coef = $enemy_player["mana"] / $enemy_player["maxmana"];
                      if($he_coef < 0.1) $he_coef = 0.1;

                    #-----------------------------------------------------
                    # �����: ������, �����, �����, ��� "������ � �����"
                      $en_bron = 0; # rand(0, $en_bron);
                      switch($pos_att){
                       case 1: $en_bron = ($enemy_player['bron1']); break;
                       case 2: $en_bron = ($enemy_player['bron2']); break;
                       case 3: $en_bron = ($enemy_player['bron3']); break;
                       case 4: $en_bron = ($enemy_player['bron4']); break;
                      }

                    #---------------------------------------
                    # ��� ����
                      if (is_nan($my_player['sila']) || is_null($my_player['sila']) || $my_player['sila'] < 0) { $my_player['sila'] = 3; }
                      $mod_sila = abs(floor($my_player['sila']));
                      $dam      = 0;

                    #----------------------
                    # ���� ����� � �����
                      if ($pos_att_my_player == 0 && $pos_att_enemy_player == 0) {
                        /*$my_player['inv_minu'] *= 1.2;
                        $my_player['inv_maxu'] *= 1.2;*/
                        $mod_sila *= 1.4;
                      }

                    #----------------------
                    # ���� ������ ����
                      if (empty($my_player['inv_minu'])) { $my_player['inv_minu'] = 0; }
                      if (empty($my_player['inv_maxu'])) { $my_player['inv_maxu'] = 0; }

                    #----------------------
                    # ���� ������ 5 ����
                      if ($mod_sila > 5) {
                        $kor_sila = sqrt($mod_sila);
                        $kor10_sila = $kor_sila*sqrt($kor_sila);

                        $minu  = ($my_player['inv_minu'])+$kor10_sila;
                        $maxu  = ($my_player['inv_maxu'])+$kor10_sila+$kor_sila;
                        if (is_nan($dam) || is_null($minu) || $minu < 0) { $minu = 0; }
                        if (is_nan($dam) || is_null($maxu) || $maxu < 0) { $maxu = 5; }

                        $dam  += float_rand($minu, $maxu);
                      } else {
                        $dam  += rand(1, 5);
                      }

                  #--------------------------
                  # �������� ������������
                    $x8_min = round(sqrt($mod_sila));
                    if($pos_att_my_player == 2){ $x8_min = round($x8_min * 1.5); }

                      $my_uvorot     = $me_coef * ($my_player['mfuvorot']     + ($my_player['lovk'] * 10));    # ������
                      $enemy_auvorot = $he_coef * ($enemy_player['mfauvorot'] + ($enemy_player['lovk'] * 10)); # ������ ������
                      $my_krit       = $me_coef * ($my_player['mfkrit']       + ($my_player['inta'] * 10));    # ����
                      $enemy_akrit   = $he_coef * ($enemy_player['mfakrit']   + ($enemy_player['inta'] * 10)); # ������ ����

                      $r_1 = 1.1;
                      $r_0 = 1.4;

                    # ���� � �����
                      if($pos_att_my_player == 1 && $pos_att_enemy_player == 1){
                       $r_0  = 1.4;
                       $dam *= 1.5;
                      }

                    # ���� � ����� ������ � ������
                      if(($pos_att_my_player == 1 && $pos_att_enemy_player == 2) || ($pos_att_my_player == 2 && $pos_att_enemy_player == 1)){
                       $r_0  = 1.2;
                       $dam *= 1.1;
                      }

                    #-------------
                    # ��� ����
                      if($pos_att_my_player == 2){              #-- � ������
                         $my_krit       /= float_rand(1, $r_0); # ��������
                         $my_uvorot     *= float_rand(1, $r_0); # ��������

                         $enemy_akrit   /= float_rand(1, $r_0); # ��������������� ����
                         $enemy_auvorot *= float_rand(1, $r_0); # �������� ����������
                        // $dam         /= 1.1;
                      } elseif($pos_att_my_player == 1){        #-- � �����
                         $my_krit       *= float_rand(1, $r_0); # �������� ����������� ����
                         $my_uvorot     /= float_rand(1, $r_0); # �������� ���� ����������

                         $enemy_akrit   /= float_rand(1, $r_0); # ��������������� ����
                         $enemy_auvorot *= float_rand(1, $r_0); # �������� ����������
                         $dam           *= 1.2;
                      }

                    #-------------
                    # ��� ����
                      if($pos_att_enemy_player == 2){           #-- � ������
                         $enemy_akrit   /= float_rand(1, $r_0); # ��������
                         $enemy_auvorot *= float_rand(1, $r_0); # ��������

                         $my_krit       /= float_rand(1, $r_0); # ��������������� ����
                         $my_uvorot     *= float_rand(1, $r_0); # �������� ����������
                      } elseif($pos_att_enemy_player == 1){     #-- � �����
                         $enemy_akrit   *= float_rand(1, $r_0); # �������� ����������� ����
                         $enemy_auvorot /= float_rand(1, $r_0); # �������� ���� ����������

                         $my_krit       /= float_rand(1, $r_0); # ��������������� ����
                         $my_uvorot     *= float_rand(1, $r_0); # �������� ����������
                        // $dam           *= $r_0;
                      }

                    #--------------------------------------
                    # �������� �����
                     // $dam -= mt_rand(sqrt($en_bron), $en_bron);
                      $dam -= sqrt($en_bron);

                    #--------------------------------------
                    # ���� ������ ����� ��� ������ ����
                      if($dam < 1) $dam = 1;

                    #-------------------
                    # ���� ����������
                      if ($my_uvorot >= 0 && empty($enemy_auvorot)) $uvorot = $my_uvorot;
                      if (rand(1, 100) <= 5) $uvorot = 0;
                      else if($my_uvorot > 0 && $enemy_auvorot > 0) $uvorot = $my_uvorot / $enemy_auvorot;
                      else $uvorot = 0;

                    #-----------------
                    # ���� ���������
                      if ($my_krit >= 0 && empty($enemy_akrit)) $krit = $my_krit;
                      if (rand(1, 100) <= 5) $krit = 0;
                      else if($my_krit > 0 && $enemy_akrit > 0) $krit = $my_krit / $enemy_akrit;
                      else $krit = 0;

                    #----------------------------
                    # �������� ����� �� ������
                      if (is_nan($dam) || is_null($dam) || $dam < 0) { $dam = 0; }
                    #---------------------------
                    # �������� ����� �� ������
                      if (is_nan($krit) || is_null($krit) || $krit < 0) { $krit = 0; }
                    #------------------------------
                    # �������� ������� �� ������
                      if (is_nan($uvorot) || is_null($uvorot) || $uvorot < 0) { $uvorot = 0; }

                	return array (
                		'id'          => $my_player['id'],
                	    'udar'        => $dam,
                		'krit'        => $krit + $mf_krit_def,
                		'uvorot'      => $uvorot + $mf_uvorot_def,
                		'x8_min'      => 1 + floor($x8_min / 3.7),
                        'priem_krit'  => '',
                        'user_krit'   => 0,
                        'user_udar'   => 0,
                	);
                };

                 $mf = array();
                 $mf[$this->user['bot_id']]     = $mf_change($this->user, $this->enemyhar, $attaka_pos_bot1, "me", "he");
                 $mf[$this->enemyhar['bot_id']] = $mf_change($this->enemyhar, $this->user, $attaka_pos_bot2, "he", "me");


             # �������� ���������� �������
               /*if ($this->battle_data['id']) {
                CHAT::chat_attention('BR', '<b>��������</b>!!!<b>'.$this->user['login'].'</b>['.$this->user['level'].']     ����='.$mf[$this->user['bot_id']]['udar'].' sila='.$this->user['sila'].' �����='.$this->user['bron1'].'  ����='.$this->user['inv_minu'].'/'.$this->user['inv_maxu'].'   ������� ='.$this->user['mfuvorot'].' ����� ='.$this->user['mfkrit'].'  ', array('addXMPP' => true));
                CHAT::chat_attention('BR', '<b>��������</b>!!!<b>'.$this->enemyhar['login'].'</b>['.$this->enemyhar['level'].']  ����='.$mf[$this->enemyhar['bot_id']]['udar'].' sila='.$this->enemyhar['sila'].' �����='.$this->enemyhar['bron1'].' ����='.$this->enemyhar['inv_minu'].'/'.$this->enemyhar['inv_maxu'].' ������� ='.$this->enemyhar['mfuvorot'].' ����� ='.$this->enemyhar['mfkrit'].'  ', array('addXMPP' => true));
               }*/
	return $mf;
}

    /***-----------------------------------
     * �������� �� ��������� "���� ����"
     **/

      function get_block ($komy, $att, $def, $enemy_schit) {
         $is_schit = 0;
         if($komy=="me"){ $is_schit = $this->user['shit']; } elseif( $komy=="he" ){ $is_schit = $this->enemyhar['shit']; }

            if($is_schit){
      	     $blocks = array (
      						  '0' => array (0),
      						  '1' => array (1,2),
      						  '2' => array (2,3),
      						  '3' => array (3,4),
      						  '4' => array (4,1),
      						  '5' => array (4,2),
      						  '6' => array (1,3)
      						);
              if (!in_array($def, array (1, 2, 3, 4, 5, 6))) { $def = 0; }
             } else {
      	     $blocks = array (
      						  '0' => array (0),
      						  '1' => array (1),
      						  '2' => array (2),
      						  '3' => array (3),
      						  '4' => array (4)
      					     );
              if (!in_array($def, array (1, 2, 3, 4))) { $def = 0; }
             }

				switch ($komy) {
					case "me" :
						if (in_array($att, $blocks[$def])) {
							return true;
						} else {
							return false;
						}
					break;
				   # ���� �������
					case "he" :
						if (in_array($att, $blocks[$def])) {
							return true;
						} else {
							return false;
						}
					break;
				}
       }

/***--------------------------
 * ������� ���� � ��������
 **/

      function solve_exp ($atak_user, $def_user, $damage, $valor = 0, $resarr = 0) {
       //
      }

/***--------------------------
 * �������������� ������ ���
 **/

  function razmen_init($istimeout = 0) {
   global $bot_attacking, $bot_user_attacking, $bot_defender, $bot_user_defender;

     $enemy_at_id  = $bot_attacking['id'];
     $enemy_def_id = $bot_defender['id'];

      if($bot_attacking['id'] && $bot_attacking['next_udar_time'] < time()) {
      if($bot_defender['id'] && $bot_defender['next_udar_time'] < time()) {

	      # �����
			$attaka_pos_bot1 = rand(1,4);
			$attaka_pos_bot2 = rand(1,4);

	      # ����
			$block_pos_bot1 = rand(1,6);
			$block_pos_bot2 = rand(1,6);

	      # �������
            $is_ataka = array (0, 0, 0, 0, 1);
            $pos1 = $is_ataka[rand(0,count($is_ataka)-1)];
            $pos2 = $is_ataka[rand(0,count($is_ataka)-1)];
            $this->battle[$enemy_at_id][$enemy_def_id] = array($attaka_pos_bot1, $block_pos_bot1, $pos1, time());
            $this->battle[$enemy_def_id][$enemy_at_id] = array($attaka_pos_bot2, $block_pos_bot2, $pos2, time());

	      # �-�� �����������
			$mf = $this->solve_mf($attaka_pos_bot1, $attaka_pos_bot2);

          # ��������� �����
            $mf_hit = array(
             $enemy_at_id   => array('hp' => 0, 'pw' => 0, 'hit_name' => '', 'hit_int' => 0, 'trv' => 0, 'type' => "", 'uron' => 0),
             $enemy_def_id  => array('hp' => 0, 'pw' => 0, 'hit_name' => '', 'hit_int' => 0, 'trv' => 0, 'type' => "", 'uron' => 0)
            );

            if ($mf && $bot_attacking['hp'] > 0 && $bot_defender['hp'] > 0) {

               $this->sort_teams($bot_attacking['id']);


 # ������� ��� ������� �������
  $exchange_attacks = function ($mf, $whom = "me", $mf_hit, $this_user, $this_enemyhar, $enemy_id, $istimeout) {
                      global $defend, $pos, $usertimeout;

                        $me_class = $this->userclass($this_user['bot_id']);
                        $he_class = $this->userclass($this_enemyhar['bot_id']);

                        $mf_z9_def      = 0.1;  # ���� � ����� ���������
                        $mf_x1_def      = 0.;   # ���� � ����� ���������
                        $mf_is_krit_max = 0.95; # ����������� ����������� = ���������
                        $mf_is_uvor_max = 0.95; # ����������� ����������� = ���������
                        $str_factor     = 0.5;  # ������� �����
                        $z9_factor      = 1;    # ���� � �����

        	            $uvorot_whom  = $mf[$this_user['bot_id']]['uvorot'];
        	            $krit_whom    = $mf[$this_enemyhar['bot_id']]['krit'];

                        $con        = 0;
                        $uve        = 0;
                        $check_next = true;

                      # ������
                        $me_user_id = $enemy_id;
                        $he_user_id = $this_user['bot_id'];
                      #  include("./magic/priem/_razmen.bifore.php");

                      # ������
                        if($check_next && (float_rand(0, $uvorot_whom)) > $mf_is_uvor_max && $mf[$this_user['bot_id']]['priem_krit'] != 1){ # � ���������
                          $uve        = 1;
                          $check_next = false;
                        }

                      # ����
                        if(($check_next && (float_rand(0, $krit_whom)) > $mf_is_krit_max) || $mf[$enemy_id]['priem_krit']==1){ # �� ��������
                          $uve        = 2;
                          $check_next = false;
                       }


                       if ($uve == 1) {

							if($con != 1){

                              # ������� ����
                                $usertimeout = 0;
                                if ($istimeout == $this_user['bot_id']) { $usertimeout = 'usertimeout'; }

                                 if ($istimeout == $enemy_id) {

    								$this->add_log($this->razmen_log("uvorot", $this->battle[$enemy_id][$this_user['bot_id']][0], $this->get_wep_type($this_enemyhar['weap'], $this_enemyhar['bot_id']), 0, $this_enemyhar, $he_class, $this_user, $me_class, 0, 0, $this->battle[$enemy_id][$this_user['bot_id']][2], $pos, $istimeout));

                                    $mf_hit[$enemy_id]['hp'] = abs(empty($mf_hit[$enemy_id]['hp']) ? 0 : $mf_hit[$enemy_id]['hp']);
                                    $mf_hit[$enemy_id]['pw'] = round(sqrt($this_enemyhar['sila']) * 0.5);
                                    $mf_hit[$this_user['bot_id']]['hit_name'] = 'hit_uvorot';
                                    $mf_hit[$this_user['bot_id']]['hit_int']  = 1;

                                    $mf_hit[$this_user['bot_id']]['type']     = "uvorot";
                                    $mf_hit[$this_user['bot_id']]['uron']     = 0;

                                 } else {
                                    $mf[$this_user['bot_id']]['user_uvorot'] = 1;

    								$this->add_log($this->razmen_log("uvorot", $this->battle[$enemy_id][$this_user['bot_id']][0], $this->get_wep_type($this_enemyhar['weap'], $this_enemyhar['bot_id']), 0, $this_enemyhar, $he_class, $this_user, $me_class, 0, 0, $this->battle[$enemy_id][$this_user['bot_id']][2], $pos, $usertimeout));

                                    $mf_hit[$enemy_id]['hp'] = abs(empty($mf_hit[$enemy_id]['hp']) ? 0 : $mf_hit[$enemy_id]['hp']);
                                    $mf_hit[$enemy_id]['pw'] = abs(round(sqrt($this_enemyhar['sila']) * 0.5));
                                    $mf_hit[$this_user['bot_id']]['hit_name'] = 'hit_uvorot';
                                    $mf_hit[$this_user['bot_id']]['hit_int']  = 1;

                                    $mf_hit[$this_user['bot_id']]['type']     = "uvorot";
                                    $mf_hit[$this_user['bot_id']]['uron']     = 0;
                                 }

							} else {
                             # ����� ������� ��� ���� ���� �� ����������
					        }

						} elseif($uve == 2) { # START ���� ���������

							if($this->get_block ($whom, $this->battle[$enemy_id][$this_user['bot_id']][0], $this->battle[$this_user['bot_id']][$enemy_id][1], $this_enemyhar['shit']))
							     { $hs = 1; $m = 'a'; $block_krit = 1; }
							else { $hs = 2; $m = '';  $block_krit = 0; }

                              # ������� ����
                                $usertimeout = 0;
                                if ($istimeout == $this_user['bot_id']) { $usertimeout = 'usertimeout'; }

                                 if ($istimeout == $enemy_id) {

    								$mf[$enemy_id]['x8_min'] = round((1 + $mf[$enemy_id]['x8_min']) * 1.5);

                                    $uuu = 'krit'.$m;
                                    $new_uron = 0;
							        $this->add_log ($this->razmen_log($uuu, $this->battle[$enemy_id][$this_user['bot_id']][0], $this->get_wep_type($this_enemyhar['weap'], $this_enemyhar['bot_id']), $new_uron, $this_enemyhar, $he_class, $this_user, $me_class, ($this->get_points('h', $enemy_id) - $new_uron), $this_user['maxhp'], $this->battle[$enemy_id][$this_user['bot_id']][2], $pos, $istimeout));

                                    $mf_hit[$this_user['bot_id']]['type'] = $uuu;

                                    $mf_hit[$enemy_id]['hp'] = abs(empty($mf_hit[$enemy_id]['hp']) ? 0  :$mf_hit[$enemy_id]['hp']);
                                    $mf_hit[$enemy_id]['pw'] = abs($mf[$enemy_id]['x8_min']);
                                    $mf_hit[$enemy_id]['hit_name'] = 'hit_krit';
                                    $mf_hit[$enemy_id]['hit_int']  = 1;

                                 } else {

                                    $mf[$enemy_id]['user_krit'] = 1;
                                    $uuu                        = 'krit'.$m;
                                    $mf[$enemy_id]['x8_min']    = round((1 + $mf[$enemy_id]['x8_min']) * 1.5);

                                  # ������
                                  #  include("./magic/priem/_razmen.after.php");

                                  # �������� ���� �� ������ ��� �������� ������
                                    $new_uron  = floor($mf[$enemy_id]['udar'] * $hs);
                                    $temp_uron = $new_uron;

                                  # ����������� ����
                                    if (floor($this_user['hp']) - $new_uron <= 0) {
                                      $new_uron = floor($this_user['hp']);
                                      $mf_hit[$this_user['bot_id']]['trv'] = floor((($temp_uron - $new_uron) * 100 / $this_user['maxhp'])/2);
                                    }

                                       if ($enemy_id < _BOTSEPARATOR_) {
                                          $exp_valor = $this->solve_exp($this_enemyhar, $this_user, $new_uron);
                                          $this->add_points('damage', $new_uron, $enemy_id);
                                          $this->add_points('exp', $exp_valor['exp'], $enemy_id);
                                          $this->add_points('valor', $exp_valor['valor'], $enemy_id);
                                       }

							            $this->add_log ($this->razmen_log($uuu, $this->battle[$enemy_id][$this_user['bot_id']][0],
                                        $this->get_wep_type($this_enemyhar['weap'],
                                        $this_enemyhar['bot_id']), $new_uron,
                                        $this_enemyhar, $he_class, $this_user,
                                        $me_class, ($this->get_points('h', $this_user['bot_id']) - $new_uron),
                                        $this_user['maxhp'], $this->battle[$enemy_id][$this_user['bot_id']][2],
                                        $pos, $usertimeout));

                                        $mf_hit[$this_user['bot_id']]['hp']   = $new_uron;
                                        $mf_hit[$enemy_id]['pw']              = $mf[$enemy_id]['x8_min'];
                                        $mf_hit[$enemy_id]['hit_name']        = 'hit_krit';
                                        $mf_hit[$enemy_id]['hit_int']         = 1;

                                        $mf_hit[$this_user['bot_id']]['type'] = $uuu;
                                        $mf_hit[$this_user['bot_id']]['uron'] = $new_uron;

                                      # ����� ������� ������
                                        if ($new_uron > ($this_enemyhar['level']*20)) {
                                          user_reit_attakt($enemy_id, $new_uron, $this->battle_data['id'], 1);
                                        }
                                 }


                        # END ���� ���������
                        } elseif(!$this->get_block ($whom, $this->battle[$enemy_id][$this_user['bot_id']][0], $this->battle[$this_user['bot_id']][$enemy_id][1], $this_enemyhar['shit']))
                        { # START ��������� ����� ���� ����

                              # ������� ����
                                $usertimeout = 0;
                                if ($istimeout == $this_user['bot_id']) { $usertimeout = 'usertimeout'; }

                                 if ($istimeout == $enemy_id) {

    								$new_uron = 0;
              						$this->add_log ($this->razmen_log("udar", $this->battle[$enemy_id][$this_user['bot_id']][0], $this->get_wep_type($this_enemyhar['weap'], $this_enemyhar['bot_id']), $new_uron, $this_enemyhar, $he_class, $this_user, $me_class, ($this->get_points('h', $this_user['bot_id']) - $new_uron), $this_user['maxhp'], $this->battle[$enemy_id][$this_user['bot_id']][2], $pos, $istimeout));

                                    $mf_hit[$this_user['bot_id']]['hp']   = abs(empty($mf_hit[$enemy_id]['hp']) ? $new_uron : $mf_hit[$enemy_id]['hp']);
                                    $mf_hit[$enemy_id]['pw']              = abs($mf[$enemy_id]['x8_min']);
                                    $mf_hit[$enemy_id]['hit_name']        = 'hit_udar';
                                    $mf_hit[$enemy_id]['hit_int']         = 1;

                                    $mf_hit[$this_user['bot_id']]['type'] = "udar";

                                 } else {

                                    $mf[$enemy_id]['user_udar'] = 1;

                                  # ������
                                  #  include("./magic/priem/_razmen.after.php");

                                  # �������� ���� �� ������ ��� �������� ������
                                    $new_uron = abs(floor($mf[$enemy_id]['udar']));
                                    if ($new_uron >= $this_user['hp']) { $new_uron = abs($this_user['hp']); }

                                    if ($enemy_id < _BOTSEPARATOR_) {
                                      $exp_valor = $this->solve_exp ($this_enemyhar, $this_user, $new_uron);
                                      $this->add_points('damage', $new_uron, $enemy_id);
                                      $this->add_points('exp', $exp_valor['exp'], $enemy_id);
                                      $this->add_points('valor', $exp_valor['valor'], $enemy_id);
                                    }

                                  # ����� ������ ���
        							$this->add_log ($this->razmen_log("udar", $this->battle[$enemy_id][$this_user['bot_id']][0], $this->get_wep_type($this_enemyhar['weap'], $this_enemyhar['bot_id']), $new_uron, $this_enemyhar, $he_class, $this_user, $me_class, ($this->get_points('h', $this_user['bot_id']) - $new_uron), $this_user['maxhp'], $this->battle[$enemy_id][$this_user['bot_id']][2], $pos, $usertimeout));

                                    $mf_hit[$this_user['bot_id']]['hp']   = $new_uron;
                                    $mf_hit[$enemy_id]['pw']              = abs($mf[$enemy_id]['x8_min']);
                                    $mf_hit[$enemy_id]['hit_name']        = 'hit_udar';
                                    $mf_hit[$enemy_id]['hit_int']         = 1;

                                    $mf_hit[$this_user['bot_id']]['type'] = "udar";
                                    $mf_hit[$this_user['bot_id']]['uron'] = $new_uron;
                                 }

                         # END ��������� ����� ���� ����
						} else { # START ��������� ������

                              # ������� ����
                                $usertimeout = 0;
                                if ($istimeout == $this_user['bot_id']) { $usertimeout = 'usertimeout'; }

                                 if ($istimeout == $enemy_id) {

                                    $this->add_log ($this->razmen_log("block", $this->battle[$enemy_id][$this_user['bot_id']][0], $this->get_wep_type($this_enemyhar['weap'], $this_enemyhar['bot_id']),0, $this_enemyhar, $he_class, $this_user, $me_class, 0, 0, $this->battle[$enemy_id][$this_user['bot_id']][2], $pos, $istimeout));

                                    $mf_hit[$enemy_id]['hp']                  = abs(empty($mf_hit[$enemy_id]['hp']) ? 0 : $mf_hit[$enemy_id]['hp']);
                                    $mf_hit[$enemy_id]['pw']                  = abs($mf[$enemy_id]['x8_min']);
                                    $mf_hit[$this_user['bot_id']]['hit_name'] = 'hit_block';
                                    $mf_hit[$this_user['bot_id']]['hit_int']  = 1;

                                    $mf_hit[$this_user['bot_id']]['type']     = "block";
                                    $mf_hit[$this_user['bot_id']]['uron']     = "block";
                                 } else {
                                    $mf[$this_user['bot_id']]['user_block'] = 1;

   						    	$this->add_log ($this->razmen_log("block", $this->battle[$enemy_id][$this_user['bot_id']][0],
                                    $this->get_wep_type($this_enemyhar['weap'],
                                    $this_enemyhar['bot_id']), 0, $this_enemyhar,
                                    $he_class, $this_user, $me_class, 0, 0,
                                    $this->battle[$enemy_id][$this_user['bot_id']][2],
                                    $pos, $usertimeout));

                                    $mf_hit[$enemy_id]['hp']                  = abs(empty($mf_hit[$enemy_id]['hp']) ? 0 : $mf_hit[$enemy_id]['hp']);
                                    $mf_hit[$enemy_id]['pw']                  = abs($mf[$enemy_id]['x8_min']);
                                    $mf_hit[$this_user['bot_id']]['hit_name'] = 'hit_block';
                                    $mf_hit[$this_user['bot_id']]['hit_int']  = 1;

                                    $mf_hit[$this_user['bot_id']]['type']     = "block";
                                    $mf_hit[$this_user['bot_id']]['uron']     = 0;
                                 }

                          # END ��������� ������
						}

    return $mf_hit;
  };



 # ��� ����
   $mf_hit = $exchange_attacks($mf, "me", $mf_hit, $this->user, $this->enemyhar, $this->enemyhar['bot_id'], $istimeout);
 # ��� ����
   $mf_hit = $exchange_attacks($mf, "he", $mf_hit, $this->enemyhar, $this->user, $this->user['bot_id'], $istimeout);

   $statis_user_type  = $mf_hit[$this->user['bot_id']]['type'];
   $statis_user_uron  = $mf_hit[$this->user['bot_id']]['uron'];
   $statis_enemy_type = $mf_hit[$this->enemyhar['bot_id']]['type'];
   $statis_enemy_uron = $mf_hit[$this->enemyhar['bot_id']]['uron'];



                    # ��������� ��������� ����
                      $n1time = time() + 5  + rand(5, (5 + $this->get_count_team($this->userclass($bot_attacking['id'], ''))));
                      $n2time = time() + 10 + rand(5, (5 + $this->get_count_team($this->userclass($bot_defender['id'], ''))));

                      if ($n1time > 30 || $n1time < 3) { $n1time = time() + rand(5, 30); }
                      if ($n2time > 30 || $n2time < 3) { $n2time = time() + rand(5, 30); }
                      $n1time = time()+1;
                      $n2time = time()+1;

                    # ��������� ����� �������
                      $bot_attacking['next_udar_time'] = $n1time;
                      $bot_defender['next_udar_time']  = $n2time;


                       # ������ ������ � ���������

                           $at_hit    = '';
                           $at_hit_hp = $mf_hit[$enemy_at_id]['hp'];
                           $at_hit_pw = $mf_hit[$enemy_at_id]['pw'];

                         if ($at_hit_hp || $at_hit_pw) {

                           # �������� ��
                             if ($at_hit_hp) {
                               // $this->add_points("h", '-'.$at_hit_hp, $enemy_at_id);
                                $at_hit .= ",`hp` = `hp` - ".$at_hit_hp."";
                                $bot_attacking['hp'] -= $at_hit_hp;
                                $bot_user_attacking['hp'] -= $at_hit_hp;
                                $this->set_points('h', $bot_attacking['hp'],   $enemy_at_id);
                             }

                           # �������� �����
                             if ($at_hit_pw) {
                               // $this->add_points("p", '-'.$at_hit_pw, $enemy_at_id);
                                $at_hit .= ",`mana` = `mana` - ".$at_hit_pw."";
                                $bot_attacking['mana'] -= $at_hit_pw;
                                $bot_user_attacking['mana'] -= $at_hit_pw;
                                $this->set_points('p', $bot_attacking['mana'], $enemy_at_id);
                             }

                             if ($at_hit) {
                               if ($enemy_at_id > _BOTSEPARATOR_) {
                                   sql_query("UPDATE `bots` SET next_udar_time = ".$n1time.", `udar_time` = ".time()." $at_hit WHERE `id` = ".$enemy_at_id." LIMIT 1;");
                               } else {
                                   sql_query("UPDATE `users` SET `fullhptime` = '".time()."' $at_hit WHERE `id` = ".$enemy_at_id." LIMIT 1;");
                               }
                             }

                         }

                       # ������ ������ � ���������

                           $def_hit    = '';
                           $def_hit_hp = $mf_hit[$enemy_def_id]['hp'];
                           $def_hit_pw = $mf_hit[$enemy_def_id]['pw'];

                         if ($def_hit_hp || $def_hit_pw) {

                           # �������� ��
                             if ($def_hit_hp) {
                               // $this->add_points("h", '-'.$def_hit_hp, $enemy_def_id);
                                $def_hit .= ",`hp` = `hp` - ".$def_hit_hp."";
                                $bot_defender['hp'] -= $def_hit_hp;
                                $bot_user_defender['hp'] -= $def_hit_hp;
                                $this->set_points('h', $bot_defender['hp'],    $enemy_def_id);
                             }

                           # �������� �����
                             if ($def_hit_pw) {
                               // $this->add_points("p", '-'.$def_hit_pw, $enemy_def_id);
                                $def_hit .= ",`mana` = `mana` - ".$def_hit_pw."";
                                $bot_defender['mana'] -= $def_hit_pw;
                                $bot_user_defender['mana'] -= $def_hit_pw;
                                $this->set_points('p', $bot_defender['mana'],  $enemy_def_id);
                             }

                             if ($def_hit) {
                               if ($enemy_def_id > _BOTSEPARATOR_) {
                                   sql_query("UPDATE `bots` SET next_udar_time = ".$n2time.", `udar_time` = ".time()." $def_hit WHERE `id` = ".$enemy_def_id." LIMIT 1;");
                               } else {
                                   sql_query("UPDATE `users` SET `fullhptime` = '".time()."' $def_hit WHERE `id` = ".$enemy_def_id." LIMIT 1;");
                               }
                             }

                         }

                         # ����� ����




                         # ����� ���� ��� ���������� � �����������
                           if ($bot_attacking['is_hbot'] || $bot_attacking['is_hranbot']) {
                              sql_query("UPDATE `users` SET `hp` = ".$bot_attacking['hp'].", `hp_precise` = ".$bot_attacking['hp'].", `mana` = ".$bot_attacking['mana'].", `pw_precise` = ".$bot_attacking['mana']." WHERE `id` = '".$bot_attacking['prototype']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                           }
                           if ($bot_defender['is_hbot'] || $bot_defender['is_hranbot']) {
                              sql_query("UPDATE `users` SET `hp` = ".$bot_defender['hp'].",  `hp_precise` = ".$bot_defender['hp'].", `mana` = ".$bot_defender['mana'].", `pw_precise` = ".$bot_defender['mana']." WHERE `id` = '".$bot_defender['prototype']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                           }

                          # ����� ���� ������
                            $this->set_user_statis($bot_defender,  $statis_user_type,  $statis_enemy_type, $statis_user_uron,  $attaka_pos_bot2, 0, $block_pos_bot2);
                            $this->set_user_statis($bot_attacking, $statis_enemy_type, $statis_user_type,  $statis_enemy_uron, $attaka_pos_bot1, 0, $block_pos_bot1);

                            $this->battle[$enemy_def_id][$enemy_at_id] = array(0, 0, 0, time());
                            $this->battle[$enemy_at_id][$enemy_def_id] = array(0, 0, 0, time());

                          # ���� ���-�� ��������!
                            $this->fast_death_to($bot_user_attacking);
    					   	$this->fast_death_to($bot_user_defender);

                          # ����� ����� � ������!
                            $this->bots[$enemy_at_id]  = $bot_attacking;
                            $this->bots[$enemy_def_id] = $bot_defender;

                            $this->write_log();                          # ����� ���
                            $this->update_battle ("");

            }

      }
      }
    }


/***--------------------------
 * �������������� ������ ��� ��������
 **/

  function razmen_init_pet($abil_name, $pet_owner) {
   global $bot_attacking, $bot_user_attacking, $bot_defender, $bot_user_defender,
          $pet_bot_attacking, $pet_user_attacking, $pet_bot_defender, $pet_user_defender;

    $enemy_at_id    = $bot_attacking['id'];
    $enemy_def_id   = $bot_defender['id'];

  # �� ���� ������ ����
    $razmen_user = array();

    if($enemy_at_id && $bot_attacking['next_udar_time'] < time() && $enemy_def_id && $bot_defender['next_udar_time'] < time()) {

     $activate_abil  = 0;

	  if($bot_attacking['id'] && $bot_defender['id']) {

       $this->sort_teams($bot_attacking['id']);

        /*
          14:03:04 ������ Astral ������� '�����������' �� ����
          14:05:04 ������ ���� ����������� ������� ����� ���� +138 [362/694]
          14:04:36 ������� ������� ��������, � ���� ������� ����� �������������� ���� -128 [864/1066]
         */

      ######################################
      # ��������������� ����� ������� "����"
      ######################################

            switch ($abil_name) {
              case 'restores_life_to_owner':

              # �������� �������
                $pet_user_owner = sql_row("SELECT * FROM `users` WHERE `id` = '".$pet_owner."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");

                  if ($pet_user_owner['id']) {
                      $hp_add = mt_rand(floor($pet_bot_attacking['maxhp'] / 6), floor($pet_bot_attacking['maxhp'] / 4));
                      $hp_rand = $pet_user_owner['hp'] + $hp_add;

                      if ($hp_rand > $pet_user_owner['maxhp']) {
                       $hp_rand = $pet_user_owner['maxhp'];
                       $hp_add = $pet_user_owner['maxhp'] - $pet_user_owner['hp'];
                      }

                       # �������� ��� �� � ����� � ���
                         if ($hp_add > 0) {
                          sql_query("UPDATE `users` SET `hp` = `hp` + ".$hp_add.", `hp_precise` = `hp_precise` + ".$hp_add." WHERE `id` = '".$pet_user_owner['id']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                          $this->set_points("h", $hp_rand, $pet_user_owner['id']);

                          if ($pet_user_owner['invis']) {
                            $hp_add                  = '??';
                            $hp_rand                 = '??';
                            $pet_user_owner['maxhp'] = '??';
                          }

                          $this->add_log(nick_team($pet_user_attacking, $this->userclass($pet_bot_attacking['id'])).' �����������'.($pet_user_attacking['sex']?"":"�").' ������� ����� '.nick_team($pet_user_owner, $this->userclass($pet_user_owner['id'])).' <b class="bplus">+'.$hp_add.'</b> ['.($hp_rand).'/'.$pet_user_owner['maxhp'].']');

                          $activate_abil    = 1;

                        # ��������� ������������� ������
                      	  $new_stats        = array();
                      	  $new_stats        = unserialize($pet_bot_attacking['stats']);
                      	  $new_stats['abil']['use'] +=  1;
                         // sql_query("UPDATE `bots` SET `stats` = '".serialize($new_stats)."' WHERE id = '".$pet_bot_attacking['id']."' LIMIT 1;");
                         }
                  }

              break;

      ######################################
      # �������������� ���� �� ���������� "���"
      ######################################

              case 'extra_kick':

                  # ������ �����������
                    if ($bot_attacking['id'] == $pet_bot_attacking['id']) {
                      $team_enemy = $this->team_enemy;
                    } else {
                      $team_enemy = $this->team_mine;
                    }

                      $enemys = array();
                      foreach ($team_enemy as $k => $v_id) {
                         if ($v_id > _BOTSEPARATOR_) {
                          $user_en = sql_row("SELECT `hp` FROM `bots` WHERE `id` = '".$v_id."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                         } else {
                          $user_en = sql_row("SELECT `hp` FROM `users` WHERE `id` = '".$v_id."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                         }
                        # ������ ������
                         if ($user_en['hp'] > 0) { $enemys[] = $v_id; }
                      }

                       $id_enemy = $enemys[rand(0,count($enemys)-1)];

                       if ($id_enemy > _BOTSEPARATOR_) {
                        $user_kick = sql_row("SELECT * FROM `bots` WHERE id = '".$id_enemy."' AND hp > 0 LIMIT 1;");
                        $user_kick['login']    = $user_kick['name'];
                        $user_kick['bothidro'] = 0;
                        $user_kick['bothran']  = 0;
                        $user_kick['sex']      = 1;

                        $new_stats = unserialize($user_kick['stats']);
                        $user_kick['invis'] = $new_stats['invis'] ? $new_stats['invis'] : 0;

                       } else {
                        $user_kick = sql_row("SELECT `id`, `login`, `invis`, `hp`, `maxhp`, `mana`, `maxmana`, `bothidro`, `bothran`, `sex` FROM `users` WHERE `id` = '".$id_enemy."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                       }

                         if (!empty($user_kick['id'])) {

                          # ����� ���� �� ���� �� ���
                            $pet_bot_attacking['maxhp'] = $this->get_points("mh", $id_enemy);

                          # ��������� ������� ������ ��
                            $hp_min  = abs(mt_rand(floor($pet_bot_attacking['maxhp'] / 6), floor($pet_bot_attacking['maxhp'] / 4)));
                            $hp_rand = $user_kick['hp'] - $hp_min;

                            if ($hp_rand <= 0) {
                             $hp_rand = 0;
                             $hp_min = $user_kick['hp'];
                            }

                          # ������ ����� ��
                            $user_kick['hp'] = $hp_rand;

                          # ���� ���������� (����)
                             if ($id_enemy > _BOTSEPARATOR_) {
                                sql_query("UPDATE `bots` SET `hp` = ".$hp_rand." WHERE `id` = '".$user_kick['id']."' LIMIT 1;");
                                $this->set_points("h", $hp_rand, $user_kick['id']);
                             } else {
                                sql_query("UPDATE `users` SET `hp` = ".$hp_rand.", `hp_precise` = `hp_precise` - ".$hp_min." WHERE `id` = '".$user_kick['id']."' LIMIT 1;");
                                $this->set_points("h", $hp_rand, $user_kick['id']);
                             }

                            # �������� ��� �� � ����������� ��� ������ ����
                              if ($bot_attacking['id'] == $pet_bot_attacking['id']) {
                                $bot_defender['hp']       = $hp_rand;
                                $bot_user_defender['hp']  = $hp_rand;
                              } else {
                                $bot_attacking['hp']      = $hp_rand;
                                $bot_user_attacking['hp'] = $hp_rand;
                              }

                             if ($user_kick['invis'] == 1) {
                               $hp_min = "??";
                               $hp_rand = "??";
                               $user_kick['maxhp'] = "??";
                             }

                             $this->add_log(nick_team($user_kick, $this->userclass($user_kick['id'])).' ��������, � '.nick_team($pet_user_attacking, $this->userclass($pet_bot_attacking['id'])).' ����� �������������� ���� <b>-'.$hp_min.'</b> ['.($hp_rand).'/'.$user_kick['maxhp'].']');

                             $activate_abil    = 1;

                           # ��������� ������������� ������
                      	     $new_stats        = array();
                      	     $new_stats        = unserialize($pet_bot_attacking['stats']);
                      	     $new_stats['abil']['use'] +=  1;

                           # �� ���� ������ ����
                             $razmen_user['id']       = $user_kick['id'];
                             $razmen_user['login']    = $user_kick['login'];
                             $razmen_user['bot_id']   = $user_kick['id'];
                             $razmen_user['battle']   = $user_kick['battle'];
                             $razmen_user['maxhp']    = $user_kick['maxhp'];
                             $razmen_user['hp']       = $user_kick['hp'];
                             $razmen_user['mana']     = $user_kick['mana'];
                             $razmen_user['maxmana']  = $user_kick['maxmana'];
                             $razmen_user['bothidro'] = $user_kick['bothidro'];
                             $razmen_user['bothran']  = $user_kick['bothran'];
                             $razmen_user['sex']      = $user_kick['sex'];

                            // sql_query("UPDATE `bots` SET `stats` = '".serialize($new_stats)."' WHERE id = '".$pet_bot_attacking['id']."' LIMIT 1;");
                         }

              break;

      ######################################
      # �������� ���� � ������������� "������"
      ######################################

              case 'fire_restores_life_to_owner':

                if (rand(1, 100) >= 50) {

                 // fire - 50% ������ �� ������� �������
                  # ������ �����������
                    if ($bot_attacking['id'] == $pet_bot_attacking['id']) {
                      $team_enemy = $this->team_enemy;
                    } else {
                      $team_enemy = $this->team_mine;
                    }

                      $enemys = array();
                      foreach ($team_enemy as $k => $v_id) {
                         if ($v_id > _BOTSEPARATOR_) {
                          $user_en = sql_row("SELECT `hp` FROM `bots` WHERE `id` = '".$v_id."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                         } else {
                          $user_en = sql_row("SELECT `hp` FROM `users` WHERE `id` = '".$v_id."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                         }
                        # ������ ������
                         if ($user_en['hp'] > 0) { $enemys[] = $v_id; }
                      }

                       $id_enemy = $enemys[rand(0,count($enemys)-1)];

                       if ($id_enemy > _BOTSEPARATOR_) {
                        $user_kick = sql_row("SELECT * FROM `bots` WHERE id = '".$id_enemy."' AND hp > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                        $user_kick['login'] = $user_kick['name'];
                        $user_kick['mana']  = $user_kick['mana'];

            	        $new_stats = unserialize($user_kick['stats']);
                        $user_kick['invis'] = $new_stats['invis'] ? $new_stats['invis'] : 0;

                       } else {
                        $user_kick = sql_row("SELECT `id`, `login`, `invis`, `hp`, `maxhp`, `mana`, `maxmana` FROM `users` WHERE `id` = '".$id_enemy."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                       }

                         if ($user_kick['id']) {

                          # ��������� ������� ������ ��
                            $pw_min   = floor($user_kick['mana'] / 2);
                            $pw_rand  = $user_kick['mana'] - $pw_min;

                            if ($pw_rand <= 0) {
                             $pw_rand = 0;
                             $pw_min  = $user_kick['mana'];
                            }


                            if ($pw_min > 1) {

                          # ���� ���������� (����)
                             if ($id_enemy > _BOTSEPARATOR_) {
                               sql_query("UPDATE `bots` SET `mana` = `mana` - ".$pw_min." WHERE `id` = '".$user_kick['id']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                               $this->set_points("p", $pw_rand, $user_kick['id']);
                             } else {
                               sql_query("UPDATE `users` SET `mana` = `mana` - ".$pw_min.", `pw_precise` = ".$pw_min." WHERE `id` = '".$user_kick['id']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                               $this->set_points("p", $pw_rand, $user_kick['id']);
                             }

                            # �������� ��� �� � ����������� ��� ������ ����
                              if ($bot_attacking['id'] == $pet_bot_attacking['id']) {
                                $bot_defender['mana']       = $pw_rand;
                                $bot_user_defender['mana']  = $pw_rand;
                              } else {
                                $bot_attacking['mana']      = $pw_rand;
                                $bot_user_attacking['mana'] = $pw_rand;
                              }

                             if ($user_kick['invis'] == 1) {
                               $pw_min = "??";
                               $pw_rand = "??";
                               $user_kick['maxmana'] = "??";
                             }

                             $this->add_log(nick_team($user_kick, $this->userclass($user_kick['id'])).' ��������, � '.nick_team($pet_user_attacking, $this->userclass($pet_bot_attacking['id'])).' ����� �������� ���� �� ������������ <b>-'.$pw_min.'</b> ['.($pw_rand).'/'.$user_kick['maxmana'].']');

                             $activate_abil    = 1;

                           # ��������� ������������� ������
                      	     $new_stats        = array();
                      	     $new_stats        = unserialize($pet_bot_attacking['stats']);
                      	     $new_stats['abil']['use'] +=  1;

                            // sql_query("UPDATE `bots` SET `stats` = '".serialize($new_stats)."' WHERE id = '".$pet_bot_attacking['id']."' LIMIT 1;");
                            }
                         }


                } else {

              # �������� �������
                $pet_user_owner = sql_row("SELECT * FROM `users` WHERE `id` = '".$pet_owner."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");

                  if ($pet_user_owner['id']) {
                      $hp_add = mt_rand(floor($pet_bot_attacking['maxhp'] / 6), floor($pet_bot_attacking['maxhp'] / 4));
                      $hp_rand = $pet_user_owner['hp'] + $hp_add;

                      if ($hp_rand > $pet_user_owner['maxhp']) {
                       $hp_rand = $pet_user_owner['maxhp'];
                       $hp_add  = $pet_user_owner['maxhp'] - $pet_user_owner['hp'];
                      }

                       # �������� ��� �� � ����� � ���
                         if ($hp_add > 0) {
                          sql_query("UPDATE `users` SET `hp` = `hp` + ".$hp_add.", `hp_precise` = `hp_precise` + ".$hp_add." WHERE `id` = '".$pet_user_owner['id']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                          $this->set_points("h", $hp_rand, $pet_user_owner['id']);

                          if ($pet_user_owner['invis']) {
                            $hp_add                  = '??';
                            $hp_rand                 = '??';
                            $pet_user_owner['maxhp'] = '??';
                          }

                          $this->add_log(nick_team($pet_user_attacking, $this->userclass($pet_bot_attacking['id'])).' �����������'.($pet_user_attacking['sex']?"":"�").' ������� ����� '.nick_team($pet_user_owner, $this->userclass($pet_user_owner['id'])).' <b class="bplus">+'.$hp_add.'</b> ['.($hp_rand).'/'.$pet_user_owner['maxhp'].']');

                          $activate_abil    = 1;

                        # ��������� ������������� ������
                      	  $new_stats        = array();
                      	  $new_stats        = unserialize($pet_bot_attacking['stats']);
                      	  $new_stats['abil']['use'] +=  1;
                         // sql_query("UPDATE `bots` SET `stats` = '".serialize($new_stats)."' WHERE id = '".$pet_bot_attacking['id']."' LIMIT 1;");
                         }
                  }
                }

              break;

      ######################################
      # ����������� � �����������
      ######################################

              case 'resurrection_killing':

                  # ������ �����������
                    if ($bot_attacking['id'] == $pet_bot_attacking['id']) {
                      $team_enemy = $this->team_enemy;
                    } else {
                      $team_enemy = $this->team_mine;
                    }

                      $enemys = array();
                      foreach ($team_enemy as $k => $v_id) {
                         if ($v_id > _BOTSEPARATOR_) {
                          $user_en = sql_row("SELECT `hp` FROM `bots` WHERE `id` = '".$v_id."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                         } else {
                          $user_en = sql_row("SELECT `hp` FROM `users` WHERE `id` = '".$v_id."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                         }
                        # ������ ������
                         if ($user_en['hp'] > 0) { $enemys[] = $v_id; }
                      }

                       $id_enemy = $enemys[rand(0,count($enemys)-1)];

                       if ($id_enemy > _BOTSEPARATOR_) {
                        $user_kick = sql_row("SELECT * FROM `bots` WHERE id = '".$id_enemy."' AND hp > 0 LIMIT 1;");
                        $user_kick['login']    = $user_kick['name'];
                        $user_kick['bothidro'] = 0;
                        $user_kick['bothran']  = 0;
                        $user_kick['sex']      = 1;

                        $new_stats = unserialize($user_kick['stats']);
                        $user_kick['invis'] = $new_stats['invis'] ? $new_stats['invis'] : 0;

                       } else {
                        $user_kick = sql_row("SELECT `id`, `login`, `invis`, `hp`, `maxhp`, `mana`, `maxmana`, `bothidro`, `bothran`, `sex` FROM `users` WHERE `id` = '".$id_enemy."' AND `hp` > 0 AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                       }

                         if (!empty($user_kick['id'])) {

                          # ����� ���� �� ���� �� ���
                            $pet_bot_attacking['maxhp'] = $this->get_points("mh", $id_enemy);

                          # ��������� ������� ������ ��
                            $hp_min  = 1;//abs(mt_rand(floor($pet_bot_attacking['maxhp'] / 6), floor($pet_bot_attacking['maxhp'] / 4)));
                            $hp_rand = /*$user_kick['hp'] - */$hp_min;

                            if ($hp_rand <= 0) {
                             $hp_rand = 0;
                             $hp_min = $user_kick['hp'];
                            }

                          # ������ ����� ��
                            $user_kick['hp'] = $hp_rand;

                          # ���� ���������� (����)
                             if ($id_enemy > _BOTSEPARATOR_) {
                                sql_query("UPDATE `bots` SET `hp` = ".$hp_rand." WHERE `id` = '".$user_kick['id']."' LIMIT 1;");
                                $this->set_points("h", $hp_rand, $user_kick['id']);
                             } else {                                                             //`hp_precise` -
                                sql_query("UPDATE `users` SET `hp` = ".$hp_rand.", `hp_precise` = ".$hp_min." WHERE `id` = '".$user_kick['id']."' LIMIT 1;");
                                $this->set_points("h", $hp_rand, $user_kick['id']);
                             }

                            # �������� ��� �� � ����������� ��� ������ ����
                              if ($bot_attacking['id'] == $pet_bot_attacking['id']) {
                                $bot_defender['hp']       = $hp_rand;
                                $bot_user_defender['hp']  = $hp_rand;
                              } else {
                                $bot_attacking['hp']      = $hp_rand;
                                $bot_user_attacking['hp'] = $hp_rand;
                              }

                             $this->add_log(nick_team($pet_user_attacking, $this->userclass($pet_bot_attacking['id'])).' ������� ����������� �� ' . nick_team($user_kick, $this->userclass($user_kick['id'])));

                             $activate_abil    = 1;

                           # ��������� ������������� ������
                      	     $new_stats        = array();
                      	     $new_stats        = unserialize($pet_bot_attacking['stats']);
                      	     $new_stats['abil']['use'] +=  1;

                           # �� ���� ������ ����
                             $razmen_user['id']       = $user_kick['id'];
                             $razmen_user['login']    = $user_kick['login'];
                             $razmen_user['bot_id']   = $user_kick['id'];
                             $razmen_user['battle']   = $user_kick['battle'];
                             $razmen_user['maxhp']    = $user_kick['maxhp'];
                             $razmen_user['hp']       = $user_kick['hp'];
                             $razmen_user['mana']     = $user_kick['mana'];
                             $razmen_user['maxmana']  = $user_kick['maxmana'];
                             $razmen_user['bothidro'] = $user_kick['bothidro'];
                             $razmen_user['bothran']  = $user_kick['bothran'];
                             $razmen_user['sex']      = $user_kick['sex'];

                             sql_query("UPDATE `bots` SET `stats` = '".serialize($new_stats)."' WHERE id = '".$pet_bot_attacking['id']."' LIMIT 1;");
                         }

              break;

            }


                         # ����� ����
                           /*$this->set_points('h', $bot_attacking['hp'],   $enemy_at_id);
                           $this->set_points('p', $bot_attacking['mana'], $enemy_at_id);

                           $this->set_points('h', $bot_defender['hp'],    $enemy_def_id);
                           $this->set_points('p', $bot_defender['mana'],  $enemy_def_id);*/


                         # ����� ���� ��� ���������� � �����������
                           if ($bot_attacking['is_hbot'] || $bot_attacking['is_hranbot']) {
                             sql_query("UPDATE `users` SET `hp` = ".$bot_attacking['hp'].", `mana` = ".$bot_attacking['mana']." WHERE `id` = '".$bot_user_attacking['id']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                           }
                           if ($bot_defender['is_hbot'] || $bot_defender['is_hranbot']) {
                             sql_query("UPDATE `users` SET `hp` = ".$bot_defender['hp'].", `mana` = ".$bot_defender['mana']." WHERE `id` = '".$bot_user_defender['id']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                           }

                         # ��������� ��������� ����
                           if ($activate_abil) {
                             $n1time = time() + 5  + rand(5, (5 + $this->get_count_team($this->userclass($bot_attacking['id'], ''))));
                             sql_query("UPDATE `bots` SET `udar_time` = ".time().", next_udar_time = ".$n1time." WHERE `id` = '".$bot_attacking['id']."' LIMIT 1;");
                             $n2time = time() + 10 + rand(5, (5 + $this->get_count_team($this->userclass($bot_defender['id'], ''))));
                             sql_query("UPDATE `bots` SET `udar_time` = ".time().", next_udar_time = ".$n2time." WHERE `id` = '".$bot_defender['id']."' LIMIT 1;");
                           }

                           $this->battle[$enemy_def_id][$enemy_at_id] = array(0,0,0,time());
                           $this->battle[$enemy_at_id][$enemy_def_id] = array(0,0,0,time());


                         # ���� ���-�� ��������!
                           /*$this->fast_death_to($bot_user_attacking);
    					   $this->fast_death_to($bot_user_defender);*/
                           if (!empty($razmen_user['id'])) {
                             $this->fast_death_to($razmen_user);
                           }

                           $this->write_log();   # ����� ���
                           $this->update_battle("");
	  }
    }
  }

/***--------------------------
 * �������� � ����� �������
 **/

            function userclass($id, $type = 'B') {
               if (in_array ($id, $this->t1)) return $type."1"; else return $type."2";
            }

/***--------------------------
 * ��������� ���
 **/

			function update_battle($df) {
                global $user, $data_battle;

                    $battle_sql = '';
                    $save_stats = (count($this->t1) + count($this->t2) > 100)?0:1;

                    if ($this->battle_data['teams']      != serialize($this->battle))     { $battle_sql .= "`teams` = '".serialize($this->battle)."',  "; }
                  # if ($this->battle_data['battle_eff'] != serialize($this->battle_eff)) { $battle_sql .= "`battle_eff` = '".serialize($this->battle_eff)."',  "; }
                    if ($this->battle_data['t1list']     != serialize($this->t1list))     { $battle_sql .= "`t1list` = '".serialize($this->t1list)."',  "; }
                    if ($this->battle_data['t2list']     != serialize($this->t2list))     { $battle_sql .= "`t2list` = '".serialize($this->t2list)."',  "; }
                  # if ($save_stats && $this->battle_data['b_hint_nbp'] != serialize($this->b_hint_nbp)) { $battle_sql .= "`b_hint_nbp` = '".serialize($this->b_hint_nbp)."',  "; }
                    if ($save_stats && $this->battle_data['statis']     != serialize($this->statis))     { $battle_sql .= "`statis` = '".serialize($this->statis)."',  "; }

                    if ($battle_sql) {
                      $data_battle['to1'] = time();
                      $data_battle['to2'] = time();

    			      return sql_query("UPDATE `battle` SET $battle_sql `to1` = '".time()."',`to2` = '".time()."' WHERE `id` = '".$this->battle_data['id']."' LIMIT 1;");
                    } else {
                      return false;
                    }

			}

/***--------------------------
 * �������� ����� � ���
 **/

			function add_log ($text, $parm = array()) {

			     $this->log .= '<span class="date">'.date("H:i:s", time()).'</span> <span style="font-size: 12px;">'.$text.'</span></span><BR>';

                 if ($parm['my_id']) $my_id = $parm['my_id']; else $my_id = 0;
                 if ($parm['he_id']) $he_id = $parm['he_id']; else $he_id = 0;

                 $mes = date("H:i:s")."|".$this->battle_log_id."|".$my_id."|".$he_id."|"."".$text;

                  if (empty($this->battle_log)) {
                    $this->battle_log = $mes;
                  } else {
                    $this->battle_log .= "||".$mes;
                  }

			}

/***--------------------------
 * ������ ������ � ����
 **/

			function write_log () {
              global  $battle_data;
               $cur_battleid = $battle_data['id'];

               if ($this->user['battle']) { $cur_battleid = $this->user['battle']; }

               if($this->log) { $this->log = $this->log."<hr>"; }
               if ($this->log_channel == "l") {
                 $file_log = 'backup/logs/battle'.$cur_battleid.'.txt';
                 $text = file_get_contents($file_log); # ���������
                 if ($text) {
                   usleep(350000);
                   $text = str_replace('<BR><hr>', '', $text);
                   $text = substr($text, 39);
                   $my_id = 0;
                   $he_id = 0;
                   $battle_log   = date("H:i:s")."|".rand(1, 100)."|".$my_id."|".$he_id."|"."".$text;
                   CHAT::chat_battle_add_lb($battle_log, $cur_battleid, array('addXMPP' => true));
                 }
               }

               if (($this->user['battle'] || $cur_battleid) && $this->log) {

                  if ($this->battle_log) {
                    CHAT::chat_battle_add_lb($this->battle_log, $cur_battleid, array('addXMPP' => true));
                  }

                # ����� ���������� � ����,
                # ��������� ���� FILE_APPEND flag ��� ����������� ����������� � ����� �����
                # � ���� LOCK_EX ��� �������������� ������ ������� ����� ���-������ ������ � ������ �����

                  if ($this->user['battle']) { $cur_battleid = $this->user['battle']; }
                  $file_log = $_SERVER["DOCUMENT_ROOT"].'/backup/logs/battle'.$cur_battleid.'.txt';
                  file_put_contents($file_log, $this->log, LOCK_EX | FILE_APPEND);

                  $this->battle_log = '';
  			      $this->log = '';
               }
			}

	}


function unset_bot($user_bot){
$ssdf = array('info', 'lozung', 'memo_text', 'helphint_viewed', 'main_buttons', 'chatoptions', 'achievements_finish', 'thematic_event',
'panel_buttons', 'actions', 'mine_resources', 'rewards', 'animals_stall', 'data_tournament', 'inv_block', 'box_station', 'chat_command_options',
'achievements', 'hide_panel_access', 'main_setting', 'my_menu', 'collection', 'casino', 'zarnica', 'shadow_bought', 'email', 'pass',
'pass_additional', 'pass2', 'pass_additional2', 'realname', 'borndate', 'usercreate', 'http', 'radio', 'rep', 'komplekts',
'battles_today', 'battleautosubmit', 'battleautoupd', 'bot_battle_panel', 'pass_token', 'pass_enter_code', 'block', 'ip', 'enter_block',
'enter_block_time', 'enter_status', 'speaking');


foreach ($ssdf as $k => $v) unset($user_bot[$v]);
return $user_bot;
}

###########################################
#   ������ ������ � ��� ��� ���� �����
###########################################


#        10026213                                                                           #  AND next_udar_time < ".time()."
 $bots_in_battle = sql_query("SELECT * FROM `bots` WHERE `battle` > 0  AND `hp` > 0  AND `city` = '".INCITY."' ORDER BY `bots`.`next_udar_time` ASC, `bots`.`id` DESC;");

 $bot_battle_id     = 0;        # �� ���
 $data_battle       = 0;        # ��� ���
 $data_users        = array();  # ��� ���������� ������
 $data_battles      = array();  # ��� ���������� ������
 $old_razmen_batle  = array();   #

 $cur_nextudar = 5;      # �������������� ������� ���� �����

 if (!empty($bots_in_battle))
 while ($bot_attacking = fetch_array($bots_in_battle)) {

  $bot_at_id     = $bot_attacking['id'];
  $bot_battle_id = $bot_attacking['battle'];

  # ������ ������ ���� ������ � ���� ��� ��� ���
   if (!in_array($bot_battle_id, $old_razmen_batle)) {

   # ��� ��� ��� �����������
     if (empty($data_battles[$bot_battle_id])) {
       $data_battle = sql_row("SELECT * FROM `battle` WHERE `id` = '".$bot_battle_id."' AND `user_update_battle` < ".(time()-2)." AND `win` = 3 AND `battle_end` = 0 LIMIT 1;");
       if ($data_battle['id']) {
         $data_battles[$bot_battle_id] = $data_battle;
       }
     } else {
       $data_battle = $data_battles[$bot_battle_id];
     }

       if ($data_battle['id'] && $bot_attacking['hp'] > 0 && $bot_battle_id && $bot_attacking['next_udar_time'] < time()) {

         # ���� � ��� ��� �������� ������� �� ������ �������������� �� 1 ���.
         if ($data_battle['user_update_battle'] > 0 && ($data_battle['user_update_battle'] + (5 * 60)) < time()) {
            sql_query("UPDATE `battle` SET `auto_completion` = '".(time() + 60)."' WHERE `id` = '".$data_battle['id']."' LIMIT 1;");
         }

         if (!empty($data_users[$bot_attacking['prototype']])) {
           $bot_user_attacking = $data_users[$bot_attacking['prototype']];
         } else {
           if ($bot_attacking['prototype']) {
             $bot_user_attacking = sql_row("SELECT * FROM `users` WHERE `id` = '".$bot_attacking['prototype']."' LIMIT 1;");
             $data_users[$bot_attacking['prototype']] = $bot_user_attacking;
           }
         }

         if ($bot_attacking['prototype'] == 0 && !empty($bot_attacking['stats'])) {
	        $new_stats = array();
	        $new_stats = unserialize($bot_attacking['stats']);

            $bot_user_attacking['sex']    = $new_stats['sex'];
            $bot_user_attacking['shit']   = $new_stats['shit'];
            $bot_user_attacking['invis']  = $new_stats['invis'];
            $bot_user_attacking['id']     = -1;
            $bot_user_attacking['weap']   = $new_stats['weap'] ? $new_stats['weap'] : $bot_user_attacking['weap'] ? $bot_user_attacking['weap'] : 0;
            $bot_user_attacking['login']  = $bot_attacking['name'];
            $bot_user_attacking['vid']    = $new_stats['vid'];
            $bot_user_attacking['user_id']= $new_stats['user_id'];

            $bot_attacking['invis']       = $new_stats['invis'];

         }

         $bot_user_attacking['bot_id']     = $bot_attacking['id'];
         $bot_user_attacking['login']      = $bot_attacking['name'];
         $bot_user_attacking['hp']         = $bot_attacking['hp'];
         $bot_user_attacking['maxhp']      = $bot_attacking['maxhp'];
         $bot_user_attacking['mana']       = $bot_attacking['mana'];
         $bot_user_attacking['maxmana']    = $bot_attacking['maxmana'];
         $bot_user_attacking['battle']     = $bot_battle_id;
         $bot_user_attacking['bot_stats']  = $bot_attacking['stats'];

         $bot_user_attacking = unset_bot($bot_user_attacking);

           @$battle_bot = new fbattle_bot($data_battle);

          # �������� �������
            $battle_bot->enemy = $battle_bot->select_enemy();
      	    $bot_def_id        = $battle_bot->enemy;

    	       if($bot_def_id > _BOTSEPARATOR_ && $bot_at_id) {

                if (empty($battle_bot->battle[$bot_at_id][$bot_def_id][3])) {
                  $battle_bot->battle[$bot_at_id][$bot_def_id] = array(0, 0, 0, (time() + $cur_nextudar));
                  continue;
                }
                if (empty($battle_bot->battle[$bot_def_id][$bot_at_id][3])) {
                  $battle_bot->battle[$bot_def_id][$bot_at_id] = array(0, 0, 0, (time() + $cur_nextudar));
                  continue;
                }

               # ����� ������������ �����
                 $btime_my = $battle_bot->battle[$bot_at_id][$bot_def_id][3] + $cur_nextudar;
               # ����� ������������ �����
                 $btime_he = $battle_bot->battle[$bot_def_id][$bot_at_id][3] + $cur_nextudar;

               # ������� �������� ������
                 if ((empty($btime_my) || $btime_my < time())
                 &&  (empty($btime_he) || $btime_he < time())
                 && $bot_attacking['id'] && $bot_attacking['next_udar_time'] < time()
                 ) {

                   # ���� ���� � ���� ���� ��� �� ��������� ����� � ���
                     if (empty($battle_bot->bots[$bot_def_id])) {
                       $bot_defender = sql_row("SELECT * FROM `bots` WHERE `id` = '".$bot_def_id."' AND `hp` > 0 LIMIT 1;");
                       $battle_bot->bots[$bot_def_id] = $bot_defender;
                     } else {
                       $bot_defender = $battle_bot->bots[$bot_def_id];
                     }

                     # ������� ����� ���� ��� ��������� �� ��� � ���
                       if ($battle_bot->fast_death_def($bot_def_id, $bot_defender)) { continue; }

                     # �������� �� �������� ���� ����������
                       if (!empty($battle_bot->bots[$bot_at_id]) && $battle_bot->bots[$bot_at_id]['hp'] <= 0) $dead_at_bot = 1; else $dead_at_bot = 0;

                     # �������� �� �������� ���� ���������
                       if (!empty($battle_bot->bots[$bot_def_id]) && $battle_bot->bots[$bot_def_id]['hp'] <= 0) $dead_def_bot = 1; else $dead_def_bot = 0;

                      # ������ ������ ��� ����� �����
                         if (!empty($bot_defender['id']) && $bot_defender['hp'] > 0 && empty($dead_at_bot) && empty($dead_def_bot)) {

                           if (!empty($data_users[$bot_defender['prototype']])) {
                               $bot_user_defender = $data_users[$bot_defender['prototype']];
                           } else {
                             if ($bot_defender['prototype']) {
                               $bot_user_defender = sql_row("SELECT * FROM `users` WHERE `id` = '".$bot_defender['prototype']."' LIMIT 1;");
                               $data_users[$bot_defender['prototype']] = $bot_user_defender;
                             }
                           }

                             if ($bot_defender['prototype'] == 0 && $bot_defender['stats']) {
                    	        $new_stats = array();
                    	        $new_stats = unserialize($bot_defender['stats']);

                                $bot_user_defender['sex']     = isset($new_stats['sex']) ? $new_stats['sex'] : 0;
                                $bot_user_defender['shit']    = isset($new_stats['shit']) ? $new_stats['shit'] : 0;
                                $bot_user_defender['invis']   = isset($new_stats['invis']) ? $new_stats['invis'] : 0;
                                $bot_user_defender['id']      = -1;
                                $bot_user_defender['weap']    = $new_stats['weap'] ? $new_stats['weap'] : $bot_user_defender['weap'] ? $bot_user_defender['weap'] : 0;
                                $bot_user_defender['login']   = $bot_defender['name'];
                                $bot_user_defender['vid']     = $new_stats['vid'];
                                $bot_user_defender['user_id'] = $new_stats['user_id'];

                                $bot_defender['invis']        = $bot_user_defender['invis'];

                             }

                             $bot_user_defender['bot_id']           = $bot_defender['id'];
                             $bot_user_defender['login']            = $bot_defender['name'];
                             $bot_user_defender['hp']               = $bot_defender['hp'];
                             $bot_user_defender['maxhp']            = $bot_defender['maxhp'];
                             $bot_user_defender['mana']             = $bot_defender['mana'];
                             $bot_user_defender['maxmana']          = $bot_defender['maxmana'];
                             $bot_user_defender['battle']           = $bot_battle_id;
                             $bot_user_defender['bot_stats']        = $bot_defender['stats'];

                             $bot_user_defender = unset_bot($bot_user_defender);

                           # ����� ������� +- 1 ��� ��� �������� �� ����� � ������ �����
                             $date_pora_change = array('damask' => -3600, 'alamut' => 3600, 'calydon' => 3600);

                              if (rand(1, 100) <= 15 && (($data_battle['to1'] + 1) < time() && ($data_battle['to2'] + 1) < time())) {

                              # ��������� ������ �������� � �� ����������
                                $pet_is_abil = 0;

                                # ���� �������� � �����
                                  if ($bot_user_attacking['vid'] > 0 && $bot_user_attacking['user_id'] && !empty($bot_attacking['stats'])) {
                                	  $abil_stats         = array();
                                	  $abil_stats         = unserialize($bot_attacking['stats']);
                                	  $abil_name          = $abil_stats['abil']['name'];
                                	  $pet_is_abil        = $abil_stats['abil']['usemax'] - $abil_stats['abil']['use'];
                                	  $pet_owner          = $bot_user_attacking['user_id'];

                                	  $pet_bot_attacking  = $bot_attacking;
                                	  $pet_user_attacking = $bot_user_attacking;

                                	  $pet_bot_defender   = $bot_defender;
                                	  $pet_user_defender  = $bot_user_defender;
                                  }

                                # ���� �������� � ������
                                  if (empty($pet_is_abil) && $bot_user_defender['vid'] > 0 && $bot_user_defender['user_id'] && !empty($bot_defender['stats'])) {
                                	  $abil_stats         = array();
                                	  $abil_stats         = unserialize($bot_defender['stats']);
                                	  $abil_name          = $abil_stats['abil']['name'];
                                	  $pet_is_abil        = $abil_stats['abil']['usemax'] - $abil_stats['abil']['use'];
                                	  $pet_owner          = $bot_user_defender['user_id'];

                                	  $pet_bot_attacking  = $bot_defender;
                                	  $pet_user_attacking = $bot_user_defender;

                                	  $pet_bot_defender   = $bot_attacking;
                                	  $pet_user_defender  = $bot_user_attacking;
                                  }

                                if ($pet_is_abil > 0) {
                                  # ������ ��� �������� "�������������� ����"
                                    $last_upd_batl = strtotime($data_battle['date']) + $date_pora_change[INCITY];
                                    if ($last_upd_batl != time()) { # �� ������ ������ ���� �� �������� ���� �������
                                      $battle_bot->razmen_init_pet($abil_name, $pet_owner, $abil_stats);
                                      $old_razmen_batle[$bot_battle_id] = $bot_battle_id;
                                    }
                                }

                              } elseif((($data_battle['to1'] + 1) < time() && ($data_battle['to2'] + 1) < time())) {

                              # ������ ��� ������
                                $last_upd_batl = strtotime($data_battle['date']) + $date_pora_change[INCITY];
                                if ($last_upd_batl != time()
                                && $battle_bot->get_points("h", $bot_user_defender['bot_id']) > 0
                                && $battle_bot->get_points("h", $bot_user_attacking['bot_id']) > 0
                                ) { # �� ������ ������ ���� �� �������� ���� �������

                                  $battle_bot->razmen_init();
                                  $old_razmen_batle[$bot_battle_id] = $bot_battle_id;

                                # ���������
                                  $data_battles[$bot_battle_id]['to1']  = time();
                                  $data_battles[$bot_battle_id]['to2']  = time();
                                  $data_battles[$bot_battle_id]['date'] = date("Y-m-d H:i:s");
                                }
                                //sleep(1);
                              }

                             if ($bot_attacking['hp'] <= 0 || $bot_defender['hp'] <= 0) {

                                $battle_bot->fast_team_death(2);
                             }

                              if ($battle_bot->battle_end($bot_battle_id)) {
                               $battle_bot->write_log();  # ����� ���
                               break;
                              }

                            # continue;                                   # ����������� ��� ��� �������� ����
                         }
                 }

    	       } else {
                  if ($battle_bot->get_timeout($bot_at_id) && $bot_attacking['hp'] > 0 && !in_array($data_battle['type'], array(30))) {

                      // ���� ���� 1 � ������
                      if ($data_battle['timeout'] <= 1) {

                        $old_razmen_batle[$bot_battle_id] = $bot_battle_id;

                        $bot_attacking['invis'] = $bot_user_attacking['invis'] ? 1 : 0;

                        $battle_bot->spare_no($bot_attacking);  # ��� �� �����
                        if ($battle_bot->battle_end($bot_battle_id)) {
                          $battle_bot->write_log();   # ����� ���
                          break;
                        }

                      } else {

                      // ������ ��� � ����� �������
                         $battle_bot->sort_teams($bot_attacking['id']);
                      // ������ ��������
                         foreach ($battle_bot->team_enemy as $v => $k_id) {

                             # �� ���������
                               $enemy_id = $k_id;
                               if (!in_array($k_id, $battle_bot->texit)) { # �� ��� ����� �� ���

                                $itout = time() - ($battle_bot->battle[$enemy_id][$bot_attacking['id']][3] + (abs(count(abs(count($battle_bot->battle[$enemy_id]))))*60));
                                   if (//($itout > ($battle_bot->battle_data['timeout']*60) || $itout > 300)
                                   //&& $battle_bot->battle[$enemy_id][$bot_attacking['id']][0]
                                   //&&
                                   $bot_attacking['hp'] > 0 && $battle_bot->get_points('h', $enemy_id) > 0) {

                                       if (rand(1, 100) >= 90) { # ����

                                          $old_razmen_batle[$bot_battle_id] = $bot_battle_id;

                                          $bot_attacking['invis'] = $bot_user_attacking['invis'] ? 1 : 0;

                                          $battle_bot->spare_no($bot_attacking);  # ��� �� �����
                                          if ($battle_bot->battle_end($bot_battle_id)) {
                                            $battle_bot->write_log();   # ����� ���
                                            break;
                                          }

                                       } else {

                                         $bot_defender = unset_bot(sql_row("SELECT * FROM `users` WHERE `id` = '".$enemy_id."' LIMIT 1;"));
                                         $bot_defender['bot_id'] = $enemy_id;
                                         $bot_user_defender = $bot_defender;
                                         $bot_user_defender['bot_id'] = $enemy_id;

                                         $battle_bot->razmen_init($enemy_id);
                                         $old_razmen_batle[$bot_battle_id] = $bot_battle_id;

                                       # ���������
                                         $ctim = time();

                                         if ($itout < ($data_battle['timeout'] * 60)) {
                                           $ctim -= $itout;
                                         }

                                         if($data_battle['timeout_next'] == 0 || $data_battle['timeout_next'] <= time()) {

                                            sql_query("UPDATE `battle` SET `timeout` = `timeout` - 1, `timeout_next` = '".(time() + ($data_battle['timeout'] * 60))."', `to1` = '".$ctim."', `to2` = '".$ctim."' WHERE `id` = '".$battle_bot->battle_data['id']."' LIMIT 1;");
                                         } else {
                                            sql_query("UPDATE `battle` SET `to1` = '".$ctim."', `to2` = '".$ctim."' WHERE `id` = '".$battle_bot->battle_data['id']."' LIMIT 1;");

                                            $data_battles[$bot_battle_id]['to1']  = $ctim;
                                            $data_battles[$bot_battle_id]['to2']  = $ctim;
                                            $data_battles[$bot_battle_id]['date'] = date("Y-m-d H:i:s");

                                         }

                                         if ($battle_bot->battle_end($bot_battle_id)) {
                                            $battle_bot->write_log();   # ����� ���
                                            break;
                                         }

                                       }

                                   }

                               }

                         }




                      }


    			  } else {
    				  # ������� ����...
    	          }
    		   }
       }
   }
 }

//include("a_process_end.php");

 # �������� ���� ������
   sql_lock_tables(false);

?>