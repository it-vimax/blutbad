<?


/***------------------------------------------
 * ����������
 **/

 $achievements = array(

          # ----------------------------
          # ���
          # ----------------------------

                       # ----------------------------
                       # �������
                         'winner_10' => array(

                                              # ��������
                                                'name' => '�������',

                                              # ��������
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',

                                              # ����
                                                'goals' => array(                         # ������� ������ �� ��������
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 10, 'listgoals' => array()),
                                                                ),

                                              # �������
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),

                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",

                                              # �������� ��� �������� ����������� ����������
                                                'done_achiev' => "",
                                                'next_achiev' => "winner_50"

                                              ),
                       # ----------------------------
                       # ������
                         'winner_50' => array(
                                                'name' => '������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 50, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_10",
                                                'next_achiev' => "winner_100"
                                              ),
                       # ----------------------------
                       # �������
                         'winner_100' => array(
                                                'name' => '�������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '25', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_50",
                                                'next_achiev' => "winner_500"
                                              ),
                       # ----------------------------
                       # ������
                         'winner_500' => array(
                                                'name' => '������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_100",
                                                'next_achiev' => "winner_1000"
                                              ),
                       # ----------------------------
                       # ������
                         'winner_1000' => array(
                                                'name' => '������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_500",
                                                'next_achiev' => "winner_2500"
                                              ),
                       # ----------------------------
                       # ���������
                         'winner_2500' => array(
                                                'name' => '���������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 2500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '150', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_1000",
                                                'next_achiev' => "winner_5000"
                                              ),
                       # ----------------------------
                       # ������
                         'winner_5000' => array(
                                                'name' => '������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 5000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_2500",
                                                'next_achiev' => "winner_10000"
                                              ),
                       # ----------------------------
                       # �������
                         'winner_10000' => array(
                                                'name' => '�������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 10000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_5000",
                                                'next_achiev' => "winner_25000"
                                              ),
                       # ----------------------------
                       # �����
                         'winner_25000' => array(
                                                'name' => '�����',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 25000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '425', 'description' => '����� ����������',  'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������� �������"',    'description' => '', 'ex' => 1, 'code' => 'item'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_10000",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ������ �� ������ - ������ �������
                         'fight_1' => array(
                                                'name' => '������ �� ������ - ������ �������',
                                                'description' => '������� ������ � ������ �������!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "������ �������"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_1",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ������ �� ������ - ������ ������������
                         'fight_2' => array(
                                                'name' => '������ �� ������ - ������ ������������',
                                                'description' => '������� ������ � ������ ������������!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "������ ������������"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_2",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ������ �� ������ - ��������� �����
                         'fight_3' => array(
                                                'name' => '������ �� ������ - ��������� �����',
                                                'description' => '������� ������ � ��������� �����!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "��������� �����"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '25', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_3",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ������ �� ������ - �������������� ��������
                         'fight_4' => array(
                                                'name' => '������ �� ������ - �������������� ��������',
                                                'description' => '������� ������ � �������������� ��������!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "�������������� ��������"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_4",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ������ �� ������ - ������������ ��������
                         'fight_5' => array(
                                                'name' => '������ �� ������ - ������������ ��������',
                                                'description' => '������� ������ � ������������ ��������!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "������������ ��������"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_5",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������� ����
                         'fight_6' => array(
                                                'name' => '����������� ����',
                                                'description' => '������� ������ �� ���� ��������� ��������� ����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ����������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������ �� ������ - ������ �������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '������ �� ������ - ������ ������������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ �� ������ - ��������� �����', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '������ �� ������ - �������������� ��������', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '������ �� ������ - ������������ ��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '1000', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '����� "����������� ����"', 'description' => '', 'ex' => 0, 'code' => 'obraz '),
                                                                 ),
                                                'code_groupe' => "statused_6",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # �������
          # ----------------------------

                       # ----------------------------
                       # ������
                         'tavern_food' => array(
                                                'name' => '������',
                                                'description' => '���������� �� ���� "���" � ������� ������������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '��������� � ��������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� � ��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "affects_1",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������
                         'tavern_nonalc' => array(
                                                'name' => '����������',
                                                'description' => '���������� �� ���� "��������������� �������" � ������� ������������� ����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�������� ���������������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '�������� ����������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ������������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '�������� ������', 'done' => 0)
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "affects_2",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # �������� ����
                         'tavern_alc' => array(
                                                'name' => '�������� ����',
                                                'description' => '���������� �� ���� "��������" � ������� ������������� �������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '���', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����� ����', 'done' => 0)
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "affects_3",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # �������� ���
                         'tavern_fish' => array(
                                                'name' => '�������� ���',
                                                'description' => '������� ��������� ���� ��� � ����� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '��������� ���', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '�������� ���', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '���������� ���', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������� ���', 'done' => 0),
                                                                                                                                                                                6 => array('name' => '���������� ���', 'done' => 0),
                                                                                                                                                                                7 => array('name' => '���������� ���', 'done' => 0),
                                                                                                                                                                                8 => array('name' => '������� ���', 'done' => 0),
                                                                                                                                                                                9 => array('name' => '��� ��-�������', 'done' => 0),
                                                                                                                                                                                10 => array('name' => '��� ��-���������', 'done' => 0),
                                                                                                                                                                                11 => array('name' => '��� ��-����������', 'done' => 0),
                                                                                                                                                                                12 => array('name' => '��� ��-����������', 'done' => 0),
                                                                                                                                                                                13 => array('name' => '����������� ���', 'done' => 0),
                                                                                                                                                                                14 => array('name' => '����-���', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '25', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "affects_4",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ������� ������
                         'tavern_all' => array(
                                                'name' => '������� ������',
                                                'description' => '�������� ��������� ����������, ��������� � ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ����', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '�������� ���', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 20% ��� ������ ����� ���� � �������������� �������� � ��������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "affects_5",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # �����������
          # ----------------------------

                       # ----------------------------
                       # ����������� � ������
                         'city_damask' => array(
                                                'name' => '����������� � ������',
                                                'description' => '��������� ��������������������� ������ ������',
                                                'goals' => array(
                                                                 1 => array('description' => '����������� ����� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����������� �������', 'done' => 0),          # 120
                                                                                                                                                                                2 => array('name' => '����� �������� ����', 'done' => 0),          # 150
                                                                                                                                                                                3 => array('name' => '�������� �����', 'done' => 0),               # 130
                                                                                                                                                                                4 => array('name' => '��������� �����', 'done' => 0),              # 140
                                                                                                                                                                                5 => array('name' => '������', 'done' => 0),                       # 100
                                                                                                                                                                                6 => array('name' => '����������� ���', 'done' => 0),              # 101
                                                                                                                                                                                7 => array('name' => '������� "����������� ������"', 'done' => 0), # 126
                                                                                                                                                                                8 => array('name' => '��������� ����', 'done' => 0),               # 152
                                                                                                                                                                                9 => array('name' => '������', 'done' => 0)                        # 136
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "loc_visit_1",
                                                'code'        => "loc_visit",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������� � ������
                         'city_alamut' => array(
                                                'name' => '����������� � ������',
                                                'description' => '��������� ��������������������� ������ ������',
                                                'goals' => array(
                                                                 1 => array('description' => '����������� ����� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����������� �������', 'done' => 0),   # 120
                                                                                                                                                                                2 => array('name' => '�������� �������', 'done' => 0),      # 150
                                                                                                                                                                                3 => array('name' => '������ �������� �����', 'done' => 0), # 130
                                                                                                                                                                                4 => array('name' => '�������� ����', 'done' => 0),         # 140
                                                                                                                                                                                5 => array('name' => '�����', 'done' => 0),                 # 100
                                                                                                                                                                                6 => array('name' => '����������� ���', 'done' => 0),       # 101
                                                                                                                                                                                7 => array('name' => '������� "������� ����"', 'done' => 0),# 126
                                                                                                                                                                                8 => array('name' => '��������� ����', 'done' => 0),        # 152
                                                                                                                                                                                9 => array('name' => '������', 'done' => 0)                 # 156
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "loc_visit_2",
                                                'code'        => "loc_visit",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # ���������
          # ----------------------------

                       # ----------------------------
                       # ��������� �������
                         'naemnik_10' => array(
                                                'name' => '��������� �������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 10, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "",
                                                'next_achiev' => "naemnik_50"
                                              ),
                       # ----------------------------
                       # ������ ������
                         'naemnik_50' => array(
                                                'name' => '������ ������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 50, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "naemnik_10",
                                                'next_achiev' => "naemnik_100"
                                              ),
                       # ----------------------------
                       # �����������
                         'naemnik_100' => array(
                                                'name' => '�����������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "naemnik_50",
                                                'next_achiev' => "naemnik_250"
                                              ),
                       # ----------------------------
                       # ������
                         'naemnik_250' => array(
                                                'name' => '������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "naemnik_100",
                                                'next_achiev' => "naemnik_500"
                                              ),

                       # ----------------------------
                       # ����������
                         'naemnik_500' => array(
                                                'name' => '����������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% ��� ������� ��� "�������������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "naemnik_250",
                                                'next_achiev' => ""
                                              ),



                       # ----------------------------
                       # �����������
                         'mentor_1' => array(
                                                'name' => '�����������',
                                                'description' => '�������� �� 5-��� ������ ������ �������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "",
                                                'next_achiev' => "mentor_5"
                                              ),
                       # ----------------------------
                       # ���������
                         'mentor_5' => array(
                                                'name' => '���������',
                                                'description' => '�������� �� 5-��� ������ ���� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "mentor_1",
                                                'next_achiev' => "mentor_15"
                                              ),
                       # ----------------------------
                       # ��������� ���������
                         'mentor_15' => array(
                                                'name' => '��������� ���������',
                                                'description' => '�������� �� 5-��� ������  ���������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "mentor_5",
                                                'next_achiev' => "mentor_30"
                                              ),
                       # ----------------------------
                       # ������� �������
                         'mentor_30' => array(
                                                'name' => '������� �������',
                                                'description' => '�������� �� 5-��� ������ �������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "mentor_15",
                                                'next_achiev' => "mentor_50"
                                              ),
                       # ----------------------------
                       # ����
                         'mentor_50' => array(
                                                'name' => '����',
                                                'description' => '�������� �� 5-��� ������ ��������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 50, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"���������� ���������� ���������� ����������� ����� �� ������ �������� �� 50%"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "mentor_30",
                                                'next_achiev' => ""
                                              ),


          # ----------------------------
          # �������
          # ----------------------------

                       # ----------------------------
                       # �������
                         'waterman_3' => array(
                                                'name' => '�������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "",
                                                'next_achiev' => "waterman_15"
                                              ),
                       # ----------------------------
                       # ���������� �������
                         'waterman_15' => array(
                                                'name' => '���������� �������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '30', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_3",
                                                'next_achiev' => "waterman_45"
                                              ),
                       # ----------------------------
                       # �����
                         'waterman_45' => array(
                                                'name' => '�����',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_15",
                                                'next_achiev' => "waterman_90"
                                              ),
                       # ----------------------------
                       # ��������
                         'waterman_90' => array(
                                                'name' => '��������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_45",
                                                'next_achiev' => "waterman_180"
                                              ),
                       # ----------------------------
                       # ��������
                         'waterman_180' => array(
                                                'name'        => '��������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 180, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_90",
                                                'next_achiev' => "waterman_270"
                                              ),
                       # ----------------------------
                       # ���������
                         'waterman_270' => array(
                                                'name' => '���������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_90",
                                                'next_achiev' => "waterman_360"
                                              ),


                       # ----------------------------
                       # �����
                         'waterman_360' => array(
                                                'name' => '�����',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 360, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"�������������� ����������� "������ �������� ���������" � ���� � ��������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_270",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # ����������
          # ----------------------------

                       # ----------------------------
                       # ������ ����
                         'dark_f_3' => array(
                                                'name' => '������ ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "",
                                                'next_achiev' => "dark_f_15"
                                              ),
                       # ----------------------------
                       # ������� ����
                         'dark_f_15' => array(
                                                'name' => '������� ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "dark_f_3",
                                                'next_achiev' => "dark_f_45"
                                              ),
                       # ----------------------------
                       # ��������� ����
                         'dark_f_45' => array(
                                                'name' => '��������� ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "dark_f_15",
                                                'next_achiev' => "dark_f_90"
                                              ),
                       # ----------------------------
                       # ������� ����
                         'dark_f_90' => array(
                                                'name' => '������� ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "dark_f_45",
                                                'next_achiev' => "dark_f_270"
                                              ),
                       # ----------------------------
                       # ������� ����
                         'dark_f_270' => array(
                                                'name' => '������� ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"���������� ������� ������� ����� ������ ����������� ���������� "����" ��� ����������� �� ������������� ����������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "dark_f_90",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ����������� ����
                         'light_f_3' => array(
                                                'name' => '����������� ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "",
                                                'next_achiev' => "light_f_15"
                                              ),
                       # ----------------------------
                       # ������������ ����
                         'light_f_15' => array(
                                                'name' => '������������ ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "light_f_3",
                                                'next_achiev' => "light_f_45"
                                              ),
                       # ----------------------------
                       # �������� ����
                         'light_f_45' => array(
                                                'name' => '�������� ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "light_f_15",
                                                'next_achiev' => "light_f_90"
                                              ),
                       # ----------------------------
                       # ������������ ����
                         'light_f_90' => array(
                                                'name' => '������������ ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "light_f_45",
                                                'next_achiev' => "light_f_270"
                                              ),
                       # ----------------------------
                       # ���������� ����
                         'light_f_270' => array(
                                                'name' => '���������� ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '',    'description' => '"���������� ������� ������� ����� ������ ����������� ���������� "����" ��� ����������� �� ������������� ����������"', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "light_f_90",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ������� ����
                         'chaos_f_3' => array(
                                                'name' => '������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "",
                                                'next_achiev' => "chaos_f_15"
                                              ),
                       # ----------------------------
                       # �������� ����
                         'chaos_f_15' => array(
                                                'name' => '�������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "chaos_f_3",
                                                'next_achiev' => "chaos_f_45"
                                              ),
                       # ----------------------------
                       # ����������� ����
                         'chaos_f_45' => array(
                                                'name' => '����������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "chaos_f_15",
                                                'next_achiev' => "chaos_f_90"
                                              ),
                       # ----------------------------
                       # �������� ����
                         'chaos_f_90' => array(
                                                'name' => '�������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "chaos_f_45",
                                                'next_achiev' => "chaos_f_270"
                                              ),
                       # ----------------------------
                       # �������� ����
                         'chaos_f_270' => array(
                                                'name' => '�������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '',    'description' => '"���������� ������� ������� ����� ������ ����������� ���������� "����" ��� ����������� �� ������������� ����������"', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "chaos_f_90",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ������� �������
                         'order_f_3' => array(
                                                'name' => '������� �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "",
                                                'next_achiev' => "order_f_15"
                                              ),
                       # ----------------------------
                       # �������� �������
                         'order_f_15' => array(
                                                'name' => '�������� �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "order_f_3",
                                                'next_achiev' => "order_f_45"
                                              ),
                       # ----------------------------
                       # ������ �������
                         'order_f_45' => array(
                                                'name' => '������ �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "order_f_15",
                                                'next_achiev' => "order_f_90"
                                              ),
                       # ----------------------------
                       # ������������� �������
                         'order_f_90' => array(
                                                'name' => '������������� �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "order_f_45",
                                                'next_achiev' => "order_f_270"
                                              ),
                       # ----------------------------
                       # �������������� �������
                         'order_f_270' => array(
                                                'name' => '�������������� �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"���������� ������� ������� ����� ������ ����������� ���������� "�������" ��� ����������� �� ������������� ����������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "order_f_90",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����
                         'dobla_1' => array(
                                                'name' => '����',
                                                'description' => '�������� ������ ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "",
                                                'next_achiev' => "dobla_2"
                                              ),
                       # ----------------------------
                       # ����
                         'dobla_2' => array(
                                                'name' => '����',
                                                'description' => '�������� ������ ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 2, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '25', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_1",
                                                'next_achiev' => "dobla_3"
                                              ),
                       # ----------------------------
                       # ��� ������
                         'dobla_3' => array(
                                                'name' => '��� ������',
                                                'description' => '�������� ������ ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '45', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_2",
                                                'next_achiev' => "dobla_4"
                                              ),
                       # ----------------------------
                       # ����
                         'dobla_4' => array(
                                                'name' => '����',
                                                'description' => '�������� ��������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 4, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '75', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_3",
                                                'next_achiev' => "dobla_5"
                                              ),
                       # ----------------------------
                       # �����
                         'dobla_5' => array(
                                                'name' => '�����',
                                                'description' => '�������� ����� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '110', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_4",
                                                'next_achiev' => "dobla_6"
                                              ),
                       # ----------------------------
                       # ������
                         'dobla_6' => array(
                                                'name' => '������',
                                                'description' => '�������� ������ ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 6, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '150', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_5",
                                                'next_achiev' => "dobla_7"
                                              ),
                       # ----------------------------
                       # ����
                         'dobla_7' => array(
                                                'name' => '����',
                                                'description' => '�������� ������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 7, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '225', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_6",
                                                'next_achiev' => "dobla_8"
                                              ),
                       # ----------------------------
                       # ������
                         'dobla_8' => array(
                                                'name' => '������',
                                                'description' => '�������� ������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 8, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_7",
                                                'next_achiev' => "dobla_9"
                                              ),
                       # ----------------------------
                       # ������
                         'dobla_9' => array(
                                                'name' => '������',
                                                'description' => '�������� ������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 9, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '450', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_8",
                                                'next_achiev' => "dobla_10"
                                              ),
                       # ----------------------------
                       # �����
                         'dobla_10' => array(
                                                'name' => '�����',
                                                'description' => '�������� ������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 10, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '450', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"��������� ����"', 'description' => '', 'ex' => 1, 'code' => 'affect'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_9",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ���������
                         'karma_1' => array(
                                                'name' => '���������',
                                                'description' => '�������� ������ ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "",
                                                'next_achiev' => "karma_2"
                                              ),
                       # ----------------------------
                       # ���
                         'karma_2' => array(
                                                'name' => '���',
                                                'description' => '�������� ������ ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 2, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '15', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_1",
                                                'next_achiev' => "karma_3"
                                              ),
                       # ----------------------------
                       # �������
                         'karma_3' => array(
                                                'name' => '�������',
                                                'description' => '�������� ������ ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '30', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_2",
                                                'next_achiev' => "karma_4"
                                              ),
                       # ----------------------------
                       # �����
                         'karma_4' => array(
                                                'name' => '�����',
                                                'description' => '�������� ��������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 4, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_3",
                                                'next_achiev' => "karma_5"
                                              ),
                       # ----------------------------
                       # ������������
                         'karma_5' => array(
                                                'name' => '������������',
                                                'description' => '�������� ����� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '75', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_4",
                                                'next_achiev' => "karma_6"
                                              ),
                       # ----------------------------
                       # ���������
                         'karma_6' => array(
                                                'name' => '���������',
                                                'description' => '�������� ������ ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 6, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_5",
                                                'next_achiev' => "karma_7"
                                              ),
                       # ----------------------------
                       # ����
                         'karma_7' => array(
                                                'name' => '����',
                                                'description' => '�������� ������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 7, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '150', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_6",
                                                'next_achiev' => "karma_8"
                                              ),
                       # ----------------------------
                       # ��������� ���
                         'karma_8' => array(
                                                'name' => '��������� ���',
                                                'description' => '�������� ������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 8, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_7",
                                                'next_achiev' => "karma_9"
                                              ),
                       # ----------------------------
                       # ������
                         'karma_9' => array(
                                                'name' => '������',
                                                'description' => '�������� ������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 9, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_8",
                                                'next_achiev' => "karma_10"
                                              ),
                       # ----------------------------
                       # �������� �����
                         'karma_10' => array(
                                                'name' => '�������� �����',
                                                'description' => '�������� ������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 10, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"100%-�� ������������ ����� ������ ����������� ����������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_9",
                                                'next_achiev' => ""
                                              ),


          # ----------------------------
          # ������ � �������
          # ----------------------------

                       # ----------------------------
                       # ���� ����� �����
                         'presents_20' => array(
                                                'name' => '���� ����� �����',
                                                'description' => '�������� ������� %s% ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������� %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 20, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "gift",
                                                'code'        => "gift",
                                                'done_achiev' => "",
                                                'next_achiev' => "presents_100"
                                              ),
                       # ----------------------------
                       # �������� ������ ����
                         'presents_100' => array(
                                                'name' => '�������� ������ ����',
                                                'description' => '�������� ������� %s% ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������� %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "gift",
                                                'code'        => "gift",
                                                'done_achiev' => "presents_20",
                                                'next_achiev' => "presents_500"
                                              ),
                       # ----------------------------
                       # ��� �����
                         'presents_500' => array(
                                                'name' => '��� �����',
                                                'description' => '�������� ������� %s% ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������� %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 20% ��� ������� ����� ��������� ���������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "gift",
                                                'code'        => "gift",
                                                'done_achiev' => "presents_100",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������
                         'friends_35' => array(
                                                'name' => '����������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 35, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "",
                                                'next_achiev' => "friends_50"
                                              ),
                       # ----------------------------
                       # ������
                         'friends_50' => array(
                                                'name' => '������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 50, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "friends_35",
                                                'next_achiev' => "friends_65"
                                              ),
                       # ----------------------------
                       # ��������
                         'friends_65' => array(
                                                'name' => '��������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 65, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "friends_50",
                                                'next_achiev' => "friends_80"
                                              ),
                       # ----------------------------
                       # ���� ��������
                         'friends_80' => array(
                                                'name' => '���� ��������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 80, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '150', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "friends_65",
                                                'next_achiev' => "friends_100"
                                              ),
                       # ----------------------------
                       # � ��������
                         'friends_100' => array(
                                                'name' => '� ��������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"����������� ���������� ������ ��������� �� ������� ������ �� 120"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "friends_80",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # ����
          # ----------------------------

                     # ----------------------------
                     # �����
                     # ----------------------------

                       # ----------------------------
                       # �������
                         'use_rune_hp_25' => array(
                                                'name' => '�������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 25, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_hp_500"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_hp_500' => array(
                                                'name' => '���������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_25",
                                                'next_achiev' => "use_rune_hp_1000"
                                              ),
                       # ----------------------------
                       # �����������
                         'use_rune_hp_1000' => array(
                                                'name' => '�����������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '20', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_500",
                                                'next_achiev' => "use_rune_hp_5000"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_hp_5000' => array(
                                                'name' => '�������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 5000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_1000",
                                                'next_achiev' => "use_rune_hp_10000"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_hp_10000' => array(
                                                'name' => '�������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 10000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_5000",
                                                'next_achiev' => "use_rune_hp_25000"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_hp_25000' => array(
                                                'name' => '�����',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 25000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_10000",
                                                'next_achiev' => "use_rune_hp_50000"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_hp_50000' => array(
                                                'name' => '�������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 50000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_25000",
                                                'next_achiev' => "use_rune_hp_100000"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_hp_100000' => array(
                                                'name' => '�����',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 100000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_50000",
                                                'next_achiev' => "use_rune_hp_250000"
                                              ),
                       # ----------------------------
                       # ��������� ����
                         'use_rune_hp_250000' => array(
                                                'name' => '��������� ����',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 250000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '600', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_100000",
                                                'next_achiev' => "use_rune_hp_500000"
                                              ),
                       # ----------------------------
                       # ������� �����������
                         'use_rune_hp_500000' => array(
                                                'name' => '������� �����������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 500000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '1000', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ��������� ���� �������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_250000",
                                                'next_achiev' => ""
                                              ),

                     # ----------------------------
                     # ������������
                     # ----------------------------


                       # ----------------------------
                       # ��������
                         'use_rune_pw_25' => array(
                                                'name' => '��������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 25, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_pw_500"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_pw_500' => array(
                                                'name' => '���������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_25",
                                                'next_achiev' => "use_rune_pw_1000"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_pw_1000' => array(
                                                'name' => '��������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '20', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_500",
                                                'next_achiev' => "use_rune_pw_5000"
                                              ),
                       # ----------------------------
                       # ���� ������
                         'use_rune_pw_5000' => array(
                                                'name' => '���� ������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 5000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_1000",
                                                'next_achiev' => "use_rune_pw_10000"
                                              ),
                       # ----------------------------
                       # ��� � ��
                         'use_rune_pw_10000' => array(
                                                'name' => '��� � ��',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 10000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_5000",
                                                'next_achiev' => "use_rune_pw_25000"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_pw_25000' => array(
                                                'name' => '��������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 25000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_10000",
                                                'next_achiev' => "use_rune_pw_50000"
                                              ),
                       # ----------------------------
                       # ���� ���������
                         'use_rune_pw_50000' => array(
                                                'name' => '���� ���������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 50000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_25000",
                                                'next_achiev' => "use_rune_pw_100000"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_pw_100000' => array(
                                                'name' => '������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 100000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_50000",
                                                'next_achiev' => "use_rune_pw_250000"
                                              ),
                       # ----------------------------
                       # ������� �����
                         'use_rune_pw_250000' => array(
                                                'name' => '������� �����',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 250000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '600', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_100000",
                                                'next_achiev' => "use_rune_pw_500000"
                                              ),
                       # ----------------------------
                       # �������������
                         'use_rune_pw_500000' => array(
                                                'name' => '�������������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 500000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '1000', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ��������� ���� ��������������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_250000",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_zah_1' => array(
                                                'name' => '��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_zah_5"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_zah_5' => array(
                                                'name' => '�������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_1",
                                                'next_achiev' => "use_rune_zah_30"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_zah_30' => array(
                                                'name' => '������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_5",
                                                'next_achiev' => "use_rune_zah_100"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_zah_100' => array(
                                                'name' => '�����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_30",
                                                'next_achiev' => "use_rune_zah_250"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_zah_250' => array(
                                                'name' => '�������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_100",
                                                'next_achiev' => "use_rune_zah_500"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_zah_500' => array(
                                                'name' => '�����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_250",
                                                'next_achiev' => "use_rune_zah_1000"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_zah_1000' => array(
                                                'name' => '������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1000 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_500",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # �����������
                         'use_rune_opl_1' => array(
                                                'name' => '�����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_opl_5"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_opl_5' => array(
                                                'name' => '���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '20', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_1",
                                                'next_achiev' => "use_rune_opl_30"
                                              ),
                       # ----------------------------
                       # �����������
                         'use_rune_opl_30' => array(
                                                'name' => '�����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_5",
                                                'next_achiev' => "use_rune_opl_100"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_opl_100' => array(
                                                'name' => '��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_30",
                                                'next_achiev' => "use_rune_opl_250"
                                              ),
                       # ----------------------------
                       # �������� �����
                         'use_rune_opl_250' => array(
                                                'name' => '�������� �����',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_100",
                                                'next_achiev' => "use_rune_opl_500"
                                              ),
                       # ----------------------------
                       # ����������
                         'use_rune_opl_500' => array(
                                                'name' => '����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_250",
                                                'next_achiev' => "use_rune_opl_1000"
                                              ),
                       # ----------------------------
                       # ������� � �����
                         'use_rune_opl_1000' => array(
                                                'name' => '������� � �����',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 1000 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '600', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "�����""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_500",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_tak_1' => array(
                                                'name' => '�����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_tak_5"
                                              ),
                       # ----------------------------
                       # ����������
                         'use_rune_tak_5' => array(
                                                'name' => '����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '20', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_1",
                                                'next_achiev' => "use_rune_tak_30"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_tak_30' => array(
                                                'name' => '���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_5",
                                                'next_achiev' => "use_rune_tak_100"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_tak_100' => array(
                                                'name' => '�����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_30",
                                                'next_achiev' => "use_rune_tak_250"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_tak_250' => array(
                                                'name' => '������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_100",
                                                'next_achiev' => "use_rune_tak_500"
                                              ),
                       # ----------------------------
                       # ����������
                         'use_rune_tak_500' => array(
                                                'name' => '����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_250",
                                                'next_achiev' => "use_rune_tak_1000"
                                              ),
                       # ----------------------------
                       # ��������� �����
                         'use_rune_tak_1000' => array(
                                                'name' => '��������� �����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1000 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '600', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_500",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������
                         'use_rune_pok_1' => array(
                                                'name' => '����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_pok_5"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_pok_5' => array(
                                                'name' => '��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_1",
                                                'next_achiev' => "use_rune_pok_30"
                                              ),
                       # ----------------------------
                       # �����������
                         'use_rune_pok_30' => array(
                                                'name' => '�����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_5",
                                                'next_achiev' => "use_rune_pok_100"
                                              ),
                       # ----------------------------
                       # ��������� �����
                         'use_rune_pok_100' => array(
                                                'name' => '��������� �����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_30",
                                                'next_achiev' => "use_rune_pok_250"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_pok_250' => array(
                                                'name' => '�������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_100",
                                                'next_achiev' => "use_rune_pok_500"
                                              ),
                       # ----------------------------
                       # ������ ���
                         'use_rune_pok_500' => array(
                                                'name' => '������ ���',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_250",
                                                'next_achiev' => "use_rune_pok_1000"
                                              ),
                       # ----------------------------
                       # �������-���������
                         'use_rune_pok_1000' => array(
                                                'name' => '�������-���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_250",
                                                'next_achiev' => "use_rune_pok_1000"
                                              ),


                       # ----------------------------
                       # ���������
                         'use_rune_vozd_1' => array(
                                                'name' => '���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_vozd_5"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_vozd_5' => array(
                                                'name' => '���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_1",
                                                'next_achiev' => "use_rune_vozd_30"
                                              ),
                       # ----------------------------
                       # ������ ����
                         'use_rune_vozd_30' => array(
                                                'name' => '������ ����',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '30', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_5",
                                                'next_achiev' => "use_rune_vozd_100"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_vozd_100' => array(
                                                'name' => '��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_30",
                                                'next_achiev' => "use_rune_vozd_250"
                                              ),
                       # ----------------------------
                       # ���������� ��������
                         'use_rune_vozd_250' => array(
                                                'name' => '���������� ��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_100",
                                                'next_achiev' => "use_rune_vozd_500"
                                              ),
                       # ----------------------------
                       # ������������ ������
                         'use_rune_vozd_500' => array(
                                                'name' => '������������ ������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_250",
                                                'next_achiev' => "use_rune_vozd_1000"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_vozd_1000' => array(
                                                'name' => '������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "���������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_250",
                                                'next_achiev' => ""
                                              ),




          # ----------------------------
          # �����
          # ----------------------------

                       # ----------------------------
                       # ���� �����
                         'klan_1' => array(
                                                'name' => '���� �����',
                                                'description' => '������� ������ ����� %s% ���',
                                                'goals' => array(
                                                                 1 => array('description' => '����� ������ ����� %s/s% ���',  'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "klan",
                                                'code'        => "klan",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),







          # ----------------------------
          # Test
          # ----------------------------

                       # ----------------------------
                       # test
                         'test_1' => array(
                                                'name' => 'test',
                                                'description' => 'Test ������ ������ � ������ ���',
                                                'goals' => array(
                                                                 1 => array('description' => 'Test ������ %s/s% ���',  'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array()),
                                                                 2 => array('description' => 'Test ������:',  'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������ ����', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '������ ����', 'done' => 0)
                                                                                                                                                                               )),
                                                                 3 => array('description' => 'Test ������ ���� ���',  'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                 4 => array('description' => '������� ����������� ����� ���� �� ������ "������" 1 ���',  'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "tests",
                                                'code'        => "tests",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),





 );


/***------------------------------------------
 * ������ ��������� ����������
 **/

                   $groop_finish_achievements = array('fight_6', 'winner_25000', 'tavern_all', 'naemnik_500', 'waterman_360', 'dark_f_270', 'light_f_270', 'order_f_270', 'chaos_f_270',
                                                      'karma_10', 'dobla_10', 'presents_500', 'use_rune_hp_500000', 'use_rune_pw_500000', 'use_rune_zah_1000',
                                                      'use_rune_opl_1000', 'use_rune_tak_1000', 'use_rune_pok_1000', 'use_rune_vozd_1000'
                                                      );

/***------------------------------------------
 * ������ ����������
 **/

                   $groops_achievements = array(
                     'fight'    => array('name' => '���', 'list' => array('fight_1', 'fight_2', 'fight_3', 'fight_4', 'fight_5', 'fight_6', 'winner_25000', 'winner_10000', 'winner_5000', 'winner_2500', 'winner_1000', 'winner_500', 'winner_100', 'winner_50', 'winner_10')),
                     'tavern'   => array('name' => '�������', 'list' => array('tavern_food', 'tavern_nonalc', 'tavern_alc', 'tavern_fish', 'tavern_all')),
                     'travel'   => array('name' => '�����������', 'list' => array('city_damask', 'city_alamut')),

                     'jobs'     => array('name' => '���������', 'list' => array('naemnik_500', 'naemnik_250', 'naemnik_100', 'naemnik_50', 'naemnik_10', 'mentor_50', 'mentor_30', 'mentor_15', 'mentor_5', 'mentor_1')),
                     'fishing'  => array('name' => '�������', 'list' => array('waterman_360', 'waterman_270', 'waterman_180', 'waterman_90', 'waterman_45', 'waterman_15', 'waterman_3')),

                     'align'    => array('name' => '����������', 'list' => array(
                                                                                  'dark_f_270', 'dark_f_90', 'dark_f_45', 'dark_f_15', 'dark_f_3',
                                                                                  'light_f_270', 'light_f_90', 'light_f_45', 'light_f_15', 'light_f_3',
                                                                                  'order_f_270', 'order_f_90', 'order_f_45', 'order_f_15', 'order_f_3',
                                                                                  'chaos_f_270', 'chaos_f_90', 'chaos_f_45', 'chaos_f_15', 'chaos_f_3',
                                                                                  'karma_1', 'karma_2', 'karma_3', 'karma_4', 'karma_5', 'karma_6', 'karma_7', 'karma_8', 'karma_9', 'karma_10',
                                                                                  'dobla_1', 'dobla_2', 'dobla_3', 'dobla_4', 'dobla_5', 'dobla_6', 'dobla_7', 'dobla_8', 'dobla_9', 'dobla_10'
                                                                                  )),
                     'social'   => array('name' => '������ � �������', 'list' => array('presents_500', 'presents_100', 'presents_20', 'friends_100', 'friends_80', 'friends_65', 'friends_50', 'friends_35')),
                     'runs'     => array('name' => '����', 'list' => array(
                                                                            'use_rune_hp_500000', 'use_rune_hp_250000', 'use_rune_hp_100000', 'use_rune_hp_50000', 'use_rune_hp_25000', 'use_rune_hp_10000', 'use_rune_hp_5000', 'use_rune_hp_1000', 'use_rune_hp_500', 'use_rune_hp_25',
                                                                            'use_rune_pw_500000', 'use_rune_pw_250000', 'use_rune_pw_100000', 'use_rune_pw_50000', 'use_rune_pw_25000', 'use_rune_pw_10000', 'use_rune_pw_5000', 'use_rune_pw_1000', 'use_rune_pw_500', 'use_rune_pw_25',
                                                                            'use_rune_zah_1', 'use_rune_zah_5', 'use_rune_zah_30', 'use_rune_zah_100', 'use_rune_zah_250', 'use_rune_zah_500', 'use_rune_zah_1000',
                                                                            'use_rune_opl_1', 'use_rune_opl_5', 'use_rune_opl_30', 'use_rune_opl_100', 'use_rune_opl_250', 'use_rune_opl_500', 'use_rune_opl_1000',
                                                                            'use_rune_tak_1', 'use_rune_tak_5', 'use_rune_tak_30', 'use_rune_tak_100', 'use_rune_tak_250', 'use_rune_tak_500', 'use_rune_tak_1000',
                                                                            'use_rune_pok_1', 'use_rune_pok_5', 'use_rune_pok_30', 'use_rune_pok_100', 'use_rune_pok_250', 'use_rune_pok_500', 'use_rune_pok_1000',
                                                                            'use_rune_vozd_1', 'use_rune_vozd_5', 'use_rune_vozd_30', 'use_rune_vozd_100', 'use_rune_vozd_250', 'use_rune_vozd_500', 'use_rune_vozd_1000'
                                                                            )),
                     'klans'    => array('name' => '�����', 'list' => array('klan_1')),
                     'quests'   => array('name' => '����������� � ������������', 'list' => array()),
                     'games'    => array('name' => '�������� ����', 'list' => array()),
                     'bonus'    => array('name' => '������� � ������', 'list' => array()),
                     'events'   => array('name' => '������� � ���������', 'list' => array()),
                    // 'tests'    => array('name' => '����', 'list' => array('test_1')),
                   );

/***------------------------------------------
 * ������ ��������� ����������
 **/

                   $groops_cur_achievements = array(
                     'winner_total'   => array('winner_10', 'winner_50', 'winner_100', 'winner_500', 'winner_1000', 'winner_2500', 'winner_5000', 'winner_10000', 'winner_25000'),

                     'statused_1'       => array('fight_1'),
                     'statused_2'       => array('fight_2'),
                     'statused_3'       => array('fight_3'),
                     'statused_4'       => array('fight_4'),
                     'statused_5'       => array('fight_5'),
                     'statused_6'       => array('fight_6'),

                     'affects_1'        => array('tavern_food'),
                     'affects_2'        => array('tavern_nonalc'),
                     'affects_3'        => array('tavern_alc'),
                     'affects_4'        => array('tavern_fish'),
                     'affects_5'        => array('tavern_all'),

                     'loc_visit_1'      => array('city_damask'),
                     'loc_visit_2'      => array('city_alamut'),

                     'karma_level'      => array('karma_1', 'karma_2', 'karma_3', 'karma_4', 'karma_5', 'karma_6', 'karma_7', 'karma_8', 'karma_9', 'karma_10'),
                     'fame_level'       => array('dobla_1', 'dobla_2', 'dobla_3', 'dobla_4', 'dobla_5', 'dobla_6', 'dobla_7', 'dobla_8', 'dobla_9', 'dobla_10'),

                     'naemnik'          => array('naemnik_10', 'naemnik_50', 'naemnik_100', 'naemnik_250', 'naemnik_500'),
                     'mentor'           => array('mentor_1', 'mentor_5', 'mentor_15', 'mentor_30', 'mentor_50'),
                     'waterman'         => array('waterman_3', 'waterman_15', 'waterman_45', 'waterman_90', 'waterman_180', 'waterman_270', 'waterman_360'),

                     'align_vamp2'      => array('dark_f_3', 'dark_f_15', 'dark_f_45', 'dark_f_90', 'dark_f_270'),
                     'align_sacr'       => array('light_f_3', 'light_f_15', 'light_f_45', 'light_f_90', 'light_f_270'),
                     'align_ch_side'    => array('chaos_f_3', 'chaos_f_15', 'chaos_f_45', 'chaos_f_90', 'chaos_f_270'),
                     'align_subm'       => array('order_f_3', 'order_f_15', 'order_f_45', 'order_f_90', 'order_f_270'),

                     'friends'          => array('friends_35', 'friends_50', 'friends_65', 'friends_80', 'friends_100'),
                     'gift'             => array('presents_20', 'presents_100', 'presents_500'),
                     'use_rune_hp'      => array('use_rune_hp_25', 'use_rune_hp_500', 'use_rune_hp_1000', 'use_rune_hp_5000', 'use_rune_hp_10000', 'use_rune_hp_25000', 'use_rune_hp_50000', 'use_rune_hp_100000', 'use_rune_hp_250000', 'use_rune_hp_500000'),
                     'use_rune_pw'      => array('use_rune_pw_25', 'use_rune_pw_500', 'use_rune_pw_1000', 'use_rune_pw_5000', 'use_rune_pw_10000', 'use_rune_pw_25000', 'use_rune_pw_50000', 'use_rune_pw_100000', 'use_rune_pw_250000', 'use_rune_pw_500000'),

                     'use_rune_list_1'  => array('use_rune_zah_1', 'use_rune_zah_5', 'use_rune_zah_30', 'use_rune_zah_100', 'use_rune_zah_250', 'use_rune_zah_500', 'use_rune_zah_1000'),
                     'use_rune_list_2'  => array('use_rune_opl_1', 'use_rune_opl_5', 'use_rune_opl_30', 'use_rune_opl_100', 'use_rune_opl_250', 'use_rune_opl_500', 'use_rune_opl_1000'),
                     'use_rune_list_3'  => array('use_rune_tak_1', 'use_rune_tak_5', 'use_rune_tak_30', 'use_rune_tak_100', 'use_rune_tak_250', 'use_rune_tak_500', 'use_rune_tak_1000'),
                     'use_rune_list_4'  => array('use_rune_pok_1', 'use_rune_pok_5', 'use_rune_pok_30', 'use_rune_pok_100', 'use_rune_pok_250', 'use_rune_pok_500', 'use_rune_pok_1000'),
                     'use_rune_list_5'  => array('use_rune_vozd_1', 'use_rune_vozd_5', 'use_rune_vozd_30', 'use_rune_vozd_100', 'use_rune_vozd_250', 'use_rune_vozd_500', 'use_rune_vozd_1000'),

                   //  'tests'          => array('test_1'),
                   );

/***------------------------------------------
 * ������ ��������� ����������
 **/

                   $groops_code_achievements = array(
                     'winner_total'   => array('winner_10', 'winner_50', 'winner_100', 'winner_500', 'winner_1000', 'winner_2500', 'winner_5000', 'winner_10000', 'winner_25000'),

                     'statused'       => array('fight_1', 'fight_2', 'fight_3', 'fight_4', 'fight_5', 'fight_6'),
                     'affects'        => array('tavern_food', 'tavern_nonalc', 'tavern_alc', 'tavern_fish', 'tavern_all'),
                     'loc_visit'      => array('city_damask', 'city_alamut'),

                     'naemnik'        => array('naemnik_10', 'naemnik_50', 'naemnik_100', 'naemnik_250', 'naemnik_500'),
                     'mentor'         => array('mentor_1', 'mentor_5', 'mentor_15', 'mentor_30', 'mentor_50'),
                     'waterman'       => array('waterman_3', 'waterman_15', 'waterman_45', 'waterman_90', 'waterman_180', 'waterman_270', 'waterman_360'),

                     'align_vamp2'    => array('dark_f_3', 'dark_f_15', 'dark_f_45', 'dark_f_90', 'dark_f_270'),
                     'align_sacr'     => array('light_f_3', 'light_f_15', 'light_f_45', 'light_f_90', 'light_f_270'),
                     'align_ch_side'  => array('chaos_f_3', 'chaos_f_15', 'chaos_f_45', 'chaos_f_90', 'chaos_f_270'),
                     'align_subm'     => array('order_f_3', 'order_f_15', 'order_f_45', 'order_f_90', 'order_f_270'),

                     'friends'        => array('friends_35', 'friends_50', 'friends_65', 'friends_80', 'friends_100'),
                     'gift'           => array('presents_20', 'presents_100', 'presents_500'),
                     'use_rune_hp'    => array('use_rune_hp_25', 'use_rune_hp_500', 'use_rune_hp_1000', 'use_rune_hp_5000', 'use_rune_hp_10000', 'use_rune_hp_25000', 'use_rune_hp_50000', 'use_rune_hp_100000', 'use_rune_hp_250000', 'use_rune_hp_500000'),
                     'use_rune_pw'    => array('use_rune_pw_25', 'use_rune_pw_500', 'use_rune_pw_1000', 'use_rune_pw_5000', 'use_rune_pw_10000', 'use_rune_pw_25000', 'use_rune_pw_50000', 'use_rune_pw_100000', 'use_rune_pw_250000', 'use_rune_pw_500000'),
                     'use_rune_list'  => array('use_rune_zah_1', 'use_rune_zah_5', 'use_rune_zah_30', 'use_rune_zah_100', 'use_rune_zah_250', 'use_rune_zah_500', 'use_rune_zah_1000',
                                               'use_rune_opl_1', 'use_rune_opl_5', 'use_rune_opl_30', 'use_rune_opl_100', 'use_rune_opl_250', 'use_rune_opl_500', 'use_rune_opl_1000',
                                               'use_rune_tak_1', 'use_rune_tak_5', 'use_rune_tak_30', 'use_rune_tak_100', 'use_rune_tak_250', 'use_rune_tak_500', 'use_rune_tak_1000',
                                               'use_rune_pok_1', 'use_rune_pok_5', 'use_rune_pok_30', 'use_rune_pok_100', 'use_rune_pok_250', 'use_rune_pok_500', 'use_rune_pok_1000',
                                               'use_rune_vozd_1', 'use_rune_vozd_5', 'use_rune_vozd_30', 'use_rune_vozd_100', 'use_rune_vozd_250', 'use_rune_vozd_500', 'use_rune_vozd_1000'),

                     'klan'           => array('klan_1'),

                    // 'tests'          => array('test_1'),

                   );
/*
 +winner_total     =  fbattle.php
 +statused_1       =  fbattle.php
 +statused_2       =  fbattle.php
 +statused_3       =  fbattle.php
 +statused_4       =  fbattle.php
 +statused_6       =  fbattle.php

 +affects_1        =  taverna.php
 +affects_2        =  taverna.php
 +affects_3        =  taverna.php
 +affects_4        =  qwest/1260001/qwest_1/quest.perform.php
 +affects_5        =  affects_1, affects_2, affects_3, affects_4

 +loc_visit_1      =  map.php
 +loc_visit_2      =  map.php

 +naemnik          =  transfer.php
 +mentor           =  transfer.php
 +klan             =  transfer.php

 +waterman         =  fbattle.holiday.php
 +align_vamp2      =  vampirbatle.php
 +align_sacr       =  lightbatle.php
 +align_ch_side    =  flipsidebatle.php
 +align_subm       =  submission.php
 +friends          =  friends.php
 +gift             =  shop.php, post.php

 +use_rune_hp      =  magic - ����
 +use_rune_pw      =  magic - ����
 +use_rune_list_1  =  magic - ���� ������    rune_shop_attack.php, rune_shop_bloody_attack.php                               ===
 +use_rune_list_2  =  magic - ���� �����     rune_ex_intervention.php, rune_ex_user_clone.php, rune_ex_battle_shout.php      ===  "������������� (���.), ���� (���.), ������ ���� (���.)"
 +use_rune_list_3  =  magic - ���� ������    rune_ex_remove_rune.php, rune_ex_exit_battle.php, rune_ex_isolation_battle.php  ===  "����� ���� (���.), ����� �� ��� (���.), �������� ��� (���.)"
 +use_rune_list_4  =  magic - ���� ������    rune_shop_hidden.php,                                                           ===  "�����������, ����������� 15+15"
 +use_rune_list_5  =  magic - ���� ��������� rune_shop_blindness.php, rune_shop_chains.php, rune_shop_silence_30.php, "���������� (���.), ��������� (���.), �������, ����, ��������"

 */


/***------------------------------------------
 * ��������� ������� �� ��������� ����������
 **/

function get_achievements($user_achiev, $ach_groupe_name, $id_goal, $type){
 global $groops_cur_achievements;

  $users_achiev = sql_query("SELECT * FROM `users_achievements` WHERE `owner` = '".$user_achiev['id']."' AND `type_groupe` = '".$ach_groupe_name."' ;");
  while ($row = fetch_array($users_achiev)) {
    $groupe = unserialize($row['groupe']);
    foreach ($groupe as $k => $v) {
      $user_achievements[$k] = $v;
    }
  }

  $ach = 0;

  switch ($type) {
    case "val":

       foreach ($groops_cur_achievements[$ach_groupe_name] as $k => $v) {
         if (!empty($user_achievements[$v]) && $user_achievements[$v]['status'] == 3) {
          if (!empty($user_achievements[$v]['goals'][$id_goal]['val'])) {
            $ach = $user_achievements[$v]['goals'][$id_goal]['val'];
          }
         }
       }

    break;
    case "":

    break;

  }

 return $ach;

}

/***------------------------------------------
 * ��������� ������� �� ��������� ����������
 **/

function finish_achievements_award($user_achiev, $achiev_name, $code){

 switch ($achiev_name) {
   case "winner_25000": # "������� �������"
     //
   break;
   case "fight_6":      # ����� "����������� ����"
     //
   break;
   case "dobla_10":     # "��������� ����"
     //
   break;
 }

}


/***------------------------------------------
 * ���������� ����������
 **/

  function achievements($user_achiev, $achiev = array(), $ach_aim_list = array()) {
    global $user, $user_my, $achievements, $groops_cur_achievements;

    $finish_ach = array();
   # $user_achievements = unserialize($user_achiev['achievements']);
    $user_achievements_finish = $user_achiev['achievements_finish'];

    $user_ach_finish = explode(";", $user_achievements_finish);

     # $user_achievements
     # - status      # ������ ����������
     # - goals       # ����
     # - datefinish  # ���� ������ ����������

        # status = 0;  # �����
        # status = 1;  # ������
        # status = 2;  # �� ���������
        # status = 3;  # �������
        # status = 4;  # ���������


    /*$achiev_new = array();
    foreach ($achiev as $k => $v) {
       foreach ($groops_cur_achievements[$v] as $kd => $vd) {
        $achiev_new[] = $vd;
       }
    }*/

   // die(serialize($achiev_new));

    foreach ($achiev as $k_id => $v_arh) {

    $groupe_ach_goals = $v_arh;
    $groupe_ach_name  = $k_id;

     switch ($groupe_ach_name) {




    # ----------------------------
    # �������
    # ----------------------------
    # �����������
    # ----------------------------



    # ----------------------------

       case "use_rune_hp":        # ���������
       case "use_rune_pw":        # �������������

       case "waterman":           # �������


       case "affects_1":          #
       case "affects_2":          #
       case "affects_3":          #
       case "affects_4":          #
       case "affects_5":          #

       case "statused_1":         #
       case "statused_2":         #
       case "statused_3":         #
       case "statused_4":         #
       case "statused_5":         #
       case "statused_6":         #

       case "karma_level":        #
       case "fame_level":         #

       case "winner_total":       #
       case "naemnik":            #
       case "mentor":             #
       case "klan":               #
       case "friends":            #
       case "gift":               #
       case "align_vamp2":        #
       case "align_sacr":         #
       case "align_ch_side":      #
       case "align_subm":         #

       case "loc_visit_1":        #
       case "loc_visit_2":        #

       case "use_rune_list_1":    #
       case "use_rune_list_2":    #
       case "use_rune_list_3":    #
       case "use_rune_list_4":    #
       case "use_rune_list_5":    #


      // case "tests":              #

   # ----------------------------

          # ���� ������ ���������
            $user_ach = sql_row("SELECT * FROM `users_achievements` WHERE `owner` = '".$user_achiev['id']."' AND `type_groupe` = '".$groupe_ach_name."' LIMIT 1;");
          # ������� ��� ����������
            $type_curent      = "";
          # ������� ����� ����������
            $type_groupe      = "";
          # ������� ��� ����� ����������
            $type_code_groupe = "";


          # ��������� �� ������ � �������� �� �������
            ksort($groops_cur_achievements[$groupe_ach_name]);


            # -----------------------------------
            # ���� ������ ��������� ��� �������
              if ($user_ach['id']) {

              # ��������� ����� ��������� ����������
                $save_arh = 1;

              # ������� ����� ������ ���������� �� ����
                $user_ach_groupe = unserialize($user_ach['groupe']);

              } else {

              # ������� ����� ����������
                $save_arh = 0;

              # -----------------------------------
              # ������� ����� ������ ����������
                  foreach ($groops_cur_achievements[$groupe_ach_name] as $k_name => $v_arc) {

                   # ������� ������ ��������
                     foreach ($achievements[$v_arc]['goals'] as $k_un => $v_un) {
                      unset($achievements[$v_arc]['goals'][$k_un]['name']);
                      unset($achievements[$v_arc]['goals'][$k_un]['description']);
                      if (empty($achievements[$v_arc]['goals'][$k_un]['listgoals'])) {
                        unset($achievements[$v_arc]['goals'][$k_un]['listgoals']);
                      }
                     }

                     $user_ach_groupe[$v_arc]['goals'] = $achievements[$v_arc]['goals'];
                     if ($achievements[$v_arc]['next_achiev']) {
                      $user_ach_groupe[$v_arc]['next_achiev'] = $achievements[$v_arc]['next_achiev'];
                     }


                   # ������� ��������� ���������� ������ ������ ������� "3"
                     if ($groops_cur_achievements[$groupe_ach_name][0] == $v_arc) {
                        $user_ach_groupe[$v_arc]['status']  = 3;
                     } else {
                       if (empty($user_ach_groupe[$v_arc]['status'])) {
                        $user_ach_groupe[$v_arc]['status'] = 1;
                       }
                     }

                  }
              }

                # ---------------------------------------------------------
                # ������� ������ ���������� �� ������ "$groupe_ach_name"
                  foreach ($groops_cur_achievements[$groupe_ach_name] as $k => $achiev_name) {
                     //echo 'status '.$user_ach_groupe[$achiev_name]['status'].'<br>';
                    # ���� ������ ������� = "3"
                      if ($user_ach_groupe[$achiev_name]['status'] == 3) {

                      # ����� ���������
                        $goals_finish = 0;
                        $goals_c      = 0;

                      # ����� ����� ����������
                        $goals = $user_ach_groupe[$achiev_name]['goals'];

                         foreach ($goals as $k_gid => $v_goal) {

                          # ��������
                            if ($num_g = @$groupe_ach_goals[$k_gid]) {

                             /*

                              $v_goal['description']
                              $v_goal['name']
                              $v_goal['val']
                              $v_goal['valmax']
                              $v_goal['listgoals']

                              $groupe_ach_name  = $k_id;
                              $groupe_ach_goals = $v_arh;

                                $goals = array('test_1' => array(
                                  1 => 2,
                                  2 => array('������ ����'),
                                  3 => 1,
                                ));

                             */

                            # ���������� ���� �����
                              $goals_c = count($user_ach_groupe[$achiev_name]['goals']);

                              $next_achiev = @$user_ach_groupe[$achiev_name]['next_achiev'];

                            # -------------------------------------
                            # ���� ���� ������ �������� ��������
                            # -------------------------------------

                              if (is_numeric($num_g)) {

                               $old_val = $user_ach_groupe[$achiev_name]['goals'][$k_gid]['val'];
                               $user_ach_groupe[$achiev_name]['goals'][$k_gid]['val'] += $num_g;

                               $val         = $user_ach_groupe[$achiev_name]['goals'][$k_gid]['val'];
                               $valmax      = $user_ach_groupe[$achiev_name]['goals'][$k_gid]['valmax'];

                               if ($val >= $valmax) {
                                 $goals_finish += 1;

                                 if ($next_achiev) {
                                  $user_ach_groupe[$next_achiev]['goals'][$k_gid]['val'] = $old_val;
                                 }

                                 $user_ach_groupe[$achiev_name]['goals'][$k_gid]['val'] = $valmax;
                               }

                              } else {

                            # -------------------------------------
                            # ���� ���� ����� ��������
                            # -------------------------------------

                                # ������ ����� � ����������
                                  $val    = 0;
                                  $valmax = count($v_goal['listgoals']);
                                 // $next_achiev = @$user_ach_groupe[$achiev_name]['next_achiev'];

                                  foreach ($v_goal['listgoals'] as $lg_id => $v_lg) {
                                    if (in_array($v_lg['name'], $num_g)) {
                                     $user_ach_groupe[$achiev_name]['goals'][$k_gid]['listgoals'][$lg_id]['done'] = 1;
                                    }

                                    if ($user_ach_groupe[$achiev_name]['goals'][$k_gid]['listgoals'][$lg_id]['done']) {
                                     $val += 1;
                                    }
                                  }

                                  if ($val >= $valmax) { $goals_finish += 1; }

                              } # end if

                            } # end if

                         } # end foreach

                              # echo $achiev_name.' - '.$goals_finish.'/'.$goals_c.'<br>';

                              # ��������� ������ ����������
                                if ($goals_finish >= $goals_c) {

                                 # ���������� ������ ���������� ����������
                                   if (!in_array($achiev_name, $finish_ach) && $user_ach_groupe[$achiev_name]['status'] == 3 && !in_array($achiev_name, $user_ach_finish)) {
                                      $finish_ach[] = $achiev_name;
                                   }

                                   $user_ach_groupe[$achiev_name]['status']     = 4;
                                   $user_ach_groupe[$achiev_name]['datefinish'] = time();

                                   if ($next_achiev) {
                                    $user_ach_groupe[$next_achiev]['status']    = 3;
                                   }

                                }

                      } # end if

                  } # end foreach

                  $type_curent   = $achiev_name;
                  $type_groupe   = $groupe_ach_name;
                  $groupe_val    = 0;
                  $groupe_maxval = 0;

                /*echo "<pre>";
                die(print_r($user_ach_groupe));
                echo "</pre>";*/

                if (!empty($user_ach_groupe)) {  }

                  # ��������� ����������� � ����������
                    if ($save_arh) {
                      $groupe = serialize($user_ach_groupe);
                      if ($user_ach['groupe'] != $groupe) {
                       # echo "���������<br>";
                         sql_query("UPDATE `users_achievements` SET `groupe` = '".$groupe."', `type_curent` = '".$type_curent."' WHERE `owner` = '".$user_achiev['id']."' AND `id` = '".$user_ach['id']."' LIMIT 1;");
                      }
                    } else {
                     # echo "�������<br>";
                       	sql_query("INSERT INTO `users_achievements` (`owner`, `owner_login`, `type_curent`, `type_groupe`, `groupe`, `groupe_val`, `groupe_maxval`, `datecreate`) values ('".$user_achiev['id']."', '".$user_achiev['login']."', '".$type_curent."', '".$type_groupe."', '".serialize($user_ach_groupe)."', '".$groupe_val."', '".$groupe_maxval."', '".time()."');");
                    }




       break;


     }

    }

    $ach_glasses = 0;

 # ��������� ����������
   if (!empty($finish_ach)) {
    foreach ($finish_ach as $k => $v) {

     $achiev_name = $v;
      if (!in_array($achiev_name, $user_ach_finish)) {

        $text      = '<b'.((count($achievements[$achiev_name]['honors']) > 1)?" class=\"attention\"":"").'>'.$achievements[$achiev_name]['name'].'</b>';
        $text     .= ' (';
        $text_chat = '';
        $di        = 0;
         foreach ($achievements[$achiev_name]['honors'] as $id_honors => $honors) {
          if ($di) { $com = ', '; } else { $com = ''; }
          $text      .= $com.'<em>'.$honors['name'].'</em>'.($honors['description']?" ".$honors['description']:"").'';
          $text_chat .= $com.'<em>'.$honors['name'].'</em>'.($honors['description']?" ".$honors['description']:"").'';
          $di++;

        # ������ ���� ����������
          if ($honors['code'] == "achievement_score") {
            $ach_glasses += (int)$honors['name'];
          }

        # ������� �� ��������� ����������
          if ($honors['ex']) {
           finish_achievements_award($user_achiev, $achiev_name, $honors['code']);
          }


         }
        $text .= ')';

        $text = str_replace(' ����������', '', $text);
        $text_chat = str_replace(' ����������', '', $text_chat);

        $user_achievements_finish = $user_achievements_finish.$achiev_name.";";

        CHAT::chat_attention($user_achiev, '�� �������� ���������� <strong style="cursor: pointer;" onclick="top.frames[\'main\'].showAchBig(\''.$achiev_name.'\');">'.$achievements[$achiev_name]['name'].'</strong> ('.$text_chat.')!', array('addXMPP' => true));

      //  fun_Add_Chat_MSys ('<font color=red>��������! </font> '._GetUserChatName($user_achiev).' �������'.($user_achiev['sex']?"":"a").' ���������� <strong style="cursor: pointer;" onclick="top.frames[&amp;#039;main&amp;#039;].showAchBig(&amp;#039;'.$achiev_name.'&amp;#039;);">'.$achievements[$achiev_name]['name'].'</strong> ('.$text_chat.')!', $user_achiev['login']);

      }

    }
   }

  # ��������� ����������
    if ($ach_glasses) {
     sql_query("UPDATE `users` SET `achievements_finish` = '".$user_achievements_finish."', `achievements_glasses` = `achievements_glasses` + ".$ach_glasses." WHERE `id` = '".$user_achiev['id']."' LIMIT 1;");
     $user_my->user['achievements_finish']   = $user_achievements_finish;
     $user_my->user['achievements_glasses'] += $ach_glasses;
     if ($user_achiev['id'] == $user['id']) {
      $user['achievements_finish'] = $user_achievements_finish;
     }
    }
  }

?>