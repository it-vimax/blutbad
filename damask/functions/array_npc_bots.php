<?
/*
8248 ���������
283 ������
22315 ����� ������
10344 �������
10398 ���������
10329 ��������
10001 �������� ���� ������
10002 ��� ������
10004 ���� �� 14 ���, 8 �����
10005 Blutbad
10007 �������
10006 �������� ����
1260001 ������ �������
25790 �������� ���������
26185 �������� ���
10012 �������� ������
10003 �������
10008 ���� ��������
10009 ������
10010 ������
10011 ������
10013 �������
10014 ��������� �����
 */

/***------------------------------------------
 * ���� ���������
 **/

# ���������
  $npc_user = array();
  $npc_user[8248]['q_id'] = 8248;
  $npc_user[8248]['id'] = 8248;
  $npc_user[8248]['level'] = 14;
  $npc_user[8248]['login'] = '��������� '.$fun_get_city(INCITY).'�';
  $npc_user[8248]['obraz'] = '1_bot_m_sage.jpg';
  $npc_user[8248]['mobraz'] = '1_bot_m_sage_s.jpg';
  $npc_user[8248]['sex'] = 1;
  $npc_user[8248]['money'] = 0;
  $npc_user[8248]['bothidro'] = 0;
  $npc_user[8248]['bothran'] = 0;
  $npc_user[8248]['incity'] = INCITY;
  $npc_user[8248]['room'] = 100;
  $npc_user[8248]['battle'] = 0;
  $npc_user[8248]['bot'] = 1;
  $npc_user[8248]['moving'] = 0;
  $npc_user[8248]['speaking'] = 1;
  $npc_user[8248]['visible']  = true;

  $npc[100] = $npc_user;

#-------------------------------------------------------------------------------------

# ������
  $npc_user = array();
  $npc_user[283]['q_id'] = 283;
  $npc_user[283]['id'] = 283;
  $npc_user[283]['level'] = 15;
  $npc_user[283]['login'] = '������';
  $npc_user[283]['obraz'] = '1_bot_m_mentor.jpg';
  $npc_user[283]['mobraz'] = '1_bot_m_mentor_s.jpg';
  $npc_user[283]['sex'] = 1;
  $npc_user[283]['money'] = 0;
  $npc_user[283]['bothidro'] = 0;
  $npc_user[283]['bothran'] = 0;
  $npc_user[283]['incity'] = INCITY;
  $npc_user[283]['room'] = 102;
  $npc_user[283]['battle'] = 0;
  $npc_user[283]['bot'] = 1;
  $npc_user[283]['moving'] = 0;
  $npc_user[283]['speaking'] = 1;
  $npc_user[283]['visible']  = true;

  $npc[102] = $npc_user;

# ����� �������
  $npc_user = array();
  $npc_user[22315]['q_id'] = 22315;
  $npc_user[22315]['id'] = 22315;
  $npc_user[22315]['level'] = 12;
  $npc_user[22315]['login'] = '����� �������';
  $npc_user[22315]['obraz'] = '1_quest_m_cityman.jpg';
  $npc_user[22315]['mobraz'] = '1_quest_m_cityman_s.jpg';
  $npc_user[22315]['sex'] = 1;
  $npc_user[22315]['money'] = 0;
  $npc_user[22315]['bothidro'] = 0;
  $npc_user[22315]['bothran'] = 0;
  $npc_user[22315]['incity'] = INCITY;
  $npc_user[22315]['room'] = 150;
  $npc_user[22315]['battle'] = 0;
  $npc_user[22315]['bot'] = 1;
  $npc_user[22315]['moving'] = 0;
  $npc_user[22315]['speaking'] = 1;
  $npc_user[22315]['visible']  = true;

# �������
  $npc_user[10344]['q_id'] = 10344;
  $npc_user[10344]['id'] = 10344;
  $npc_user[10344]['level'] = '??';
  $npc_user[10344]['login'] = '������� '.$fun_get_city(INCITY).'�';
  $npc_user[10344]['obraz'] = '1_bot_m_marauder.jpg';
  $npc_user[10344]['mobraz'] = '1_bot_m_marauder_s.jpg';
  $npc_user[10344]['sex'] = 1;
  $npc_user[10344]['money'] = 0;
  $npc_user[10344]['bothidro'] = 0;
  $npc_user[10344]['bothran'] = 0;
  $npc_user[10344]['incity'] = INCITY;
  $npc_user[10344]['room'] = 150;
  $npc_user[10344]['battle'] = 0;
  $npc_user[10344]['bot'] = 1;
  $npc_user[10344]['moving'] = 0;
  $npc_user[10344]['speaking'] = 1;
  $npc_user[10344]['visible']  = true;

  $npc[150] = $npc_user;

# ���������
  $npc_user = array();
  $npc_user[10398]['q_id']     = 10398;
  $npc_user[10398]['id']       = 10398;
  $npc_user[10398]['level']    = 12;
  $npc_user[10398]['login']    = '��������� '.$fun_get_city(INCITY).'�';
  $npc_user[10398]['obraz']    = '1_bot_m_vicar.jpg';
  $npc_user[10398]['mobraz']   = '1_bot_m_vicar_s.jpg';
  $npc_user[10398]['sex']      = 1;
  $npc_user[10398]['money']    = 0;
  $npc_user[10398]['bothidro'] = 0;
  $npc_user[10398]['bothran']  = 0;
  $npc_user[10398]['incity']   = INCITY;
  $npc_user[10398]['room']     = 130;
  $npc_user[10398]['battle']   = 0;
  $npc_user[10398]['bot']      = 1;
  $npc_user[10398]['moving']   = 0;
  $npc_user[10398]['speaking'] = 1;
  $npc_user[10398]['visible']  = true;

  $npc[130] = $npc_user;

# ��������
#  $data_ids_begar = array(0, 8205, 10329, 25305);
#  $id_begar = $data_ids_begar[$fun_get_city(INCITY, 1)];

  $npc_user = array();
  $npc_user[10329]['q_id']     = 10329;
  $npc_user[10329]['id']       = 10329;
  $npc_user[10329]['level']    = 1;
  $npc_user[10329]['login']    = '�������� '.$fun_get_city(INCITY).'�';
  $npc_user[10329]['obraz']    = '1_beggar.gif';
  $npc_user[10329]['mobraz']   = '1_bot_m_rogue_s.jpg';
  $npc_user[10329]['sex']      = 1;
  $npc_user[10329]['money']    = 0;
  $npc_user[10329]['bothidro'] = 0;
  $npc_user[10329]['bothran']  = 0;
  $npc_user[10329]['incity']   = INCITY;
  $npc_user[10329]['room']     = 120;
  $npc_user[10329]['battle']   = 0;
  $npc_user[10329]['bot']      = 1;
  $npc_user[10329]['moving']   = 1;
  $npc_user[10329]['speaking'] = 1;
  $npc_user[10329]['visible']  = true;

# �������� ���� ������
  $npc_user[10001]['q_id']     = 10001;
  $npc_user[10001]['id']       = 10001;
  $npc_user[10001]['level']    = 1;
  $npc_user[10001]['login']    = '�������� ���� ������';
  $npc_user[10001]['obraz']    = '1_bot_santa_helper.jpg';
  $npc_user[10001]['mobraz']   = '1_bot_m_santa_helper_s.jpg';
  $npc_user[10001]['sex']      = 0;
  $npc_user[10001]['money']    = 0;
  $npc_user[10001]['bothidro'] = 0;
  $npc_user[10001]['bothran']  = 0;
  $npc_user[10001]['incity']   = INCITY;
  $npc_user[10001]['room']     = 120;
  $npc_user[10001]['battle']   = 0;
  $npc_user[10001]['bot']      = 1;
  $npc_user[10001]['moving']   = 0;
  $npc_user[10001]['speaking'] = 1;
  $npc_user[10001]['visible']  = false; # ��������� �� ����

# ��� ������
  $npc_user[10002]['q_id']     = 10002;
  $npc_user[10002]['id']       = 10002;
  $npc_user[10002]['level']    = 12;
  $npc_user[10002]['login']    = '��� ������ '.$fun_get_city(INCITY).'�';
  $npc_user[10002]['obraz']    = '1_bot_sir_knight.jpg';
  $npc_user[10002]['mobraz']   = '1_bot_m_sir_knight_s.jpg';
  $npc_user[10002]['sex']      = 1;
  $npc_user[10002]['money']    = 0;
  $npc_user[10002]['bothidro'] = 0;
  $npc_user[10002]['bothran']  = 0;
  $npc_user[10002]['incity']   = INCITY;
  $npc_user[10002]['room']     = 120;
  $npc_user[10002]['battle']   = 0;
  $npc_user[10002]['bot']      = 1;
  $npc_user[10002]['moving']   = 0;
  $npc_user[10002]['speaking'] = 1;
  $npc_user[10002]['visible']  = true;

# ���� �� 14 ���
  $npc_user[10004]['q_id']     = 10004;
  $npc_user[10004]['id']       = 10004;
  $npc_user[10004]['level']    = 33;
  $npc_user[10004]['login']    = '���� �� '.$fun_get_city(INCITY).'�';
  $npc_user[10004]['obraz']    = '1_bot_amur.jpg';
  $npc_user[10004]['mobraz']   = '1_bot_m_amur_s.jpg';
  $npc_user[10004]['sex']      = 1;
  $npc_user[10004]['money']    = 0;
  $npc_user[10004]['bothidro'] = 0;
  $npc_user[10004]['bothran']  = 0;
  $npc_user[10004]['incity']   = INCITY;
  $npc_user[10004]['room']     = 120;
  $npc_user[10004]['battle']   = 0;
  $npc_user[10004]['bot']      = 1;
  $npc_user[10004]['moving']   = 0;
  $npc_user[10004]['speaking'] = 1;
  $npc_user[10004]['visible']  = true;

# Blutbad
  $npc_user[10005]['q_id']     = 10005;
  $npc_user[10005]['id']       = 10005;
  $npc_user[10005]['level']    = 100;
  $npc_user[10005]['login']    = 'Blutbad';
  $npc_user[10005]['obraz']    = 'invisible_new.jpg';
  $npc_user[10005]['mobraz']   = '1_bot_m_invisible_new_s.jpg';
  $npc_user[10005]['sex']      = 1;
  $npc_user[10005]['money']    = 0;
  $npc_user[10005]['bothidro'] = 0;
  $npc_user[10005]['bothran']  = 0;
  $npc_user[10005]['incity']   = INCITY;
  $npc_user[10005]['room']     = 120;
  $npc_user[10005]['battle']   = 0;
  $npc_user[10005]['bot']      = 1;
  $npc_user[10005]['moving']   = 0;
  $npc_user[10005]['speaking'] = 1;
  $npc_user[10005]['visible']  = true;

# �������
  $npc_user[10007]['q_id']     = 10007;
  $npc_user[10007]['id']       = 10007;
  $npc_user[10007]['level']    = '??';
  $npc_user[10007]['login']    = '�������';
  $npc_user[10007]['obraz']    = '1_evil_spirits.jpg';
  $npc_user[10007]['mobraz']   = '1_evil_spirits.jpg';
  $npc_user[10007]['sex']      = 1;
  $npc_user[10007]['money']    = 0;
  $npc_user[10007]['bothidro'] = 0;
  $npc_user[10007]['bothran']  = 0;
  $npc_user[10007]['incity']   = INCITY;
  $npc_user[10007]['room']     = 120;
  $npc_user[10007]['battle']   = 0;
  $npc_user[10007]['bot']      = 1;
  $npc_user[10007]['moving']   = 0;
  $npc_user[10007]['speaking'] = 0;
  $npc_user[10007]['visible']  = false;

# ���� ��������
  $npc_user[10008]['q_id']     = 10008;
  $npc_user[10008]['id']       = 10008;
  $npc_user[10008]['level']    = '??';
  $npc_user[10008]['login']    = '���� ��������';
  $npc_user[10008]['obraz']    = '1_ChristmasThief.jpg';
  $npc_user[10008]['mobraz']   = '1_ChristmasThief.jpg';
  $npc_user[10008]['sex']      = 1;
  $npc_user[10008]['money']    = 0;
  $npc_user[10008]['bothidro'] = 0;
  $npc_user[10008]['bothran']  = 0;
  $npc_user[10008]['incity']   = INCITY;
  $npc_user[10008]['room']     = 120;
  $npc_user[10008]['battle']   = 0;
  $npc_user[10008]['bot']      = 1;
  $npc_user[10008]['moving']   = 0;
  $npc_user[10008]['speaking'] = 0;
  $npc_user[10008]['visible']  = false;

  $npc[120] = $npc_user;

#-------------------------------------------------------------------------------------
# �������� ����
  $npc_user = array();
  $npc_user[10006]['q_id']     = 10006;
  $npc_user[10006]['id']       = 10006;
  $npc_user[10006]['level']    = 16;
  $npc_user[10006]['login']    = '�������� ����';
  $npc_user[10006]['obraz']    = '1_bot_merchantsaid.jpg';
  $npc_user[10006]['mobraz']   = '1_bot_merchantsaid_s.jpg';
  $npc_user[10006]['sex']      = 1;
  $npc_user[10006]['money']    = 0;
  $npc_user[10006]['bothidro'] = 0;
  $npc_user[10006]['bothran']  = 0;
  $npc_user[10006]['incity']   = INCITY;
  $npc_user[10006]['room']     = 123;
  $npc_user[10006]['battle']   = 0;
  $npc_user[10006]['bot']      = 1;
  $npc_user[10006]['moving']   = 0;
  $npc_user[10006]['speaking'] = 1;
  $npc_user[10006]['visible']  = true;

  $npc[123] = $npc_user;

# ������ �������
  $npc_user = array();
  $npc_user[1260001]['q_id']     = 1260001;
  $npc_user[1260001]['id']       = 1260001;
  $npc_user[1260001]['level']    = 10;
  $npc_user[1260001]['login']    = '������ ������� �����';
  $npc_user[1260001]['obraz']    = '1_bot_m_tavernkeeper.jpg';
  $npc_user[1260001]['mobraz']   = '1_bot_m_tavernkeeper_s.jpg';
  $npc_user[1260001]['sex']      = 1;
  $npc_user[1260001]['money']    = 0;
  $npc_user[1260001]['bothidro'] = 0;
  $npc_user[1260001]['bothran']  = 0;
  $npc_user[1260001]['incity']   = INCITY;
  $npc_user[1260001]['room']     = 126;
  $npc_user[1260001]['battle']   = 0;
  $npc_user[1260001]['bot']      = 1;
  $npc_user[1260001]['moving']   = 1;
  $npc_user[1260001]['speaking'] = 1;
  $npc_user[1260001]['visible']  = true;

  $npc[126] = $npc_user;

#-------------------------------------------------------------------------------------

# �������� ���������
  $npc_user = array();
  $npc_user[25790]['q_id']    = 25790;
  $npc_user[25790]['id']      = 25790;
  $npc_user[25790]['level']   = 12;
  $npc_user[25790]['login']   = '�������� ���������';
  $npc_user[25790]['obraz']   = 'bot_keepers_maze.jpg';
  $npc_user[25790]['mobraz']  = '';
  $npc_user[25790]['sex']     = 1;
  $npc_user[25790]['money']   = 0;
  $npc_user[25790]['bothidro']= 0;
  $npc_user[25790]['bothran'] = 0;
  $npc_user[25790]['incity']  = INCITY;
  $npc_user[25790]['room']    = 431;
  $npc_user[25790]['battle']  = 0;
  $npc_user[25790]['bot']     = 1;
  $npc_user[25790]['moving']  = 1;
  $npc_user[25790]['speaking']= 1;
  $npc_user[25790]['visible'] = true;

  $npc[431] = $npc_user;

# �������� ���
  $npc_user = array();
  $npc_user[26185]['q_id']    = 26185;
  $npc_user[26185]['id']      = 26185;
  $npc_user[26185]['level']   = 10;
  $npc_user[26185]['login']   = '��������� ��������';
  $npc_user[26185]['obraz']   = 'bot_keeper_catacombs.jpg';
  $npc_user[26185]['mobraz']  = '';
  $npc_user[26185]['sex']     = 1;
  $npc_user[26185]['money']   = 0;
  $npc_user[26185]['bothidro']= 0;
  $npc_user[26185]['bothran'] = 0;
  $npc_user[26185]['incity']  = INCITY;
  $npc_user[26185]['room']    = 441;
  $npc_user[26185]['battle']  = 0;
  $npc_user[26185]['bot']     = 1;
  $npc_user[26185]['moving']  = 1;
  $npc_user[26185]['speaking']= 1;
  $npc_user[26185]['visible']  = true;

  $npc[441] = $npc_user;

#-------------------------------------------------------------------------------------

# �������� ������
  $npc_user = array();
  $npc_user[10012]['q_id']    = 10012;
  $npc_user[10012]['id']      = 10012;
  $npc_user[10012]['level']   = '??';
  $npc_user[10012]['login']   = '��������� ������';
  $npc_user[10012]['obraz']   = 'bot_keeper_lair.jpg';
  $npc_user[10012]['mobraz']  = '';
  $npc_user[10012]['sex']     = 1;
  $npc_user[10012]['money']   = 0;
  $npc_user[10012]['bothidro']= 0;
  $npc_user[10012]['bothran'] = 0;
  $npc_user[10012]['incity']  = INCITY;
  $npc_user[10012]['room']    = 471;
  $npc_user[10012]['battle']  = 0;
  $npc_user[10012]['bot']     = 1;
  $npc_user[10012]['moving']  = 1;
  $npc_user[10012]['speaking']= 1;
  $npc_user[10012]['visible']  = true;

  $npc[471] = $npc_user;

# �������
  $npc_user = array();
  $npc_user[10003]['q_id']    = 10003;
  $npc_user[10003]['id']      = 10003;
  $npc_user[10003]['level']   = 14;
  $npc_user[10003]['login']   = '������� '.$fun_get_city(INCITY).'�';
  $npc_user[10003]['obraz']   = '1_bot_miner.jpg';
  $npc_user[10003]['mobraz']  = '1_bot_m_miner_s.jpg';
  $npc_user[10003]['sex']     = 1;
  $npc_user[10003]['money']   = 0;
  $npc_user[10003]['bothidro']= 0;
  $npc_user[10003]['bothran'] = 0;
  $npc_user[10003]['incity']  = INCITY;
  $npc_user[10003]['room']    = 450;
  $npc_user[10003]['battle']  = 0;
  $npc_user[10003]['bot']     = 1;
  $npc_user[10003]['moving']  = 1;
  $npc_user[10003]['speaking']= 1;
  $npc_user[10003]['visible']  = true;

  $npc[450] = $npc_user;

#-------------------------------------------------------------------------------------

# ������
  $npc_user = array();
  $npc_user[10009]['q_id']    = 10009;
  $npc_user[10009]['id']      = 10009;
  $npc_user[10009]['level']   = '??';
  $npc_user[10009]['login']   = '������';
  $npc_user[10009]['obraz']   = '1_bot_m_marauder.jpg';
  $npc_user[10009]['mobraz']  = '1_bot_m_marauder_s.jpg';
  $npc_user[10009]['sex']     = 1;
  $npc_user[10009]['money']   = 0;
  $npc_user[10009]['bothidro']= 0;
  $npc_user[10009]['bothran'] = 0;
  $npc_user[10009]['incity']  = INCITY;
  $npc_user[10009]['room']    = 901;
  $npc_user[10009]['battle']  = 0;
  $npc_user[10009]['bot']     = 1;
  $npc_user[10009]['moving']  = 1;
  $npc_user[10009]['speaking']= 0;
  $npc_user[10009]['visible']  = true;

  $npc[901] = $npc_user;

# ������
  $npc_user = array();
  $npc_user[10010]['q_id']    = 10010;
  $npc_user[10010]['id']      = 10010;
  $npc_user[10010]['level']   = '??';
  $npc_user[10010]['login']   = '������';
  $npc_user[10010]['obraz']   = '1_bot_m_marauder.jpg';
  $npc_user[10010]['mobraz']  = '1_bot_m_marauder_s.jpg';
  $npc_user[10010]['sex']     = 1;
  $npc_user[10010]['money']   = 0;
  $npc_user[10010]['bothidro']= 0;
  $npc_user[10010]['bothran'] = 0;
  $npc_user[10010]['incity']  = INCITY;
  $npc_user[10010]['room']    = 902;
  $npc_user[10010]['battle']  = 0;
  $npc_user[10010]['bot']     = 1;
  $npc_user[10010]['moving']  = 1;
  $npc_user[10010]['speaking']= 0;
  $npc_user[10010]['visible']  = true;

  $npc[902] = $npc_user;

# ������
  $npc_user = array();
  $npc_user[10011]['q_id']    = 10011;
  $npc_user[10011]['id']      = 10011;
  $npc_user[10011]['level']   = '??';
  $npc_user[10011]['login']   = '������';
  $npc_user[10011]['obraz']   = '1_bot_m_marauder.jpg';
  $npc_user[10011]['mobraz']  = '1_bot_m_marauder_s.jpg';
  $npc_user[10011]['sex']     = 1;
  $npc_user[10011]['money']   = 0;
  $npc_user[10011]['bothidro']= 0;
  $npc_user[10011]['bothran'] = 0;
  $npc_user[10011]['incity']  = INCITY;
  $npc_user[10011]['room']    = 903;
  $npc_user[10011]['battle']  = 0;
  $npc_user[10011]['bot']     = 1;
  $npc_user[10011]['moving']  = 1;
  $npc_user[10011]['speaking']= 0;
  $npc_user[10011]['visible']  = true;

  $npc[903] = $npc_user;

#-------------------------------------------------------------------------------------

# �������
  $npc_user = array();
  $npc_user[10013]['q_id']    = 10013;
  $npc_user[10013]['id']      = 10013;
  $npc_user[10013]['level']   = '??';
  $npc_user[10013]['login']   = '�������';
  $npc_user[10013]['obraz']   = '1_bot_m_sorcerers.jpg';
  $npc_user[10013]['mobraz']  = '1_bot_m_sorcerers_s.jpg';
  $npc_user[10013]['sex']     = 0;
  $npc_user[10013]['money']   = 0;
  $npc_user[10013]['bothidro']= 0;
  $npc_user[10013]['bothran'] = 0;
  $npc_user[10013]['incity']  = INCITY;
  $npc_user[10013]['room']    = 154;
  $npc_user[10013]['battle']  = 0;
  $npc_user[10013]['bot']     = 1;
  $npc_user[10013]['moving']  = 0;
  $npc_user[10013]['speaking']= 1;
  $npc_user[10013]['visible']  = true;

  $npc[154] = $npc_user;

#-------------------------------------------------------------------------------------

# ��������� �����
  $npc_user = array();
  $npc_user[10014]['q_id']    = 10014;
  $npc_user[10014]['id']      = 10014;
  $npc_user[10014]['level']   = '??';
  $npc_user[10014]['login']   = '��������� �����';
  $npc_user[10014]['obraz']   = '1_bot_high_priestess.jpg';
  $npc_user[10014]['mobraz']  = '1_bot_high_priestess_s.jpg';
  $npc_user[10014]['sex']     = 0;
  $npc_user[10014]['money']   = 0;
  $npc_user[10014]['bothidro']= 0;
  $npc_user[10014]['bothran'] = 0;
  $npc_user[10014]['incity']  = INCITY;
  $npc_user[10014]['room']    = 101;
  $npc_user[10014]['battle']  = 0;
  $npc_user[10014]['bot']     = 1;
  $npc_user[10014]['moving']  = 0;
  $npc_user[10014]['speaking']= 1;
  $npc_user[10014]['visible']  = true;

  $npc[101] = $npc_user;
?>