<?
/*
 ������
  AUTO_INCREMENT �� AUTOINCREMENT
  int = INTEGER
  longtext, tinyint ���= TEXT
 */
$sql = "

CREATE TABLE battle (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  coment TEXT,
  teams TEXT,
  teams_old TEXT,
  timeout INTEGER,
  type INTEGER,
  status INTEGER,
  status_battle INTEGER,
  t1 TEXT,
  t2 TEXT,
  texit TEXT,
  tintervention TEXT,
  date TEXT,
  win INTEGER,
  damage TEXT,
  karma TEXT,
  valor TEXT,
  statis TEXT,
  to1 INTEGER,
  to2 INTEGER,
  startbattle INTEGER,
  user_update_battle INTEGER,
  b_hint_nbp TEXT,
  battle_eff TEXT,
  battle_runes TEXT,
  exp TEXT,
  blood INTEGER,
  hideb INTEGER,
  t1list TEXT,
  t2list TEXT,
  t1hist TEXT,
  t2hist TEXT,
  tools TEXT,
  closed INTEGER,
  room INTEGER,
  city TEXT,
  us_nich INTEGER,
  battle_end INTEGER,
  battle_err TEXT
);
";
?>