<?

/***--------------------------------------
 * �������� ������� � ����������
 **/

function check_original_item($cur, $orig){
   if (
      $cur['duration'] != $orig['duration']
   || $cur['grind']    != $orig['grind']
   || $cur['maxdur']   != $orig['maxdur']
   || $cur['cost']     != $orig['cost']
   || $cur['nlevel']   != $orig['nlevel']
   || $cur['nsila']    != $orig['nsila']
   || $cur['nlovk']    != $orig['nlovk']
   || $cur['ninta']    != $orig['ninta']
   || $cur['nintel']   != $orig['nintel']
   || $cur['nmudra']   != $orig['nmudra']
   || $cur['nskill_fishing']   != $orig['nskill_fishing']
   || $cur['minu']     != $orig['minu']
   || $cur['maxu']     != $orig['maxu']
   || $cur['gsila']    != $orig['gsila']
   || $cur['glovk']    != $orig['glovk']
   || $cur['ginta']    != $orig['ginta']
   || $cur['gintel']   != $orig['gintel']
   || $cur['gskill_fishing']   != $orig['gskill_fishing']
   || $cur['ghp']      != $orig['ghp']
   || $cur['gmana']    != $orig['gmana']
   || $cur['mfkrit']   != $orig['mfkrit']
   || $cur['mfakrit']  != $orig['mfakrit']
   || $cur['mfuvorot'] != $orig['mfuvorot']
   || $cur['mfauvorot']!= $orig['mfauvorot']
   || $cur['bron1']    != $orig['bron1']
   || $cur['bron2']    != $orig['bron2']
   || $cur['bron3']    != $orig['bron3']
   || $cur['bron4']    != $orig['bron4']
   || $cur['magic']    != $orig['magic']
   || $cur['massa']    != $orig['massa']
   || $cur['opisan']   != $orig['opisan']
   ) {
     return false;
   } else {
     return true;
   }
}

/***--------------------------------------
 * �������� �������������� ��������
 **/

function show_invisible($dress, $inv = 0, $invis = false) {
  global $mine_name_res;
 $magic = magicinf($dress['magic']);
if ($dress) {

 $check_parms = function ($num1, $num2){
   $color = '';
   if ($num1 > $num2) {
     $color = 'style="background-color: #F3C2C2;';
   } else {
     $color = '';
   }
   return $color;
 }

?>
<div class="<?= ($invis?"invisible":"") ?>">

	<ul class="item-description">
		<li style=' vertical-align: middle;'>
			<b class="title" style=' vertical-align: middle;'><?= $dress['name'] ?></b>
           <? if ($dress['massa']) { ?>
			<span style=' vertical-align: middle;'>(�����:&nbsp;<span class="weight"><?= strrtrim($dress['massa'], '.0') ?></span>)</span>  
           <? } ?>
           <?
            if (!empty($dress['present'])) { ?>
              <img src="http://img.blutbad.ru/i/podarok.gif" style=" vertical-align: middle; " width="16" height="18" border=0>
           <? } ?>
		</li>
		<? if ($dress['cost'] > 0) { ?><li>����: <?= $dress['cost'] ?> ��.</li><? } ?>
		<? if ($dress['ecost'] > 0) { ?><li>����: <?= $dress['ecost'] ?> ���.</li><? } ?>
        <? if ($dress['maxdur']) { ?>
		<li <?= ($dress['duration'] != 0?'style="background-color: #F3C2C2;"':'') ?>>
			�������������: <span class="durability"><span class=""><?= $dress['duration'] ?></span>/<?= $dress['maxdur'] ?></span>
		</li>
        <? } ?>
		<li>
		    <? if ($dress['goden'] || $dress['dategoden']) { ?><span class="">���� ��������: <?= $dress['goden']." ��. ".($dress['dategoden']?'(����� �� '.date("Y.m.d H:i", $dress['dategoden']).')':'')."" ?></span><? } ?>
		</li>
		<? if (!empty($dress['text'])) { ?>
        <li>
            <b>����������:</b><ul><li>�� ����� �������������: <span class="endurance"><b><?= $dress['text'] ?></b></span><br></li></ul>
        </li>
        <? } ?>
        <? if (!empty($dress['coating'])) {
         $coating = explode("_", $dress['coating']);
        ?>
        <li>
          <span style="vertical-align: middle;">��������: <img class="tooltip" style="vertical-align: middle;" src="http://img.blutbad.ru/i/dungeon/min/mini/obj_min_<?= $coating[0] ?>.gif" width="15" height="15" title="<?= $mine_name_res[$coating[0]]['name'] ?>">&nbsp;�������&nbsp;<?= '['.$coating[1].']' ?></span>
        </li>
        <? } ?>

        <? if ($dress['nlevel'] || $dress['nage'] || $dress['nsila'] || $dress['nlovk'] || $dress['ninta'] || $dress['nvinos']) { ?>
		<li>
			<b>����������:</b>
			<ul>
				<? if ($dress['nlevel']!=='0') { ?><li>�������: <?= $dress['nlevel'] ?></li><? } ?>
				<? if ($dress['nage']!=='0') { ?><li>�������: <?= $dress['nage'] ?></li><? } ?>
				<? if ($dress['nsila']!=='0') { ?><li>����: <?= $dress['nsila'] ?></li><? } ?>
				<? if ($dress['nlovk']!=='0') { ?><li>��������: <?= $dress['nlovk'] ?></li><? } ?>
				<? if ($dress['ninta']!=='0') { ?><li>��������: <?= $dress['ninta'] ?></li><? } ?>
				<? if ($dress['nvinos']!=='0') { ?><li>����������������: <?= $dress['nvinos'] ?></li><? } ?>
				<? if ($dress['nintel']!=='0') { ?><li>���������: <?= $dress['nintel'] ?></li><? } ?>
				<? if ($dress['nmudra']!=='0') { ?><li>��������: <?= $dress['nmudra'] ?></li><? } ?>
				<? if ($dress['nskill_fishing']!=='0') { ?><li>����� �����������: <?= $dress['nskill_fishing'] ?></li><? } ?>
				<? if ($dress['nlicense']==2) { ?><li>�������� ��������</li><? } ?>
			</ul>
		</li>
        <? } ?>
        <? if ($dress['gsila'] || $dress['glovk'] || $dress['ginta'] || $dress['gintel'] || $dress['ghp'] || $dress['gmana']
         || $dress['minu'] || $dress['maxu'] || $dress['mfkrit'] || $dress['mfakrit'] || $dress['mfuvorot'] || $dress['mfauvorot']
          || $dress['bron1'] || $dress['bron2'] || $dress['bron3'] || $dress['bron4'] || $magic['chanse'] || $magic['time']) { ?>
		<li>
			<b>��������:</b>
			<ul>
                <? if ($dress['gsila']>0) { ?><li>
					����: <span class="endurance">+<?= $dress['gsila'] ?></span>
				</li><? } ?>
				<? if ($dress['glovk']>0) { ?><li>
					��������: <span class="endurance">+<?= $dress['glovk'] ?></span>
				</li><? } ?>
				<? if ($dress['ginta']>0) { ?><li>
					��������: <span class="endurance">+<?= $dress['ginta'] ?></span>
				</li><? } ?>
				<? if ($dress['gintel']>0) { ?><li>
					���������: <span class="endurance">+<?= $dress['gintel'] ?></span>
				</li><? } ?>
				<? if ($dress['ghp']>0) { ?><li class="red">
					������� �����: <span class="ghp">+<?= $dress['ghp'] ?></span>
				</li><? } ?>
				<? if ($dress['gmana']!=='0') { ?><li class="gma">
					 ������������: <span class="gma">+<?= $dress['gmana'] ?></span>
				</li><? } ?>
				<? if ($dress['minu']!=='0') { ?><li>
					����������� ����: <span class="uronmin">+<?= $dress['minu'] ?></span>
				</li><? } ?>
				<? if ($dress['maxu']!=='0') { ?><li>
					������������ ����: <span class="uronmax">+<?= $dress['maxu'] ?></span>
				</li><? } ?>
                <? if ($dress['mfkrit']!=='0') { ?><li class="�krit">
					����������� ����: <span class="�krit">+<?= $dress['mfkrit'] ?></span>
				</li><? } ?>
                <? if ($dress['mfakrit']!=='0') { ?><li class="�krit">
					������ ������������ �����: <span class="�krit">+<?= $dress['mfakrit'] ?></span>
				</li><? } ?>
                <? if ($dress['mfuvorot']!=='0') { ?><li class="�uvor">
					�����������: <span class="�uvor">+<?= $dress['mfuvorot'] ?></span>
				</li><? } ?>
                <? if ($dress['mfauvorot']!=='0') { ?><li class="�uvor">
					������ �����������: <span class="�uvor">+<?= $dress['mfauvorot'] ?></span>
				</li><? } ?>
				<? if ($dress['bron1']!=='0') { ?><li>
					����� ������: <span class="br1">+<?= $dress['bron1'] ?></span>
				</li><? } ?>
				<? if ($dress['bron2']!=='0') { ?><li>
					����� �������: <span class="br2">+<?= $dress['bron2'] ?></span>
				</li><? } ?>
				<? if ($dress['bron3']!=='0') { ?><li>
					����� �����: <span class="br3">+<?= $dress['bron3'] ?></span>
				</li><? } ?>
				<? if ($dress['bron4']!=='0') { ?><li>
					����� ���: <span class="br4">+<?= $dress['bron4'] ?></span>
				</li><? } ?>
				<? if ($magic['chanse'] > 0) { ?><li>
					 ����������� ������������: <span class="endurance"><?= $magic['chanse'] ?>%</span>
				</li><? } ?>
				<? if ($magic['time'] > 0) { ?><li>
					 �����������������: <span class="endurance"><?= $magic['time'] ?> ���.</span>
				</li><? } ?>
				<? if ($dress['gskill_fishing']) { ?><li>
					 ����� �����������: <span>+<?= $dress['gskill_fishing'] ?></span>
				</li><? } ?>

			</ul>
		</li>
        <? } ?>
		<? if ($dress['opisan']!=='') { ?><li>
        <b>��������:</b><br><?= $dress['opisan'] ?><br>
        </li><? } ?>


<?
  # ������� "�������������"
    if (@$dress['otdel'] == 18) {

    global $ar_rod;

     if (!empty($ar_rod[$dress['name']])) {
       echo "<li class=features><b>�������������:</b><ul><li>���������� �������:";
         echo "<ul>";
           foreach ($ar_rod[$dress['name']] as $k => $v) {
             echo "<li>".$v."</li>";
           }
         echo "</ul>";
       echo "</li></ul></li>";
     }
    }
?>

		<? if ($dress['cost']) { ?><li>
          <br />���� � ��������:  <?= $dress['cost']." ��" ?>.<br />������� � ��������: ����</li><? } ?>

	</ul>
</div>
<?
 }
}

?>