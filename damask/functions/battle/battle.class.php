<?php

 #--------------------------------
 # �������� �������� ����� �� ����
   if (empty($_SESSION['lastturn']) && @$_SESSION['lastturn'] != 0) { $_SESSION['lastturn'] = 0; }

   $text_death = array(" ���������.", " ��������.");
   $text_salvation = array(" ��������.", " ������.", " �����������.", " ����������.", " ����� ��������.", " ����� ������.");

   $text_atakt = array("������", "�����", "�����", "����");
   $text_isudar = array("�����", "�������");

/***--------------------------
 * ������� ��� ��������� �����
 **/

  # ���������
    if (!empty($_SESSION['rune_hint'])) { $_SESSION['rune_hint'] = ''; }
    if ($user_my->user['hit_hod']) {
     $_SESSION['main_hint']   = '';       # �������
     $_SESSION['zayavka_hit'] = '';       # ������
     $_SESSION['battle_hint'] = '';       # ������ ���������
    }

  # ����
    $hit_hod    = $user_my->user['hit_hod'];
    $hit_hp     = $user_my->user['hit_hp'];
    $hit_udar   = $user_my->user['hit_udar'];
    $hit_block  = $user_my->user['hit_block'];
    $hit_uvorot = $user_my->user['hit_uvorot'];
    $hit_krit   = $user_my->user['hit_krit'];

/***------------------------------------------
 * ����� ������������� ����
 **/
function isuse_battle_rune($ibattle_runes, $row){
 global $user_my;
  $battle_runes = unserialize($ibattle_runes);

  $battle_runes[$user_my->user['id']][$row['id']]['name'] = $row['name'];      # ��� ����
  $battle_runes[$user_my->user['id']][$row['id']]['ex']   = ($row['stl'] || $row['ecost'])?1:0;   # ������������ ���� ��� ���

  sql_query("UPDATE `battle` SET `battle_runes` = '".serialize($battle_runes)."' WHERE `id` = ".$user_my->user['battle']." LIMIT 1;");
}

/***--------------------------
 * ������� ��� �����
 **/

function percent_exp($user_p, $is_battle_p) {
 global $eff_obraz;

 $stand_percent = 100;            # ������� ������� �����
 $percent       = $stand_percent; # ������� ������� �����

 if (!empty($user_p['id']) || !empty($is_battle_p['id'])) {

   $eff_bonus = sql_row("SELECT sum(total) as total_sum FROM `effects` WHERE `owner` = '".$user_p['id']."' AND (`type` = 1039 OR `type` = 1034);");

   if (!empty($eff_bonus['total_sum'])) { $percent += $eff_bonus['total_sum']; }

   if ($is_battle_p['id']) {
     # ��������� ����� �����������
       if ($is_battle_p['type'] == 25) { $percent += 100; }

    # �� ������
      if(date("w") < 6 && date("w") > 0){
      # ����� � 1 �� 8 ����
      	if (date("G") >= 1 && date("G") <= 8){
      		$percent += 20;
      	}
      } else {
      # �� ��������
        $percent += 50;
      }

    # ����� ������ ������� � �����
      if (isset($eff_obraz[$user_p['shadow']]['persent_exp'])) {
        $percent += $eff_obraz[$user_p['shadow']]['persent_exp'];
      }

      if ($user_p['klan']) {
       $klan_user_p = sql_row("SELECT clanlevel, name, place_count FROM `clans` WHERE `name` = '{$user_p['klan']}' LIMIT 1;");
       $klan_user_maxlimit = klan_user_maxlimit($klan_user_p, 'count');
            if ($klan_user_maxlimit >= floor($klan_user_p['place_count']))      { $percent += 20; }
        elseif ($klan_user_maxlimit >= floor($klan_user_p['place_count']*0.85)) { $percent += 10; }
        elseif ($klan_user_maxlimit >= floor($klan_user_p['place_count']*0.7))  { $percent += 5; }
      }

    # ��� ��������
      if($is_battle_p['blood']) { $percent += 50; }

    # �� ����� ��� ���� +100% � �����
      if(date("m") == 1 && (date("d") == 1 || date("d") == 2 || date("d") == 3)) { $percent += 100; }

      if ($is_battle_p['id']) {
        # ���� ��� ���������
        switch ($is_battle_p['status_battle']) {
          case 1: $percent += 10;  break; # ������ �������!
          case 2: $percent += 30;  break; # ������ ������������
          case 3: $percent += 50;  break; # ��������� �����!
          case 4: $percent += 75;  break; # �������������� ��������!
          case 5: $percent += 100; break; # ������������ ��������!
        }
      }
   }
 }
  return $percent;
}

/***------------------------------------------
 * �������� � ����� �� ������� ��� ��������
 **/

  function getisroom() { global $user_my;
    return $user_my->user['dungeon']?"dungeon":"main";
  }

/***----------------------------------------
 * �������� ����� �� �� ������������ �����
 **/

function is_activete_priem($user_my, $this_battle, $priem){
  global $hit_hod, $hit_hp,  $hit_udar, $hit_block, $hit_uvorot, $hit_krit;

 if ($hit_hod    >= $priem['n_hod']
  && $hit_hp     >= $priem['n_hp']
  && $hit_udar   >= $priem['n_udar']
  && $hit_block  >= $priem['n_block']
  && $hit_uvorot >= $priem['n_uvorot']
  && $hit_krit   >= $priem['n_krit']
  && $user_my->user['str_mind'] >= $priem['n_str_mind']
  && ($user_my->user['intel'] + $user_my->user['inv_intelligence']) >= $priem['nu_intel']
  && empty($this_battle->battle_eff[$user_my->user['id']][$priem['priem']])) {
  return true;
 } else {
  return false;
 }
}

/***--------------------------
 * ��������� ��� �������
 **/

function get_priem_requirements($priem){
 global $user_my, $hit_hod, $hit_hp,  $hit_udar, $hit_block, $hit_uvorot, $hit_krit;
 $str = '';
 if ($priem['nu_intel'] && ($user_my->user['intel'] + $user_my->user['inv_intelligence']) < $priem['nu_intel']) {
     $str .= '<li><span style="color: red">���������:  '.$priem['nu_intel'].'</span></li>';
 } if ($priem['n_str_mind']) {
     $str .= '<li><span>���� ����:  '.$priem['n_str_mind'].(($priem['n_str_mind'] > $user_my->user['str_mind'])?' <b><i>(��� '.($priem['n_str_mind']-$user_my->user['str_mind']).')</i></b>':"").'</span><img alt="" src="http://img.blutbad.ru/i/hit_str_mind.png" title="���� ����"></li>';
 } if ($priem['n_hod']) {
     $str .= '<li><span>������:  '.$priem['n_hod'].(($priem['n_hod'] > $hit_hod)?' <b><i>(��� '.($priem['n_hod']-$hit_hod).')</i></b>':"").'</span><img alt="" src="http://img.blutbad.ru/i/hit_udar.png" title="������"></li>';
 } if ($priem['n_udar']) {
     $str .= '<li><span>������� ������:  '.$priem['n_udar'].(($priem['n_udar'] > $hit_udar)?' <b><i>(��� '.($priem['n_udar']-$hit_udar).')</i></b>':"").'</span><img alt="" src="http://img.blutbad.ru/i/hit_udar.png" title="������� ����"></li>';
 } if ($priem['n_hp']) {
     $str .= '<li><span>���������:  '.$priem['n_hp'].(($priem['n_hp'] > $hit_hp)?' <b><i>(��� '.($priem['n_hp']-$hit_hp).')</i></b>':"").'</span><img alt="" src="http://img.blutbad.ru/i/hit_hp.png" title="���������"></li>';
 } if ($priem['n_block']) {
     $str .= '<li><span>������� ������:  '.$priem['n_block'].(($priem['n_block'] > $hit_block)?' <b><i>(��� '.($priem['n_block']-$hit_block).')</i></b>':"").'</span><img alt="" src="http://img.blutbad.ru/i/hit_block.png" title="������� ������"></li>';
 } if ($priem['n_uvorot']) {
     $str .= '<li><span>�������� �� �����:  '.$priem['n_uvorot'].(($priem['n_uvorot'] > $hit_uvorot)?' <b><i>(��� '.($priem['n_uvorot']-$hit_uvorot).')</i></b>':"").'</span><img alt="" src="http://img.blutbad.ru/i/hit_uvor.png" title="�������� �� �����"></li>';
 } if ($priem['n_krit']) {
     $str .= '<li><span>����������� ������:  '.$priem['n_krit'].(($priem['n_krit'] > $hit_krit)?' <b><i>(��� '.($priem['n_krit']-$hit_krit).')</i></b>':"").'</span><img alt="" src="http://img.blutbad.ru/i/hit_krit.png" title="����������� ������"></li>';
 }
 return $str;
}

/***-------------------------------
 * ������ ������������ ���� �����
 **/

	class fbattle {
			public $log_channel    = '';        # ������ ��������
			public $status         = integer;   # ������ ��������  ---- 0 - ��� �����; 1 - ���� �����
			public $battle         = array();   # ������ � ������������
			public $battle_eff     = array();   # ������ � ��������� ���
			public $b_hint_nbp     = array();   # � ������� ���

			public $battle_data    = array();   # ������ �� �����
			public $enemy          = null;      # ������������� ����������

			public $tintervention  = array();   # ������ � ���������� � ���
			public $t1             = array();   # ������ �������
			public $t2             = array();   # ������ �������
			public $t1list         = array();   # ������ ������ �������
			public $t2list         = array();   # ������ ������ �������
			public $tools          = array();   # ��������� ���

			public $texit          = array();   # ������� ��������
			public $team_enemy     = array();   # ������� ���������� (������ �� �����)
			public $team_mine      = array();   # ���� �������

  			public $user           = array();   # ���� �� ������
			public $enemyhar       = array();   # ���� �� ����������

  			public $raz_user_left  = array();   # ���� �� ������     � �������
			public $raz_user_right = array();   # ���� �� ���������� � �������

			public $enemy_dress    = array();   # ���� �����
			public $user_dress     = array();   # ����  ����
			public $en_class, $my_class;        # ����� ��� ����
			public $bots           = array ();
			public $log            = "";        # ���������� ����
			public $battle_log     = "";        # ���������� ����
			public $battle_log_id  = 0;         # ���������� ���� ��
			public $to1;                        # ������� ������ �������
            public $to2;                        # ������� ������ �������
            public $upd_required   = 0;         # ��������� ����������
			public $exp            = array();   # �����
			public $statis         = array();   # ���������� ��� �������

			public $battle_mod     = false;     # ���� ��������� � ��� ��� ����������

    /***-------------------------------------
     * �������� ������ � ���� �������� ����
     **/

	function __construct ($battle_id = 0) {
	 global $user_my, $user, $_POST, $hit_hod, $hit_hp, $hit_udar, $hit_block, $hit_uvorot, $hit_krit, $mysql_access;

	    $this->user = $user_my->user;
	    $upd_battle = '';

	    # ���������� ��������
	      if ($battle_id > 0) {

              $this->battle_log_id  = date("His").rand(1, 10); # ���������� ���� ��

	        # C����� ������ ����� �� 1 "���� �����"
			  $this->status = 1;
			# ��������� �����������
			  $this->battle_data = sql_row("SELECT * FROM `battle` WHERE `id` = '".$battle_id."' LIMIT 1;");
              $this->log_channel = $this->battle_data['log_channel'];

		    # ��������� ���
		   	  $this->tools = unserialize($this->battle_data['tools']);

            # ������ ������� ���
              $this->t1list = unserialize($this->battle_data['t1list']);
              $this->t2list = unserialize($this->battle_data['t2list']);

			  $this->sort_teams($user_my->user['id']);

			# ��� ���������?
			  $this->battle = unserialize($this->battle_data['teams']);
			  $this->set_new_teams();

    		# ������ � ��������� ���
    		  $this->battle_eff = unserialize($this->battle_data['battle_eff']);
    		# ������ � ������� ���
    		  $this->b_hint_nbp = unserialize($this->battle_data['b_hint_nbp']);

		   # �������� ���������� ���
		     $this->statis = unserialize($this->battle_data['statis']);

		   # ���� ����
			 $this->to1 = $this->battle_data['to1'];
			 $this->to2 = $this->battle_data['to2'];

              if (in_array($this->battle_data['type'], array(30))) {

                            # �������� �������
        					  $this->enemy = (int)$this->select_enemy();

        						if($this->enemy > 0) {
        						  $this->return = 1; # �������� ����� - �����
        					   	} else {
        							 if ($this->get_timeout() && $this->user['hp'] > 0) {
        								$this->return = 3; # �������� �������
        						     } else {
        								$this->return = 2; # ������� ����...
        						     }
        						}


               // echo "ccccccc";
                $this->return = 1;
                return $this->return;
              } else {

            # ����� � ����� ������� ��� ����
			  if($this->log_channel == 'l') { $upd_battle .= "`log_channel` = 'l".$battle_id."', "; }

	        # ���� ���� ��� �������� � ��� � �� ��� � ��� �� ���������� �� ����
	          if ($this->battle_data['win'] < 3 && (time() - $this->battle_data['to1']) > 5 && $user_my->user['battle']) {
	            sql_query("UPDATE `users` SET `battle` = 0 WHERE `id` = '".$user_my->user['id']."' LIMIT 1;");
	            $user_my->user['battle'] = 0;
	          }

            # ����� � ����� ������� ��� ����
			  if($this->battle_data['room'] <= 0) { $upd_battle .= "`room` = '".$user_my->user['room']."', "; }

            # ����� ������� ��������� ���
              if ($hit_hod == 0 && !empty($this->tools['b_rooms'])) {
                if (empty($this->tools['b_rooms'])) @$this->tools['b_rooms'] = array();
                if (empty($this->tools['b_rooms']) || !in_array($user_my->user['room'], $this->tools['b_rooms'])) {
                   if ($user_my->user['room']) $this->tools['b_rooms'][] = $user_my->user['room'];
                      $upd_battle .= "`tools` = '".serialize($this->tools)."', ";
                }
              }

           # ��������� ���
             if ($upd_battle) {
                sql_query("UPDATE `battle` SET $upd_battle `to1` = '".time()."', `to2` = '".time()."' WHERE `id` = '".$battle_id."' LIMIT 1;");
             }

                    if ($this->battle_data['win'] == 3) {

                       # ���� ���������� ���
                         if ($this->battle_data['auto_completion'] > 0 && $this->battle_data['auto_completion'] < time()) {
                           $this->auto_completion();
                         }

                       # �������� ����������
                         if ($this->get_points('i', $user_my->user['id']) && empty($user_my->user['invis'])
                         || $user_my->user['hp'] != $this->get_points('h', $user_my->user['id'])
                         || $user_my->user['mana'] != $this->get_points('p', $user_my->user['id'])
                         || empty($this->get_points('id', $user_my->user['id']))) {

                          if (empty($this->get_points('id', $user_my->user['id']))) {

                           # ������� �������� ��������� �� �������������
                             $nick_l = nick_ofset($user_my->user, 1, 1);
                             $nick_l['wep_type'] = '';
                             $nick_l['shit']     = $user_my->user['shit']?1:0;
                             $nick_l['exp']      = 0;
                             $nick_l['damage']   = 0;
                             $nick_l['karma']    = 0;
                             $nick_l['valor']    = 0;
                             $nick_l['pM']       = 0;

                           # ����������� ���-���
                      	     if (in_array ($user_my->user['id'], $this->t1))
                                  $this->t1list[$user_my->user['id']] = $nick_l;
                      	     else $this->t2list[$user_my->user['id']] = $nick_l;


                          } else {

                            $this->set_points('i', $user_my->user['invis'], $user_my->user['id']);

                            $this->set_points('h', $user_my->user['hp'], $user_my->user['id']);
                            $this->set_points('mh', $user_my->user['maxhp'], $user_my->user['id']);

                            $this->set_points('p', $user_my->user['mana'], $user_my->user['id']);
                            $this->set_points('mp', $user_my->user['maxmana'], $user_my->user['id']);
                          }
                          $this->update_battle("���������� ������ �����");
                         }


                       #----------------------------------------------------------------------------------------------
                       # ������� �����
    				     $bots = sql_query('SELECT * FROM `bots` WHERE `battle` = "'.$battle_id.'" AND hp > 0');

                         if (num_rows($bots) > 0) {
    					  while ($bot = fetch_array($bots)) {
                            $this->bots[$bot['id']] = $bot;

                            if (!empty($bot['stats'])) {
                             $stats = unserialize($bot['stats']);

                              if (!empty($stats['no_user'])) {
                               $this->bots[$bot['id']]['guild']      = $stats['guild'];
                               $this->bots[$bot['id']]['align']      = $stats['align'];
                               $this->bots[$bot['id']]['level']      = $stats['level'];
                               $this->bots[$bot['id']]['sex']        = $stats['sex'];
                               $this->bots[$bot['id']]['incity']     = $stats['incity'];
                               $this->bots[$bot['id']]['klan']       = $stats['klan'];
                               $this->bots[$bot['id']]['invis']      = $stats['invis'];
                               $this->bots[$bot['id']]['shit']       = $stats['shit'];

                               $this->bots[$bot['id']]['weap']       = $stats['weap'];
                               $this->bots[$bot['id']]['obraz']      = $stats['obraz'];
                               $this->bots[$bot['id']]['bothidro']   = $stats['bothidro'];
                               $this->bots[$bot['id']]['bothran']    = $stats['bothran'];

                               $this->bots[$bot['id']]['stuff_cost'] = $stats['stuff_cost'];
                               $this->bots[$bot['id']]['KBO']        = $stats['KBO'];
                              }
                            }

                          }
                         }

                       # �������� HP � ����� ���� ���� ����
                         if (empty($this->bots)) {
                           if (in_array($user_my->user['id'], $this->t1)) {
                             foreach ($this->t2list as $k => $v_data) {
                               if ($v_data['id'] > _BOTSEPARATOR_  && $v_data['h'] > 0) {
                                 sql_query("UPDATE `bots` SET `hp` = '".$v_data['h']."' WHERE `id` = '".$v_data['id']."' LIMIT 1;");
                               }
                             }
                           } else {
                             foreach ($this->t1list as $k => $v_data) {
                               if ($v_data['id'] > _BOTSEPARATOR_  && $v_data['h'] > 0) {
                                 sql_query("UPDATE `bots` SET `hp` = '".$v_data['h']."' WHERE `id` = '".$v_data['id']."' LIMIT 1;");
                               }
                             }
                           }
                         }

                     # ��� ������ �������� ���� � ��� ����� ��� �� �������  "i:15024;a:0:{}"
                       if ($user_my->user['hp'] > 0 && (empty($this->battle[$user_my->user['id']]) || count($this->battle[$user_my->user['id']]) == 0)) {
                         $this->check_breach_integrity_battle();
                       }

                       #----------------------------------------------------------------------------------------------
                       # ��������� �������
                         if (@$_GET['cmd'] == 'priem' && $_GET['act'] || (@$_POST['cmd'] == "priem.target" && @$_POST['name'])) {

                         # ������ � ������� ����
                           if (@$_POST['cmd'] == "priem.target") {
                            $target_user = sql_row("SELECT * FROM `users` WHERE `login` = '".@$_POST['name']."' AND `battle` = '".$this->battle_data['id']."' AND `bot` = 0 AND `invis` = 0 LIMIT 1;");
                            $_GET['act'] = $_POST['act'];
                           }

                           $priems = unserialize($user_my->user['actions']);
                           $priem  = @$priems[$_GET['act']];

                           $gn_str_mind = abs($priem['n_str_mind']);

                           if (empty($priem['id'])) {
                              $_SESSION['battle_hint'] = '����� ����� �� ������ '.$priem['id'];
                           } elseif (!is_activete_priem($user_my, $this, $priem)) {
                              $_SESSION['battle_hint'] = '�� �� ������ ���� ������������ ���� �����';
                           } elseif ($_SESSION['priem_time'] > time()) {
                              $_SESSION['battle_hint'] = '������ ��� ����� ������������ ������ '.($_SESSION['priem_time'] - time());
                           } elseif (empty($target_user['id']) && @$_POST['cmd'] == "priem.target") {
                              $_SESSION['battle_hint'] = "���� �� �������";
                           } elseif ($target_user['bot'] && @$_POST['cmd'] == "priem.target") {
                              $_SESSION['battle_hint'] = "�������� ������ ����";
                           } elseif ($this->battle_data['type'] == 27 || $this->battle_data['type'] == 29) {
                              $_SESSION['battle_hint'] = "������ ������������ ������ � ��������";
                           } elseif ($this->battle_data['type'] == 31) {
                              $_SESSION['battle_hint'] = "������ ������������ ������ � ��� � ��������";
                           } elseif ($gn_str_mind > $user_my->user['str_mind']) {
                              $_SESSION['battle_hint'] = "������������ ���� ����";
                           } elseif ($user_my->user['hp'] <= 0) {
                              $_SESSION['battle_hint'] = "�� ��� ���������";
                           } else {
                              if ($user_my->user['sex'] == 1) {$mesex="";} else {$mesex="�";}
                      	    // $this->add_log(nick5($user_my->user['id'],$this->my_class).' �����������'.$mesex.' ����� <b>"'.$priem['name'].'"</b>');
                              include("./magic/priem/".$priem['priem'].".php");

                     	      sql_query("UPDATE `battle` SET `to1` = '".time()."', `to2` = '".time()."' WHERE `id` = ".$user_my->user['battle']." LIMIT 1;");
                              $_SESSION['priem_time']  = time() + 20;
                              $_SESSION['battle_hint'] = '����� <b>"'.$priem['name'].'"</b> �����������';
                             // $this->update_battle("��������� �������");
                             // if ($this->battle_end()) { $this->return = 2; }
                             // header ("Location:fbattle.php"); die();
                           }
                         }

                       #----------------------------------------------------------------------------------------------
                       # �������� ������ ���� ����� ��������� ���������
                       # �������� ����� ���
                       $tyrn = 1;
                         if ($tyrn && $this->user['hp'] > 0 && $this->get_timeout()/* && ($this->battle_data['timeout'] > 1 || $this->battle_data['status_battle'] > 0)*/) {

                             foreach ($this->team_enemy as $v => $k_id) {

                             # �� ���������
                               $enemy_id = $k_id;

                               if (!in_array($k_id, $this->texit)) { # �� ��� ����� �� ���
                                  if($k_id > _BOTSEPARATOR_) {

                                   $itout = time() - ($this->battle[$enemy_id][$this->user['id']][3] + (abs(count(abs(count($this->battle[$enemy_id]))))*60));

                                   if ($itout > ($this->battle_data['timeout']*60)
                                   && $this->battle[$enemy_id][$this->user['id']][0]
                                   && $this->user['hp'] > 0 && $this->get_points('h', $enemy_id) > 0) {

                                       if (rand(1, 100) >= 90) { # ����
                                          $bot = $this->bots[$enemy_id];
                                          if (isset($bot['id'])) { $this->spare_no($bot); break; } # ��� �� �����
                                       } else {
                                         $battle_razmen_init = $this->razmen_init(
                                         $enemy_id,
                                         $this->user['id'],
                                         rand(1, 4), # ����
                                         rand(1, 4), # ����
                                         rand(1, 2), # �������
                                         '',
                                         $this->user['id']);

                                         if ($battle_razmen_init) {
                                            $this->battle = $battle_razmen_init;
              					   	        $this->write_log();   # ����� ���
                        	                sql_query("UPDATE `battle` SET `timeout` = `timeout` - 1 WHERE `id` = '".$this->battle_data['id']."' AND `timeout` > 1 LIMIT 1;");
                                            $this->battle_data['timeout'] -= 1;
                                         }
                                       }

                                   }

                                  } else {
                                      $itout = time() - ($this->battle[$this->user['id']][$enemy_id][3] + (abs(count(abs(count($this->battle[$this->user['id']]))))*60));
                                      /*echo abs(count($this->team_enemy)).'<br>';
                                      echo abs(count($this->battle[$this->user['id']])).'<br>';

                                      echo $this->user['id'].'<br>';
                                      echo $enemy_id.'<br>';

                                      echo $this->battle[$this->user['id']][$enemy_id][3]." hhhhhhhhhhhhhhhhhhhhh ".$itout.'<br>';
                                      echo $this->battle[$this->user['id']][$enemy_id][0].'<br>';
                                      echo ($this->battle_data['timeout']*60).'<br>';*/

                                      if (($itout > $this->battle_data['timeout']*60)
                                      && $this->battle[$this->user['id']][$enemy_id][0]
                                      && $this->get_points('h', $enemy_id) > 0) {

                                         $battle_razmen_init = $this->razmen_init(
                                         $this->user['id'],
                                         $enemy_id,
                                         $this->battle[$this->user['id']][$enemy_id][0], # ����
                                         $this->battle[$this->user['id']][$enemy_id][1], # ����
                                         $this->battle[$this->user['id']][$enemy_id][2], # �������
                                         '',
                                         $enemy_id);

                                         if ($battle_razmen_init) {
                                            $this->battle = $battle_razmen_init;
              					   	        $this->write_log();   # ����� ���
                        	                sql_query("UPDATE `battle` SET `timeout` = `timeout` - 1 WHERE `id` = '".$this->battle_data['id']."' AND `timeout` > 1 LIMIT 1;");
                                            $this->battle_data['timeout'] -= 1;
                                         }
                                      }
                                  }
                               }
                             }
                         }

             /*if ($user_my->user['id'] == 1) {
              echo "<pre>";
              print_r($this->battle);
              echo "</pre>";
              }*/

                       #----------------------------------------------------------------------------------------------
                       # ���������� ����� ��������� ������� ���� �� ����������
                         if (@$_POST['cmd'] == 'turn' || @$_POST['act'] == 'bturn') {

                           $_SESSION['battle_hint'] = "";
                           $ablock = 1;

                           if ($user_my->user['shit']
                           && !(
                              @$_POST['D1'] && @$_POST['D2']
                           || @$_POST['D2'] && @$_POST['D3']
                           || @$_POST['D4'] && @$_POST['D4']
                           || @$_POST['D1'] && @$_POST['D4']
                           || @$_POST['D2'] && @$_POST['D4']
                           || @$_POST['D1'] && @$_POST['D3']
                           )) {
                              $_SESSION['battle_hint'] = "���������� ������� 2 �����";
                              $ablock = 0;
                           }

                           if (!$user_my->user['shit']
                           && (
                              @!$_POST['D1']
                           && @!$_POST['D2']
                           && @!$_POST['D3']
                           && @!$_POST['D4']
                           )) {
                              $_SESSION['battle_hint'] = "���������� ������� 2 �����";
                              $ablock = 0;
                           }

                           if ((@$_POST['A1'] || @$_POST['A2'] || @$_POST['A3'] || @$_POST['A4'])) {
                              $audar = 1;
                           } else {
                              $_SESSION['battle_hint'] = "���������� ������� 1 ����";
                              $audar = 0;
                           }

                           if (!empty($user_my->user['bot_battle_panel'])) {
                             $ablock = 1;
                             $audar = 1;

                             if (empty($_POST['pos'])) $_POST['pos'] = 2;
                             //$_POST['pos'] = 2; // ���� ��� � ������ �� ������ � ������ �����

                             $_SESSION['battle_hint'] = "";
                           }

                         }

                       $turn = 1;

                       #----------------------------------------------------------------------------------------------
                       # ��������� ������� ���� �� ����������
                         if($turn && (@$_POST['cmd'] == 'turn' || @$_POST['act'] == 'bturn') && @$_POST['enemy'] > 0 && ((@$_POST['pos']==1 || @$_POST['pos']==2) || @$_POST['enemy'] > _BOTSEPARATOR_) && @$_POST['attack'] && @$_POST['defend'] && $ablock && $audar) {

                           if (@$_SESSION['turnstop'] < time()) {   # ��������� �� ������� ������ �� ���
    					    # ���������
                              if ($turn) {
                               $turn = 0;
                               unset($_POST['cmd']);
    						   $battle_razmen_init = $this->razmen_init ($user_my->user['id'], $_POST['enemy'], $_POST['attack'], $_POST['defend'], $_POST['pos']);

                               if ($battle_razmen_init) {
                                  $this->battle = $battle_razmen_init;
    					   	      $this->write_log();   # ����� ���
                               }
                              }
                           }

                         }
        				    # ���� ��� �������� ������ ����� � ���
                              $this->status_battle();
        					# $this->sort_teams($user_my->user['id']);
                            # �������� �������
        					  $this->enemy = (int)$this->select_enemy();

        						if($this->enemy > 0) {
        						  $this->return = 1; # �������� ����� - �����
        					   	} else {
        							 if ($this->get_timeout() && $this->user['hp'] > 0) {
        								$this->return = 3; # �������� �������
        						     } else {
        								$this->return = 2; # ������� ����...
        						     }
        						}

                       #---------------
      				   # ������� ����� ����� �� ����
                          if (count($this->bots) > 0) {
                           $need_upd = false;
                           foreach ($this->bots as $k_id => $bot) {

      					  # ������� �����������, � ���������� ����� ��� ����� ����������
      						if($bot['hp'] > 0) {

                                 if (@$this->battle[$bot['id']]) {

                                      $att_array = array(1,2,3,4);
                                      $def_array = array(1,2,3,4,5,6);
                                      $pos_array = array(0, 1, 2);

                                      $turn = true;

                                      if (!empty($this->battle[$user_my->user['id']][$bot['id']][0]) && $this->battle[$user_my->user['id']][$bot['id']][0] > 0
                                      && (!empty($this->battle[$bot['id']][$user_my->user['id']][3]) && $this->battle[$bot['id']][$user_my->user['id']][3] < time())
                                      && (in_array($this->battle[$user_my->user['id']][$bot['id']][0], $att_array))
                                      && (in_array($this->battle[$user_my->user['id']][$bot['id']][1], $def_array))
                                      && (in_array($this->battle[$user_my->user['id']][$bot['id']][2], $pos_array))
                                      && $turn
                                      && empty($need_upd)
                                      ) {

                                      $bot_attact = mt_rand(1,4);   # ����� 1-4
                                      $bot_block  = mt_rand(1,6);   # ����� � �����

                                         $bot_pos = 0;
                                         if ($bot['is_hbot']) {
                                           if (mt_rand(1,100) > 60) $bot_pos = 1; else $bot_pos = 2;
                                         }

      					                 $this->battle[$bot['id']][$user_my->user['id']] = array($bot_attact, $bot_block, $bot_pos, time());
                                         if ($turn) {
                                           $turn = 0;

                                           $battle_razmen_init = $this->razmen_init($user_my->user['id'],
                                                                                    $bot['id'],
                                                                                    $this->battle[$user_my->user['id']][$bot['id']][0],
                                                                                    $this->battle[$user_my->user['id']][$bot['id']][1],
                                                                                    $this->battle[$user_my->user['id']][$bot['id']][2],
                                                                                    'razmbot');

                                           if (@$battle_razmen_init) {
                                              $this->battle = $battle_razmen_init;
                					   	      $this->write_log();   # ����� ���
                                              $need_upd = true;
                                           }
                                         }

                                            # ������� �������
                        					  $this->enemy = (int)$this->select_enemy();
                        					  if($this->enemy > 0) {
                        						  $this->return = 1; # �������� ����� - �����
                        					  } else {
                        					      if ($this->get_timeout() && $this->user['hp'] > 0) {
                        					        $this->return = 3; # �������� �����
                        						  } else {
                        						    $this->return = 2; # ������� ����...
                        					      }
                        					  }
                                      }
                                 }
      						}
                           }

                             if ($need_upd) {
                              # ���� ��� �� ������ �������� ����� � ��� ������
                                if (empty($this->enemy)) $this->return = 2; else $this->return = 1;
                             }

                          }


    					if (@$_GET['cmd'] == 'timeout') {

                          # ������ �����
    				    	if (@$_GET['status'] == 'draw') {
    						    $this->end_draft($user_my->user);
                                $this->update_battle("timeout");
                                $this->battle_end($user_my->user['battle']);
    					    }

                          # �������� �����������
    					    if (@$_GET['status'] == 'win') {
    					    	$this->spare_no($user_my->user);
                                $this->update_battle("timeout");
                                $this->battle_end($user_my->user['battle']);
     					    }
                        }

                      $this->fast_death();
                      $this->update_battle("������� ����������");
                      if ($this->battle_end($user_my->user['battle'])) { $this->return = 0; }
  					  $this->write_log();   # ����� ���
                    }

               if (empty($this->return)) { $this->return = 0; }
		       return $this->return;
              }
	      } else {
	        # ������ ������ ����� �� "��� �����"
	          $this->status = 0;
	      }
	}

  #-----------------------------------------------
  # �������� ������
    function __destruct() {
      global $mysql;
      //
      if ($this->battle_mod  == true) {

      }
    }

/***------------------------------------------
 * ���������� ����� ����� ������
 **/

    function get_data_teams($us_id = 0, $en_id = 0) {
      $team_data = array();
      foreach ($this->battle as $k_id => $v_data) {
        foreach ($v_data as $k => $v) { $team_data = $v; break; }
      }
      return $team_data;
    }

/***------------------------------------------
 * ���� ���������� ���
 **/

function auto_completion(){

 $team_ids     = '';
 $team_bot_ids = '';
 $team_1_exp   = 0;
 foreach ($this->t1list as $k_id => $v_data) $team_1_exp += $v_data['exp'];

 $team_2_exp = 0;
 foreach ($this->t2list as $k_id => $v_data) $team_2_exp += $v_data['exp'];

 $this->add_log('���� ���������� ���');

 # ������� ���������� ���������� ����� "�����"
   if ($team_1_exp == $team_2_exp) {
    foreach ($this->t1list as $k_id => $v_data) {
      $this->t1list[$k_id]['h'] = 0;
      if ($k_id > _BOTSEPARATOR_)
           $team_bot_ids .= $team_bot_ids ? ','.$k_id : $k_id;
      else $team_ids     .= $team_ids ? ','.$k_id : $k_id;
      unset($this->battle[$k_id]);
    }

    foreach ($this->t2list as $k_id => $v_data) {
      $this->t2list[$k_id]['h'] = 0;
      if ($k_id > _BOTSEPARATOR_)
           $team_bot_ids .= $team_bot_ids ? ','.$k_id : $k_id;
      else $team_ids     .= $team_ids ? ','.$k_id : $k_id;
      unset($this->battle[$k_id]);
    }

  # �������� �����
    if ($team_ids) sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0 WHERE `id` IN (".$team_ids.");");
    if($team_bot_ids) sql_query("UPDATE `bots` SET `hp` = 0 WHERE `id` IN (".$team_bot_ids.");");
   } else
 # �������� ������ �������
   if ($team_1_exp > $team_2_exp) {
    foreach ($this->t2list as $k_id => $v_data) {
      $this->t2list[$k_id]['h'] = 0;
      if ($k_id > _BOTSEPARATOR_)
           $team_bot_ids .= $team_bot_ids ? ','.$k_id : $k_id;
      else $team_ids     .= $team_ids ? ','.$k_id : $k_id;
      unset($this->battle[$k_id]);
    }

  # �������� �����
    if($team_ids) sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0 WHERE `id` IN (".$team_ids.");");
    if($team_bot_ids) sql_query("UPDATE `bots` SET `hp` = 0 WHERE `id` IN (".$team_bot_ids.");");
   } else
 # �������� ������ �������
   if ($team_2_exp > $team_1_exp) {
    foreach ($this->t1list as $k_id => $v_data) {
      $this->t1list[$k_id]['h'] = 0;
      if ($k_id > _BOTSEPARATOR_)
           $team_bot_ids .= $team_bot_ids ? ','.$k_id : $k_id;
      else $team_ids     .= $team_ids ? ','.$k_id : $k_id;
      unset($this->battle[$k_id]);
    }

  # �������� �����
    if($team_ids)     sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0 WHERE `id` IN (".$team_ids.");");
    if($team_bot_ids) sql_query("UPDATE `bots` SET `hp` = 0 WHERE `id` IN (".$team_bot_ids.");");
   }
 $this->update_battle('���� ���������� ���');
}

/***------------------------------------------
 * ���������� ����� ����� ������
 **/

    function set_new_teams($t_new = false) {
      if ($t_new == true) {
        return $this->battle;
      }
      /*if ($this->battle_data['new_teams'] == 0 || $t_new) {
        $this_teams = array();
          foreach ($this->battle as $k_id => $v_data) {
             $sdsf = '';
              foreach ($v_data as $k => $v) {
               $sdsf .= $k.'='.$v[0].','.$v[1].','.$v[2].','.$v[3].'|';
              }
              $this_teams[$k_id] = $sdsf;
          }
        if ($t_new == false) sql_query("UPDATE `battle` SET `teams` = '".serialize($this_teams)."', new_teams = 1 WHERE `id` = '".$this->battle_data['id']."' LIMIT 1;"); else return $this_teams;
      } else {
        $this_teams = array();
        foreach ($this->battle as $k_id => $v_data) {
         $vmas_data = explode("|", $v_data);
          if (!empty($vmas_data)) {
            foreach ($vmas_data as $k => $v) {
              $vmas = explode("=", $v);
              $vmas_d = explode(",", $vmas[1]);
              if (abs($vmas[0]) > 0 && $vmas_d[3] > 0) {
                $this_teams[$k_id][$vmas[0]] = array($vmas_d[0], $vmas_d[1], $vmas_d[2], $vmas_d[3]);
              }
            }
          }
        }
        $this->battle = $this_teams;
      }*/
    }

/***------------------------------------------
 * ��������:
 * exp
 * damage
 * karma
 * valor
 **/

    function add_points($type, $value = 0, $us_id) {

     if ($type == 'karma') {
       $value = $value;      # ������������� ���������� ������� ����� number_format($value, 4,".","");
     } elseif ($type == 'h' && $value < 0) {
       $value = 0;           # ���� �� ������ ���� �� ������ ����
     } else {
       $value = abs(number_format($value, 4,".",""));
     }

      if (!empty($this->t1list[$us_id])) {
         if (empty($this->t1list[$us_id][$type])) {
          $this->t1list[$us_id][$type] = $value;
         } else {
          $this->t1list[$us_id][$type] += $value;
         }
      } else {
         if (empty($this->t2list[$us_id][$type])) {
          $this->t2list[$us_id][$type] = $value;
         } else {
          $this->t2list[$us_id][$type] += $value;
         }
      }
    }

/***------------------------------------------
 * ��������:
 * exp
 * damage
 * karma
 * valor
 **/

    function set_points($type, $value = 0, $us_id) {
      if (!empty($this->t1list[$us_id])) {
        $this->t1list[$us_id][$type] = $value;
      } elseif (!empty($this->t2list[$us_id])) {
        $this->t2list[$us_id][$type] = $value;
      }
    }

/***------------------------------------------
 * ����������:
 * exp
 * damage
 * karma
 * valor
 **/

    function get_points($type, $us_id) {
      if (!empty($this->t1list[$us_id])) {
        if (empty($this->t1list[$us_id][$type])) return 0;
        else return $this->t1list[$us_id][$type];
      } elseif (!empty($this->t2list[$us_id])) {
        if (empty($this->t2list[$us_id][$type])) return 0;
        else return $this->t2list[$us_id][$type];
      }
     return 0;
    }

/***-----------------------------------------
 * ����������� ����������� ���
 **/

    function check_breach_integrity_battle(){
     global $user_my;

       $save          = false;
       $this_teams    = $this->battle;
       $data_users_t1 = array();
       $data_users_t2 = array();

         # ---------------------------------
         # ������ ����� � ������ �������
           foreach ($this->t1 as $k => $v_id) {
            $battle_user = $this->t1list[$v_id];
            if ($v_id > _BOTSEPARATOR_) {
                $battle_user = sql_row("SELECT `id`, `hp` FROM `bots` WHERE `id` = '".$v_id."' LIMIT 1;");
                $battle_user['h'] = $battle_user['hp'];
            } else {
                $battle_user = sql_row("SELECT `id`, `hp` FROM `users` WHERE `id` = '".$v_id."' LIMIT 1;");
                $battle_user['h'] = $battle_user['hp'];
            }
            if (!empty($battle_user['id']) && $battle_user['h'] > 0) $data_users_t1[$battle_user['id']] = $battle_user;
           }

         # ---------------------------------
         # ������ ����� � ������ �������
           foreach ($this->t2 as $k => $v_id) {
            $battle_user = $this->t2list[$v_id];
            if ($v_id > _BOTSEPARATOR_) {
                $battle_user = sql_row("SELECT `id`, `hp` FROM `bots` WHERE `id` = '".$v_id."' LIMIT 1;");
                $battle_user['h'] = $battle_user['hp'];
            } else {
                $battle_user = sql_row("SELECT `id`, `hp` FROM `users` WHERE `id` = '".$v_id."' LIMIT 1;");
                $battle_user['h'] = $battle_user['hp'];
            }
            if (!empty($battle_user['id']) && $battle_user['h'] > 0) $data_users_t2[$battle_user['id']] = $battle_user;
           }

            # ---------------------------------
            # ������ ������ �������
               foreach ($data_users_t1 as $k => $v_data) {
                  $u_id = @$v_data['id'];
                  if (!empty($u_id) && (empty($this->battle[$u_id]) || count($this->battle[$u_id]) == 0 || count($this->battle[$u_id]) != $this->count_att($u_id) )) {
                    foreach ($data_users_t2 as $k => $v) {
                      $j_id = $v['id'];
                      if (in_array($j_id, $this->t2) && in_array($u_id, $this->t1)) {
                        $this_teams[$u_id][$j_id] = array(0,0,0,time());
      	                $this_teams[$j_id][$u_id] = array(0,0,0,time());
                      }
                    }
                   $save = true;
                  }
               }

            # ---------------------------------
            # ������ ������ �������
               foreach ($data_users_t2 as $k => $v_data) {
                  $u_id = @$v_data['id'];
                  if (!empty($u_id) && (empty($this->battle[$u_id]) || count($this->battle[$u_id]) == 0 || count($this->battle[$u_id]) != $this->count_att($u_id) )) {
                    foreach ($data_users_t1 as $k => $v) {
                      $j_id = $v['id'];
                      if (in_array($j_id, $this->t1) && in_array($u_id, $this->t2)) {
                        $this_teams[$u_id][$j_id] = array(0,0,0,time());
      	                $this_teams[$j_id][$u_id] = array(0,0,0,time());
                      }
                    }
                   $save = true;
                  }
               }

         if ($user_my->user['id'] == 1 || $user_my->user['id'] == 17) {
           foreach ($this_teams as $k => $v) { echo $user_my->user['id']." -- ".serialize($v).'<br>'; }
         }

         # ---------------------------------
         # ���������
           if ($save) {
            # sql_query("UPDATE `battle` SET `teams` = '".serialize($this_teams)."' WHERE `id` = '".$this->battle_data['id']."' LIMIT 1;");
            $this->battle = $this_teams;
            #addchp ($user_my->user['id'].'<b> ������ � ��� � �������</b> <a href="/log='.$this->battle_data['id'].'" target="_blank">���</a>  '.serialize($this_teams), "�����");
            return true;
           }

      return false;
    }

/***------------------------------------------
 * ����� � ��� ���� ���������
 **/

    function add_write_drop($winer_user, $itemname, $bots, $itimg = 0, $img = '') {
      if ($itimg) {
       $wrimg = "'".$itemname."'";
      } else {
       $wrimg = '<img alt="'.$itemname.'" class="tooltip" src="http://img.blutbad.ru/i/'.$img.'" title="'.$itemname.'" style=" vertical-align: middle; ">';
      }

      if($winer_user['sex'] == 0) {$sex="a";}else{$sex="";}
      $sdrop = nick_team($winer_user, $this->userclass($winer_user['id']))." �������".$sex." ������� ".$wrimg."";

      $this->add_log($sdrop.' � <font class="'.$this->userclass($bots['id']).'">'.$bots['name'].'</font> ');
      $this->write_log ();
      CHAT::chat_attention($winer_user, '�� �������� ������� "'.$itemname.'" � <b>'.$bots['name'].'</b>', array('addXMPP' => true));
    }

  # ������ ����� ���������
    function add_drop($itn, $itemrnd, $winer_user, $bots) {}

    function add_drop_item($itn_name, $itemrnd, $winer_user, $bots, $ex = 0) {

      $rnd = rand(1, 100);
      switch($itn_name){
       case '��������� +25':
       case '�������������� +25':
       case '��������� +50':
       case '�������������� +50':
       case '��������� +100':
       case '�������������� +100':
       case '��������� +250':
       case '�����������':

       case '��������� � ��������':
       case '��������� � ��������':

       case '������������':
       case '��������� +100':
       case '��������� +250':
       case '����� �� ���':
       case '������� ��������� +500':
       case '����� ����':
       case '���� ����':
       case '����� ����':
       case '���� ����':

     # ex

       case '��������� +100':
       case '������� ��������� +500':
       case '�������������� +100':
       case '��������� +250':
       case '�������������� +250':
       case '������������':

            if (!empty($itn_name) && $rnd <= $itemrnd) {

             if ($ex) {

                $item = sql_row("SELECT * FROM `berezka` WHERE `name` = '".$itn_name."' LIMIT 1;");
                if ($item['id']) {
                  $item['podzem']     = 1;
                  $item['ecost']      = 0;
                  $item['foronetrip'] = 1;
                  $item['goden']      = 30;

                  fun_create_shop_exclusive($item, $winer_user['id']);
                  $this->add_write_drop($winer_user, $itn_name, $bots, 0, $item['img']);
                }

             } else {

                $item = sql_row("SELECT * FROM `shop` WHERE `name` = '".$itn_name."' LIMIT 1;");
                if ($item['id']) {
                  $item['podzem']     = 1;
                  $item['cost']       = 0;
                  $item['foronetrip'] = 1;
                  $item['goden']      = 30;

                  fun_create_shop($item, $winer_user['id']);
                  $this->add_write_drop($winer_user, $itn_name, $bots, 0, $item['img']);
                }

             }

            }

       break;

       case '������� -������� �������-':
       case '������� -������-':
       case '������� -����-':
       case '������� -�����-':
       case '������� -���-':
       case '������� -�������-':
       case '������� -������-':
       case '������� -��������-':
       case '������� -����-':
       case '������� -������-':
       case '������� -�����-':
       case '������� -����� ������-':
       case '������� -�������-':
       case '������� -�������-':
       case '������� -����� ������-':
       case '������� -�����-':
       case '������� -�������-':
       case '������� -���-':
       case '������� �� ��� ����':

            if (!empty($itn_name) && $rnd <= $itemrnd) {
              $item = sql_row("SELECT * FROM `shop` WHERE `name` = '".$itn_name."' LIMIT 1;");
              if ($item['id']) {
                $item['cost']       = 0;
                $item['foronetrip'] = 1;
                fun_create_shop($item, $winer_user);
                $this->add_write_drop($winer_user, $itn_name, $bots, 1, $item['img']);
              }
            }

       break;

      }
    }


/***--------------------------
 * ��������� ������ � ���
 **/

    function getudarintlog($pos, $type_pos) {
    # $pos == 1 - �����
    # $pos == 2 - ������
    # $pos == 0 - ��� �������
    $e_pos = '.';
        if ($pos == 1) { # ������� �����
            switch($type_pos){
             case 'udar':   $e_pos = '2';break;
             case 'uvorot': $e_pos = '1';break;
             case 'block':  $e_pos = '0';break;
             case 'krit':   $e_pos = '3';break;
             case 'krita':  $e_pos = '4';break;
            }
        }
        if ($pos == 2) { # ������� ������
            switch($type_pos){
             case 'udar':   $e_pos = '7';break;
             case 'uvorot': $e_pos = '6';break;
             case 'block':  $e_pos = '5';break;
             case 'krit':   $e_pos = '8';break;
             case 'krita':  $e_pos = '9';break;
            }
        }
        if ($pos == 0) { # ��� �������
            switch($type_pos){
             case 'udar':   $e_pos = 'c';break;
             case 'uvorot': $e_pos = 'b';break;
             case 'block':  $e_pos = 'a';break;
             case 'krit':   $e_pos = 'd';break;
             case 'krita':  $e_pos = 'e';break;
            }
        }
     return $e_pos;
    }

/***--------------------------
 * ������ �� ��������� ��������
 *
 * $type = ����� ���� ����� (������ ���� ���)
 * $uron = �� ������� �������
 * $udar_in = ���� ����
 * $udar_pos = ������� �����
 **/

    function set_user_statis($us_stat, $type, $hetype, $uron=0, $ataka=0, $pos=0, $defend=0){

     $us_id = $us_stat['id'];

    if (empty($this->statis[$us_id])) { $this->statis[$us_id] = fun_new_statis($us_stat); }

      if ($ataka && $type) {
        $udar_in1 = '.';$udar_in2 = '.';$udar_in3 = '.';$udar_in4 = '.';
        $udar_protection1 = '.';$udar_protection2 = '.';$udar_protection3 = '.';$udar_protection4 = '.';

           switch($ataka){
               case 1: $udar_in1 = $this->getudarintlog($pos, $type); break; # udars = ���������� ������
               case 2: $udar_in2 = $this->getudarintlog($pos, $type); break; # udars = ���������� ������
               case 3: $udar_in3 = $this->getudarintlog($pos, $type); break; # udars = ���������� ������
               case 4: $udar_in4 = $this->getudarintlog($pos, $type); break; # udars = ���������� ������
           }

            $this->statis[$us_id][1][0] = $this->statis[$us_id][1][0].$udar_in1; # udars = ���������� ������
            $this->statis[$us_id][1][1] = $this->statis[$us_id][1][1].$udar_in2; # udars = ���������� ������
            $this->statis[$us_id][1][2] = $this->statis[$us_id][1][2].$udar_in3; # udars = ���������� ������
            $this->statis[$us_id][1][3] = $this->statis[$us_id][1][3].$udar_in4; # udars = ���������� ������

            if($this->get_points('shit', $us_stat['id'])) {
                 if ($defend==1 || $defend==4 || $defend==6) {$udar_protection1 = $this->getudarintlog($pos, $hetype);}
                 if ($defend==1 || $defend==2 || $defend==5) {$udar_protection2 = $this->getudarintlog($pos, $hetype);}
                 if ($defend==2 || $defend==3 || $defend==6) {$udar_protection3 = $this->getudarintlog($pos, $hetype);}
                 if ($defend==3 || $defend==4 || $defend==5) {$udar_protection4 = $this->getudarintlog($pos, $hetype);}
            } else {
                 if ($defend==1) {$udar_protection1 = $this->getudarintlog($pos, $hetype);}
                 if ($defend==1) {$udar_protection2 = $this->getudarintlog($pos, $hetype);}
                 if ($defend==2) {$udar_protection3 = $this->getudarintlog($pos, $hetype);}
                 if ($defend==3) {$udar_protection4 = $this->getudarintlog($pos, $hetype);}
            }

            $this->statis[$us_id][2][0] = $this->statis[$us_id][2][0].$udar_protection1; # udars = ���������� ������
            $this->statis[$us_id][2][1] = $this->statis[$us_id][2][1].$udar_protection2; # udars = ���������� ������
            $this->statis[$us_id][2][2] = $this->statis[$us_id][2][2].$udar_protection3; # udars = ���������� ������
            $this->statis[$us_id][2][3] = $this->statis[$us_id][2][3].$udar_protection4; # udars = ���������� ������
      }

         if ($uron) {
             $this->statis[$us_id][5][0] += $uron; # uron
              if ($type == 'krit' || $type == 'krita') {
                 $this->statis[$us_id][5][1] += $uron; # krituron
              }
         }
    }

/***------------------------------------------
 * ������� ������ ���������� ���
 **/

    function upd_battle_err($battle_err){
    // sql_query("UPDATE `battle` SET `battle_err` = '".serialize($battle_err)."' WHERE `id` = '".$this->battle_data['id']."' AND `battle_err` != '".serialize($battle_err)."' LIMIT 1;");
    }


/***--------------------------
 * �������� � ���������� ���
 **/

    function battle_end ($battle_id = 0) {
     global $user_my, $caverooms, $base_mods, $lim_exp, $cur_battleid, $baseexp;

      if (empty($battle_id) && $user_my->user['battle'] > 0) { $battle_id = $user_my->user['battle']; }
      if (empty($battle_id)) { $battle_id = $cur_battleid; }

       if ($battle_id) {

      # ��������� ����������

         # ������ �����
    	 #  $fighters = @array_keys($this->battle);

            $flag        = 2;
        	$t1life      = 0;
        	$t2life      = 0;
        	$allvragdead = 1;

    		  # ��������� �������� ������ �������
    			foreach ($this->t1 as $k => $v) {
                    # �� ��� ����� �� ���
    				  if (!in_array($v, $this->texit) && $this->t1list[$v]['h'] > 0) { $t1life++; }
    			}

    		  # ��������� �������� ������ �������
    			foreach ($this->t2 as $k => $v) {
    		        # �� ��� ����� �� ���
    				  if (!in_array($v, $this->texit) && $this->t2list[$v]['h'] > 0) { $t2life++; }
    			}
                                                       $statusbattle_end  = 0;   # ��� ��� ����
               if ($t1life > 0 && $t2life <= 0)      { $statusbattle_end  = 1; } # �������� ������ �������
               elseif ($t2life > 0 && $t1life <= 0)  { $statusbattle_end  = 2; } # �������� ������ �������
               elseif ($t2life <= 0 && $t1life <= 0) { $statusbattle_end  = 3; } # �����

                    switch($statusbattle_end){

                     case 0: # ��� ���������� ��� ��� ����
                     break;

                     case 1: # �������� 1� �������
                        $win_team  = $this->t1;
                        $lose_team = $this->t2;
                     break;

                     case 2: # �������� 2� �������
                        $win_team  = $this->t2;
                        $lose_team = $this->t1;
                     break;

                     case 3: # �����
                        $nich_team = array_merge ($this->t1, $this->t2);
                        $win_team  = $nich_team;
                        $lose_team = $nich_team;
                     break;
                    }

      /*
       battle_end = 0 = ��� ����
       battle_end = 1 = ��� ������ ���������
       battle_end = 2 = ��� ���� ���
       battle_end = 3 = ��� ��������
      */

     # �������� ������ ��� ������ ��� ����� �������
       if ($statusbattle_end == 1 || $statusbattle_end == 2 || $statusbattle_end == 3) {

        $charge_win = sql_row('SELECT `id`, `win`, `battle_end` FROM `battle` WHERE `id` = '.$this->battle_data['id'].' LIMIT 1;');

           if ($allvragdead && !empty($charge_win['id']) && $charge_win['win'] == 3 && ($charge_win['battle_end'] == 0 || $charge_win['battle_end'] == 2)) {

        	 if (sql_query("UPDATE `battle` SET `battle_end` = 1 WHERE `id` = '".$this->battle_data['id']."' AND `battle_end` != 3 LIMIT 1;")) {
               $charge_win['battle_end']        = 1;
               $this->battle_data['battle_end'] = 1;
             }

            if ($charge_win['battle_end'] == 1) {

             $battle_err = array();
             $this->battle_data['win']        = $statusbattle_end;
             $battle_err['win']               = $statusbattle_end;
             $battle_err['status_start']      = 'error';
             $battle_err['status_start_1']    = 'error';
             $battle_err['status_start_2']    = 'error';
             $battle_err['status_modif_2']    = 'error';
             $battle_err['status_modif_1']    = 'error';
             $battle_err['status_modif_2']    = 'error';
             $battle_err['status_win_status'] = 'error';
             $battle_err['status_lose_team']  = 'error';
             $battle_err['status_ends1']      = 'error';

             $this->upd_battle_err($battle_err);

                $winers = '';
        	    $flag = $statusbattle_end;

                $team1 = array();
                $nks1  = array();

                $team2 = array();
                $nks2  = array();

                # ������ ����������� � ������ � ��� ���������� ��� ��� ������ �������
                  foreach ($this->t1 as $k => $v) {
                   if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���
                     $party_user['id']    = $this->get_points('id', $v);
                     $party_user['invis'] = $this->get_points('i', $v);
                     $party_user['login'] = $this->get_points('n', $v);
                   # ������ ������� ��� ���������� ���
                     $team1[$k] = nick_team($party_user, "B1");   # nick5($v, 'B1');
                   # ������ ������� ��� � ��������� ����
                     $nks1[]    = $party_user['invis']?"���������":$this->get_points('n', $v); # n = login or ���������;
                   }
                  }

                # ������ ����������� � ������ � ��� ���������� ��� ��� ������ �������
                  foreach ($this->t2 as $k => $v) {
                   if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���
                     $party_user['id']    = $this->get_points('id', $v);
                     $party_user['invis'] = $this->get_points('i', $v);
                     $party_user['login'] = $this->get_points('n', $v);
                
                   # ������ ������� ��� ���������� ���
                     $team2[$k] = nick_team($party_user, "B2");  # nick5($v, 'B1');
                   # ������ ������� ��� � ��������� ����
                     $nks2[]    = $party_user['invis']?"���������":$this->get_points('n', $v); # n = login or ���������;
                   }
                  }

             $battle_err['status_start'] = 'ok';
             $this->upd_battle_err($battle_err);

            if ($statusbattle_end == 1 || $statusbattle_end == 2) { # �������� ������ ��� ������ �������

           ###########################################################
           #  ������� �����������
           ###########################################################

              foreach ($win_team as $k => $v) { # ����������

        	   $flag = $statusbattle_end;

               if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���

        	     sql_query("UPDATE `battle` SET `win` = '".$statusbattle_end."' WHERE `id` = '".$this->battle_data['id']."' LIMIT 1;");
                 $this->battle_data['win'] = $statusbattle_end;

                  #------------------------------------
                  # ��� ��� ����������
                    if ($this->battle_data['type'] == 12 && $v > _BOTSEPARATOR_) {
                      include("functions/battle/fbattle.endhran.php");
                    }

                    if($v < _BOTSEPARATOR_) {

        			  $user_win = sql_row("SELECT `id`, `level`, `login`, `login_jaber`, `animals`, `incity`, `klan`, `ps`, `room`, `sex`, `invis`, `battle`, `battles_today`, `win`, `win_next`, `dungeon`, `achievements_finish`, `thematic_event`, `rep`, `zside`, `zarnica`, `eff_all`, `shadow`, `quest_all` FROM `users` WHERE `id` = '".$v."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");

                      if (!empty($user_win['id'])) {

                       #---------------------------
                       # ������� % ����� �� ���
                         $battle_exp_percent = empty($this->tools['exp_percent'])?0:$this->tools['exp_percent'];
                         $exp_percent = percent_exp($user_win, $this->battle_data) + $battle_exp_percent;

                       #---------------------------
                       # ���������s�� ���� �� ���
                         $earned_damage = floor($this->get_points('damage', $user_win['id']));
                         if (empty($earned_damage)) { $earned_damage = 0; }

                       #---------------------------
                       # ����������� ���� �� ���
                         $earned_exp = floor($this->get_points('exp', $user_win['id']) / 100 * $exp_percent);
                         $this->set_points('exp', $earned_exp, $user_win['id']);
                         if (empty($earned_exp) || $earned_exp < 0) { $earned_exp = 0; $this->set_points('exp', 0, $user_win['id']); }

                       #---------------------------
                       # ����������� ����� �� ���
                         $earned_karma = round($this->get_points('karma', $user_win['id']), 2);
                         $this->set_points('karma', $earned_karma, $user_win['id']);
                         if (empty($earned_karma)) { $earned_karma = 0; }

                       #---------------------------
                       # ������������ ������� "���� ������"
                         $thematic_event = unserialize($user_win['thematic_event']);
                         if (!empty($thematic_event['name']) && $thematic_event['name'] == "���� ������") {
                          $t_earned_valor = $this->get_points('valor', $user_win['id']);
                          $this->set_points('valor', ($t_earned_valor + ($t_earned_valor * 50 / 100)), $user_win['id']);
                         }

             $battle_err['status_start_1'] = 'ok';
             $this->upd_battle_err($battle_err);

                       #---------------------------
                       # ����������� �������� �� ���

                       # � ��������� ��� ���� ������ ��������
                         if ($this->battle_data['hideb'] == 1) $hideb = 2; else $hideb = 1;

                         $earned_valor = round((($this->get_points('valor', $user_win['id']) / 100 * $exp_percent) * $hideb), 2);

                       # ������� ������� ���� ������ ��������
                              if ($user_win['id'] <= 7)  $earned_valor *= 3;
                         else if ($user_win['id'] <= 10) $earned_valor *= 2;

                         $this->set_points('valor', $earned_valor, $user_win['id']);
                         if (empty($earned_valor)) { $earned_valor = 0; }

                       # ��� ��������
                         if (!empty($this->tools['no_valor']) || $user_win['dungeon']) {
                          $earned_valor = 0;
                          $this->set_points('valor', 0, $user_win['id']);
                         }

                       #---------------------------
                       # �������� ������ ��������
                         if ($earned_valor > 500) {
                           $earned_valor = 500;
                           $this->set_points('valor', 500, $user_win['id']);
                           send_user_letter(1, '[���������� ��������]', $user_win['login'].' ����� ����� �������', 'battle_id '.$this->battle_data['id'].', earned_valor '.$earned_valor.', valor <b>'.$this->valor[$user_win['id']].'</b> ');
                         }

                              if (
                                 $this->battle_data['type'] != 20    # ����� ��� "���������"
                              && $this->battle_data['type'] != 25    # ����� ��� "������ �����������"
                              && $this->battle_data['type'] != 27    # ����� ��� "��������� ������"
                              && empty($this->tools['no_exp_limit']) # ��� ������ �� �����
                              ) {
                                # ���� �������� ������ ��� ��������� ����� �� ������ �����
                                  if($earned_exp > $lim_exp[$user_win['level']]){
                                    $earned_exp = $lim_exp[$user_win['level']];
                                    $this->set_points('exp', $earned_exp, $v);
                                  }
                              }

                            # ��� ������� ������� ������ ��������� 30 ����� �� ���
                              if ($user_win['level'] == 0) {
                                $earned_exp = 30;
                                $this->set_points('exp', $earned_exp, $v);
                              }

                            # �������� �����
                              if ($this->battle_data['type'] == 14 || $this->battle_data['type'] == 17) {
                               # ���� �� �������� ��� �����
                                 $earned_exp = 0;
                                 $this->set_points('exp', $earned_exp, $v);
                              }

                                #-----------------------------
                                # ��� ������ = ��� ��������
                                  include ("functions/battle/specialwin.php");

             $battle_err['status_start_2'] = 'ok';
             $this->upd_battle_err($battle_err);

                                #---------------------------------------------
                                # ����� � ��� ��� ���
            			          CHAT::chat_attention($user_win, '<a href="http://'.$user_win['incity'].'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>���</b></a> ��������. ����� ���� �������� �����: <b>'.$earned_damage.'</b>. �������� �����: <b>'.$earned_exp.' ('.$exp_percent.'%).</b> �����: <b>'.$earned_karma.'</b>. ��������: <b>'.$earned_valor.'</b>.', array('addXMPP' => true));

                                  $reit_klan_iswar = "";

                                #---------------------------------------------
                                # ���� �������� � ����� �� ����� � ��� ��� � ���� �������� �����
                                  if ($user_win['klan']) {
                                   if ($earned_valor != 0) {
                                     $klan_valor = $earned_valor;
                                     CHAT::chat_attention($user_win, '�������� �������� ��������:  <b>'.$klan_valor.'</b>.', array('addXMPP' => true));
        						     sql_query("UPDATE `clans` SET `clanexp` = `clanexp` + ".$klan_valor." WHERE `name` = '".$user_win['klan']."' LIMIT 1;");
                                   }

                                   # ����� ���������� ���� ��� ��� ��������
                                     if ($this->battle_data['type'] == 14) {
                                       $reit_klan_iswar = ' `reit_klan_war_wins` = `reit_klan_war_wins` + 1, ';
                                     }
                                  }

                                #---------------------------------------------
                                # ���������� "���������� �����"
                                  if (!in_array($user_win["room"], $caverooms) && !$user_win["dungeon"]) {
                                    if (!in_array("winner_25000", explode(";", $user_win['achievements_finish']))) {
                                      achievements($user_win, array('winner_total' => array(1 => 1)));
                                    }
                                  }

                                #---------------------------------------------
                                # ����� ��������
                                  sql_query("UPDATE `enterlog` SET

                                    $reit_klan_iswar

                                        `reit_exp` = `reit_exp` + ".$earned_exp.",
                                        `reit_karma` = `reit_karma` + ".$earned_karma.",
                                        `reit_valor` = `reit_valor` + ".$earned_valor."

                                  WHERE `owner` = '".$user_win['id']."' AND `type` = 0 LIMIT 1;");

                                    #-----------------------------------------------
        			                # ���� ���� ����� �� ����� � ��� � ���� ��� ����
                                     if (!empty($user_win['animals']) && $user_win['animals'] != 'a:0:{}') {
        			                  if($earned_exp > 0) {
                                         $animals = unserialize($user_win['animals']);

                                           foreach ($animals as $k => $v_anim) {
                                              if ($v_anim['war'] && @$v_anim['battle'] == $user_win['battle']) {

                      			                 $my_animal_exp = floor($earned_exp / rand(3, 5));
                        			             if($my_animal_exp)
                                                   $animals[$v_anim['name']]['battle'] = 0;
                                                   $animals[$v_anim['name']]['exp']   += $my_animal_exp;

                      			                   CHAT::chat_attention($user_win, '��� �����'.(($v_anim['animal_login'] != $v_anim['name'])?" �� ������ <b>".$v_anim['animal_login'].'</b>':" <b>".$v_anim['name']."</b>").' ������� <b>'.$my_animal_exp.'</b> �����.', array('addXMPP' => true));
                                              }
                                           }
                        			     sql_query("UPDATE `users` SET `animals` = '".serialize($animals)."' WHERE `id` = '".$user_win['id']."' LIMIT 1;");
        			                  }
                                     }

             $battle_err['status_modif_1'] = 'ok';
             $this->upd_battle_err($battle_err);

                                    #-----------------------------
                                    # ��������� ����
                                      include("functions/battle/fbattle.holiday.php");

                                    #-----------------------------
                                    # ��� ������� ��������
                                	  include ("functions/battle/battle.drop_object.php");

                                    #-----------------------------
                                    # ������ ������ � ������ � ���
                                      add_chronicle_in_battle($user_win);

                                    #-------------------------------
                                    # �������� �����
                                      if ($user_win['ps'] > 0) {
                                        $ps = '`ps` = `ps` + 1, ';
                                      } elseif ($user_win['ps'] <= 0) {
                                        $ps = '`ps` = 1, ';
                                      }

                                    # ��� �������� �����
                                      if (!empty($this->tools['no_ps'])) { $ps = ''; }

                                    #-------------------------------
                                    # ���� ���������� ����� �� ������ � ���
                                      $chances_wheel = '';
                                      if ($user_win['battles_today'] == 0) {
                                         $chances_wheel = '`chances_wheel` = `chances_wheel` + 1, ';
              			                 CHAT::chat_attention($user_win, '�� ������ � ������ ��� �� ������� �� ��������� �������������� ����� ���������� �������������� ������� "������ �����"!', array('addXMPP' => true));
                                      }

                                   #-------------------------------
                                   # ���������� �������� ��� ������
                                   # �������� ���� �� ���
                                   # �������� �� ���
        				             sql_query("UPDATE `users` SET $ps $chances_wheel
                                     `hit_hod` = 0, `hit_hp` = 0, `hit_udar` = 0, `hit_block` = 0, `hit_uvorot` = 0, `hit_krit` = 0,
                                     `win` = `win` + 1, `fullhptime` = ".time().", `fullmptime` = ".time().",
                                     `exp` = `exp` + '".$earned_exp."', `karma` = `karma` + '".$earned_karma."', `valor` = `valor` + '".$earned_valor."',
                                     `battle` = 0, `battles_today` = `battles_today` + 1
                                      WHERE `id` = '".$user_win['id']."' LIMIT 1;");

                                       if ($user_my->user['id'] == $user_win['id']) { $user_my->user['battle'] = 0; }

             $battle_err['status_modif_2'] = 'ok';
             $this->upd_battle_err($battle_err);

                                     }

                                   } else {

                                      # ��� ��� �����
                                        $user_bot = sql_row("SELECT `name`, `prototype`, `is_hbot`, `is_hranbot` FROM `bots` WHERE id = '".$v."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                                        if (!empty($user_bot['prototype'])) {
                                          # �������� �� ���
                                            if ($user_bot['is_hbot'] || $user_bot['is_hranbot']) {
        			                          sql_query("UPDATE `users` SET
                                              `win` = `win` + 1, `fullhptime` = ".time().", `fullmptime` = ".time().",
                                              `battle` = 0
                                               WHERE id = '".$user_bot['prototype']."' LIMIT 1;");
                                            }

                                        }
                                   }
               }
              }

        /***-------------------------------
         * 1. �������� �����
         * 2. ����� ��������� ����� "��� ����������� ��������"
         **/

             include("functions/battle/fbattle.klan_war.php");

        /***-------------------------------
         * 1. �������
         * 2. ����� ��������� ���
         **/

            if ($this->battle_data['type'] == 28) {
             include("functions/battle/fbattle.zarnica.php");
            }

        /***-------------------------------
         * 1. ��������� ����
         **/

            if ($this->battle_data['type'] == 35) {
             include("functions/battle/fbattle.distribution.php");
            }

             $this->write_log();

             $endlog = "��� ��������, ������ �� ";

              # ��������� ��������� ��� � ���� ������� ����� � ���
                if (!empty($win_team) && $this->battle_data['status_battle'] > 0) {
                  $win_exp = array();
                   foreach ($win_team as $k => $v) {
                     $win_exp[$v] = $this->get_points('exp', $v);
                   }

                     $end_str_boy = '';
                     $end_str_sysboy = '';
                     $m_money1 = 10; $m_money2 = 5; $m_money3 = 3;

                       $made = '��� ����������';
                       switch($this->battle_data['status_battle']){
                         case 1: $m_stl_money1 = 0;    $m_stl_money2 = 0;    $m_stl_money3 = 0;    $m_money1 = 10;   $m_money2 = 6;   $m_money3 = 3;   $made = '������ ������� �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>������ �������</b></a> �����������'; break; # ������ �������!
                         case 2: $m_stl_money1 = 300;  $m_stl_money2 = 150;  $m_stl_money3 = 75;   $m_money1 = 100;  $m_money2 = 50;  $m_money3 = 25;  $made = '������ ������������ �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>������ ������������</b></a> �����������'; break; # ������ ������������
                         case 3: $m_stl_money1 = 700;  $m_stl_money2 = 750;  $m_stl_money3 = 175;  $m_money1 = 200;  $m_money2 = 100; $m_money3 = 50;  $made = '��������� ����� �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>��������� �����</b></a> �����������'; break; # ��������� �����!
                         case 4: $m_stl_money1 = 1500; $m_stl_money2 = 750;  $m_stl_money3 = 375;  $m_money1 = 500;  $m_money2 = 250; $m_money3 = 125; $made = '�������������� �������� �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>�������������� ��������</b></a> �����������'; break; # �������������� ��������!
                         case 5: $m_stl_money1 = 4000; $m_stl_money2 = 2000; $m_stl_money3 = 1000; $m_money1 = 1000; $m_money2 = 500; $m_money3 = 250; $made = '������������ �������� �����������'; $madesys = '<a class="sys" href="http://'.INCITY.'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>������������ ��������</b></a> �����������'; break; # ������������ ��������!
                       }

                     $end_str_bla = '��������� ������ ������� �����';

                     # ������  �����
                       $max_uron = null;
                       $win_user = null;
                       $maxkey = 0;
                       foreach ($win_exp as $key_id => $val_ur) {  if ($val_ur > $max_uron || is_null($max_uron)) { $max_uron = $val_ur; $maxkey = $key_id; } }
                       $pervi_max_uron = $max_uron;

                       if ($maxkey > _BOTSEPARATOR_) {

                            if (empty($this->bots[$maxkey]))
                                 $win_bot = sql_row('SELECT `id`, `name`, `prototype` FROM `bots` WHERE `id` = "'.$maxkey.'" LIMIT 1;');
                            else $win_bot = $this->bots[$maxkey];

                         $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex, achievements_finish, rewards FROM `users` WHERE `id` = '{$win_bot['prototype']}' LIMIT 1;");
                         $win_user['id'] = $win_bot['id'];
                         $win_user['login'] = $win_bot['name'];
                         $win_user['battle'] = $battle_id;
                         $isus = 0;
                       } else { $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex, achievements_finish, rewards FROM `users` WHERE `id` = '{$maxkey}' LIMIT 1;"); $isus = 1; }

                       #
                         if ($m_baseexp[$win_user['level']] > $pervi_max_uron) { # ������ ������ ������ �������� �����
                            $pervi_max_uron = 0;
                         } else {

                           if ($win_user['id']) {
                             if ($win_user['invis']==1) {
                                $end_str_boy .= "<i class='user".($this->userclass($win_user['id'], '')+2)."'>���������</i>";
                             } else {
                                //$end_str_boy .= "<script type=\"text/javascript\">u(\"".$win_user['login']."\",\"".$win_user['level']."\",\"".($win_user['guild']?"":$win_user['align'])."\",\"".($win_user['klan']?str_to_spacelower($win_user['klan']):"")."\",\"".$win_user['klan']."\",\"".($win_user['guild']?'paladins'.$win_user['align']:"")."\",\"".$win_user['guild']."\",\"".($win_user['sex']?"M":"F")."\",\"".$this->userclass($win_user['id'], '')."\",\"%alt%\",0,0,1,".$win_user['top'].");</script>";
                                $end_str_boy .= nick_ofgethtml(nick_ofset($win_user, 0, $this->userclass($win_user['id'], '')), 0, 0);
                             }

                            $end_str_sysboy = _GetUserChatName($win_user);
                              if ($isus) {
                               $sql_rewards = "";

                                #-------------------------------------
                                # ���������� "������ �� ������"
                                  $status_battle = $this->battle_data['status_battle'];
                                  $hideb = $this->battle_data['hideb'];
                                  $no_foodbatl = $this->battle_data['type'] == 20 ? false : true; # ��� ���������

                                  $statused = "";

                                   switch($status_battle){
                                     case 1: $statused = "������ �� ������ - ������ �������";          break; # ������ �������!
                                     case 2: $statused = "������ �� ������ - ������ ������������";     break; # ������ ������������
                                     case 3: $statused = "������ �� ������ - ��������� �����";         break; # ��������� �����!
                                     case 4: $statused = "������ �� ������ - �������������� ��������"; break; # �������������� ��������!
                                     case 5: $statused = "������ �� ������ - ������������ ��������";   break; # ������������ ��������!
                                   }

                                   if ($statused) {
                                     if (!in_array("fight_".$status_battle, explode(";", $win_user['achievements_finish']))) {
                                       achievements($win_user, array('statused_'.$status_battle => array(1 => 1)));

                                       #---------------------------------------------
                                       # ���������� "����������� ����"
                                         achievements($win_user, array('statused_6' => array(1 => array($statused))));
                                     }

                                   # ������ ������������
                                     if ($status_battle == 2) {
                                      $rewards = unserialize($win_user['rewards']);

                                      $n_rewards['name'] = '������ ������������!';
                                      $n_rewards['type'] = 'military_clashes';
                                      $n_rewards['date_create'] = time();

                                      $rewards[] = $n_rewards;
                                      $sql_rewards = ", `rewards` = '".serialize($rewards)."'";
                                      send_user_letter($win_user['id'], '[�������������]', "������� �� ������ ����� � ������ ������������", "��� ���� ������ ������� ��� ������� ����� � <b>������ ������������</b>");
                                     }

                                   # ��������� �����
                                     if ($status_battle == 3) {
                                      $rewards = unserialize($win_user['rewards']);

                                      $n_rewards['name'] = '��������� �����!';
                                      $n_rewards['type'] = 'local_battle';
                                      $n_rewards['date_create'] = time();

                                      $rewards[] = $n_rewards;
                                      $sql_rewards = ", `rewards` = '".serialize($rewards)."'";
                                      send_user_letter($win_user['id'], '[�������������]', "������� �� ������ ����� � ��������� �����", "��� ���� ������ ������� ��� ������� ����� � <b>��������� �����</b>");
                                     }

                                   # ������� �� ������� ����� � ��������� ���
                                     if ($hideb == 1 && isset($m_stl_money1) && $no_foodbatl) {
                                        $sql_rewards = ", `ekr` = `ekr` + '".$m_stl_money1."'";
                                     }
                                   }

                               sql_query("UPDATE `users` SET `money` = `money` + '".$m_money1."' ".$sql_rewards." WHERE `id` = '".$win_user['id']."' LIMIT 1;");
                               send_user_letter($win_user['id'], '[�������������]', "�� ������ ������ �������� ����� ����� ������ � ���", $madesys." ������� ��������� ������ �������, ������� �������� ������� <b>".$m_money1."</b> ��.".(($hideb == 1 && isset($m_stl_money1) && $no_foodbatl)?", ".$m_stl_money1." ���.":""));
                               add_private_matter($win_user['id'], 0, '�� ������ ������ �������� ����� ����� ������ � ���'. $madesys.' ������� ��������� ������ �������, ������� �������� ������� <b>'.$m_money1.'</b> ��.'.(($hideb == 1 && isset($m_stl_money1) && $no_foodbatl)?", ".$m_stl_money1." ���.":""), '[���]');

                              }
                           }
                         }

                     # ������ �����
                       $sql_rewards = "";
                       $win_user = null;
                       $max_uron = null;
                       $maxkey = 0;
                       foreach ($win_exp as $key_id => $val_ur) { if ($pervi_max_uron != $val_ur && ($val_ur > $max_uron || is_null($max_uron))) { $max_uron = $val_ur; $maxkey = $key_id; } }
                       $vtoroy_max_uron = $max_uron;
                       $vtoroy_persent = 0;

                        if ($vtoroy_max_uron > 0 && $pervi_max_uron > 0) {
                         $vtoroy_persent = floor($vtoroy_max_uron * 100 / $pervi_max_uron);
                        }

                         if ($vtoroy_persent >= 90) {

                           if ($maxkey > _BOTSEPARATOR_) {

                            if (empty($this->bots[$maxkey]))
                                  $win_bot = sql_row('SELECT id, name, prototype FROM `bots` WHERE `id` = "'.$maxkey.'" LIMIT 1;');
                            else  $win_bot = $this->bots[$maxkey];

                             $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex FROM `users` WHERE `id` = '{$win_bot['prototype']}' LIMIT 1;");
                             $win_user['id'] = $win_bot['id'];
                             $win_user['login'] = $win_bot['name'];
                             $win_user['battle'] = $battle_id;
                             $isus = 0;
                           } else { $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex FROM `users` WHERE `id` = '{$maxkey}' LIMIT 1;"); $isus = 1;}

                             if ($m_baseexp[$win_user['level']] > $vtoroy_max_uron) { # ������ ������ ������ �������� �����
                                $vtoroy_max_uron = 0;
                             } else {
                                 if ($win_user['id']) {

                                   if ($win_user['invis']) {
                                      $end_str_boy .= ", <i class='user".($this->userclass($win_user['id'], '')+2)."'>���������</i>";
                                   } else {
                                     // $end_str_boy .= ", <script type=\"text/javascript\">u(\"".$win_user['login']."\",\"".$win_user['level']."\",\"".($win_user['guild']?"":$win_user['align'])."\",\"".($win_user['klan']?str_to_spacelower($win_user['klan']):"")."\",\"".$win_user['klan']."\",\"".($win_user['guild']?'paladins'.$win_user['align']:"")."\",\"".$win_user['guild']."\",\"".($win_user['sex']?"M":"F")."\",\"".$this->userclass($win_user['id'], '')."\",\"%alt%\",0,0,1, ".$win_user['top'].");</script>";
                                      $end_str_boy .= ", ".nick_ofgethtml(nick_ofset($win_user, 0, $this->userclass($win_user['id'], '')), 0, 0);
                                   }

                                  $end_str_sysboy .= ', '._GetUserChatName($win_user);
                                  $end_str_bla = '��������� ������� ������ ������';

                                # ������� �� ������� ����� � ��������� ���
                                  if ($hideb == 1 && isset($m_stl_money2) && $no_foodbatl) {
                                     $sql_rewards = ", `ekr` = `ekr` + '".$m_stl_money2."'";
                                  }

                                  if ($isus) {
                                   sql_query("UPDATE `users` SET `money` = `money` + ".$m_money2." ".$sql_rewards." WHERE `id` = '".$win_user['id']."' LIMIT 1;");
                                   send_user_letter($win_user['id'], '[�������������]', "�� ������ ������ ����� ����� ������ � ���", $madesys." ������� ��������� ������ �������, ������� �������� ������� <b>".$m_money2."</b> ��.".(($hideb == 1 && isset($m_stl_money2) && $no_foodbatl)?", ".$m_stl_money2." ���.":""));
                                   add_private_matter($win_user['id'], 0, '�� ������ ������ ����� ����� ������ � ���', $madesys.' ������� ��������� ������ �������, ������� �������� ������� <b>'.$m_money2.'</b> ��.'.(($hideb == 1 && isset($m_stl_money2) && $no_foodbatl)?", ".$m_stl_money2." ���.":""), '[���]');
                                  }
                                 }
                             }
                         }

                     # ������ �����
                       $sql_rewards = "";
                       $win_user = null;
                       $max_uron = null;
                       $maxkey = 0;
                       foreach ($win_exp as $key_id => $val_ur) { if ($pervi_max_uron != $val_ur && $vtoroy_max_uron != $val_ur && ($val_ur > $max_uron || is_null($max_uron))) { $max_uron = $val_ur; $maxkey = $key_id; } }
                       $tretiy_max_uron = $max_uron;
                       $tretiy_persent = 0;

                        if ($vtoroy_max_uron > 0 && $pervi_max_uron > 0) {
                         $tretiy_persent = floor($tretiy_max_uron * 100 / $pervi_max_uron);
                        }

                         if ($tretiy_persent >= 80) {

                         if ($maxkey > _BOTSEPARATOR_) {

                            if (empty($this->bots[$maxkey]))
                                  $win_bot = sql_row('SELECT id, name, prototype FROM `bots` WHERE `id` = "'.$maxkey.'" LIMIT 1;');
                            else  $win_bot = $this->bots[$maxkey];


                           $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex FROM `users` WHERE `id` = '{$win_bot['prototype']}' LIMIT 1;");
                           $win_user['id'] = $win_bot['id'];
                           $win_user['login'] = $win_bot['name'];
                           $win_user['battle'] = $battle_id;
                           $isus = 0;
                         } else { $win_user = sql_row("SELECT id, login, top, battle, level, invis, guild, align, klan, sex FROM `users` WHERE `id` = '{$maxkey}' LIMIT 1;"); $isus = 1;}

                           if ($m_baseexp[$win_user['level']] > $tretiy_persent) { # ������ ������ ������ �������� �����
                              $tretiy_persent = 0;
                           } else {
                               if ($win_user['id']) {

                                   if ($win_user['invis']) {
                                      $end_str_boy .= ", <i class='user".($this->userclass($win_user['id'], '')+2)."'>���������</i>";
                                   } else {
                                     // $end_str_boy .= ", <script type=\"text/javascript\">u(\"".$win_user['login']."\",\"".$win_user['level']."\",\"".($win_user['guild']?"":$win_user['align'])."\",\"".($win_user['klan']?str_to_spacelower($win_user['klan']):"")."\",\"".$win_user['klan']."\",\"".($win_user['guild']?'paladins'.$win_user['align']:"")."\",\"".$win_user['guild']."\",\"".($win_user['sex']?"M":"F")."\",\"".$this->userclass($win_user['id'], '')."\",\"%alt%\",0,0,1, ".$win_user['top'].");</script>";
                                      $end_str_boy .= ", ".nick_ofgethtml(nick_ofset($win_user, 0, $this->userclass($win_user['id'], '')), 0, 0);
                                   }

                                $end_str_sysboy .= ', '._GetUserChatName($win_user);
                                $end_str_bla = '��������� ������� ������ ������';

                              # ������� �� ������� ����� � ��������� ���
                                if ($hideb == 1 && isset($m_stl_money3) && $no_foodbatl) {
                                   $sql_rewards = ", `ekr` = `ekr` + '".$m_stl_money3."'";
                                }

                                if ($isus) {
                                 sql_query("UPDATE `users` SET `money` = `money` + ".$m_money3." ".$sql_rewards." WHERE `id` = '".$win_user['id']."' LIMIT 1;");
                                 send_user_letter($win_user['id'], '[�������������]', "�� ������ ������ ����� ����� ������ � ���", $madesys." ������� ��������� ������ �������, ������� �������� ������� <b>".$m_money3."</b> ��.".(($hideb == 1 && isset($m_stl_money3) && $no_foodbatl)?", ".$m_stl_money3." ���.":""));
                                 add_private_matter($win_user['id'], 0, '�� ������ ������ ����� ����� ������ � ���', $madesys.' ������� ��������� ������ �������, ������� �������� ������� <b>'.$m_money3.'</b> ��.'.(($hideb == 1 && isset($m_stl_money3) && $no_foodbatl)?", ".$m_stl_money3." ���.":""), '[���]');
                                }
                               }
                           }
                         }

                   if (!empty($end_str_bla) && !empty($end_str_boy) && !empty($made)) {
                     $endlog = " ��� ��������. ".$end_str_bla." ".$end_str_boy." ".$made." ������� ";
                     if (!empty($madesys) && !empty($end_str_sysboy)) {
                       CHAT::chat_city_attention (false, $madesys.' �������! '.$end_str_bla.' '.$end_str_sysboy, array('addXMPP' => true));
                     }
                   }

                }

             $battle_err['status_win_status'] = 'ok';
             $this->upd_battle_err($battle_err);

           ###########################################################
           #  ������� �����������
           ###########################################################

              foreach ($lose_team as $k => $v) { # �����������

               #-----------------------------------------
        	   # ��� ��� ��� �������� ������ ���� ���...

        		  if (!in_array($v, $this->texit)) { # �� ��� ����� �� ���
                    if($v < _BOTSEPARATOR_) {
                       $user_v_lomka = sql_row("SELECT * FROM `users` WHERE `id` = '".$v."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                        if (!empty($user_v_lomka['id'])) {

                           $user_lomka = new UserControl($user_v_lomka);
                           $user_lomka->USER_load_inventory_data();
                           $user_lomka->del_cur_mysql = false;

                         #-------------------------------
                         # ������������ �������
                           $thematic_event = unserialize($user_v_lomka['thematic_event']);

                         #-------------------------------
                         # ������� % ����� �� ���
                           $exp_percent = percent_exp($user_v_lomka, $this->battle_data);

                         #-------------------------------
                         # ���������s�� ���� �� ���
                           $earned_damage = 0 + $this->get_points('damage', $user_v_lomka['id']);

                         #-------------------------------
                         # ����������� ���� �� ���
                           $old_earned_exp = $this->get_points('exp', $user_v_lomka['id']);
                           $this->set_points('exp', 0, $user_v_lomka['id']);
                           $earned_exp = 0;

                         #-------------------------------
                         # ��� ��������� ���� ��������� ��������
                           $old_valor = $this->get_points('valor', $user_v_lomka['id']);
                           $earned_valor  = 0;

                         # ������������ ������� "���� ������"
                           if (!empty($thematic_event['name']) && $thematic_event['name'] == "���� ������") {
                             $earned_valor -= round(sqrt((1 + $old_valor) * sqrt(1 + $old_earned_exp)), 2);
                           } else {
                         # � ��������� ��� �������� ������ ��������
                             $earned_valor -= round((sqrt((1 + $old_valor) * sqrt(1 + $old_earned_exp))) / ($this->battle_data['hideb'] == 1 ? 3 : 1), 2);
                          }

                           $this->set_points('valor', $earned_valor, $user_v_lomka['id']);

                         # ��� ��������
                           if (!empty($this->tools['no_valor'])) {
                            $earned_valor = 0;
                            $this->set_points('valor', 0, $user_v_lomka['id']);
                           }

                         #-------------------------------------------
                         # ������ ���� �������� � �����
                         # ���� ���� ����� ����� �� ����� = ��� ������ �������

                          if (empty($this->tools["break_items"]) && !substr_count($user_v_lomka['eff_all'], "1033;")) {  # ���� ��� ��������

                            $dur_ids = '';

                          # ������������ ������� "���� ������"
                            if (!empty($thematic_event['name']) && $thematic_event['name'] == "���� ������") {
                              $user_item_lomka = sql_query("SELECT `id`, `name`, `maxdur`, `duration`, `type`, `magic` FROM `inventory` WHERE `dressed` = 1 AND (`type` <> 25) AND `type` != 20 AND `owner` = '".$user_v_lomka['id']."';");
                            } else {
                              $user_item_lomka = sql_query("SELECT `id`, `name`, `maxdur`, `duration`, `type`, `magic` FROM `inventory` WHERE `dressed` = 1 AND (`type` <> 25) AND `owner` = '".$user_v_lomka['id']."';");
                            }

                            $dur = 1;
          				    while ($r_broken = fetch_array($user_item_lomka)) {
                               if (rand(1, 2) == 1 && (/*$r_broken['type'] != 20 && */$r_broken['type'] != 39 && $r_broken['type'] != 40 && $r_broken['type'] != 41 && $r_broken['type'] != 42)){
                                   $r_broken['duration'] += $dur;
                                   $r_type = type_dreass_item($r_broken, $user_v_lomka);

                                   if (!empty($user_lomka->inventory_data[$r_type]['id']) && $user_lomka->inventory_data[$r_type]['id'] == $r_broken['id']) {
                                    $user_lomka->inventory_data[$r_type]['duration'] = $r_broken['duration'];
                                    $user_lomka->mod_inventory_data = true;
                                    $user_lomka->USER_save_inventory_data();
                                   }

                                 if ($dur_ids) $coma = ','; else $coma = '';
                                 $dur_ids .= $coma.$r_broken['id'];

              				     if (fun_broken_item($r_broken) && $r_broken['type'] != 20){
              					  	$this->add_log('� '.nick_team($user_v_lomka, $this->userclass($v)).' ������� "'.$r_broken['name'].'" � ����������� ���������!');
                                	$this->write_log();
              					 }
                               }
          				    }

                             if ($dur_ids) {
                              sql_query("UPDATE `inventory` SET `duration` = `duration` + ".$dur." WHERE `owner` = '".$user_v_lomka['id']."' AND `id` in(".$dur_ids.") ;");
                             }
                          }

                         #-------------------------------------------
                         # ����������� ��� ������ ������ �����������
                           if($this->battle_data['blood'] == 1) { $tr = $user_lomka->USER_put_trauma(75); } elseif(CONST_ZARNICA == 0) { $tr = $user_lomka->USER_put_trauma(10); }
                           if (@$tr) {
                             $this->add_log(nick_team($user_v_lomka, $this->userclass($v)).$tr);
                             $this->write_log();
                           }

                         #-------------------------------
                         # ����� � ��� "���������"
        				   CHAT::chat_attention($user_v_lomka, '<a href="http://'.$user_v_lomka['incity'].'.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank"><b>���</b></a> ��������. ����� ���� �������� �����: <b>'.conv_hundredths($earned_damage, 0).'</b>. �������� �����: <b>0</b>. �����: <b>0.00</b>. ��������: <b>'.$earned_valor.'</b>.', array('addXMPP' => true));

                         #-------------------------------
                         # �������� �����
                          if ($user_v_lomka['ps'] >= 0) {
                            $ps = '`ps` = -1, ';
                          } elseif ($user_v_lomka['ps'] < 0) {
                            $ps = '`ps` = `ps` - 1, ';
                          }

                         # ��� �������� �����
                           if (!empty($this->tools['no_ps'])) { $ps = ''; }

                         #-------------------------------
                         # �������� ���� �� ���
                         # ���� �������� + ��� - � ���� ���������
                         # ������ �� � ����� �� 0
                         # �������� �� ���
                           sql_query("UPDATE `users` SET $ps
                           `hit_hod` = 0, `hit_hp` = 0, `hit_udar` = 0, `hit_block` = 0, `hit_uvorot` = 0, `hit_krit` = 0,
                           `valor` = `valor` + ".$earned_valor.", `lose` = `lose` + 1,
                           `hp` = 0, `hp_precise` = 0, `mana` = 0, `pw_precise` = 0, `fullhptime` = ".time().", `fullmptime` = ".time().",
                           `battle` = 0
                           WHERE `id` = '".$user_v_lomka['id']."' LIMIT 1;");

                           if ($user_my->user['id'] == $user_v_lomka['id']) {
                             $user_my->user['hp']     = 0;
                             $user_my->user['mana']   = 0;
                             $user_my->user['battle'] = 0;
                           }

                         #---------------------------------------------
                         # ���� �������� � ����� �� ����� � ��� ��� � ������� �����
                           if ($user_v_lomka['klan']) {
                             $klan_valor = $earned_valor;
                             CHAT::chat_attention($user_v_lomka, '�������� �������� ��������:  <b>'.$klan_valor.'</b>.', array('addXMPP' => true));
                             sql_query("UPDATE `clans` SET `clanexp` = `clanexp` + '".$klan_valor."' WHERE `name` = '".$user_v_lomka['klan']."' LIMIT 1;");

                           # ����� ���������� ���� ��� ��� �������� "��� �����"
                             if ($this->battle_data['type'] == 14) {
        				       sql_query("UPDATE `enterlog` SET
                               `klan_losewar_isday` = `klan_losewar_isday` + 1,
                               `klan_iswar_isday` = `klan_iswar_isday` + 1,
                               `klan_valor_isday` = `klan_valor_isday` + ".$klan_valor."
                               WHERE `owner` = '".$user_v_lomka['id']."' AND `type` = 0 LIMIT 1;");
                             } else {
                              sql_query("UPDATE `enterlog` SET `klan_valor_isday` = `klan_valor_isday` + ".$klan_valor." WHERE `owner` = '".$user_v_lomka['id']."' AND `type` = 0 LIMIT 1;");
                             }
                           }


                         #-----------------------------
                         # ��� ��������� = ��� �������
                           include("functions/battle/speciallose.php");

                        }

                    } else {

                      if (empty($this->bots[$v]))
                            $user_bot = sql_row("SELECT `name`, `prototype`, `is_hbot`, `is_hranbot` FROM `bots` WHERE `id` = '".$v."' LIMIT 1;");
                      else  $user_bot = $this->bots[$v];


                      if (!empty($user_bot['prototype'])) {

                        if ($user_bot['is_hbot'] || $user_bot['is_hranbot']) {
                          sql_query('UPDATE users SET `battle` = 0, `lose` = `lose` + 1 WHERE `id` = "'.$user_bot['prototype'].'" LIMIT 1;');
                        }

                      }

                    }
        		  }
              }

             $battle_err['status_lose_team'] = 'ok';
             $this->upd_battle_err($battle_err);

              # ������ �����������
                if ($statusbattle_end == 1) {
                   $winers .= implode(", ", $team1);
                } else {
                   $winers .= implode(", ", $team2);
                }

                $this->add_log($endlog.$winers);
                $this->write_log();

          #-----------------------------
          # ������ � ��������
            if (in_array($this->battle_data['type'], array(19, 20, 22, 29)) || $this->battle_data['hideb'] == 1) {
               include($_SERVER["DOCUMENT_ROOT"]."/functions/battle/ratings.php");
            }

            } elseif ($statusbattle_end == 3) {

           /***------------------------------------------
            * �����
            **/

             $battle_err['nich_start'] = 'ok';
             $this->upd_battle_err($battle_err);

             $battle_err['nich_write_log'] = 'ok';
             $this->upd_battle_err($battle_err);

              # ������� ������ � ������
        	    foreach ($nich_team as $v => $k) {
        		 if (!in_array($k, $this->texit)) { # �� ��� ����� �� ���
                  if($k > _BOTSEPARATOR_) {

                      if (empty($this->bots[$k]))
                            $user_bot = sql_row("SELECT `prototype`, `is_hbot`, `is_hranbot` FROM `bots` WHERE `id` = '".$k."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1 ;");
                      else  $user_bot = $this->bots[$k];

                      $user_nich = $user_bot;

                        if ($user_nich['is_hbot'] || $user_nich['is_hranbot']) {
                          sql_query("UPDATE `users` SET `battle` = 0, `nich` = `nich` + 1, `hp` = 0, `hp_precise` = 0, `fullhptime` = ".time().", `mana` = 0, `pw_precise` = 0, `fullmptime` = ".time()." WHERE `id` = '".$user_bot['prototype']."' LIMIT 1;");
                        }
                      sql_query('UPDATE `bots` SET `battle` = 0, `hp` = 0 WHERE `id` = "'.$k.'" LIMIT 1;');
                  } else {
                      $user_nich = sql_row("SELECT * FROM `users` WHERE id = '".$k."' LIMIT 1;");

                      $user_draw = new UserControl($user_nich);
                      $user_draw->USER_load_inventory_data();
                      $user_draw->del_cur_mysql = false;

                    #-------------------------------------------
                    # ������������ �������
                      $thematic_event = unserialize($user_nich['thematic_event']);

                    #-------------------------------------------
                    # ������������ ���� �� ���
                      $earned_damage = conv_hundredths($this->get_points('damage', $user_nich['id']), 0);
                      $this->set_points('damage', $earned_damage, $user_nich['id']);

                    #-------------------------------------------
                    # ����������� ����� �� ���
                      $earned_karma = conv_hundredths($this->get_points('karma', $user_nich['id']), 2);
                      $this->set_points('karma', $earned_karma, $user_nich['id']);

                    #-------------------------------------------
                    # �������� ���� �� ���
                      sql_query('UPDATE `users` SET
                      `hit_hod` = 0, `hit_hp` = 0, `hit_udar` = 0, `hit_block` = 0, `hit_uvorot` = 0, `hit_krit` = 0,
                      `battle` = 0, `battles_today` = `battles_today` + 1, `nich` = `nich` + 1,
                      `hp` = 0, `hp_precise` = 0, `fullhptime` = '.time().',
                      `mana` = 0, `pw_precise` = 0, `fullmptime` = '.time().',
                      `karma` = `karma` + '.$earned_karma.'
                       WHERE `id` = "'.$user_nich['id'].'" LIMIT 1;');

                       if ($user_my->user['id'] == $user_nich['id']) {
                          $user_my->user['hp']     = 0;
                          $user_my->user['mana']   = 0;
                          $user_my->user['battle'] = 0;
                       }

                     #-------------------------------------------
                     # ������ ���� �������� � �����
                     # ���� ���� ����� ����� �� ����� = ��� ������ �������

                      if (empty($this->tools["break_items"]) && !substr_count($user_nich['eff_all'], "1033;")) {  # ���� ��� ��������

                        $dur_ids = '';

                      #-------------------------------
                      # ������������ ������� "���� ������"
                        if (!empty($thematic_event['name']) && $thematic_event['name'] == "���� ������") {
                          $user_item_lomka = sql_query("SELECT `id`, `name`, `maxdur`, `duration`, `type`, `magic` FROM `inventory` WHERE `dressed` = 1 AND (`type` <> 25) AND `type` != 20 AND `owner` = '".$user_nich['id']."';");
                        } else {
                          $user_item_lomka = sql_query("SELECT `id`, `name`, `maxdur`, `duration`, `type`, `magic` FROM `inventory` WHERE `dressed` = 1 AND (`type` <> 25) AND `owner` = '".$user_nich['id']."';");
                        }

      				    while ($r_broken = fetch_array($user_item_lomka)) {
                           if (rand(1, 2) == 1 && (/*$r_broken['type'] != 20 && */$r_broken['type'] != 39 && $r_broken['type'] != 40 && $r_broken['type'] != 41 && $r_broken['type'] != 42)){

                               if ($user_draw->inventory_data[type_dreass_item($r_broken, $user_nich)]['id'] == $r_broken['id']) {
                                $user_draw->inventory_data[type_dreass_item($r_broken, $user_nich)]['duration'] = $r_broken['duration'] + 1;
                                $user_draw->mod_inventory_data = true;
                                $user_draw->USER_save_inventory_data();
                               }

                             if ($dur_ids) $coma = ','; else $coma = '';
                             $dur_ids .= $coma.$r_broken['id'];

                             $r_broken['duration'] += 1;

          				     if (fun_broken_item($r_broken) && $r_broken['type'] != 20){
          					  	$this->add_log('� '.nick_team($user_nich, $this->userclass($v)).' ������� "'.$r_broken['name'].'" � ����������� ���������!');
                            	$this->write_log();
          					 }
                           }
      				    }

                         if ($dur_ids) {
                          sql_query("UPDATE `inventory` SET `duration` = `duration` + 1 WHERE `owner` = '".$user_nich['id']."' AND `id` in(".$dur_ids.") ;");
                         }
                      }

                    #-------------------------------
                    # ����� ���������� ���� ��� ��� ��������
                      if ($user_nich['klan']) {
                       if ($this->battle_data['type'] == 14) {
                         sql_query("UPDATE `enterlog` SET
                         `klan_losewar_isday` = `klan_losewar_isday` + 1,
                         `klan_iswar_isday` = `klan_iswar_isday` + 1
                          WHERE `owner` = '".$user_nich['id']."' AND `type` = 0 LIMIT 1;");
                       }
                      }

                    # ����� � ���
                      CHAT::chat_attention($user_nich, '<a href="http://'.$this->battle_data['city'].'.blutbad.ru/log='.$battle_id.'" target="_blank"><b>���</b></a> ��������. ����� ���� �������� �����: <b>'.$earned_damage.'</b>. �������� �����: <b>0</b>. �����: <b>'.$earned_karma.'</b>. ��������: <b>0.00</b>.', array('addXMPP' => true));

                    # ��� ������ = ��� �������
                      include("functions/battle/specialnich.php");
                  }

                  $this->set_points('exp', 0, $k);     # �������� �����
                  $this->set_points('valor', 0, $k);   # �������� ��������

        		 }
        	    }

             $this->add_log('��� ��������. <b>�����</b>.');
             $this->write_log();

             $battle_err['nich_t1'] = 'ok';
             $this->upd_battle_err($battle_err);

             # C����� ����� ��� ���
          	   sql_query("UPDATE `battle` SET `win` = 0 WHERE `id` = '".$this->battle_data['id']."';");
               $this->battle_data['win'] = 0;

             #--> �������� ���� ���� � �������� �� 0
             #	$this->exp = array();
      	     #	$this->valor = array();

            }

             $battle_err['status_ends1'] = 'ok';
             $this->upd_battle_err($battle_err);

           # ���������� ������ ������� ��� ����
             $rr = "";
        	 if ($flag == 1 || $flag == 2 || $flag == 3) {
              $t1count = 0;
              $t2count = 0;

               $rr_e = "";
               $rr .= "<span>";
            	   foreach ($nks1 as $k => $v) {
            	     $t1count++;
            	     $dcoma = "";
            	     if ($k > 0) {$dcoma = ", "; }
                     $rr .= $dcoma."<b>".$v."</b>";
            	   }
               $rr .= "</span>";
               if ($flag == 1) $rr .= " <img src=http://img.blutbad.ru/i/flag.gif>";
               $rr .= " � ";
               $rr .= "<span>";
            	   foreach ($nks2 as $k => $v) {
            	     $t2count++;
            	     $dcoma = "";
            	     if ($k > 0) {$dcoma = ", "; }
                     $rr .= $dcoma."<b>".$v."</b>";
            	   }
               $rr .= "</span>";

               if ($flag == 2) $rr .= " <img src=http://img.blutbad.ru/i/flag.gif>";

               if ($flag != 3 && $t1count > 1 && $t2count > 1) {
                 if ($flag == 1) {
                   $rr_e = " �������� <b style=\" color: #BB5500; \">�������</b> �������";
                 } elseif ($flag == 2) {
                   $rr_e = " �������� <b style=\" color: #0055BB; \">�����</b> �������";
                 }
               }
        	 } else {
        		$rr = implode("</b>, <b>", $nks1)."</b> � <b>".implode("</b>, <b>",$nks2);
                $rr_e = " <b>�����</b>";
        	 }


           # �������� ���� �� ���
             $idbattle   = $battle_id;
             $roombattle = $this->user['room'];

         	 sql_query("UPDATE `battle` SET `teams` = '', `t1list` = '".serialize($this->t1list)."', `t2list` = '".serialize($this->t2list)."', `b_hint_nbp` = '', `battle_end` = 3 WHERE `id` = '".$idbattle."' LIMIT 1;");

           	 sql_query("DELETE FROM `bots` WHERE `battle` = '".$idbattle."' OR `battle` = 0 ;");

             if (empty($this->tools['b_rooms'])) {
      	        CHAT::chat_system ($this->user, "<a href=\"http://".$this->battle_data['city'].".blutbad.ru/log=".$idbattle."\" target=_blank><b>���</b></a> ����� ".$rr." ��������. ".$rr_e, $roombattle, array('addXMPP' => true));
             } else {
                $b_rooms = $this->tools['b_rooms'];
                foreach ($b_rooms as $k => $b_room) {
      	          CHAT::chat_system ($this->user, "<a href=\"http://".$this->battle_data['city'].".blutbad.ru/log=".$idbattle."\" target=_blank><b>���</b></a> ����� ".$rr." ��������. ".$rr_e, $b_room, array('addXMPP' => true));
                }
             }

             $id_city = get_city(INCITY, 1);
             XMPP::battleRoomManager($id_city, $idbattle, 2);
        	 XMPP::groupRoomManager($id_city, $idbattle, 1, 2);
        	 XMPP::groupRoomManager($id_city, $idbattle, 2, 2);
             Cache::addXml(XMPP::xml());

             unset($this->battle);

             $this->battle_data['teams'] = '';
             $this->battle_data['battle_eff'] = $this->battle_eff;
             $this->battle_data['b_hint_nbp'] = '';
             $this->battle_data['battle_end'] = 3;

           # ��� �� ����� ������
             if ($this->battle_data['type'] == 29) {
               include("functions/battle/special_arenatour_win.php");
             }

             $this->return = 0;
           return true;
          }
         }
       }
      return false;
     }
    }

/***----------------------------------------------------------------------
 * �������� ���������� �������� � ��� (�� ��� ���������� ����� �� ����)
 **/

    function spare_no($is_spare) {

      if ($this->battle_data['timeout'] > 3 && $is_spare['id'] < _BOTSEPARATOR_) {
         $this->check_breach_integrity_battle();
         sql_query("UPDATE `battle` SET `timeout` = `timeout` - 1, `to1` = '".time()."',`to2` = '".time()."' WHERE `id` = '".$this->battle_data['id']."' AND `timeout` > 1 LIMIT 1;");
         $_SESSION['battle_hint'] = "���� �� ����� ���� ��� �������� � 1 ���.";
      } else {

        # ������� ����� ����������
          $hp_spare = abs($this->get_points('h', $is_spare['id']));

    	  if ($this->battle_data['id'] && $this->get_timeout() && ($this->battle_data['win'] == 3 && $this->battle_data['battle_end'] == 0) && $hp_spare > 0)  {
             $this->sort_teams($is_spare['id']);
             sql_query("UPDATE `battle` SET `battle_end` = 2, `us_nich` = '".$is_spare['id']."' WHERE `id` = '".$this->battle_data['id']."' AND us_nich = '' LIMIT 1;");
             $this->battle_data['battle_end'] = 2;
             $this->battle_data['us_nich'] = $is_spare['id'];

           # ���� �����
             $this->add_points('karma', -1, $is_spare['id']);

             if ($is_spare['id'] > _BOTSEPARATOR_) {
               $user_bot = sql_row("SELECT `id`, `invis`, `battle`, `sex` FROM `users` WHERE `id` = '".$is_spare['prototype']."' LIMIT 1 ;");
               $user_bot['id']    = $is_spare['id'];
               $user_bot['login'] = $is_spare['name'];
               $user_bot['invis'] = $is_spare['invis'] == 1 ? 1 : 0;
               //$user_bot['invis'] = 0;
               $is_spare          = $user_bot;
             }

           # ����� � ���
             $this->add_log(nick_team($is_spare, $this->userclass($is_spare['id']))." ��������".($is_spare['sex']?"":"a")." ��� �� ��������.");

            # ������� �����������
              foreach ($this->team_enemy as $k => $id_enemy) {
                 if (!in_array($id_enemy, $this->texit)) { # �� ��� ����� �� ���
                    if($id_enemy > _BOTSEPARATOR_) {

                        $user_bot = $this->bots[$id_enemy];
                        if (($user_bot['is_hbot'] || $user_bot['is_hranbot']) && $user_bot['prototype']) {
                          sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0, `fullhptime` = ".time().", `mana` = 0, `pw_precise` = 0, `fullmptime` = ".time()." WHERE `id` = '".$user_bot['prototype']."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                        }

                        if ($user_bot['prototype']) { # ������� ���� �������� "�������� ����� ��� ����"
                          if (sql_query('UPDATE `bots` SET `hp` = 0 WHERE `id` = "'.$id_enemy.'" AND `hp` > 0 LIMIT 1;')) {
                             $death_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$user_bot['prototype']."' LIMIT 1;");
                             $death_user['bot_id'] = $id_enemy;
                             $death_user['login']  = $user_bot['name'];
                             $death_user['hp']     = 0;
                             $this->fast_death_to($death_user, false, 1);
                          }
                        }

                    } else {

                         $death_user = sql_row("SELECT * FROM `users` WHERE `id` = '".$id_enemy."' AND `battle` = '".$this->battle_data['id']."' LIMIT 1;");
                         if (!empty($death_user['id'])) {
                           $death_user['bot_id'] = $death_user['id'];
                           if (sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0, `fullhptime` = ".time().", `mana` = 0, `pw_precise` = 0, `fullmptime` = ".time()." WHERE `id` = '".$id_enemy."' AND `hp` > 0 LIMIT 1;")) {
                              $death_user['hp']   = 0;
                              $death_user['mana'] = 0;
                              $this->fast_death_to($death_user, false, 1);
                           }

                           if(!$this->battle_data['blood']) { # ���� ��� �� ��������
                            if (CONST_ZARNICA == false) {
                              $tr = settravma($death_user, 75);
                              if (!empty($tr)) {
                                $this->add_log(nick_team($death_user, $this->userclass($id_enemy)).$tr);
                              }
                            }
                           }
                         }
                    }
                 }
              }
    	  }

      }
    }

/***--------------------------
 * ������ ����� ��� ���
 **/

	function end_draft($is_spare) {
     global $user_my;

		if(!empty($user_my->user['id'])) {
        	if ($this->get_timeout() && ($this->battle_data['win'] == 3 || $this->battle_data['win'] == 4 || ($this->user['battle'] && ($this->battle_data['win'] == 1 || $this->battle_data['win'] == 2)) ))  {
        	    $this->battle = array();

              # ���� �����
                $this->add_points('karma', -1, $is_spare['id']);

        		$this->add_log(nick_team($user_my->user, $this->userclass($user_my->user['id']))." ��������".isusersex($user_my->user, 2)." ��� �� ��������.");
        		$this->write_log();

                foreach ($this->team_enemy as $k => $v) {
                  $this->set_points('h', 0, $v);
                  $this->set_points('exp', 0, $v);
                  $this->set_points('valor', 0, $v);
                }

                foreach ($this->team_mine as $k => $v) {
                  $this->set_points('h', 0, $v);
                  $this->set_points('exp', 0, $v);
                  $this->set_points('valor', 0, $v);
                }

              # �������� �����
                $this->update_battle('end_draft');
                $this->battle_end($is_spare['battle']);
                $this->write_log ();
                header ("Location:fbattle.php"); die("");
          	}
		}
	}

/***--------------------------
 * ������ ������ �����
 **/

  	function status_battle() {
        $cur_battle = $this->battle_data;
        $coun_team_all = count($this->t1) + count($this->t2);

        if($cur_battle['id'] && $coun_team_all > 25) {
            $status = 0;
            $status_ch = '';
            $status_log = '';

          # ������� ��������� ����
            switch ($coun_team_all) {
            # ������������ ��������!
              case ($coun_team_all >= 500 && ($cur_battle['status_battle'] == 4 || $cur_battle['status_battle'] == 0)):
                  $status     = 5;
                  $status_ch  = '������������� ��������';
                  $status_log = '������������ ��������';
                  break;
            # �������������� ��������!
              case ($coun_team_all >= 300 && ($cur_battle['status_battle'] == 3 || $cur_battle['status_battle'] == 0)):
                  $status     = 4;
                  $status_ch  = '��������������� ��������';
                  $status_log = '�������������� ��������';
                  break;
            # ��������� �����!
              case ($coun_team_all >= 150 && ($cur_battle['status_battle'] == 2 || $cur_battle['status_battle'] == 0)):
                  $status     = 3;
                  $status_ch  = '��������� �����';
                  $status_log = '��������� �����';
                  break;
            # ������ ������������
              case ($coun_team_all >= 70 && ($cur_battle['status_battle'] == 1 || $cur_battle['status_battle'] == 0)):
                  $status     = 2;
                  $status_ch  = '������� ������������';
                  $status_log = '������ ������������';
                  break;
            # ������ �������!
              case ($coun_team_all >= 30 && $cur_battle['status_battle'] ==  0):
                  $status     = 1;
                  $status_ch  = '������ �������';
                  $status_log = '������ �������';
                  break;
            }

            if ($status && $status_ch && $status_log) {
                  sql_query("UPDATE `battle` SET `status_battle` = ".$status." WHERE `id` = '".$cur_battle['id']."' LIMIT 1;");
                  CHAT::chat_city_attention (false, '��� ����� ���� ������ <b><a class="sys" href="/log='.$cur_battle['id'].'" target="_blank">'.$status_ch.'</a></b>', array('addXMPP' => true));
                  $this->add_log('��� �������� ������ <b>'.$status_log.'</b>');
            }
        }
      }

/***--------------------------
 * �������� �������� ����� �� ��������
 **/

      function fast_death_to($death_user, $att_user, $type = 0) {  # $type = 1 �������� ��������
  	   global $text_death, $text_salvation, $user_my;

  	  # ������� ������
  		if($this->battle) {

          if (!empty($death_user['bot_id'])) {

          # ������ ��� ��� ���
    		if($death_user['bot_id'] > _BOTSEPARATOR_) $is_defeated_bot = 1; else $is_defeated_bot = 0;
          # id �����������
            $is_defeated_id  = $death_user['bot_id'];

            # ���������� ���� ������������ ���� "��������"
              if (empty($type) && (int)$death_user['hp'] <= 0 && $is_defeated_id && !empty($this->battle_eff[$is_defeated_id]['salvation'])) {

                # ��������� + 10% ������
                  $ran_pw = floor($death_user['mana'] + ($death_user['maxmana'] * 0.1));
                  if ($ran_pw > $death_user['maxmana']) $ran_pw = $death_user['maxmana'];

                  if ($is_defeated_bot) {
                      $ran_salvation = 0;

                      if (($death_user['bothran'] || $death_user['bothidro'])) {

                          sql_query("UPDATE `users` SET
                            `hp` = `maxhp`, `hp_precise` = `maxhp`,
                            `mana` = ".$ran_pw.", `pw_precise` = ".$ran_pw."
                          WHERE id = '".$is_defeated_id."' LIMIT 1;");

                          $this->set_points("h", $death_user['maxhp'], $is_defeated_id);
                          $this->set_points("p", $ran_pw, $is_defeated_id);
                          $death_user['hp'] = $death_user['maxhp'];

                          $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');
                          $this->write_log ();
                          $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_salvation[$ran_salvation + $death_user['sex']].'');
                      } elseif (sql_query("UPDATE `bots` SET `hp` = `maxhp`, `mana` = ".$ran_pw." WHERE `id` = '".$is_defeated_id."' LIMIT 1;")) {

                          $this->set_points("h", $death_user['maxhp'], $is_defeated_id);
                          $this->set_points("p", $ran_pw, $is_defeated_id);
                          $death_user['hp'] = $death_user['maxhp'];

                          $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');
                          $this->write_log ();
                          $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_salvation[$ran_salvation + $death_user['sex']].'');
                      }

                  } else {
                      if (sql_query("UPDATE `users` SET `hp` = `maxhp`, `hp_precise` = `maxhp`, `mana` = ".$ran_pw.", `pw_precise` = ".$ran_pw." WHERE `id` = '".$is_defeated_id."' LIMIT 1;")) {
                        $ran_salvation = 0;

                      # ������ HP � PW �����
                        if ($user_my->user['id'] == $is_defeated_id) {
                          $user_my->user['hp']    = $user_my->user['maxhp'];
                          $user_my->user['mana']  = $ran_pw;
                        }

                        $death_user['hp'] = $death_user['maxhp'];
                        $this->set_points("h", $death_user['maxhp'], $is_defeated_id);
                        $this->set_points("p", $ran_pw, $is_defeated_id);

                        $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');
                        $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_salvation[$ran_salvation + $death_user['sex']].'');
                      }
                  }

                 $this->battle_eff[$is_defeated_id]['salvation'] = 0;

            # ������� �� ���
              } elseif($death_user['bot_id'] && (int)$death_user['hp'] <= 0 && $is_defeated_id) {

              # ������� �� ������ ���
                unset($this->battle[$is_defeated_id]);
              # ������� ���� �������
                unset($this->b_hint_nbp[$is_defeated_id]);

                  if ($is_defeated_bot && $is_defeated_id) {
                      if (isset($death_user['bothran']) || isset($death_user['bothidro'])) {
                         sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0 WHERE `id` = '".$is_defeated_id."' LIMIT 1;");
                      } else {
                        # sql_query("UPDATE `bots` SET `hp` = 0 WHERE `id` = '".$is_defeated_id."' LIMIT 1;");
                      }

                      $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');

                    # ������� ����
                      unset($this->bots[$is_defeated_id]);

                      $this->set_points("h", 0, $is_defeated_id);

                    # ��������� ����������� ����������
                      if ($att_user != false && isset($att_user['id'])) {
                        $this->add_points("usdeath", 1, $att_user['id']);
                      }

                  } else {
          	          if (sql_query("UPDATE `users` SET `hp` = 0, `hp_precise` = 0 WHERE `id` = '".$is_defeated_id."' LIMIT 1;")) {

                     # ������ HP � PW �����
                        if ($user_my->user['id'] == $is_defeated_id) $user_my->user['hp']   = 0;

                        $this->set_points("h", 0, $is_defeated_id);
                        $this->add_log(nick_team($death_user, $this->userclass($is_defeated_id)).$text_death[$death_user['sex']].'');

                      # ��������� ����������� ����������
                        if ($att_user != false && isset($att_user['id'])) {
                          $this->add_points("usdeath", 1, $att_user['id']);
                        }
                      }
                  }

                # ������� �� ������ ��� ����� �� ����
  				  foreach ($this->battle as $pers_id => $vav) {
  					unset($this->battle[$pers_id][$is_defeated_id]);
  				  }

              }

            # ���� ���� ���� � �� �� � ��� �� �������� ��� �� ����
  	    	  if($death_user['bot_id'] && ($death_user['battle'] != $this->battle_data['id'] || $death_user['hp'] <= 0)) {
  	          # ������� �� ������
  		        unset($this->battle[$is_defeated_id]);

              # ������� ���� �������
                unset($this->b_hint_nbp[$is_defeated_id]);

  		       	foreach ($this->battle as $pers_id => $vav) {
  		   	      unset($this->battle[$pers_id][$is_defeated_id]);
  			    }
  	    	  }

  		    unset($death_user);
          }
  		}
      }

/***--------------------------------------
 * ��������� �������� �� ��������
 **/

      function fast_death() {
       #------------------
  	   # ������� ������

         if (!empty($this->battle)) {
  		  foreach($this->battle as $defeated_id => $v) {

           if ($defeated_id > _BOTSEPARATOR_) {

            $bot_defeated = @$this->bots[$defeated_id];

              if (!empty($bot_defeated['id'])) {

              #---------------------------------------------------------
              # ���� ���� ���� � �� �� � ��� �� �������� ��� �� ����
    	    	  if(empty($bot_defeated['hp']) || ($bot_defeated['id'] && ($bot_defeated['battle'] != $this->battle_data['id'] || $bot_defeated['hp'] <= 0))) {

                 # �������� HP
                   $this->set_points("h", 0, $defeated_id);

                 # ������� �� ������
    		       unset($this->battle[$defeated_id]);

                 # ������� ���� �������
                   unset($this->b_hint_nbp[$defeated_id]);

    		       	foreach ($this->battle as $pers_id => $vav) {
    		   	      unset($this->battle[$pers_id][$defeated_id]);
    			    }
    	    	  }
              }

           }

         #---------------------------------------------------------
         # ������� �� ��� ���� ��� �� � ������ � ������ �������
           if ((!empty($this->t1list[$defeated_id]) && $this->t1list[$defeated_id]['h'] <= 0 && $this->t1list[$defeated_id]['h'] != "??") || (!empty($this->t2list[$defeated_id]) && $this->t2list[$defeated_id]['h'] <= 0 && $this->t2list[$defeated_id]['h'] != "??")) {

             # �������� HP
               $this->set_points("h", 0, $defeated_id);

             # ������� �� ������
		       unset($this->battle[$defeated_id]);

             # ������� ���� �������
               unset($this->b_hint_nbp[$defeated_id]);

		       	foreach ($this->battle as $kak => $vav) {
		   	      unset($this->battle[$kak][$defeated_id]);
			    }
           }

  		  }

          #------------------------------------------
          # ��� ���� �������� ���� ���������� � ���
            foreach ($this->t1list as $k => $v) {
              if (!empty($v['id']) && abs($v['h']) <= 0 && @$this->battle[$v['id']]) {
                foreach ($this->battle as $kak => $vav) { unset($this->battle[$kak][$v['id']]); }
                unset($this->battle[$v['id']]);
              }
            }

            foreach ($this->t2list as $k => $v) {
              if (!empty($v['id']) && abs($v['h']) <= 0 && @$this->battle[$v['id']]) {
                foreach ($this->battle as $kak => $vav) { unset($this->battle[$kak][$v['id']]); }
                unset($this->battle[$v['id']]);
              }
            }

          // end if

         }
      }

/***----------------------------------------------
 * ���������� �������, � �����������/���������
 **/

      function sort_teams($cus = 0) {
    	  if ($cus == 0) { $cus = $this->user['id']; }
    	  # ������ ������� � ��� ��� ����� �� ���
    		$this->t1 = explode(";", $this->battle_data['t1']);
    		$this->t2 = explode(";", $this->battle_data['t2']);
    		$this->texit = explode(";", $this->battle_data['texit']);

    	  # ����������� ���-���
    		if (in_array ($cus, $this->t1)) {
    			$this->my_class = "B1";
    			$this->en_class = "B2";
    			$this->team_mine = $this->t1;
    			$this->team_enemy = $this->t2;
    		} else {
    			$this->my_class = "B2";
    			$this->en_class = "B1";
    			$this->team_mine = $this->t2;
    			$this->team_enemy = $this->t1;
    		}
      }

/***--------------------------
 * �������������� ������ ���
 **/

      function razmen_init($enuser, $enemy, $attack, $defend, $pos, $isbotrazm = '', $istimeout = 0) {
       global $user_my, $caverooms;

       include 'functions/battle/razmen.php';

       return $return_razmen_init;
      }


/***--------------------------
 * ������� ���� � ��������
 **/

      function solve_exp($atak_user, $def_user, $damage) {
       global $baseexp;

       # ���� ������� ��� �� �� ��������
         if ($atak_user['bot'] && empty($atak_user['bothidro'])) return array("valor" => 0, "exp" => 0);

         $base_valor = 1;
         $isbot      = 1;

       # ��� ���� ��� ����
      	 if($atak_user['bot'] && !$atak_user['bothidro']) $isbot = 2;

          $at_kbo = (int)$atak_user['KBO'];
          if ($at_kbo < 1) $at_kbo = 1;

        # ��� ���� ��� ����������
          if($def_user['bot'] && !$def_user['bothidro']) $isbot = 2;


        # ���� ����� ���������
          $def_cost = $def_user['stuff_cost'];

          $def_kbo = (int)$def_user['KBO'];
          if ($def_kbo < 1) { $def_kbo = 1; }
          $d_h = date("H");

          # �� 4 ������ �� ���� ���� ���� ����������
            if ($atak_user['bot'] && (($this->battle_data['type'] == 7 && $this->user['level'] < 4) || $atak_user['bothidro'])) {
              $isbot = 2;

               if ($this->user['level'] >= 7 && ($d_h >= 12 || $d_h <= 20)) {
                switch (INCITY) {
                  case 'damask':  $isbot = rand(2, 3); break;
                  case 'alamut':  $isbot = rand(2, 4); break;
                  case 'calydon': $isbot = rand(2, 4); break;
                }
               }
            }

          # �� 4 ������ �� ���� ���� ���� ����������
            if ($def_user['bot'] && (($this->battle_data['type'] == 7 && $this->user['level'] < 4) || $def_user['bothidro'])) {
              $isbot = 2;
               if ($this->user['level'] >= 7 && ($d_h >= 12 || $d_h <= 20)) {
                switch (INCITY) {
                  case 'damask':  $isbot = rand(2, 3); break;
                  case 'alamut':  $isbot = rand(2, 4); break;
                  case 'calydon': $isbot = rand(2, 4); break;
                }
               }
            }

              # ��������� �������� � ����
                if (empty($def_user['id']) || empty($atak_user['id'])) {
                   return array("valor" => 0, "exp" => 0);
                } else {

                    $res_box = array();

                  # ��� ���� ��� �������
                    $fkbo = kboislevel($atak_user['level']);

                  # ���� ��� ��� �� ������ "1"
                    if (empty($fkbo)) { $fkbo = 1; }

                  # ������ ���� �� ����
                    $result_exp = ($baseexp[$atak_user['level']] * $at_kbo / 100 * ($def_cost / $fkbo) * ($def_user['level'] / $atak_user['level']) *  $damage / $def_user['maxhp']);
                    $t = $result_exp / $isbot;
                    if ($t < 0 || is_nan($t) || is_null($t)) $t = 0;

                    $res_box['exp'] = abs($t);

                  # ������ �������� �� ����
                    if($isbot >= 3) $isbot *= 2;
                    $result_val = ($base_valor * $at_kbo / 100 * ($def_cost / $fkbo) * ($def_user['level'] / $atak_user['level']) * $damage / $def_user['maxhp']);
                    $t = ($result_val / $isbot) * 1.1;
                    if ($t < 0 || is_nan($t) || is_null($t)) $t = 0;

                    $res_box['valor'] = abs($t);

                    return $res_box;
                }
      }

/***--------------------------
 * �������� ��� ������
 **/

      function get_wep_type($idwep, $id_us) {

        if ($this->get_points("wep_type", $id_us)) {
         return $this->get_points("wep_type", $id_us);
	    } elseif ($idwep == 0 || $idwep == null || $idwep == '') {
         return "kulak";
        } else {
	     $wep = sql_row('SELECT `otdel` FROM `inventory` WHERE `id` = '.$idwep.' LIMIT 1;');
         $name_wep = '';

          switch ($wep['otdel']) {
            case '1':   $name_wep = "noj";    break;
            case '11':  $name_wep = "topor";  break;
            case '12':  $name_wep = "dubina"; break;
            case '13':  $name_wep = "mech";   break;
            case '14':  $name_wep = "molot";  break;
            case '15':  $name_wep = "kastet"; break;
            case '52':  $name_wep = "posoh";  break;
            case '18':  $name_wep = "tackle"; break;
            default:    $name_wep = "kulak";  break;
          }

         $this->set_points("wep_type", $name_wep, $id_us);
         return $name_wep;
        }
      }

/***--------------------------
 * ��������� ������
 **/

      function razmen_log($type,$kuda,$chem,$uron,$kto,$c1,$pokomy,$c2,$hp,$maxhp,$mepos,$hepos, $isusertimeout = 0) {
        global $user_my;

               $uronex = abs(floor($uron));
               $hp     = abs(floor($hp));
               if($hp < 0) { $hp = 0; }
               $maxhp  = abs(floor($maxhp));

               $us_kto = $kto;
               if ($kto['bot_id']) { $kto_id = $kto['bot_id']; } else { $kto_id = $kto['id']; }

               $us_pokomy = $pokomy;
               if ($pokomy['bot_id']) { $us_pokomy_id = $pokomy['bot_id']; } else { $us_pokomy_id = $pokomy['id']; }

               if($us_pokomy['invis']) {
            	 $uron   = '??';
                 $hp     = '??';
                 $maxhp  = '??';
            	 $uronex = '??';
               }

               if ($isusertimeout) {
                 if ($kto_id == $isusertimeout) {
                  $user_timeout = $us_kto;
                 } else if($us_pokomy_id == $isusertimeout) {
                  $user_timeout = $us_pokomy;
                 } else {
                  $user_timeout = sql_row("SELECT `id`, `login`, `invis` FROM `users` WHERE `id` = '".$isusertimeout."' LIMIT 1;");
                 }
               }

                   #--> ��������� ��� ������� �����
                        if (!in_array($mepos, array(0, 1, 2))) { $mepos = 0; }
                   #--> ��������� ������� ����� ����������
                        if (!in_array($hepos, array(0, 1, 2))) { $hepos = 0; }

                        $sex1 = false;
                        $sex2 = false;

            			if ($this->enemyhar['sex']  && $kto_id       == $this->enemyhar['id']) { $sex1 = false; }
            			if (!$this->enemyhar['sex'] && $kto_id       == $this->enemyhar['id']) { $sex1 = true;  }
            			if ($this->enemyhar['sex']  && $us_pokomy_id == $this->enemyhar['id']) { $sex2 = false; }
            			if (!$this->enemyhar['sex'] && $us_pokomy_id == $this->enemyhar['id']) { $sex2 = true;  }

            			if ($this->user['sex']  && $kto_id           == $this->user['id']) { $sex1 = false; }
            			if (!$this->user['sex'] && $kto_id           == $this->user['id']) { $sex1 = true;  }
            			if ($this->user['sex']  && $us_pokomy_id     == $this->user['id']) { $sex2 = false; }
            			if (!$this->user['sex'] && $us_pokomy_id     == $this->user['id']) { $sex2 = true;  }

                      # ������� ����
            			$tmp_udars = array(
            				1 => array ('� ���','� ����','� �������','�� �������','� ������','�� �����','� ���'),
            				2 => array ('� �����','� ��������� ���������','�� �����','� ������','�� ������'),
            				3 => array ('� �����','� ������','� �����','� ������','� �����','�� ������','� ���','����� ���'),
            				4 => array ('�� �����','� ������','�� ���������','�� �����','�� ���������','�� �����','�� �����')
            			);

                      # ������ ������
                        if (date("m") == 4 && date("d") == 1) {
                          $tmp_sex_1_textfail_p_1 = array ('�������', '�� ������', '� ��������', '������');
                          $tmp_sex_1_textfail_p_2 = array ('�������������', '�������', '�����������', '�������������', '������������');

                          $tmp_sex_1_textfail_pos_1 = array ('���������', '������', '�������');
                          $tmp_sex_1_textfail_pos_2 = array ('� �����', '���������', '�������������');

                          $tmp_sex_0_textfail_pos_1 = array ('����������', '�������', '��������');
                          $tmp_sex_0_textfail_pos_2 = array ('� �����', '����������', '��������������');

                          $tmp_uvorot_text_sex_1    = array (', ������ �� ������ ', ', �������� �� ���� ', ', �������� �� ����� ', ', ���������� �� ����� ');
                          $tmp_uvorot_text_sex_0    = array (', ������� �� ������ ', ', ��������� �� ���� ', ', ��������� �� ����� ', ', ����������� �� ����� ');

                          $tmp_block_text_sex_1     = array (', ������������� ������ ', ', �������� ��� ������ ', ', ������ ���� ', ', ������ ���� ');
                          $tmp_block_text_sex_0     = array (', �������������� ������ ', ', ��������� ��� ������ ', ', ������� ���� ', ', ������� ���� ');

                          $tmp_krit_text_sex_1      = array (', ������� �� ���������', ', ������ �� ��� �������', ', ������ �� ��������', ', ������ �� �� ������');
                          $tmp_krit_text_sex_0      = array (', �������� �� ���������', ', ������� �� ��� �������', ', ������� �� ��������', ', ������� �� �� ������');

                          $tmp_krita_text_sex_1     = array (', �������� ���������', ', ������ �����', ', ������ ��������', ', ���� �����');
                          $tmp_krita_text_sex_0     = array (', ��������� ���������', ', ������� �����', ', ������� ��������', ', ����� �����');

                          $tmp_udar_text_sex_1      = array(', ����', ', �������', ', �������', ', ������');
                          $tmp_udar_text_sex_0      = array(', �����', ', ��������', ', ��������', ', �������');
                        } else {
                          $tmp_sex_1_textfail_p_1   = array ('������');
                          $tmp_sex_1_textfail_p_2   = array ('���������');

                          $tmp_sex_1_textfail_pos_1 = array ('��������');
                          $tmp_sex_1_textfail_pos_2 = array ('���������');

                          $tmp_sex_0_textfail_pos_1 = array ('���������');
                          $tmp_sex_0_textfail_pos_2 = array ('����������');

                          $tmp_uvorot_text_sex_1    = array (', ��������� �� ����� ');
                          $tmp_uvorot_text_sex_0    = array (', ���������� �� ����� ');

                          $tmp_block_text_sex_1     = array (', ������������ ���� ');
                          $tmp_block_text_sex_0     = array (', ������������� ���� ');

                          $tmp_krit_text_sex_1      = array (', ����� ����������� ����');
                          $tmp_krit_text_sex_0      = array (', ������� ����������� ����');

                          $tmp_krita_text_sex_1     = array (', ������ ����');
                          $tmp_krita_text_sex_0     = array (', ������� ����');

                          $tmp_udar_text_sex_1      = array(', ������');
                          $tmp_udar_text_sex_0      = array(', �������');
                        }

            		  # ��� ����
            			$textchem = array (
  							'kulak'   => array('','',''),
  							'noj'     => array('�����','�����','�����'),
  							'posoh'   => array('������','������','������'),
  							'dubina'  => array('�������','�������','�������'),
  							'topor'   => array('�������','�������','�������'),
  							'mech'    => array('�����','�����','�����'),
  							'molot'   => array('�������','�������','�������'),
  							'tackle'  => array('�������','��������','���������','��������','��������','�������� � ��������','�������'),
  							'buket'   => array('������� ������','�������','�������','���������','������','�������','��������','�������',),
  							'kastet'  => array('��������','��������','��������'),
  							'alcohol' => array('�������','�������','�������','����������','������','�������','������','������','�����','�����������')
  						);

                     # ������ �����
                       if ($this->battle_data['type'] == 24) $chem = 'alcohol';

            		   $textchem = $textchem[$chem];

            		  # ���� ����
            			$udars = $tmp_udars;

                     # ���� ������
                       if ($kuda) $kuda = $udars[$kuda][rand(0,count($udars[$kuda])-1)]; else $kuda = '';

            	      # ����� �� ������������
            			if (!$sex1) {
            			   if ($mepos==1) { $textfail = $tmp_sex_1_textfail_pos_1;  } elseif ($mepos==0 || $mepos==2) { $textfail = $tmp_sex_1_textfail_pos_2; }
            			} else {
                           if ($mepos==1) { $textfail = $tmp_sex_0_textfail_pos_1; } elseif ($mepos==0 || $mepos==2) { $textfail = $tmp_sex_0_textfail_pos_2; }
            			}

            			if (!$sex2) {
            			  if ($hepos==1) { $textud = $tmp_sex_1_textfail_pos_1; } elseif ($hepos==0 || $hepos==2) { $textud = $tmp_sex_1_textfail_pos_2;}
            			} else {
            			  if ($hepos==1) { $textud = $tmp_sex_0_textfail_pos_1;} elseif ($hepos==0 || $hepos==2) { $textud = $tmp_sex_0_textfail_pos_2;}
            			}
            			if (!$sex2) {
            			  if ($mepos==1) { $textudk = $tmp_sex_1_textfail_p_1;  } elseif ($mepos==0 || $mepos==2) { $textudk = $tmp_sex_1_textfail_p_2;}
            			} else {
            			  if ($mepos==1) { $textudk = $tmp_sex_1_textfail_p_1;  } elseif ($mepos==0 || $mepos==2) { $textudk = $tmp_sex_1_textfail_p_2;}
            			}

                        if (!empty($isusertimeout) && $isusertimeout == 'usertimeout') {
              			 if (!$sex2) {
              			   if ($hepos==1) { $textud = array ('�����������');} elseif ($hepos==0 || $hepos==2) { $textud = array ('�����������');}
              			 } else {
              			   if ($hepos==1) { $textud = $tmp_sex_0_textfail_pos_1;} elseif ($hepos==0 || $hepos==2) { $textud = $tmp_sex_0_textfail_pos_2;}
              			 }
                        }

                        $str = '';

        				switch ($type) {
        				  # ������
        					case "uvorot":
        						if ($sex2) { $textuvorot = $tmp_uvorot_text_sex_0; }
        					      	  else { $textuvorot = $tmp_uvorot_text_sex_1;  }

            					if (!$sex2) {
            					  if ($hepos==1) {$textudk = $tmp_sex_1_textfail_p_1; } elseif ($hepos==0 || $hepos==2) { $textudk = $tmp_sex_1_textfail_p_2;}
            					} else {
            					  if ($hepos==1) {$textudk = $tmp_sex_1_textfail_p_1; } elseif ($hepos==0 || $hepos==2) { $textudk = $tmp_sex_1_textfail_p_2;}
            					}

                               if (!empty($isusertimeout) && $isusertimeout == 'usertimeout') {
            					if (!$sex2) {
            					  if ($hepos==1) { $textudk = array ('����������� �');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('����������� �');}
            					} else {
            					  if ($hepos==1) { $textudk = array ('����������� �');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('����������� �');}
            					}
                               }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
        					     return $str.nick_team($us_kto, $c1).' '.$textfail[rand(0,count($textfail)-1)].', �� '.nick_team($us_pokomy, $c2).' '.$textudk[rand(0,count($textud)-1)].''.$textuvorot[rand(0,count($textuvorot)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.'.';
                               }
        					break;
        				  # ����
        					case "block":
        						if ($sex2) { $textblock = $tmp_block_text_sex_0; }
        					          else { $textblock = $tmp_block_text_sex_1; }

            					if (!$sex2) {
            					  if ($hepos==1) {$textudk = $tmp_sex_1_textfail_p_1;} elseif ($hepos==0 || $hepos==2) { $textudk = $tmp_sex_1_textfail_p_2;}
            					} else {
            					  if ($hepos==1) {$textudk = $tmp_sex_1_textfail_p_1;} elseif ($hepos==0 || $hepos==2) { $textudk = $tmp_sex_1_textfail_p_2;}
            					}

                               if (!empty($isusertimeout) && $isusertimeout == 'usertimeout') {
            					if (!$sex2) {
            					  if ($hepos==1) {$textudk = array ('����������� �');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('����������� �');}
            					} else {
            					  if ($hepos==1) {$textudk = array ('����������� �');} elseif ($hepos==0 || $hepos==2) { $textudk = array ('����������� �');}
            					}
                               }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
        					     return $str.nick_team($us_kto, $c1).' '.$textfail[rand(0,count($textfail)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_pokomy, $c2).' '.$textudk[rand(0,count($textudk)-1)].''.$textblock[rand(0,count($textblock)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.'.';
                               }
        					break;
        				  # ����
        					case "krit":
        						if ($sex1) { $textkrit = $tmp_krit_text_sex_0; }
        					          else { $textkrit = $tmp_krit_text_sex_1; }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
        						 return $str.nick_team($us_pokomy, $c2).' '.$textud[rand(0,count($textud)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_kto, $c1).' '.$textudk[rand(0,count($textud)-1)].''.$textkrit[rand(0,count($textkrit)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.' <font color="red"><b>-'.$uronex.'</b></font> ['.$hp.'/'.$maxhp.']'.'';
                               }
        					break;
        				  # ���� ������ ����
        					case "krita":
        						if ($sex1) { $textkrit = $tmp_krita_text_sex_0; }
        					          else { $textkrit = $tmp_krita_text_sex_1; }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
        						 return $str.nick_team($us_pokomy, $c2).' '.$textud[rand(0,count($textud)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_kto, $c1).' '.$textudk[rand(0,count($textud)-1)].''.$textkrit[rand(0,count($textkrit)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.' <font color="red"><b>-'.$uronex.'</b></font> ['.$hp.'/'.$maxhp.']'.'';
                               }
        					break;
        				  # ���������
        					case "udar":
        						if ($sex1) { $textudar = $tmp_udar_text_sex_0; }
                                      else { $textudar = $tmp_udar_text_sex_1; }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
         						 return $str.nick_team($us_pokomy, $c2).' '.$textud[rand(0,count($textud)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_kto, $c1).' '.$textudk[rand(0,count($textud)-1)].''.$textudar[rand(0,count($textudar)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.' <b>-'.$uronex.'</b> ['.$hp.'/'.$maxhp.']'.' ';
                              }
        					break;
        				  # ��������� �����
        					case "shield_udar":
        						if ($sex1) { $textudar = array(', ������������� �������'); }
                                      else { $textudar = array(', ������������� ������'); }

                               if ($isusertimeout > 0 && $isusertimeout != 'usertimeout') {
        						 return $str.nick_team($user_timeout, $this->userclass($user_timeout['id'])).' ��������� ��� ';
                               } else {
         						 //return $str.nick_team($us_pokomy, $c2).' '.$textud[rand(0,count($textud)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_kto, $c1).' '.$textudk[rand(0,count($textud)-1)].''.$textudar[rand(0,count($textudar)-1)].' '.$textchem[rand(0,count($textchem)-1)].' '.$kuda.' <b>-'.$uronex.'</b> ['.$hp.'/'.$maxhp.']'.' ';
         						 return $str.nick_team($us_pokomy, $c2).' '.$textud[rand(0,count($textud)-1)].''.(($isusertimeout != 0 && $isusertimeout=='usertimeout')?", � ":", �� ").''.nick_team($us_kto, $c1).' '.$textudk[rand(0,count($textud)-1)].''.$textudar[rand(0,count($textudar)-1)].' ����� <b>-'.$uronex.'</b> ['.$hp.'/'.$maxhp.']'.' ';
                              }

        					break;
        				}
      }

    /***-----------------------------------
     * �������� �� ��������� "���� ����"
     **/

      function get_block ($komy, $att, $def, $enemy_schit, $parm = array()) {

            if($enemy_schit){
      	     $blocks = array (
      						  '0' => array (0),
      						  '1' => array (1,2),
      						  '2' => array (2,3),
      						  '3' => array (3,4),
      						  '4' => array (4,1),
      						  '5' => array (4,2),
      						  '6' => array (1,3)
      						);
              if (!in_array($def, array (1, 2, 3, 4, 5, 6))) { $def = 0; }
             } else {
      	     $blocks = array (
      						  '0' => array (0),
      						  '1' => array (1),
      						  '2' => array (2),
      						  '3' => array (3),
      						  '4' => array (4)
      					     );
              if (!in_array($def, array (1, 2, 3, 4))) { $def = 0; }
             }
       #   global $user;
       #   CHAT::chat_attention($user, $enemy_schit.'-'.$def.' == ���='.$parm[0].' = "'.$parm[1].'", �� ����='.$parm[2].' = "'.$parm[3].'",    komy='.$komy.', att='.$att.', def='.$def.', enemy_schit='.$enemy_schit, array('addXMPP' => true));
				switch ($komy) {
					case "me" :
						if (in_array($att, $blocks[$def])) {
							return true;
						} else {
							return false;
						}
					break;
				   # ���� �������
					case "he" :
						if (in_array($att, $blocks[$def])) {
							return true;
						} else {
							return false;
						}
					break;
				}
       }


    /***--------------------------
     * �������� �����������
     **/

       function select_enemy() {
         global $user_my;

            $enemys = array();
    		if(!empty($user_my->user['id']) &&($this->user['hp'] > 0 && $this->user['battle'] > 0) && !empty($this->battle[$user_my->user['id']]) && $this->battle_data['win']==3) {
    			foreach($this->battle[$user_my->user['id']] as $k => $v) {
    				if (empty($this->battle[$user_my->user['id']][$k][0]) || $this->battle[$user_my->user['id']][$k][0]==0) {
    				   if ($k > 0) $enemys[] = $k;
                    }
    			}

                if (!empty($enemys)) {
                       return $enemys[rand(0,count($enemys)-1)];
                } else return 0;

    		} else return 0;
       }

/***--------------------------
 * C������ ������������
 **/

            function solve_mf($user_left, $user_right, $myattack) {

             $mf_z9_def  = 0.1;
             $mf_x1_def  = 0.1;
             $mf_z9_max  = 0.95;
             $mf_x1_max  = 0.95;
             $str_factor = 0.5;
             $z9_factor  = 1;

             $mf         = array ();

             $enuser     = $user_left['bot_id'];
             $enemy      = $user_right['bot_id'];

           /* echo "<pre>";
            print_r($user_left);
            echo "</pre>";

            echo "<pre>";
            print_r($user_right);
            echo "</pre>";
            die();*/

              # ������
                $mf_change = function ($my_player, $enemy_player, $pos_att = 0, $m_me = "me",  $m_he = "he") {

                   $mf_krit_def    = 0.01;  # ���� � ����� ���������
                   $mf_uvorot_def  = 0.01;  # ���� � ����� ���������
                   $mf_is_krit_max = 0.95; # ����������� ����������� = ���������
                   $mf_is_uvor_max = 0.95; # ����������� ����������� = ���������
                   $str_factor     = 0.5;  # ������� �����
                   $z9_factor      = 1;    # ���� � �����

                 # ��� ��������������
                   $my_sila  = abs($my_player['sila']  + $my_player['inv_strength']);
                   $my_lovk  = abs($my_player['lovk']  + $my_player['inv_dexterity']);
                   $my_inta  = abs($my_player['inta']  + $my_player['inv_success']);

                 # ���������� ��������������.
                   $en_sila  = abs($enemy_player['sila']  + $enemy_player['inv_strength']);
                   $en_lovk  = abs($enemy_player['lovk']  + $enemy_player['inv_dexterity']);
                   $en_inta  = abs($enemy_player['inta']  + $enemy_player['inv_success']);

                    #----------------------------------------------------------
                    # ������� ����� "2=������, 1=�����, 0=��� �������"
                      $pos_att_my_player    = $this->battle[$my_player['bot_id']][$enemy_player['bot_id']][2];
                      $pos_att_enemy_player = $this->battle[$enemy_player['bot_id']][$my_player['bot_id']][2];

                    #----------------------------------------------------------
                    # ��� ������� �� �������� ������
                      if ($my_player["mana"] > 0 && $my_player["maxmana"] > 0) {
                        $me_coef = $my_player["mana"] / $my_player["maxmana"];
                        if($me_coef < 0.1) $me_coef = 0.1;
                      } else {
                        $me_coef = 0.1;
                      }

                    #----------------------------------------------------------
                    # ��� ������� �� �������� ������
                      if ($enemy_player["mana"] > 0 && $enemy_player["maxmana"] > 0) {
                        $he_coef = $enemy_player["mana"] / $enemy_player["maxmana"];
                        if($he_coef < 0.1) $he_coef = 0.1;
                      } else {
                        $he_coef = 0.1;
                      }

                    #-----------------------------------------------------
                    # �����: ������, �����, �����, ��� "������ � �����"
                      $en_bron = 0; # rand(0, $en_bron);
                      switch($pos_att){
                       case 1: $en_bron = ($enemy_player['inv_bron1']); break;
                       case 2: $en_bron = ($enemy_player['inv_bron2']); break;
                       case 3: $en_bron = ($enemy_player['inv_bron3']); break;
                       case 4: $en_bron = ($enemy_player['inv_bron4']); break;
                      }

                    #---------------------------------------
                    # ��� ����
                      if (is_nan($my_sila) || is_null($my_sila) || $my_sila < 0) { $my_sila = 3; }
                      $mod_sila = floor(abs($my_sila));
                      $dam      = 0;

                    #----------------------
                    # ���� ����� � �����
                      if ($pos_att_my_player == 0 && $pos_att_enemy_player == 0) {
                        /*$my_player['inv_minu'] *= 1.2;
                        $my_player['maxu'] *= 1.2;*/
                        $mod_sila *= 1.4;
                      }

                    #----------------------
                    # ���� ������ ����
                      if (empty($my_player['inv_minu'])) { $my_player['inv_minu'] = 0; }
                      if (empty($my_player['inv_maxu'])) { $my_player['inv_maxu'] = 0; }

                    #----------------------
                    # ���� ������ 5 ����
                      if ($mod_sila > 5 || $my_player['inv_minu']) {
                        $kor_sila = sqrt($mod_sila);
                        $kor10_sila = $kor_sila*sqrt($kor_sila);

                        $minu  = ($my_player['inv_minu'])+$kor10_sila;
                        $maxu  = ($my_player['inv_maxu'])+$kor10_sila+$kor_sila;
                        if (is_nan($dam) || is_null($minu) || $minu < 0) { $minu = 0; }
                        if (is_nan($dam) || is_null($maxu) || $maxu < 0) { $maxu = 5; }

                        $dam  += float_rand($minu, $maxu);
                      } else {
                        $dam  += rand(1, 5);
                      }

                  #--------------------------
                  # �������� ������������
                    $x8_min = round(sqrt($mod_sila));
                    if($pos_att_my_player == 2){ $x8_min = round($x8_min * 1.5); }

                      $my_uvorot     = $me_coef * ($my_player['inv_uvor']     + ($my_lovk * 10));    # ������
                      $enemy_auvorot = $he_coef * ($enemy_player['inv_auvor'] + ($en_lovk * 10)); # ������ ������
                      $my_krit       = $me_coef * ($my_player['inv_krit']     + ($my_inta * 10));    # ����
                      $enemy_akrit   = $he_coef * ($enemy_player['inv_akrit'] + ($en_inta * 10)); # ������ ����

                      $r_1 = 1.1;
                      $r_0 = 1.4;

                    # ���� � �����
                      if($pos_att_my_player == 1 && $pos_att_enemy_player == 1){
                       $r_0  = 1.4;
                       $dam *= 1.5;
                      }

                    # ���� � ����� ������ � ������
                      if(($pos_att_my_player == 1 && $pos_att_enemy_player == 2) || ($pos_att_my_player == 2 && $pos_att_enemy_player == 1)){
                       $r_0  = 1.2;
                       $dam *= 1.1;
                      }

                    #-------------
                    # ��� ����
                      if($pos_att_my_player == 2){              #-- � ������
                         $my_krit       /= float_rand(1, $r_0); # ��������
                         $my_uvorot     *= float_rand(1, $r_0); # ��������

                         $enemy_akrit   /= float_rand(1, $r_0); # ��������������� ����
                         $enemy_auvorot *= float_rand(1, $r_0); # �������� ����������
                        // $dam         /= 1.1;
                      } elseif($pos_att_my_player == 1){        #-- � �����
                         $my_krit       *= float_rand(1, $r_0); # �������� ����������� ����
                         $my_uvorot     /= float_rand(1, $r_0); # �������� ���� ����������

                         $enemy_akrit   /= float_rand(1, $r_0); # ��������������� ����
                         $enemy_auvorot *= float_rand(1, $r_0); # �������� ����������
                         $dam           *= 1.2;
                      }

                    #-------------
                    # ��� ����
                      if($pos_att_enemy_player == 2){           #-- � ������
                         $enemy_akrit   /= float_rand(1, $r_0); # ��������
                         $enemy_auvorot *= float_rand(1, $r_0); # ��������

                         $my_krit       /= float_rand(1, $r_0); # ��������������� ����
                         $my_uvorot     *= float_rand(1, $r_0); # �������� ����������
                      } elseif($pos_att_enemy_player == 1){     #-- � �����
                         $enemy_akrit   *= float_rand(1, $r_0); # �������� ����������� ����
                         $enemy_auvorot /= float_rand(1, $r_0); # �������� ���� ����������

                         $my_krit       /= float_rand(1, $r_0); # ��������������� ����
                         $my_uvorot     *= float_rand(1, $r_0); # �������� ����������
                        // $dam           *= $r_0;
                      }

                    #--------------------------------------
                    # �������� �����
                     // $dam -= mt_rand(sqrt($en_bron), $en_bron);
                      $dam -= sqrt($en_bron);

                    #--------------------------------------
                    # ���� ������ ����� ��� ������ ����
                      if($dam < 1) $dam = 1;

                    #-------------------
                    # ���� ����������
                      if ($my_uvorot >= 0 && empty($enemy_auvorot)) $uvorot = $my_uvorot;
                      if (rand(1, 100) <= 5) $uvorot = 0;
                      else $uvorot =  (empty($my_uvorot)?1:$my_uvorot) / (empty($enemy_auvorot)?1:$enemy_auvorot);

                    #-----------------
                    # ���� ���������
                      if ($my_krit >= 0 && empty($enemy_akrit)) $krit = $my_krit;
                      if (rand(1, 100) <= 5) $krit = 0;
                      else $krit = (empty($my_krit)?1:$my_krit) / (empty($enemy_akrit)?1:$enemy_akrit);

                    #----------------------------
                    # �������� ����� �� ������
                      if (is_nan($dam) || is_null($dam) || $dam < 0) { $dam = 0; }
                    #---------------------------
                    # �������� ����� �� ������
                      if (is_nan($krit) || is_null($krit) || $krit < 0) { $krit = 0; }
                    #------------------------------
                    # �������� ������� �� ������
                      if (is_nan($uvorot) || is_null($uvorot) || $uvorot < 0) { $uvorot = 0; }
                    if ($my_player['id'] ==1) {
                      $uvorot =0;
                    }
                	return array (
                		'id'          => $my_player['id'],
                	    'udar'        => floor($dam),
                		'krit'        => $krit + $mf_krit_def,
                		'uvorot'      => $uvorot + $mf_uvorot_def,
                		'x8_min'      => 1 + floor($x8_min / 3.7),
                        'priem_krit'  => '',
                        'user_krit'   => 0,
                        'user_udar'   => 0,
                	);
                };

                 $mf = array();
                 $mf[$user_left['bot_id']]  = $mf_change($user_left, $user_right, $myattack, "me", "he");
                 $mf[$user_right['bot_id']] = $mf_change($user_right, $user_left, $myattack, "he", "me");

               #  $mf[$this->user['bot_id']]['udar'] = 1;

              # ��� ������� ��������� ����
          		if($user_right['bot'] && $enemy > _BOTSEPARATOR_ && ($this->battle_data['type'] == 2 || $this->battle_data['type'] == 7)) {

                  # ������ ���� ������ ���  1-4 �������
          		  if ($user_right['level'] < 4 && $user_right['level'] != 0) {
          		    if ($mf[$user_right['bot_id']]['udar'] > 7) {
                        $mf[$user_right['bot_id']]['udar'] = floor($mf[$user_right['bot_id']]['udar'] / 2.1);
          		    }
          		  }

                  # ������ ��������� ���� ������ ��� ������� �������

          		  if ($user_right['level'] == 0) {
                      if ($mf[$user_right['bot_id']]['udar'] > 5) {
                          $mf[$user_right['bot_id']]['udar'] -= floor($mf[$user_right['bot_id']]['udar'] / 2);
                      } else {
                          $mf[$user_right['bot_id']]['udar'] -= floor($mf[$user_right['bot_id']]['udar'] / 1.5);
                      }
          		  }
          		}

             # �������� ���������� �������
               /*if ($user_left['login'] == "BR") {
                addchp ('<b>��������</b>!!!<b>'.$user_left['login'].'</b>['.$user_left['level'].']     ����='.$mf[$user_left['bot_id']]['udar'].' sila='.($user_left['sila'] + $user_left['inv_strength']).' �����='.$user_left['inv_bron1'].'  ����='.$user_left['inv_minu'].'/'.$user_left['inv_maxu'].'   ������� ='.$user_left['inv_uvor'].' ����� ='.$user_left['inv_krit'].'  ', "BR");
                addchp ('<b>��������</b>!!!<b>'.$user_right['login'].'</b>['.$user_right['level'].']  ����='.$mf[$user_right['bot_id']]['udar'].' sila='.($user_right['sila'] + $user_right['inv_strength']).' �����='.$user_right['inv_bron1'].' ����='.$user_right['inv_minu'].'/'.$user_right['inv_maxu'].' ������� ='.$user_right['inv_uvor'].' ����� ='.$user_right['inv_krit'].'  ', "BR");
               }*/

            	return $mf;
            }

/***--------------------------
 * �������� � ����� �������
 **/

            function userclass($id, $type = 'B') {
               if (in_array ($id, $this->t1)) return $type."1"; else return $type."2";
            }

/***--------------------------
 * �������� ���������� ������� ������� ����� ����
 **/

            function count_att($u_id){
               $count_att = 0;
               $tlist     = array();

               if (in_array($u_id, $this->t1)) $tlist = $this->t1list; else $tlist = $this->t2list;
               foreach ($tlist as $k => $v_data) { if ($v_data['h']) $count_att++; }
               return $count_att;
            }

/***--------------------------
 * ��������� ���
 **/

			function update_battle($df = "") {
                 if ($this->upd_required) { }

                    if (substr_count(serialize($this->battle), "N;")) {

  				      $thisdamage = unserialize($this->battle_data['teams']);
                        if (empty($this->battle_data['id'])) {
                          sql_query("UPDATE `battle` SET `teams_old` = '".serialize($thisdamage)."' WHERE `id` = '".$this->battle_data['id']."' LIMIT 1;");
#                          addchp ('<b>��������</b>!!!<b>'.$user_my->user['login'].'</b> <a href="http://damask.blutbad.ru/log='.$this->battle_data['id'].'" target="_blank">'.$this->battle_data['id'].'</a>�������������� ��� '.(time() - $this->battle_data['to1']).' ���. ��� ������ ', '�����');
                        }
                    }

                    $battle_sql = "";
                    $save_stats = (count($this->t1) + count($this->t2) > 100)?0:1;

                    if ($this->battle_data['teams']      != serialize($this->battle)) {
                      $battle_sql .= "`teams` = '".serialize($this->set_new_teams(true))."',  ";
                    }

                    if ($this->battle_data['battle_eff'] != serialize($this->battle_eff)) { $battle_sql .= "`battle_eff` = '".serialize($this->battle_eff)."',  "; }
                    if ($this->battle_data['t1list']     != serialize($this->t1list))     { $battle_sql .= "`t1list` = '".serialize($this->t1list)."',  "; }
                    if ($this->battle_data['t2list']     != serialize($this->t2list))     { $battle_sql .= "`t2list` = '".serialize($this->t2list)."',  "; }
                    if ($save_stats && $this->battle_data['b_hint_nbp'] != serialize($this->b_hint_nbp)) { $battle_sql .= "`b_hint_nbp` = '".serialize($this->b_hint_nbp)."',  "; }
                    if ($save_stats && $this->battle_data['statis']     != serialize($this->statis))     { $battle_sql .= "`statis` = '".serialize($this->statis)."',  "; }

                    if ($battle_sql) {
                      # echo $df." update_battle<br>";
    			      return sql_query("UPDATE `battle` SET $battle_sql `user_update_battle` = '".(time() + 1)."', `to1` = '".time()."',`to2` = '".time()."' WHERE `id` = '".$this->battle_data['id']."' LIMIT 1;");
                    } else {
                      return false;
                    }

			}

/***--------------------------
 * ���� �� ���� � ��������� �����?
 **/

			function get_timeout() {
				if($this->battle_data['win'] == 3) {

					if ($this->my_class == 'B1') {
						if($this->to2 <= $this->to1) {
							return ((time() - $this->to2) > $this->battle_data['timeout'] * 60);
						} else {
							return false;
						}
					} else {
						if($this->to2 >= $this->to1) {
							return ((time() - $this->to1) > $this->battle_data['timeout'] * 60);
						} else {
							return false;
						}
					}
				}
			}

/***--------------------------
 * �������� ����� � ���
 **/

			function add_log ($text, $parm = array()) {
			  global $cur_battleid;

			     $this->log .= ((abs($parm['my_id']) || abs($parm['he_id']))?'<uid>'.abs($parm['my_id']).';'.abs($parm['he_id']).';</uid>':'').'<span class="date">'.date("H:i:s", time()).'</span> <span style="font-size: 12px;">'.$text.'</span></span><BR>';

                 if ($this->battle_data['log_channel']) {
                   if ($parm['my_id']) $my_id = $parm['my_id']; else $my_id = 0;
                   if ($parm['he_id']) $he_id = $parm['he_id']; else $he_id = 0;

                   $mes = date("H:i:s")."|".$this->battle_log_id."|".$my_id."|".$he_id."|"."".$text;

                    if (empty($this->battle_log)) {
                      $this->battle_log = $mes;
                    } else {
                      $this->battle_log .= "||".$mes;
                    }
                 }

			}

/***--------------------------
 * ������ ������ � ����
 **/

			function write_log () {
			 global $cur_battleid;

               if($this->log) { $this->log = $this->log."<hr>"; }

               if (($this->user['battle'] || $cur_battleid) && $this->log) {

                  if ($this->battle_log) {
                    CHAT::chat_battle_add_lb($this->battle_log, $cur_battleid, array('addXMPP' => true));
                  }

                # ����� ���������� � ����,
                # ��������� ���� FILE_APPEND flag ��� ����������� ����������� � ����� �����
                # � ���� LOCK_EX ��� �������������� ������ ������� ����� ���-������ ������ � ������ �����

                  if ($this->user['battle']) { $cur_battleid = $this->user['battle']; }
                  $file_log = $_SERVER["DOCUMENT_ROOT"].'/backup/logs/battle'.$cur_battleid.'.txt';
                  file_put_contents($file_log, $this->log, LOCK_EX | FILE_APPEND);

                  $this->battle_log = '';
  			      $this->log = '';
               }
			}
	}
?>