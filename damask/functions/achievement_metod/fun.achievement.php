<?

/***------------------------------------------
 * Проверка мобильного устройства
 **/

function check_smartphone() {
    $phone_array = array('iphone', 'android', 'pocket', 'palm', 'windows ce', 'windowsce', 'cellphone', 'opera mobi', 'ipod', 'small', 'sharp', 'sonyericsson', 'symbian', 'opera mini', 'nokia', 'htc_', 'samsung', 'motorola', 'smartphone', 'blackberry', 'playstation portable', 'tablet browser');
    $agent = strtolower( $_SERVER['HTTP_USER_AGENT'] );

    foreach ($phone_array as $value) {

        if ( strpos($agent, $value) !== false ) return true;

    }
 return false;
}

/***------------------------------------------
 * Кодировка текста
 **/

function BLUTBAD_text_encrypt($text_data, $key = "23456786543245678"){
      return base64_encode(
      mcrypt_encrypt(
          MCRYPT_RIJNDAEL_128,
          $key,
          $text_data,
          MCRYPT_MODE_CBC,
          "\0\1\2\1\2\0\1\2\0\2\0\2\2\2\0\1"
      )
  );
}

/***------------------------------------------
 * Розкодировка текста
 **/

function BLUTBAD_text_decrypt($text_data, $key = "23456786543245678"){
    $decode = base64_decode($text_data);
    return mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_128,
                    $key,
                    $decode,
                    MCRYPT_MODE_CBC,
                    "\0\1\2\1\2\0\1\2\0\2\0\2\2\2\0\1"
            );
}


?>