<?php

class Router {
	
	public static $segments = [];
	public static $gets = [];
	public static $posts = [];
	
	public static $isGet = false;
	public static $isPost = false;
	
	public static function init() {
		$uri = trim($_SERVER['REQUEST_URI'], '/');
		$uri = explode('?', $uri);
		self::$segments = explode('/', $uri[0]);
		
		if(is_array($_GET) && count($_GET)) {
			self::$isGet = true;
			
			foreach($_GET as $key => $value) {
				self::$gets[$key] = $value;
			}
		}
		
		if(is_array($_POST) && count($_POST)) {
			self::$isPost = true;
			
			foreach($_POST as $key => $value) {
				self::$posts[$key] = $value;
			}
		}
	}
	
	public static function segment( $element = 0 ) {
		return strtolower(self::$segments[$element]);
	}
	
	public static function get( $key ) {
		return self::$gets[$key];
	}
	
	public static function isGet() {
		return self::$isGet;
	}
	
	public static function post( $key ) {
		return self::$posts[$key];
	}
	
	public static function isPost() {
		return self::$isPost;
	}
	
}

?>