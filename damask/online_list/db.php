<?php

define('DBHOST', 'localhost');
define('DBUSER', 'root');
define('DBPASS', '');
define('DBNAME', 'test5');

class DB {
	
	private static $connect;
	
	public static function connect() {
		self::$connect = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);
		
		if(self::$connect) self::query('SET NAMES cp1251');
	}
	
	public static function query( $sql ) {
		return mysqli_query(self::$connect, $sql);
	}
	
	public static function fetchArray( $result ) {
		return mysqli_fetch_array($result);
	}
	
	public static function getObject( $query ) {
		return mysqli_fetch_object(self::query($query));
	}
	
}

?>