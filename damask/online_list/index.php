<?php

require_once('router.php');
//require_once('db.php');
require_once('../connect.php');
require_once('../m_game_function.php');

//$rooms = [100 => '�����', 120 => '���������� �����'];

Router::init();

header('Content-Type: text/xml');

$break_names = "";

$city_room = explode('_', Router::segment(1));

$citys = array(
 1 => "damask",
 2 => "alamut",
 3 => "calydon",
);

$room = $city_room[1];
$city = $citys[$city_room[0]];

if (!is_numeric($room) || empty($city)) { die(); }

# ���� � ��������
if ($room > 10000) {
   $dungeon_in = sql_row("SELECT id, map_name FROM `dungeon_in` WHERE `id` = '".$room."' LIMIT 1;");
   if ($dungeon_in['id']) {
     $rooms[$room] = $dungeon_in['map_name'];
   }
}

echo "<root>\n";
echo '<room>' . CP1251_to_UTF8($rooms[$room]) . "</room>\n";

echo CP1251_to_UTF8(show_s_bot($room));

$q = sql_query('SELECT * FROM `users` WHERE `incity` = \'' . $city . '\' AND `invis` = 0 AND `room` = ' . $room . ' AND `upd_online` > ' . (time() - 60));

while($row = fetch_array($q)) {
  if ($row['bot'] && !substr_count($break_names, $row['login'])) {
    echo CP1251_to_UTF8(xmlData($row));
  } elseif ($row['bot'] == 0) {
    echo CP1251_to_UTF8(xmlData($row));
  }
}

echo '</root>';

function xmlData( $params ) {
	$data = '<data>';

   $realName = @$params['login'];
   $username = @$params['login'];

   $params['guildTitle'] = '';

   if ($params['bot'])   { $username = $params['login']."|".$params['id']; }
   if ($params['guild'] == 'Paladins')  { $params['guild'] = 'paladins'.$params['align'];  $params['guildTitle'] = '�������'; }
   if ($params['guild'] == 'Assassins') { $params['guild'] = 'assassins'.$params['align']; $params['guildTitle'] = '�������'; }

   if (empty($params['battle'])) {
    $params['battle'] = 0;
   }

   if (empty($params['dungeon'])) {
    $params['dungeon_x'] = 0;
    $params['dungeon_y'] = 0;
   }

   $bcolor = unserialize($params['bcolor']);
   $params['nick_color'] = (isset($bcolor['n_c'])?$bcolor['n_c']:"");

  # if (empty($params['count'])) { $params['count'] = 0; }
   if (empty($params['zimmune'])) { $params['zimmune'] = 0; }
   if (empty($params['top'])) { $params['top'] = 0; }
   if (empty($params['meddle_queue'])) { $params['meddle_queue'] = 0; }
   if (empty($params['halloween_side'])) { $params['halloween_side'] = 0; }
   if (empty($params['rebornicon'])) { $params['rebornicon'] = 0; }

   if (in_array('12', explode(";", $params['eff_all']))) $hurt = 1; else $hurt = 0;

  # id ����� ��� ���������
    $attack = 0;

  # id ��� ��� ������
    if ($params['battle'] && $params['bot']) { $meddle = $params['id']; } else { $meddle = 0; }

	$data .= str_replace(' ', '_', mb_strtolower ($username)) . ','; # username
	$data .= @$params['id'] . ',';                                   # id
	$data .= $realName . ',';                                        # realName
	$data .= (@$params['align']?$params['align']:0) . ',';           # align
	$data .= ($params['sex']?"M":"F") . ',';                         # sex
	$data .= @$params['mentor_id'] . ',';                            # mentorId
	$data .= str_to_spacelower(@$params['klan']) . ',';              # clan
	$data .= @$params['klan'] . ',';                                 # clanTitle
	$data .= @$params['guild'] . ',';                                # guild
	$data .= @$params['guildTitle'] . ',';                           # guildTitle
	$data .= @$params['eff_silence'] . ',';                          # silence
	$data .= @$hurt.',';                                             # hurt
	$data .= @$params['eff_blin'] . ',';                             # blind
	$data .= @$params['eff_chain'] . ',';                            # hobble ����
	$data .= @$params['battle'] . ',';                               # battle
	$data .= @$params['alc_intoxication'] . ',';                     # alcohol  eff_drunk
	$data .= @$params['zside'] . ',';                                # ������� � ������� 1=������, 2=�������, 3=�������
	$data .= @$params['dungeon'] . ',';                              # dungeon
	$data .= @$params['hp'] . ',';                                   # hp
	$data .= @$params['maxhp'] . ',';                                # maxHP
	$data .= @$params['dungeon_x'] . ',';                            # dungeon_x
	$data .= @$params['dungeon_y'] . ',';                            # dungeon_y
	$data .= @$params['bot'] . ',';                                  # isBot
	$data .= (@$params['speaking']?"1":"0") . ',';                   # speaking
	$data .= @$params['level'] . ',';                                # level
	$data .= $attack . ',';                                          # attack
	$data .= $meddle . ',';                                          # meddle
	$data .= @$params['count'] . ',';                                # count
	$data .= @$params['zimmune'] . ',';                              # 0,1 ��������� � ��������� � ���� "�������"
	$data .= @$params['top'] . ',';                                  # topicon
	$data .= @$params['meddle_queue'] . ',';                         # meddle_queue
	$data .= @$params['halloween_side'] . ',';                       # halloween_side
	$data .= @$params['rebornicon'] . ',';                           # rebornicon
	$data .= @$params['nick_color'];                                 # nick_color

	$data .= "</data>\n";

	return $data;
}

/*
			userInfo[ 'username' ] = params[ 0 ];
			userInfo[ 'id' ] = params[ 1 ];
			userInfo[ 'realName' ] = params[ 2 ];
			userInfo[ 'align' ] = params[ 3 ];
			userInfo[ 'sex' ] = params[ 4 ];
			userInfo[ 'mentorId' ] = params[ 5 ];
			userInfo[ 'clan' ] = params[ 6 ];
			userInfo[ 'clanTitle' ] = params[ 7 ];
			userInfo[ 'guild' ] = params[ 8 ];
			userInfo[ 'guildTitle' ] = params[ 9 ];
			userInfo[ 'silence' ] = params[ 10 ];
			userInfo[ 'hurt' ] = params[ 11 ];
			userInfo[ 'blind' ] = params[ 12 ];
			userInfo[ 'hobble' ] = params[ 13 ];
			userInfo[ 'battle' ] = params[ 14 ];
			userInfo[ 'alcohol' ] = params[ 15 ];
			userInfo[ 'zside' ] = params[ 16 ];
			userInfo[ 'dungeon' ] = params[ 17 ];
			userInfo[ 'hp' ] = params[ 18 ];
			userInfo[ 'maxHP' ] = params[ 19 ];
			userInfo[ 'dungeon_x' ] = params[ 20 ];
			userInfo[ 'dungeon_y' ] = params[ 21 ];
			userInfo[ 'isBot' ] = params[ 22 ];
			userInfo[ 'speaking' ] = params[ 23 ];
			userInfo[ 'level' ] = params[ 24 ];
			userInfo[ 'attack' ] = params[ 25 ];
			userInfo[ 'meddle' ] = params[ 26 ];
			userInfo[ 'count' ] = params[ 27 ];
			userInfo[ 'zimmune' ] = params[ 28 ];
			userInfo[ 'topicon' ] = params[ 29 ] == 1 ? 1 : 0;
			userInfo[ 'meddle_queue' ] = params[ 30 ];
			userInfo[ 'halloween_side' ] = params[ 31 ];
			userInfo[ 'rebornicon' ] = params[ 32 ] == 1 ? 1 : 0;




*/



function show_s_bot($room){
  global $break_names, $npc;
 $data = "";

  $npc_file = $_SERVER["DOCUMENT_ROOT"].'/adata/qwest_NPC/npc.txt';

  if (is_file($npc_file)) {
    $n_npc = unserialize(file_get_contents($npc_file));       # ���������
  } else {
    $n_npc = $npc;
    file_put_contents($npc_file, serialize($n_npc), LOCK_EX); # ���������
  }

   if (!empty($n_npc[$room])) {
     foreach ($n_npc[$room] as $k => $row) {
      if (!empty($row['visible']) && $row['id']) {

        $row['bot'] = 1;
        $data .= xmlData($row);
        $break_names .= $row['login'].";";

        /*
        //  <li class="olist" id="online-user-<?= $ibot ?>" name="<?= $row['login'] ?>" data="<?= get_oUser($row) ?>" style=""><?= nick_1($row, 0,10); ?></li>
*/
      }
     }
   }

 return $data;

}

?>