<?
 $name_page = str_replace('/', '', str_replace('.php', '', $_SERVER["PHP_SELF"]));

/*
   setcookie("sdt", 111,(time()+5), "/", $_SERVER["HTTP_HOST"]);
 # Блок странички если нет куков
   if (empty(@$_COOKIE['sdt'])) { die("try later cooc"); }

    header('HTTP/1.1 403 Forbidden');
    die();

*/

# Если пусто
  if (empty($_SESSION["limit_requests"][$name_page]["upd"])) { $_SESSION["limit_requests"][$name_page]["upd"] = 0; }

 $stop              = false;
 $max_upd           = 0; # обновений странички
 $max_time_interval = 0; # секунд блокировано страничку

 switch ($name_page) {
   case "npc":
    $max_upd           = 60;  # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "klan":
    $max_upd           = 120; # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "map":
    $max_upd           = 120; # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "zayavka":
    $max_upd           = 120; # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "logs":
    $max_upd           = 60;  # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "inf":
    $max_upd           = 10;  # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "inventory":
    $max_upd           = 120; # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "auction":
    $max_upd           = 120; # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "enter":
    $max_upd           = 100;  # обновений странички
    $max_time_interval = 3600; # секунд блокировано страничку
   break;
   case "enter_npass":
    $max_upd           = 7;  # обновений странички
    $max_time_interval = 3600; # секунд блокировано страничку
   break;
   case "kcaptcha":
    $max_upd           = 10;  # обновений странички
    $max_time_interval = 180; # секунд блокировано страничку
   break;
   case "locator":
    $max_upd           = 10;  # обновений странички
    $max_time_interval = 15;  # секунд блокировано страничку
   break;
   case "crafting":
   // $max_upd           = 10;  # обновений странички
   // $max_time_interval = 15;  # секунд блокировано страничку
   break;
   case "shop":
    $max_upd           = 30;  # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "lotery":
    $max_upd           = 30;  # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;
   case "blutbad":
    $max_upd           = 10;  # обновений странички
    $max_time_interval = 60;  # секунд блокировано страничку
   break;

/*  default:
    $max_upd           = 60; # обновений странички
    $max_time_interval = 3600; # секунд блокировано страничку*/
 }


 if ($max_upd && $max_time_interval) {

    $_SESSION["limit_requests"][$name_page]["upd"] += 1;

      if (empty($_SESSION["limit_requests"][$name_page]["start_time"])) { $_SESSION["limit_requests"][$name_page]["start_time"] = time(); }

      if ($_SESSION["limit_requests"][$name_page]["start_time"] < time()) {
        $_SESSION["limit_requests"][$name_page]["start_time"] = time() + $max_time_interval;
        $_SESSION["limit_requests"][$name_page]["upd"] = 1;
      }

      $cur_start_time = $_SESSION["limit_requests"][$name_page]["start_time"];
      $cur_upd = $_SESSION["limit_requests"][$name_page]["upd"];

      if (($cur_upd >= $max_upd && $cur_start_time > time())) { $stop = true; }


 # Блок странички
   if ($stop) {
     die('
          <html>
            <link href="/css/main.css" rel="stylesheet" type="text/css">
          <head>
            <title>Страница заблокирована</title>
          </head>

          <body class="main-content">
               <p class="center bold">Превышен лимит запросов. <br>Ваш IP временно заблокирован</p>
          </body>
          </html>
     ');
   }
 }

?>