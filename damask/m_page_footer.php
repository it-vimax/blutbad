<? if ($user['level'] < 5 || $user['klan'] == 'Test' || $user['guild'] == 'Paladins') { ?>
<style type="text/css">#u_helper_body{z-index:3;position:fixed;background:#788b9e;opacity:0.8;width:100%;height:100%;top:0;left:0;cursor:pointer;display:none}
#u_helper{position:fixed;top:5px;right:5px;max-width:50%;z-index:5}
#u_h_post{min-width:300px}
#u_helper #u_h_butt{cursor:pointer;float:right}
#u_helper #u_h_block{min-width: 300px;border:1px solid #22384e;border-radius: 3px;float:left;background:#dadada;padding:5px;margin-right:10px}
#u_h_help{color:#1E9617;cursor:pointer}
.u_h_link b,#u_mlist li{cursor:pointer}
.u_h_link b:hover{text-decoration:underline;color:#74667A}
#u_mblock{padding:5px;border-left:2px solid #5f74fd;background:#f9f9f9;margin-bottom:5px;margin-top:5px;min-width:300px;display:none}
#u_mlist{padding-bottom:10px;padding-top:10px;padding-left:5px;min-width:300px}
#u_mlist ul{list-style:circle;padding-left:15px}
#u_mlist ul li{font-size: 12px;}
#u_mlist li:hover{text-decoration:underline}
#u_h_overlay{
  background-image: url(http://img.blutbad.ru/i/loader.gif);
    background-color: #d0dae4;
    display: none;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 2;
    background-position: 50% 50%;
    background-repeat: no-repeat;
    opacity: 0.7;
}

</style>
<script>
     $(function() {

        var u_h_open = false;
        var u_h_open_id = 0;

    	$("#u_h_butt, #u_helper_body").click(function(){
    	  var u_h_block = $('#u_h_block');
          if (u_h_block.css('display') == 'none') {
            if (u_h_open == false) {
              $("#u_h_link_list").click();
              u_h_open = true;
            }
            $('#u_helper_body').show();
            u_h_block.show().css({'margin-right':'5px'}).animate({'margin-right':'10px'});
            $('#u_h_overlay').hide();
          } else {
            u_h_block.hide();
            $('#u_helper_body').hide();
          }
    	});

        $("#u_h_link_back, #u_h_link_next, #list_rel").live('click', function() {
           $('#u_h_overlay').show();
           rel = $(this).attr('rel');
           u_h_open_id = rel;
           $(this).css({'list-style': 'none', 'opacity': '0.7'});
			$.post('/users_helper.php', {
				cmd: 'lnext',
				is_ajax: 1,
				cur_id: rel,
			}, function(json) {

              if (json.message) {
                $('#u_h_title').html(json.title);
                $('#u_h_text').html(json.message);
                $('#u_h_link_back').attr('rel', json.id_b);
                $('#u_h_link_next').attr('rel', json.id_n);

                $('#u_mblock, #u_h_link_back, #u_h_link_next, #u_h_link_list').show();

                $('#u_h_back, #u_mlist, #u_h_overlay').hide();
              }

			}, 'json');

    	});

        // �������� ������ ��������
    	$("#u_h_link_list").click(function(){

         if (u_h_open == false) {

           $('#u_h_overlay').show();
           rel = $(this).attr('rel');

			$.post('/users_helper.php', {
				cmd: 'llist',
				is_ajax: 1,
			}, function(json) {

              if (json.blist) {
                $('#u_mblock').hide();
                $('#u_h_link_back, #u_h_link_next, #u_h_post').hide();

                $('#u_mlist').empty();

                var li = '';
                for(var id in json.blist) { li += '<li id="list_rel" rel="'+json.blist[id].id+'">' + json.blist[id].t + '</li>'; }
                $('#u_mlist').append('<ul>' + li + '</ul>');

                $('#u_mlist, #u_h_back').show();
                $('#u_h_link_list, #u_h_overlay').hide();
              }

			}, 'json');

         } else {
                $('#u_mblock').hide();
                $('#u_h_link_back, #u_h_link_next, #u_h_post').hide();
                $('#u_mlist, #u_h_back').show();
                $('#u_h_link_list').hide();
         }

    	});

    	$("#u_h_back").click(function(){
    	  if (u_h_open_id > 0) {
            $('#u_mblock, #u_h_link_list, #u_h_link_back, #u_h_link_next').show();

            $('#u_mlist, #u_h_post').hide();
            $(this).hide();
    	  } else {
    	    alert("������� �������� ������");
    	  }
    	});

    	$("#u_h_help").click(function(){
            $('#u_h_post, #u_h_link_list').show();
            $('#u_mblock, #u_h_link_back, #u_h_link_next, #u_h_back, #u_mlist').hide();

    	});

        var t_hover = setInterval (function() {
         $('#u_h_butt').stop().animate({'opacity': '0.5'}).animate({'opacity': '1'});
         $('#u_helper').stop().animate({'top': '7px'}, 100).animate({'top': '5px'}, 100);
        }, 2000);
     });

</script>

<div id="u_helper">
    <img alt="������" id="u_h_butt" src="http://img.blutbad.ru/i/u_helper.png">
    <div id="u_h_block" style="display: none;">
      <div id="u_h_overlay"></div>
      <span style="font-size: 12px;"><b>������ �� ����.</b></span><b style="float: right"><span id="u_h_help">������ ���� ������!</span></b><br>
      <div id="u_mblock"><b><span id="u_h_title" style="font-size: 12px;border-bottom: 1px solid #5f74fd;display: block;margin-bottom: 0px;">����</span></b><br><span id="u_h_text" style="font-size: 12px;">����� �������� ������</span></div>
      <div id="u_mlist" style="display: none"></div>
      <div id="u_h_post" style="border-top: 1px solid #71818d;margin-top: 10px;padding-top: 5px;display: none">
        <form method="POST" action="users_helper.php">
           <input type="hidden" name="cmd" value="nuhelp">
           <table>
             <tr>
               <td style="vertical-align: middle;">����� �������: </td>
               <td><input name="nutitle" class="text" type="text" value="">&nbsp;?</td>
             </tr>
             <tr>
               <td>&nbsp;</td>
               <td style="padding-top: 5px;"><input class="xbbutton" type="submit" value="��������" title="��������"></td>
             </tr>
           </table>
        </form>
      </div>
      <div class="u_h_link" style="border-top: 1px solid #71818d;margin-top: 10px;padding-top: 5px;"><!--<b style="padding-right: 5px;display: none" id="u_h_link_back" rel="1">[����������]</b><b style="padding-left: 5px;display: none" id="u_h_link_next" rel="2">[���������]</b>--><b style="float: right" id="u_h_link_list">[������]</b><b style="float: right;display: none;" id="u_h_back">[�����]</b></div>
    </div>
</div>

<div id="u_helper_body"></div>

<?
}
  # ������� "������ ��������"
   unset($_SESSION['REQUESTS_PROTECTION']);

?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76353551-1', 'auto');
  ga('send', 'pageview');

</script>