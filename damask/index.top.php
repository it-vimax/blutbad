<?php




class antiDdos
{
    // �����
    public $debug = false;
    // ���������� ��� �������� ������ ����������� ��������
    public $dir = 'tmp/';
    // ����� icq ��������������
    public $icq = '7779077';
    // ��������� ��� ����������� �����
    public $off_message = '��������� ���������, ����������, ���������.';
    // �������������� �����������
    private $indeficator = null;
    // ��������� ��� ����, �������� �������, ����� ������������ - {ICQ}, {IP}, {UA}, {DATE}
    public $ban_message = '�� ���� �������������';
    // ������� ���������� ���� � ���������
    public $exec_ban = 'iptables -A INPUT -s {IP} -j DROP';
    // ��� ������ �� �����:
    /* ��������� �������� $ddos 1-5:
    | 1. ������� �������� �� �����, �� ���������(����������)
    | 2. ������� �������� ����� $_GET antiddos � meta refresh
    | 3. ������ �� ����������� WWW-Authenticate
    | 4. ������ ���������� �����, ���� �� �����������!!!
    | 5. ��������� ���� ���� �������� ������� ������� �� �������, ���� �� �����������!!!
    */
    var $ddos = 2;
    // ����� ������ ��������� �����, �� strpos()
    private $searchbots = array('googlebot.com', 'yandex.ru', 'ramtel.ru', 'rambler.ru', 'aport.ru', 'sape.ru', 'msn.com', 'yahoo.net');
    // ��������� ���������� ������ ��� ������ �������
    private $attack = false;
    private $is_bot = false;
    private $ddosuser;
    private $ddospass;
    private $load;
    public $maxload = 80;

    function __construct($debug)
    {
        @session_start() or die('session_start() filed!');
        $this->indeficator = md5(sha1('botik' . strrev(getenv('HTTP_USER_AGENT'))));
        $this->ban_message = str_replace(array('{ICQ}', '{IP}', '{UA}', '{DATE}'),
                                         array($this->icq, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], date('d.m.y H:i')),
                                         $this->ban_message
                                        );
        if (eregi(ip2long($_SERVER['REMOTE_ADDR']), file_get_contents($this->dir . 'banned_ips')))
            die($this->ban_message);
        $this->exec_ban = str_replace('{IP}', $_SERVER['REMOTE_ADDR'], $this->exec_ban);
        $this->debug = $debug;
        if(!function_exists('sys_getloadavg'))
        {
            function sys_getloadavg()
            {
                return array(0,0,0);
            }
        }
        $this->load = sys_getloadavg();
        if(!$this->sbots())
        {
            $this->attack = true;
            $f = fopen($this->dir . ip2long($_SERVER["REMOTE_ADDR"]), "a");
            fwrite($f, "query\n");
            fclose($f);
        }
    }

    /**
    * �������
    **/
    function start()
    {
        if($this->attack == false)
            return;
        switch($this->ddos)
        {
            case 1:
                $this->addos1();
                break;
            case 2:
                $this->addos2();
                break;
            case 3:
                $this->ddosuser = substr(ip2long($_SERVER['REMOTE_ADDR']), 0, 4);
                $this->ddospass = substr(ip2long($_SERVER['REMOTE_ADDR']), 4, strlen(ip2long($_SERVER['REMOTE_ADDR'])));
                $this->addos3();
                break;
            case 4:
                die($this->off_message);
                break;
            case 5:
                if ($this->load[0] > $this->maxload)
                {
                    header('HTTP/1.1 503 Too busy, try again later');
                    die('<center><h1>503 Server too busy.</h1></center><hr><small><i>Server too busy. Please try again later. Apache server on ' . $_SERVER['HTTP_HOST'] . ' at port 80</i></small>');
                }
                break;
            default:
                break;
        }
        if ($_COOKIE['ddos'] == $this->indeficator)
            @unlink($this->dir . ip2long($_SERVER["REMOTE_ADDR"]));
    }

    /**
    * ������� ��������� �� �������� �� ������ ��������� �����
    **/
    function sbots()
    {
        $tmp = array();
        foreach($this->searchbots as $bot)
        {
            $tmp[] = strpos(gethostbyaddr($_SERVER['REMOTE_ADDR']), $bot) !== false;
            if($tmp[count($tmp) - 1] == true)
            {
                $this->is_bot = true;
                break;
            }
        }
        return $this->is_bot;
    }

    /**
    * ������� ����
    **/
    private function ban()
    {
        if (! system($this->exec_ban))
        {
            $f = fopen($this->dir . 'banned_ips', "a");
            fwrite($f, ip2long($_SERVER['REMOTE_ADDR']) . '|');
            fclose($f);
        }
        die($this->ban_message);
    }
    /**
    * ������ ��� ������
    **/
    function addos1()
    {
        if (empty($_COOKIE['ddos']) or !isset($_COOKIE['ddos']))
        {
            $counter = @file($this->dir . ip2long($_SERVER["REMOTE_ADDR"]));
            setcookie('ddos', $this->indeficator, time() + 3600 * 24 * 7 * 356); // ������ ���� �� ���.
            if (count($counter) > 10) {
                if (! $this->debug)
                    $this->ban();
                else
                    die("�����������.");
            }
            if (! $_COOKIE['ddos_log'] == '1')
            {
                if (! $_GET['antiddos'] == 1)
                {
                    setcookie('ddos_log', '1', time() + 3600 * 24 * 7 * 356); //���� �� ������������ ��������� ��������.
                    if(headers_sent())
                        die('Header already sended, check it, line '.__LINE__);
                    header("Location: ./?antiddos=1");
                }
            }
        } elseif ($_COOKIE['ddos'] !== $this->indeficator)
        {
            if (! $this->debug)
                $this->ban();
            else
                die("�����������.");
        }
    }

    /**
    * ������ ��� ������
    **/
    function addos2()
    {
        if (empty($_COOKIE['ddos']) or $_COOKIE['ddos'] !== $this->indeficator)
        {
            if (empty($_GET['antiddos']))
            {
                if (! $_COOKIE['ddos_log'] == '1')
                    //�������� ���� �� ������ � ����� ��� ��� ������
                    die('<meta http-equiv="refresh" content="0;URL=?antiddos=' . $this->indeficator . '" />');
            } elseif ($_GET['antiddos'] == $this->indeficator)
            {
                setcookie('ddos', $this->indeficator, time() + 3600 * 24 * 7 * 356);
                setcookie('ddos_log', '1', time() + 3600 * 24 * 7 * 356); //���� ������ ��� ��� ���� �� ������������ ��������� ��������.
            }
            else
            {
                if (!$this->debug)
                    $this->ban();
                else
                {
                    echo "May be shall not transform address line?";
                    die("�����������.");
                }
            }
        }
    }

    /**
    * ������ ��� ������
    **/
    function addos3()
    {
        if (! isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] !== $this->ddosuser || $_SERVER['PHP_AUTH_PW'] !== $this->ddospass)
        {
            header('WWW-Authenticate: Basic realm="Vvedite parol\':  ' . $this->ddospass . ' | Login: ' . $this->ddosuser . '"');
            header('HTTP/1.0 401 Unauthorized');
            if (! $this->debug)
                $this->ban();
            else
                die("�����������.");
            die("<h1>401 Unauthorized</h1>");
        }
    }
}

$nowstart = time()-(mktime (0,0,0,date("m") ,date("d"),date("Y")));
$sdr = time()-$nowstart;
$timenow = time()-90;
# ������� �� �������
   $online_user_today = sql_number("SELECT real_time FROM online WHERE real_time >= '".$sdr."';");
# ������� ������
   $online_user = sql_number("SELECT * FROM users, online WHERE online.id = users.id AND online.real_time >= ".(time()-90)." AND users.bothidro = 0 ;");
# ����� ����� ������
   $online_hbotuser = sql_number("SELECT * FROM users, online WHERE online.id = users.id AND online.real_time >= ".(time()-90)." AND users.bothidro = 1 ;");
# �������� ������
   $count_bot = sql_number("SELECT * FROM `users` WHERE `bot` = 1;");
   $online_create_bot = $count_bot+sql_number("SELECT * FROM `bots`,`battle` WHERE `battle`.`win` = 3 AND `bots`.`hp` > 0 AND `battle`.`id` = `bots`.`battle`;");
# ������� ����
   $online_battle = sql_number("SELECT * FROM `battle` WHERE `win`='3' and `to1`>'".floor(time()-900)."' and `to2`>'".floor(time()-900)."' ORDER by `id` ASC;");

# ������� � ����
   $online_battle_user =0;
   $online_battle_user2 =0;
   $online_b_us = sql_query("SELECT * FROM `battle` WHERE `win`='3' and `to1`>'".floor(time()-900)."' and `to2`>'".floor(time()-900)."' ORDER by `id` ASC;");
 	while ($online_b_us_row = mysql_fetch_array($online_b_us)) {
     $arhbattle = mysql_fetch_array(mysql_query ('SELECT * FROM `battle` WHERE `id` = '.$online_b_us_row['id'].' LIMIT 1;'));

			  $arht1 = array(); // ������ �������
			  $arht2 = array(); // ������ �������
			  $arhbattleteams = array(); //������ � ������������

             $arhbattleteams = unserialize($arhbattle['teams']);
             $arht1 = explode(";",$arhbattle['t1']);
             $arht2 = explode(";",$arhbattle['t2']);

                    foreach ($arht1 as $k => $v) {
						if (in_array($v,array_keys($arhbattleteams))) {
                          $online_battle_user ++;
						}
					}
					foreach ($arht2 as $k => $v) {
						if (in_array($v,array_keys($arhbattleteams))) {
                            $online_battle_user2 ++;
						}
                    }
	}

# ������� � ���� ����������
   $online_battle_user = $online_battle_user+$online_battle_user2;

	$facs = array (
    "��������� ��������� ����� ������, ������� � 1 ������",
    "����� ���� ����� ������ � ������ ��������, �� ������ ������ �����, ��������� �� ��� � ����.",
    "����� ��� � �������� ����� ��������� ������������ ������� ����� � ������������.",
    "��� ��������� ����������� ����� ����������� ������ ����� 25%.",
    "��������� ������� �������� ����, ����������, ����������� ����, �� �������� ��������������� ���� � ���� ����������.",
    "�������� ������� �������� ��������������� ���� � ���� ����������, �������� ����������� ���� � ����������, ���� �� ��� ����� ������, �� � ��� ���� ����� ������.",
    "�� ���������� ����� ���� ���������� ����� ������ ���������.",
    "����������� ���������� � ������� �� ������� �� ����� �����.",
    "� ���� ��� ������� ����� ��������, ���� �� �� ������ 3 ������.",
    "����� ������, ������ � ������� ����� ����������� � ������� ����������. ������ ������� �� ������� � ������ �� 1 �� 4 - �������� ����, ������ ������� - ���� (�� 1 �� 6), ������ - ������� (1 ��� 2). ������� ESC �������� ��� ������������� �������.",
    "������� �� ����������, �� ������������ ��� �� ����� ������ �� ���.",
    "���� ����� �� �������� � ������� ����, ��� ���������.",
    "��� ������ �� ����-���� ���� �������� � �������������� ����� ������.",
    "��������� ��� �� ��������, ���� � ������ �� ��������� ������� ������� ��� �����.",
    "��������� � ��� � ������� ����� ������ �� ��� �� �������, � ������� ��������� ����.",
    "��� '��� ������' � ������ ��������� ������������� �������, ����������� ������� 75%.",
    "������� � ������� ���������� ��������� ������ ����� ����������� �������, ���� ���������, ���������� �� ����� www.bank.blutbad.ru",
    "���������� �� ������� �� ����� ����� ������ ��������� ����������������� ����, ������� ����� ����, ������� �� ������������ �� �����, �����.",
    "����������� �������� ���������������, ���� ������� ��� ������������� 5 ��� ������.",
    "� ���������� �������� ���� ������ ���������� ������ �����, � � ���������� �������� ���� - �������.",
    "���� ��� ��������� � ������� ������� ������� ������, ������ �� ��������� � ���.",
    "�������� ����������� ���������� �� ������ ������������ �� ��������� ������ ��� �������� ������.",
    "����� 20:00 ����� ���� ���� ����� ��� ������� � ��� ����� ������ ��� ����� � ������������.",
    "���� ����� ����� �������� ��� �� ������ �������� ����� �� 72 ����.",
    "��� ������ ��������� ������ �� ����� ���������, ��� ���� ����������� �������� ����� (���).",
    "��� ���� ����������� �������� �����, ��� ������ �� ��������� ����� � ������ ������ � ���.",
    "� ������ ������ � �������� ���, �� �������� �� 50% ����� ������, ��� � �������.",
    "������������ ����������� �������� � ������� �������� ������ ������� � 18 ��� � ������."
    );
 
$fac = $facs[mt_rand(0,count($facs)-1)];

?>
<html>
	<head>
		<title>
            <?= $title ?>
        </title>
		<meta http-equiv=Content-type content="text/html; charset=windows-1251">
        <meta name="description" content="��������������������� ������ ���� blutbad � ���������� MMORPG ������ ���������, ������ ��������� �� ���� ��������">
		<meta name="keywords" content="����, ������ ����, �������, ��������, ����� ������, ��������, ������ ����, ������ ����, ����������" />

        <link type="text/css" rel="StyleSheet" href="css/mst.css">
        <link type="text/css" rel="StyleSheet" href="c/reg.css">
<? if ($_SERVER["PHP_SELF"]=='/register.php') { ?>
<? } ?>
    	<script src="/js/jquery/jquery.js" type="text/javascript"></script>
    	<script src="/js/ind.js" type="text/javascript"></script>

<script type="text/javascript" src="http://vk.com/js/api/share.js?86" charset="windows-1251"></script>

<style type="text/css">
.mli a{
    font-size: 12px;
    font-weight: bold;
    color: #C5581A;
    text-decoration: none;
}
.mli a:hover {
text-decoration: underline;
color: #B3B3B3;
}

a {
color: #404040;
text-decoration: none;
font-size: 11px;
}
a, a:hover {
color: #FBD5C2;

text-decoration: none;
}

input.b_orang {
margin: .5em 3px;
border: 1px solid #797676;
border-radius: 3px;
padding: 0.3em 10px;
background-color: #727272;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #9C9C9C), color-stop(1, #727272));
background-image: -webkit-linear-gradient(top, #9C9C9C, #727272);
background-image: -moz-linear-gradient(top, #AE0, #580);
background-image: -o-linear-gradient(top, #AE0, #580);
background-image: -ms-linear-gradient(top, #AE0, #580);
background-image: linear-gradient(top, #AE0, #580);
font-weight: bold;
color: #201F1F;
cursor: pointer;
-webkit-box-shadow: 1px 1px 1px #A6A7AD, 1px 1px 1px #A6A7AD inset;
-moz-box-shadow: 0 1px 2px #888, 0 1px 2px #FFF inset;
box-shadow: 1px 1px 1px #A6A7AD, 1px 1px 1px #A6A7AD inset;
}


td{
  vertical-align: top;
}
#logo-swf {

outline: 0;
position: absolute;
top: 36px;
}
.text{
background-color: rgba(41, 41, 41, 0.5);
border: 1px solid #646464;
padding: 3px;
color: #DFCBCB;
}

.label{
vertical-align: middle;
padding-right: .2em;
text-align: right;
}
.fixed{
position: fixed;
top: 0px;
}
 .invisible{  display: none; }
</style>
<script language="JavaScript" type="text/javascript">
$(function() {
$(window).scroll(function () {
  var kscrollTop = $(document).scrollTop();
  var tab_r_b = $("#tab_r_b").height();
  var tab_l_b = $("#tab_l_b").height();
  var m_tb = $("#m_td").height();
                                 // 121
     if (kscrollTop > 260) {
         $("#tab_r_b").css({'paddingTop' : 0});
         $("#tab_r_b").addClass("fixed");
         $("#tab_l_b").css({'paddingTop' : 0});
         $("#tab_l_b").addClass("fixed");
     } else {
         $("#tab_r_b").removeClass("fixed");
         $("#tab_l_b").removeClass("fixed");
     }
     if (kscrollTop > m_tb-tab_r_b+260) {
         $("#tab_r_b").removeClass("fixed");
         $("#tab_r_b").css({'paddingTop' : m_tb-tab_r_b});
     }
     if (kscrollTop > m_tb-tab_l_b+260) {
         $("#tab_l_b").removeClass("fixed");
         $("#tab_l_b").css({'paddingTop' : m_tb-tab_l_b});
     }

});

$(window).resize(function () {
  var winh = $(window).height();
  var m_tb = 260 + $("#m_td").height();
  var tb_but = $("#tb_but").height();
   $("#tb_but").css({'paddingTop' : (winh-m_tb)-tb_but});

 if (winh >= 566) {

 //  alert("tb_but");
 }else{
   $("#tb_but").css({'paddingTop' : 0});
 }

});
$(window).load(function () {
$(window).resize();
});

});
</script>



</head>

<body style="background:#000000; margin:0px; padding:0px;height: 100%">
<div style="position:absolute;top:0px;left:0px;width:100%;padding:10px 0 10px 0;text-align:center;" class="mli">

<!--<img alt="" src="http://img.blutbad.ru/i/main/logo.png" border="0" style="background-repeat: no-repeat; width: 700px; height: 130px; margin-top: 30;">
--><br>

<?
/*<? if ($_SERVER["PHP_SELF"]=='/index.php') { ?>
<a href="javascript:register()" title="�����������">�����������</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<? } ?>
<? if ($_SERVER["PHP_SELF"]=='javascript:register()') { ?>
<a href="/" title="�������">�������</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<? } ?>
<a href="forum.php"  target="_blank" title="�����">�����</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="news.php" target="_blank" title="�������">�������</a>
*/
?>
<table cellpadding="0" cellspacing="0" border="0" style="width: 500px;  text-align: center;  height: 70px; margin: 0 auto; vertical-align: middle;">
  <tbody>
<tr><td style="background-repeat: no-repeat;height: 140px;background-image: url('http://img.blutbad.ru/i/main/logo.png');"><object type="application/x-shockwave-flash" data="http://img.blutbad.ru/i/main/star.swf" width="500" height="130" style="visibility: visible;"><param name="wmode" value="transparent"></object></td></tr>

</tbody>
</table>
<table cellpadding="0" cellspacing="0" border="0" style="max-width: 720px;white-space: nowrap; text-align: center;  height: 70px; margin: 0 auto; vertical-align: middle;">
  <tbody>
  <tr>
    <td style=" background-image: url(http://img.blutbad.ru/i/main/r.png); background-repeat: no-repeat; width: 165px;height: 67px"></td>
    <td style="white-space: nowrap; height: 67px; background-image: url(http://img.blutbad.ru/i/main/c.png); background-repeat: repeat-x;background-position: center;vertical-align: middle;">
<? if ($_SERVER["PHP_SELF"]=='/index.php') { ?>
<a href="javascript:register()" title="�����������">�����������</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<? } ?>
<? if ($_SERVER["PHP_SELF"]=='/register.php') { ?>
<a href="/" title="�������" alt="�������">�������</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<? } ?>
<a href="javascript:forum()"  target="_blank" title="�����" alt="�����">�����</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="news.php" target="_blank" title="�������" alt="�������">�������</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="http://lib.blutbad.ru/" target="_blank" title="����������" alt="����������">����������</a>
    </td>
    <td style="border: 0px solid; background-image: url(http://img.blutbad.ru/i/main/l.png); background-repeat: no-repeat; width: 168px;height: 67px"></td>
  </tr>
</tbody>
</table>
</div>
<table cellpadding="0" cellspacing="0" border="0" width="100%" height="260" style="min-width: 979px;background:url('http://img.blutbad.ru/i/main/2.gif');">
<tbody><tr><td align="left"><img src="http://img.blutbad.ru/i/main/3.jpg" border="0" alt=""></td><td align="right"><img src="http://img.blutbad.ru/i/main/4.jpg" border="0" alt=""></td></tr>
</tbody></table>
<div style="min-width: 100%; min-height: 100%; position: relative; background: url('http://img.blutbad.ru/i/main/but11w.jpg') 100% 100% no-repeat; min-height: 100%; ">

<div  style="margin: 0 auto; width: 979px;">
<table cellpadding="0" cellspacing="0" border="0" style=" height: 100%; margin: 0 auto; width: 979px;"><tbody><tr><td width="100%" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>

<td valign="top" width="215">
<div  id="tab_l_b"  class="qw">
<table border="0" cellpadding="0" cellspacing="0" width="215" style="background:url('http://img.blutbad.ru/i/main/5.png') no-repeat;">
<tbody><tr>
<td height="35" align="left" style="padding-left:20px;padding-top:21px;color: #FDCFA0;font-size: 12px;"><b>���������� ����</b></td></tr>
<tr>
<td style="padding: 20px 15px 5px 10px;">
<table style=" width: 100%; ">
  <tr>
    <td>������� ������</td>
    <td style=" text-align: right; "><b><?= $online_user + $online_hbotuser  ?></b></td>
  </tr>
  <tr>
    <td>������� �� �������</td>
    <td style=" text-align: right; "><b><?= $online_user_today+$online_hbotuser ?></b><br><br></td>
  </tr>
  <tr>
    <td>�������� ������</td>
    <td style=" text-align: right; "><b><?= $online_create_bot-$online_hbotuser ?></b><br><br></td>
  </tr>
  <tr>
    <td>������� ����:</td>
    <td style=" text-align: right; "><b><?= $online_battle ?></b></td>
  </tr>
  <tr>
    <td>������� � ����:</td>
    <td style=" text-align: right; "><b><?= $online_battle_user ?></b><br><br></td>
  </tr>
</table>
</td></tr>
</tbody>
</table>
<table style="background:url('http://img.blutbad.ru/i/main/5.1.png') no-repeat;height: 80px;width: 215px;margin-top: -50px;">
<tr><td></td></tr>
</table>

</div>

</td>

<td valign="top" height="100%">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%" id="m_td">
<tbody><tr><td width="4" height="4"><img src="http://img.blutbad.ru/i/main/6.gif" border="0" alt=""></td>
<td height="4" style="background:url('http://img.blutbad.ru/i/main/7.gif');"></td>
<td width="4" height="4"><img src="http://img.blutbad.ru/i/main/8.gif" border="0" alt=""></td></tr>
<tr><td style="background:url('http://img.blutbad.ru/i/main/9.gif');max-width: 4px;"></td>
<td style="background:url('http://img.blutbad.ru/i/main/10.jpg');padding:10px; text-align:left;width: 570px" valign="top">