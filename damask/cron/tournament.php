<?php
$tour_start = 0;
$tour_start_day = 0;
$istour     = 1;
$str_tour   = '';

# ------------------
# �� 2016 ���
# ------------------

# ��������� ������ "������"
  if (date("m") == 2) { if (in_array(date("d"), array(9, 2, 3, 4, 5, 6, 7, 8))) $tour_start = 1; $tour_start_day = 2; }
# ��������� ������ "������"
  if (date("m") == 4) { if (in_array(date("d"), array(17, 18, 19, 20, 21, 22, 23))) $tour_start = 1; $tour_start_day = 17; }
# ��������� ������ "����"
  if (date("m") == 7) { if (in_array(date("d"), array(17, 18, 19, 20, 21, 22, 23))) $tour_start = 1; $tour_start_day = 17; }
# ��������� ������ "�������"
  if (date("m") == 10) { if (in_array(date("d"), array(16, 17, 18, 19, 20, 21, 22))) $tour_start = 1; $tour_start_day = 16; }


# ��������� ������
  if ($tour_start) {
   $str_tour .= "tour_start<br>";
   switch (INCITY) {
     case "damask":
       $id_tour_8_9   = 6; # �� ������� ����� 8-9 ��������
       $id_tour_10_11 = 5; # �� ������� ����� 10-11 ��������
       $id_tour_12_13 = 4; # �� ������� ����� 12-13 ��������
     break;
     case "alamut":
       $id_tour_8_9   = 0; # �� ������� ����� 8-9 ��������
       $id_tour_10_11 = 0; # �� ������� ����� 10-11 ��������
       $id_tour_12_13 = 0; # �� ������� ����� 12-13 ��������
     break;
     case "calydon":
       $id_tour_8_9   = 3; # �� ������� ����� 8-9 ��������
       $id_tour_10_11 = 2; # �� ������� ����� 10-11 ��������
       $id_tour_12_13 = 1; # �� ������� ����� 12-13 ��������
     break;
   }


  # ������ ��������� �������
    $lvl_tour = array(
       $id_tour_12_13 => array("min_lvl" => 12, "max_lvl" => 13),
       $id_tour_10_11  => array("min_lvl" => 10,  "max_lvl" => 11),
       $id_tour_8_9   => array("min_lvl" => 8,  "max_lvl" => 9),
    );

  # ����� ������ � ��������� ������� �������
    $start_times_tour = array(

     # 10-12 ������
       "13:00" => array("name" => "1/16 ������", "next_tour" => "1/8 ������",  "id" => $id_tour_12_13, "oldid" => $id_tour_10_11,   "status" => 1),
       "14:30" => array("name" => "1/8 ������",  "next_tour" => "1/4 ������",  "id" => $id_tour_12_13, "oldid" => $id_tour_10_11,   "status" => 3),
       "16:00" => array("name" => "1/4 ������",  "next_tour" => "���������",   "id" => $id_tour_12_13, "oldid" => $id_tour_10_11,   "status" => 3),
       "17:30" => array("name" => "���������",   "next_tour" => "�����",       "id" => $id_tour_12_13, "oldid" => $id_tour_10_11,   "status" => 4),
       "19:00" => array("name" => "�����",       "next_tour" => "",            "id" => $id_tour_12_13, "oldid" => $id_tour_10_11,   "status" => 5),

     # 8-9 ������
       "12:30" => array("name" => "1/16 ������", "next_tour" => "1/8 ������",  "id" => $id_tour_10_11,   "oldid" => $id_tour_8_9,   "status" => 1),
       "14:00" => array("name" => "1/8 ������",  "next_tour" => "1/4 ������",  "id" => $id_tour_10_11,   "oldid" => $id_tour_8_9,   "status" => 2),
       "15:30" => array("name" => "1/4 ������",  "next_tour" => "���������",   "id" => $id_tour_10_11,   "oldid" => $id_tour_8_9,   "status" => 3),
       "17:00" => array("name" => "���������",   "next_tour" => "�����",       "id" => $id_tour_10_11,   "oldid" => $id_tour_8_9,   "status" => 4),
       "18:30" => array("name" => "�����",       "next_tour" => "",            "id" => $id_tour_10_11,   "oldid" => $id_tour_8_9,   "status" => 5),

     # 6-7 ������
       "12:00" => array("name" => "1/16 ������", "next_tour" => "1/8 ������",  "id" => $id_tour_8_9,   "oldid" => 0,              "status" => 1), # ��� old ��� ��� ��� ��� ������ ������
       "13:30" => array("name" => "1/8 ������",  "next_tour" => "1/4 ������",  "id" => $id_tour_8_9,   "oldid" => $id_tour_12_13, "status" => 2),
       "15:00" => array("name" => "1/4 ������",  "next_tour" => "���������",   "id" => $id_tour_8_9,   "oldid" => $id_tour_12_13, "status" => 3),
       "16:30" => array("name" => "���������",   "next_tour" => "�����",       "id" => $id_tour_8_9,   "oldid" => $id_tour_12_13, "status" => 4),
       "18:00" => array("name" => "�����",       "next_tour" => "",            "id" => $id_tour_8_9,   "oldid" => $id_tour_12_13, "status" => 5),

     # ����� ���������� �������
       "19:30" => array("name" => "�����", "next_tour" => "",       "id" => 0, "oldid" => $id_tour_12_13, "status" => 5),
     #  "10:02" => array("name" => "1/16 ������", "next_tour" => "1/8 ������", "id" => $id_tour_10_11,   "oldid" => $id_tour_12_13, "status" => 1),
    );

  # ������� �����
    $cur_time = date("H").":".date("i");

  # ������ ��� ������������
//    $cur_time = "12:00";


  # ������� �� ������� ������ ���
    $cur_tour_oldid  = @$start_times_tour[$cur_time]['oldid'];
  # ������� �� ������� ������ ���
    $cur_tour_next_tour  = @$start_times_tour[$cur_time]['next_tour'];

  # ������� �� �������
    $cur_tour_id     = @$start_times_tour[$cur_time]['id'];
  # ������� �������� �������
    $cur_tour_name   = @$start_times_tour[$cur_time]['name'];
  # ������� ������ �������
    $cur_tour_status = @$start_times_tour[$cur_time]['status'];



   $str_tour .= "id_tour_8_9 - ".$id_tour_8_9."<br>";
   $str_tour .= "id_tour_10_11 - ".$id_tour_10_11."<br>";
   $str_tour .= "id_tour_12_13 - ".$id_tour_12_13."<br>";

   $str_tour .= "cur_tour_oldid - ".$cur_tour_oldid."<br>";
   $str_tour .= "cur_tour_next_tour - ".$cur_tour_next_tour."<br>";
   $str_tour .= "cur_tour_id - ".$cur_tour_id."<br>";
   $str_tour .= "cur_tour_name - ".$cur_tour_name."<br>";
   $str_tour .= "cur_tour_status - ".$cur_tour_status."<br>";


        /***------------------------------------------
         * C��������� ���! ������ ���������� ���
         **/

		function tour_battle_start($id_cur_zay, $tools, $team_1, $team_2, $timeout, $type, $blood, $hideb, $closed) {
		  global $user;

              # -----------------------------------
              # ���� ���� � ������������ ������
				$all = count($team_1) - 1;
				$power1 = 0;
                $power2 = 0;

                $eff_invis_ids = ""; # Ids ������ ������� ������ � ������ � �����

			  # ��������� �������
				for($i = 0; $i <= $all; $i++) {
                    $hb_id = $team_1[$i];
                    if (is_numeric($hb_id)) {
  				     $gamer = sql_row("SELECT (level*50)+sila+lovk+inta+vinos+intel+mudra+inv_strength+inv_dexterity+inv_success+inv_intelligence+stats+stuff_cost as userpower FROM `users` WHERE `id` = '".$hb_id."' LIMIT 1;");

                     switch ($gamer['level']) {
                       case 11: $gamer['userpower'] *= 2;  break;
                       case 12: $gamer['userpower'] *= 3;  break;
                       case 13: $gamer['userpower'] *= 5; break;
                       case 14: $gamer['userpower'] *= 9; break;
                     }

  					 $cost[] = array($team_1[$i], $gamer['userpower']);
                    }
				}

			    $team_1 = null;
                $team_2 = null;
				$flag   = true;
				rsort($cost);
				while($flag) {
					$flag = false;
					for($ii = 0; $ii <= $all-1; $ii++){
						if($cost[$ii][1] < $cost[$ii+1][1]) {
						  $ctr = $cost[$ii+1];
						  $cost[$ii+1] = $cost[$ii];
						  $cost[$ii] = $ctr;
						  $flag = true;
						}
					}
				}

				while (count($cost) > 0) {
					if ($power1 <= $power2) {
						$tmp      = array_shift($cost);
						$power1  += $tmp[1];        # power++
						$team_1[] = $tmp[0];        # to command
					} else {
						$tmp      = array_shift($cost);
						$power2  += $tmp[1];        # power++
						$team_2[] = $tmp[0];        # to command
					}
				}

              # -----------------------------------

              //
              // ��� ������� ������ ������� ��� ����� ���
              //

             $list_1 = array();
             $list_2 = array();

            # �������� ������ ������� �� ������������� ������� ����� � ������ � ����
               if ($team_1) {
                $hb_teams_new_t1 = array();
  				 foreach($team_1 as $k => $v_id) {
                      $zay_user = sql_row("SELECT `id`, `login`, `level`, `align`, `guild`, `klan`, `sex`, `incity`, `hp`, `maxhp`, `mana`, `maxmana`, `top`, `invis`, `shit` FROM users WHERE id = '".$v_id."' AND `battle` = 0 LIMIT 1;");
                      if (!empty($zay_user['id'])) {
                          $nick_1 = nick_ofset($zay_user, 1, 1);
                          $nick_1['wep_type'] = '';
                          $nick_1['shit']    = $zay_user['shit']?1:0;
                          $nick_1['exp']     = 0;
                          $nick_1['damage']  = 0;
                          $nick_1['karma']   = 0;
                          $nick_1['valor']   = 0;

                          $nick_1['i']       = 1; # � �����������

                          $list_1[$v_id]     = $nick_1;
                          $hb_teams_new_t1[] = $v_id;

                        # ������ �����������
                          $h_time = time()+(15*60);
                          $eff_invis_ids .= ($eff_invis_ids?",":"").$eff_invis_ids;
                          sql_query("INSERT INTO `effects` (`owner`,`name`,`time`,`type`,`opisan`,`from_whom`) values ('".$v_id."', '��������� �����������', '{$h_time}', 1022, '��� �� ����� ������ ������ �� ����� ��� &asymp;15 ����� ����� ���', '������');");

                      }
  				 }
                $team_1 = $hb_teams_new_t1;
               }

            # �������� ������ ������� �� ������������� ������� ����� � ������ � ����
               if ($team_2) {
                $hb_teams_new_t2 = array();
  				 foreach($team_2 as $k => $v_id) {
                      $zay_user = sql_row("SELECT `id`, `login`, `level`, `align`, `guild`, `klan`, `sex`, `incity`, `hp`, `maxhp`, `mana`, `maxmana`, `top`, `invis`, `shit` FROM users WHERE id = '".$v_id."' AND `battle` = 0 LIMIT 1;");
                      if (!empty($zay_user['id'])) {
                          $nick_2 = nick_ofset($zay_user, 1, 2);
                          $nick_2['wep_type'] = '';
                          $nick_2['shit']    = $zay_user['shit']?1:0;
                          $nick_2['exp']     = 0;
                          $nick_2['damage']  = 0;
                          $nick_2['karma']   = 0;
                          $nick_2['valor']   = 0;

                          $nick_2['i']       = 1; # � �����������

                          $list_2[$v_id]     = $nick_2;
                          $hb_teams_new_t2[] = $v_id;

                        # ������ �����������
                          $h_time = time()+(15*60);
                          $eff_invis_ids .= ($eff_invis_ids?",":"").$eff_invis_ids;
                          sql_query("INSERT INTO `effects` (`owner`,`name`,`time`,`type`,`opisan`,`from_whom`) values ('".$v_id."', '��������� �����������', '{$h_time}', 1022, '��� �� ����� ������ ������ �� ����� ��� &asymp;15 ����� ����� ���', '������');");
                      }
                 }
                $team_2 = $hb_teams_new_t2;
               }

               # ������ ���
    		    $teams = array();
                 if ($team_1 && $team_2) {

    			  # ����� �� ������ �������
    				foreach($team_1 as $k => $v) { foreach($team_2 as $kk => $vv) { $teams[$v][$vv] = array(0,0,0,time()); } }
    			  # ����� �� ������ �������
    				foreach($team_2 as $k => $v) { foreach($team_1 as $kk => $vv) { $teams[$v][$vv] = array(0,0,0,time()); } }

        			if (count($teams) > 1) {

                        if ($tools["tour_name"] == "�����") {
                           $auto_completion = time() + 3*60*60;
                        } else {
                           $auto_completion = time() + 60*60;
                        }

        				sql_query("INSERT INTO `battle`
        						(
        							`teams`,`timeout`,`type`,`t1`,`t2`,`to1`,`to2`,`t1list`,`t2list`,`tools`,`blood`,`hideb`,`closed`, `startbattle`, `city`, `auto_completion`
        						)
        						VALUES
        						(
        							'".serialize($teams)."','".$timeout."','".$type."','".implode(";", $team_1)."','".implode(";", $team_2)."','".time()."','".time()."','".serialize($list_1)."','".serialize($list_2)."','".serialize($tools)."','".$blood."','".$hideb."','".$closed."','".time()."','".INCITY."','".$auto_completion."'
        						)");

                      # �� ������ ���������� ���
        				$batl_id = sql_insert_id();

                        if ($batl_id) {

        				# ������� ���
                          $joinuser = array();
        				  $rr = "";
        				  $rrc = "";
        				  foreach($team_1 as $k => $v ) {
        					if ($k != 0) { $rr .= ", "; $rrc .= ", "; }
                            $v_login = $list_1[$v]['i']?"���������":$list_1[$v]['n'];
        					  $rr .= nick_ofgethtml($list_1[$v], 0, 1);
        				      $rrc .= '<b>'.$v_login.'</b>';
                              if ($v < _BOTSEPARATOR_) {
                                $jlogin['login_jaber'] = CHAT::chat_format_login($list_1[$v]['n']);
                                if ($v != $my_zay) {
                                  CHAT::chat_command($jlogin, "sound|type=1");
                                  CHAT::chat_command($jlogin, "refresh|frame=main|url=/fbattle.php");
                                }
                              # ���������� � ������� ������ � ���� ����
                                $joinuser[] = ['j_login' => $jlogin['login_jaber'], 'side' => 1];
                              }
        				  }

        				  $rr .= " � ";
                          $rrc .= " � ";
        				  foreach($team_2 as $k => $v ) {
        					if ($k!=0) { $rr .= ", "; $rrc .= ", ";}
                            $v_login = $list_2[$v]['i']?"���������":$list_2[$v]['n'];
            				  $rr .= nick_ofgethtml($list_2[$v], 0, 2);
            				  $rrc .= '<b>'.$v_login.'</b>';
                              if ($v < _BOTSEPARATOR_) {
                                $jlogin['login_jaber'] = CHAT::chat_format_login($list_2[$v]['n']);
                                if ($v != $my_zay) {
                                  CHAT::chat_command($jlogin, "sound|type=1");
                                  CHAT::chat_command($jlogin, "refresh|frame=main|url=/fbattle.php");
                                }
                              # ���������� � ������� ������ � ���� ����
                                $joinuser[] = ['j_login' => $jlogin['login_jaber'], 'side' => 2];
                              }
        				  }

        				  $rr .= "";
        				  CHAT::chat_system($user, "<a href=\"/log=".$batl_id."\" target=_blank><b>���</b></a> ����� ".$rrc." �������. ", 0, array('addXMPP' => true));

                          addwrite_log($batl_id);
           			      addlog($batl_id, '������� ��� ����� '.$rr);
                          addwrite_log($batl_id);

                        # ���������� ���� � ������ � ���� ���
                          CHAT::chat_battle_c_glb($batl_id, array('addXMPP' => true, 'joinuser' => $joinuser));
                          $battle_log = date("H:i:s")."|".rand(1, 100)."|0|0|".""."������� ��� ����� ".$rr;
                          CHAT::chat_battle_add_lb($battle_log, $batl_id, array('addXMPP' => true));

                        # ���� � ���
                          sql_query("UPDATE `users` SET `battle` = '".$batl_id."', `zayavka` = 0, `tournament` = 0, `invis` = 1, `eff_all` = CONCAT(`eff_all`, '1022;') WHERE `zayavka` = '".$id_cur_zay."' AND `battle` = 0");

                        # ������ ������ ���� ������ �� ���� ���
                          if ($eff_invis_ids) {
                            $eff_tools = array("battle_tour_id" => $batl_id);
                            sql_query("UPDATE `effects` SET `tools` = '".serialize($eff_tools)."' WHERE `owner` IN (".$eff_invis_ids.") AND `type` = 1022 AND `name` = '��������� �����������'");
                          }

                        }

        			}
                 }
		}



/***------------------------------------------
 * ������ ���������� ������� ������ � 12:00
 **/

  if ($cur_time == "12:00" && $id_tour_8_9 && $id_tour_10_11 && $id_tour_12_13) {

          # ����� ������� 8-9 �������
            $tour_create = time() + (30*60); # 30 ��� �� ������ ����. �������
            sql_query("UPDATE `zayavka_tournament` SET `tour_start` = 1, `tour_name` = '1/16 ������', `team1` = '', `status` = 1, `tour_create` = '".$tour_create."', `tour_date_creare` = '".time()."' WHERE `id` = ".$id_tour_8_9." AND `incity` = '".INCITY."' LIMIT 1;");

          # ����� ������� 10-11 �������
            $tour_create = time() + (60*60); # 60 ��� �� ������ ����. �������
            sql_query("UPDATE `zayavka_tournament` SET `tour_start` = 1, `tour_name` = '1/16 ������', `team1` = '', `status` = 1, `tour_create` = '".$tour_create."', `tour_date_creare` = '".time()."' WHERE `id` = ".$id_tour_10_11." AND `incity` = '".INCITY."' LIMIT 1;");

          # ����� ������� 12-13 �������
            $tour_create = time() + (90*60); # 90 ��� �� ������ ����. �������
            sql_query("UPDATE `zayavka_tournament` SET `tour_start` = 1, `tour_name` = '1/16 ������', `team1` = '', `status` = 1, `tour_create` = '".$tour_create."', `tour_date_creare` = '".time()."' WHERE `id` = ".$id_tour_12_13." AND `incity` = '".INCITY."' LIMIT 1;");

          # ��������� ���� �� ��������
            sql_query("UPDATE `users` SET `zayavka` = 0, `tournament` = 0 WHERE `tournament` > 0 ;");

          # �������� ���������
           if ($tour_start_day == date("d")) {
              $users_oldrep = sql_query("SELECT `id`, `rep` FROM `users` WHERE `bot` = 0 AND `level` >= 7 AND `rep` LIKE '%league_gladiators%';");
              while ($row = fetch_array($users_oldrep)) {
                 $rep = unserialize($row['rep']);
                 if (isset($rep['league_gladiators']['glasses'])) {
                   $rep['league_gladiators']['wins'] = 0;
                   $rep['league_gladiators']['glasses'] = 0;
                   sql_query("UPDATE `users` SET `rep` = '".serialize($rep)."' WHERE `id` = '".$row['id']."' LIMIT 1;");
                 }
              }
           }

          # ����� br ��� ��� �������
            $user_adm = array();
            $user_adm['login_jaber'] = 'br';
            CHAT::chat_attention($user_adm, '���������� � ���� ��������� ���������� � ���� ������� � 12,00', array('addXMPP' => true));

  }



/***------------------------------------------
 * ����� ���������� ���������� ���
 **/

     if ($cur_tour_oldid && $cur_tour_name && $istour) {
        $tour_to_start = sql_row("SELECT * FROM `zayavka_tournament` WHERE `id` = ".$cur_tour_oldid." AND `status` >= 1 AND `incity` = '".INCITY."' LIMIT 1;");

        if ($tour_to_start['id']) {

            $minlvl        = $tour_to_start['min_lvl'];
            $maxlvl        = $tour_to_start['max_lvl'];
            $old_tour_name = $cur_tour_next_tour; # $tour_to_start['tour_name'];

          # �������� ������ �������
            $team_1  = explode(";", $tour_to_start['team1']);
          # �������� ������ �������
            $team_2  = array();

          # ����������� ���������� ���������
            $team_min_users  = 4;

            # ����� ���� ���� 4 ��������
              if (count($team_1) >= $team_min_users) {

                $tools                      = array();
                $tools["break_items"]       = 1;       # ���� �� ��������
                $tools["exp_percent"]       = 100;     # +100 � �����
                $tools["no_exclusive_rune"] = 1;       # ��� ��� ���
                $tools["no_shop_rune"]      = 1;       # ��� ��� ��������
                $tools["no_ng_rune"]        = 1;       # ��� �� ���
                $tools["no_ps"]             = 1;       # ��� ������� �� �������� �����
                $tools["no_ability"]        = 1;       # ��� ������
                $tools["no_klan_ability"]   = 1;       # ��� �������� ������
                $tools["no_animals"]        = 1;       # ��� ��������
                $tools["no_valor"]          = 1;       # ��� ��������
                $tools["tour_name"]         = $tour_to_start['tour_name']; # �������� �������

                $timeout = 5;
                $type    = 27;
                $blood   = 0;
                $hideb   = 0;
                $closed  = 1;

              # ����� br ��� ��� �������
                $user_adm = array();
                $user_adm['login_jaber'] = 'br';
                CHAT::chat_attention($user_adm, '111 tour '.$cur_tour_oldid.' team_1='.$tour_to_start['team1'].' ����� '.serialize($team_1).'', array('addXMPP' => true));

              # ����� ���������� ���
                tour_battle_start($cur_tour_oldid, $tools, $team_1, $team_2, $timeout, $type, $blood, $hideb, $closed);

              # ����� � ��� ����
                CHAT::chat_city_attention(false, '������� ��������� ��������� ��� <b>'.$tour_to_start['tour_name'].'</b> ��� ������� <b>'.$minlvl.'</b> - <b>'.$maxlvl.'</b> �������.', array('addXMPP' => true, 'incity' => INCITY));
              }

            # �������� ������� �������
              $tour_create = time() + (90*60); # 90 ���. �� ������ ����. �������
              $tour_start  = 1;                # �������� ������; 1 = �������� | 0 = ���

              if ($tour_to_start['tour_name'] == "�����") { $tour_start = 0; } # ��������� ������ ���� "�����"

            # �������� ������� �������
              sql_query("UPDATE `zayavka_tournament` SET `tour_start` = ".$tour_start.", `tour_name` = '".$old_tour_name."', `team1` = '', `status` = '".$cur_tour_status."', `tour_create` = '".$tour_create."' WHERE `id` = '".$cur_tour_oldid."' LIMIT 1;");

            # ��������� ���� �� ��������
              sql_query("UPDATE `users` SET `zayavka` = 0, `tournament` = 0 WHERE `tournament` = ".$cur_tour_oldid." ;");

          }

     }



/***------------------------------------------
 * ����� ������ �������
 **/

     if ($cur_tour_id && $cur_tour_name && $istour) {
        $minlvl = $lvl_tour[$cur_tour_id]['min_lvl'];
        $maxlvl = $lvl_tour[$cur_tour_id]['max_lvl'];

      # ����� �������
        $team_id     = 1;
      #  $teams       = "";
        $tour_create = time() + (30*60); # 30 ��� �� ������ �������

       // sql_query("UPDATE `zayavka_tournament` SET `tour_start` = 1, `tour_name` = '".$cur_tour_name."', `team".$team_id."` = '".$teams."', `status` = '".$cur_tour_status."', `tour_create` = '".$tour_create."' WHERE `id` = '".$cur_tour_id."' LIMIT 1;");
         sql_query("UPDATE `zayavka_tournament` SET `tour_start` = 1, `tour_name` = '".$cur_tour_name."', `status` = '".$cur_tour_status."', `tour_create` = '".$tour_create."' WHERE `id` = '".$cur_tour_id."' LIMIT 1;");
         CHAT::chat_city_attention(false, '������� ��������� ������ <b>'.$cur_tour_name.'</b> ��� ������� <b>'.$minlvl.'</b> - <b>'.$maxlvl.'</b> �������.', array('addXMPP' => true, 'incity' => INCITY));
     }

  }

#  echo $str_tour;

?>