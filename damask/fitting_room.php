<?
  include("a_process_start.php");
  session_start();
  if ($_SESSION['uid'] == null) header("Location: index.php");
  include "connect.php";
  include "functions.php";

 include("functions/dungeon/dungeon_bots.php");

function cp1251_to_utf($str) {
 if ($str) {
    return iconv('cp1251', 'utf-8', $str);
 }
 return $str;
}

/***------------------------------------------
 * �������� �����
 **/

if (isset($_POST['is_ajax']) && $_POST['cmd'] == 'load_bot') {

 $json_error = '';

 $id_bot   = abs($_POST['id']);
 $data_bot = array();
 $people   = $_POST['people'];

 foreach ($DUNGEON_BOTS as $k => $v_data) {
  if ($v_data['id'] == $id_bot && empty($v_data['qwestbot'])) { $data_bot = $v_data; break; }
 }

  if (empty($data_bot)) {
   $json_error = '��� �� ������';
  // $json_error = cp1251_to_utf($json_error);;
      echo '<xml>';
          echo '<body>';
              echo '<error>'.$json_error.$_POST['id'].'</error>';
              echo '<message>111</message>';
              echo '<user_money>'.number_format($user['money'],2,"."," ").'</user_money>';
          echo '</body>';
      echo '</xml>';
  } else {
    echo '<?xml version="1.0" encoding="utf-8"?>';

    echo '<bot>';
    	echo '<name>��������</name>';
    	echo '<id>'.$data_bot['id'].'</id>';
    	echo '<people>'.$people.'</people>';
    	echo '<sex>'.($data_bot['sex']?"M":"F").'</sex>';
    	echo '<obraz>obraz/'.$data_bot['obraz'].'</obraz>';
    	echo '<level>'.$data_bot['level'].'</level>';
    	echo '<strength>'.$data_bot['sila'].'</strength>';
    	echo '<dexterity>'.$data_bot['lovk'].'</dexterity>';
    	echo '<success>'.$data_bot['inta'].'</success>';
    	echo '<endurance>'.$data_bot['vinos'].'</endurance>';
    	echo '<intelligence>0</intelligence>';
    	echo '<wisdom>0</wisdom>';
    echo '</bot>';
  }
die();
}

/***------------------------------------------
 * �������� ��������
 **/

if (@$_POST['is_ajax'] && @$_POST['cmd']=='fight') {

   //  header('Content-Type: text/xml; charset=UTF-8');

  $json_error   = '';     # ������ ��� ���������
  $json_message = '';   #
  $fight_counts = abs($_POST['counts']);

  if ($_POST['ext'] == 2) {
   $ncost = abs(0.2 * $fight_counts);
  } else {
    $ncost = abs(0.1 * $fight_counts);
  }

  $left_array  = $_POST['left'];
  $right_array = $_POST['right'];

  $left_user   = explode(':',$left_array);
  $right_user  = explode(':',$right_array);

  if (!in_array($fight_counts, array(1, 10, 100, 1000))) {
   $json_error   = ('������������ ���������� ����');
   $json_message = ('������������ ���������� ����');
      echo '<xml>';
          echo '<body>';

              echo '<error>'.$json_error.'</error>';
              echo '<message>111</message>';
              echo '<user_money>'.number_format($user['money'],2,"."," ").'</user_money>';
          echo '</body>';
      echo '</xml>';
  } elseif ($user['money'] < $ncost || $ncost <= 0) {
   $json_error   = ('�� ���������� ��������');
   $json_message = ('�� ���������� ��������');
      echo '<xml>';
          echo '<body>';

              echo '<error>'.$json_error.'</error>';
              echo '<message>111</message>';
              echo '<user_money>'.number_format($user['money'],2,"."," ").'</user_money>';
          echo '</body>';
      echo '</xml>';
  } elseif (($left_user[0] == 1 && $right_user[0] != 1) || ($left_user[0] != 1 && $right_user[0] == 1)) {
   $json_error   = ('������ �� ������');
   $json_message = ('������ �� ������');
      echo '<xml>';
          echo '<body>';

              echo '<error>'.$json_error.'</error>';
              echo '<message>111</message>';
              echo '<user_money>'.number_format($user['money'],2,"."," ").'</user_money>';
          echo '</body>';
      echo '</xml>';
  } else {

   include("fitting_room.fight.php");

   $json_message = cp1251_to_utf($json_message);
   $json_error = cp1251_to_utf($json_error);;

      $all_udar_count = 0;
      $sleft = '';
      $sright = '';

      $all_hp_l = 0;
      $all_maxhp_l = 0;
      $all_pw_l = 0;
      $all_maxpw_l = 0;
      $all_uron_l_count = 0;
      $all_udar_l_count = 0;
      $all_uvor_l_count = 0;
      $all_krit_l_count = 0;
      $all_block_l_count = 0;

      $all_hp_r = 0;
      $all_maxhp_r = 0;
      $all_pw_r = 0;
      $all_maxpw_r = 0;

      $all_pwmax_l = 0;
      $all_pwmax_r = 0;


      $all_uron_r_count = 0;
      $all_udar_r_count = 0;
      $all_uvor_r_count = 0;
      $all_krit_r_count = 0;
      $all_block_r_count = 0;

      $all_nich_count = 0;
      $all_win1_count = 0;
      $all_win2_count = 0;

      $log = "";

         for ($ic=1; $ic<=$fight_counts; $ic++)  {

          $mf_rl_data = array(

             "l_hp" =>  $left_user[5],           # ���������� ������
             "l_maxhp" =>  $left_user[5],        # ����. ���������� ������
             "l_pw" => $left_user[6],            # ���������� ������
             "l_pwmax" => $left_user[6],         # ����. ���������� ������
             "l_count_block" => 0,               # ������������� ������
             "l_count_udar" => 0,                # ����������� ������
             "l_count_uron" => 0,                # ����������� �����
             "l_count_uvor" => 0,                # % �������� �� ���
             "l_count_krit" => 0,                # % ������ �� ���
             "l_uron" => 0,
             "l_pos_left" => @$_POST['pos_left'],

             "r_hp" =>  $right_user[5],          # ���������� ������
             "r_maxhp" =>  $right_user[5],       # ����. ���������� ������
             "r_pw" => $right_user[6],           # ���������� ������
             "r_pwmax" => $right_user[6],        # ����. ���������� ������
             "r_count_block" => 0,               # ������������� ������
             "r_count_udar" => 0,                # ����������� ������
             "r_count_uron" => 0,                # ����������� �����
             "r_count_uvor" => 0,                # $ �������� �� ���
             "r_count_krit" => 0,                # $ ������ �� ���
             "r_uron" => 0,
             "r_pos_left" => @$_POST['pos_right'],

             "log" => "",
          );

          for ($i=1; $i<=5000; $i++)  {
              if ($mf_rl_data['l_hp'] > 0 && $mf_rl_data['r_hp'] > 0) {

                $all_udar_count++;

                $mf_rl_data = razmen_init ($left_user, $right_user, $mf_rl_data);

              }
          }

          $all_uron_l_count += $mf_rl_data['l_count_uron'];
          $all_uron_r_count += $mf_rl_data['r_count_uron'];

          $all_udar_l_count += $mf_rl_data['l_count_udar'];
          $all_udar_r_count += $mf_rl_data['r_count_udar'];

          $all_uvor_l_count += $mf_rl_data['l_count_uvor'];
          $all_uvor_r_count += $mf_rl_data['r_count_uvor'];

          $all_krit_l_count += $mf_rl_data['l_count_krit'];
          $all_krit_r_count += $mf_rl_data['r_count_krit'];

          $all_block_l_count += $mf_rl_data['l_count_block'];
          $all_block_r_count += $mf_rl_data['r_count_block'];

          $all_hp_l += $mf_rl_data['l_hp'];
          $all_hp_r += $mf_rl_data['r_hp'];
          $all_maxhp_l += $mf_rl_data['l_maxhp'];
          $all_maxhp_r += $mf_rl_data['r_maxhp'];


          $all_pw_l += $mf_rl_data['l_pw'];
          $all_pw_r += $mf_rl_data['r_pw'];
          $all_pwmax_l += $mf_rl_data['l_pwmax'];
          $all_pwmax_r += $mf_rl_data['r_pwmax'];

          if ($mf_rl_data['l_hp'] > 0) {
              $all_win1_count = $all_win1_count + 1;
          } elseif ($mf_rl_data['r_hp'] > 0) {
              $all_win2_count = $all_win2_count + 1;
          } else{
              $all_nich_count = $all_nich_count + 1;
          }
           $log .= $mf_rl_data['log'];
         }

if ($fight_counts != 1) { $log = ""; }

echo '<xml>';
    echo '<body>';

        echo '<error>'.$json_error.'</error>';
        echo '<message>111</message>';
        echo '<user_money>'.number_format($user['money'],2,"."," ").'</user_money>';


    if ($_POST['ext']==2) {
        $sleft = '
          total_dmg="'.@conv_hundredths((@$all_uron_l_count / @$all_udar_l_count), 2).'"
          total_dmgt="'.conv_hundredths(($all_uron_l_count / $all_udar_count), 2).'"
          total_kick="'.conv_hundredths($all_udar_count, 2).'"
          suc_kick="'.conv_hundredths($all_udar_l_count, 2).'"
          suc_uvor="'.conv_hundredths(($all_uvor_l_count * 100 / $all_udar_count), 2).'"
          suc_krit="'.@conv_hundredths((@$all_krit_l_count * 100 / @$all_udar_l_count), 2).'"
          suc_kritt="'.conv_hundredths(($all_krit_l_count * 100 / $all_udar_count), 2).'"
          suc_kub="'.$all_krit_l_count.'/'.$all_uvor_l_count.'/'.$all_block_l_count.'"
          suc_hp="'.$all_hp_l.'/'.$all_maxhp_l.'"
          suc_pw="'.$all_pw_l.'/'.$all_pwmax_l.'"';
        //  suc_hp="'.conv_hundredths(($all_hp_l * 100 / $all_maxhp_l), 2).'/'.$all_hp_l.'"

        $sright = '
          total_dmg="'.@conv_hundredths((@$all_uron_r_count / @$all_udar_r_count), 2).'"
          total_dmgt="'.conv_hundredths(($all_uron_r_count / $all_udar_count), 2).'"
          total_kick="'.conv_hundredths($all_udar_count, 2).'"
          suc_kick="'.conv_hundredths($all_udar_r_count, 2).'"
          suc_uvor="'.conv_hundredths(($all_uvor_r_count * 100 / $all_udar_count), 2).'"
          suc_krit="'.@conv_hundredths((@$all_krit_r_count * 100 / @$all_udar_r_count), 2).'"
          suc_kritt="'.conv_hundredths(($all_krit_r_count * 100 / $all_udar_count), 2).'"
          suc_kub="'.$all_krit_r_count.'/'.$all_uvor_r_count.'/'.$all_block_r_count.'"
          suc_hp="'.$all_hp_r.'/'.$all_maxhp_r.'"
          suc_pw="'.$all_pw_r.'/'.$all_pwmax_r.'"';
        //  suc_hp="'.conv_hundredths(($all_hp_r * 100 / $all_maxhp_r), 2).'/'.$all_hp_r.'"

        sql_query('UPDATE users SET money = money - (0.2*'.$fight_counts.') WHERE id = '.$user['id'].';');
    } else {
        sql_query('UPDATE users SET money = money - (0.1*'.$fight_counts.') WHERE id = '.$user['id'].';');
        $sleft = ''; $sright = '';
    }

        echo '<sleft '.$sleft.'></sleft>';
        echo '<sright '.$sright.'></sright>';

        echo '<left>'.$all_win1_count.'</left>';
        echo '<draws>'.$all_nich_count.'</draws>';
        echo '<right>'.$all_win2_count.'</right>';
        echo '<log>'.$log.'</log>';

    echo '</body>';
echo '</xml>';
 }
exit();
}

/***------------------------------------------
 *  �������� �����
 **/

if (@$_POST['is_ajax'] && @$_POST['cmd']=='load' && @$_POST['name']) {

     header('Content-Type: text/xml; charset=UTF-8');
     $json_error = '';   # ������ ��� ���������
     $json_name = @$_POST['name'];   # ������ ��� ���������


     $loaduser = sql_row("SELECT * FROM `users` WHERE `login` = '".($json_name)."' LIMIT 1;");

if (!$loaduser['id']) {
   //  $json_name = iconv('cp1251', 'utf-8', $json_name);
     $json_name = iconv('utf-8', 'cp1251', $json_name);
     $loaduser = sql_row("SELECT * FROM `users` WHERE `login` = '".($json_name)."' LIMIT 1;");
}

if (!$loaduser['id']) {
  $json_error = '<font color="#FF0000">�������� �� ������</font>';
}
if ($loaduser['bot'] && !$loaduser['bothidro']) {
  $json_error = '<font color="#FF0000">�������� ����������</font>';
}

if ((get_ismax_obraz($loaduser) || substr_count($loaduser['shadow'], "big_")) && $user['id'] != 1) {
 if ($loaduser['id'] != $user['id']) {
  $json_error = '<font color="#FF0000">���������� ��������� ��������� � �������������� �������</font>';
 } else {
  $loaduser['shadow'] = '0_0_'.($loaduser['id']?"M":"F").'000.jpg';
 }
}

if ((get_ismax_obraz($loaduser) || substr_count($loaduser['shadow'], "big_")) && $user['id'] == 1) {
  $loaduser['shadow'] = '0_0_'.($loaduser['id']?"M":"F").'000.jpg';
}

    if ($json_error) {
      $json_error = iconv('cp1251', 'utf-8', $json_error);
    }

	  $us_stats = sql_row("select SUM(gsila),SUM(glovk),SUM(ginta),SUM(gintel) FROM `inventory` WHERE `owner` = '".$loaduser['id']."' AND `dressed` = 1 AND `magic` = 0 AND (`type` < 29 or `type` = 50 or `type` = 30)  LIMIT 1;");

echo '<xml>';
    echo '<body>';
        echo '<error>'.$json_error.'</error>';
        echo '<people>'.@$_POST['people'].'</people>';
        echo '<obraz>obraz/'.$loaduser['shadow'].'</obraz>';
        echo '<sex>'.$loaduser['sex'].'</sex>';
        echo '<level>'.$loaduser['level'].'</level>';
        echo '<strength>'.(($loaduser['sila'] + $loaduser['inv_strength']) - $us_stats[0]).'</strength>';
        echo '<dexterity>'.(($loaduser['lovk'] + $loaduser['inv_dexterity']) - $us_stats[1]).'</dexterity>';
        echo '<success>'.(($loaduser['inta'] + $loaduser['inv_success']) - $us_stats[2]).'</success>';
        echo '<endurance>'.$loaduser['vinos'].'</endurance>';
        echo '<intelligence>'.(($loaduser['intel'] + $loaduser['inv_intelligence']) - $us_stats[3]).'</intelligence>';
        echo '<wisdom>'.$loaduser['mudra'].'</wisdom>';
        echo '<bot>'.$loaduser['bot'].'</bot>';
        echo '<name>'.$loaduser['login'].'</name>';


            if ($loaduser['helm']) {
              $g_inventory_helm = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['helm']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_helm = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_helm['name']."' AND type = '".$g_inventory_helm['type']."' LIMIT 1");
                if ($g_shop_helm['id']) {
                  echo '<item>';
                  echo '<slot>helmet</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>8</type>';
                  echo '<num>'.$g_shop_helm['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['sergi']) {
              $g_inventory_sergi = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['sergi']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_sergi = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_sergi['name']."' AND type = '".$g_inventory_sergi['type']."' LIMIT 1");
                if ($g_shop_sergi['id']) {
                  echo '<item>';
                  echo '<slot>earring</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>15</type>';
                  echo '<num>'.$g_shop_sergi['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['kulon']) {
              $g_inventory_kulon = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['kulon']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_kulon = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_kulon['name']."' AND type = '".$g_inventory_kulon['type']."' LIMIT 1");
                if ($g_shop_kulon['id']) {
                  echo '<item>';
                  echo '<slot>necklace</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>14</type>';
                  echo '<num>'.$g_shop_kulon['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['perchi']) {
              $g_inventory_perchi = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['perchi']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_perchi = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_perchi['name']."' AND type = '".$g_inventory_perchi['type']."' LIMIT 1");
                if ($g_shop_perchi['id']) {
                  echo '<item>';
                  echo '<slot>gloves</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>11</type>';
                  echo '<num>'.$g_shop_perchi['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['weap']) {
              $g_inventory_weap = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['weap']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_weap = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_weap['name']."' AND type = '".$g_inventory_weap['type']."' LIMIT 1");

            	switch($g_shop_weap['razdel']) {
                 case 12: $drawtype = 2; break;
                 case 13: $drawtype = 3; break;
                 case 1: $drawtype = 4; break;
                 case 11: $drawtype = 5; break;
                 case 14: $drawtype = 6; break;
                 case 52: $drawtype = 7; break;
                }

                if ($g_shop_weap['id'] && $drawtype) {
                  echo '<item>';
                  echo '<slot>weapon</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>'.$drawtype.'</type>';
                  echo '<num>'.$g_shop_weap['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['bron']) {
              $g_inventory_bron = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['bron']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_bron = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_bron['name']."' AND type = '".$g_inventory_bron['type']."' LIMIT 1");
                if ($g_shop_bron['id']) {
                  echo '<item>';
                  echo '<slot>armor</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>10</type>';
                  echo '<num>'.$g_shop_bron['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['r1']) {
              $g_inventory_ring1 = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['r1']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_ring1 = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_ring1['name']."' AND type = '".$g_inventory_ring1['type']."' LIMIT 1");
                if ($g_shop_ring1['id']) {
                  echo '<item>';
                  echo '<slot>ring</slot>';
                  echo '<slot0>1</slot0>';
                  echo '<type>13</type>';
                  echo '<num>'.$g_shop_ring1['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['r2']) {
              $g_inventory_ring2 = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['r2']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_ring2 = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_ring2['name']."' AND type = '".$g_inventory_ring2['type']."' LIMIT 1");
                if ($g_shop_ring2['id']) {
                  echo '<item>';
                  echo '<slot>ring</slot>';
                  echo '<slot0>2</slot0>';
                  echo '<type>13</type>';
                  echo '<num>'.$g_shop_ring2['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['r3']) {
              $g_inventory_ring3 = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['r3']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_ring3 = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_ring3['name']."' AND type = '".$g_inventory_ring3['type']."' LIMIT 1");
                if ($g_shop_ring3['id']) {
                  echo '<item>';
                  echo '<slot>ring</slot>';
                  echo '<slot0>3</slot0>';
                  echo '<type>13</type>';
                  echo '<num>'.$g_shop_ring3['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['r4']) {
              $g_inventory_ring4 = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['r4']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_ring4 = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_ring4['name']."' AND type = '".$g_inventory_ring4['type']."' LIMIT 1");
                if ($g_shop_ring4['id']) {
                  echo '<item>';
                  echo '<slot>ring</slot>';
                  echo '<slot0>4</slot0>';
                  echo '<type>13</type>';
                  echo '<num>'.$g_shop_ring4['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['shit']) {
              $g_inventory_shit = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['shit']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_shit = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_shit['name']."' AND type = '".$g_inventory_shit['type']."' LIMIT 1");
                if ($g_shop_shit['id']) {
                  echo '<item>';
                  echo '<slot>shield</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>16</type>';
                  echo '<num>'.$g_shop_shit['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['boots']) {
              $g_inventory_boots = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['boots']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_boots = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_boots['name']."' AND type = '".$g_inventory_boots['type']."' LIMIT 1");
                if ($g_shop_boots['id']) {
                  echo '<item>';
                  echo '<slot>shoes</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>9</type>';
                  echo '<num>'.$g_shop_boots['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['naruchi']) {
              $g_inventory_naruchi = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['naruchi']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_naruchi = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_naruchi['name']."' AND type = '".$g_inventory_naruchi['type']."' LIMIT 1");
                if ($g_shop_naruchi['id']) {
                  echo '<item>';
                  echo '<slot>bracelet</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>17</type>';
                  echo '<num>'.$g_shop_naruchi['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['leg']) {
              $g_inventory_leg = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['leg']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_leg = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_leg['name']."' AND type = '".$g_inventory_leg['type']."' LIMIT 1");
                if ($g_shop_leg['id']) {
                  echo '<item>';
                  echo '<slot>pants</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>19</type>';
                  echo '<num>'.$g_shop_leg['id'].'</num>';
                  echo '</item>';
                }
            }
            if ($loaduser['belt']) {
              $g_inventory_belt = sql_row("SELECT * FROM inventory WHERE id = '".$loaduser['belt']."' AND owner = '".$loaduser['id']."' LIMIT 1");
              $g_shop_belt = sql_row("SELECT * FROM shop WHERE name = '".$g_inventory_belt['name']."' AND type = '".$g_inventory_belt['type']."' LIMIT 1");
                if ($g_shop_belt['id']) {
                  echo '<item>';
                  echo '<slot>belt</slot>';
                  echo '<slot0></slot0>';
                  echo '<type>12</type>';
                  echo '<num>'.$g_shop_belt['id'].'</num>';
                  echo '</item>';
                }
            }


    echo '</body>';
echo '</xml>';

exit();
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="js win chrome chrome31 webkit webkit537"><head>
	<title>�����������</title>

	<meta http-equiv="content-type" content="text/html; charset=windows-1251">
	<link href="/css/main.css" rel="stylesheet" type="text/css">
	<link href="/c/fitting_room.css" rel="stylesheet" type="text/css">

	<script src="/j/jquery/jquery.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery-ui.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.browser.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.cookie.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.tooltip.mod.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.pnotify.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.placeholder.js" type="text/javascript"></script>
	<script src="/j/jquery/jquery.countdown.js" type="text/javascript"></script>

    <script type="text/javascript">
  		var isBattle = Number(<?= $user['battle'] ?>);
  		var isBid = Number(<?= $user['zayavka'] ?>);
    </script>

	<script src="/js/iner.js" type="text/javascript"></script>

<?

function drawitem()
{
 $drawit = '';
  $datasssale = sql_query("SELECT * FROM shop WHERE (count > 0 or (type = 4 AND nlevel = 0) OR nlevel = 12)");
	while($row = fetch_array($datasssale)) {

//  createItem(type, num, name, image, weight, durability, price, level, str, dex, suc, end, intel, wis, strength, dexterity, success, intelligence, hp, pw, uronmin, uronmax, krit, ukrit, uvor, uuvor, b1, b2, b3, b4) {

$drawtype = '';
$drawtypeitem = '';

	switch($row['razdel']) {

     case 12: $drawtype = 2; $drawtypeitem = 'weapon'; break;
     case 13: $drawtype = 3; $drawtypeitem = 'weapon'; break;
     case 1: $drawtype = 4; $drawtypeitem = 'weapon'; break;
     case 11: $drawtype = 5; $drawtypeitem = 'weapon'; break;
     case 14: $drawtype = 6; $drawtypeitem = 'weapon'; break;
     case 52: $drawtype = 7; $drawtypeitem = 'weapon'; break;
     case 24: $drawtype = 8; $drawtypeitem = 'helmet'; break;
     case 2: $drawtype = 9; $drawtypeitem = 'shoes'; break;
     case 23: $drawtype = 10; $drawtypeitem = 'armor'; break;
     case 21: $drawtype = 11; $drawtypeitem = 'gloves'; break;
     case 26: $drawtype = 12; $drawtypeitem = 'belt'; break;
     case 42: $drawtype = 13; $drawtypeitem = 'ring'; break;
     case 41: $drawtype = 14; $drawtypeitem = 'necklace'; break;
     case 4: $drawtype = 15; $drawtypeitem = 'earring'; break;
     case 3: $drawtype = 16; $drawtypeitem = 'shield'; break;
     case 25: $drawtype = 17; $drawtypeitem = 'bracelet'; break;
     case 27: $drawtype = 19; $drawtypeitem = 'pants'; break;


    }


    if ($drawtypeitem) {
		$drawit .= $drawtypeitem."[".$drawtype."].push(new createItem(
			'".$drawtype."',
			'".$row['id']."',
			'".$row['name']."',
			'".$row['img']."',
			'".rtrim($row['massa'], '.0')."',
			'".$row['duration']."',
			'".number_format($row['cost'],2,"."," ")."',
			'".$row['nlevel']."',
			'".$row['nsila']."',
			'".$row['nlovk']."',
			'".$row['ninta']."',
			'".$row['nvinos']."',
			'".$row['nintel']."',
			0,
			'".$row['gsila']."',
			'".$row['glovk']."',
			'".$row['ginta']."',
			'".$row['gintel']."',
			'".$row['ghp']."',
			'".$row['gmana']."',
			'".$row['minu']."',
			'".$row['maxu']."',
			'".$row['mfkrit']."',
			'".$row['mfakrit']."',
			'".$row['mfuvorot']."',
			'".$row['mfauvorot']."',
			'".$row['bron1']."',
			'".$row['bron2']."',
			'".$row['bron3']."',
			'".$row['bron4']."'));";

    }
}



 $mf_statitem = array(
			     "1" => array (1),
			     "2" => array (2),
			     "3" => array (3),
			     "4" => array (4),
				 "5" => array (5),
				 "6" => array (6),
				 "7" => array (7),
			     "8" => array (8),
				 "9" => array (9),
				 "10" => array (10),
				 "11" => array (11),
				 "12" => array (12),
				 "13" => array (13),
				 "14" => array (14),
				 "15" => array (15)
				 );
 $mf_item = array(
			     "1" =>  array (5),
			     "2" =>  array (10),
			     "3" =>  array (15),
			     "4" =>  array (20),
				 "5" =>  array (30),
				 "6" =>  array (40),
				 "7" =>  array (55),
			     "8" =>  array (70),
				 "9" =>  array (90),
				 "10" => array (115),
				 "11" => array (140),
				 "12" => array (170),
				 "13" => array (210),
				 "14" => array (260),
				 "15" => array (320),
				 );

 $mf_uron_brom = array(
			     "2" => array (2),
			     "3" => array (3),
			     "4" => array (5),
				 "5" => array (7),
				 "6" => array (9),
				 "7" => array (12),
			     "8" => array (15),
				 "9" => array (19),
				 "10" => array (24),
				 "11" => array (29),
				 "12" => array (37),
				 "13" => array (45),
				 "14" => array (54),
				 "15" => array (66),
				 );
$drawtype = '';
$drawtypeitem = '';
$dvvv = 0;
$sdf = 0;
$dddsdf = 0;

//  $datasssale = sql_query("SELECT * FROM berezka WHERE razdel = 20 AND maxdur = 10");
  $datasssale = sql_query("SELECT * FROM berezka WHERE razdel = 20");
	while($row = fetch_array($datasssale)) {
      $sdf++;
       for ($i = 1; $i <= 13; $i++ ) {
         $dddsdf += $i;
		$drawit .= "stone[20].push(new createItem(
			'20',
			'".($dddsdf)."',
			'".$row['name']."',
			'".$row['img']."',
			'".rtrim($row['massa'], '.0')."',
			'".$row['maxdur']."',
			'".number_format($row['cost'],2,"."," ")."',
			'".$i."',
			'".$row['nsila']."',
			'".$row['nlovk']."',
			'".$row['ninta']."',
			'".$row['nvinos']."',
			'".$row['nintel']."',
			0,
			'".($row['gsila']?$mf_statitem[$i][0]:0)."',
			'".($row['glovk']?$mf_statitem[$i][0]:0)."',
			'".($row['ginta']?$mf_statitem[$i][0]:0)."',
			'".($row['gintel']?$mf_statitem[$i][0]:0)."',
			'".($row['ghp']?$mf_item[$i][0]:0)."',
			'".($row['gmana']?$mf_item[$i][0]:0)."',
			'".($row['minu']?1:0)."',
			'".($row['maxu']?$mf_uron_brom[$i][0]:0)."',
			'".($row['mfkrit']?$mf_item[$i][0]:0)."',
			'".($row['mfakrit']?$mf_item[$i][0]:0)."',
			'".($row['mfuvorot']?$mf_item[$i][0]:0)."',
			'".($row['mfauvorot']?$mf_item[$i][0]:0)."',
			'".($row['bron1']?$mf_uron_brom[$i][0]:0)."',
			'".($row['bron2']?$mf_uron_brom[$i][0]:0)."',
			'".($row['bron3']?$mf_uron_brom[$i][0]:0)."',
			'".($row['bron4']?$mf_uron_brom[$i][0]:0)."'));";

     }
    $dddsdf += 20;
    }
/* 			'".$mf_item[$i][0]."',
			'".$mf_item[$i][0]."',
			'".$mf_uron_brom[$i][0]."',
			'".$mf_uron_brom[$i][0]."',
			'".$mf_item[$i][0]."',
			'".$mf_item[$i][0]."',
			'".$mf_item[$i][0]."',
			'".$mf_item[$i][0]."',
			'".$mf_uron_brom[$i][0]."',
			'".$mf_uron_brom[$i][0]."',
			'".$mf_uron_brom[$i][0]."',
			'".$mf_uron_brom[$i][0]."'));"; */
return $drawit;

}
?>

	<script src="/j/fitting_room.js" type="text/javascript"></script>
	<script type="text/javascript">
		var typesWeapons = new Array();
		var typesArmors = new Array();
		var typesMagics = new Array();

		//<![CDATA[
<?= drawitem() ?>

		$(function(){
			$('#partner_type').change(function(){
				if(this.value == 1){
					$('.positions').hide();
				} else {
					$('.positions').show();
				}
			});
		});
		//]]>
	</script>
</head>
<body class="main-content">

<?
/*<form id="boy" action="fitting_room.php" method="post">
 <input type="hidden" name="cmd" value="fight" />
 <input type="hidden" name="is_ajax" value="1" />
 <input type="hidden" name="partner_type" value="fight" />
 <input type="hidden" name="left" value="fight" />
 <input type="hidden" name="right" value="fight" />
 <input type="hidden" name="pos_left" value="fight" />
 <input type="hidden" name="pos_right" value="fight" />
 <input type="hidden" name="ext" value="fight" />
 <input type="hidden" name="counts" value="fight" />

 <input name="" type="submit" value="���" />
</form>*/
?>

	<div id="fitting-room-container">
		<table id="fitting-room">
			<tbody><tr>
				<td class="char-col">
					<div class="block char-block">
						<form action="" class="char-loader" onsubmit="return loadPeople('left');">
							<label for="left_name">���:</label>
							<input class="text" id="left_name" type="text">
							<input class="xgbutton" type="submit" value="���������">
						</form>
						<form action="" class="char-loader" onsubmit="return loadBot('left');">
							<label for="left_bot">������:</label>
							<select id="left_bot" style="width: 100px;">
                               <?
                               foreach ($DUNGEON_BOTS as $k => $v_data) {
                               if (empty($v_data['qwestbot'])) {
                               ?>
                               <option value="<?= $v_data['id'] ?>"><?= $v_data['login'] ?></option>
                               <? }} ?>
							</select>
							<input class="xbbutton" type="submit" value="���������">
						</form>
						<ul class="inv-items" id="left_user_wrap">
							<li id="left_obraz" class="avatar" style="background-image: url(http://img.blutbad.ru/i/obraz/M.jpg); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_armor" style="background-image: url(http://img.blutbad.ru/i/char_armor.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_rune1" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_rune2" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_rune3" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_rune4" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_rune5" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_rune6" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_rune7" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_rune8" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_rune9" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_helmet" style="background-image: url(http://img.blutbad.ru/i/char_helmet.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_belt" style="background-image: url(http://img.blutbad.ru/i/char_belt.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_shoes" style="background-image: url(http://img.blutbad.ru/i/char_shoes.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_bracelet" style="background-image: url(http://img.blutbad.ru/i/char_bracelet.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_gloves" style="background-image: url(http://img.blutbad.ru/i/char_gloves.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_weapon" style="background-image: url(http://img.blutbad.ru/i/char_weapon.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_shield" style="background-image: url(http://img.blutbad.ru/i/char_shield.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_earring" style="background-image: url(http://img.blutbad.ru/i/char_earring.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_necklace" style="background-image: url(http://img.blutbad.ru/i/char_necklace.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><!--<li id="left_pants" style="background-image: url(http://img.blutbad.ru/i/char_pants.gif); background-position: initial initial; background-repeat: initial initial;">-->
							<!--<li id="left_pocket1" />-->
							<!--<li id="left_pocket2" />-->
							<!--</li>--><li id="left_ring1" style="background-image: url(http://img.blutbad.ru/i/char_ring.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_ring2" style="background-image: url(http://img.blutbad.ru/i/char_ring.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_ring3" style="background-image: url(http://img.blutbad.ru/i/char_ring.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_ring4" style="background-image: url(http://img.blutbad.ru/i/char_ring.gif); background-position: initial initial; background-repeat: initial initial;">

							</li><!--<li id="left_shoulder" style="background-image: url(http://img.blutbad.ru/i/char_shoulder.gif); background-position: initial initial; background-repeat: initial initial;">
							</li>--><li id="left_stone1" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_stone2" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_stone3" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_stone4" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_stone5" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_stone6" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="left_stone7" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
						</li></ul>
						<select class="items-select" id="left_items_list">
							<optgroup label="������">
								<option value="7">�����</option>
								<script type="text/javascript">typesWeapons.push({ type: 7, name: '�����' });</script>
								<option value="2">������</option>
								<script type="text/javascript">typesWeapons.push({ type: 2, name: '������' });</script>
								<option value="1">�������</option>
								<script type="text/javascript">typesWeapons.push({ type: 1, name: '�������' });</script>
								<!--<option value="18">������</option>
								<script type="text/javascript">typesWeapons.push({ type: 18, name: '������' });</script>-->
								<option value="6">������</option>
								<script type="text/javascript">typesWeapons.push({ type: 6, name: '������' });</script>
								<option value="3">����</option>
								<script type="text/javascript">typesWeapons.push({ type: 3, name: '����' });</script>
								<option value="4">����</option>
								<script type="text/javascript">typesWeapons.push({ type: 4, name: '����' });</script>
								<option value="5">������</option>
								<script type="text/javascript">typesWeapons.push({ type: 5, name: '������' });</script>
							</optgroup>
							<optgroup label="�����">
								<option value="17">��������</option>
								<script type="text/javascript">typesArmors.push({ type: 17, name: '��������' });</script>
								<option value="16">����</option>
								<script type="text/javascript">typesArmors.push({ type: 16, name: '����' });</script>
								<option value="10">�����</option>
								<script type="text/javascript">typesArmors.push({ type: 10, name: '�����' });</script>
								<option value="11">��������</option>
								<script type="text/javascript">typesArmors.push({ type: 11, name: '��������' });</script>
								<!--<option value="23">����������</option>
								<script type="text/javascript">typesArmors.push({ type: 23, name: '����������' });</script>-->
								<option value="9">������</option>
								<script type="text/javascript">typesArmors.push({ type: 9, name: '������' });</script>
								<option value="12">�����</option>
								<script type="text/javascript">typesArmors.push({ type: 12, name: '�����' });</script>
								<option value="8">�����</option>
								<script type="text/javascript">typesArmors.push({ type: 8, name: '�����' });</script>
								<!--<option value="19">�����</option>
								<script type="text/javascript">typesArmors.push({ type: 19, name: '�����' });</script>-->
							</optgroup>
							<optgroup label="���������">
								<option value="20">�����</option>
								<script type="text/javascript">typesMagics.push({ type: 20, name: '�����' });</script>
								<option value="14">��������</option>
								<script type="text/javascript">typesMagics.push({ type: 14, name: '��������' });</script>
								<option value="13">������</option>
								<script type="text/javascript">typesMagics.push({ type: 13, name: '������' });</script>
								<option value="50">����</option>
								<script type="text/javascript">typesMagics.push({ type: 50, name: '����' });</script>
								<option value="15">������</option>
								<script type="text/javascript">typesMagics.push({ type: 15, name: '������' });</script>
							</optgroup>
						</select>
						<div class="items-levels" id="left_items_levels"><a class="tab" onclick="updateItemsList('left', $('#left_items_list').attr('value'), 0, 0);"><span id="left_items_levels_all" style="font-weight: normal;">���</span></a>&nbsp;<a class="tab" onclick="updateItemsList('left', $('#left_items_list').attr('value'), 0, 0);"><span id="left_items_levels_0" style="font-weight: bold;">0</span></a></div>
						<div class="items" id="left_items"><div class="w100 tac" style="margin-top: 150px;">��� ���������<br>��� ����� ������</div></div>
						<ul class="controls">
							<li><input class="xgbutton" id="left_button_custom_item" type="button" onclick="wearCustomItemDialog('left');" value="������ ������������ ����"></li>
							<li><input class="xgbutton" id="left_button_calc_stats" type="button" onclick="calcStats('left');" value="��������� ��������������"></li>
							<li>
								<input class="xgbutton clear" id="left_button_clear" onclick="clearPeople('left');" type="button" value="��������">
								<input class="xgbutton clone" id="left_button_clone" onclick="clonePeople('left');" type="button" value="����������� &gt;">
							</li>
							<li><input class="xgbutton" id="left_button_save" type="button" onclick="saveComplect('left');" value="��������� ��������"></li>
							<li><input class="xgbutton" id="left_button_load" type="button" onclick="loadComplect('left');" value="������ ��������"></li>
							<li>
                             <?
                               $user_komplekt = sql_query("SELECT * FROM komplekt WHERE owner = '".$user['id']."' AND fitting != '' LIMIT 5;");
                               while ($row = fetch_array($user_komplekt)) {
                                echo '<a href="#" onclick="loadstrComplect(\'left\', \''.$row['fitting'].'\');" >������ '.$row['name'].'</a><br>';
                               }
                             ?>
                            </li>
						</ul>
					</div>
				</td>
				<td class="center_stats stats">
					<table>
						<tbody>
							<tr>
								<td>
									<select id="left_level" value="0">
										<option value="0" selected="selected">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									<span id="left_level_span" style="display: none;">0</span>
								</td>
								<td></td>
								<th>�������</th>
								<td></td>
								<td>
									<select id="right_level" value="0">
										<option value="0" selected="selected">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									<span id="right_level_span" style="display: none;">0</span>
								</td>
							</tr>
							<tr>
								<td>
									<span id="left_weight">0</span>/<span id="left_maxweight">18</span>
								</td>
								<td></td>
								<td>��� ���������</td>
								<td></td>
								<td>
									<span id="right_weight">0</span>/<span id="right_maxweight">18</span>
								</td>
							</tr>
							<tr>
								<td id="left_price">0</td>
								<td></td>
								<td>���������</td>
								<td></td>
								<td id="right_price">0</td>
							</tr>
							<tr class="spacer">
								<td colspan="5"></td>
							</tr>
							<tr>
								<th colspan="5">��������������</th>
							</tr>
							<tr>
								<td colspan="2" id="left_kbo">100%</td>
								<td>���</td>
								<td colspan="2" id="right_kbo">100%</td>
							</tr>
							<tr>
								<td>
									<input class="text" id="left_strength" type="text" value="3">
								</td>
								<td id="left_str">3</td>
								<td>����</td>
								<td id="right_str">3</td>
								<td>
									<input class="text" id="right_strength" type="text" value="3">
								</td>
							</tr>
							<tr>
								<td>
									<input class="text" id="left_dexterity" type="text" value="3">
								</td>
								<td id="left_dex">3</td>
								<td>��������</td>
								<td id="right_dex">3</td>
								<td>
									<input class="text" id="right_dexterity" type="text" value="3">
								</td>
							</tr>
							<tr>
								<td>
									<input class="text" id="left_success" type="text" value="3">
								</td>
								<td id="left_suc">3</td>
								<td>��������</td>
								<td id="right_suc">3</td>
								<td>
									<input class="text" id="right_success" type="text" value="3">
								</td>
							</tr>
							<tr>
								<td>
									<input class="text" id="left_endurance" type="text" value="3">
								</td>
								<td id="left_end">3</td>
								<td>����������������</td>
								<td id="right_end">3</td>
								<td>
									<input class="text" id="right_endurance" type="text" value="3">
								</td>
							</tr>
							<tr>
								<td>
									<input class="text" id="left_intelligence" type="text" value="0">
								</td>
								<td id="left_intel">0</td>
								<td>���������</td>
								<td id="right_intel">0</td>
								<td>
									<input class="text" id="right_intelligence" type="text" value="0">
								</td>
							</tr>
							<tr>
								<td>
									<input class="text" id="left_wisdom" type="text" value="0">
								</td>
								<td id="left_wis">0</td>
								<td>��������</td>
								<td id="right_wis">0</td>
								<td>
									<input class="text" id="right_wisdom" type="text" value="0">
								</td>
							</tr>
							<tr>
								<td id="left_up">12</td>
								<td></td>
								<td class="sum">��������</td>
								<td></td>
								<td id="right_up">12</td>
							</tr>
							<tr class="spacer">
								<td colspan="5"></td>
							</tr>
							<tr>
								<th colspan="5">���������</th>
							</tr>
							<tr>
								<td class="" id="left_hp">30</td>
								<td class="red" colspan="3">������� �����</td>
								<td class="" id="right_hp">30</td>
							</tr>
							<tr>
								<td class="" id="left_pw">18</td>
								<td class="green" colspan="3">������������</td>
								<td class="" id="right_pw">18</td>
							</tr>
							<tr>
								<td class="" id="left_uronmin">0</td>
								<td colspan="3">����������� ����</td>
								<td class="" id="right_uronmin">0</td>
							</tr>
							<tr>
								<td class="" id="left_uronmax">0</td>
								<td colspan="3">������������ ����</td>
								<td class="" id="right_uronmax">0</td>
							</tr>
							<tr>
								<td class="" id="left_krit">0</td>
								<td class="red" colspan="3">����������� ����</td>
								<td class="" id="right_krit">0</td>
							</tr>
							<tr>
								<td class="" id="left_ukrit">0</td>
								<td class="red" colspan="3">������ ������������ �����</td>
								<td class="" id="right_ukrit">0</td>
							</tr>
							<tr>
								<td class="" id="left_uvor">0</td>
								<td class="blue" colspan="3">�����������</td>
								<td class="" id="right_uvor">0</td>
							</tr>
							<tr>
								<td class="" id="left_uuvor">0</td>
								<td class="blue" colspan="3">������ �����������</td>
								<td class="" id="right_uuvor">0</td>
							</tr>
							<tr class="spacer">
								<td colspan="5"></td>
							</tr>
							<tr>
								<th colspan="5">�����</th>
							</tr>
							<tr>
								<td id="left_b1">0</td>
								<td colspan="3">������</td>
								<td id="right_b1">0</td>
							</tr>
							<tr>
								<td id="left_b2">0</td>
								<td colspan="3">������</td>
								<td id="right_b2">0</td>
							</tr>
							<tr>
								<td id="left_b3">0</td>
								<td colspan="3">����</td>
								<td id="right_b3">0</td>
							</tr>
							<tr>
								<td id="left_b4">0</td>
								<td colspan="3">����</td>
								<td id="right_b4">0</td>
							</tr>
						</tbody>
					</table>

<div class="block fight-block">
						<table>
							<tbody><tr>
								<th colspan="2">�������� ��������</th>
							</tr>
							<tr>
								<!--<td colspan="2"><span id="partner_type_title">���������</span></td> -->
							</tr>

							<tr>
								<!--<td class="value" colspan="2">
									<select name="partner_type" id="partner_type">
										<option value="0" selected="selected">�����</option>
										<option value="1">���</option>
									</select>
								</td> -->
							</tr>
							<tr class="positions" style="display: table-row;">
								<td colspan="2">�������</td>
							</tr>
							<tr class="positions" style="display: table-row;">
								<td class="value">
									<ul class="position-list">
										<li>
											<input checked="checked" id="left_fight_position_0" name="left_fight_position" type="radio" value="0">
											<label for="left_fight_position_0">��������</label>
										</li>
										<li>
											<input id="left_fight_position_1" name="left_fight_position" type="radio" value="1">
											<label for="left_fight_position_1">���������</label>
										</li>
										<li>
											<input id="left_fight_position_2" name="left_fight_position" type="radio" value="2">
											<label for="left_fight_position_2">��������</label>
										</li>
									</ul>
								</td>
								<td class="value">
									<ul class="position-list">
										<li>
											<input checked="checked" id="right_fight_position_0" name="right_fight_position" type="radio" value="0">
											<label for="right_fight_position_0">��������</label>
										</li>
										<li>
											<input id="right_fight_position_1" name="right_fight_position" type="radio" value="1">
											<label for="right_fight_position_1">���������</label>
										</li>
										<li>
											<input id="right_fight_position_2" name="right_fight_position" type="radio" value="2">
											<label for="right_fight_position_2">��������</label>
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<!--<td colspan="2">
									<p class="m0 p0" style="margin-top: 20px">��������� ���������� ���������</p>
								</td>-->
							</tr>
							<tr class="items-requirements">
								<!--<td>
									<input checked="checked" id="left_fight_reqs" type="checkbox">
									<label for="left_fight_reqs">��</label>
								</td>
								<td>
									<input checked="checked" id="right_fight_reqs" type="checkbox">
									<label for="right_fight_reqs">��</label>
								</td>-->
							</tr>
							<tr>
								<td colspan="2">
									<script type="text/javascript">
										//<![CDATA[
										function calc_price(){
											var val1 = $("#fights_counts").val();
											var val2 = $("[name=ext]:checked").val();
											$('#fight_price').html(val1 / 10 * val2);
											if (val2 == 1) {
												$("#stat_table").hide();
											}
											if (val2 == 2) {
												$("#stat_table").show();
											}
										}
										//]]>
									</script>

									<div style="margin-top: 20px;">���������� ����</div>
									<div style="text-align: left;">
										<input name="ext" id="ext2" onclick="calc_price()" type="radio" value="2">
										<label for="ext2">����������� ����������</label>
									</div>
									<div style="text-align: left;">
										<input checked="checked" name="ext" id="ext1" onclick="calc_price()" type="radio" value="1">
										<label for="ext1">������ ���������</label>
									</div>
									<div style="margin: .5em 0; text-align: left;">
										<select id="fights_counts" onchange="calc_price()">
											<option value="1" selected="selected">1</option>
											<option value="10">10</option>
											<option value="100">100</option>
											<option value="1000">1000</option>
										</select>
										���� �����
										<b id="fight_price" class="bold">0.1</b> ��.
									</div>
									<div style="margin-bottom: 20px;">� ��� � �������: <b id="user_money"><?=number_format($user['money'],2,"."," ")?></b> ��.</div>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<table>
										<tbody>
											<tr>
												<td>������</td>
												<td colspan="2">�����</td>
												<td>������</td>
											</tr>
											<tr>
												<td class="vat" style="height: 25px;" id="left_wins">0</td>
												<td class="vat" id="draws" colspan="2">0</td>
												<td class="vat" id="right_wins">0</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr id="stat_table" style="display: none;">
								<td colspan="2">
									<table class="table-list">
										<tbody>
											<tr>
												<td id="left_total_dmg" class="vat"></td>
												<td class="vat">��. ���� �� ���������</td>
												<td id="right_total_dmg" class="vat"></td>
											</tr>
											<tr>
												<td id="left_total_dmgt" class="vat"></td>
												<td class="vat">��. ���� �� ���</td>
												<td id="right_total_dmgt" class="vat"></td>
											</tr>
											<tr>
												<td id="left_total_kick" class="vat"></td>
												<td class="vat">����� �� ���</td>
												<td id="right_total_kick" class="vat"></td>
											</tr>
											<tr>
												<td id="left_suc_kick" class="vat"></td>
												<td class="vat">��������� �� ���</td>
												<td id="right_suc_kick" class="vat"></td>
											</tr>
											<tr>
												<td id="left_suc_uvor" class="vat"></td>
												<td class="vat">������� (%)</td>
												<td id="right_suc_uvor" class="vat"></td>
											</tr>
											<tr>
												<td id="left_suc_krit" class="vat"></td>
												<td class="vat">����� (% ���������)</td>
												<td id="right_suc_krit" class="vat"></td>
											</tr>
											<tr>
												<td id="left_suc_kritt" class="vat"></td>
												<td class="vat">����� (% �����)</td>
												<td id="right_suc_kritt" class="vat"></td>
											</tr>
											<tr>
												<td id="left_suc_kub" class="vat"></td>
												<td class="vat">�����/�������/�����</td>
												<td id="right_suc_kub" class="vat"></td>
											</tr>
											<tr>
												<td id="left_suc_hp" class="vat"></td>
												<td class="vat">����� (%)</td>
												<td id="right_suc_hp" class="vat"></td>
											</tr>
											<tr>
												<td id="left_suc_pw" class="vat"></td>
												<td class="vat">����� (%)</td>
												<td id="right_suc_pw" class="vat"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="padding-bottom: 0; text-align: center;">
									<img alt="���!" id="fight_img" src="http://img.carnage.ru/i/fitting_tool_button_primary.gif" style="display: block; margin: 0 auto;" title="���!">
								</td>
							</tr>
						</tbody></table>
					</div>
				</td>
				<td class="char-col">
					<div class="block char-block">
						<form action="" class="char-loader" onsubmit="return loadPeople('right')">
							<label for="right_name">���:</label>
							<input class="text" id="right_name" type="text">
							<input class="xgbutton" type="submit" value="���������">
						</form>
						<form action="" class="char-loader" onsubmit="return loadBot('right');">
							<label for="right_bot">������:</label>
							<select id="right_bot" style="width: 100px;">
                               <?
                               foreach ($DUNGEON_BOTS as $k => $v_data) {
                               if (empty($v_data['qwestbot'])) {
                               ?>
                               <option value="<?= $v_data['id'] ?>"><?= $v_data['login'] ?></option>
                               <? }} ?>
							</select>
							<input class="xbbutton" type="submit" value="���������">
						</form>
						<ul class="inv-items" id="right_user_wrap">
							<li id="right_obraz" class="avatar" style="background-image: url(http://img.blutbad.ru/i/obraz/M.jpg); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_rune1" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_rune2" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_rune3" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_rune4" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_rune5" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_rune6" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_rune7" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_rune8" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_rune9" style="background-image: url(http://img.blutbad.ru/i/char_rune.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_helmet" style="background-image: url(http://img.blutbad.ru/i/char_helmet.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_armor" style="background-image: url(http://img.blutbad.ru/i/char_armor.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_belt" style="background-image: url(http://img.blutbad.ru/i/char_belt.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_shoes" style="background-image: url(http://img.blutbad.ru/i/char_shoes.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_bracelet" style="background-image: url(http://img.blutbad.ru/i/char_bracelet.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_gloves" style="background-image: url(http://img.blutbad.ru/i/char_gloves.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_weapon" style="background-image: url(http://img.blutbad.ru/i/char_weapon.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_shield" style="background-image: url(http://img.blutbad.ru/i/char_shield.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_earring" style="background-image: url(http://img.blutbad.ru/i/char_earring.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_necklace" style="background-image: url(http://img.blutbad.ru/i/char_necklace.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><!--<li id="right_pants" style="background-image: url(http://img.blutbad.ru/i/char_pants.gif); background-position: initial initial; background-repeat: initial initial;">-->
							<!--<li id="right_pocket1" />-->
							<!--<li id="right_pocket2" />-->
							<!--</li>--><li id="right_ring1" style="background-image: url(http://img.blutbad.ru/i/char_ring.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_ring2" style="background-image: url(http://img.blutbad.ru/i/char_ring.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_ring3" style="background-image: url(http://img.blutbad.ru/i/char_ring.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_ring4" style="background-image: url(http://img.blutbad.ru/i/char_ring.gif); background-position: initial initial; background-repeat: initial initial;">

							</li><!--<li id="right_shoulder" style="background-image: url(http://img.blutbad.ru/i/char_shoulder.gif); background-position: initial initial; background-repeat: initial initial;">
							</li>--><li id="right_stone1" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_stone2" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_stone3" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_stone4" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_stone5" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_stone6" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
							</li><li id="right_stone7" style="background-image: url(http://img.blutbad.ru/i/char_stone.gif); background-position: initial initial; background-repeat: initial initial;">
						</li></ul>
						<select class="items-select" id="right_items_list">
							<optgroup label="������">
								<option value="7">�����</option>
								<option value="2">������</option>
								<option value="1">�������</option>
								<!--<option value="18">������</option>-->
								<option value="6">������</option>
								<option value="3">����</option>
								<option value="4">����</option>
								<option value="5">������</option>
							</optgroup>
							<optgroup label="�����">
								<option value="17">��������</option>
								<option value="16">����</option>
								<option value="10">�����</option>
								<option value="11">��������</option>
								<!--<option value="23">����������</option>-->
								<option value="9">������</option>
								<option value="12">�����</option>
								<option value="8">�����</option>
								<!--<option value="19">�����</option>-->
							</optgroup>
							<optgroup label="���������">
								<option value="20">�����</option>
								<option value="14">��������</option>
								<option value="13">������</option>
								<option value="50">����</option>
								<option value="15">������</option>
							</optgroup>
						</select>
						<div class="items-levels" id="right_items_levels"><a class="tab" onclick="updateItemsList('right', $('#right_items_list').attr('value'), 0, 0);"><span id="right_items_levels_all" style="font-weight: normal;">���</span></a>&nbsp;<a class="tab" onclick="updateItemsList('right', $('#right_items_list').attr('value'), 0, 0);"><span id="right_items_levels_0" style="font-weight: bold;">0</span></a></div>
						<div class="items" id="right_items"><div class="w100 tac" style="margin-top: 150px;">��� ���������<br>��� ����� ������</div></div>
						<ul class="controls">
							<li><input class="xgbutton" id="right_button_custom_item" type="button" onclick="wearCustomItemDialog('right');" value="������ ������������ ����"></li>
							<li><input class="xgbutton" id="right_button_calc_stats" type="button" onclick="calcStats('right');" value="��������� ��������������"></li>
							<li>
								<input class="xgbutton clone" id="right_button_clone" type="button" onclick="clonePeople('right');" value="&lt; �����������">
								<input class="xgbutton clear" id="right_button_clear" type="button" onclick="clearPeople('right');" value="��������">
							</li>
							<li><input class="xgbutton" id="right_button_save" type="button" onclick="saveComplect('right');" value="��������� ��������"></li>
							<li><input class="xgbutton" id="right_button_load" type="button" onclick="loadComplect('right');" value="������ ��������"></li>
							<li>
                             <?
                               $user_komplekt = sql_query("SELECT * FROM komplekt WHERE owner = '".$user['id']."' AND fitting != '' LIMIT 5;");
                               while ($row = fetch_array($user_komplekt)) {
                                echo '<a href="#" onclick="loadstrComplect(\'right\', \''.$row['fitting'].'\');" >������ '.$row['name'].'</a><br>';
                               }
                             ?>
                            </li>
						</ul>
					</div>
				</td>
			</tr>
		</tbody></table>
        <div id="log" style="text-align: left; margin: 0 auto;width: 340px;"></div>
		<div class="popup_stats">
			<div id="equiped_stats">
				<h3 id="info_name_equiped"></h3>
				<div class="vat tal" id="stats_info_equiped"></div>
			</div>
			<div id="current_stats">
				<h3 id="info_name"></h3>
				<div id="stats_info"></div>
			</div>
		</div>
		<iframe class="popup_stats_frame"></iframe>
	</div>

</body>
</html><? include("a_process_end.php");  ?>