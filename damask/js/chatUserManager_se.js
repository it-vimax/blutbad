function showUserMessages( e ) {
	var word  = $('#filt_mess').val().trim().toLowerCase();
	var messages = top.frames[ 'chat' ][ 'chat2' ].oChat.mainChannel.htmlContainer.childNodes;

    var current, findOut = 'class="user-from-(room|private)" name="' + word;

	if(word.length == 0) for(var i = 0; i < messages.length; i++) messages[i].style.display = '';
	else {
		if(e.keyCode == 13) {
            findOut += '"';

			for(var i = 0; i < messages.length; i++) {
				current = messages[i].innerHTML;

				if(new RegExp(findOut, 'i').test(current)) messages[i].style.display = '';
				else messages[i].style.display = 'none';
			}
		} else {
			for(var i = 0; i < messages.length; i++) {
				current = messages[i].innerHTML;

				if(new RegExp(findOut, 'i').test(current)) messages[i].style.display = '';
				else messages[i].style.display = 'none';
			}
		}
	}

    return false;
}

function getChatUsers() {
	var users = {};

	var messages = top.frames[ 'chat' ][ 'chat2' ].oChat.mainChannel.htmlContainer.childNodes;

	var findOut = 'class="user-(from|to)-(room|private)" name="(.+?)"';

	for(var i = 0; i < messages.length; i++) {
		current = messages[i].innerHTML;

		if(new RegExp(findOut, 'i').test(current)) users[RegExp.$3] = true;
	}

    var list_body = '';
    for(var name in users) list_body += '<a href="#" onclick="top.frames[\'chat\'][\'chat2\'].jQuery(\'#filt_mess\').val(\'' + name + '\'); showUserMessages({keyCode: 13}); getChatUsers(); return false;">[' + name + '] </a>';
    jQuery('#list_body').html(list_body);

	return users;
}