/*
 * ������ cookie
 */ 
function setCookie(name, value, expires, path, domain, secure){
	document.cookie = name + "=" + escape(value) +
		((expires) ? "; expires=" + expires : "") +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
}		

/*
 * ��������� cookie
 */ 
function getCookie(Name) {
	var cookie = ' ' + document.cookie;
	var search = ' ' + Name + '=';
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(';', offset);
			if (end == -1) end = cookie.length;
			return unescape(cookie.substring(offset, end));
		}
	}
	
	return false;
}

// ������ ������� ���� �������� ��� ���������� ��������
if(!getCookie('no-maximize')) {
	window.moveTo(0, 0);
	window.resizeTo(screen.width, screen.height);
}

if (top != self) {
	top.location.href = location.href;
}

var CheckFrame = 1;
var ChatTrans = false;

var OnlineHandCookie = getCookie('online-hand.' + userId);
var OnlineHand = (OnlineHandCookie === '0') ? false : true;

var CtrlPress = false;
var delay = 18;
var currHP, maxHP;
var TimerOn = -1;
var speed = 100;
var delayPW = 18;
var TimerOnPW = -1;
var currPW, maxPW ;
var speedPW = 100;

var onlineScroll = {
	left: 0,
	top: 0
};

var ignore = new Object();
//var help_hint = new HelpHint();

var copyNick;

function showHint(hint){
	hint = hint.evalJSON(true);
	//help_hint.show(hint);
	top.frames['carnage'].showHelpHint(hint);
}

function setHP(value, max, newspeed){
	currHP = value;
	maxHP = max;
	if (TimerOn >= 0) {
		clearTimeout(TimerOn);
		TimerOn = -1;
	}
	speed = newspeed;
	setHPlocal();
}

function setHPlocal(){
	if (currHP > maxHP) {
		currHP = maxHP;
	}
	var sz1 = Math.round((244 / maxHP) * currHP);
	var sz2 = 245 - sz1;
	if (top.frames['carnage'].document.getElementById('HP')) {
		top.frames['carnage'].document.HP2.width = sz2;
		top.frames['carnage'].document.HP1.width = sz1;
		if (currHP / maxHP < 0.33) {
			top.frames['carnage'].document.HP1.src = 'http://img.carnage.ru/i/hp3.gif';
		} else {
			if (currHP / maxHP < 0.66) {
				top.frames['carnage'].document.HP1.src = 'http://img.carnage.ru/i/hp2.gif';
			} else {
				top.frames['carnage'].document.HP1.src = 'http://img.carnage.ru/i/hp1.gif';
			}
		}
	}
	
	currHP = (currHP + (maxHP / 100) * speed / 1000);
	
	if (currHP < maxHP) {
		TimerOn = setTimeout('setHPlocal()', delay * 100);
	} else {
		top.frames['carnage'].document.HP2.width = 1;
		top.frames['carnage'].document.HP1.width = 244;
		top.frames['carnage'].document.HP1.src = 'http://img.carnage.ru/i/hp1.gif';
		TimerOn = -1;
		currHP = maxHP;
	}
}



function setPW(value, max, newspeed){
	currPW = value;
	maxPW = max;
	if (TimerOnPW >= 0) {
		clearTimeout(TimerOnPW);
		TimerOnPW = -1;
	}
	speedPW = newspeed;
	setPWlocal();
}

function setPWlocal(){
	if (currPW > maxPW) {
		currPW = maxPW;
	}
	var sz1 = Math.round((244 / maxPW) * currPW);
	var sz2 = 245 - sz1;
	if (top.frames['carnage'].document.getElementById('PW')) {
		top.frames['carnage'].document.PW2.width = sz2;
		top.frames['carnage'].document.PW1.width = sz1;
		
		if (currPW / maxPW < 0.33) {
			top.frames['carnage'].document.PW1.src = 'http://img.carnage.ru/i/pw3.gif';
		} else {
			if (currPW / maxPW < 0.66) {
				top.frames['carnage'].document.PW1.src = 'http://img.carnage.ru/i/pw2.gif';
			} else {
				top.frames['carnage'].document.PW1.src = 'http://img.carnage.ru/i/pw1.gif';
			}
		}
	}
	
	currPW = (currPW + (maxPW / 100) * speedPW / 1000);
	if (currPW < maxPW) {
		TimerOnPW = setTimeout('setPWlocal()', delay * 100);
	} else {
		top.frames['carnage'].document.PW2.width = 1;
		top.frames['carnage'].document.PW1.width = 244;
		top.frames['carnage'].document.PW1.src = 'http://img.carnage.ru/i/pw1.gif';
		TimerOnPW = -1;
		currPW = maxPW;
	}
}

function nsetHP(value, max, newspeed){
	currHP = value;
	maxHP = max;
	if (TimerOn >= 0) {
		clearTimeout(TimerOn);
		TimerOn = -1;
	}
	speed = newspeed;
	nsetHPlocal();
}

function nsetPW(value, max, newspeed){
	currPW = value;
	maxPW = max;
	if (TimerOnPW >= 0) {
		clearTimeout(TimerOnPW);
		TimerOnPW = -1;
	}
	speedPW = newspeed;
	nsetPWlocal();
}

function nsetHPlocal(){
	if (currHP > maxHP) {
		currHP = maxHP;
	}
	var sz1 = Math.round((244 / maxHP) * currHP);
	
	if (top.frames['carnage'].document.getElementById('nHP')) {
		top.frames['carnage'].document.getElementById('nHP').style.width = sz1 + 'px';
		if (currHP / maxHP < 0.33) {
			top.frames['carnage'].document.getElementById('nHP').style.backgroundImage = 'url("http://img.carnage.ru/i/hp3.gif")';
		} else {
			if (currHP / maxHP < 0.66) {
				top.frames['carnage'].document.getElementById('nHP').style.backgroundImage = 'url("http://img.carnage.ru/i/hp2.gif")';
			} else {
				top.frames['carnage'].document.getElementById('nHP').style.backgroundImage = 'url("http://img.carnage.ru/i/hp1.gif")';
			}
		}
	}
	
	currHP = (currHP + (maxHP / 100) * speed / 1000);
	if (currHP < maxHP) {
		TimerOn = setTimeout('nsetHPlocal()', delay * 100);
	} else {
		top.frames['carnage'].document.getElementById('nHP').style.width = '244px';
		top.frames['carnage'].document.getElementById('nHP').style.backgroundImage = 'url("http://img.carnage.ru/i/hp1.gif")';
		TimerOn = -1;
		currHP = maxHP;
	}
}

function nsetPWlocal() {
	if (currPW > maxPW) {
		currPW = maxPW;
	}
  
    var sz1 = Math.round((244 / maxPW) * currPW);
	
	if (top.frames['carnage'].document.getElementById('nPW')) {
		top.frames['carnage'].document.getElementById('nPW').style.width = sz1 + 'px';
		if (currPW / maxPW < 0.33) {
			top.frames['carnage'].document.getElementById('nPW').style.backgroundImage = 'url("http://img.carnage.ru/i/pw3.gif")';
		} else {
			if (currPW / maxPW < 0.66) {
				top.frames['carnage'].document.getElementById('nPW').style.backgroundImage = 'url("http://img.carnage.ru/i/pw2.gif")';
			} else {
				top.frames['carnage'].document.getElementById('nPW').style.backgroundImage = 'url("http://img.carnage.ru/i/pw1.gif")';
			}
		}
	}
  
    currPW = (currPW + (maxPW / 100) * speedPW / 1000);
	if (currPW < maxPW) {
		TimerOnPW = setTimeout('nsetPWlocal()', delay * 100);
	} else {
		top.frames['carnage'].document.getElementById('nPW').style.width = '244px';
		top.frames['carnage'].document.getElementById('nPW').style.backgroundImage = 'url("http://img.carnage.ru/i/pw1.gif")';
		TimerOnPW = -1;
		currPW = maxPW;
	}
	
	return true;
}

function AddTo(user, a){
	if (CtrlPress && a) {
		window.open('inf.pl?user=' + user, '_blank');
	} else {
		user = unescape(user);
		var o = top.frames['carnage'].formname;
		
		if (!o) {
			o = top.frames['carnage'].document.getElementById('FormName');
		}
		
		var flag = true;
		// ������� ���� � ����, ��� ��� ��������
		if (top.frames['carnage'].pasteNick) {
			flag = top.frames['carnage'].pasteNick(user, true);
		}
		
		if (o != null && o != '') {
			var o = o.value ? o.value : o;
			var field = window.top.frames.carnage.document.getElementById(o);
			
			if (field) {
				field.value = user;
				if (field.style.display != 'none') {
					field.focus();
				}
				flag = false;
			}
		}
		
		if (flag) {
			if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
				top.frames[ 'chat' ][ 'chat2' ].oChat.writeToEditorTo( user );
			}
		}
	}
}

function AddToPrivate(user, a){
	if (CtrlPress && a) {
		window.open('inf.pl?user=' + user, '_blank');
	} else {
		if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
			user = unescape( user );
			top.frames[ 'chat' ][ 'chat2' ].oChat.writeToEditorPrivate( user );
		}
	}
}

// ��������� ����� ������� ��� ������������� cookie
function getSubDomains() {
	var domain = document.domain;
	var arr = domain.split('.');
	var length = arr.length;
	var subDomains = '.' + arr[length - 2] + '.' + arr[length - 1];
	return subDomains;
}

function FrameRefresh(url, frame){
	if (!frame) {
		frame = 'carnage';
	}
	
	if (url == 'into_battle') {
		if (top.frames[frame].document.getElementById("flashdungeon")) {
			return;
		}
		if (top.frames[frame].document.getElementById("battle")) {
			var A5 = top.frames[frame].document.getElementById("A5");
			var D5 = top.frames[frame].document.getElementById("D5");
			var pos = (A5 && A5.checked) ? 1 : 0;
			pos += (D5 && D5.checked) ? 1 : 0;
			if (top.frames[frame].document.getElementById("wait")) {
				top.frames[frame].location = 'battle.pl?' + Math.random()
			//} else if (top.frames[frame].document.getElementById("error") && !pos && (top.frames[frame].checkAttack() == 0) && (top.frames[frame].checkDefend() == 0)) {
			//	top.frames[frame].location = 'battle.pl?' + Math.random()
			}
		} else {
			top.frames[frame].location = 'battle.pl?' + Math.random()
		}
	} else if ( url == 'reload_all' ) {
		top.location.href = '/carnage.pl?' + Math.random();
	} else {
		if (top.frames[frame]) {
			if (url && url.length > 0) {
				if (url.match(/dungeon/) && top.frames[frame].document.getElementById("flashdungeon")) {
					return;
				}
				if (url.match(/dungeon/) && top.frames[frame].document.getElementById("Fish")) {
					return;
				}
				top.frames[frame].location = url;
			} else {
				top.frames[frame].location.reload();
			}
		}
	}
}

function dosound( type ) {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.playSound( type );
	}
}

function getIEVersion(){
	var ua = navigator.userAgent;
	var offset = ua.indexOf('MSIE ');
	if (offset == -1) {
		return 0;
	} else {
		return parseFloat(ua.substring(offset + 5, ua.indexOf(';', offset)));
	}
}

var id = getCookie('id');
var rnd= Math.random();
if (id == null || id == '') {
	document.write('<body style="background-color: #c0c0c0;">������ Cookie.</body>');
} else {
	if (hide_quick_buttons == 1 || userTutorial) {
		writeFrames();
	} else {
		writeFramesWithPanel();
	}
}

function prepareRows(rows) {
	var defaultRows = '60%,*';
	var defaultLength = 2;
	
	if (!rows) {
		return defaultRows;
	}
	var arr = rows.split(',');
	
	if (rows.split(',').length != defaultLength) {
		rows = defaultRows;
	} else {
		var height = document.documentElement.clientHeight;
		var sumHeight = hide_quick_buttons ? 0 : 30;
		for (var i = 0; i < defaultLength; i++) {
			if (!isNaN(arr[i])) {
				sumHeight += parseInt(arr[i]);
			}
		}
		
		if (sumHeight > height) {
			rows = defaultRows;
		} else {
			rows;
		}
	}

	return rows;
}

function prepareCols(cols) {
	var defaultCols = '*,200';
	
	if (!cols) {
		return defaultCols;
	}
	
	var arr = cols.split(',');
	
	if (cols.split(',').length != 2) {
		cols = defaultCols;
	} else {
		var width = document.documentElement.clientWidth;
		var sumWidth = parseInt(arr[1]) - (hide_quick_buttons ? 0 : 65);
		if (!(arr[0] == '*' && sumWidth < width)) {
			cols = defaultCols;
		}
	}
	return cols;
}

function writeFrames() {
	var IE = Prototype.Browser.IE;
	var Opera = Prototype.Browser.Opera;
	//var IE = $.browser.msie;
	//var Opera = $.browser.opera;

	var rows = getCookie( 'main-set-rows.' + userId );
	rows = prepareRows( rows );
	var cols = getCookie( 'chat-set-cols.' + userId );
	cols = prepareCols( cols );
	if (userTutorial) cols = "*";

	var main_scr = "main.pl?fromenter=1&" + rnd;
	if (userTutorial) main_scr = "map.pl?" + rnd;

	var root_rows = "30,*,30";
	if (userTutorial) root_rows = "30,*";

	document.write(
		'<frameset border="0" onload="if ( top.frames[\'chat\'][\'chat2\'] ) { top.frames[\'chat\'][\'chat2\'].start(); }" rows="'+root_rows+'">' +
			'<frame src="menu.pl?' + rnd + '" name="menu" scrolling="no" noresize="noresize" />' +
			'<frameset border="3" bordercolor="#758999" frameborder="1" framespacing="3" id="main-set" rows="' + rows + '">' +
				'<frame frameborder="0" id="carnage" name="carnage" src="'+main_scr+ '" />' +
				'<frameset cols="' + cols + '" id="chat-set">' +
					'<frame name="chat" scrolling="no" src="/ch/html/ch_frames.html?' + rnd + '" />' +
					(userTutorial ? '' : '<frame' + (!Opera ? ' frameborder="0"' : '') + ' name="online" scrolling="no" src="/ch/html/online.html?' + rnd + '" />') +
				'</frameset>' +
			'</frameset>' +
			(userTutorial ? '' : '<frame id="bottomFrame" name="down" noresize="noresize" scrolling="no" src="/button.pl?' + rnd + '" />') +
		'</frameset>'
	);
}

function writeFramesWithPanel() {
	var IE = Prototype.Browser.IE;
	var Opera = Prototype.Browser.Opera;
	//var IE = $.browser.msie;
	//var Opera = $.browser.opera;

	var rows = getCookie( 'main-set-rows.' + userId );
	rows = prepareRows( rows );
	var cols = getCookie( 'chat-set-cols.' + userId );
	cols = prepareCols( cols );

	document.write(
		'<frameset border="0" cols="*" onload="top.frames[\'chat\'][\'chat2\'].start();">' +

			'<frameset border="0" rows="30,*,30">' +
				'<frame name="menu" noresize="noresize" scrolling="no" src="top.html?' + rnd + '" />' +
				'<frameset border="3" bordercolor="#758999" frameborder="1" framespacing="3" id="main-set" rows="' + rows + '">' +
					'<frame frameborder="0" id="carnage" name="main" src="main.php?fromenter=1&' + rnd + '" />' +
					'<frameset cols="' + cols + '" id="chat-set">' +
						'<frame name="chat" scrolling="no" src="buttons.php?ch=' + rnd + '" />' +
						'<frame' + (!Opera ? ' frameborder="0"' : '') + ' name="online" scrolling="no" src="ch.php?online=' + rnd + '" />' +
					'</frameset>' +
				'</frameset>' +
				'<frame id="bottomFrame" name="down" noresize="noresize" scrolling="no" src="/buttons.php?' + rnd + '" />' +
			'</frameset>' +
		'</frameset>'
	);
}

function refreshInbox( mailbox_new ) {
	var oLetterImage = null;
	var oLetterNumber = null;

	if ( top && top.frames[ 'down' ] && ( oLetterImage = top.frames[ 'down' ].document.getElementById( 'ch_mail' ) ) && ( oLetterNumber = top.frames[ 'down' ].document.getElementById( 'lettersNumber' ) ) ) {
		if ( mailbox_new > 0 ) {
			oLetterImage.title = '����� �����: ' + mailbox_new;
			oLetterImage.src = 'http://img.carnage.ru/i/button/mailon.gif';
			oLetterNumber.style.display = 'block';
			oLetterNumber.innerHTML = mailbox_new;
			oLetterNumber.title = '����� �����: ' + mailbox_new;
		} else {
			oLetterImage.title = '����� ����� ���';
			oLetterImage.src = 'http://img.carnage.ru/i/button/mailoff.gif';
			oLetterNumber.style.display = 'none';
			oLetterNumber.title = '����� ����� ���';
		}
	}
}

function refreshDeals( state ) {
	var oDeals = null;

	if ( top && top.frames[ 'down' ] && ( oDeals = top.frames[ 'down' ].document.getElementById( 'deals_img' ) ) ) {
		if ( state > 0 ) {
			oDeals.src = 'http://img.carnage.ru/i/button/new/ch_transfer_blink.gif';
		} else {
			oDeals.src = 'http://img.carnage.ru/i/button/new/ch_transfer.gif';
		}
	}
}

/*
 * ������� �������� ��������
 */ 
Event.observe(window, 'load', function() {
//$(window).load(function(){
	var frameOnline = top.frames['online'];

	if (frameOnline) {
		// ������� ��������� ������ ��������� ����
		Event.observe(top.frames['chat'].window, 'resize', function() {
		//$(top.frames['chat'].window).resize(function() {
			if(frameOnline.window.innerHeight) {
				var onlineHeight = frameOnline.window.innerHeight;
				var onlineWidth = frameOnline.window.innerWidth;
			} else if(frameOnline.document.documentElement.offsetHeight) {
				var scroll = getScrollBarWidth(frameOnline.document);
				var onlineHeight = frameOnline.document.documentElement.offsetHeight + (frameOnline.isHorizontalScroll() && getIEVersion() == 7 ? scroll : 0);
				var onlineWidth = frameOnline.document.documentElement.offsetWidth + (frameOnline.isVerticalScroll() && getIEVersion() == 7 ? scroll : 0);
			}
			
			if(onlineHeight && onlineWidth) {
				var Opera = Prototype.Browser.Opera;
				
				var date = new Date();
				date.setDate(date.getDate() + 365);
			
				var rows = '*,' + onlineHeight;
				if(Opera)
					document.getElementById('main-set').setAttribute('rows', rows);
				setCookie('main-set-rows.' + userId, rows, date.toGMTString(), '/', getSubDomains());
				/*$.cookie('main-set-rows.' + userId, rows, {
					esxpires: 365,
					domain: getSubDomains(),
					path: '/'
				});*/
				
				var cols = '*,' + onlineWidth;
				if(Opera)
					document.getElementById('chat-set').setAttribute('cols', cols);
				setCookie('chat-set-cols.' + userId, cols, date.toGMTString(), '/', getSubDomains());
				/*$.cookie('chat-set-cols.' + userId, cols, {
					esxpires: 365,
					domain: getSubDomains(),
					path: '/'
				});*/
			}
		});
	}
	new Ajax.Request( '/tick.pl?' + Math.random(), { method : 'get' } );
	//$.get('/tick.pl?' + Math.random());
	setInterval(function(){
		new Ajax.Request('/tick.pl?' + Math.random(), { method: 'get' });
		//$.get('/tick.pl?' + Math.random());
	}, 30000);
});

/*
 * �������� ������ ���������
 */
function getScrollBarWidth (doc) {
	var inner = doc.createElement('p');
	inner.style.width = "100%";
	inner.style.height = "200px";

	var outer = doc.createElement('div');
	outer.style.position = "absolute";
	outer.style.top = "0px";
	outer.style.left = "0px";
	outer.style.visibility = "hidden";
	outer.style.width = "200px";
	outer.style.height = "150px";
	outer.style.overflow = "hidden";
	outer.appendChild (inner);

	doc.body.appendChild (outer);
	var w1 = inner.offsetWidth;
	outer.style.overflow = 'scroll';
	var w2 = inner.offsetWidth;
	if (w1 == w2) w2 = outer.clientWidth;

	doc.body.removeChild (outer);
	
	return (w1 - w2);
};

/*
 * ������������� ����� ������
 */
function quickPanelReload () {
	if (top.frames['panel']) {
		top.frames['panel'].location.reload();
	}
}

/*
 * ������������� ���������� � ����������
 */
setDungeonCoords = function( x, y ) {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].iDungeonX = x;
		top.frames[ 'chat' ][ 'chat2' ].iDungeonY = y;
	}
}

// ��������� ��������� ��� ���������
var tutorialHighlight = {
	spirit: false,
	mentors: false,
	stats: false,
	inventory: false,
	inventory_put_on: false,
	skills: false,
	expa: false,
	training: false,
	location: false,
	quests: false,
	end_dialog: false
};

var tutorialState = {
	begin: { spirit: true },
	stats: { inventory: true, stats: true },
	stats_inc: { inventory: true, stats: true },
	stats_inc_all: { spirit: true, location: true, quests: true },
	stats_end: { spirit: true, end_dialog: true },
	inventory: { inventory: true, inventory_put_on: true },
	inventory_put_on: { location: true, spirit: true, quests: true },
	inventory_end: { spirit: true, end_dialog: true },
	expa: { expa: true },
	expa_up: { spirit: true, quests: true },
	expa_end: { spirit: true, end_dialog: true },
	battle: { location: true, mentors: true },
	mentors: { location: true, training: true }
}

function setTutorialState(state) {
	for (var key in tutorialHighlight) {
		var highlighted = tutorialState[state] ? (tutorialState[state][key] ? true : false) : false;
		if (highlighted != tutorialHighlight[key]) {
			tutorialHighlight[key] = highlighted;
			highlightTutorialElement(key,highlighted);
		}
	}
}

function highlightAllTutorialElements(state) {
	for (var key in tutorialHighlight) {
		var highlighted = tutorialState[state] ? (tutorialState[state][key] ? true : false) : false;
		if (highlighted) {
			tutorialHighlight[key] = true;
			highlightTutorialElement(key,true);
		}
	}
}

function getNPCArrow() {
	var arrow = top.frames['carnage'].$('#npc-arrow');
	if(!arrow.length) {
		arrow = top.frames['carnage'].$('<img />', {
			alt: '',
			height: '80',
			id: 'npc-arrow',
			src: 'http://img.carnage.ru/i/tutor_pointer.gif',
			width: '120'
		}).appendTo('body');
	}
	
	return arrow;
}

function getMenuArrow() {
	var arrow = top.frames['carnage'].$('#menu-arrow');
	if(!arrow.length) {
		arrow = top.frames['carnage'].$('<img />', {
			alt: '',
			height: '120',
			id: 'menu-arrow',
			src: 'http://img.carnage.ru/i/tutor_pointer_vert.gif',
			width: '80'
		}).appendTo('body');
	}
	return arrow;
}

var tutorialInterval = {};
var tutotialPeriod = 500;
function highlightTutorialElement(code, highlighted) {
	var spiritIDs = '#npc-13125 .avatar, #npc-13269 .avatar, #npc-13265 .avatar, #npc-13264 .avatar, #npc-13267 .avatar, #npc-13266 .avatar, #npc-13268 .avatar, #npc-13270 .avatar, #npc-22736 .avatar';
	var mentorIDs = '#npc-13139 .avatar, #npc-13288 .avatar, #npc-13289 .avatar, #npc-13293 .avatar, #npc-13291 .avatar, #npc-13290 .avatar, #npc-13292 .avatar, #npc-13294 .avatar, #npc-22738 .avatar';
	
	switch(code) {
		case "spirit": // ������ ����
			var jQuery = top.frames['carnage'].$;
			if (jQuery) {
				var avatar = jQuery(spiritIDs);
				if(avatar.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								avatar.toggleClass('avatar-highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						avatar.removeClass('avatar-highlight');
					}
				}
			}
			break;
		
		case "mentors": // ������ ���������� �����������
			var jQuery = top.frames['carnage'].$;
			if (jQuery) {
				var avatar = jQuery(mentorIDs);
				if(avatar.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								avatar.toggleClass('avatar-highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						avatar.removeClass('avatar-highlight');
					}
				}
			}
			break;
		
		case "stats": // ����� ��������� �������������� � ����� ��������� ���� �������������
			var jQuery = top.frames['carnage'].$;
			if(jQuery) {
				var stats = jQuery('.stat.abilities tbody, .stat.abilities .dist-table-link');
				if(stats.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								stats.toggleClass('stats-highlight')
							} catch(e) {}
						},tutotialPeriod);
					} else {
						stats.removeClass('stats-highlight')
					}
				}
			}	
			break;
		
		case "inventory": // ������ ��������� � ������ ������� ������
			var jQuery = top.frames['menu'].$;
			if(jQuery) {
				var inventory = jQuery('#char a[href="/char.pl"]');
				if(inventory.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								inventory.toggleClass('menu-highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						inventory.removeClass('menu-highlight');
					}
				}
			}
			break;
		
		case "location": // ������ ������� � ������ ������� ������
			var jQuery = top.frames['menu'].$;
			if(jQuery) {
				var location = jQuery('#char a[href="/map.pl?action=map"]');
				if(location.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								location.toggleClass('menu-highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						location.removeClass('menu-highlight');
					}
				}
			}
			break;
		
		case "inventory_put_on": // ������ ������ �������� ��������
			var jQuery = top.frames['carnage'].$;
			if(jQuery) {
				var action = jQuery('tr[data-type-num="10-55"] a:contains(������), tr[data-type-num="10-56"] a:contains(������)');
				if(action.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								action.parent().toggleClass('action-highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						action.parent().removeClass('action-highlight');
					}
				}
			}
			break;
		
		case "expa": // ������� ��������, ��� � ��������
			var jQuery = top.frames['menu'].$;
			if(jQuery) {
				var expa = jQuery('#char a[href="/zayavka.pl"]');
				if(expa.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								expa.toggleClass('menu-highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						expa.removeClass('menu-highlight');
					}
				}
			}
			
			jQuery = top.frames['carnage'].$;
			if(jQuery) {
				var training = jQuery('#battle-training, .map-col a[href*="zayavka.pl"]');

				if(training.length) {
					clearInterval(tutorialInterval['expa2']);
					
					if(highlighted) {
						tutorialInterval['expa2'] = setInterval(function() {
							try {
								training.toggleClass('highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						training.removeClass('highlight');
					}
				}
			}
			break;
		
		case "training": // ������ "� ������������� ���"
			var jQuery = top.frames['carnage'].$;
			if(jQuery) {
				var room = jQuery('#room_locations_main a:contains(������������� ���), #room_locations_main a:contains(������������� ������)');
				if(room.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								room.parent().toggleClass('highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						room.parent().removeClass('highlight');
					}
				}
			}
			break;
		
		case 'quests': // ���������
			var jQuery = top.frames['carnage'].$;
			if(jQuery) {
				var quests = jQuery('#quests-current');
				if(quests.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								quests.toggleClass('highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						quests.removeClass('highlight');
					}
				}
			}
			break;
		
		case 'end_dialog': // ��������� ������ "� ������ �������"
			var jQuery = top.frames['carnage'].$;
			if(jQuery) {
				var button = jQuery('#message-col .button[value="� ������ �������"]');
				if(button.length) {
					clearInterval(tutorialInterval[code]);
					
					if(highlighted) {
						tutorialInterval[code] = setInterval(function() {
							try {
								button.toggleClass('button-highlight');
							} catch(e) {}
						},tutotialPeriod);
					} else {
						button.removeClass('button-highlight');
					}
				}
			}
			break;
	}
}

/**
 * ���������� ������� ������ ��� Firefox, ����������� console.log
 */
function debug() {
	if(typeof console != 'undefined') {
		console.log.apply('', arguments);
	}
}
