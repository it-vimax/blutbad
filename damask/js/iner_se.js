  var imgURL = 'http://img.blutbad.ru/i/';
  var nullImg = imgURL + 'null.gif';
  var libURL = 'http://lib.blutbad.ru/';


// ���������� ��� �������� ������
var monthTypes = new Array(
	'winter', 'winter', 'summer',
	'summer', 'summer', 'summer',
	'summer', 'summer', 'summer',
	'summer', 'summer', 'winter'
);


// ������� ��� ��� �������� ��������, ��� ���� - ��� ������,
// � �������� - ��� ����������������� ����������
var utf8Array = {};
// ������� ��������� ����������� 255 ��������
//var i = j = j2 = 0;
for (var i = 0; i <= 255; i++) {
	var j = parseInt(i / 16);
	var j2 = parseInt(i % 16);
	utf8Array[String.fromCharCode(i)] = ('%' + j.toString(16) + j2.toString(16)).toUpperCase();
}
// � �������� �������� ������� ���������
var rusAdditional = {
	'_' : '%5F', '�' : '%C0', '�' : '%C1', '�' : '%C2', '�' : '%C3', '�' : '%C4', '�' : '%C5',
	'�' : '%C6', '�' : '%C7', '�' : '%C8', '�' : '%C9', '�' : '%CA', '�' : '%CB', '�' : '%CC',
	'�' : '%CD', '�' : '%CE', '�' : '%CF', '�' : '%D0', '�' : '%D1', '�' : '%D2', '�' : '%D3',
	'�' : '%D4', '�' : '%D5', '�' : '%D6', '�' : '%D7', '�' : '%D8', '�' : '%D9', '�' : '%DA',
	'�' : '%DB', '�' : '%DC', '�' : '%DD', '�' : '%DE', '�' : '%DF', '�' : '%E0', '�' : '%E1',
	'�' : '%E2', '�' : '%E3', '�' : '%E4', '�' : '%E5', '�' : '%E6', '�' : '%E7', '�' : '%E8',
	'�' : '%E9', '�' : '%EA', '�' : '%EB', '�' : '%EC', '�' : '%ED', '�' : '%EE', '�' : '%EF',
	'�' : '%F0', '�' : '%F1', '�' : '%F2', '�' : '%F3', '�' : '%F4', '�' : '%F5', '�' : '%F6',
	'�' : '%F7', '�' : '%F8', '�' : '%F9', '�' : '%FA', '�' : '%FB', '�' : '%FC', '�' : '%FD',
	'�' : '%FE', '�' : '%FF', '�' : '%B8', '�' : '%A8'
}

for (i in rusAdditional) {
	utf8Array[i] = rusAdditional[i];
}



function pnotify(message, error, title) {
	if(message) {
		var notify = $.pnotify({
			pnotify_title: title ? title : '��������!',
			pnotify_type: error ? 'error' : 'notice',
			pnotify_text: message //json.error ? json.error : json.message
		});
		notify.appendTo('#notifies').click(notify.pnotify_remove);
	}
}

/**
 * ����������� ���������� �������� ��� �����
 *
 * @param 	{Number} number
 * @param 	{String} den
 * @param 	{String} dnya
 * @param 	{String} dney
 * @return	{String}
 */
function suffix(number, den, dnya, dney){
	if (RegExp(/,|\./).test(number))
		return dnya

	number = Math.abs(number);

	var ones = number % 10;
	var ten = (number - ones) / 10;

	if (RegExp(/.*1\d$/).test(number))
		return dney

	if (ones == 1)
		return den

	if (ones > 1 && ones < 5)
		return dnya

	return dney
}


/**
 * �������������
 *
 * @param {String} str
 */
function urlEncode(str) {
    // ������� ��������
    if (!str || typeof(str) == "undefined") return false;

    // ����������� �������� ������� �� �� ����������������� �����������
    var res = "";
    for(i = 0; i < str.length; i++) {
        var simbol = str.substr(i,1);
        res += typeof utf8Array[simbol] != "undefined" ? utf8Array[simbol] : simbol;
    }
    // ������� �������� �� �����
    res = res.replace(/\s/g, "+");
    return res;
}

/**
 * ���������� ������� ��������� � ���������� ����������
 *
 * @param {Number} num		�����
 * @param {Object} cases	�������� ����� {nom: '���', gen: '����', plu: '�����'}
 * @return {String}
 */
function units(num, cases) {
	num = Math.abs(num);

	var word = '';

	if(num != NaN) {
		if (num.toString().indexOf('.') > -1) {
			word = cases.gen;
		} else {
			word = (
				num % 10 == 1 && num % 100 != 11
					? cases.nom
					: num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)
						? cases.gen
						: cases.plu
			);
		}
	}

	return word;
}

/**
 * ��������������� ������� �� EN � RU
 * @param 	{String} 	s			����������������� ������
 * @param 	{Boolean}	reverse
 * @return 	{String}				�������������� �� EN � RU, �� ���� (reverse==true) - RU � EN
 */
function transliterate(str, reverse) {
	if(jQuery.trim(str)) {
		var trans = str;
		var en_ru = [
			["shh", "SHH", "sh", "SH", "ch", "CH", "jo", "JO", "zh", "ZH", "je", "JE", "ju", "JU", "ja", "JA", "sx", "SX", "j/o", "J/O", "j/e", "J/E", "a", "A", "b", "B", "v", "V", "g", "G", "d", "D", "e", "E", "z", "Z", "i", "I", "j", "J", "k", "K", "l", "L", "m", "M", "n", "N", "o", "O", "p", "P", "r", "R", "s", "S", "t", "T", "u", "U", "f", "F", "x", "X", "h", "H", "c", "C", "w", "W", "##", "#", "y", "Y", "''", "'"],
			["�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "��", "��", "��", "��", "��", "��", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�"]
		];

		if(reverse) {
			en_ru.reverse();

			for(var i = 0; i < en_ru[0].length; i++) {
				while (trans.indexOf(en_ru[0][i]) >= 0) {
					trans = trans.replace(en_ru[0][i], en_ru[1][i]);
				}
			}
		} else {
			// ��������� ������ ��� ���������� �������� �� ������ � ��������������
		   	var arr = trans.split(':');

		   	for(var j = 0; j < arr.length; j++) {
				// ��������� ����������� �������
			  //	if(arr[j] && (!smilies[arr[j]] || j == 0 || j == arr.length -1)) {
				if(arr[j] && (j == 0 || j == arr.length -1)) {
					for(var i = 0; i < en_ru[0].length; i++) {
						while(arr[j].indexOf(en_ru[0][i]) >= 0) {
							arr[j] = arr[j].replace(en_ru[0][i], en_ru[1][i]);
						}
					}
				}
			}

		  	trans = arr.join(':');
		}

		return trans;
	} else {
		return '';
	}
}

function changeLocation(path){
	if (top.frames['main']) {
		top.frames['main'].location.href = '' + path + '&' + Math.random();
	} else {
		window.location.href = '' + path + '&' + Math.random();
	}
}


function statUp(name, ability, nd) {
	if(confirm('�� ������������� ������ ��������� ' + name + '?')) {
		location.href = '/inventory.php?cmd=ability.up&nd=' + nd + '&ability=' + ability + '&' + Math.random();
	}
}



/**
 * ������ ��� ������������ ����� � ������
 */
(function($) {
	$.extend({
		buildForm: function(options) {
			var o = $.extend({}, $.buildForm.defaults, options);
			// ������� ��� ������ �����
			var form = $('<form />', {
				'action': o.url + '?' + Math.random(),
				'class': 'dialog-table',
				'method': o.method
			});

			// ������� ������� ��� ���������� ����� �����
			var table = $('<table />', {
				'class': 'form-table'
			}).appendTo(form);
			var tbody = $('<tbody />').appendTo(table);

			var fields = o.fields;

			for(var i in fields) {
				var field = fields[i];

				if(!field.type && !field.group) {
					field.type = 'hidden';
				}

				if (field.type == 'hidden') {
					form.append(drawField(field));
				} else {
					tbody.append(drawRow(field));
				}
			}

			return form;
		}
	});

	/**
	 * ��������� ���� � �����
	 *
	 * @param {Object} field	����(�) ��� ���������
	 * @return					���������� ������ ���� tr
	 */
	function drawRow(field) {
		var row = $('<tr />');

		if(!field.colspan) {
			var labelTd = $('<td />', {
				'class': 'label'
			}).appendTo(row);
		}

		if(field.label) {
			$('<label />', {
				'for': field.id ? field.id : field.name,
				'text': field.label + ':'
			}).appendTo(labelTd);
		}

		var inputTd = $('<td />', {
			'class': 'input'
		}).appendTo(row);

		if(field.colspan) {
			inputTd.attr('colspan', 2).css('text-align', 'right');
		}

		if (field.group) {
			inputTd.addClass('nowrap');
			for(var i in field.group) {
				var input = field.group[i];

				inputTd.append(drawField(input)).append(' ');
			}
		} else {
			inputTd.append(drawField(field));
		}

		if(field.comment) {
			inputTd.append(field.comment);
		}

		return row;
	}

	/**
	 * ��������� ���� �����
	 * ��-�� IE 6,7 (��� � ��������� name) ���������� ������������ ���� ����� ������� �������� �������� �����
	 *
	 * @param {Object} field	���� ����� (inpu, select � �.�.)
	 * @return {Object}			���������� ������ ���� �����
	 */
	function drawField(field) {
		var el;

		// ���� ��� ���� �� ������, �� �� ��������� ��� hidden
		if(!field.type) {
			field.type = 'hidden';
		}

		switch(field.type) {
			case 'button':
			case 'submit':
				el = $('<input name="' + field.name + '" type="' + field.type + '" />').attr({
					'class': 'xgbutton',
					'value': field.value
				}).click(field.click);
				break;

			case 'hidden':
				el = $('<input name="' + field.name + '" type="' + field.type + '" />').attr('value', field.value);
				break;

			case 'checkbox':
			case 'radio':
				el = $('<ul />', {
					'class': field.type + '-list'
				});

				for(var i in field.list) {
					var item = field.list[i];

					var li = $('<li />').appendTo(el);
					$('<input' + (item.checked ? ' checked="checked"' : '') + ' name="' + field.name + '" type="' + field.type + '" />').attr({
						'class': field.type,
						'id': field.name + '-' + i,
						'value': item.value
					}).appendTo(li);

					$('<label />', {
						'for': field.name + '-' + i,
						'html': item.label
					}).appendTo(li);
				}
				break;

			case 'message':
				el = field.value;
				break;

			case 'text':
				el = $('<input name="' + field.name + '" type="' + field.type + '" />').attr({
					'autocomplete': field.autocomplete,
					'class': 'text' + (field['class'] ? ' ' + field['class'] : ''),
					'id': field.id ? field.id : field.name,
					'maxlength': field.maxlength,
					'size': field.size,
					'value': field.value
				});
				break;

			default:
				break;
		}

		return el;
	}

	$.buildForm.defaults = {
		'method': 'post'
	};
})(jQuery);


/**
 * ���������� ������ ������� ������
 *
 * @param {Boolean} dot		���� true, �� ��������� ������� ������ �����
 * @return {String}			���������� ������� � �������
 */
function getDomain(dot) {
	var domain = document.domain;
	var arr = domain.split('.');
	var length = arr.length;

	return (dot ? '.' : '') + arr[length - 2] + '.' + arr[length - 1];
}


/*
 * ���������
 */
/**
 * ������������� �������������� ������������� � ���������
 *
 * @param {Object} el
 */
function initInventoryPropertyExpander(el) {
	jQuery(el ? el : '.properties-expander').click(function() {
		var ul = jQuery(this).nextAll('ul.invisible:first');
		this.src = imgURL + (ul.is(':visible') ? 'sb_plus.gif' :'sb_minus.gif');
		ul.toggle();
	}).tooltip(jQuery.extend({}, tooltipDefaults, {
		bodyHandler: function() {
			return '<ul>' + jQuery(this).nextAll('ul.invisible:first').html() + '</ul>';
		},
		extraClass: 'properties'
	}));
}
/* end: ��������� */



/**
 * ���������� � �����
 *
 * @param {Integer} id
 */
function speak(id){
	top.frames['main'].document.location.href = '/npc.php?cmd=npc&nid=' + id + '&' + Math.random();
}

/**
 * ���������� � ����� � ��������
 *
 * @param {Integer} id
 */
function pickupsubw(id, slog){
	top.frames['main'].document.location.href = '/subway.php?cmd=pickup&sid=' + id + '&lid=' + slog + '&' + Math.random();
}

/**
 * ������� �������
 *
 * @param {Integer} id
 */
function speaksubw(id){
	top.frames['main'].document.location.href = '/subway.php?cmd=npc&nid=' + id + '&' + Math.random();
}

/**
 * ��������� ����
 *
 * @param {Integer} id
 */
function attacksubw(id){
	top.frames['main'].document.location.href = '/subway.php?cmd=attack_bot&bot=' + id + '&n=' + id + '&' + Math.random();
}

function meddlesubw(id){
	top.frames['main'].document.location.href = '/subway.php?cmd=meddle_bot&bot=' + id + '&n=' + id + '&' + Math.random();
}

/**
 * ������ ������
 *
 * @param {String} text
 */
function tort(text){
	text = unescape(text);

	if (top.frames['chat'] && top.frames['chat']['chat2'].oChat) {
		top.frames['chat']['chat2'].oChat.throwThing(text, 'cake');
	}
}

/**
 * ������ �������
 *
 * @param {String} text
 */
function snezhok(text){
	text = unescape(text);

	if (top.frames['chat'] && top.frames['chat']['chat2'].oChat) {
		top.frames['chat']['chat2'].oChat.throwThing(text, 'snowball');
	}
}


/**
 * ��������� ����������� ���� ���������
 *
 * @param {String} login	��� ���������
 * @param {Number} battle	��������� ���������� � ���
 * @return					���������� HTML-��� ������������ ����
 */
function getUserContextMenu(login, battle) {
	var html = '';

	// �������� ���� ���� ����� �������
    var i1 = login.indexOf('[')
	var i2 = login.indexOf(']');
    if (i1 >= 0 && i2 > 0) {
		login = login.substring(i1 + 1, i2);
	}
	login = escape(login);

    html +=
		'<li><a href="#" onclick="top.AddTo(\'' + login + '\');return false;">�������</a></li>' +
		'<li><a href="#" onclick="top.AddToPrivate(\'' + login + '\');return false;">�������</a></li>' +
		'<li class="separator"><a href="#" onclick="window.open(\'/inf=' + urlEncode(unescape(login)) + '\', \'_blank\');return false;">����������</a></li>' +
		'<li><a href="#" id="context-copy-nick" onclick="return false;" rel="' + login + '">���������� ���</a></li>';

	var monthType = monthTypes[new Date().getMonth()];
    if(monthType == 'winter') {
		html += '<li><a href="#" onclick="snezhok(\'' + login + '\');return false;">������ �������</a></li>';
    } else if( monthType == 'summer' ) {
		html += '<li><a href="#" onclick="tort(\'' + login + '\');return false;">������ ������</a></li>';
    }

    html += '<li class="separator"><a href="#" onclick="contacts2(\'' + login + '\');return false;">� ��������</a></li>';

	if (top && top.ignore && top.ignore[unescape(login)]) {
		html += '<li><a href="#" onclick="unignore2(\'' + login + '\');return false;">�� ������</a></li>';
	} else {
		html += '<li><a href="#" onclick="ignore2(\'' + login + '\');return false;">� �����</a></li>'
	}

    if (battle > 0) {
		html += '<li><a href="#" onclick="window.open(\'/log.php?log=' + battle + '\', \'_blank\');return false;">���������� ���</a></li>';
	}

    return html;
}

function getBotContextMenu(login, id, battle, meddle, attack, speaking){
	var html = '';

	var i1 = login.indexOf('[');
	var i2 = login.indexOf(']');
	if (i1 >= 0 && i2 > 0) {
		login = login.substring(i1 + 1, i2);
	}
	login = escape(login);

	html +=
		(speaking > 0 ? '<li><a href="#" onclick="speak(\'' + id + '\');return false;">����������</a></li>' : '') +
		'<li><a href="#" onclick="top.AddTo(\'' + login + '\');return false;">�������</a></li>' +
		'<li><a href="#" onclick="top.AddToPrivate(\'' + login + '\');return false;">�������</a></li>' +
		'<li class="separator"><a href="#" onclick="window.open(\'/inf=' + urlEncode(unescape(login)) + '\', \'_blank\');return false;">����������</a></li>' +
		'<li><a href="#" id="context-copy-nick" rel="' + login + '" onclick="return false;">���������� ���</a></li>';

	var monthType = monthTypes[new Date().getMonth()];
	if (monthType == 'winter') {
		html += '<li><a href="#" onclick="snezhok(\'' + login + '\');return false;">������ �������</a></li>';
	} else if (monthType == 'summer') {
		html += '<li><a href="#" onclick="tort(\'' + login + '\');return false;">������ ������</a></li>';
	}

	if (battle > 0) {
		html += '<li><a href="#" onclick="window.open(\'/log.php?log=' + battle + '\', \'_blank\');return false;">���������� ���</a></li>';

		if (meddle > 0) {
			html += '<li><a href="#" onclick="meddle(\'' + id + '\');return false;">���������</a></li>';
		}

		if (attack > 0) {
			html += '<li><a href="#" onclick="attack(\'' + id + '\');return false;">�������</a></li>';
		}
	} else if (attack > 0) {
		html += '<li><a href="#" onclick="attack(\'' + id + '\');return false;">�������</a></li>';
	}

	return html;
}

function getSUBWContextMenu(login, id, battle, meddle, attack, speaking, pickup){
	var html = '';

	slogin = (login);
	login = escape(login);



	if (pickup > 0) {
		html += '<li><a href="javascript:pickupsubw(\'' + id + '\', ' + slogin + ');">������� �������</a></li>';
	} else {

	if (speaking > 0) {
		html += '<li><a href="javascript:speaksubw(\'' + id + '\');">����������</a></li>';
	}

	if (battle > 0) {
		html += '<li><a href="/logs.php?log=' + battle + '" target="_blank">���������� ���</a></li>';

		if (meddle > 0) {
			html += '<li><a href="javascript:meddlesubw(\'' + id + '\');">��������� �� ' + slogin + '</a></li>';
		}
		if (attack > 0) {
			html += '<li><a href="javascript:attacksubw(\'' + id + '\');">������� �� ' + slogin + '</a></li>';
		}
	} else if (attack > 0) {
		html += '<li><a href="javascript:attacksubw(\'' + id + '\');">������� �� ' + slogin + '</a></li>';
	}
                //  class="separator"
	html += '<li><a href="/inf=' + urlEncode(unescape(login)) + '" target="_blank">����������</a></li>';

	}

	return html;
}


function getNPCContextMenu(login, id, battle, meddle, attack, speaking){
	var html = '';

	login = escape(login);

	if (speaking > 0) {
		html += '<li><a href="javascript:speak(\'' + id + '\');">����������</a></li>';
	}

	html +=
		'<li><a href="javascript:top.AddTo(\'' + login + '\');">�������</a></li>' +
		'<li><a href="javascript:top.AddToPrivate(\'' + login + '\');">�������</a></li>' +
		'<li class="separator"><a href="/inf=' + urlEncode(unescape(login)) + '" target="_blank">����������</a></li>';/* +
		'<li><a href="#" id="context-copy-nick" rel="' + login + '" onclick="return false;">���������� ���</a></li>';*/

	var monthType = monthTypes[new Date().getMonth()];
	if (monthType == 'winter') {
		html += '<li><a href="javascript:snezhok(\'' + login + '\');">������ �������</a>';
	} else if (monthType == 'summer') {
		html += '<li><a href="javascript:tort(\'' + login + '\');">������ ������</a></li>';
	}

	if (battle > 0) {
		html += '<li><a href="/log.php?log=' + battle + '" target="_blank">���������� ���</a></li>';

		if (meddle > 0) {
			html += '<li><a href="javascript:meddle(\'' + id + '\');">���������</a></li>';
		}
		if (attack > 0) {
			html += '<li><a href="javascript:attack(\'' + id + '\');">�������</a></li>';
		}
	} else if (attack > 0) {
		html += '<li><a href="javascript:attack(\'' + id + '\');">�������</a></li>';
	}

	return html;
}

function getSuspUserContextMenu(login) {
	var html = '';

	login = escape(login);

	html += '<li><a href="#" id="context-copy-nick" onclick="return false;" rel="' + login + '">���������� ���</a></li>';

	if (!options) {
		var options = {
			f_date: '',
			f_date2: '',
			f_city: '0',
			f_curcity: '0',
			f_status: 'none',
			f_type: '0',
			f_user: '',
			rows_on_page: '20'
		};
	}

	html += '<li><a href="#" onclick="window.open(\'/suspicions.php?cmd=suspicions_show&f_date='+
		options.f_date+'&f_date2='+options.f_date2+'&f_city='+options.f_city+'&f_curcity='+
		options.f_curcity+'&f_type='+options.f_type+'&f_status='+options.f_status+'&rows_on_page='+
		options.rows_on_page+'&f_user='+
		urlEncode(unescape(login))+'\', \'_blank\');return false;">������� � ������ ����</a></li>';

	return html;
}

var nickZeroClip;

/**
 * ��������� ����������� ���������� ��� ���������
 *
 * @param {Object} el
 */
function makeCopiableNick(el) {
	if(el) {
		if(jQuery.browser.msie && jQuery.browser.versionX == 6) {
			jQuery(el).click(function() {
				window.clipboardData.setData('Text', unescape(jQuery(this).attr('rel')));
			});
		} else {
			ZeroClipboard.setMoviePath('/f/ZeroClipboard.swf');
			if (nickZeroClip) {
				nickZeroClip.destroy();
			}
			nickZeroClip = new ZeroClipboard.Client();

			nickZeroClip.glue(el);
			nickZeroClip.setText(unescape(jQuery(el).attr('rel')));

			nickZeroClip.addEventListener('onComplete', hideContext);
			nickZeroClip.addEventListener('onMouseOver', preventAutoHide);
			nickZeroClip.addEventListener('onMouseOut', activateAutoHide);
		}
	}
}

/**
 * �������� ���������� ����
 */
function hideContext() {
	clearTimeout(autoHideContext);
	autoHideContextActive = false;
	jQuery(document).trigger('click.jeegoocontext');
}

var autoHideContext;
var autoHideContextActive = false;

/**
 * ���������� ����������� ������������ ����
 */
function activateAutoHide() {
	if (!autoHideContextActive) {
		autoHideContextActive = true;
		autoHideContext = setTimeout(function(){
			jQuery(document).trigger('click.jeegoocontext');
		}, 5000);
	}
}

/**
 * ������������� ������� ������������ ����
 */
function preventAutoHide() {
	clearTimeout(autoHideContext);
	autoHideContextActive = false;
}

/**
 * ��������� ����������� ���� � �������� ���������� context (� ��������� �� ���)
 *
 * @param {Object} context	������ ��������, ������� ����������� ����������� ����
 * @param {Boolean} isChat	true ��� ���� ����
 */
function addContextMenu(context, isChat) {
	if (!context || !context.length) {
		context = jQuery('.context');
	}

	var oldOpera = jQuery.browser.opera && jQuery.browser.version < 10.5;

	// ������������� ������������ ����
	if(jQuery.fn.jeegoocontext && context.length) {
		var contextMenu = jQuery('#context-menu');
		if(!contextMenu.length) {
			contextMenu = jQuery('<ul />', {
				id: 'context-menu'
			})
			jQuery('body').append(contextMenu);

			contextMenu.hover(preventAutoHide, activateAutoHide);
		}

		var events = oldOpera ? 'click.context' : 'contextmenu.context'
		context.unbind(events);

		function getMenu(event) {
			// ������� ���������� ����
			contextMenu.empty();

			var $context = jQuery(event.target);
			var menu = jQuery('<li />');
			var login = $context.text();

			if (!(isChat && event.target.tagName.toLowerCase() != 'span')) {
				// ����� ���������������� ������������ ���� � ����������� �� ������
				if ($context.hasClass('user')) {
					var battle = parseInt($context.attr('rel'));
					menu = getUserContextMenu(login, battle);
					contextMenu.append(menu);
				} else if ($context.hasClass('bot')) {
					var arr = $context.attr('rel').split(',');
					menu = getBotContextMenu(login, arr[0], arr[1], arr[2], arr[3], arr[4]);
					contextMenu.append(menu);
				} else if ($context.hasClass('npc')) {
					var arr = $context.attr('rel').split(',');
					menu = getNPCContextMenu(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]);
					contextMenu.append(menu);
				} else if($context.is('img.status, img.avatar') && $context.parent().hasClass('npc')) {
					var arr = $context.parent().attr('rel').split(',');
					menu = getNPCContextMenu(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]);
					contextMenu.append(menu);
				} else if($context.is('img.status') && $context.parent().hasClass('subw')) {
					var arr = $context.parent().attr('rel').split(',');
					menu = getSUBWContextMenu(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6]);
					contextMenu.append(menu);
				} else if ($context.hasClass('susp_user')) {
					menu = getSuspUserContextMenu(login);
					contextMenu.append(menu);
				} else {
					var battle = parseInt($context.attr('rel'));
					menu = getUserContextMenu(login, battle);
					contextMenu.append(menu);
				}
			}

			top.copyNick = contextMenu.find('#context-copy-nick').get(0);
		}

		context.bind(events, getMenu);

		context.nojeegoocontext('context-menu');
		context.jeegoocontext('context-menu', {
			delay: 0,
			fadeIn: 0,
			heightOverflowOffset: 2,
			isChat: isChat,
			onHide: function(e, context) {
				preventAutoHide();
				if (nickZeroClip) {
					nickZeroClip.hide();
				} else {
					jQuery(top.copyNick).unbind('click');
				}
			},
			onHover: preventAutoHide,
			onShow: function(event, context, lol) {
				setTimeout(function(){
					makeCopiableNick(top.copyNick);
					if (nickZeroClip) {
						nickZeroClip.show();
					}
				}, 10);

				activateAutoHide();
			},
			operaEvent: oldOpera ? 'dblclick' : 'contextmenu',
			widthOverflowOffset: 2
		});
	}
}
/* end: Context menu */






function confirmAbility(title, visible_fields, hidden_fields, options){
	var pars = options['action'];
	for (var i in hidden_fields) {
		pars += i + '=' + hidden_fields[i] + '&';
	}
	if (confirm(title + '?')) {
		document.location.href = pars + Math.random();
	}
}

function openDialogWindow(title, visible_fields, hidden_fields, options){
	if ((hidden_fields["cmd"] == "ability.wsacrifice") || (hidden_fields["cmd"] == "ability.wchangeside")) {
		confirmAbility(title, visible_fields, hidden_fields, options);
		return false;
	}

	var div = document.getElementById("win1");
	if (!div) {
		alert('�� ������ ���������� ����');
		return 1
	}

	var html = '<table border="0" width="100%" cellspacing="1" cellpadding="0" bgcolor="#92a2af"><tbody><tr><td align="center"><strong>' + title + '</strong></td><td width="1%" align="right" valign="middle" style="cursor: pointer" onclick="document.getElementById(\'win1\').style.visibility=\'hidden\';formname=\'\';"><img src="http://img.blutbad.ru/i/x.gif" alt="" /></td></tr><tr><td colspan="2"><form action="' + options['action'] + '" style="padding:0;margin:0" method="post"><table border="0" width="100%" cellspacing="2" cellpadding="2" bgcolor="#dee2e7"><tbody>';

	var first_input = '';

	for (var i in visible_fields) {
		first_input = first_input ? first_input : i;
		html += '<tr><td>' + visible_fields[i] + ':</td><td><input class="text' + ( i == 'name' ? ' paste-nick' : '' ) + '" type="text" id="' + i + '" name="' + i + '" value="" /></td></tr>';
	}

	html += '<tr><td colspan="2">';

	for (var i in hidden_fields) {
		html += '<input type="hidden" name="' + i + '" value="' + hidden_fields[i] + '" />';
	}

	html += '<input class="xgbutton" type="button" value="OK" onclick="submit(true);" /></td></tr></tbody></table></form></td></tr></tbody></table>';

	div.innerHTML = html;

	div.style.visibility = "visible";
	div.style.left = options['width'] || 150;
	div.style.top = options['top'] || (document.body.scrollTop + 100);
	document.getElementById(first_input).focus();
	formname = 'name';
}


/**
 * ���������� ������ ����������� ����. ������ ���������� ����, ���� ��� �����������
 *
 * @param {String} 	id	id ����������� ����
 * @return {Object}		������ ����������� ����, false - ���� ��� jQuery UI
 */
function getDialog(id) {
	if(!id) {
		id = 'common-dialog';
	}

	var dialog = jQuery('#' + id);

	if(dialog.length) {
		dialog.dialog('option', 'width', 'auto');
		return dialog;
	} else {
		dialog = jQuery('<div />', {
			'class': 'invisible',
			id: id
		}).appendTo('body');

		dialog.dialog({
			autoOpen: false,
			minHeight: 0,
			open: function(event, ui) {
				dialog.find('.text:first').focus();
			},
			width: 'auto'
		});

		return dialog;
	}
}

/**
 * ���������� ���� ���������� ���������
 */
function savekomplDialog(url, title, params) {
	var select = getDialog();

	if(!select) return false;

	var html = '';
	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="select-dialog-form" method="post">' +
			'<input name="cmd" type="hidden" value=' + params.cmd + ' />' +
			(params.nd ? '<input name="nd" type="hidden" value="1" />' : '') +

			'<table>' +
				'<tbody>' +
					'<tr>' +
						'<td class="label">' +
							'<label for="kit">��������:</label>' +
						'</td>' +
						'<td class="input">' +
							'<input class="text paste-nick" type="text" id="kit" name="kit" />' +
						'</td>' +
						'<td>' +
							'<input class="xgbutton" type="submit" value="OK" />' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>' +
		'</form>';


	select.html(html);
	select.dialog('option', 'title', title);
	dialogOpen(select);
}

/**
 * ���������� ���� � ������� ���� � ����������� �����������
 */
function selectDialog(url, title, params) {
	var select = getDialog();

	if(!select) return false;

	var html = '';
	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="select-dialog-form" method="post">' +
			'<input name="cmd" type="hidden" value=' + params.cmd + ' />' +
			(params.nd ? '<input name="nd" type="hidden" value="' + params.nd + '" />' : '') +

			'<table>' +
				'<tbody>' +
					'<tr>' +
						'<td class="label">' +
							'<label for="' + params.nick + '">�����:</label>' +
						'</td>' +
						'<td class="input">' +
							'<input class="text paste-nick" type="text" id="' + params.nick + '" name="' + params.nick + '" />' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td class="label">' +
							(params.select.label ? '<label for="' + params.select.name + '">' + params.select.label + ':</label>' : '') +
						'</td>' +
						'<td class="input">' +
							'<select id="' + params.select.name + '" name="' + params.select.name + '">';
	for(var i in params.select.list) {
		html +=					'<option value="' + i +'">' + params.select.list[i] + '</option>';
	}
	html +=					'</select>' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td></td>' +
						'<td>' +
							'<input class="xgbutton" type="submit" value="OK" />' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>' +
		'</form>';


	select.html(html);
	select.dialog('option', 'title', title);
	dialogOpen(select);
}
/*
 * end: Dialog window functions
 */


/**
 * ����������� ������� ��� �������� ����������� ���� ��� IE 6,7 � ������, ����� ��� ����� ����������
 *
 * @see jQuery UI Dialog
 * @param {Object} dialog	jQuery-������ ����������� ����
 */
function dialogOpen(dialog) {
	width = dialog.dialog('option', 'width');
	if(width == 'auto' && jQuery.browser.msie && jQuery.browser.versionX < 8) {
		/*var prev = dialog.prev();
		prev.width(0);
		dialog.dialog('open');
		prev.width(dialog.outerWidth());*/

		var clone = $('<div />', {
			html: dialog.html()
		}); // jQuery 1.5+
		//var clone = dialog.clone(); // jQuery 1.4.4
		clone.appendTo('body').css({
			left: '-1000px',
			position: 'absolute',
			top: '-1000px'
		}).show();

		width = clone.width() + 22;
		dialog.dialog('option', 'width', width);
		dialog.dialog('open');
		document.write(clone).remove();
	} else {
		dialog.dialog('open');
	}
}

/**
 * ����� ������ ����������� ����
 *
 * @param {String} url
 * @param {String} title
 * @param {String} name
 * @param {Object} hiddens
 * @param {Object} value
 */
function commonDialog(url, title, name, hiddens, value) {
	var common = getDialog();

	if(!common) return false;

	var html = '';
	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="nowrap" method="post">';

	for(var i in hiddens) {
		if(hiddens[i]) {
			html += '' +
			'<input name="' + i + '" type="hidden" value="' + hiddens[i] + '" />';
		}
	}

	html += '' +
			'<label for="' + name + '">�����:</label> ' +
			'<input class="text paste-nick" id="' + name + '" name="' + name + '" type="text"' + (value ? ' value="' + value + '"' : '') + ' /> ' +
			'<input class="xgbutton" type="submit" value="OK" />' +
		'</form>';

    Hint3Name = name;
	common.html(html);
	common.dialog('option', 'title', title);

	var input = jQuery('#' + name);
	// ������ ����� �� ���� ����� ��� ������ ����
	common.unbind('dialogopen').bind('dialogopen', function(event, ui) {
		input.focus();
	});
	if(input.is(':visible')) {
		input.focus();
	}

	dialogOpen(common);
}

/**
 * ����� ������ ������� �������������
 *
 * @param {String} url
 * @param {Object} params
 * @param {String} question
 */
function commonConfirm(url, params, question) {
	if(!question) question = '������������ ������?';

	if (confirm(question)) {
		var loc = url + '?';
		for(var i in params) {
			loc += i + '=' + params[i] + '&';
		}
		loc += Math.random();
		window.location = loc;
	}
}

function commonPriceDialog(url, title, name, hiddens, value, price) {
	var common = getDialog();

	if(!common) return false;

	var html = '';
	html += '' +
		'<form action="' + url + '?' + Math.random() + '" class="price-table nowrap" method="post">';

	for(var i in hiddens) {
		if(hiddens[i]) {
			html += '' +
			'<input name="' + i + '" type="hidden" value="' + hiddens[i] + '" />';
		}
	}

	html += '' +
			'<table>' +
				'<tbody>' +
					'<tr>' +
						'<td>' +
							'<label for="' + name + '">�����:</label> ' +
						'</td>' +
						'<td>' +
							'<input class="text paste-nick" id="' + name + '" name="target" type="text"' + (value ? ' value="' + value + '"' : '') + ' />' +
						'</td>' +
						'<td>' +
							'<input type="submit" class="xgbutton" value="OK" />' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td>' +
							'<label for="magic-price">���� (��.):</label>' +
						'</td>' +
						'<td colspan="2">' +
							'<input class="text" id="magic-price" name="magic_price" type="text" value="' + price.value + '"/>' +
						'</td>' +
					'</tr>' +
					'<tr>' +
						'<td></td>' +
						'<td colspan="2">' +
							'(���� �� ' +
							'<a class="js min-price" href="#">' + price.min + '</a>' +
							' �� ' +
							'<a class="js max-price" href="#">' + price.max + '</a>' +
							' ��.)' +
						'</td>' +
					'</tr>' +
				'</tbody>' +
			'</table>' +
		'</form>';

    Hint3Name = name;
	common.html(html);

	common.find('a.min-price').click(function() {
		jQuery('#magic-price').val(jQuery(this).text());
		return false;
	});

	common.find('a.max-price').click(function() {
		jQuery('#magic-price').val(jQuery(this).text());
		return false;
	});

	common.dialog('option', 'title', title);
	dialogOpen(common);
}

function teleportDialog(url, title, params){
	var teleport = getDialog();

	if(!teleport) return false;

	var html = '';
	html += '<form action="' + url + '?' + Math.random() + '" class="teleport-dialog-form nowrap" method="post" name="teleport-form" style="text-align: center;">';
	for(var v in params){
		if(v == "nick"){
			if(params.nick) {
				html += '' +
					'<label for="' + params.nick + '">�����:</label> ' +
					'<input class="text paste-nick" type="text" id="' + params.nick + '" name="' + params.nick + '" />' +
					'<br />';
			}
		}else if(v == "select"){
		}else{
			html +=
			'<input name="' + v + '" type="hidden" value="' + params[v] + '" />'
			;
		}
	}

	var counter = 1;

	params.select.list.sort(function(a, b) {
		return a.id - b.id;
	});

	for (var i in params.select.list) {
		if (params.select.current == params.select.list[i].id) {
			html += '<img alt="" class="current" src="' + imgURL + 'teleport/' + params.select.list[i].id + '.gif" title="' + params.select.list[i].name + '" />';
		} else {
			html += '<img alt="" onclick="forms[\'teleport-form\'].' + params.select.name + '.value = ' + params.select.list[i].id + '; forms[\'teleport-form\'].submit()" src="' + imgURL + 'teleport/' + params.select.list[i].id + '.gif" title="' + params.select.list[i].name + '" />';
		}

		if(counter % 4 == 0) html += '<br />';

		counter++;
	}

	html += '<input name="' + params.select.name + '" type="hidden" />' +
		'</form>';

	teleport.html(html);
	jQuery('.teleport-dialog-form img:not(.current)').hover(
		function() {
			jQuery(this).addClass('hover');
		},
		function() {
			jQuery(this).removeClass('hover');
		}
	);
	teleport.dialog('option', 'title', title);
	dialogOpen(teleport);
}

/**
 * ����������� ��������� � ������� ������ ����
 */
function showNotification(text, error, title) {
	if(text) {
		if(!jQuery('#notofies').length) {
			jQuery('body').append('<div id="notifies" />');
		}
		var notify = jQuery.pnotify({
			pnotify_title: title ? title : '��������!',
			pnotify_type: error ? 'error' : 'notice',
			pnotify_text: text
		});
		notify.appendTo('#notifies').click(notify.pnotify_remove);
	}
}

function showHelpHint(helpHintObject) {
	if(helpHintObject) {
		jQuery(function() {
			var helpDialog = $('<div />', {
				css: {
					backgroundColor: helpHintObject.background,
					color: '#000000'
				},
				id: 'help-hint',
				html: helpHintObject.content,
				title: '���������'
			});

			helpDialog.append('<div style="margin-top: 1em; text-align: center; width: 100%;"><input class="xgbutton" type="button" value="�������" /></div>')

			helpDialog.appendTo('body').dialog({
				close: function(event, ui) {
					$.post('/settings_ajax.php',{
						cmd: 'helphint_viewed',
						code: helpHintObject.code,
						is_ajax: 1
					});
				},
				dialogClass: 'help-hint',
				draggable: false,
				minHeight: 0,
				open: function(event, ui) {
					helpDialog.parent().css('opacity', .85);


				},
				width: Number(helpHintObject.width) + 22
			});

            helpDialog.children(".ui-dialog-titlebar").css('text-align', 'center');

			helpDialog.find('.xgbutton').click(function() {
				helpDialog.dialog('close');
			});

			helpDialog.parent().append('<div class="tl"></div><div class="tr"></div><div class="bl"></div><div class="br"></div>');

			var helphint_time = (Number(helpHintObject.time) * 1000) || 60000;
			setTimeout(function() {
				helpDialog.dialog('close');
			}, helphint_time);
		});
	}
}

var smilies = {
	'smile': {
		codes: ['smile', ''],
		group: 1,
		height: 18,
		width: 18
	},
	'laugh': {
		codes: ['laugh', ''],
		group: 1,
		height: 15,
		width: 15
	},
	'fingal': {
		codes: ['fingal', ''],
		group: 1,
		height: 15,
		width: 22
	},
	'eek': {
		codes: ['eek', ''],
		group: 1,
		height: 15,
		width: 15
	}
}

smileyCodes = {
	'smile': 'smile',
	'': 'smile',

	'laugh': 'laugh',
	'�����': 'laugh',

	'fingal': 'fingal',
	'': 'fingal',

	'eek': 'eek',
	'': 'eek'
};


/**
 * �������������� ���� �� ����������
 *
 * @param buttons	�������� ������ ��� ������ ���� ���������
 * @param target	�������� ��������, ���� ����������� ��������
 * @param maxLength	������������ ����� �������� � ������
 */
function initSmiliesDialog(buttons, target, maxLength) {
	var $ = jQuery;

	var smiliesDialog = $('#smilies-dialog');

	if(smiliesDialog.length) {
		smiliesDialog.dialog({
			autoOpen: false,
			height: 'auto',
			minHeight: 0,
			width: 500
		});

		$(buttons).click(function() {
			smiliesTarget = $(target)[0];
			smiliesTargetMaxLength = maxLength;

			if ($(window).height() > 600 && $(window).width() > 400) {
				smiliesDialog.dialog('open');
			} else {
				var left = Math.floor(screen.width / 2 - 400 / 2);
				var top = Math.floor(screen.height / 2 - 515 / 2);
				window.open('/smilies.html', '_blank', 'height=500, left=' + left + ', location=no, resizable=no, top=' + top + ', width=400');
			}
		});
	}
}

/**
 * ������������ ���� ��������� � �������� � ������������ �� ����� ���������
 *
 * @param {String} str		������ ��� ���������������
 * @param {Integer} limit 	����������� �� ����� �������������� �������
 * @return {String} 		���������� ����������������� ������
 */
function smiley2img(str, limit) {
	if (limit == undefined) {
		limit = 3;
	}

	if (limit !== 0) {
		var arr = str.match(/:[a-z�-�0-9_]+:/g);

		if (arr) {
			var i = 0;
			var counter = 0;

			while (counter < limit && i < arr.length) {
				var code = arr[i].replace(/:/g, '');

				if (smileyCodes[code]) {
					code = smileyCodes[code];
					str = str.replace(arr[i], '<img alt="&#058;' + code + '&#058;" class="smiley" height="' + smilies[code].height + '" src="' + imgURL + 'sm/' + code + '.gif" title="&#058;' + smilies[code].codes.join('&#058;  &#058;') + '&#058;" width="' + smilies[code].width + '" />');
					counter++;
				}

				i++;
			}
		}
	}

	return str;
}
/**
 * ������� ���� �� ��� ��������� ����
 *
 * @param nick			������ ����
 * @param compatibility	�������� �������� ������������� � ���������� �������
 * @return 				true, ���� ������� ���� �������, false, ���� �� ����� ������� �� ����
 */
function pasteNick(nick, compatibility) {
	var fields = jQuery('.paste-nick:visible');

	if(jQuery('#win1').css('visibility') == 'hidden') {
		fields = fields.filter(':not(#win1 .paste-nick);');
	}

	if(fields.length) {
		fields.removeClass('placeholder').val(nick);
		var last = fields.filter(':last');
		last.focus();

		if (jQuery.browser.msie) {
			moveCaretToEnd(last[0]);
		}

		return compatibility ? false : true;
	} else {
		return compatibility ? true : false;
	}
}

function pnotify(message, error, title) {
	if(message) {
		var notify = $.pnotify({
			pnotify_title: title ? title : '��������!',
			pnotify_type: error ? 'error' : 'notice',
			pnotify_text: message //json.error ? json.error : json.message
		});
		notify.appendTo('#notifies').click(notify.pnotify_remove);
	}
}

/**
 * ���������� ������� � ����� ������
 *
 * @param {Object} inputObject	������-������
 */
function moveCaretToEnd(inputObject) {
    if (inputObject.createTextRange) {
        var r = inputObject.createTextRange();
        r.collapse(false);
        r.select();
    }
    else if (typeof inputObject.selectionStart != 'undefined') {
        var end = inputObject.value.length;
        inputObject.setSelectionRange(end, end);
        inputObject.focus();
    }
}

/**
 * �������� ��������� � ������
 *
 * @param {String} name		��� ���������
 */
function addToPrivate(name) {
	var privateText = 'private [' + name + ']';

	if (top.frames['down']) {
		var text = top.frames['down'].document.forms[0].text;
	} else if(window.opener && window.opener.top.frames['down']) {
		var text = window.opener.top.frames['down'].document.forms[0].text;
	}

	var toText = 'to [' + name + ']';
	while (text.value.indexOf(toText) !== -1) {
		text.value = text.value.replace(toText, privateText);
	}

	if (text.value.indexOf(privateText) == -1) {
		text.value = privateText + ' ' + text.value;
	}

	text.focus();
}

function mentorDealDialog(mentor,deal,nd) {
	var dialog = getDialog();

	var html = '' +
		'<form action="transfer.php?' + Math.random() + '" method="post">' +
			'<a href="/inf=' + mentor + '" target="_blank">' +
				'<b>'+mentor+'</b>' +
			'</a> ' +
			'���������� ��� ����� ����� �����������.<br />' +
			'��������� ��������� ������� ��� ����������� � ���������� ����.<br />' +

			'<div style="margin-top: .5em; text-align: center;">' +
				'<input class="xgbutton" type="submit" value="������� � ����������" /> ' +
			'</div>' +
		'</form>';

	dialog.html(html);
	dialog.dialog('option', 'title', '����������� ����������');
	dialog.dialog('option', 'width', 450);
	dialog.dialog('open');
}


function showAchBig(ach, user_id) {
	var dialog = getDialog();
   if (ach == "cave_item") {
	dialog.dialog('option', 'title', '��������');
	dialog.dialog('option', 'width', 690);
   } else {
	dialog.dialog('option', 'title', '����������');
	dialog.dialog('option', 'width', 642);
   }

	dialog.html('<div>���� ��������</div>');
	dialog.dialog('open');

    dialog

	var params = {
		ach: ach,
		cmd: 'get_ach',
		is_ajax: 1
	};
	if (user_id) params.user_id = user_id;
	else if (top.userId) params.user_id = top.userId;

	jQuery.post('common_ajax.php',params,
		function(data) {
           if (ach == "cave_item") {
			dialog.html(htmlCaveBig(data));
			dialog.dialog('option', 'top', '100');
           } else {
			dialog.html(htmlAchBig(data));
           }
			dialog.dialog('option', 'height', 'auto');
		}
	);
}

function htmlCaveBig(data) {
	var value = '';

  if (data.code == 1) {
	return '��� ���������';
  } else {
   var df = 111;
    if (data.aims[0].list.length > 6) {
      df = 311;
    } else if (data.aims[0].list.length > 3) {
      df = 211;
    }

	value += '<div>�������� ��������:</div><br>';
	value += '<div style=" overflow: auto; height: '+df+'px;"><ul>';
	for (var i=0;i<data.aims.length;i++) {
		var aim = data.aims[i];
		var ij = 1;

		if (aim.list && aim.list.length) {
			value += '<ul class="ach_aim_list">';
			for (var j=0;j<aim.list.length;j++) {
				value += '<li style="text-align: center;float: left;padding-left: 5;padding-right: 5;border: 1px solid #696969;padding: 0.3em 0.5em;width: 190px;height: 85;margin: 5px;">';
			    	value += '<b style="float: left;">' + (j + ij) + '</b><br>';
			    	value += '<div style="vertical-align: middle;font-weight: bold;"></b><img alt="' + aim.list[j].text + '" class="thumb" src="http://img.blutbad.ru/i/' + aim.list[j].img + '" title="' + aim.list[j].text + '">';
			    	value += '<br>' + aim.list[j].text + '</div>';
				value += '</li>';
			}
			value += '</ul>';
		}

	}
	value += '</ul></div>';
	return value;
  }

}

function htmlAchBig(data) {
	var value = '';
	value += '<div><table class="table-list" style="width: 600px; margin: 10px;"><tr>';
   	value += '<td class="img"><img alt="" height="80" src="http://img.blutbad.ru/i/achievement/'+data.code+'_f.jpg" width="80" /></td>';

	value += '<td><div><b>'+data.title+'</b></div>';
	value += '<div style="margin-left: 10px;">';
	if (data.desc) value += '<div><i>'+data.desc+'</i></div>';

	value += '<div>����:</div>';
	value += '<ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">';
	for (var i=0;i<data.aims.length;i++) {
		var aim = data.aims[i];
		var self_finished = false;
		if (data.self && ((data.self.state == "r" && aim.finished == 1) || data.self.state == "f")) self_finished = true;
		value += '<li>';
		if (self_finished) value += '<b>';
		value += aim.text;
		if (self_finished) value += '</b>';
		if (aim.list && aim.list.length) {
			value += '<ul class="ach_aim_list">';
			for (var j=0;j<aim.list.length;j++) {
				value += '<li>';
				if (self_finished || aim.list[j].done) value += '<b>';
				value += aim.list[j].text;
				if (self_finished || aim.list[j].done) value += '</b>';
				value += '</li>';
			}
			value += '</ul>';
		}
		value += '</li>';
	}
	value += '</ul>';

	value += '<div>�������:</div>';
	value += '<ul  style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">';
	for (var i=0;i<data.rewards.length;i++) {
		value += '<li>'+data.rewards[i].text+'</li>';
	}
	value += '</ul>';

	value += '</div>';

	if (data.self) {
		value += '<div>';
		if (data.self.state == "f") value += "���� ��� ���������� �������� "+data.self.datetime;
		else if (data.self.state == "n") value += "���� ��� ���������� �� ��������";
		else if (data.self.state == "r") value += "� ��� ��� ���������� � �������� ����������";
		value += '</div>';
	}
	value += '</td></tr></table></div>';

	return value;
}

function confirm_s(text, loc){
	if (confirm(text)) {
			return  window.location = loc;
	} else {
		return false;
	}
}

/**
 *  ����������� ������� ������ � ����
 */
(function($) {
	var interval = 100;
	var durationCoef = 3;
	var duration = interval * durationCoef;

	/**
	 * URL �������� ��� �������
	 */
	function getBgImage(o, width) {
		var css;
		if(width < 33) {
			css = '3';
		} else if(width < 66) {
			css = '2';
		} else {
			css = '1';
		}

		return imgURL + o.image + css + '.gif';
	}

	/**
	 * ������������� �������� �����
	 */
	function set(el) {
		var o = el.data('scale');
		if(o.current > o.max) {
			o.current = o.max;
		}

		var width = (o.max == 0) ? 0 : (o.current / o.max * 100);
		el.width(width + '%');

		var image = getBgImage(o, parseInt(width));
		el.css('background-image', 'url(' + image + ')');
	}

	/**
	 * ��������������� �� "������� �������" "������� �������"
	 */
	function regenerate(el) {
		if (typeof el != 'object') {
			el = this;
		}

		var o = el.data('scale');

		o.current += o.coef;

		if(o.current >= o.max) {
			clearInterval(o.interval);
			o.interval = 0;
		}

		set(el);
	}

	function getCoef(o) {
		return (o.max / 1000) * (o.speed / 100) / 1800 * interval;
	}

	$.fn.HpPw = function(options) {
		var self = true;
		for(var i in options) {
			if(options[i].self === false) {
				self = false;
			}
		}

        if(top.frames['panel'] && top.frames['panel'].$ && top.frames['panel'].$.fn.HpPwDigits && self) {
			top.frames['panel'].$('.hp-pw-digits').HpPwDigits(options);
		} else if(top.frames['player'] && top.frames['player'].$ && top.frames['player'].$.fn.HpPwDigits && self) {
			top.frames['player'].$('.hp-pw-digits').HpPwDigits(options);
		}

		return this.each(function() {
			var el = $(this);
			if (el.data('HpPw:options')) { // ���������� ������� �����
				var o = $(this).data('HpPw:options');

				for(var scale in options) {
					var scaleEl = $(this).find(o[scale].selector);
					var scaleOptions = scaleEl.data('scale');

					// ������������� �����������
					clearInterval(scaleOptions.interval);
					scaleOptions.interval = 0;

					// C����� �������� �����
					var current = scaleOptions.current;

					// ���������� ��������
					$.extend(scaleOptions, options[scale]);
					// ���������� �������� �������������� �����
					scaleOptions.coef = getCoef(scaleOptions);

					// ����� �������� ����� � ������ ��������
					var newCurWithAnim = Math.min(scaleOptions.current + scaleOptions.coef * durationCoef, scaleOptions.max);
					var delta = Math.abs(newCurWithAnim - current) / scaleOptions.max;

					if (delta > .1) { // ���� �������� ����� ������ ����������
						scaleOptions.current = newCurWithAnim;

						scaleEl.animate(
							{
								width: (newCurWithAnim / scaleOptions.max * 100) + '%'
							},
							duration, 'linear',
							jQuery.proxy(function() {
								var scaleEl = this;
								var scaleOptions = scaleEl.data('scale');

								set(this);

								// ��������� ����������� � ������ �������������
								if(scaleOptions.speed && scaleOptions.current < scaleOptions.max) {
									scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
								}
							}, scaleEl)
						);
					} else {
						set(scaleEl);

						// ��������� ����������� � ������ �������������
						if(scaleOptions.speed && scaleOptions.max && scaleOptions.current < scaleOptions.max) {
							scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
						}
					}
				}
			} else { // �������������
				var o = $.extend(true, {}, $.fn.HpPw.defaults, options);
				el.data('HpPw:options', o);

				for (var scale in o) {
					var scaleEl = el.find(o[scale].selector);
					var scaleOptions = o[scale];

					scaleOptions.coef = getCoef(scaleOptions);

					scaleEl.data('scale', scaleOptions);
					// ����������� ��������� ��� �������
					scaleEl.parent().tooltip({
						bodyHandler: function(){
							var data = $(this).children().data('scale');
							return '<span class="nowrap">' + data.name + ': ' + (data.invisible ? '??/??' : (Math.floor(data.current) + '/' + data.max)) + '</span>';
						},
						delay: 0,
						showURL: false,
						track: true
					});

					set(scaleEl);

					if (scaleOptions.speed && o[scale].max && scaleOptions.current < o[scale].max) {
						scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
					}
				}
			}
		});
	}

	$.fn.HpPw.defaults = {
		hp: {
			current: 1,
			image: 'green',
			invisible: false,
			max: 1,
			name: '������� �����',
			selector: '.hp',
			speed: 0
		},
		pw: {
			current: 1,
			image: 'blue',
			invisible: false,
			max: 1,
			name: '������� ������������',
			selector: '.pw',
			speed: 0
		}
	}




})(jQuery);

/**
 * ������ ��� ����������� �������� �������� � ������������ � ����� ������� ����
 *
 * @param {Object} $ jQuery 1.4+
 */
(function($) {
	var interval = 1000;
	var durationCoef = 3;
	var duration = interval * durationCoef;

	/**
	 * ������� ������ ��� ������� � ����������� �� ���������� ��������/������������
	 *
	 * @param {Number} width
	 * @return {String} className
	 */
	function getClass(width) {
		var className = 'current';
		if(width < 33) {
			className += ' current-3';
		} else if(width < 66) {
			className += ' current-3';
		}

		return className;
	}

	/**
	 * ������������� �������� �����
	 *
	 * @param {Object} el
	 */
	function set(el) {
		var o = el.data('scale');

		if(o.current > o.max) {
			o.current = o.max;
		}

		var className = getClass(o.current / o.max * 100);

		el.find('.current').text(Math.floor(o.current)).attr('class', className);
		el.find('.max').text(o.max);
	}

	/**
	 * ��������������� �� "������� �������" "������� �������"
	 *
	 * @param {Object} el
	 */
	function regenerate(el) {

      if (top.frames['main']) {
        var isBattle = top.frames['main'].isBattle;
      }

	  if (isBattle == 0) {

		if (typeof el != 'object') {
			el = this;
		}

		var o = el.data('scale');

		o.current += o.coef;

		if(o.current >= o.max) {
			clearInterval(o.interval);
			o.interval = 0;
		}

		set(el);
	  }
	}

	function getCoef(o) {
		return (o.max / 1000) * (o.speed / 100) / 1800 * interval;
	}

	$.fn.HpPwDigits = function(options) {

		return this.each(function() {
			var el = $(this);

			if(options.hp.max > 999 || options.pw.max > 999) {
				el.addClass('hp-pw-digits-small');
			} else {
				el.removeClass('hp-pw-digits-small');
			}

			if (el.data('HpPw:options')) { // ���������� ������� �����
				var o = $(this).data('HpPw:options');

				for(var scale in options) {
					var scaleEl = $(this).find(o[scale].selector);
					var scaleOptions = scaleEl.data('scale');

					// ������������� �����������
					clearInterval(scaleOptions.interval);
					scaleOptions.interval = 0;

					// ���������� ��������
					$.extend(scaleOptions, options[scale]);
					// ���������� �������� �������������� �����
					scaleOptions.coef = getCoef(scaleOptions);

					set(scaleEl);

					// ��������� ����������� � ������ �������������
					if(scaleOptions.regenerate && scaleOptions.current < scaleOptions.max) {
						scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
					}
				}

			} else { // �������������
				var o = $.extend(true, {}, $.fn.HpPwDigits.defaults, options);
				el.data('HpPw:options', o);

				for (var scale in o) {
					var scaleEl = el.find(o[scale].selector);
					var scaleOptions = o[scale];

					scaleOptions.coef = getCoef(scaleOptions);

					scaleEl.data('scale', scaleOptions);

					set(scaleEl);

					if (scaleOptions.regenerate && scaleOptions.speed && scaleOptions.current < o[scale].max) {
						scaleEl.data('scale').interval = setInterval(jQuery.proxy(regenerate, scaleEl), interval);
					}
				}
			}
		});
	}

	$.fn.HpPwDigits.defaults = {
		hp: {
			current: 1,
			image: 'hp',
			invisible: false,
			max: 1,
			regenerate: true,
			selector: '.hp',
			speed: 0
		},
		pw: {
			current: 1,
			image: 'pw',
			invisible: false,
			max: 1,
			regenerate: true,
			selector: '.pw',
			speed: 0
		}
	}
})(jQuery);

function itemSearchSubmit(form) {
	if(form.search.value.length < 3) {
		alert('������� �������� ������. ������� ����� ���� ����.');
		return false;
	} else {
		return true;
	}
}

function itemSearchNoNumbers(event) {
	numcheck = /\d/;
	var keyCode = event.keyCode ? event.keyCode : event.charCode;
	return !numcheck.test(String.fromCharCode(keyCode));
}

function invisibilityOff(nd) {
	if (confirm('����� �����������?')) {
		$.post('/inventory.php', {
			cmd: 'invisibility.off',
			is_ajax: 1,
			nd: nd
		}, function(json) {
			var notify = $.pnotify({
				pnotify_title: '��������!',
				pnotify_type: json.error ? 'error' : 'notice',
				pnotify_text: json.error ? json.error : json.message
			});
			notify.appendTo('#notifies').click(notify.pnotify_remove);
		}, 'json');
	}
}

/**
 * Nickname
 */
function getAlignName(alignId) {
	alignId = Number(alignId);

	switch(alignId) {
		case 0.98:
			return '���� ����';

		case 0.99:
			return '���� �����';

		case 0.97:
			return '���� �����';

		case 0.96:
			return '���� �������';

		case 0.198:
			return '������ ����';

		case 0.199:
			return '������ �����';

		case 0.197:
			return '������ �����';

		case 0.196:
			return '������ �������';

		case 2.2:
			return '�����';

		default:
			return '';
	}
}

/**
 * ���������� ������ �� ���������� � ���������� � ���������� ��������
 *
 * @param {Integer} id
 * @return {String}
 */
function getAlignLink(id){
	id = Number(id);

	if (id) {
		var link = libURL + 'aligns/';

		switch(id) {
			case 0.98:
				link += 'dark/';
				break;

			case 0.99:
				link += 'light/';
				break;

			case 0.97:
				link += 'haos/';
				break;

			case 0.96:
				link += 'order/';
				break;

			case 0.198:
			case 0.199:
			case 0.197:
			case 0.196:
				link += 'consuls/';
				break;

			default:
				break;
		}

		return link;
	} else {
		return '';
	}
}


/**
 * ���������� ���������� ����������
 *
 * @param {Integer} ax
 * @param {Integer} ay
 * @param {Integer} bx
 * @param {Integer} by
 * @return {Array}
 */
function getDungeonLocation(ax, ay, bx, by) {
	if (!bx) {
		bx = iDungeonX;
	}

	if (!by) {
		by = iDungeonY;
	}

	if (!bx || !by || !ax || !ay) {
		return false;
	}

	if (bx == ax && by == ay) {
		return false;
	}

	ay *= -1;
	by *= -1;

	var deltaX = ax - bx;
	var deltaY = ay - by;
	var steps = Math.ceil(Math.sqrt(deltaX * deltaX + deltaY * deltaY));

	var postfix = '��';
	if (steps.toString().match(/[234]$/)) {
		postfix = '�';
	}
	if (steps.toString().match(/1\d$/)) {
		postfix = '��';
	}
	if (steps.toString().match(/1$/)) {
		postfix = '';
	}

	var titles = {
		'e': '������',
		'w': "�����",
		's': '��',
		'n': '�����',
		'en': '������-������',
		"es": '���-������',
		"wn": '������-�����',
		"ws": '���-�����'
	};

	var quarter = 0;
	var degree = Math.atan(Math.abs(deltaY) / Math.abs(deltaX)) * 180 / Math.PI;
	if (deltaX < 0 && deltaY >= 0) {
		degree = 180 - degree;
	} else if (deltaX < 0 && deltaY < 0) {
		degree = 180 + degree;
	} else if (deltaX >= 0 && deltaY < 0) {
		degree = 360 - degree;
	}

	var arrow;
	if (degree >= 345 || degree < 15) {
		arrow = 'e';
	} else if (degree >= 15 && degree < 75) {
		arrow = 'en';
	} else if (degree >= 75 && degree < 105) {
		arrow = 'n';
	} else if (degree >= 105 && degree < 165) {
		arrow = 'wn';
	} else if (degree >= 165 && degree < 195) {
		arrow = 'w';
	} else if (degree >= 195 && degree < 255) {
		arrow = 'ws';
	} else if (degree >= 255 && degree < 295) {
		arrow = 's';
	} else {
		arrow = 'es';
	}

	return [arrow, titles[arrow] + ', ' + steps + ' ���' + postfix];
}

var defaultNickOptions = {
	alcohol: 0, // ������� ��������
	align: '', // {code: 29} ����������
	anotherCity: false, // �������� ��������� � ������ ������
	append: '', // ���������� ������������� ����������� � ���� ������
	battle: false, // �� � �����
	blind: false, // �������
	bot: '', // bot {attack: false, count: 1, id: 123, speak: false}
	clan: '', // ���� clan: {img: '', name: ''}
	ownerDocument: document, // ������������� � ��������
	dungeon: '', // ���������� � ���������� dungeon: {hp: 0, maxhp:0, self: false, x: 0, y: 0}
	eventSide: '', // �������
	guild: '', // ������� guild: {img: '', name: ''}
	ignore: false, // �������������
	invisible: false, // ���������
	isMentor: false, // �������� �� ������� �������� �����������
	hobble: false, // ����
	hurt: 0, // ������
	level: '', // �������
	mentor: false, // id ����������
	name: '', // ��� ���������
	nickClass: 'nick', // ����� ����
	offline: false, // �������� � ��������
	privateMess: true, // ���������� ������ ���������� ���������
	returnHtml: false, // ���������� ������� ������ ������
	sex: '', // ���
	silence: false, // ��������
	showEmpty: false // �� ���������� ������ ������������ ������ ����������� ����������, ������ � �������
}

var eventSidesNames = [
	/*
	'"����"',
	'"Green Peace"',
	'"���������"',
	'"����"',
	'"������"'
	*/
	'"������"',
	'"�������"',
	'"�������"'
	/*
	'"��������"',
	'"������"',
	'"�������"'
	*/
];

/**
 * ���������� ��� ���������
 *
 * @param {Object} options	��������� ��. ���� defaultNickOptions
 * @return {Object|String}	Object - �� ���������, String, ����� returnHtml == true
 */
function getNick(options) {
	var $ = jQuery;
	options = $.extend({}, defaultNickOptions, options);

	if(!options.level) {
		options.level = 0;
	}

	var nickClass = options.nickClass;

	if(options.anotherCity) {
		nickClass += ' nick-another-city';
	}

	if(options.offline) {
		nickClass += ' nick-offline';
	}

	if(options.ignore) {
		nickClass += ' nick-ignore';
	}

	var nickname = $(options.ownerDocument.createElement('span')).attr({
		'class': nickClass
	});

	// ������ �������� ��� ��������� ������
	var height = $(options.ownerDocument.createElement('img')).attr({
		alt: '',
		'class': 'empty empty-height',
		src: nullImg
	});

	if (options.invisible) { // ���������
		var invisible = $(options.ownerDocument.createElement('i')).text('���������');

		nickname.append(invisible);
	} else {
		if (options.showEmpty) {
			var empty = $(options.ownerDocument.createElement('img')).attr({
				alt: '',
				'class': 'empty',
				src: nullImg
			});
		}

		// ��������� ���������
		if (options.privateMess && !options.bot) {
			var privateMess = $(options.ownerDocument.createElement('img')).attr({
				alt: '',
				'class': 'private',
				src: imgURL + 'pr' + (options.offline || options.anotherCity ? '_grey' : '') + '.png',
				title: '��������� ���������'
			});
			nickname.append(privateMess);
		} else if(options.showEmpty) {
			nickname.append(empty.clone().addClass('empty-private'));
		}

		// ������������ ��������� � ��� �� ������
		if (!options.anotherCity) {
			// ���������� � ����������
			if(options.dungeon) {
				var location = [];
				if(sUser && sUser != options.name && options.dungeon.x && options.dungeon.y && iDungeonX && iDungeonY && getDungeonLocation) {
					location = getDungeonLocation(options.dungeon.x, options.dungeon.y);
				}
				var coords = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'location',
					src: imgURL + 'dungeon/online/arrow-' + (location[0] ? location[0] : 'dot') + '.gif',
					title: location[1] ? location[1] : ''
				});
				nickname.append(coords);
			}

			if(options.eventSide) {
				var side = $(options.ownerDocument.createElement('img')).attr({
					alt: '�������',
					'class': 'event-side',
					src: imgURL + 'zarnica2016_' + options.eventSide + '.gif',
					title: '������� ' + eventSidesNames[options.eventSide - 1]
				});
				nickname.append(side);
			}

			if (options.topicon == 1) {
				var topicon = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'topicon',
					src: imgURL + 'icons/top.gif',
					title: '���'
				});

				nickname.append(topicon);
			}

			// ����������
			if(typeof options.align != 'undefined' && (Number(options.align) || Number(options.align.code))) {
				if(typeof options.align == 'object') {
					options.align = Number(options.align.code);
				} else {
					options.align = Number(options.align);
				}

				var alignLink = $(options.ownerDocument.createElement('a')).attr({
					'class': 'align',
					href: getAlignLink(options.align),
					target: '_blank'
				});

				var align = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'align',
					src: imgURL + 'align_' + options.align + '.gif',
					title: getAlignName(options.align)
				}, options.ownerDocument);

				alignLink.append(align);
				nickname.append(alignLink);
			} else if (options.showEmpty) {
				nickname.append(empty.clone());
			}

			// ����
			if (options.clan && options.clan.name) {
				var clanLink = $(options.ownerDocument.createElement('a')).attr({
					'class': 'clan',
					href: libURL + 'clans/' + options.clan.img + '',
					target: '_blank'
				});
				var clan = $(options.ownerDocument.createElement('img')).attr({
					alt: '����',
					'class': 'clan',
					src: imgURL + 'klan/' + options.clan.img + '.gif',
					title: options.clan.name
				});
				clanLink.append(clan);
				nickname.append(clanLink);

			// �������
			} else if (options.guild && options.guild.name) {
				var guild = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'guild',
					src: imgURL + 'guild/' + options.guild.img + '.gif',
					title: options.guild.name
				});

				nickname.append(guild);
			}

			// ��������� ���������
			if (((typeof top.isMentor != 'undefined' && top.isMentor) || options.isMentor) && !options.mentor && !options.bot && options.level == 0) {
				var study = $(options.ownerDocument.createElement('img')).attr({
					alt: '',
					'class': 'study',
					src: imgURL + 'study.gif',
					title: '��������� ���������'
				});

				nickname.append(study);
			}

			// ���: ����������
			if(options.bot && options.bot.count) {
				var count = $(options.ownerDocument.createElement('span')).attr({
					'class': 'count'
				}).text('(' + options.bot.count + ')');
				nickname.append(count);
			}
		}

		if (nickname.html() != '') { // ������ � �����
			nickname.append(' ');
		}

		// ���
        var ncol = "color: #404040;";
        if(top.frames['chat'] && typeof top.frames['chat']['chat2'].UserNColor != "undefined" && top.frames['chat']['chat2'].UserNColor != ""){
          if (top.frames['chat']['chat2'].sUser == options.name) {
            ncol = "color: " + top.frames['chat']['chat2'].UserNColor + ";";
          }
	    }
        if(typeof UserNColor != "undefined" && UserNColor != ""){
          if (sUser == options.name) {
            ncol = "color: " + UserNColor + ";";
          }
	    }
        if (typeof options.nick_color != "undefined" && options.nick_color != "") {
            ncol = "color: " + options.nick_color + ";";
        }

        if (options.battle) {
			var ncol = "";
		}

		var name = $(options.ownerDocument.createElement('b')).attr({
			'class': 'name',
			'style': '' + ncol + ';'
		}).text(options.name);

		if(options.bot) {
			name.addClass('bot');
			if(options.bot.speak) {
				name.addClass('speak');
			}
		}

		if (options.battle) {
			name.addClass('battle');
		}

		nickname.append(name);

		// �������
		if (!options.anotherCity) {
			var level = $(options.ownerDocument.createElement('span')).attr({
				'class': 'level'
			}).text('[' + options.level + ']');
			nickname.append(' ').append(level);
		}
		nickname.append(' ');

		// ���������� � ������������
		var href = '/inf=';
		if(document.domain.toString().split('.')[0] == 'top')
			href = 'http://damask.blutbad.ru' + href;

		var info = $(options.ownerDocument.createElement('a')).attr({
			'class': 'info',
			href: href + options.name.replace(/\s/g, '+'),
			onclick: 'window.open(\'' + href + urlEncode(options.name) + '\'); return false;',
			target: '_blank',
			title: '���������� � ���������'
		});

		// ���� ������ "?" � ����������� �� ����
		var sex = $(options.ownerDocument.createElement('img')).attr({
			alt: '',
			'class': 'sex',
			src: imgURL + 'inf' + (options.anotherCity ? '' : options.sex) + '.gif'
		});
		info.append(sex);
		nickname.append(info);

		if(options.alcohol || options.blind || options.silence || options.hobble || options.hurt) { // ������ ��� ���. ������
			nickname.append(' ');
		}

		// ��������
		if (options.alcohol > 0) {
			var title = '';

			if(options.alcohol < 16) {
				title = '������ ����' + (options.sex == 'M' ? '' : '�');
			} else if (options.alcohol < 32) {
				title = '����' + (options.sex == 'M' ? '' : '�');
			} else if (options.alcohol < 64) {
				title = '������ ����' + (options.sex == 'M' ? '' : '�');
			} else {
				title = '����' + (options.sex == 'M' ? '' : '�') + ' � �������';
			}

			var alcohol = $(options.ownerDocument.createElement('img')).attr({
				alt: '��������',
				'class': 'alcohol',
				src: imgURL + 'ch_alcohol.gif',
				title: title
			});
			nickname.append(alcohol);
		}

		// ��������
		if (options.silence) {
			var silence = $(options.ownerDocument.createElement('img')).attr({
				alt: '��������',
				'class': 'silence',
				src: imgURL + 'ch_silence.gif',
				title: '�������� �������� ��������'
			});
			nickname.append(silence);
		}

		// �������
		if (options.blind) {
			var silence = $(options.ownerDocument.createElement('img')).attr({
				alt: '�������',
				'class': 'blind',
				src: imgURL + 'ch_blind.gif',
				title: '�������� �������� �������'
			});
			nickname.append(silence);
		}

		// ����
		if (options.hobble) {
			var hobble = $(options.ownerDocument.createElement('img')).attr({
				alt: '����',
				'class': 'hobble',
				src: imgURL + 'ch_hobble.gif',
				title: '�������� �������� ���'
			});
			nickname.append(hobble);
		}

		// ������
		if (options.hurt) {
			var hurt = $(options.ownerDocument.createElement('img')).attr({
				alt: '������',
				'class': 'hurt',
				src: imgURL + 'ch_travma.gif',
				title: '�����������' + (options.sex == 'M' ? '' : '�')
			});
			nickname.append(hurt);
		}

		// ���������
		if (options.zimmune) {
			var hurt = $(options.ownerDocument.createElement('img')).attr({
				alt: '��������� � ��������� � ���� "�������"',
				'class': 'zimmune',
				src: imgURL + 'ch_zarnica_immun.gif',
				title: '��������� � ��������� � ���� "�������"'
			});
			nickname.append(hurt);
		}

		// �������� ��������� � ����������
		if(options.dungeon) {
			var hp = $(options.ownerDocument.createElement('span')).attr({
				'class': 'hp'
			}).text('[' + options.dungeon.hp + '/' + options.dungeon.maxhp + ']');
			nickname.append(' ').append(hp);
		}

		//
		if(options.append) {
			nickname.append(options.append);
		}

		if (options.showEmpty) { // ������� ������� �������� �������
			empty.remove();
		}
	}

	if (options.returnHtml) {
        return '<span class="' + nickClass + '">' + nickname.html() + '</span>';
	} else {
		return nickname;
	}
}
/* end: Nickname */


/**
 * ��������� �� ��������� ��� ���������
 */


var tooltipDefaults = {
	bodyHandler: function() {
		return smiley2img(this.tooltipText);
	},
	showURL: false,
	track: true
}




function str_replace(search, replace, subject) {
	return subject.replace(new RegExp (search, 'g'), replace);
}

// ���� ������� � ������ ���������
var PRIVATE_NICK = 'private_nick';
var TO_NICK      = 'to_nick';


function addToList ( userRealName, currentText, insertType ) {
	var regPrivate = new RegExp( /^([\/\.\!\,])(p|private|w|whisper|�|�������|�������)\s+([^:]*?)\s*:\s*(.+)$/ );
	var regTo = new RegExp( /^([\/\.\!\,])(to)\s+([^:]*?)\s*:\s*(.+)$/ );
	var reg1 = regPrivate;
	var reg2 = regTo;
	var prefix = 'private ';

	if ( insertType == TO_NICK ) {
		reg1 = regTo;
		reg2 = regPrivate;
		prefix = 'to ';

	}

	var test1 = reg1.test( currentText );
	var test2 = reg2.test( currentText );
	var currentListStr = '';
	var changePrefix = false;

	if ( test1 || test2 ) {
		currentText = RegExp.$4;
		currentListStr = RegExp.$3;

		if ( test1 ) {
			prefix = RegExp.$1 + RegExp.$2 + ' ';
			changePrefix = true;

		} else {
			prefix = RegExp.$1 + prefix;
		}

		currentText = currentText.replace( /^\s+/, '' );
		currentText = currentText.replace( /\s+$/, '' );
		currentListStr = currentListStr.replace( /^\s+/, '' );
		currentListStr = currentListStr.replace( /\s+$/, '' );

	} else {
		prefix = '/' + prefix;
	}

	userRealNames = userRealName.split( ',' );

	for ( var i = userRealNames.length - 1; i >= 0; i-- ) {
		userRealNames[ i ] = userRealNames[ i ].replace( /^\s+/, '' );
		userRealNames[ i ] = userRealNames[ i ].replace( /\s+$/, '' );

	}

	var currentList = new Array();

	if ( currentListStr != '' ) {
		currentList = currentListStr.split( ',' );
	}

	for ( var j = 0; j < userRealNames.length; j++ ) {
		var exists = false;

		for ( var i = currentList.length - 1; i >= 0; i-- ) {
			currentList[ i ] = currentList[ i ].replace( /^\s+/, '' );
			currentList[ i ] = currentList[ i ].replace( /\s+$/, '' );

			if ( currentList[ i ] == '' ) {
				currentList.splice( i, 1 );
			} else {
				if ( currentList[ i ].toLowerCase() == userRealNames[ j ].toLowerCase() ) {
					exists = true;
				}

			}

		}

		if ( ! exists ) {
			currentList.push( userRealNames[ j ] );
		} else {
			if ( insertType == TO_NICK && changePrefix ) {
				prefix = '/private ';
			}

		}

	}

	return prefix + currentList.join( ', ' ) + ': ' + currentText;

}

function addToListEx (currentText) {
	var regPrivate = new RegExp( /^([\/\.\!\,])(p|private|w|whisper|�|�������|�������)\s+([^:]*?)\s*:\s*(.+)$/ );
	var regTo = new RegExp( /^([\/\.\!\,])(to)\s+([^:]*?)\s*:\s*(.+)$/ );
	var reg1 = regPrivate;
	var reg2 = regTo;
	var prefix = 'private ';
	var prefixEx = 'private ';

	/*if ( insertType == TO_NICK ) {

	}*/
		reg1 = regTo;
		reg2 = regPrivate;
		prefix = 'to ';

	var test1 = reg1.test( currentText );
	var test2 = reg2.test( currentText );
	var currentListStr = '';
	var changePrefix = false;

	if ( test1 || test2 ) {
		currentText = RegExp.$4;
		currentListStr = RegExp.$3;

		if ( test1 ) {
			prefix = RegExp.$1 + RegExp.$2 + ' ';
			prefixEx =  RegExp.$2 + ' ';
			changePrefix = true;

		} else {
			prefix = RegExp.$1 + prefix;
			prefixEx =  prefixEx;
		}

		currentText = currentText.replace( /^\s+/, '' );
		currentText = currentText.replace( /\s+$/, '' );
		currentListStr = currentListStr.replace( /^\s+/, '' );
		currentListStr = currentListStr.replace( /\s+$/, '' );

	} else {
		prefix = '/' + prefix;
		prefixEx = '' + prefixEx;
	}

	userRealNames = currentText.split( ',' );

	for ( var i = userRealNames.length - 1; i >= 0; i-- ) {
		userRealNames[ i ] = userRealNames[ i ].replace( /^\s+/, '' );
		userRealNames[ i ] = userRealNames[ i ].replace( /\s+$/, '' );

	}
	var currentList = new Array();
	if ( currentListStr != '' ) {
		currentList = currentListStr.split( ',' );
	}

	for ( var j = 0; j < userRealNames.length; j++ ) {
		var exists = false;

		for ( var i = currentList.length - 1; i >= 0; i-- ) {
			currentList[ i ] = currentList[ i ].replace( /^\s+/, '' );
			currentList[ i ] = currentList[ i ].replace( /\s+$/, '' );

			if ( currentList[ i ] == '' ) {
				currentList.splice( i, 1 );
			} else {
				if ( currentList[ i ].toLowerCase() == userRealNames[ j ].toLowerCase() ) {
					exists = true;
				}
			}
		}

		if ( ! exists ) {
			currentList.push(userRealNames[ j ] );
		} else {
			/*if ( insertType == TO_NICK && changePrefix ) {
				prefix = '/private ';
			}*/
		}
	}

	    for ( var i = currentList.length - 2; i >= 0; i-- ) {
			currentList[ i ] = prefixEx+'['+currentList[ i ]+']';
		}

  	return currentList.join( ' ' );
}


/*
 * Smilies
 */
var smilies = {
	'jhi': {
		codes: ['jhi', 'jhi'],
		group: 1,
		height: 26,
		width: 36
	},
	'hi': {
		codes: ['hi', 'hi'],
		group: 1,
		height: 25,
		width: 29
	},
	'diler': {
		codes: ['diler', 'diler'],
		group: 1,
		height: 30,
		width: 30
	},
	'badevil': {
		codes: ['badevil', 'badevil'],
		group: 1,
		height: 26,
		width: 30
	},
	'ok': {
		codes: ['ok', 'ok'],
		group: 1,
		height: 26,
		width: 30
	},
	'eusa': {
		codes: ['eusa', 'eusa'],
		group: 1,
		height: 25,
		width: 30
	},
	'beggar': {
		codes: ['beggar', 'beggar'],
		group: 1,
		height: 26,
		width: 35
	},
	'upset': {
		codes: ['upset', 'upset'],
		group: 1,
		height: 23,
		width: 20
	},
	'notknow': {
		codes: ['notknow', 'notknow'],
		group: 1,
		height: 23,
		width: 38
	},
	'kz': {
		codes: ['kz', 'kz'],
		group: 1,
		height: 25,
		width: 20
	},
	'tooth': {
		codes: ['tooth', 'tooth'],
		group: 1,
		height: 27,
		width: 38
	},
	'neigh': {
		codes: ['neigh', 'neigh'],
		group: 1,
		height: 20,
		width: 27
	},
	'roll': {
		codes: ['roll', 'roll'],
		group: 1,
		height: 25,
		width: 30
	},
	'sleep': {
		codes: ['sleep', 'sleep'],
		group: 1,
		height: 24,
		width: 30
	},
	'grimace': {
		codes: ['grimace', 'grimace'],
		group: 1,
		height: 22,
		width: 18
	},
	'kuku': {
		codes: ['kuku', 'kuku'],
		group: 1,
		height: 26,
		width: 30
	},
	'help': {
		codes: ['help', 'help'],
		group: 1,
		height: 22,
		width: 26
	},
	'smash': {
		codes: ['smash', 'smash'],
		group: 1,
		height: 29,
		width: 34
	},
	'pray2': {
		codes: ['pray2', 'pray2'],
		group: 1,
		height: 30,
		width: 32
	},
	'nunu': {
		codes: ['nunu', 'nunu'],
		group: 1,
		height: 26,
		width: 25
	},
	'badtease': {
		codes: ['badtease', 'badtease'],
		group: 1,
		height: 25,
		width: 30
	},
	'plak': {
		codes: ['plak', 'plak'],
		group: 1,
		height: 23,
		width: 26
	},
	'pank': {
		codes: ['pank', 'pank'],
		group: 1,
		height: 31,
		width: 28
	},
	'rupor': {
		codes: ['rupor', 'rupor'],
		group: 1,
		height: 20,
		width: 37
	},
	'kruger': {
		codes: ['kruger', 'kruger'],
		group: 1,
		height: 27,
		width: 40
	},
	'above': {
		codes: ['above', 'above'],
		group: 1,
		height: 26,
		width: 30
	},
	'acute': {
		codes: ['acute', 'acute'],
		group: 1,
		height: 23,
		width: 24
	},
	'dont': {
		codes: ['dont', 'dont'],
		group: 1,
		height: 30,
		width: 32
	},
	'kap': {
		codes: ['kap', 'kap'],
		group: 1,
		height: 36,
		width: 53
	},
	'poll': {
		codes: ['poll', 'poll'],
		group: 1,
		height: 25,
		width: 38
	},
	'protest': {
		codes: ['protest', 'protest'],
		group: 1,
		height: 25,
		width: 33
	},
	'mib1': {
		codes: ['mib1', 'mib1'],
		group: 1,
		height: 23,
		width: 20
	},
	'ponder': {
		codes: ['ponder', 'ponder'],
		group: 1,
		height: 26,
		width: 27
	},
	'haha': {
		codes: ['haha', 'haha'],
		group: 1,
		height: 25,
		width: 29
	},
	'gy': {
		codes: ['gy', 'gy'],
		group: 1,
		height: 25,
		width: 34
	},
	'evil': {
		codes: ['evil', 'evil'],
		group: 1,
		height: 23,
		width: 20
	},
	'alc': {
		codes: ['alc', 'alc'],
		group: 1,
		height: 43,
		width: 51
	},
	'ura': {
		codes: ['ura', 'ura'],
		group: 1,
		height: 40,
		width: 40
	},
	'alcoholic': {
		codes: ['alcoholic', 'alcoholic'],
		group: 1,
		height: 32,
		width: 45
	},
	'amaze': {
		codes: ['amaze', 'amaze'],
		group: 1,
		height: 30,
		width: 40
	},
	'angel': {
		codes: ['angel', 'angel'],
		group: 1,
		height: 26,
		width: 29
	},
	'applause': {
		codes: ['applause', 'applause'],
		group: 1,
		height: 28,
		width: 32
	},
	'baby': {
		codes: ['baby', 'baby'],
		group: 1,
		height: 26,
		width: 15
	},
	'banghead': {
		codes: ['banghead', 'banghead'],
		group: 1,
		height: 23,
		width: 28
	},
	'basketball': {
		codes: ['basketball', 'basketball'],
		group: 1,
		height: 37,
		width: 36
	},
	'blabla': {
		codes: ['blabla', 'blabla'],
		group: 1,
		height: 24,
		width: 36
	},
	'blind': {
		codes: ['blind', 'blind'],
		group: 1,
		height: 27,
		width: 30
	},
	'blush': {
		codes: ['blush', 'blush'],
		group: 1,
		height: 24,
		width: 37
	},
	'box': {
		codes: ['box', 'box'],
		group: 1,
		height: 27,
		width: 44
	},
	'brainy': {
		codes: ['brainy', 'brainy'],
		group: 1,
		height: 25,
		width: 29
	},
	'bye2': {
		codes: ['bye2', 'bye2'],
		group: 1,
		height: 23,
		width: 39
	},
	'bye': {
		codes: ['bye', 'bye'],
		group: 1,
		height: 25,
		width: 30
	},
	'camomile': {
		codes: ['camomile', 'camomile'],
		group: 1,
		height: 29,
		width: 31
	},
	'campfire': {
		codes: ['campfire', 'campfire'],
		group: 1,
		height: 27,
		width: 40
	},
	/*'close': {
		codes: ['close', 'close'],
		group: 1,
		height: 39,
		width: 46
	},*/
	'coffee': {
		codes: ['coffee', 'coffee'],
		group: 1,
		height: 26,
		width: 42
	},
	'comp': {
		codes: ['comp', 'comp'],
		group: 1,
		height: 24,
		width: 50
	},
	'confeta': {
		codes: ['confeta', 'confeta'],
		group: 1,
		height: 27,
		width: 30
	},
	'crazy': {
		codes: ['crazy', 'crazy'],
		group: 1,
		height: 24,
		width: 38
	},
	'cry2': {
		codes: ['cry2', 'cry2'],
		group: 1,
		height: 24,
		width: 40
	},
	'cry': {
		codes: ['cry', 'cry'],
		group: 1,
		height: 26,
		width: 34
	},
	'cymbals': {
		codes: ['cymbals', 'cymbals'],
		group: 1,
		height: 30,
		width: 31
	},
	'dance3': {
		codes: ['dance3', 'dance3'],
		group: 1,
		height: 25,
		width: 39
	},
	'dance': {
		codes: ['dance', 'dance'],
		group: 1,
		height: 26,
		width: 31
	},
	'declare': {
		codes: ['declare', 'declare'],
		group: 1,
		height: 27,
		width: 40
	},
	'devil': {
		codes: ['devil', 'devil'],
		group: 1,
		height: 37,
		width: 41
	},
	'dirge': {
		codes: ['dirge', 'dirge'],
		group: 1,
		height: 37,
		width: 41
	},
	'doctor': {
		codes: ['doctor', 'doctor'],
		group: 1,
		height: 25,
		width: 43
	},
	'dwarf': {
		codes: ['dwarf', 'dwarf'],
		group: 1,
		height: 46,
		width: 41
	},
	'eat': {
		codes: ['eat', 'eat'],
		group: 1,
		height: 31,
		width: 44
	},
	'encore': {
		codes: ['encore', 'encore'],
		group: 1,
		height: 34,
		width: 40
	},
	'figa': {
		codes: ['figa', 'figa'],
		group: 1,
		height: 27,
		width: 36
	},
	'fingal': {
		codes: ['fingal', 'fingal'],
		group: 1,
		height: 23,
		width: 23
	},
	'fu': {
		codes: ['fu', 'fu'],
		group: 1,
		height: 25,
		width: 18
	},
	'furious2': {
		codes: ['furious2', 'furious2'],
		group: 1,
		height: 24,
		width: 28
	},
	'furious': {
		codes: ['furious', 'furious'],
		group: 1,
		height: 18,
		width: 18
	},
	'gitler': {
		codes: ['gitler', 'gitler'],
		group: 1,
		height: 23,
		width: 23
	},
	'good2': {
		codes: ['good2', 'good2'],
		group: 1,
		height: 23,
		width: 36
	},
	'good': {
		codes: ['good', 'good'],
		group: 1,
		height: 25,
		width: 36
	},
	'hang': {
		codes: ['hang', 'hang'],
		group: 1,
		height: 42,
		width: 32
	},
	'hb': {
		codes: ['hb', 'hb'],
		group: 1,
		height: 37,
		width: 52
	},
	'horse': {
		codes: ['horse', 'horse'],
		group: 1,
		height: 47,
		width: 74
	},
	'itchy': {
		codes: ['itchy', 'itchy'],
		group: 1,
		height: 25,
		width: 35
	},
	'king': {
		codes: ['king', 'king'],
		group: 1,
		height: 28,
		width: 28
	},
	'kloun': {
		codes: ['kloun', 'kloun'],
		group: 1,
		height: 30,
		width: 43
	},
	'laugh': {
		codes: ['laugh', 'laugh'],
		group: 1,
		height: 24,
		width: 22
	},
	'love2': {
		codes: ['love2', 'love2'],
		group: 1,
		height: 26,
		width: 35
	},
	'mishka': {
		codes: ['mishka', 'mishka'],
		group: 1,
		height: 25,
		width: 44
	},
	/*'nafik': {
		codes: ['nafik', 'nafik'],
		group: 1,
		height: 31,
		width: 38
	},*/
	'newy': {
		codes: ['newy', 'newy'],
		group: 1,
		height: 30,
		width: 47
	},
	'no': {
		codes: ['no', 'no'],
		group: 1,
		height: 23,
		width: 18
	},
	'pirate': {
		codes: ['pirate', 'pirate'],
		group: 1,
		height: 35,
		width: 45
	},
	'prayer': {
		codes: ['prayer', 'prayer'],
		group: 1,
		height: 26,
		width: 31
	},
	'preved': {
		codes: ['preved', 'preved'],
		group: 1,
		height: 33,
		width: 50
	},
	'regulation': {
		codes: ['regulation', 'regulation'],
		group: 1,
		height: 29,
		width: 29
	},
	'rendezvous': {
		codes: ['rendezvous', 'rendezvous'],
		group: 1,
		height: 34,
		width: 49
	},
	'roar': {
		codes: ['roar', 'roar'],
		group: 1,
		height: 23,
		width: 31
	},
	'roast': {
		codes: ['roast', 'roast'],
		group: 1,
		height: 25,
		width: 52
	},
	'angel2': {
		codes: ['angel2', 'angel2'],
		group: 1,
		height: 27,
		width: 37
	},
	'sad': {
		codes: ['sad', 'sad'],
		group: 1,
		height: 24,
		width: 22
	},
	'scare': {
		codes: ['scare', 'scare'],
		group: 1,
		height: 31,
		width: 40
	},
	'scream': {
		codes: ['scream', 'scream'],
		group: 1,
		height: 22,
		width: 36
	},
	'secret': {
		codes: ['secret', 'secret'],
		group: 1,
		height: 27,
		width: 22
	},
	'serenity': {
		codes: ['serenity', 'serenity'],
		group: 1,
		height: 27,
		width: 38
	},
	'sick': {
		codes: ['sick', 'sick'],
		group: 1,
		height: 25,
		width: 33
	},
	'smile': {
		codes: ['smile', 'smile'],
		group: 1,
		height: 25,
		width: 22
	},
	'smoke': {
		codes: ['smoke', 'smoke'],
		group: 1,
		height: 31,
		width: 40
	},
	'smoked': {
		codes: ['smoked', 'smoked'],
		group: 1,
		height: 27,
		width: 31
	},
	'sos': {
		codes: ['sos', 'sos'],
		group: 1,
		height: 34,
		width: 32
	},
	/*'spartak': {
		codes: ['spartak', 'spartak'],
		group: 1,
		height: 36,
		width: 61
	},*/
	'stinker': {
		codes: ['stinker', 'stinker'],
		group: 1,
		height: 26,
		width: 31
	},
	'stop': {
		codes: ['stop', 'stop'],
		group: 1,
		height: 25,
		width: 38
	},
	'stoped': {
		codes: ['stoped', 'stoped'],
		group: 1,
		height: 24,
		width: 38
	},
	'superstition': {
		codes: ['superstition', 'superstition'],
		group: 1,
		height: 26,
		width: 36
	},
	'tongue2': {
		codes: ['tongue2', 'tongue2'],
		group: 1,
		height: 23,
		width: 36
	},
	'tongue': {
		codes: ['tongue', 'tongue'],
		group: 1,
		height: 24,
		width: 26
	},
	'vdv': {
		codes: ['vdv', 'vdv'],
		group: 1,
		height: 29,
		width: 43
	},
	'warrior': {
		codes: ['warrior', 'warrior'],
		group: 1,
		height: 30,
		width: 45
	},
	'whistle': {
		codes: ['whistle', 'whistle'],
		group: 1,
		height: 27,
		width: 28
	},
	'who': {
		codes: ['who', 'who'],
		group: 1,
		height: 30,
		width: 33
	},
	'writer': {
		codes: ['writer', 'writer'],
		group: 1,
		height: 37,
		width: 38
	},
	'yahoo': {
		codes: ['yahoo', 'yahoo'],
		group: 1,
		height: 28,
		width: 44
	},
	'yawn': {
		codes: ['yawn', 'yawn'],
		group: 1,
		height: 25,
		width: 22
	},
	'polet': {
		codes: ['polet', 'polet'],
		group: 1,
		height: 25,
		width: 41
	},
	'tongue3': {
		codes: ['tongue3', 'tongue3'],
		group: 1,
		height: 24,
		width: 18
	},
	'gazeta': {
		codes: ['gazeta', 'gazeta'],
		group: 1,
		height: 31,
		width: 25
	},
	'victory': {
		codes: ['victory', 'victory'],
		group: 1,
		height: 25,
		width: 24
	},
	'air_kiss': {
		codes: ['air_kiss', 'air_kiss'],
		group: 1,
		height: 27,
		width: 26
	},
	'gaz': {
		codes: ['gaz', 'gaz'],
		group: 1,
		height: 25,
		width: 36
	},
	'igry': {
		codes: ['igry', 'igry'],
		group: 1,
		height: 34,
		width: 32
	},
	'grust': {
		codes: ['grust', 'grust'],
		group: 1,
		height: 23,
		width: 18
	},
	'flover': {
		codes: ['flover', 'flover'],
		group: 1,
		height: 32,
		width: 52
	},
	'juggler': {
		codes: ['juggler', 'juggler'],
		group: 1,
		height: 33,
		width: 38
	},
	'ass': {
		codes: ['ass', 'ass'],
		group: 1,
		height: 25,
		width: 36
	},
	'hiya': {
		codes: ['hiya', 'hiya'],
		group: 1,
		height: 30,
		width: 28
	},
	'agree': {
		codes: ['agree', 'agree'],
		group: 2,
		height: 22,
		width: 44
	},
	'friday': {
		codes: ['friday', 'friday'],
		group: 2,
		height: 26,
		width: 54
	},
	'friends': {
		codes: ['friends', 'friends'],
		group: 2,
		height: 27,
		width: 50
	},
	'punish': {
		codes: ['punish', 'punish'],
		group: 2,
		height: 31,
		width: 50
	},
	'kissmy': {
		codes: ['kissmy', 'kissmy'],
		group: 2,
		height: 26,
		width: 63
	},
	'duel': {
		codes: ['duel', 'duel'],
		group: 2,
		height: 32,
		width: 101
	},
	'beer': {
		codes: ['beer', 'beer'],
		group: 2,
		height: 27,
		width: 50
	},
	'beer2': {
		codes: ['beer2', 'beer2'],
		group: 2,
		height: 26,
		width: 60
	},
	'kiss2': {
		codes: ['kiss2', 'kiss2'],
		group: 2,
		height: 29,
		width: 61
	},
	'kiss': {
		codes: ['kiss', 'kiss'],
		group: 2,
		height: 34,
		width: 66
	},
	'lariat': {
		codes: ['lariat', 'lariat'],
		group: 2,
		height: 29,
		width: 78
	},
	'love': {
		codes: ['love', 'love'],
		group: 2,
		height: 33,
		width: 45
	},
	'affection': {
		codes: ['affection', 'affection'],
		group: 2,
		height: 26,
		width: 52
	},
	'mother': {
		codes: ['mother', 'mother'],
		group: 2,
		height: 24,
		width: 41
	},
	'noshbask': {
		codes: ['noshbask', 'noshbask'],
		group: 2,
		height: 28,
		width: 69
	},
	'kosa': {
		codes: ['kosa', 'kosa'],
		group: 2,
		height: 23,
		width: 48
	},
	'tango': {
		codes: ['tango', 'tango'],
		group: 2,
		height: 28,
		width: 68
	},
	'serenada': {
		codes: ['serenada', 'serenada'],
		group: 2,
		height: 55,
		width: 33
	},
	'buket': {
		codes: ['buket', 'buket'],
		group: 2,
		height: 30,
		width: 50
	},
	'stp': {
		codes: ['stp', 'stp'],
		group: 2,
		height: 24,
		width: 43
	},
	'moder': {
		codes: ['moder', 'moder'],
		group: 1,
		height: 25,
		width: 85
	},
	'newyear': {
		codes: ['newyear', 'newyear'],
		group: 2,
		height: 48,
		width: 48,
	},
	'maniak': {
		codes: ['maniak', 'maniak'],
		group: 2,
		height: 37,
		width: 100
	}
}


smileyCodes = {
	'above': 'above',
	'above': 'above',

	'acute': 'acute',
	'acute': 'acute',

	'affection': 'affection',
	'affection': 'affection',

	'alc': 'alc',
	'alc': 'alc',

	'ura': 'ura',
	'ura': 'ura',

	'dont': 'dont',
	'dont': 'dont',

	'kap': 'kap',
	'kap': 'kap',

	'poll': 'poll',
	'poll': 'poll',

	'protest': 'protest',
	'protest': 'protest',

	'mib1': 'mib1',
	'mib1': 'mib1',

	'ponder': 'ponder',
	'ponder': 'ponder',

	'haha': 'haha',
	'haha': 'haha',

	'gy': 'gy',
	'gy': 'gy',

	'evil': 'evil',
	'evil': 'evil',

	'alcoholic': 'alcoholic',
	'alcoholic': 'alcoholic',

	'amaze': 'amaze',
	'amaze': 'amaze',

	'angel': 'angel',
	'angel': 'angel',

	'baby': 'baby',
	'baby': 'baby',

	'applause': 'applause',
	'applause': 'applause',

	'banghead': 'banghead',
	'banghead': 'banghead',

	'basketball': 'basketball',
	'basketball': 'basketball',

	'beer': 'beer',
	'beer': 'beer',

	'beer2': 'beer2',
	'beer2': 'beer2',

	'blabla': 'blabla',
	'blabla': 'blabla',

	'blind': 'blind',
	'blind': 'blind',

	'blush': 'blush',
	'blush': 'blush',

	'box': 'box',
	'box': 'box',

	'brainy': 'brainy',
	'brainy': 'brainy',

	'bye2': 'bye2',
	'bye2': 'bye2',

	'bye': 'bye',
	'bye': 'bye',

	'camomile': 'camomile',
	'camomile': 'camomile',

	'campfire': 'campfire',
	'campfire': 'campfire',

	/*'close': 'close',
	'close': 'close',*/

	'coffee': 'coffee',
	'coffee': 'coffee',

	'comp': 'comp',
	'comp': 'comp',

	'confeta': 'confeta',
	'confeta': 'confeta',

	'crazy': 'crazy',
	'crazy': 'crazy',

	'cry2': 'cry2',
	'cry2': 'cry2',

	'cry': 'cry',
	'cry': 'cry',

	'cymbals': 'cymbals',
	'cymbals': 'cymbals',

	'dance3': 'dance3',
	'dance3': 'dance3',

	'dance': 'dance',
	'dance': 'dance',

	'declare': 'declare',
	'declare': 'declare',

	'devil': 'devil',
	'devil': 'devil',

	'dirge': 'dirge',
	'dirge': 'dirge',

	'doctor': 'doctor',
	'doctor': 'doctor',

	'dwarf': 'dwarf',
	'dwarf': 'dwarf',

	'eat': 'eat',
	'eat': 'eat',

	'encore': 'encore',
	'encore': 'encore',

	'figa': 'figa',
	'figa': 'figa',

	'fingal': 'fingal',
	'fingal': 'fingal',

	'friday': 'friday',
	'friday': 'friday',

	'friends': 'friends',
	'friends': 'friends',

	'fu': 'fu',
	'fu': 'fu',

	'furious2': 'furious2',
	'furious2': 'furious2',

	'furious': 'furious',
	'furious': 'furious',

	'gitler': 'gitler',
	'gitler': 'gitler',

	'good2': 'good2',
	'good2': 'good2',

	'good': 'good',
	'good': 'good',

	'grimace': 'grimace',
	'grimace': 'grimace',

	'hang': 'hang',
	'hang': 'hang',

	'hb': 'hb',
	'hb': 'hb',

	'jhi': 'jhi',
	'jhi': 'jhi',

	'horse': 'horse',
	'horse': 'horse',

	'itchy': 'itchy',
	'itchy': 'itchy',

	'king': 'king',
	'king': 'king',

	'kiss2': 'kiss2',
	'kiss2': 'kiss2',

	'kiss': 'kiss',
	'kiss': 'kiss',

	'kloun': 'kloun',
	'kloun': 'kloun',

	'kz': 'kz',
	'kz': 'kz',

	'lariat': 'lariat',
	'lariat': 'lariat',

	'laugh': 'laugh',
	'laugh': 'laugh',

	'love2': 'love2',
	'love2': 'love2',

	'love': 'love',
	'love': 'love',

	'mishka': 'mishka',
	'mishka': 'mishka',

	'mother': 'mother',
	'mother': 'mother',

	/*'nafik': 'nafik',
	'nafik': 'nafik',*/

	'neigh': 'neigh',
	'neigh': 'neigh',

	'newy': 'newy',
	'newy': 'newy',

	'no': 'no',
	'no': 'no',

	'pirate': 'pirate',
	'pirate': 'pirate',

 	'plak': 'plak',
	'plak': 'plak',

	'pank': 'pank',
	'pank': 'pank',

	'rupor': 'rupor',
	'rupor': 'rupor',

	'kruger': 'kruger',
	'kruger': 'kruger',


	'prayer': 'prayer',
	'prayer': 'prayer',

	'preved': 'preved',
	'preved': 'preved',

	'punish': 'punish',
	'punish': 'punish',

	'regulation': 'regulation',
	'regulation': 'regulation',

	'rendezvous': 'rendezvous',
	'rendezvous': 'rendezvous',

	'roar': 'roar',
	'roar': 'roar',

	'roast': 'roast',
	'roast': 'roast',

	'angel2': 'angel2',
	'angel2': 'angel2',

	'sad': 'sad',
	'sad': 'sad',

	'scare': 'scare',
	'scare': 'scare',

	'scream': 'scream',
	'scream': 'scream',

	'secret': 'secret',
	'secret': 'secret',

	'serenity': 'serenity',
	'serenity': 'serenity',

	'sick': 'sick',
	'sick': 'sick',

	'sleep': 'sleep',
	'sleep': 'sleep',

	'smile': 'smile',
	'smile': 'smile',

	'smoke': 'smoke',
	'smoke': 'smoke',

	'smoked': 'smoked',
	'smoked': 'smoked',

	'sos': 'sos',
	'sos': 'sos',

	/*'spartak': 'spartak',
	'spartak': 'spartak',*/

	'stinker': 'stinker',
	'stinker': 'stinker',

	'stop': 'stop',
	'stop': 'stop',

	'stoped': 'stoped',
	'stoped': 'stoped',

	'superstition': 'superstition',
	'superstition': 'superstition',

	'tongue2': 'tongue2',
	'tongue2': 'tongue2',

	'tongue': 'tongue',
	'tongue': 'tongue',

	'tooth': 'tooth',
	'tooth': 'tooth',

	'vdv': 'vdv',
	'vdv': 'vdv',

	'warrior': 'warrior',
	'warrior': 'warrior',

	'whistle': 'whistle',
	'whistle': 'whistle',

	'who': 'who',
	'who': 'who',

	'writer': 'writer',
	'writer': 'writer',

	'yahoo': 'yahoo',
	'yahoo': 'yahoo',

	'yawn': 'yawn',
	'yawn': 'yawn',

	'polet': 'polet',
	'polet': 'polet',

	'tongue3': 'tongue3',
	'tongue3': 'tongue3',

	'gazeta': 'gazeta',
	'gazeta': 'gazeta',

	'victory': 'victory',
	'victory': 'victory',

	'air_kiss': 'air_kiss',
	'air_kiss': 'air_kiss',

	'gaz': 'gaz',
	'gaz': 'gaz',

	'igry': 'igry',
	'igry': 'igry',

	'grust': 'grust',
	'grust': 'grust',

	'flover': 'flover',
	'flover': 'flover',

	'juggler': 'juggler',
	'juggler': 'juggler',

	'ass': 'ass',
	'ass': 'ass',

	'hiya': 'hiya',
	'hiya': 'hiya',

	'roll': 'roll',
	'roll': 'roll',

	'kissmy': 'kissmy',
	'kissmy': 'kissmy',

	'noshbask': 'noshbask',
	'noshbask': 'noshbask',

	'diler': 'diler',
	'diler': 'diler',

	'badevil': 'badevil',
	'badevil': 'badevil',

	'ok': 'ok',
	'ok': 'ok',

	'eusa': 'eusa',
	'eusa': 'eusa',

	'beggar': 'beggar',
	'beggar': 'beggar',

	'hi': 'hi',
	'hi': 'hi',

	'upset': 'upset',
	'upset': 'upset',

	'notknow': 'notknow',
	'notknow': 'notknow',

	'kuku': 'kuku',
	'kuku': 'kuku',

	'help': 'help',
	'help': 'help',

	'smash': 'smash',
	'smash': 'smash',

	'pray2': 'pray2',
	'pray2': 'pray2',

	'nunu': 'nunu',
	'nunu': 'nunu',

	'badtease': 'badtease',
	'badtease': 'badtease',

	'kosa': 'kosa',
	'kosa': 'kosa',

	'serenada': 'serenada',
	'serenada': 'serenada',

	'buket': 'buket',
	'buket': 'buket',

	'stp': 'stp',
	'stp': 'stp',

	'tango': 'tango',
	'tango': 'tango',

	'moder': 'moder',
	'moder': 'moder',

	'duel': 'duel',
	'duel': 'duel',

	'newyear': 'newyear',
	'newyear': 'newyear',

	'maniak': 'maniak',
	'maniak': 'maniak'

};

var smiliesTarget, smiliesTargetMaxLength;

/**
 * �������������� ������� �� ����������
 *
 * @param {Object} targetWindow
 */
function initSmilies(targetWindow) {
	var $ = jQuery;

	if(!targetWindow) {
		targetWindow = window;
	}

	smiliesTabs = $('#smilies-tabs');


	if(!smiliesTabs.data('tabs-init')) {
		var single = smiliesTabs.find('#single-smilies').empty();
		var group = smiliesTabs.find('#group-smilies').empty();

		for(var name in smilies) {
			var smiley = smilies[name];
			var img = $('<img />', {
				alt: ':' + name + ':',
				click: function() {
					if(!targetWindow.smiliesTarget.disabled) {
						if (targetWindow.smiliesTargetMaxLength && targetWindow.smiliesTarget.value.length + this.alt.length + 1 > targetWindow.smiliesTargetMaxLength) {
							alert("����� ��������� ������������ ����� ��������!");
						} else {
							targetWindow.smiliesTarget.value += this.alt;

							targetWindow.smiliesTarget.focus();
							moveCaretToEnd(targetWindow.smiliesTarget);

							var smiliesDialog = $('#smilies-dialog');

							if (smiliesDialog.length) {
								smiliesDialog.dialog('close');
							} else {
								window.close();
							}
						}
					}
				},
				height: smiley.height + 'px',
				mouseenter:	function() {
					$(this).addClass('hover');
				},
				mouseleave:	function() {
					$(this).removeClass('hover');
				},
				src: imgURL + 'smilies/' + name + '.gif',
				title: ':' + smilies[name].codes.join(':  :') + ':',
				width: smiley.width + 'px'
			});

			if(smilies[name].group == 1) {
				single.append(img);
			} else if(smilies[name].group == 2) {
				group.append(img);
			}
		}

		smiliesTabs.tabs().data('tabs-init', true).show();
	}
}



/**
 * ��������� �� ��������� ��� ������������ ���������
 */
var paginationDefaults = {
	callback: function(event, container) {
		container.prepend('��������:');
	},
	items_per_page: 1,
	next_text: '&rarr;',
	next_show_always: false,
	num_edge_entries: 1,
	prev_show_always: false,
	prev_text: '&larr;'
}

jQuery(function($) {

	try {
		$('div.roll-tabs-js').tabs({
			cookie: {
				expires: 1 / 24
			}
		});
	} catch(e) {
	}

	if($.tooltip) {
		$('.tooltip[title]').tooltip(tooltipDefaults);
		$('.tooltip-next').tooltip($.extend({}, tooltipDefaults, {
			bodyHandler: function(){
				return $(this).next().html();
			}
		}));
	}

	jQuery('.nick img.private').live('click', function() {
		var b_name = jQuery(this).parent().children('.name').text();
		var oWindow = window;

		if ( opener ) {
			oWindow = opener;
		}

		if (oWindow.top.frames['chat'] && oWindow.top.frames['chat']['chat2'].oChat) {
			oWindow.top.frames['chat']['chat2'].oChat.writeToEditorPrivate(b_name);
		}
	});

	jQuery('.nick .name').live('click.nick', function(event) {
		if (!jQuery(this).closest('.ui-dialog').length) {
			if (event.ctrlKey) {
				window.open('/inf=' + jQuery(this).text(), '_blank');
			} else {
				if (top.frames['bottom']) {
					var b_name = jQuery(this).text();

					if (!pasteNick(b_name)) {
						if (top.frames['chat'] && top.frames['chat']['chat2'].oChat) {
						  	top.frames['chat']['chat2'].oChat.writeToEditorPrivate(b_name);
                            top.frames['chat']['chat2'].jQuery('#filt_mess').val(b_name);
						}
					}
				}
			}
		}
	});

	// ��������� ���������� ����
  	addContextMenu();

	var rightCol = jQuery('div.right-col-float').css('top', jQuery(window).scrollTop());
	jQuery(window).scroll(function() {
		rightCol.stop().animate({
			top: jQuery(window).scrollTop()
		}, 100);
	});

    jQuery(window).load(function () {
      jQuery('a').each(function (index) {
        if (this.href.indexOf(".blutbad.ru/forum.php") > 0 && window.location.pathname != '/forum.php' && top.current_incity) {
         this.href = this.href.replace('damask.blutbad.ru/forum.php',  top.current_incity + '.blutbad.ru/forum.php');
         this.href = this.href.replace('alamut.blutbad.ru/forum.php',  top.current_incity + '.blutbad.ru/forum.php');
         this.href = this.href.replace('calydon.blutbad.ru/forum.php', top.current_incity + '.blutbad.ru/forum.php');
         console.info(this.href);
        }
      });
     });

});