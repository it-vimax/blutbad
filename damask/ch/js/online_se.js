var zeroClip = null;
ZeroClipboard.setMoviePath( '/f/ZeroClipboard.swf' );


function processRefresh() {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.refreshOnlineList();
	}

}


function processAuto( oCheckbox ) {
	if ( top.frames[ 'chat' ] && top.frames[ 'chat' ][ 'chat2' ].oChat ) {
		top.frames[ 'chat' ][ 'chat2' ].oChat.setAutorefreshOnlineList( oCheckbox.checked );
	}

}


function setAutorefreshState( state ) {
	var date = new Date();
	date.setDate( date.getDate() + 365 );

	top.setCookie( 'online-hand.' + top.userId, state ? 0 : 1, date.toGMTString(), '/', top.getSubDomains() );

}


function isHorizontalScroll() {
	return jQuery( document ).width() - jQuery( window ).width();
}


function isVerticalScroll() {
	return jQuery( document ).height() - jQuery( window ).height();
}


function assignZeroClip( text ) {
	if ( zeroClip ) {
		zeroClip.destroy();
	}

	zeroClip = new ZeroClipboard.Client();
	zeroClip.glue( 'copy_name' );
	zeroClip.setText( text );
	zeroClip.addEventListener( 'onComplete', function() { top.frames[ 'chat' ][ 'chat2' ].oChat.hidePopup( window ); } );

}
