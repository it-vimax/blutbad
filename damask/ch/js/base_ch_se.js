/*
Object.prototype.Inherits = function( parent ) {
	if ( arguments.length > 1 ) {
		parent.apply( this, Array.prototype.slice.call( arguments, 1 ) );
	} else {
		parent.call( this );
	}

}


Function.prototype.Inherits = function( parent ) {
	this.prototype = new parent();
	this.prototype.constructor = this;
	this.prototype.parent = parent.prototype;

}
*/


// ����� ������������ ��������
Inherited = function( child, parent ) {
	if ( arguments.length > 2 ) {
		parent.apply( child, Array.prototype.slice.call( arguments, 2 ) );
	} else {
		parent.call( child );
	}

}


// ������������
Inheritance = function( func, parent ) {
	func.prototype = new parent();
	func.prototype.constructor = func;
	func.prototype.parent = parent.prototype;

}


/*****************************************************************************
* BaseChat
*/
function BaseChat( chatParams, chatOptions ) {
    if ( typeof( chatParams ) == 'undefined' ) {
		chatParams = {};
	}

	if ( typeof( chatParams.userName ) == 'undefined' ) {
		chatParams.userName = '';
	}

	if ( typeof( chatParams.password ) == 'undefined' ) {
		chatParams.password = '';
	}

	if ( typeof( chatParams.roomId ) == 'undefined' ) {
		chatParams.roomId = '';
	}

	if ( typeof( chatParams.roomName ) == 'undefined' ) {
		chatParams.roomName = '';
	}

	this.chatParams = chatParams;
	this.chatOptions = chatOptions;

	this.userName = this.chatParams.userName;
	this.password = this.chatParams.password;
	this.roomId = this.chatParams.roomId;
	this.roomName = this.chatParams.roomName;

	this.currentChannelId = this.roomId;
	this._connected = false;
	this._reconnecting = false;
	this._channels = {};
	this._channelLastId = 0;
	this._channelNames = {};
	this._userNames = {};
	this._userLastId = 0;

	this._MainDocument = document;
	this._MainWindow = window;

}


BaseChat.prototype.run = function() {
	this._url = this._getUrl();
	this._server = this._getServer();
	this._domain = this._getDomain();

}


BaseChat.prototype.connect = function() {
	if ( ! this.connected() ) {
		try {
			this._bindChatClient();
			this._connect();

		} catch( e ) {
			alert( e );
		}

	}

}


BaseChat.prototype.disconnect = function( type ) {
	if ( this.connected() ) {
		try {
			this._disconnect( type );
		} catch( e ) {
			alert( e );
		}

	}

}


BaseChat.prototype.reconnect = function() {
	this._reconnecting = true;
	this.disconnect();

}


BaseChat.prototype.connected = function() {
	return this._connected;
}


BaseChat.prototype.addChannel = function( id, name, outputName, isCustom ) {
	id = ( '' + id ).toLowerCase();

	if ( this.channelExists( id ) || this.channelExists( name ) ) {
		this.onError( 'Channel "' + name + '" already exists.' );

		return null;

	} else {
		var oChannel = this._createChannel( id, name, outputName, isCustom );

		this._channels[ id ] = oChannel;
		this._channelNames[ name ] = id;

		return oChannel;

	}

}


BaseChat.prototype.removeChannelById = function( id, forced ) {
	id = ( '' + id ).toLowerCase();

	if ( this.channelExists( id ) ) {
		var oChannel = this._channels[ id ];
		this._removeChannel( oChannel, forced );

	} else {
		this.onError( 'Channel "' + id + '" does not exist.' );
	}

}


BaseChat.prototype.removeChannelByName = function( name, forced ) {
	name = '' + name;

	if ( this.channelExists( name ) ) {
		var oChannel = this._channels[ this._channelNames[ name ] ];
		this._removeChannel( oChannel, forced );

	} else {
		this.onError( 'Channel "' + name + '" does not exist.' );
	}

}


BaseChat.prototype.generateChannelId = function() {
	this._channelLastId++;

	return this._channelLastId;

}


BaseChat.prototype.channelExists = function( id ) {
	id = '' + id;

	if ( typeof( this._channels[ id.toLowerCase() ] ) != 'undefined' || typeof( this._channelNames[ id ] ) != 'undefined' ) {
		return true;
	} else {
		return false;
	}

}


BaseChat.prototype.getChannelById = function( id ) {
	id = ( '' + id ).toLowerCase();

	if ( typeof( this._channels[ id ] ) != 'undefined' ) {
		return this._channels[ id ];
	} else {
		return null;
	}

}


BaseChat.prototype.getChannelByName = function( name ) {
	name = '' + name;

	if ( typeof( this._channelNames[ name ] ) != 'undefined' ) {
		return this._channels[ this._channelNames[ name ] ];
	} else {
		return null;
	}

}


BaseChat.prototype.changeRoom = function( roomId ) {
}


BaseChat.prototype.addUser = function( userName ) {
	if ( userName && userName.length > 0 && ! this.userExists( userName ) && ! this.isUserInvisible( userName ) ) {
		var oUser = this._createUser( userName );
		this._userNames[ userName ] = oUser;

		return oUser;

	} else {
		return null;
	}

}


BaseChat.prototype.removeUser = function( userName ) {
	if ( this.userExists( userName ) ) {
		var oUser = this.getUserByName( userName );
		this._removeUser( oUser );
		delete( this._userNames[ userName ] );

	}

}


BaseChat.prototype.cleanUserList = function() {
	for ( var userName in this._userNames ) {
		this.removeUser( userName );
	}

}


BaseChat.prototype.generateUserId = function() {
	this._userLastId++;

	return this._userLastId;

}


BaseChat.prototype.userExists = function( userName ) {
	if ( typeof( this._userNames[ userName ] ) != 'undefined' ) {
		return true;
	} else {
		return false;
	}

}


BaseChat.prototype.getUserByName = function( userName ) {
	if ( typeof( this._userNames[ userName ] ) != 'undefined' ) {
		return this._userNames[ userName ];
	} else {
		return null;
	}

}


BaseChat.prototype.appendText = function( channel, text ) {
	this.getChannelByName( channel ).appendText( text );
}


BaseChat.prototype.appendMessage = function( channel, mId, type, time, from, to, text, special ) {
	this.getChannelByName( channel ).appendMessage( mId, type, time, from, to, text, special );
}


BaseChat.prototype.isUserInvisible = function( userName ) {
}


BaseChat.prototype.formatMessage = function( messageId, type, time, from, to, text, style, special ) {
}


BaseChat.prototype.onConnect = function() {
}


BaseChat.prototype.onDisconnect = function() {
}


BaseChat.prototype.onError = function( error ) {
}


BaseChat.prototype.onAuthenticate = function() {
}


BaseChat.prototype.onMessage = function( message ) {
}


BaseChat.prototype.onPresence = function( presence ) {
}


BaseChat.prototype.onQuery = function( query ) {
}


// ���������� ������
BaseChat.prototype._bindChatClient = function() {
	try {
		if ( ! this._ChatClient ) {
			this._ChatClient = this._getChatClient();
			this._ChatClient.owner = this;

		}

	} catch( e ) {
		alert( e );
	}

}


BaseChat.prototype._attachEventHandlers = function() {
	this._attachOnConnect();
	this._attachOnDisconnect();
	this._attachOnError();
	this._attachOnAuthenticate();
	this._attachOnMessage();
	this._attachOnPresence();
	this._attachOnQuery();

}


BaseChat.prototype._removeChannel = function( oChannel, forced ) {
	this._deleteChannel( oChannel, forced );
	delete( this._channelNames[ oChannel.name ] );
	delete( this._channels[ oChannel.id ] );

}


BaseChat.prototype._onConnect = function() {
	this._attachEventHandlers();
	this.onConnect();

}


BaseChat.prototype._onDisconnect = function() {
	this._connected = false;
	this.onDisconnect();

}


BaseChat.prototype._onError = function( error ) {
	this.onError( error );
}


BaseChat.prototype._onAuthenticate = function() {
	this._connected = true;
	this.onAuthenticate();
}


BaseChat.prototype._onMessage = function( message ) {
	this.onMessage( message );
}


BaseChat.prototype._onPresence = function( presence ) {
	this.onPresence( presence );
}


BaseChat.prototype._onQuery = function( query ) {
	this.onQuery( query );
}


BaseChat.prototype._createChannel = function( id, name, outputName, isCustom ) {
	return new BaseChannel( id, name, owner, outputName, isCustom );
}


BaseChat.prototype._createUser = function( userName ) {
	return new BaseUser( userName );
}


BaseChat.prototype._getUrl = function() {
}


BaseChat.prototype._getServer = function() {
}


BaseChat.prototype._getDomain = function() {
}


BaseChat.prototype._connect = function() {
}


BaseChat.prototype._disconnect = function( type ) {
}


BaseChat.prototype._getChatClient = function() {
}


// �������� _onConnect
BaseChat.prototype._attachOnConnect = function() {
}


// �������� _onDisconnect
BaseChat.prototype._attachOnDisconnect = function() {
}


// �������� _onError
BaseChat.prototype._attachOnError = function() {
}


// �������� _onAuthenticate
BaseChat.prototype._attachOnAuthenticate = function() {
}


// �������� _onMessage
BaseChat.prototype._attachOnMessage = function() {
}


// �������� _onPresence
BaseChat.prototype._attachOnPresence = function() {
}


// �������� _onQuery
BaseChat.prototype._attachOnQuery = function() {
}


BaseChat.prototype._removeUser = function( oUser ) {
}


BaseChat.prototype._deleteChannel = function( oChannel, forced ) {
}





/*****************************************************************************
* BaseChannel
*/
function BaseChannel( id, name, owner, outputName, isCustom ) {
	if ( typeof( id ) == 'undefined' ) {
		id = '';
	}

	if ( typeof( name ) == 'undefined' ) {
		name = '';
	}

	if ( typeof( outputName ) == 'undefined' ) {
		outputName = name;
	}

	if ( typeof( isCustom ) == 'undefined' ) {
		isCustom = 0;
	}

	isCustom = ( isCustom > 0 ) ? 1 : 0;

	this.id = id;
	this.name = name;
	this.owner = owner;
	this.outputName = outputName;
	this.isCustom = isCustom;

	if ( this.owner ) {
		this.channelId = this.owner.generateChannelId();
	}

}


BaseChannel.prototype.appendMessage = function( mId, type, time, from, to, text, style, special ) {
	var str = this.owner.formatMessage( mId, type, time, from, to, text, style, special, this );

	if ( str.length > 0 ) {
		this.appendText( str );
		this._afterAppendMessage();

	}

}


BaseChannel.prototype.appendText = function( text ) {
}


BaseChannel.prototype.clear = function() {
}


BaseChannel.prototype.scrollBottom = function() {
}


BaseChannel.prototype._afterAppendMessage = function() {
}





/*****************************************************************************
* BaseUser
*/
function BaseUser( userName, owner ) {
	this.userName = userName;
	this.owner = owner;
	this.userInfo = {};

	if ( this.owner ) {
		this.userId = this.owner.generateUserId();
	}

}
