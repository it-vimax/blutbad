<?
	include $_SERVER["DOCUMENT_ROOT"]."/connect.php";

  header('Content-type: application/json');
  header('Content-Type: text/html; charset=utf-8');

  function CP1251_to_UTF8($text = '', $revers = true){
   return $revers?@iconv('CP1251', 'UTF-8', $text):@iconv('UTF-8', 'CP1251', $text);
  }

  function f_format_string_date($date_str, $parm = array()) {
    if ($date_str == '' || empty($date_str)) return '';

    $date       = date('d.m.Y', $date_str);
    $date_month = date('d.m', $date_str);
    $date_year  = date('Y', $date_str);
    $date_time  = date('H:i', $date_str);

    $ndate_exp = explode('.', $date);
    $nmonth = array(1 => '������', 2 => '�������', 3 => '�����', 4 => '������', 5 => '���', 6 => '����', 7 => '����', 8 => '�������', 9 => '��������', 10 => '�������', 11 => '������', 12 => '�������');

     foreach ($nmonth as $key => $value) { if($key == intval($ndate_exp[1])) $nmonth_name = $value; }

     if (isset($parm['future'])) {
            if (($date_str - 3 * 60) <= time()){ $datetime = '������ ���'; }
       else if (($date_str - 59 * 60) <= time()){ $datetime = floor((time() - $date_str) / 60).' ���. ���'; }
       else if ($date == date('d.m.Y')){ $datetime = 'C������ � ' .$date_time; }
       else if ($date == date('d.m.Y', strtotime('+1 day'))) {$datetime = '������ � ' .$date_time;}
       else if ($date != date('d.m.Y') && $date_year != date('Y')){ $datetime = $ndate_exp[0].' '.$nmonth_name.' '.$ndate_exp[2]. ' � '.$date_time; }
       else { $datetime = $ndate_exp[0].' '.$nmonth_name.' � '.$date_time; }
     } else {
            if (($date_str + 3 * 60) >= time()){ $datetime = '������ ���'; }
       else if (($date_str + 59 * 60) >= time()){ $datetime = floor((time() - $date_str) / 60).' ���. �����'; }
       else if ($date == date('d.m.Y')){ $datetime = 'C������ � ' .$date_time; }
       else if ($date == date('d.m.Y', strtotime('-1 day'))) {$datetime = '����� � ' .$date_time;}
       else if ($date != date('d.m.Y') && $date_year != date('Y')){ $datetime = $ndate_exp[0].' '.$nmonth_name.' '.$ndate_exp[2]. ' � '.$date_time; }
       else { $datetime = $ndate_exp[0].' '.$nmonth_name.' � '.$date_time; }
     }

   return $datetime;
  }

    $login = CP1251_to_UTF8($_POST['login']?$_POST['login']:$_GET['login'], false);
    $pass  = CP1251_to_UTF8($_POST['pass']?$_POST['pass']:$_GET['pass'], false);
    $user_id  = CP1251_to_UTF8($_POST['user_id']?$_POST['user_id']:$_GET['user_id'], false);
    $user_hach  = CP1251_to_UTF8($_POST['user_hach']?$_POST['user_hach']:$_GET['user_hach'], false);

  if ($login == 'ww') $login = '�����'; else $login = CP1251_to_UTF8($login, false);

  /***------------------------------------------
   * �������� ������ ����� ���������
   **/

  if (isset($login)) {

     $posts_user = sql_row("SELECT `id` FROM `users` WHERE `login` = '".$login."' LIMIT 1;");

     if (empty($posts_user['id'])) {
       die('{"data":[{"status":"error"}]}');
     } else {

       $eff_all = sql_query("SELECT * FROM `effects` WHERE `owner` = '".$posts_user['id']."' ORDER BY `type` DESC;");

       $list_eff = "";
       while ($row = fetch_array($eff_all)) {

        if (empty($row['subject'])) $row['subject'] = "��� ����";

        $row['subject'] = str_replace("'",'&#039;', $row['subject']);
        $row['text'] = str_replace("'",'&#039;', $row['text']);
        $row['text'] = str_replace("&nbsp;",' ', $row['text']);

       # ������� ��� html ����
         $row['subject'] = strip_tags($row['subject']);
         $row['text'] = strip_tags($row['text']);

         $list_eff .= ($list_eff?",":"")."{'name':'".CP1251_to_UTF8($row['name'])."', 'opisan':'".CP1251_to_UTF8($row['opisan'])."', 'from_whom':'".CP1251_to_UTF8($row['from_whom'])."', 'total':'".CP1251_to_UTF8($row['total'])."', 'time':'".CP1251_to_UTF8(f_format_string_date($row['time'], array("future" => true)))."'}";


       }

     }

     if ($list_eff) {

       die('{"data":[{"status":"ok"}, '.$list_eff.']}');

     } else {
       die('{"data":[{"status":"noumenie"}]}');
     }

  } else {
     die('{"data":[{"status":"error"}]}');
  }

?>