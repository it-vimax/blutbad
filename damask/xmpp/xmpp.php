<?php

require_once($_SERVER["DOCUMENT_ROOT"].'/jabber_cache.php');

class XMPP {

	private static $login;

	private static $server;

	private static $conference;

	private static $xml;

	public static function init( $login, $server, $conference ) {
		self::$login = $login;

		self::$server = $server;

		self::$conference = $conference;

		self::$xml = '';

		Cache::init();
	}

	public static function message( $from, $to, $text, $type = null ) {
		if(empty($from) || $from == null) $from = self::$login;
		if(!is_array($to)) $to = [$to];
		if(!$type || (int) $type == 1) $type = 'chat';

		if($type == 'groupchat' || (int) $type == 2) {
			$type = 'groupchat';

			$conference = self::$conference . '.';
		}

		if($from != -1) {
			if($type == 'chat') $from = self::nameToLogin($from) . '@' . self::$server;
			elseif($type == 'groupchat') $from = self::nameToLogin($from);
		}

		for($i = 0; $i < count($to); $i++) {
			$jid = self::nameToLogin($to[$i]) . '@' . $conference . self::$server;

			$xml .= "<message to='{$jid}' type='{$type}'>";
				$xml .= "<body>".self::jab_bodytext($text)."</body>";

				if($from != -1) $xml .= "<from>{$from}</from>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function newUser( $login, $password, $name = null ) {
		$login = self::nameToLogin($login);
		$password = trim($password);

		if($name != null) $name = trim($name);
		else $name = '';

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-register'>";
				$xml .= "<username>{$login}</username>";
				$xml .= "<password>{$password}</password>";
				$xml .= "<name>{$name}</name>";
			$xml .= '</query>';
		$xml .= '</iq>';

		self::$xml .= $xml;
	}

	public static function editUser( $login, $password = null, $name = null, $email = null ) {
		$login = self::nameToLogin($login);

		if($password) $password = '<password>' . trim($password) . "</password>";
		if($name) $name = '<name>' . trim($name) . "</name>";
		if($email) $email = '<email>' . trim($email) . "</email>";

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-edit'>";
				$xml .= "<username>{$login}</username>";
				$xml .= $password;
				$xml .= $name;
				$xml .= $email;
			$xml .= "</query>";
		$xml .= '</iq>';

		self::$xml .= $xml;
	}

	public static function removeUser( $login ) {
		$login = self::nameToLogin($login);

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-unregister'>";
				$xml .= "<username>{$login}</username>";
			$xml .= '</query>';
		$xml .= '</iq>';

		self::$xml .= $xml;
	}

	public static function changeUsername( $old, $new ) {
		$old = self::nameToLogin($old);
		$new = self::nameToLogin($new);

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-change-username'>";
				$xml .= "<old>{$old}</old>";
				$xml .= "<new>{$new}</new>";
			$xml .= "</query>";
		$xml .= '</iq>';

		if(strlen($old) > 0 && strlen($new) > 0) self::$xml .= $xml;
	}

	public static function changeStyle( $login, $style ) {
		$login = self::nameToLogin($login);
		$style = trim($style);

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-style'>";
				$xml .= "<username>{$login}</username>";
				$xml .= "<style>{$style}</style>";
			$xml .= "</query>";
		$xml .= '</iq>';

		if(strlen($login) > 0 && strlen($style) > 0) self::$xml .= $xml;
	}

	public static function changeInvisible( $login, $invisible ) {
		$login = self::nameToLogin($login);
		$invisible = abs($invisible);

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-invisible'>";
				$xml .= "<username>{$login}</username>";
				$xml .= "<invisible>{$invisible}</invisible>";
			$xml .= "</query>";
		$xml .= '</iq>';

		if(strlen($login) > 0 && strlen($invisible) > 0) self::$xml .= $xml;
	}

	public static function changeTelepathy( $login, $telepathy ) {  # login bd =  login1,login2
		$login = self::nameToLogin($login);
		$telepathy = self::nameToLogin($telepathy);

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-telepathy'>";
				$xml .= "<username>{$login}</username>";
				$xml .= "<telepathy>{$telepathy}</telepathy>";
			$xml .= "</query>";
		$xml .= '</iq>';

		if(strlen($login) > 0) self::$xml .= $xml;
	}

	public static function changeBlind( $login, $blind ) {
		$login = self::nameToLogin($login);
		$blind = trim($blind);

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-blind'>";
				$xml .= "<username>{$login}</username>";
				$xml .= "<blind>{$blind}</blind>";
			$xml .= "</query>";
		$xml .= '</iq>';

		if(strlen($login) > 0 && strlen($blind) > 0) self::$xml .= $xml;
	}

	public static function changeSilence( $login, $silence ) {
		$login = self::nameToLogin($login);
		$silence = trim($silence);

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-silence'>";
				$xml .= "<username>{$login}</username>";
				$xml .= "<silence>{$silence}</silence>";
			$xml .= "</query>";
		$xml .= '</iq>';

		if(strlen($login) > 0 && strlen($silence) > 0) self::$xml .= $xml;
	}

	public static function closeSession( $login ) {
		$login = self::nameToLogin($login);

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-close-session'>";
				$xml .= "<username>{$login}</username>";
			$xml .= '</query>';
		$xml .= '</iq>';

		if(strlen($login) > 0) self::$xml .= $xml;
	}

	public static function changeIgnored( $login, $ignored, $action = 1 ) {
		$login = self::nameToLogin($login);

		if(is_array($ignored)) $ignored = join(',', $ignored);

		if($action === 1) $action = 'add';
		elseif($action === 2) $action = 'remove';

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-ignored'>";
				$xml .= "<username>{$login}</username>";
				$xml .= "<action>{$action}</action>";
				$xml .= "<ignored>{$ignored}</ignored>";
			$xml .= '</query>';
		$xml .= '</iq>';

		if(strlen($login) > 0 && strlen($ignored) > 0) self::$xml .= $xml;
	}

	public static function roomManager( $name, $action = 1, $userlist = null, $persistent = false, $moderated = false ) {
		//$name = self::nameToLogin($name);

		if($action === 1) $action = 'create';
		elseif($action === 2) $action = 'delete';
		elseif($action === 3) $action = 'removeUser';

		if($userlist == null) $userlist = '';
		elseif(is_array($userlist)){
		  $users = $userlist;

		  foreach($users as $user) $_userlist[] = $user['j_login'];

          $userlist = join(',', $_userlist);

        //  Cache::redis()->rpush('room', 'name ' .  $name);
        //  Cache::redis()->rpush('room', 'userlist ' . CP1251_to_UTF8($userlist));
		}

		if($persistent === false) $persistent = '0';
		elseif($persistent === true) $persistent = '1';

		if($moderated === false) $moderated = '0';
		elseif($moderated === true) $moderated = '1';

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-room-manager'>";
				$xml .= "<name>{$name}</name>";
				$xml .= "<action>{$action}</action>";
				$xml .= "<userlist>{$userlist}</userlist>";
				$xml .= "<persistent>{$persistent}</persistent>";
				$xml .= "<moderated>{$moderated}</moderated>";
			$xml .= '</query>';
		$xml .= '</iq>';

		self::$xml .= $xml;
	}

	public static function battleRoomManager( $city, $battleID, $action = 1, $userlist = null ) {
		self::roomManager($city . '_l' . $battleID, $action, $userlist);
	}

	public static function groupRoomManager( $city, $battleID, $side, $action = 1 ) {
		self::roomManager($city . '_b' . $battleID . '_' . $side, $action);
	}

	public static function system( $city, $room, $text, $notme = null ) {
		if($room == null) $room = 'system';
		if(!is_array($room)) $room = [$room];

		for($i = 0; $i < count($room); $i++) {
			$city_room = $city . '_' . $room[$i];

			$jid = $city_room . '@' . self::$conference . '.' . self::$server;

			$xml .= "<message type='groupchat' to='{$jid}'>";
				$xml .= "<body special='1' message_type='system'>message|room={$city}_system|type=system|message=".self::jab_bodytext($text)."|</body>";

				if($notme != null) $xml .= "<notme>{$notme}</notme>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function message_room( $city, $room, $text ) {
		if(!is_array($room)) $room = [$room];

		for($i = 0; $i < count($room); $i++) {
			$city_room = $city . '_' . $room[$i];

			$jid = $city_room . '@' . self::$conference . '.' . self::$server;

			$xml .= "<message type='groupchat' to='{$jid}'>";
				$xml .= "<body special='1' message_type='room'>message|room={$city}_" . $room[$i] . "|type=room|message=".self::jab_bodytext($text)."|</body>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function room_message( $city_room, $text ) {
			$jid = $city_room . '@' . self::$conference . '.' . self::$server;

			$xml .= "<message type='groupchat' to='{$jid}'>";
				$xml .= "<body special='1' message_type='room'>message|subtype=|type=room|room=" . $city_room . "|message=".self::jab_bodytext($text)."|</body>";
			$xml .= "</message>";

		self::$xml .= $xml;
	}

	public static function attention( $to, $text ) {
		if(!is_array($to)) $to = [$to];

		for($i = 0; $i < count($to); $i++) {
			$jid = self::nameToLogin($to[$i]) . '@' . self::$server;

			$xml .= "<message type='chat' to='{$jid}'>";
				$xml .= "<body special='1' message_type='attention'>message|type=attention|message=".self::jab_bodytext($text)."|</body>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function log_message( $to, $text ) {
		if(!is_array($to)) $to = [$to];

		for($i = 0; $i < count($to); $i++) {
			$jid = $to[$i] . '@' . self::$server;

			$xml .= "<message type='chat' to='{$jid}'>";
			  	$xml .= "<body command='1'>battle_log|rows=".self::jab_bodytext($text)."|</body>";
			   //	$xml .= '<body command="1">battle_log|rows=13:57:57&amp;#166;2&amp;#166;1013061402&amp;#166;1001431261&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ��������, �� {{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}}, ���������, ����� ���� ������� �� ��������� {{b}}-0{{/b}} [2187/2187]&amp;#166;&amp;#166;13:57:57&amp;#166;2&amp;#166;1001431261&amp;#166;1013061402&amp;#166;{{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}} ���������, �� {{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}}, ������, ����� ����������� ���� ������� � ���� {{b}}{{font color&amp;#61;red}}-330{{/font}}{{/b}} [0/330]&amp;#166;&amp;#166;13:57:57&amp;#166;2&amp;#166;1013061402&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}} ��������&amp;#166;&amp;#166;13:57:57&amp;#166;4&amp;#166;1001431261&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ������� ������� &apos;������� �������&apos;.&amp;#166;&amp;#166;13:57:57&amp;#166;5&amp;#166;1001431261&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ������� ������� &apos;������ �������&apos;.&amp;#166;&amp;#166;13:57:57&amp;#166;6&amp;#166;0&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ����� ������� &apos;�����������&apos; � {{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}}&amp;#166;&amp;#166;13:57:57&amp;#166;7&amp;#166;0&amp;#166;0&amp;#166;��� ��������. ������ �� [[zr2bl()]]{{span class&amp;#61;&quot;user1&quot;}}������ ������{{/span}}|</body>';
			$xml .= "</message>";
		}
         // 	<message xmlns="jabber:client" from="4_l1043397747@rooms.localhost/admin" to="fullzero@localhost/Carnage" xml:lang="en" id="6111837" type="groupchat">
         // 		<body time="13:57:58" command="1">battle_log|rows=13:57:57&amp;#166;2&amp;#166;1013061402&amp;#166;1001431261&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ��������, �� {{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}}, ���������, ����� ���� ������� �� ��������� {{b}}-0{{/b}} [2187/2187]&amp;#166;&amp;#166;13:57:57&amp;#166;2&amp;#166;1001431261&amp;#166;1013061402&amp;#166;{{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}} ���������, �� {{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}}, ������, ����� ����������� ���� ������� � ���� {{b}}{{font color&amp;#61;red}}-330{{/font}}{{/b}} [0/330]&amp;#166;&amp;#166;13:57:57&amp;#166;2&amp;#166;1013061402&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}} ��������&amp;#166;&amp;#166;13:57:57&amp;#166;4&amp;#166;1001431261&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ������� ������� &apos;������� �������&apos;.&amp;#166;&amp;#166;13:57:57&amp;#166;5&amp;#166;1001431261&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ������� ������� &apos;������ �������&apos;.&amp;#166;&amp;#166;13:57:57&amp;#166;6&amp;#166;0&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ����� ������� &apos;�����������&apos; � {{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}}&amp;#166;&amp;#166;13:57:57&amp;#166;7&amp;#166;0&amp;#166;0&amp;#166;��� ��������. ������ �� [[zr2bl()]]{{span class&amp;#61;&quot;user1&quot;}}������ ������{{/span}}|</body>
		self::$xml .= $xml;
	}

	public static function private_message( $to, $text ) {
		if(!is_array($to)) $to = [$to];

		for($i = 0; $i < count($to); $i++) {
			$jid = $to[$i] . '@' . self::$server;

			$xml .= "<message type='chat' to='{$jid}'>";
			   	$xml .= "<body special='1' tutorial='0' message_type='private'>message|type=private|message=".self::jab_bodytext($text)."|</body>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function city( $city, $text, $notme = null ) {
		$xml .= "<message type='groupchat' to='" . $city . '_system@' . self::$conference . '.' . self::$server . "'>";
			$xml .= "<body special='1' city='all'>".self::jab_bodytext($text)."</body>";

			if($notme != null) {
				$notme = self::nameToLogin($notme);

				$xml .= "<notme>{$notme}</notme>";
			}
		$xml .= "</message>";

		self::$xml .= $xml;
	}

	public static function battleLog( $city, $battleID, $text ) {
		self::command_room($city, 'l' . $battleID, 'battle_log|rows=' . self::jab_bodytext($text));
	}

	public static function command( $to, $text ) {
		$to = self::nameToLogin($to);

		$xml = "<message to='{$to}@" . self::$server . "' type='chat'>";
			$xml .= "<body command='1'>".$text."</body>";
		$xml .= "</message>";

		self::$xml .= $xml;
	}

	public static function command_room( $city, $room, $text ) {
		$to = $city . '_' . $room;

		$xml = "<message to='{$to}@" . self::$conference . '.' . self::$server . "' type='groupchat'>";
			$xml .= "<body command='1'>".$text."|</body>";
		$xml .= '</message>';

		self::$xml .= $xml;
	}

	public static function inbox( $to, $count ) {
		self::command($to, 'inbox|count=' . $count);
	}

	public static function changeRoom( $to, $city, $room, $room_name ) {
		self::command($to, 'change_room|room=' . $city . '_' . $room . '|room_name=' . $room_name);
	}

	public static function reloadPanel( $to ) {
		self::command($to, 'reload_panel');
	}

	public static function joinChannel( $to, $city, $battleID, $side = null ) {
		if($side == null) self::command($to, 'join_channel|channel=' . $city . '_l' . $battleID . '|channel_name=��� ���');
		elseif($side === 1 || $side === 2) self::command($to, 'join_channel|channel=' . $city . '_b' . $battleID . '_' . $side . '|channel_name=���������');
	}

	public static function leaveChannel( $to, $city, $battleID, $side = null ) {
		if($side == null) self::command($to, 'leave_channel|channel=' . $city . '_l' . $battleID);
		elseif($side === 1 || $side === 2) self::command($to, 'leave_channel|channel=' . $city . '_b' . $battleID . '_' . $side);
	}

	private static function nameToLogin( $login ) {
	   //	return mb_strtolower(str_replace(' ', '_', trim($login)), "CP1251");
        return str_replace(' ', '_', mb_strtolower(str_replace('"', '', $login), "CP1251"));
	}

	public static function xml() {
	   $xml = self::$xml;

		self::$xml = '';

		return $xml;
	}

	public static function jab_bodytext($text, $parm_no_char = array()) {

        $text = str_replace("&", "&amp;", $text);

      # �������
        $text = str_replace("<script>", "[[", $text);
        $text = str_replace("</script>", "]]", $text);

      # �������
        $text = str_replace("\'", '&#39;', $text);
        $text = str_replace('\"', '&quot;', $text);
        $text = str_replace('"', "&quot;", $text);
        $text = str_replace("'", "&#39;", $text);

        $text = str_replace("'", "&apos;", $text);
      # ����� �������
        $text = str_replace("�", "-", $text);


        if (empty($parm_no_char['|'])) {
          $text = str_replace('|', '&amp;#166;', $text);
        }
        if (empty($parm_no_char['='])) {
          $text = str_replace('=', '&amp;#61;', $text);
        }

        $text = str_replace('<', '{{', $text);
        $text = str_replace('>', '}}', $text);

	    return $text;
	}

}

?>