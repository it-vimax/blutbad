<?php

require_once('XMPP.php');

class XMPP {

	public static $encryption = true;

	private static $xmpp;

	private static $server;

	private static $login;

	private static $conference;

	private static $rooms;

	private static $xml;

	public static function connect( $login, $password, $server, $conference = null ) {
		self::$server = $server;

		self::$login = $login;

		self::$conference = $conference;

		self::$xmpp = new XMPPHP_XMPP($server, 5222, $login, $password, 'PHP');

		self::$xmpp->useEncryption(self::$encryption);

		self::$xmpp->connect();

		self::$xmpp->processUntil('session_start');

		self::$xmpp->presence(-1);

		self::$rooms = [];
	}

	public static function init( $login, $server, $conference = null ) {
		self::$server = $server;

		self::$login = $login;

		self::$conference = $conference;
	}

	public static function message( $from, $to, $text, $type = null ) {
		if(empty($from) || $from == null) $from = self::$login;
		if(!is_array($to)) $to = [$to];
		if(!$type || (int) $type == 1) $type = 'chat';

		if($type == 'groupchat' || (int) $type == 2) {
			$type = 'groupchat';

			$conference = self::$conference . '.';
		}

		if($from != -1) {
			if($type == 'chat') $from = $from . '@' . self::$server;
			elseif($type == 'groupchat') $from = $from;
		}

		for($i = 0; $i < count($to); $i++) {
			$jid = $to[$i] . '@' . $conference . self::$server;

			$xml .= "<message to=\"{$jid}\" type='{$type}'>";
				$xml .= "<body>".self::jab_bodytext($text)."</body>";

				if($from != -1) $xml .= "<from>{$from}</from>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function newUser( $login, $password, $name = null, $email = null ) {
		$login = str_replace(' ', '_', trim($login));
		$password = trim($password);

		if($name) $name = '<name>' . trim($name) . "</name>";
		if($email) $email = '<email>' . trim($email) . "</email>";

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:register'>";
				$xml .= "<username>{$login}</username>";
				$xml .= "<password>{$password}</password>";
				$xml .= $name;
				$xml .= $email;
			$xml .= "</query>";
		$xml .= '</iq>';

		self::$xml .= $xml;
	}

	public static function editUser( $login, $password = null, $name = null, $email = null ) {
		$login = str_replace(' ', '_', trim($login));

		if($password) $password = '<password>' . trim($password) . "</password>";
		if($name) $name = '<name>' . trim($name) . "</name>";
		if($email) $email = '<email>' . trim($email) . "</email>";

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-edit'>";
				$xml .= "<username>{$login}</username>";
				$xml .= $password;
				$xml .= $name;
				$xml .= $email;
			$xml .= "</query>";
		$xml .= '</iq>';

		self::$xml .= $xml;
	}

	public static function changeUsername( $old, $new ) {
		$old = str_replace(' ', '_', trim($old));
		$new = str_replace(' ', '_', trim($new));

		$xml = "<iq type='set' to='" . self::$server . "'>";
			$xml .= "<query xmlns='jabber:iq:blutbad-change-username'>";
				$xml .= "<old>{$old}</old>";
				$xml .= "<new>{$new}</new>";
			$xml .= "</query>";
		$xml .= '</iq>';

		if(strlen($old) > 0 && strlen($new) > 0) self::$xml .= $xml;
	}

	public static function room( $name, $password = null ) {
		if(is_array($name)) $name = $name[0] . '_' . ($name[1] == null ? 'system' : $name[1]);
		else $name = trim($name);

		if($password) $password = trim($password);

		$xml = "<presence to='{$name}@" . self::$conference . "." . self::$server . "/" . self::$login . "'>";

			if($password) $xml .= "<x xmlns='http://jabber.org/protocol/muc'><password>{$password}</password></x>";
			else $xml .= "<x xmlns='http://jabber.org/protocol/muc'/>";

		$xml .= "</presence>";

		self::$rooms[] = $name;

		self::$xml .= $xml;
	}

	private static function roomOut( $name ) {
		if(is_array($name)) $name = $name[0] . '_' . ($name[1] == null ? 'system' : $name[1]);
		else $name = trim($name);

		self::$xml .= "<presence to='{$name}@" . self::$conference . "." . self::$server . "/" . self::$login . "' type='unavailable' />";
	}

/***------------------------------------------
 * �������� ��� � ��� �����
 *
 * $city  = ����� ������
 * $room  = ������� ������: 100|null  � ���� ��� �� ���� �������� ��������
 * $text  = �����
 * $notme = 'br' ��� �� ��������� ���� �������
 *
 **/

	public static function system( $city, $room, $text, $notme = null ) {
		if($room == null) $room = 'system';
		if(!is_array($room)) $room = [$room];

		for($i = 0; $i < count($room); $i++) {
			$city_room = $city . '_' . $room[$i];

			$jid = $city_room . '@' . self::$conference . '.' . self::$server;

			$xml .= "<message type='groupchat' to='{$jid}'>";
				$xml .= "<body special='1' message_type='system'>message|room={$city}_system|type=system|message=".self::jab_bodytext($text)."|</body>";

				if($notme != null) $xml .= "<notme>{$notme}</notme>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function attention( $to, $text ) {
		if(!is_array($to)) $to = [$to];

		for($i = 0; $i < count($to); $i++) {
			$jid = $to[$i] . '@' . self::$server;

			$xml .= "<message type='chat' to='{$jid}'>";
				$xml .= "<body special='1' message_type='attention'>message|type=attention|message=".self::jab_bodytext($text)."|</body>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function log_message( $to, $text ) {
		if(!is_array($to)) $to = [$to];

		for($i = 0; $i < count($to); $i++) {
			$jid = $to[$i] . '@' . self::$server;

			$xml .= "<message type='chat' to='{$jid}'>";
				$xml .= "<body command='1'>battle_log|rows=".self::jab_bodytext($text)."|</body>";
			$xml .= "</message>";
		}
         // 	<message xmlns="jabber:client" from="4_l1043397747@rooms.localhost/admin" to="fullzero@localhost/Carnage" xml:lang="en" id="6111837" type="groupchat">
         // 		<body time="13:57:58" command="1">battle_log|rows=13:57:57&amp;#166;2&amp;#166;1013061402&amp;#166;1001431261&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ��������, �� {{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}}, ���������, ����� ���� ������� �� ��������� {{b}}-0{{/b}} [2187/2187]&amp;#166;&amp;#166;13:57:57&amp;#166;2&amp;#166;1001431261&amp;#166;1013061402&amp;#166;{{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}} ���������, �� {{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}}, ������, ����� ����������� ���� ������� � ���� {{b}}{{font color&amp;#61;red}}-330{{/font}}{{/b}} [0/330]&amp;#166;&amp;#166;13:57:57&amp;#166;2&amp;#166;1013061402&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}} ��������&amp;#166;&amp;#166;13:57:57&amp;#166;4&amp;#166;1001431261&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ������� ������� &apos;������� �������&apos;.&amp;#166;&amp;#166;13:57:57&amp;#166;5&amp;#166;1001431261&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ������� ������� &apos;������ �������&apos;.&amp;#166;&amp;#166;13:57:57&amp;#166;6&amp;#166;0&amp;#166;0&amp;#166;{{font class&amp;#61;&quot;user1&quot;}}������ ������{{/font}} ����� ������� &apos;�����������&apos; � {{font class&amp;#61;&quot;user2&quot;}}FullZero{{/font}}&amp;#166;&amp;#166;13:57:57&amp;#166;7&amp;#166;0&amp;#166;0&amp;#166;��� ��������. ������ �� [[zr2bl()]]{{span class&amp;#61;&quot;user1&quot;}}������ ������{{/span}}|</body>
		self::$xml .= $xml;
	}

	public static function room_message( $to, $text ) {
		if(!is_array($to)) $to = [$to];

		for($i = 0; $i < count($to); $i++) {
			$jid = $to[$i] . '@' . self::$server;

			$xml .= "<message type='chat' to='{$jid}'>";
				$xml .= "<body special='1' message_type='room'>message|type=room|message=".self::jab_bodytext($text)."|</body>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function private_message( $to, $text ) {
		if(!is_array($to)) $to = [$to];

		for($i = 0; $i < count($to); $i++) {
			$jid = $to[$i] . '@' . self::$server;

			$xml .= "<message type='chat' to='{$jid}'>";
			   	$xml .= "<body special='1' tutorial='0' message_type='private'>message|type=private|message=".self::jab_bodytext($text)."|</body>";
			$xml .= "</message>";
		}

		self::$xml .= $xml;
	}

	public static function city( $city, $text, $notme = null ) {
		$xml .= "<message type='groupchat' to='" . $city . '_system@' . self::$conference . '.' . self::$server . "'>";
			$xml .= "<body special='1' city='all'>".self::jab_bodytext($text)."</body>";

		if($notme != null) $xml .= "<notme>{$notme}</notme>";
		$xml .= "</message>";

		self::$xml .= $xml;
	}


	public static function command( $to, $text ) {
		$xml = "<message to='{$to}@" . self::$server . "' type='chat'>";
			$xml .= "<body command='1'>".self::jab_bodytext($text)."</body>";
		$xml .= "</message>";

		self::$xml .= $xml;
	}

	public static function inbox( $to, $count ) {
		self::command($to, 'inbox|count=' . $count);
	}

	public static function changeRoom( $to, $city, $room, $room_name ) {
		self::command($to, 'change_room|room=' . $city . '_' . $room . '|room_name=' . $room_name);
	}

	public static function reloadPanel( $to ) {
		self::command($to, 'reload_panel');
	}

	public static function xml() {
		for($i = 0; $i < count(self::$rooms); $i++) self::roomOut(self::$rooms[$i]);

		self::$rooms = [];

		$xml = self::$xml;

		self::$xml = '';

		return $xml;
	}

	public static function send( $xml ) {
        return self::$xmpp->send(iconv('CP1251', 'UTF-8', $xml));
	}

	public static function off() {
		self::$xmpp->disconnect();
	}

	public static function jab_bodytext($text) {

        $text = str_replace('<font color=red>��������! </font>', '', $text);
        $text = str_replace('<font color="red">��������!</font>', '', $text);

      # ��������� ��� ���� ���� ����� ������
       // $text = str_replace('&mdash;', '', $text);

        $text = str_replace('<', '{{', $text);
        $text = str_replace('>', '}}', $text);

        $text = str_replace('"', '&quot;', $text);
        $text = str_replace("'", '&#39;', $text);

	    return $text;
	}

}

?>