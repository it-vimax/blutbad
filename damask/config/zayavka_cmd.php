<?

 /***-------------------------------
  * ����� �� ����� � ������
  **/

function can_found_in_zayavka($typebattle, $hideb){
  global $user, $rooms, $str_stats_error;

 $lethal_injury = 0;

# ��������� ������
  if ($user['sila'] == 0 || $user['lovk'] == 0 || $user['inta'] == 0) { $lethal_injury = 1; }

  if ($typebattle == 3 && $user['room'] == 102) {
   $_SESSION['zayavka_hit'] = "� ������� '".$rooms[$user['room']]."' ������ ����������� � ����������� ����"; return false;
  }

  if ($typebattle == 7 && $user['invis'] && ($user['align'] != 1.99 && $user['align'] != 2.2)) {
   $_SESSION['zayavka_hit'] = "��� ������������ ������"; return false;
  }

  if (($typebattle == 3 || $typebattle == 4)) {
   $is_zay = sql_number("SELECT `id` FROM `zayavka` WHERE id > 1 AND (`type` = 3 or `type` = 7);");
    if ($is_zay >= 20) {
     $_SESSION['zayavka_hit'] = "������ �� ��� ���������� �������� �� ��� ������������"; return false;
    }
  }

   if ($user['eff_chain']) {
     	$_SESSION['zayavka_hit'] = "�� ��� �������� ����"; return false;
   } elseif ($lethal_injury) {
     	$_SESSION['zayavka_hit'] = "� ��� ��������� ������ �� �� ������ ����������� � ���������"; return false;
   } elseif ($user['battle']) {
     	$_SESSION['zayavka_hit'] = "�� ���������� � ���"; return false;
   } elseif (($user['hp'] <= $user['maxhp']*0.5) || ($user['mana'] <= $user['maxmana']*0.5)) {
     	$_SESSION['zayavka_hit'] = "�� ������� ��������� ��� ���, ��������������."; return false;
   } elseif((@$_GET['level'] != 'haot' || @$_GET['level'] != 'group' || @$_GET['level'] != 'begin' || @$_GET['level'] != 'duel') && ($user['room'] < 101 || $user['room'] > 113)){
     	$_SESSION['zayavka_hit'] = "� ���� ����� ��� ����������. ����� ����������� � ����, ��������� � ���� �� ������ �����."; return false;
   } elseif (get_normalstats($user['id']) != 0) {
        CHAT::chat_attention($user, $str_stats_error, array('addXMPP' => true));

     	$_SESSION['zayavka_hit'] = $str_stats_error; return false;
   } else {
    return true;
   }

}

$zay_section = @$_GET['zay_section'];

/***------------------------------------------
 * cmd = �������������
 **/
   if ($zay_section == 'tren') {
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/cmd_tren.php");
   }

/***------------------------------------------
 * cmd = �����
 **/
   if ($zay_section == 'duel' || $zay_section == 'duel-inter') {
     include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/cmd_duel.php");
   }

/***------------------------------------------
 * cmd = �����
 **/
   if ($zay_section == 'arena') {
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/cmd_arena.php");
   }

/***------------------------------------------
 * cmd = ���������
 **/
   if ($zay_section == 'group') {
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/cmd_group.php");
   }

/***------------------------------------------
 * cmd = ������
 **/
   if ($zay_section == 'haot') {
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/cmd_haot.php");
   }

/***------------------------------------------
 * cmd = �������
 **/
   if ($zay_section == 'tour') {
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/cmd_tour.php");
   }

/***------------------------------------------
 * cmd = ������������
 **/
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/cmd_halloween.php");

/***------------------------------------------
 * cmd = ����������
 **/
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/cmd_ny_chaos.php");


  if ($user['zayavka']) {
    $zaylevel = $class_ZAYAVKA->zay_inuser;
  } else {
    $zaylevel['level'] = 0;
    $zaylevel['type']  = 0;
  }

/***------------------------------------------
 * ���� �������� ��������� ���������
 **/

if($_POST['cmd'] != 'getpage'){

   if (empty($_GET['level'])) {
     if ($user['level'] == 0) {
        $_GET['level']='tren';
     } elseif ($user['level'] == 1) {
        $_GET['level']='duel';
     } elseif ($user['level'] == 2) {
        $_GET['level']='group';
     } elseif ($user['level'] >= 3) {
        $_GET['level']='haot';
     }

     switch ($zaylevel['type']) {
       case 1:  $_GET['level']  = 'tren';      break;
       case 2:  $_GET['level']  = 'duel';      break;
       case 4:  $_GET['level']  = 'group';     break;
       case 5:  $_GET['level']  = 'haot';      break;
       case 6:  $_GET['level']  = 'ny_chaos';  break;
       case 7:
       case 26: $_GET['level']  = 'halloween'; break;
       case 18: $_GET['level']  = 'ny_chaos';  break;
       case 19: $_GET['level']  = 'arena';     break;
     }
   }

}


/***------------------------------------------
 * ��������� ������ � �������
 **/

if ($_POST['cmd'] == 'getpage' && isset($_POST['is_ajax'])) {

 header('Content-type: text/html');

 $nd     = abs($_POST['nd']);
 $c_page = $_POST['c_page'];
 $dody_to_js = 'zaybody_table';

 switch ($c_page) {
   case 'zbtest':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/test.php");
   break;
   case 'zbtren':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/begin.php");
   break;
   case 'zbduel':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/duel.php");
   break;
   case 'zbarena':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/arena.php");
   break;
   case 'zbgroup':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/group.php");
   break;
   case 'zbhaot':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/haot.php");
   break;
   case 'zbny_chaos':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/ny_chaos.php");
   break;
   case 'zbtournament':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/tournament.php");
   break;
   case 'zbhalloween':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/halloween.php");
   break;
   case 'zbcur':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/tklogs.php");
   break;
   case 'zbend':
    include($_SERVER["DOCUMENT_ROOT"]."/functions/zayavka/logs.php");
   break;
 }

 sql_lock_tables(false);  # �������� ���� ������
 die();
}
?>