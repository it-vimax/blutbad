var oPopup;

function showSmiles(el) {
	smiliesTarget = $('#editor')[0];
	smiliesTargetMaxLength = 250;

	if ($.browser.msie) {
		var iX = $(el).offset().left
		var iWidth = 400;

		if(iX + iWidth > $(window).width()) {
			iX -= iX + iWidth - $(window).width();
		}

		oPopup.show(iX, -500, iWidth, 500, document.body);
	} else {
		var x = window.screenX + $(window).width() - 500 - 0;
		var y = window.screenY + top.outerHeight - 500 - 100;
		if($.browser.chrome) {
		  	y -= 45;
		  	x += 45;
		} else if($.browser.opera || $.browser.safari) {
			y += 78;
		}

		window.open('/smilies.html', '_blank', 'left=' + x + ',top=' + y + ', height=530, location=no, resizable=no, width=500');
	}
};

$(function() {
	if($.browser.msie) {
    	oPopup = window.createPopup();

		var oPopBody = oPopup.document.body;

		var body = $(oPopup.document).find('body').css({
			backgroundColor: '#E0E0E0',
			border: '1px solid #000000',
			font: '11px/1.27em Verdana, Arial, Helvetica, Tahoma, sans-serif',
			padding: '1em'
		});

        var html = '' +
        	'<div id="smilies-tabs">' +
				'<ul>' +
			        '<li class="first">' +
				        '<span>���������</span>' +
					'</li>' +
					'<li class="last">' +
				       	'<span>���������</span>' +
					'</li>' +
				'</ul>' +
				'<div class="ui-tabs-panel" id="single-smilies"></div>' +
				'<div class="ui-tabs-panel" id="group-smilies"></div>' +
			'</div>';
		oPopBody.innerHTML = html;

		body.find('ul').css({
			listStyle: 'none',
			margin: '0 0 1em 0',
			padding: 0
		});

		body.find('li').css({
			backgroundColor: '#C0C0C0',
			display: 'inline',
			fontWeight: 'bold',
			textAlign: 'center',
			width: '48%',
			zoom: 1
		}).filter('.last').css({
			marginLeft: '8%'
		});

		body.find('li span').css({
			color: '#404040',
			cursor: 'pointer',
			display: 'block',
			fontWeight: 'bold',
			padding: '.2em',
			textDecoration: 'none'
		}).click(function() {
			body.find('.ui-tabs-panel').toggle();
		}).hover(
			function() {
				this.style.color = '#A50B0B'
			},
			function() {
				this.style.color = '#404040'
			}
		);

		var single = body.find('#single-smilies').empty();
		var group = body.find('#group-smilies').empty().hide();

		for(var name in smilies) {
			var smiley = smilies[name];
			var img = '' +
				'<img' +
					' alt=":' + name + ':"' +
					' height="' + smiley.height +'"' +
					' src="' + imgURL + 'sm/' + name + '.gif"' +
					' title=":' + smilies[name].codes.join(':  :') + ':"' +
					' width="' + smiley.width + '" />';

			if(smilies[name].group == 1) {
				single[0].innerHTML += img;
			} else if(smilies[name].group == 2) {
				group[0].innerHTML += img;
			}
		}

		body.find('img').css({
			cursor: 'pointer',
			margin: '.2em'
		}).click(
			function() {
				if(!smiliesTarget.disabled) {
					if (smiliesTargetMaxLength && smiliesTarget.value.length + this.alt.length + 1 > smiliesTargetMaxLength) {
						alert("����� ��������� ������������ ����� ��������!");
					} else {
						smiliesTarget.value += this.alt;

						smiliesTarget.focus();
						moveCaretToEnd(smiliesTarget);

						oPopup.hide();
					}
				}
			}
		).hover(
			function() {
				this.style.backgroundColor = '#FFFFFF';

			},
			function() {
				this.style.backgroundColor = '';
			}
		)
	}
});
