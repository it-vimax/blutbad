var blockCount = 4;
var botBattle;
var botBattlePanel;
var newbatl;

  function set_ad() {

    $(function() {

        var $defend = 0;
        var $attack = 0;

        var defendD = $(this).attr('name');

        var  defendD1 = $("#D1").is(':checked');
        var  defendD2 = $("#D2").is(':checked');
        var  defendD3 = $("#D3").is(':checked');
        var  defendD4 = $("#D4").is(':checked');

        if (defendD1) {$defend = 1;}
        if (defendD2) {$defend = 2;}
        if (defendD3) {$defend = 3;}
        if (defendD4) {$defend = 4;}

        if (defendD1 && defendD2) {$defend = 1;}
        if (defendD2 && defendD3) {$defend = 2;}
        if (defendD3 && defendD4) {$defend = 3;}
        if (defendD4 && defendD1) {$defend = 4;}
        if (defendD4 && defendD2) {$defend = 5;}
        if (defendD1 && defendD3) {$defend = 6;}

        var  attackA1 = $("#A1").is(':checked');
        var  attackA2 = $("#A2").is(':checked');
        var  attackA3 = $("#A3").is(':checked');
        var  attackA4 = $("#A4").is(':checked');
        if (attackA1) {$attack = 1;}
        if (attackA2) {$attack = 2;}
        if (attackA3) {$attack = 3;}
        if (attackA4) {$attack = 4;}
        if (botBattlePanel  == 0) {
          $("#attack").attr('value', $attack);
          $("#defend").attr('value', $defend);
        }
    });
  }

$(function() {
 $('.checkbox').live('click', function() {
    set_ad();
 });
});

function checkSubmit(form){
	set_ad();
	if (battleAutoSubmit && !checkForm(0)) {
	   if (newbatl) {
         document.getElementById('fight').click();
	   } else {
	     document.forms['bform'].submit();
	   }
	}
}

function checkAttack(obj){
	if (botBattle && !botBattlePanel) return attackCount;
	var ret = checkBlock(attackCount, 'A');
	if (ret == attackCount && obj)
		checkSubmit(obj.form);
	return ret;
}

function checkDefend(obj){
	if (botBattle && !botBattlePanel) return defendCount;
	var ret = checkBlock(defendCount, 'D');
	if (ret == defendCount && obj)
		checkSubmit(obj.form);
	return ret;
}

function checkBlock(maxCount, blockName){
	if (botBattle && !botBattlePanel) return 0;
	var count = 0;
	for (var i = 1; i <= blockCount; i++) {
	    var bl_name = blockName + '' + i;
		var block = document.getElementById(bl_name) ? document.getElementById(bl_name) : document.all[bl_name];

                if($('#' + bl_name).attr("checked") == 'checked') {
                    $('#' + bl_name).parent().addClass('selbl');
                } else {
                    $('#' + bl_name).parent().removeClass('selbl');
                }

		if (block && block.checked) {
			count++;
		}
	}
	for (var i = 1; i <= blockCount; i++) {
	    var bl_name = blockName + i;
		var block = document.getElementById(bl_name) ? document.getElementById(bl_name) : document.all[bl_name];

                if($('#' + bl_name).attr("checked") == 'checked') {
                    $('#' + bl_name).parent().addClass('selbl');
                } else {
                    $('#' + bl_name).parent().removeClass('selbl');
                }



		if (block) {
			if (count == maxCount) {
				block.disabled = !block.checked;
			} else {
                block.disabled = false;
			}
		}
	}
	return count;
}

function checkForm(displayError){
	if (botBattle && !botBattlePanel) return false;
	var aCnt = checkAttack();
	var dCnt = checkDefend();
	var error = [];
	var errorCount = 0;
	var diff = 0;
	var err = '';
	diff = attackCount - aCnt;
	if (diff) {
		errorCount++;
		switch (diff) {
			case 1:
				error[errorCount] = "���� ����";
				break;
			case 2:
				error[errorCount] = "��� �����";
				break;
			case 3:
				error[errorCount] = "��� �����";
				break;
		}
		if (aCnt) {
			error[errorCount] = '��� ' + error[errorCount];
		};
			}
	diff = defendCount - dCnt;

	if (diff) {
		errorCount++;
		switch (diff) {
			case 1:
				error[errorCount] = "���� ����";
				break;
			case 2:
				error[errorCount] = "��� �����";
				break;
			case 3:
				error[errorCount] = "��� �����";
				break;
		}
		if (dCnt) {
			error[errorCount] = '��� ' + error[errorCount];
		};
			}
	if (!botBattle && !(document.getElementById('A5').checked || document.getElementById('D5').checked)) {
		errorCount++;
		error[errorCount] = "�������";
	}

	switch (errorCount) {
		case 1:
			err = error[1];
			break;
		case 2:
			err = error[1] + ' � ' + error[2];
			break;
		case 3:
			err = error[1] + ', ' + error[2] + ' � ' + error[3];
			break;
	}

	if (err && displayError) {
		var block = document.getElementById('error');
		block.innerHTML = '���������� ������� ' + err + ' ��� ����';
	}
	return err ? true : false;
}

function botBattlePanelFirstDialog(nd) {
	var dialog = getDialog();

	if(!dialog) return false;

	var html = '' +
		'<div id="battle-controls-settings">' +
			'�� ������ ������� ���� �� ��������� ������� ��� � ���������:' +
			'<form id="bot_battle_panel_first_form">' +
				'<ul>' +
					'<li>' +
						'<label for="controls-simple">' +
							'<img src="http://img.blutbad.ru/i/battle/bot_battle_panel_simple_cut.jpg" />' +
						'</label>' +
						'<br />' +
						'<input checked="checked" id="controls-simple" name="bot_battle_panel" type="radio" value="simple" />' +
					'</li>' +
					'<li>' +
						'<label for="controls-original">' +
							'<img src="http://img.blutbad.ru/i/battle/bot_battle_panel_extended_cut.jpg" />' +
						'</label>' +
						'<br />' +
						'<input id="controls-original" name="bot_battle_panel" type="radio" value="extended" />' +
					'</li>' +
				'</ul>' +
			'</form>' +

			'�� ������ ������ ���������� �������� ������� � ����� ����������<br />' +
			'<input class="xbbutton" id="bot_battle_panel_first_ok" style="margin-top: 1em;" type="button" value="��" />' +
		'</div>';


	dialog.html(html);
	if($.browser.msie) {
		dialog.find('label').click(function() {
			$('#' + $(this).attr('for')).attr('checked', 'checked');
		});
	}

	dialog.dialog('option', 'title', '�������� ������ ������� ��� � ���������');
    dialog.dialog('option', 'width', '605');
    dialog.dialog('option', 'height', '260');
    dialog.dialog('option', 'left', Math.floor(screen.width / 2));
    dialog.dialog('option', 'top', Math.floor(screen.height / 2));

	dialog.bind('dialogclose', function() {
		$.post('settings_ajax.php',{
			cmd: 'bot_battle_panel',
			is_ajax: 1,
			nd: nd,
			set: jQuery('#bot_battle_panel_first_form :radio:checked').val()

		},function() {
			document.location.href = document.location.href;
		});
	});

	$("#bot_battle_panel_first_ok").click(function() {
		dialog.dialog('close');
	});

	setTimeout(function() {
		dialog.dialog('close');
	}, 60000);

	dialogOpen(dialog);
}
