$(function(){
	var headCheckbox = $('#complaints thead :checkbox');
	var bodyCheckboxes = $('#complaints tbody :checkbox');
	
	headCheckbox.click(function() {
		if(this.checked) {
			bodyCheckboxes.attr('checked', 'checked');
		} else {
			bodyCheckboxes.removeAttr('checked');
		}
	});

	bodyCheckboxes.click(function() {
		if(bodyCheckboxes.length == bodyCheckboxes.filter(':checked').length) {
			headCheckbox[0].checked = true;
		} else {
			headCheckbox[0].checked = false;
		}
	});
	
	if (pages > 1) {
		$('.pagination-container').pagination(pages, $.extend({}, paginationDefaults, {
			current_page: current_page,
			link_to: link_to
		}));
	}
	
	$('#complaints input.xbutton').click(function() {
		var comment = getDialog();
		comment.dialog('option', 'title', '����������� ��������������');
		
		var form = $.buildForm({
			'fields': [
				{
					'name': 'rows_on_page',
					'value': rows_on_page
				}, {
					'name': 'source',
					'value': source
				}, {
					'name': 'co_type',
					'value': co_type
				}, {
					'name': 'sort_by',
					'value': sort_by
				}, {
					'name': 'cs_status',
					'value': cs_status
				}, {
					'name': 'choosen_complaint',
					'value': $(this).data('id')
				},{
					'label': '�����������',
					'group': [
						{
							'autocomplete': 'off',
							'name': 'cp_comment',
							'size': 60,
							'type': 'text'
						}, {
							'type': 'submit',
							'value': 'OK'
						}
					]
				}
			],
			'url': '/proccomplaint.php'
		});

		comment.html(form)
		dialogOpen(comment);
	});
});