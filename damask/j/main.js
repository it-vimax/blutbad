var colorpicker;

function input_color_focus(){
	var color = $('#userbar_color').attr('value');
	if (!color || !RegExp('^\#[a-f0-9]{6}$').test(color)) 
		$('#userbar_color').attr('value', '');
}

function input_color_blur(){
	var color = $('#userbar_color').attr('value');
	if (!color || !RegExp('^\#[a-f0-9]{6}$').test(color)) 
		$('#userbar_color').attr('value', '<�� ���������>');
}

function toggleUserbar() {
	var userbar = $('#form-userbar');
	
	if(!userbar.is(':visible')) {
		$.cookie('form-userbar', 1, { expires: 365 });
	} else {
		$.cookie('form-userbar', null);
	}
	
	userbar.toggle();
	
	return false;
}

$(function() {

	if(swfobject.getFlashPlayerVersion().major == 0) {
		$('.no-flash').show();
	}

	var referralLink = $('#referral-link');

	if(referralLink.length) {
		if($.browser.msie && $.browser.versionX == 6) {
			referralLink.click(function() {
				window.clipboardData.setData('Text', 'http://blutbad.ru/r' + top.userId);
				alert('����������� ������ ���� ����������� � ����� ������.');
				return false;
			});
		} else {
			ZeroClipboard.setMoviePath('/f/ZeroClipboard.swf');
			var clip = new ZeroClipboard.Client();
			clip.glue(referralLink[0]);
			clip.setText('http://blutbad.ru/r' + top.userId);

			clip.addEventListener('complete', function(){
				alert('����������� ������ ���� ����������� � ����� ������.');
			});

			$(window).resize(function() {
				clip.reposition();
			});
		}
	}

	var htmlcodLink = $('#htmlcod-link');

	if(htmlcodLink.length) {
		if($.browser.msie && $.browser.versionX == 6) {
			htmlcodLink.click(function() {
				window.clipboardData.setData('Text', '<a alt="���������������!" href="http://blutbad.ru/r' + top.userId + '"><img src="http://img.blutbad.ru/i/userbar/' + top.userId + '.png" title="���������������!" /></a>');
				alert('����������� ������ ���� ����������� � ����� ������.');
				return false;
			});
		} else {
		   //	ZeroClipboard.setMoviePath('/f/ZeroClipboard.swf');
			var htmlcod_clip = new ZeroClipboard.Client();
			htmlcod_clip.glue(htmlcodLink[0]);
			htmlcod_clip.setText('<a alt="���������������!" href="http://blutbad.ru/r' + top.userId + '"><img src="http://img.blutbad.ru/i/userbar/' + top.userId + '.png" title="���������������!" /></a>');

			htmlcod_clip.addEventListener('complete', function(){
				alert('HTML-��� ��� ������� �� ���� ��������� ��� ���������� � ����� ������.');
			});

			$(window).resize(function() {
				htmlcod_clip.reposition();
			});
		}
	}

	var bbcodLink = $('#bbcod-link');

	if(bbcodLink.length) {
		if($.browser.msie && $.browser.versionX == 6) {
			bbcodLink.click(function() {
				window.clipboardData.setData('Text', '<a alt="���������������!" href="http://blutbad.ru/r' + top.userId + '"><img src="http://img.blutbad.ru/i/userbar/' + top.userId + '.png" title="���������������!" /></a>');
				alert('����������� ������ ���� ����������� � ����� ������.');
				return false;
			});
		} else {
		   //	ZeroClipboard.setMoviePath('/f/ZeroClipboard.swf');
			var htmlcod_clip = new ZeroClipboard.Client();
			htmlcod_clip.glue(bbcodLink[0]);
			htmlcod_clip.setText('[url=http://blutbad.ru/r' + top.userId + '][img]http://img.blutbad.ru/i/userbar/' + top.userId + '.png[/img][/url]');

			htmlcod_clip.addEventListener('complete', function(){
				alert('BB-��� ��� ������� � ����� ��� ���������� � ����� ������.');
			});

			$(window).resize(function() {
				htmlcod_clip.reposition();
			});
		}
	}

	$('#shout-timeout .elapsed-time').removeClass('invisible').each(function(){
		$(this).countdown({
			format: 'yodhms',
			layout: '{y<}{yn} {yl} {y>}{o<}{on} {ol} {o>}{d<}{dn} {dl} {d>}{h<}{hn} {hl} {h>}{m<}{mn} {ml} {m>}{sn} {sl}',
			onExpiry: function() {
				$('li.shout').removeClass('invisible');
				$('#shout-timeout').remove();
			},
			until: $(this).text()
		})
	});
	
	if ($('#colorpicker, #userbar_color').length == 2) {
		colorpicker = $.farbtastic('#colorpicker', '#userbar_color');
	}
	
	$('#shout').charCounter(null, {
		container: $('#shout-charcounter')
	});
	
	$('#shout-cities').charCounter(null, {
		container: $('#shout-cities-charcounter')
	});
	
	$('#autoanswer').charCounter(null, {
		container: $('#autoanswer-charcounter')
	});
	
	if($.cookie('form-userbar')) {
		$('#form-userbar').removeClass('invisible');
	}
});