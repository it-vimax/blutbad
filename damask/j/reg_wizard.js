var needCheck = true;
		
function resetCheck(){
	needCheck = false;
}
	
function checkAgree() {
	var cb = document.getElementById('agree');
	if (cb == null)
		return true;
	
	if (needCheck && !document.getElementById('agree').checked) {
		alert("������� ����� ������� ������� � ������ �������� ����!");
		return false;
	} else {
		return true;
	}
}

$(function() {
	var form = $('#register');
	var agree = $('#agree');
	
	var buttons = $('.button');
	if (buttons.length) {
		if (agree.length) {
			buttons.filter('.button-right').click(function() {
				if (!agree.filter(':checked').length) {
					alert("������� ����� ������� ������� � ������ �������� ����!");
				} else {
					form.submit();
				}
			});
		}
		
		if ($.browser.msie && $.browser.versionX == 6) {
			buttons.hover(
				function() {
					$(this).addClass('button-hover');
				}, 
				function() {
					$(this).removeClass('button-hover');
				}
			);
		}
	}
	
	var regCity = $('#reg_city');
	if(regCity) {
		var link = $('#select-city-link');
		var cities = $('#cities');
		
		if($.browser.msie && $.browser.versionX == 6) {
			$('#cities img').hover(
				function(){
					$(this).addClass('hover');
				},
				function(){
					$(this).removeClass('hover');
				}
			);
		}
		
		cities.dialog({
			autoOpen: false,
			beforeClose: function() {
				if($.browser.msie) {
					cities.find('img').css({
						visibility: 'hidden'
					});
				}
			},
			hide: 'fade',
			minHeight: 0,
			modal: true,
			open: function() {
				if($.browser.msie) {
					setTimeout(function() {
						cities.find('img').css({
							visibility: 'visible'
						});
					}, 400);
				}
			},
			show: 'fade',
			width: 'auto'
		});
		
		if($.browser.msie && $.browser.versionX < 8) {
			cities.dialog('option', 'width', cities.children().length * (100 + 24) )
		}
		
		link.click(function() {
			cities.dialog('open');
			return false;
		});
		
		$('#cities li img').click(function() {
			var $this = $(this);
			$('#cities li').removeClass('selected');
			$this.parent().addClass('selected');
			cities.dialog('close');
			link.text(this.alt);
			regCity[0].selectedIndex = $this.parent().index();
			regCity.change();
		});
	}
	
	var newname = $("#newname");
	if (newname.length) {
		var keyup_timeout;
		var ajaxCounter = 0;
		
		var validator = function(event, el) {
			if (!el) {el=this}
			clearTimeout(keyup_timeout);
			if (el.value.length>2) {
				var nn = el.value;
				keyup_timeout = setTimeout(function(){
					var flag = ajaxCounter;
					ajaxCounter++;
					$.get('reg.php', {
						cmd:"check_name",
						is_ajax: 1,
						name: nn
					}, function(data) {
						if(ajaxCounter < 2) {
							$('#warning_message').html(html_nick_error(data));
							$('#nick_variants').html(html_nick_variants(data));
							$('li.variant a').bind('click',function() { $(el).trigger('input'); });
						}
						ajaxCounter--;
					});
		
				}, 500);
			} else {
				$('#warning_message').html("");
			}
		}
	
		newname.bind('keyup input', validator);
		validator(null, newname[0]);
	}
	
});

function html_nick_error(data) {
	var arr = data.split(/\r?\n/);
	return arr[1] ? arr[1] : arr[0];
}

function html_nick_variants(data) {
	var arr = data.split(/\r?\n/);
	var variants = arr[1] ? arr[0].split(",") : null;
	if (!variants || !variants.length) return '';

	var value = '<div>�������:</div>';
	value += '<ul style="list-style-type: none;margin-top:0">';
	for (var i=0;i<variants.length;i++) {
		value += '<li class="variant"><a style="color:white;" href="#" onclick="$(\'#newname\').val(\''+variants[i]+'\'); return false;">'+variants[i]+'</a></li>';
	}
	value += '</ul>';
	return value;
}
