var accessway = true;

function sol(room){
	if (top.CtrlPress) {
		window.open('/online.html?room=' + top.currentCity + '_' + room + '&' + Math.random(), 'onlines', 'width=300,height=500,toolbar=no,location=no,scrollbars=yes,resizable=yes');
	} else if (accessway == true) {
		window.location.href = 'main.pl?map=' + room + '&' + Math.random();
	}
}

/**
 * ��������� "�����" � ���������
 */
function showStripes(listItems, start) {
	if(listItems && listItems.length) {
		if(start == undefined) {
			$(listItems).each(function(i) {
				if((i + 1) % 2) {
					$(this).removeClass('even').addClass('odd');
				} else {
					$(this).removeClass('odd').addClass('even');
				}
			});
		} else {
			var parity = ['even', 'odd'];
			$(listItems).each(function() {
				$(this).removeClass(parity[1 - start]).addClass(parity[start]);
				start = 1 - start;
			});
		}
	}
}

/**
 * �������������� ����� (��� ���)
 */
function formatWeight(weight) {
	if(weight != parseInt(weight)) {
		return String(weight.toFixed(3)).replace(/(\.0+|0+)$/, '');
	} else {
		return weight;
	}
}

/**
 * �������� ��������� �������
 * 
 * @param {String} id		���������� ����� ��������
 * @param {String} name		�������� ��������
 * @param {String} back		�������� ������� ������� � ���������
 */
function drop(el, id, name, back) {
    if (confirm('��������� ������� "' + name + '"?') && confirm('������� ����� ������, �� �������?')) {		
		$.post('/char.pl', {
			back: back,
			cmd: 'item.drop',
			is_ajax: 1,
			item: id,
			nd: nd
		}, function(json) {
			var notify = $.pnotify({
				pnotify_title: '��������!',
				pnotify_type: json.error ? 'error' : 'notice',
				pnotify_text: json.error ? json.error : json.message
			});
			notify.appendTo('#notifies').click(notify.pnotify_remove);
			
			if(json.message) {
				$(el).closest('.inventory-list-item').slideUp('fast', function() {
					var listItem = $(this);
					
					// ���������� ���������
					var weight = Number(listItem.find('.weight').text());
					var $cur_weight = $('.current-weight');
					$cur_weight.text(formatWeight(Number($cur_weight.text()) - weight));
					
					var $count = $('.total-count');
					$count.text(Number($count.text()) - 1);
					
					var start = listItem.hasClass('even') ? 0 : 1;
					var nextItems = listItem.nextAll(':not(.inventory-list-item-empty)');
					
					var list = listItem.parent();
					
					listItem.remove();
					
					if(list.children().length > 1) {
						showStripes(nextItems, start);
					} else {
						list.children().show();
					}
				});
			}
		}, 'json');
    }
}

/**
 * ����������� ��������� ��������� �� ���������
 * 
 * @param {String} 	type	
 * @param {String} 	num
 * @param {Integer} max		����������� ��������� ����� ������������� ���������
 * @param {String} 	name
 * @param {String} 	back	������� �������� ����� ������������ ��������
 */
function multi_drop(type, num, max, name, back, el) {
	var drop = getDialog();
	drop.dialog('option', 'title', '�������� "' + name + '"');
	
	var form = $.buildForm({
		'fields': [
			{ 'name': 'cmd',	'value': 'items.drop' },
			{ 'name': 'nd',		'value': nd},
			{ 'name': 'type',	'value': type },
			{ 'name': 'back',	'value': back },
			{ 'name': 'num',	'value': num },
			{
				'label': '����������',
				'group': [
					{
						'name': 'cnt',
						'size': 8,
						'type': 'text'
					}, {
						'click': function() {
							$('#cnt').val(max).focus();
						},
						'type': 'button',
						'value': '���'
					}
				]
			}, {
				'type': 'submit',
				'value': 'OK'
			}
		],
		'url': '/char.pl'
	}).submit(function() {
		var cnt = Number($(this).find('#cnt').val());
		
		if(cnt != NaN && cnt > 0) {
			if (confirm('�������� ����� �������, �� �������?')) {
				return true;
			} else {
				return false;
			}
		} else {
			cnt = cnt > max ? max : cnt;
			
			alert('����� ����� �� ���� �!');
			return false;
		}
	});
	
	form.ajaxForm({
		data: {
			is_ajax: 1
		},
		dataType: 'json',
		success: function(json) {
			drop.dialog('close');
			
			// �����������
			showNotification(json.message, json.error ? true : false);
			
			// ��������� �������
			var items = json.items;
			var listItem = $(el).closest('.inventory-list-item');
			if(items.length) {
				var durability = listItem.find('.durability');
				var fish_weight = listItem.find('.fish-weight');
				var valid = listItem.find('.valid');
				var weight = listItem.find('.weight');
				
				var data = {
					'durability': {},
					'fish_weight': {},
					'validdate': {},
					'weight': {}
				};
				
				var unique_counters = {
					'durability': 0,
					'fish_weight': 0,
					'validdate': 0,
					'weight': 0
				};
				
				for(var i = 0; i < items.length; i++) {
					var item = items[i];
					
					for(var key in data) {
						if(item[key]) {
							if(data[key][item[key]]) {
								data[key][item[key]]++;
							} else {
								data[key][item[key]] = 1;
								unique_counters[key]++;
							}
						}
					}
				}
				
				// ��.
				var max = listItem.find('.max').text();
				listItem.find('.max').text(items.length);
				var old_onclick = listItem.find('.multi_drop').attr("onclick");
				var new_onclick = old_onclick.replace("'"+max+"'","'"+items.length+"'");
				
				listItem.find('.multi_drop').attr("onclick",new_onclick);
				// �����
				var drop_weight = Number(weight.text());
				var total_weight = 0;
				for(var w in data['weight']) {
					total_weight += Number(w) * Number(data['weight'][w]);
					drop_weight -= Number(w) * Number(data['weight'][w]);
				}
				weight.text(formatWeight(total_weight));
				
				// ���������� ���������
				var $cur_weight = $('.current-weight');
				$cur_weight.text(formatWeight(Number($cur_weight.text()) - drop_weight));
					
				var $count = $('.total-count');
				$count.text(Number($count.text()) - (max - items.length));
				
				// �������������
				durability.children(':not(.label)').remove();
				
				if(unique_counters['durability'] > 1) { // ����� ������ � ����������� ����������
					var expander  = $('<img />', {
						'class': 'properties-expander',
						'src': imgURL + 'sb_plus.gif'
					});
					expander.appendTo(durability);
					
					// ���������� ������
					var ul = $('<ul />', {
						'class': 'invisible'
					});
					
					for(var value in data['durability']) {
						$('<li />', {
							'html': data['durability'][value] + ' &times; ' + value
						}).appendTo(ul);
					}
					ul.appendTo(durability);
					
					initInventoryPropertyExpander(expander);
				} else { // ���� �������� �������� � ���������� ������ ��������
					for(var value in data['durability']) {
						$('<span />', {
							text: value
						}).appendTo(durability);
					}
				}
				
				// ��� (����)
				if(unique_counters['fish_weight']) {
					fish_weight.children(':not(.label)').remove();
					
					if(unique_counters['fish_weight'] > 1) {
						// ����� ������ � ����������� ����������
						fish_weight.children('.label').css('font-weight', 'bold');
						var expander  = $('<img />', {
							'class': 'properties-expander',
							'src': imgURL + 'sb_plus.gif'
						});
						
						// ���������� ������
						var ul = $('<ul />', {
							'class': 'invisible'
						});
						var total_fish_weigh = 0;
						for(var mass in data['fish_weight']) {
							$('<li />', {
								'html': data['fish_weight'][mass] + ' &times; ' + mass + ' ��'
							}).appendTo(ul);
							total_fish_weigh += data['fish_weight'][mass] * mass;
						}
						
						$('<span />', {
							text: formatWeight(total_fish_weigh) + ' �� '
						}).appendTo(fish_weight)
						expander.appendTo(fish_weight);
						ul.appendTo(fish_weight);
						
						initInventoryPropertyExpander(expander);
					} else {
						// ���� �������� �������� � ���������� ������ ��������
						fish_weight.children('.label').css('font-weight', 'normal');
						for(var mass in data['fish_weight']) {
							$('<span />', {
								text: (Number(mass) * data['fish_weight'][mass]) + ' ��'
							}).appendTo(fish_weight);
						}
					}
				}
				
				// ����� ��
				valid.children(':not(.label)').remove();
				if(unique_counters['validdate'] > 1) {
					// ����� ������ � ����������� ����������
					valid.children('.label').css('font-weight', 'bold');
					
					var expander  = $('<img />', {
						'class': 'properties-expander',
						'src': imgURL + 'sb_plus.gif'
					});
					expander.appendTo(valid);
					
					// ���������� ������
					var ul = $('<ul />', {
						'class': 'invisible'
					});
					
					for(var date in data['validdate']) {
						$('<li />', {
							'html': data['validdate'][date] + ' &times; ' + date
						}).appendTo(ul);
					}
					ul.appendTo(valid);
					
					initInventoryPropertyExpander(expander);
				} else {
					// ���� �������� �������� � ���������� ������ ��������
					valid.children('.label').css('font-weight', 'normal');
					for(var date in data['validdate']) {
						$('<span />', {
							text: date
						}).appendTo(valid);
					}
				}
			} else {
				listItem.slideUp('fast', function() {
					// ���������� ���������
					var weight = Number(listItem.find('.weight').text());
					var $cur_weight = $('.current-weight');
					$cur_weight.text(formatWeight(Number($cur_weight.text()) - weight));
					
					var $count = $('.total-count');
					$count.text(Number($count.text()) - Number(listItem.find('.max').text()));
					
					var start = listItem.hasClass('even') ? 0 : 1;
					var nextItems = listItem.nextAll(':not(.inventory-list-item-empty)');
					
					var list = listItem.parent();
					
					listItem.remove();
					
					if(list.children().length > 1) {
						showStripes(nextItems, start);
					} else {
						list.children().show();
					}
				});
			}
		}
	});
	
	drop.html(form);
	dialogOpen(drop);
	
	return false;
}

function statUp(name, ability, nd) {
	if(confirm('�� ������������� ������ ��������� ' + name + '?')) {
		location.href = '/char.pl?cmd=ability.up&nd=' + nd + '&ability=' + ability + '&' + Math.random();
	}
}

var groups_nums = {
	'item': 0,
	'runes': 1,
	'animals': 2,
	'stuff': 3,
	'misc': 4
}

function put_off_error(slot, message) {	
	if(message)
		pnotify(message, true);
	
	overlays.hide();
}

function put_off_item(img, slot_num) {
	if(IE6) {
		overlays.each(function() {
			$(this).width( $(this).parent().innerWidth() );
			$(this).height( $(this).parent().innerHeight() );
		});
	}
	overlays.show();
	
	var $img = $(img);
	
	// ������ ��� AJAX �������
	var url = '/char.pl';
	var data = {
		back: $('.ui-tabs-selected a').data('group'),
		cmd: 'item.unequip',
		is_ajax: 1,
		item: slot_num,
		nd: nd
	}
	
	var type_num = $(img).data('type-num');
	var type = type_num.split('-')[0];
	var group;
	
	if( (type >= 1 && type <= 20) || type == 23) {
		group = 'items';
	} else if( type == 21 || type == 50 ) {
		group = 'runes';
	} else if( (type >= 24 && type <= 27) || (type >= 36 && type <= 38) || type == 60 || type == 63 || type == 91 || type == 92 ) {
		group = 'stuff';
	}
	
	var slot = $img.parent();
	if (slot.hasClass("tooltip-next")) {
		slot = slot.parent();
	}

	$.post(url, data, function(json) {
		if(json) {
			if(json.error) {
				put_off_error(slot, json.error);
			} else if(json.message) {
				set_stats(json);
				
				if(group == 'items' || group == 'runes') {
					need_refresh[0] = true;
					need_refresh[1] = true;
				}
				
				// ��������� ������������� �������
				$group = $('#inventory-' + group);
				
				if($group.length) {					
					if($group.is(':visible')) {
						load_tab(selected_ui, true);
					} else {
						need_refresh[groups_nums[group]] = true;
						overlays.hide();
					}
				} else {
					overlays.hide();
				}
				
				slot.addClass(slot.data('slot-name') + '-empty').empty();
			}
		} else {
			put_off_error(slot, '�������� ����� �������');
		}
	}, 'json');
	
	
	return false;
	
	location.href = '/char.pl?cmd=item.unequip&nd=' + nd + '&item=' + slot_num + '&back=' + $('.ui-tabs-selected a').attr('href').replace(/\#([a-z]+)-\d+/, '$1').replace('is_ajax=1', '') + '&' + Math.random();
}

function get_inventory_item(data) {
	
}

function load_tab(ui, silent) {
	if(!silent)
		$(ui.panel).empty().append(loader);
	
	$.post('/char.pl', {
		back: $(ui.tab).data('group'),
		is_ajax: 1,
		nd: nd
	}, function(html) {
		$(ui.panel).html(html);
		initInventoryPropertyExpander($(ui.panel).find('.properties-expander'));
		need_refresh[ui.index] = false;
		
		overlays.hide();
	});
}

function load_item(ui, el) {
	$.post('/char.pl', {
		back: $(ui.tab).data('group'),
		is_ajax: 1,
		nd: nd
	}, function(html) {
		var id = el[0].id;
		var item = $(html).find('#' + id);
		if(item.length) {
			el.html(item.html());
			initInventoryPropertyExpander(el.find('.properties-expander'));
		}
		need_refresh[ui.index] = false;
		
		el.slideDown('fast')
		
		overlays.hide();
	});
}

function put_on_error(el, message) {
	pnotify(message, true);
	
	var start = el.hasClass('even') ? 0 : 1;
	showStripes(el.nextAll(':not(.inventory-list-item-empty)'), 1 - start);
	
	el.slideDown('fast');
	
	overlays.hide();
}

/*
 * AJAX ��������� ���������
 */
function put_on_item(item_id, slot_type, link) {
	// ���� �������� �� ���������
	var char_slot = $('ul.char-items > .' + slot_type + ':empty:first');
	// ���� ������� ����� ��� - ���� ������
	if(!char_slot.length)
		char_slot = $('ul.char-items > .' + slot_type + ':first');
	
	if(char_slot.length) {
		//char_slot.addClass('loading');
		if(IE6) {
			overlays.each(function() {
				var $this = $(this);
				$this.width($this.parent().innerWidth());
				$this.height($this.parent().innerHeight());
			});
		}
		overlays.show();
		// ������� ������ ��������� li
		var el = $(link).closest('.inventory-list-item');
		var is_stack = Number(el.data('stack'));
		var count = 1;
		if(is_stack)
			count = Number(el.find('.number .max').text());
		
		function process() {
			// ����� �������������� "�����" ��� ������
			var start = el.hasClass('even') ? 0 : 1;
			showStripes(el.nextAll(':not(.inventory-list-item-empty)'), start);
			
			/*$.manageAjax.add('inventory', {
				data: {
					back: selected_group,
					cmd: 'item.equip',
					is_ajax: 1,
					is_stack: is_stack,
					item: item_id,
					nd: nd
				},
				dataType: 'json',
				success: function(json) {
					put_on_item_callback(json, el, char_slot, is_stack, count);
				},
				type: 'POST',
				url: '/char.pl'
			});*/
			
			// ���������� ������ �� ������
			$.post('/char.pl', {
				back: selected_group,
				cmd: 'item.equip',
				is_ajax: 1,
				is_stack: is_stack,
				item: item_id,
				nd: nd
			}, function(json) {
				put_on_item_callback(json, el, char_slot, is_stack, count);
			}, 'json');
		}
		
		if(count == 1) {
			el.slideUp(process);
		} else {
			process();
		}
		
		return false;
	}
}

function put_on_item_callback(json, el, char_slot, is_stack, count) {
	//char_slot.removeClass('loading');
	if(json) {
		// ������� ������ ������ �� �����-�� �������
		if(json.error) {
			put_on_error(el, json.error);
		} else if(json.message) {
			char_slot.removeClass(char_slot.data('slot-name') + '-empty').empty();
			
			// ���� � ���������
			var slot = is_stack ? el.find('.slot').clone() : el.find('.slot');
			// �������� �������� � ���������
			var description = is_stack ? el.find('.item-description').clone() : el.find('.item-description');
			if(is_stack) {
				var durability = description.find('li.durability');
				if(durability.length && durability.children('.properties-expander').length) {
					durability.children(':not(.label)').remove();
					$('<span />', {
						text: json.items[0].durability
					}).appendTo(durability);
				}
				
				var valid = description.find('li.valid');
				if(valid.length && valid.children('.properties-expander').length) {
					valid.children(':not(.label)').remove();
					$('<span />', {
						text: json.items[0].validdate
					}).appendTo(valid);
				}
			}
			
			set_stats(json);
			
			// �������� �������� �������� ��� ���������
			var hidden_desc = $('<div />', {
				"class": "invisible",
				"html": description
			});
			
			// ��������� ������� � �������� � ����
			char_slot.append(slot.contents()).append(hidden_desc);
			
			// ���������� ���������
			char_slot.children('img.thumb').addClass('active').tooltip($.extend({}, tooltipDefaults, {
				bodyHandler: function(){
					return $(this).nextAll('.invisible').html();
				}
			})).removeAttr('onclick').click(function() {
				put_off_item(this, char_slot.data('slot'));
			});
			
			var list = el.parent();
			
			// ������� �������
			if(!is_stack || count == 1)
				el.remove();
			
			if(list.children().length == 1) {
				list.children().show();
			}
			
			if(selected_group == 'items') {
				need_refresh[0] = true;
				need_refresh[1] = true;
			}
			need_refresh[inventory.tabs('option', 'selected')] = true;
			
			// ������������ ������� �������
			load_tab(selected_ui, true);
		} else { // ����������� ������
			put_on_error(el, json.error);
		}
		
		if(json.tutorial) {
			top.highlightAllTutorialElements(json.tutorial);
		}
	} else { // ����������� ������
		put_on_error(el, json.error);
	}
}

function get_sign(value) {
	return value > 0 ? '+' : value < 0 ? '-' : '';
}

function set_stats( json ) {
	if(json.exp_kf) {
		$('#exp_kf').text(json.exp_kf + '%');
	}
	
	var hppw = json.hppw;
	
	$('.hp-pw').HpPw({
		hp: {
			current: hppw.chp,
			max: hppw.mhp,
			speed: hppw.shp
		},
		pw: {
			current: hppw.cpw,
			max: hppw.mpw,
			speed: hppw.spw
		}
	});
	
	// ������ ������������ ���
	$('.max-weight').text(json.max_weight);
	
	var stats = json.allstats;
	var stats_ids = [ 'str', 'dex', 'suc', 'end', 'int', 'wis' ];
	for(var i in stats_ids) {
		var stat_id = stats_ids[i];
		
		$('#' + stat_id).text(stats[stat_id]);
		
		//stat_items = stats[stat_id + '_items'];
		//stat_tr = stats[stat_id + '_tr'];
		
		var add = stats[stat_id] - stats[stat_id + '_base'];
		
		if(add) {
			$('#' + stat_id + '_base').text(stats[stat_id + '_base']);
			
			//var add = stat_items - stat_tr;
			$('#' + stat_id + '_add').text(get_sign(add) + ' ' + Math.abs(add));
			
			$('#' + stat_id + '_info').removeClass('invisible');
		} else {
			$('#' + stat_id + '_info').addClass('invisible');
		}
	}
	
	var mf = json.allmf;
	for(var id in mf) {
		var value = mf[id];
		var text = Math.round(Math.abs(value) * 100) / 100;
		
		if(id != 'uron_min' && id != 'uron_max') {
			text = get_sign(value) + text;
		}
		
		$('#' + id).text(text);
	}
	
	var actives = json.allactives;
	
	if(actives.length) {
		for(var i = 0; i < actives.length; i++) {
			var tbody = $('#active-states tbody');
			switch(actives[i].type) {
				case 'invisibility':
					$('#invisibility').remove();
					
					var click = function() {
						commonConfirm('/char.pl', {
							cmd: 'invisibility.off',
							nd: nd
						}, '����� �����������?');
						
						return false;
					};
					
					var tr = $('<tr />', {
						"id": "invisibility"
					}).appendTo(tbody);
					
					var td_img = $('<td />', {
						"class": "img"
					}).append(
						$('<img />', {
							"alt": "�����������",
							"class": "button",
							"click": click,
							"src": imgURL + "rune/invisible.gif",
							"title": "����� �����������"
						})
					).appendTo(tr);
					
					var td_desc = $('<td />').appendTo(tr);
					
					td_desc.append('����������� (' + (actives[i].elapsed ? '<span class="elapsed-time">' + actives[i].elapsed + '</span>' : '����������') + ')<br />')
					.append($('<a />', {
						"href": "#",
						"css": {
							"font-weight": "bold"
						},
						"click": click,
						"text": "[�����]"
					}));
					
					$('#active-states').removeClass('invisible');
					
					tr.find('.elapsed-time').removeClass('invisible').each(function(){
						$(this).countdown({
							format: 'yodhms',
							layout: '{y<}{yn} {yl} {y>}{o<}{on} {ol} {o>}{d<}{dn} {dl} {d>}{h<}{hn} {hl} {h>}{mn} {ml}',
							onExpiry: function() {
								var tr = $(this).closest('tr');
								
								if(!tr.siblings().length) {
									$('#active-states').addClass('invisible');
								}
								tr.remove();
							},
							until: $(this).text()
						})
					});
					
					break;
				
				default:
					break;
			}
		}
	}/* else {
		$('#active-states').addClass('invisible').children('tbody').empty();
	}*/
}

var overlays;
var IE6 = $.browser.msie && $.browser.versionX == 6;

$(function(){
	overlays = $('#char-overlay, #inventory-overlay');
	
	var kitDialog = $('#kit-dialog');
	if (kitDialog.length) {
		// ������ - �������� ���������
		var kit = $('#kit');
		
		// �������������� ���������� ���� ���������� ���������
		kitDialog.dialog({
			autoOpen: false,
			minHeight: 0,
			open: function(event, ui){
				kit.focus();
			},
			width: 'auto'
		});
		
		// ������� �� ����� �� ������ � ���������� ���������
		$('#save-kit-link').click(function(){
			dialogOpen(kitDialog);
			return false;
		});
		
		// AJAX-���������� ���������
		kitDialog.find('form').ajaxForm({
			data: {
				ajax: true,
				is_ajax: 1
			},
			dataType: 'json',
			resetForm: true,
			success: function(json) {
				showNotification(json.message, json.error ? true : false);
				
				if(!json.error) {
					kitDialog.dialog('close');
					
					var list = $('.stat.kits tbody ul');
					
					var isKitExists = false;
					
					list.find('.dress-up span').each(function() {
						if($(this).text() == json.name) {
							isKitExists = true;
						}
					});
					
					if(!isKitExists) {
						li = '' +
							'<li>' +
								'<img alt="������� ��������" class="delete" onclick="dropcomp(' + json.kit + ', \'' + json.name + '\', this);" src="' + imgURL + 'x.gif' + '" title="������� ��������" /> ' +
								'<a class="dress-up" href="/char.pl?cmd=complect.equip&amp;nd=' + json.nd + '&amp;kit=' + json.kit + '&amp;' + Math.random() + '">' +
									'������ &laquo;<span>' + json.name + '</span>&raquo;' + 
								'</a>' +
							'</li>';
						list.append(li);
						list.children(':first').addClass('invisible');
					}
				}
			}
		});
	}
	
	if ($('.characteristics').length && $.ui && $.ui.sortable) {
		$('.characteristics').sortable({
			axis: 'y',
			cancel: '.toggle',
			handle: '.block',
			items: '.sortable',
			update: function(event, ui){
				var data = {
					cmd: 'block.shuffle',
					is_ajax: 1,
					nd: nd
				}
				
				$('.characteristics .sortable').each(function(i){
					data[ $(this).data('type') ] = i + 1;
				});
				
				$.post('/char.pl', data);
			}
		});
		
		$('.characteristics .toggle').click(function(){
			var $this = $(this);
			var table = $this.closest('table.stat');
			//var blockName = table.find('.block img.move').attr('data');
			var blockName = table.parent().data('type');
			
			var imgToggle = table.find('.block img.toggle');
			var spanToggle = table.find('.block span.toggle');
			
			var closed = table.children('tbody').css('display') == 'none' ? 1 : 0;
			
			if (closed) {
				imgToggle[0].src = imgURL + 'sb_minus.gif';
				imgToggle[0].title = '��������';
			} else {
				imgToggle[0].src = imgURL + 'sb_plus.gif';
				imgToggle[0].title = '����������';
			}
			
			$.post('/char.pl', {
				block: blockName,
				cmd: 'block.toggle',
				toggle: 1 - closed
			});
			
			table.children('tbody, tfoot').toggle();
		});
	}
	
	initInventoryPropertyExpander();
	
	//create an ajaxmanager named someAjaxProfileName
	/*if($.manageAjax) {
		$(document).bind('inventoryAjaxStart', function(e){
			console.log('inventoryAjaxStart');
		});
		
		$(document).bind('inventoryAjaxStop', function(e, data){
			console.log('inventoryAjaxStop');
		});
		
		$.manageAjax.create('inventory', {
			cacheResponse: false,
			preventDoubleRequests: false,
			queue: true
		});
	}*/
});
