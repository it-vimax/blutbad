function word_end(num){
	if (num > 10 && num < 20)
		return "��";
	if ((num % 10) == 0)
		return "��";
	if ((num % 10) == 1)
		return "�";
	else if ((num % 10) < 5)
		return "�";
	else
		return "��";
}

function html_current_answers(){
	var answersCol = $('#answers_area').empty();

	if (answers) {
		answers.reverse();

		var answersList = $('<ul />').appendTo(answersCol);
		for (var i = 0; i < answers.length; i++) {
			var value = '' +
				'<b class="monospace">' +
					answers[i].answer +
				'</b> ' +
				'<i>(' + answers[i].time + ' �.)</i>';

			answersList.append('<li>' + value + '</li>');
		}
	}
}

function html_hint(){
	$('#hint_area').html('<span class="monospace">' + hint + '</span> (����: <b>' + hint.length + '</b>)');
}

/**
 * ��������� ���� ���������
 */
function updateHintCol(){
	if (hints) {
		$('#hint-col').show();

		if (hints.length) {
			hints.reverse();

			/**
			 * ����� �����
			 */
			var total_chars = hint.length;

			/**
			 * ������ ���� �����
			 */
			var hint_array = hint.split('');

			/**
			 * ����� �������� ����������� ����
			 */
			var known_chars = 0;
			for (var i = 0; i < hint_array.length; i++) {
				if (hint_array[i] != "*") {
					known_chars++;
				}
			}

			if (((total_chars >= 3) && (total_chars - known_chars <= 2)) || (total_chars - known_chars <= 1)) {
				$('#take_hint').hide();
				$('#no-hints').show();
			} else {
				for (var i = 0; i < hints.length; i++) {
					var price = Number(free_hint) ? '���������' : hints[i].price + ' ��.';

					if (hints[i].code == 'one_specific_letter') {
						var price_first = Number(free_hint) ? '���������' : hints[i].price_first + ' ��.';

						$('#price-selected').text(Number(free_hint) ? '���������' : '������ - ' + price_first + ', ��������� - ' + price);

						var letters = $('#letter-selector').empty();

						for (var j = 0; j < hint_array.length; j++) {
							var li = $('<li />').appendTo(letters);
							if (hint_array[j] == '*') {
								$('<input class="xbbutton" onclick="take_hint(\'' + hints[i].code + '\',' + (j + 1) + ');" style="width:' + (j >= 9 ? 27 : 20) + 'px;" type="button" value="' + (j + 1) + '" />').appendTo(li);
							} else {
								$('<input class="xbbutton" disabled="disabled" style="width:' + (j >= 9 ? 27 : 20) + 'px;" type="button" value="' + (j + 1) + '" />').appendTo(li);
							}
						}
					} else {
						$('#letter-random').html('<input class="xgbutton" id="take_hint_' + hints[i].code + '" onclick="take_hint(\'' + hints[i].code + '\');" type="button" value="' + hints[i].desc + '" /> ' + price);
						//$('#price-random').text(price);
					}
				}

				$('#money').text(money);

				$('#take_hint').show();
				$('#no-hints').hide();
			}
		} else {
			$('#take_hint').hide();
			$('#no-hints').show();
		}
	} else {
		$('#hint-col').hide();
	}
}

function html_timer(){
	var value = '';

	var timer_secs = Math.floor(round_end_timer / 1000);

	if (timer_secs == 0) {
		value += '<span class="attention">����������� ���������...</span>';
	} else {
		if (timer_secs < 10) {
			value += '<span class="attention">';
		}

		value += timer_secs + ' ���.';

		if (timer_secs < 10) {
			value += '</span>';
		}
	}

	$('#timer_area').html(value);
}

function accept_answer(event){
	if (round_end_timer < 1000) {
		return true;
	}

	var keynum;

	if (window.event) {
		keynum = event.keyCode;
	} else if (event.which) {
		keynum = event.which;
	}

	if (keynum != 13) {
		return;
	}

	_accept_answer();
}

function _accept_answer(){
	var answer = $("#input_answer").val();

	if (!answer) {
		return;
	}

	if (answer.length > 100) {
		showError("������ �������� ������: �������� ������� �����");
		return false;
	}

	if (top.ChatTrans) {
		answer = transliterate(answer);
	}

	for (var i = 0; i < answers.length; i++) {
		if (answers[i].answer.toLowerCase() == answer.toLowerCase()) {
			$("#input_answer").val('').filter(':visible').focus();
			return true;
		}
	}

	var current_round_saved = current_round;

	$.ajax({
		cache: false,
		data: {
			answer: answer,
			cmd: "answer",
			current_round: current_round,
			nd: mynd
		},
		error: accept_answer_failure,
		success: function(data, textStatus, XMLHttpRequest) {
			accept_answer_success(current_round_saved, XMLHttpRequest);
		},
		type: 'post',
		url: '/victorina.php?' + Math.random()
	});

	$("#input_answer").val('').focus();
}

function accept_answer_success(current_round_saved, transport){
	if (current_round == current_round_saved) {
		var result = transport.responseText.split(" ");

       // console.log('accept_answer_success ' + transport.responseText );

		if (result[0] != "OK") {
			showError("������ �������� ������: " + transport.responseText);
		} else {
			var time = result[1];
			result.splice(0, 2);
			var answer = result.join(" ");
			answers.push({
				answer: answer,
				time: time
			});

			html_current_answers();
		}
	}
}

function accept_answer_failure(){
	showError("������ �������� ������. ��������, ������ � ��������� ����� ����������");
}

/**
 * ���������� ��������� �� ������
 *
 * @param {String} text	����� ���������
 */
function showError(text){
	var notify = $.pnotify({
		pnotify_title: '��������!',
		pnotify_text: text,
		pnotify_type: 'error',
		pnotify_width: '250px'
	});
	notify.appendTo('#notifies');
}

function find_hint_info(hint){
	if (hints) {
		for (var i = 0; i < hints.length; i++) {
			if (hints[i].code == hint) {
				return hints[i];
			}
		}
	} else {
		return null;
	}
}

function take_hint(code,param){
	if (!param) param = '';
	var el = $("#take_hint_" + code + "_param");

	$.ajax({
		data: {
			cmd: "take_hint",
			current_round: current_round,
			hint: code,
			param: ((el && el.val()) ? el.val() : param),
			nd: mynd
		},
		error: take_hint_failure,
		success: function(data) {
			take_hint_success(current_round, data);
		},
		type: 'post',
		url: '/victorina.php?' + Math.random()
	});

	for (var i = 0; i < hints.length; i++) {
		$("#take_hint input").attr('disabled', true);
	}

	$("#input_answer").focus();
}

function take_hint_success(current_round_saved, transport){
	if (!hints) {
		return false;
	}

	if (current_round != current_round_saved) {
		return false;
	}

	for (var i = 0; i < hints.length; i++) {
		$("#take_hint input").attr('disabled', false);
	}

	var result = transport.split(" ");
    // console.log('take_hint_success "' + transport+'"' );
	if (result[0] != "OK") {
		alert("������ ��������� ���������: " + transport);
		return false;
	}

	result.splice(0, 1);
	var take_hint_info = eval('(' + result.join(' ') + ')');
	hint = take_hint_info.hint;
	hints = take_hint_info.hints;
	money = take_hint_info.money;

//   console.log('take_hint_info ' + JSON.stringify( take_hint_info ) );

	if (Number(free_hint)) {
		free_hint--;
	}

	html_hint();
	updateHintCol();
}

function take_hint_failure(){
	if (hints) {
		for (var i = 0; i < hints.length; i++) {
			$("#take_hint input").attr('disabled', false);
		}
		alert("������ ��������� ���������");
	}
}

function update_round_timer(){
	var current_time = (new Date()).getTime();
	if (current_question && round_end_timer > 0) {
		round_end_timer -= current_time - last_update_timer_time;
		if (round_end_timer < 0) {
			round_end_timer = 0;
		}

		html_timer();
	}

	var interval;

	if (!current_question) {
		interval = 15000;
	} else {
		interval = 1000;
	}

	last_update_timer_time = current_time;
	timeout = setTimeout(update_round_timer, interval);

	if (round_end_timer == 0 && !updating_round) {
		update_round();
	}

	if (current_question && round_end_timer < 1000) {
		if (hints) {
			$("#take_hint input").attr('disabled', true);
		}
		if ($("#accept_answer_button").length) {
			$("#accept_answer_button")[0].disabled = true;
		}
	}
}

function update_round(){
	updating_round = true;

//   console.log('update_round current_round ' + current_round + ' current_question: ' + current_round );
//   console.log('');

	$.ajax({
		cache: false,
		data: {
			cmd: 'update_round',
			current_round: current_round,
			current_question: (current_question ? 1 : 0)
		},
		error: update_round_failure,
		success: function(data, textStatus, XMLHttpRequest) {
			update_round_success(XMLHttpRequest);
		},
		type: 'post',
		url: '/victorina.php?' + Math.random()
	});
}

function update_round_success(transport){
	updating_round = false;
	var result = transport.responseText.split(" ");
 //  console.log('round_info responseText ' + result );
 //  console.log('');

	if (result[0] != "OK") {
		return;
	}

	result.splice(0, 1);

	var round_info = eval('(' + result.join(" ") + ')');

 //  console.log('round_info ' + JSON.stringify( round_info ) );
 //  console.log('');

	if (round_info.current_round == current_round && current_question) {
		return;
	}

	if (!current_question && !round_info.current_question) {
		return;
	}

	current_round = round_info.current_round;
	last_round_results = round_info.last_round_results;

	if (round_info.money)
		money = round_info.money;
	if (Number(round_info.last_round_score)) {
		current_score += Number(round_info.last_round_score);
		week_score += Number(round_info.last_round_score);
		updateScores();
	}

	users = round_info.users;
	updatePlayersInfo(users);

	if (member && !round_info.member) {
		document.location.href = "?" + Math.random();
		return;
	}

	var resume = false;
	if (!current_question && round_info.current_question) {
		resume = true;
	}
//   console.log('current_question ' + round_info.current_question + ' current_round ' + current_round + ' current_question ' + current_question);

	if (!round_info.current_question) {
		current_question = '';

		$("#round_area").addClass("invisible");
		$("#pause_area").removeClass("invisible");

		last_update_timer_time = (new Date()).getTime();
		clearTimeout(timeout);
		timeout = setTimeout(update_round_timer, 15000);
	} else {
		$("#pause_area").addClass("invisible");

		hints = round_info.hints;
		hint = round_info.hint;
		free_hint = Number(round_info.free_hint);
		current_round = round_info.current_round;
		current_question = round_info.current_question;
		round_end_timer = round_info.round_end_timer * 1000;
		answers = round_info.answers;

		$("#round-num").text(current_round);

		$("#question_img")
			.attr('src', imgURL + 'null.gif')
			.attr('height', round_info.question_height)
			.attr('width', round_info.question_width)
			.attr('src', '/victorina.php?cmd=question_img&' + Math.random());

		if (member) {
			html_hint();
			html_current_answers();
		}

		html_timer();

		if (member) {
			updateHintCol();
			$("#accept_answer_button")[0].disabled = false;
			$("#input_answer").val("");
		}

		$("#round_area").removeClass("invisible");

		if (member) {
			setTimeout(function() {
				$('#input_answer:visible').focus();
			}, 10);
		}

		last_update_timer_time = (new Date()).getTime();
		clearTimeout(timeout);
		timeout = setTimeout(update_round_timer, 1000);
	}

	if (!resume) {
		showLastRound();
	}
}

function update_round_failure(){
	updating_round = false;
}

function enter(){
	submit_form({
		cmd: "enter"
	});
}

function quit(){
	submit_form({
		cmd: "quit"
	});
}

function submit_form(params, action) {
	var form = document.createElement('form');
	form.setAttribute("method", "post");
	if (action) {
		form.setAttribute("action", action);
	}
	for (var name in params) {
		var element = document.createElement("input");
		element.setAttribute("type", "hidden");
		element.setAttribute("name", name);
		element.setAttribute("value", params[name]);
		form.appendChild(element);
	}
	document.body.appendChild(form);
	form.submit();
}

var last_update_timer_time = (new Date()).getTime();
var updating_round = false;

/**
 * ������������ ����� ������������ ������� ��� ��������
 */
var maxVisiblePlayers = 20;

/**
 * ��������� ���������� �� ������� � �� �����
 *
 * @param {Object} users
 */
function updatePlayersInfo(users) {
	$('#players-number').text(users ? users.length : 0);

	var players = $('#players').empty();

	if (!users || !users.length) {
		players.text('���');
	} else {
		for (var i = 0; i < users.length; i++) {
			var user = users[i];                                                                                                                // + (user.member ? ' (' + Number(user.week_score) + ')' : '')
			var scores = '<b class="scores ' + (i < maxVisiblePlayers  ? '' : 'invisible') + '">&mdash; <span class="nowrap">' + Number(user.score) + ' (' + Number(user.week_score) + ')' + (i < users.length - 1 ? ',' : '') + '</span></b> ';

			// ������ ��� ������
			if (user.invisible) {
				var player = getNick({
					invisible: true,

					nickClass: i < maxVisiblePlayers  ? 'nick' : 'nick invisible'
				});
			} else if (user.user) {
				var player = getNick({
					align: users[i].align,
					guild: users[i].guild ? {
						img: users[i].guild,
						name: users[i].guildname
					} : '',
					clan: users[i].klan ? {
						img: users[i].klan,
						name: users[i].klanname
					} : '',
					name: users[i].user,
					level: users[i].level,
					sex: users[i].sex,

					nickClass: i < maxVisiblePlayers  ? 'nick' : 'nick invisible'
				});
			}

			// ��������� ���� ������
			players.append(player).append(' ').append(scores);
		}

		if(users.length > maxVisiblePlayers) {
			var more = $('<span />', {
				click: function() {
					players.children('.invisible').removeClass('invisible');
					$(this).remove();
				},
				css: {
					cursor: 'pointer'
				},
				html: '[&hellip;]'
			});

			players.append(more);
		}
	}
}

function updateScores() {
	$('#scores').text(current_score + (member ? ' (' + Number(week_score) + ')' : ''));
}

/**
 * ���������� ���������� � ��������� ������
 */
function showLastRound(){
	if (last_round_results) {
		var round = last_round_results;

		var value = '';

		value +=
			'<div class="block recent-question" style="background-color: #D5D8DF; border-color: #7F9DB9;">' +
				'<fieldset>' +
					'<legend>��������� ����� � ' + round.round + '</legend>' +

					'<table style="float: left;">' +
						'<tbody>' +
							'<tr>' +
								'<th>������:</th>' +
								'<td>' + round.question + '</td>' +
							'</tr>' +
							'<tr>' +
								'<th>�����:</th>' +
								'<td>' + round.answer + '</td>' +
							'</tr>' +
							'<tr>' +
								'<th>�������� ���������:</th>' +
								'<td class="right-answers">';

		if (round.answers.length == 0) {
			value += 				'�����';
		} else {
			for (var i = 0; i < round.answers.length; i++) {
				var answer = round.answers[i];

				value += 			'<b' + (current_user == answer.user ? ' class="mine"' : '') + '>' +
										answer.user +
									'</b>' +
									' (<i>' + answer.time + ' c.</i>)' +
							 		(answer.score > 0 ? ' - <i>' + answer.score + '</i> ���' + word_end(answer.score) : '') +
									(i < round.answers.length - 1 ? ', ' : '');
			}
		}

		value += '' +
								'</td>' +
							'</tr>' +
							'<tr>' +
								'<th>�������� �����������:</th>' +
								'<td class="wrong-answers">';

		if (!round.wrong_answers || round.wrong_answers.length == 0) {
			value += 				'�����';
		} else {
			for (var i = 0; i < round.wrong_answers.length; i++) {
				var answer = round.wrong_answers[i];

				value += 			'<b' + (current_user == answer.user ? ' class="mine"' : '') + '>' +
						 				answer.user +
									'</b>' +
									' (<i>' + answer.time + ' c.</i>)' +
						 			' - <i>' + answer.answer + '</i>' +
						 			' <a href="#" onclick="complaintAnswer('+round.round+','+i+'); return false;" id="ComplaintLink_'+round.round+'_'+i+'">(������������)</a>' +
									(i < round.wrong_answers.length - 1 ? ', ' : '');
			}
		}

		value += 				'</td>' +
							'</tr>' +
						'</tbody>' +
					'</table>' + (member ? ' <div style="float:right;"><input type="button" class="xbbutton" onclick="complaintQuestion('+round.round+'); return false;" id="ComplaintLink_'+round.round+'" value="������������" /></div>' : '') +
				'</fieldset>' +
			'</div>';

		$(value).hide().insertAfter('#round_area').slideDown('normal');

		var recentQuestions = $('.recent-question');
		if(recentQuestions.length > 2) {
			recentQuestions.last().slideUp('normal', function() {
				$(this).remove();
			});
		}
	}
}

function initAnswerFields() {
	$('#input_answer').bind('keydown', 'ctrl+v shift+insert', function(event) {
		var el = this;
		var value = this.value;

		if ($.browser.opera) {
			setTimeout(function(){
				el.value = value;
			}, 1);
		}

	   //	return false;
	}).bind('contextmenu', function() {
		return false;
	}).bind('drop', function(event) {
		if (typeof event.target.textContent != 'udefined') {
			this.value = '';
			event.preventDefault();
		}

		return false;
	}).mouseover(function() {
		$(this).focus();
	}).keydown(accept_answer).focus();

	$('#accept_answer_button').click(_accept_answer);
}

function complaintAnswer(round,index) {
	$.ajax({
		cache: false,
		data: {
			round: round,
			index: index,
			cmd: "complaint",
			nd: mynd
		},
		error: function(data) { complaint_answer_failure(round,index); },
		success: function(data) { complaint_answer_success(round,index); },
		type: 'post',
		url: '/victorina.php?' + Math.random()
	});
	$('#ComplaintLink_'+round+'_'+index).replaceWith('<span id="ComplaintLink_'+round+'_'+index+'">(������ ������������...)</span>');
}

function complaint_answer_failure(round,index) {
	$('#ComplaintLink_'+round+'_'+index).replaceWith('<span id="ComplaintLink_'+round+'_'+index+'">(������ �������� ������)</span>');
}

function complaint_answer_success(round,index) {
	$('#ComplaintLink_'+round+'_'+index).replaceWith('<span id="ComplaintLink_'+round+'_'+index+'">(������ �������)</span>');
}

function complaintQuestion(round) {
	$.ajax({
		cache: false,
		data: {
			round: round,
			cmd: "complaint_question",
			nd: mynd
		},
		error: function(data) { complaint_question_failure(round); },
		success: function(data) { complaint_question_success(round); },
		type: 'post',
		url: '/victorina.php?' + Math.random()
	});
	;

	$('#ComplaintLink_'+round).replaceWith('<input type="button" class="xbbutton" id="ComplaintLink_'+round+'" value="������ ������������..." disabled="disabled" />');
}

function complaint_question_failure(round) {
	$('#ComplaintLink_'+round).replaceWith('<input type="button" class="xbbutton" id="ComplaintLink_'+round+'" value="������ �������� ������" disabled="disabled" />');
}

function complaint_question_success(round) {
	$('#ComplaintLink_'+round).replaceWith('<input type="button" class="xbbutton" id="ComplaintLink_'+round+'" value="������ �������" disabled="disabled" />');
}

var timeout = null;
$(function(){
	showLastRound();
	timeout = setTimeout(update_round_timer, current_question ? 1000 : 15000);
});