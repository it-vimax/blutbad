function showItems(array_items){
	var list = $('#items-col ul').empty();

	for(var i = 0; i < array_items.length; i++) {
		var item = $('<li />');
		item.append(description(array_items[i]));
		list.append(item);
	}
	$('#tooltip.items').hide();
	list.find('img[title]').tooltip($.extend({}, tooltipDefaults, {
		extraClass: 'items'
	}));
	$('#items-col fieldset').show();
}

function showMessage(){
	document.getElementById('message1').innerHTML = message2;
	message2 = '';
}

function toBattle(){
	top.frames['main'].location.href = '/fbattle.php?' + Math.random();
}

function clearItems(){
	$('#tooltip.items').hide();
	$('#items-col fieldset').hide();
	$('#items-col ul').empty();
}

function talk_npc(id){
	top.frames['main'].location.href = '/npc.php?cmd=npc&nid=' + id;
}

function escapeFromDungeon(el){
	if (confirm("�� ������������� ������ �������� ����������?")) {
		el.disabled = true;
		top.frames['main'].location.href = '/dungeon.php?cmd=giveup&' + Math.random();
	}
}

function description(item){

	//var chars = item.name.split(/_/);
	//var name = '';
	//item.name = '';
	//for (var i = 0; i < chars.length; i++) {
	//	if (chars[i] == 32) {
	//		item.name = item.name + ' ';
	//		continue;
	//	}
	//	if (chars[i] == 184) {
	//		item.name = item.name + '�';
	//		continue;
	//	}
	//
	//	if (chars[i] == 45) {
	//		item.name = item.name + '-';
	//		continue;
	//	}
	//	item.name = item.name + String.fromCharCode(Number(848) + Number(chars[i]));
	//
	//}

	var img = $('<img />', {
		alt: item.name,
		click: function() {
			takeItem(item.name, item.type, item.num, item.id);
		},
		src: item.i,
		title: item.name
	});

	return img;
}

function refresh(){
	top.frames['main'].location.href = '/map.php?' + Math.random();
}

function fishing(){
	top.frames['main'].location.href = '/fishing.php?' + Math.random();
}

function forge(){
	top.frames['main'].location.href = '/forge.php?' + Math.random();
}

function takeItem(name, type, num, entry){
	if (confirm('�� �������, ��� ������ ������� "' + name + '"') && send_ajax) {
		//if (send_ajax) {
		send_ajax(type, num, entry);
		//}
	}
}

function showError(error){
	jQuery('#message1').html(error);
}

function klan_heal(){
	top.frames['main'].location.href = 'hospital.php?' + Math.random();
}

function klan_repair(){
	top.frames['main'].location.href = 'repair.php?' + Math.random();
}

function klan_chest(){
	top.frames['main'].location.href = 'klan_chest.php?' + Math.random();
}

function klan_shop(){
	top.frames['main'].location.href = 'klan_shop.php?' + Math.random();
}

function crafting(){
	top.frames['main'].location.href = 'crafting.php?' + Math.random();
}