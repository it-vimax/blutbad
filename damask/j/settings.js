function prepareInterface(form){
	var rows = top.getCookie('main-set-rows.' + top.userId);
	if (rows) {
		var date = new Date();
		date.setDate(date.getDate() + 365);

		var arr = rows.split(',');
		if (form['settings.hide_quick_buttons'].checked && arr.length == 4) {
			arr.unshift('30');
			top.setCookie('main-set-rows.' + top.userId, arr.join(','), date.toGMTString(), '/', top.getSubDomains());
		} else if (!form['settings.hide_quick_buttons'].checked && arr.length == 5) {
			arr.shift();
			top.setCookie('main-set-rows.' + top.userId, arr.join(','), date.toGMTString(), '/', top.getSubDomains());
		}
	}
}

function setObraz(name, money, sterling, new_year_money, time){
	var question = '�� ������������� ������ �������� �����?';
	var free_obraz = 0;
	if (confirm(question)) {
		if (money > 0 && free_obraz == 0) {
			return confirm_setObraz(money, '��.', name, time);
		} else if (sterling > 0 && free_obraz == 0) {
			return confirm_setObraz(sterling, '���.', name, time);
		} else if (new_year_money > 0) {
			return confirm_setObraz(new_year_money, '�.�.', name, time);
		} else {
			return st(name);
		}
	} else {
		return false;
	}
}

function confirm_setObraz(value, currency, name,time){
	var str = '���� ����� ����� ' + value + ' ' + currency;
	if (time > 0) {
		str += ' � ����� ����������� '+time+'��.';
	}

	if (confirm(str + '\n������?')) {
		return st(name);
	} else {
		return false;
	}
}


function st(name){
	location.href = '?cmd=obraz.set&name=' + name;
}

function delete_image(entry){
	if (confirm("�� ������������� ������ ������� ��� �����������?")) {
		location.href = '?cmd=attaches.delete&imgentry=' + entry + '&nd=' + nd + '&' + Math.random();
	}
}

/**
 * ������������� ���� ������������
 */
function initMyMenu() {
	var myMenu = $('#my-menu').empty();
	for(var i = 1; i <= 10; i++) {
		if(myMenuItems[i]) {
			var li = $('<li />').appendTo(myMenu);

			$('<input />', {
				name: myMenuItems[i].name,
				type: 'hidden',
				value: i
			}).appendTo(li);

			$('<span />', {
				text: myMenuItems[i].title
			}).appendTo(li);
		}
	}

	myMenu.sortable({
		out: function(event, ui) {
			if(!ui.item.hasClass('ui-draggable')) {
				$('#main-menu').addClass('bin');
			}

			$(this).removeClass('over');
		},
		over: function(event, ui) {
			$(this).addClass('over');
		},
		stop: function() {
			$('#main-menu').removeClass('bin');
		},
		update: function(event, ui) {
			myMenu.children().each(function(i) {
				$(this).removeClass('ui-draggable');
				var input = $(this).children('input[type="hidden"]');
				input.val(i + 1);
				var name = input[0].name;
				$('#main-menu .menu-item li:visible:has(input[name="' + name + '"])').hide();
			});

			checkMaxItems();
		}
	});
}

function checkMaxItems() {
	if($('#my-menu li').length < 10) {
		$('#main-menu .menu-item li').draggable('enable');
	} else {
		$('#main-menu .menu-item li').draggable('disable');
	}
}

$(function() {

	$('[placeholder]').placeholder();
	// ���������� ��� ��
	$('#birthday').datepicker({
		dateFormat: 'dd.mm.yy',
		maxDate: new Date(),
		yearRange: 'c-100:c+100'
	});

$('#profile-comment').charCounter(null, {
		container: $('#profile-comment-charcounter')
	});
            
	$('.avatar:not(.selected)').hover(
		function() {
			$(this).animate({
				opacity: 1
			}, 300);
		},
		function() {
			$(this).animate({
				opacity: .3
			}, 300);
		}
	);

 	// ������ ���� �� ������� - ����������� �������� � ����� ����
	$('img.avatar[data]').bind('contextmenu', function() {
		window.open('http://enc.blutbad.ru/obraz/' + $(this).attr('data') + '.html', 'avatar');
		return false;
	});

	/**
	 * ��� ����
	 */
	$('#main-menu').droppable({
		drop: function(event, ui) {
			$('#main-menu').removeClass('bin');

			var input = ui.draggable.children('input[type="hidden"]');
			if(input.val()) {
				var name = input[0].name;
				$('#main-menu').find('input[name="' + name + '"]').parent().show();

				// ����� � ������� ��-�� IE < 9
				ui.draggable.hide();
				setTimeout(function() {
					ui.draggable.remove();
					checkMaxItems();
				}, 0);
			}
		}
	});

	$('#main-menu .menu-item li').draggable({
		connectToSortable: '#my-menu',
		helper: 'clone',
		revert: 'invalid',
		revertDuration: 0
	});

	$('#my-menu').sortable('refresh');

	checkMaxItems();

});