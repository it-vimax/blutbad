var params = {
	strength: '����',
	dexterity: '��������',
	success: '��������',
	weariness: '������� ������������',
	endurance: '������� �����',
	krit: '����������� ����',
	ukrit: '������ ������������ �����',
	uvor: '�����������',
	uuvor: '������ �����������',
	uronmin: '����������� ����',
	uronmax: '������������ ����',
	b1: '����� ������',
	b2: '����� �������',
	b3: '����� �����',
	b4: '����� ���',
	intelligence: '���������',
	speedHP: '�������� ������������� �����',
	speedPW: '�������� �������������� ������������',
	durability: '�������������'
};

function hintItem(hash){
	hash = eval("(" + hash + ")");
	
	if(hash.weight) {
		hash.weight = hash.weight.replace(/\.?0+$/, '');
	}
	
	var result = '<b>' + (hash.name || '����������') + '</b> (�����: ' + (hash.weight || 0) + ') ' + '(' + hash.param3 + ' ��.)<br />';
	
	result += '<ul class="properties">';

	for (param in params) {
		var name = params[param] || param;
		var value = hash[param];

		if (value != 0 && typeof(value) != 'undefined')
          result += '<li>' + name + ': +' + value + '</li>';
	}
	result += '</ul>';
	
	return '<div class="item">' + result + '</div>';
}

function changeLocation(path){
	if (top.frames['main']) {
		top.frames['main'].location.href = '' + path + '&' + Math.random();
	} else {
		window.location.href = '' + path + '&' + Math.random();
	}
}

function showHintItem2(hash, cnt){
	var weight = hash.weight || 0;

	weight = weight.replace(/0+$/,"");
	weight = weight.replace(/\.$/,"");

	var result = '<b>' + (hash.name || '����������') + '</b> (�����: ' + weight + ') ' + '(' + cnt + ' ��.)';

	var list = '';
	for (param in params) {
		var name = params[param] || param;
		var value = hash[param];
		
		if (value != 0 && typeof(value) != 'undefined') {
         if (value > 0) {
			list += '<li>' + name + ': +' + value + '</li>';
         } else {
			list += '<li>' + name + ': ' + value + '</li>';
         }
        }
	}
	
	if(list) {
		list = '<ul class="description">' + list + '</ul>';
	}
	
	return '<div class="item">' + result + list + '</div>';
}

function linkDialogQuestGet(link){
	if (confirm('�� �������?')) {
		changeLocation(link);
	}
}

$(function() {
	$('.tooltip-hidden').tooltip($.extend({}, tooltipDefaults, {
		bodyHandler: function(){
			return hintItem($(this).next().html());
		}
	}));
	
	$('.tooltip-count-hidden').tooltip($.extend({}, tooltipDefaults, {
		bodyHandler: function(){
			return eval($(this).next().html());
		}
	}));
	
	var quests = jQuery('.replicas li.tutorial-dialog');
	if(quests.length) {
		setInterval(function() {
			quests.toggleClass('highlight');
		}, 500);
	}

	$('.elapsed-time').removeClass('invisible').each(function(){
		$(this).countdown({
			format: 'yodhms',
			layout: '{y<}{yn} {yl} {y>}{o<}{on} {ol} {o>}{d<}{dn} {dl} {d>}{h<}{hn} {hl} {h>}{m<}{mn} {ml} {m>}{sn} {sl}',
			onExpiry: function() {
				if ($('#a_time_elapsed_time')) {
					$('#a_time_elapsed_time').text('(���������)');
					top.frames['carnage'].location.reload();
				}
			},
			until: $(this).text()
		})
	});
});