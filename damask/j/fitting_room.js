var f_i = 1;

var typesWeapons = new Array();
var typesArmors = new Array();
var typesMagics = new Array();

MALE_IMG = "obraz/0_0_M000.jpg";
FEMALE_IMG = "obraz/0_0_F000.jpg";
CUSTOM_ITEM_NUM = 999;

// List of abilities
ABILITIES = new Array(
    "str",
    "dex",
    "suc",
    "end",
    "intel",
    "wis",
    "weight",
    "price",
    "hp",
    "pw",
    "uronmin",
    "uronmax",
    "krit",
    "ukrit",
    "uvor",
    "uuvor",
    "b1",
    "b2",
    "b3",
    "b4"
);

STATS = new Array(
    "level",
    "strength",
    "dexterity",
    "success",
    "endurance",
    "intelligence",
    "wisdom"
);

SHORT_STATS = new Array(
    "l",
    "p",
    "d",
    "s",
    "e",
    "i",
    "w"
);

REQS = {
    "level": "level",
    "str":   "strength",
    "dex":   "dexterity",
    "suc":   "success",
    "end":   "endurance",
    "intel": "intelligence",
    "wis":   "wisdom"
};

SLOTS = new Array(
    "helmet",
    "armor",
    "belt",
    "shoes",
    "bracelet",
    "gloves",
    "weapon",
    "shield",
    "earring",
    "necklace",
	"pants",
	//"pocket1",
	//"pocket2",
    "ring1",
    "ring2",
    "ring3",
	"ring4",
   //	"shoulder",
    "stone1",
    "stone2",
    "stone3",
    "stone4",
    "stone5",
    "stone6",
    "stone7",
    "rune1",
    "rune2",
    "rune3",
    "rune4",
    "rune5",
    "rune6"
);

SHORT_SLOTS = new Array(
    "h",
    "a",
    "b",
    "s",
    "t",
    "g",
    "w",
    "p",
    "e",
    "n",
	"pa",
	//"po1",
	//"po2",
    "k1",
    "k2",
    "k3",
    "k4",
	"sh",
    "s1",
    "s2",
    "s3",
    "s4",
    "s5",
    "s6",
    "s7",
    "r1",
    "r2",
    "r3",
    "r4",
    "r5",
    "r6"

);

TYPE_CODES = {
	1: 'cas',
	2: 'club',
	3: 'sw',
	4: 'kn',
	5: 'axe',
	6: 'ham',
	7: 'sp',
	8: 'helm',
	9: 'boot',
	10: 'armor',
	11: 'glov',
	12: 'belt',
	13: 'ring',
	14: 'neck',
	15: 'ear',
	16: 'shield',
	17: 'bracelet',
	18: 'tackle',
	19: 'pants',
  	20: 'stone',
  //	23: 'shoulder',
	50: 'rune'
};

// Inventory positions by items type
slotByType = {
     1: "weapon",
     2: "weapon",
     3: "weapon",
     4: "weapon",
     5: "weapon",
     6: "weapon",
     7: "weapon",
     8: "helmet",
     9: "shoes",
    10: "armor",
    11: "gloves",
    12: "belt",
    13: "ring",
    14: "necklace",
    15: "earring",
    16: "shield",
    17: "bracelet",
    18: "weapon",
	19: "pants",
    20: "stone",
  //	23: "shoulder",
    50: "rune",
	91: "pocket"
};

KBO = new Array(

    6,
    63,
    131,
    251,
    358,
    510,
    690,
    910,
    1220,
    1580,
    1889,
    2272
);

UP = new Array(
    6,
    8,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    31,
    40,
    14
);

function itemRequires(level, str, dex, suc, end, intel, wis) {
    this.level = level;
    this.str   = str;
    this.dex   = dex;
    this.suc   = suc;
    this.end   = end;
    this.intel = intel;
    this.wis   = wis;
}

function itemProperties(strength, dexterity, success, intelligence, weight, price, hp, pw, uronmin, uronmax, krit, ukrit, uvor, uuvor, b1, b2, b3, b4) {
    this.str     = strength;
    this.dex     = dexterity;
    this.suc     = success;
    this.end     = 0;
    this.intel   = intelligence;
    this.wis     = 0;
    this.weight  = weight;
    this.price   = price;
    this.hp      = hp;
    this.pw      = pw;
    this.uronmin = uronmin;
    this.uronmax = uronmax;
    this.krit    = krit;
    this.ukrit   = ukrit;
    this.uvor    = uvor;
    this.uuvor   = uuvor;
    this.b1      = b1;
    this.b2      = b2;
    this.b3      = b3;
    this.b4      = b4;
}

function createItem(type, num, name, image, weight, durability, price, level, str, dex, suc, end, intel, wis, strength, dexterity, success, intelligence, hp, pw, uronmin, uronmax, krit, ukrit, uvor, uuvor, b1, b2, b3, b4) {
    this.type       = type;
    this.num        = num;
    this.name       = name;
    this.image      = image;
    this.durability = durability;

    this.requires   = new itemRequires(level, str, dex, suc, end, intel, wis);
    this.properties = new itemProperties(strength, dexterity, success, intelligence, weight, price, hp, pw, uronmin, uronmax, krit, ukrit, uvor, uuvor, b1, b2, b3, b4);
}

function peopleSlots() {
    this.helmet   = new createItem(0, 0, '', 'char_helmet.gif',   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.armor    = new createItem(0, 0, '', 'char_armor.gif',    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.belt     = new createItem(0, 0, '', 'char_belt.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.shoes    = new createItem(0, 0, '', 'char_shoes.gif',    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.bracelet = new createItem(0, 0, '', 'char_bracelet.gif', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.gloves   = new createItem(0, 0, '', 'char_gloves.gif',   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.weapon   = new createItem(0, 0, '', 'char_weapon.gif',   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.shield   = new createItem(0, 0, '', 'char_shield.gif',   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.earring  = new createItem(0, 0, '', 'char_earring.gif',  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.necklace = new createItem(0, 0, '', 'char_necklace.gif', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	this.pants    = new createItem(0, 0, '', 'char_pants.gif',	  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
//	this.pocket1  = new createItem(0, 0, '', 'char_pocket.gif',   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
//	this.pocket2  = new createItem(0, 0, '', 'char_pocket.gif',   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.ring1    = new createItem(0, 0, '', 'char_ring.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.ring2    = new createItem(0, 0, '', 'char_ring.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.ring3    = new createItem(0, 0, '', 'char_ring.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.ring4    = new createItem(0, 0, '', 'char_ring.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
  //	this.shoulder = new createItem(0, 0, '', 'char_shoulder.gif',   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.stone1   = new createItem(0, 0, '', 'char_stone.gif',    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.stone2   = new createItem(0, 0, '', 'char_stone.gif',    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.stone3   = new createItem(0, 0, '', 'char_stone.gif',    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.stone4   = new createItem(0, 0, '', 'char_stone.gif',    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.stone5   = new createItem(0, 0, '', 'char_stone.gif',    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.stone6   = new createItem(0, 0, '', 'char_stone.gif',    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.stone7   = new createItem(0, 0, '', 'char_stone.gif',    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.rune1    = new createItem(0, 0, '', 'char_rune.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.rune2    = new createItem(0, 0, '', 'char_rune.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.rune3    = new createItem(0, 0, '', 'char_rune.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.rune4    = new createItem(0, 0, '', 'char_rune.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.rune5    = new createItem(0, 0, '', 'char_rune.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    this.rune6    = new createItem(0, 0, '', 'char_rune.gif',     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

function createPeople() {
	this.id           = 0;
    // Abilities
    this.sex          = "M";
    this.obraz        = MALE_IMG;
    this.level        = 0;
    this.strength     = 3;
    this.dexterity    = 3;
    this.success      = 3;
    this.endurance    = 3;
    this.intelligence = 0;
    this.wisdom       = 0;
    this.str          = this.strength;
    this.dex          = this.dexterity;
    this.suc          = this.success;
    this.end          = this.endurance;
    this.intel        = this.intelligence;
    this.wis          = this.wisdom;
    this.weight       = 0;
    this.maxweight    = this.str * 6;
    this.price        = 0;
    this.hp           = this.end * 10;
    this.pw           = this.str * 6;
    this.uronmin      = 0;
    this.uronmax      = 0;
    this.krit         = 0;
    this.ukrit        = 0;
    this.uvor         = 0;
    this.uuvor        = 0;
    this.b1           = 0;
    this.b2           = 0;
    this.b3           = 0;
    this.b4           = 0;

    this.slots        = new peopleSlots();

    this.levelmin     = 0;
    this.levelmax     = 0;
    this.bot          = 0;
    this.name         = "";
}

function createPeopleFromXML(xml) {
    var error = $("error", xml).text() || "";
    if (error != "") {
        alert("������ ��������: "+error);
        return 0;
    }

    var people = $("people", xml).text() || "";
    /*if (people == "") {
        alert("�� ��������� ������������ ����� �������� � ������.\n��� IP �������� ������������!");
        return 0;
    }*/

    eval(people + 'People = new createPeople()');

    var p = eval(people + 'People');

    // Abilities
    p.sex          = $("sex", xml).text();
    p.obraz        = $("obraz", xml).text();
    p.level        = new Number($("level", xml).text());
    p.strength     = new Number($("strength", xml).text());
    p.dexterity    = new Number($("dexterity", xml).text());
    p.success      = new Number($("success", xml).text());
    p.endurance    = new Number($("endurance", xml).text());
    p.intelligence = new Number($("intelligence", xml).text());
    p.wisdom       = new Number($("wisdom", xml).text());

	if(p.level > 11) {
		p.level = 11;
	}

    if (p.obraz == 'obraz/') {
        p.obraz = (p.sex == 'F') ? FEMALE_IMG : MALE_IMG;
    }

    // Items
    $("item", xml).each(function() {
        var slot  = $("slot", this).text();
        var slot0 = $("slot0", this).text();
        var type  = $("type", this).text();
        var num   = $("num", this).text();

        var items = eval(slot + '[' + type + ']');

        for (var i = 0; i < items.length; i++) {
            var item = items[i];

            if (item.num == num) {
                p.slots[slot + slot0] = item;
                break;
            }
        }
    });

    updatePeople(people);
}

function createPeopleFromBotXML(xml) {
    var error = $("error", xml).text() || "";
    if (error != "") {
        alert("������ ��������: "+error);
        return 0;
    }

    var people = $("people", xml).text() || "";

    if (people == "") {
        alert("�� ��������� ������������ ����� �������� � ������.\n��� IP �������� ������������!");
        return 0;
    }

    eval(people + 'People = new createPeople()');

    var p = eval(people + 'People');

    // Abilities
    p.sex          = $("sex", xml).text();
    p.obraz        = $("obraz", xml).text();
    p.level        = new Number($("level", xml).text());
    p.strength     = new Number($("strength", xml).text());
    p.dexterity    = new Number($("dexterity", xml).text());
    p.success      = new Number($("success", xml).text());
    p.endurance    = new Number($("endurance", xml).text());
    p.intelligence = new Number($("intelligence", xml).text());
    p.wisdom       = new Number($("wisdom", xml).text());
    p.bot		   = 1;
    p.name		   = $("name",xml).text();
    p.id           = new Number($("id",xml).text());

    if (p.obraz == 'obraz/') {
        p.obraz = (p.sex == 'F') ? FEMALE_IMG : MALE_IMG;
    }

    updatePeople(people);
}

function updatePeople(people) {
    var p = eval(people + 'People');
    var ep = eval((people == "left" ? "right" : "left") + 'People');

    // Reset values
    p.str       = p.strength;
    p.dex       = p.dexterity;
    p.suc       = p.success;
    p.end       = p.endurance;
    p.intel     = p.intelligence;
    p.wis       = p.wisdom;
    p.weight    = 0;
    p.maxweight = 0;
    p.price     = 0;
    p.hp        = 0;
    p.pw        = 0;
    p.uronmin   = 0;
    p.uronmax   = 0;
    p.krit      = 0;
    p.ukrit     = 0;
    p.uvor      = 0;
    p.uuvor     = 0;
    p.b1        = 0;
    p.b2        = 0;
    p.b3        = 0;
    p.b4        = 0;

    // Bonuses
    for (var i = 0; i < ABILITIES.length; i++) {
        var ability = ABILITIES[i];
        var sum     = new Number(p[ability]);

        for (var j = 0; j < SLOTS.length; j++) {
            var slot = SLOTS[j];
            var val  = new Number(p.slots[slot].properties[ability]);

            if ((ability == 'price' || ability == 'hp' || ability == 'pw') && (slot == 'rune1' || slot == 'rune2' || slot == 'rune3' || slot == 'rune4' || slot == 'rune5' || slot == 'rune6')) { } else {
                sum = sum + val;
            }
        }

        if (ability == 'str' || ability == 'dex' || ability == 'suc' || ability == 'end') {
            p[ability] = sum || 3;
        } else {
            p[ability] = sum;
        }

        if (
			p.bot > 0 && ability != 'str' && ability != 'dex' && ability != 'suc' && ability != 'end' && ability != 'intel' && ability != 'wis' &&
			ability != 'weight' && ability != 'price'
		) {
			$("#" + people + "_" + ability).html("?");

		}
		else {
			$("#" + people + "_" + ability).html(sum);
		}
    }

    p.maxweight = p.str * 6;
    p.hp        = p.hp + (p.end * 10) || 18;
    p.pw        = p.pw + (p.str * 6) || 18;

    $("#" + people + "_maxweight").html(p.bot > 0 ? 0 : p.maxweight);
    $("#" + people + "_hp").html(p.bot > 0 ? "?" : p.hp);
    $("#" + people + "_pw").html(p.bot > 0 ? "?" : p.pw);

    // Images
    for (var i = 0; i < SLOTS.length; i++) {
        var slot = SLOTS[i];
        var item = p.slots[slot];

        var require = false;

        if (
            item.requires.level > p.level ||
            item.requires.str > p.str     ||
            item.requires.dex > p.dex     ||
            item.requires.suc > p.suc     ||
            item.requires.end > p.end     ||
            item.requires.intel > p.intel ||
            item.requires.wis > p.wis
        ) {
            require = true;
        }
        if (p.bot > 0) {
            $("#" + people + "_" + slot).css("background", "none").css("display", "none");
        } else {
            $("#" + people + "_" + slot).css("display", "").css("background", (require ? "#FCC " : "") + "url(http://img.blutbad.ru/i/" + p.slots[slot].image + ")");
        }

    }

    // Obraz
    $("#" + people + "_obraz").css("background", "url(http://img.blutbad.ru/i/" + p.obraz + ")");
    if (p.bot > 0) $("#" + people + "_user_wrap").addClass("inv-items-full-size");
    else $("#" + people + "_user_wrap").removeClass("inv-items-full-size");

    // Abilities
    var kbo = KBO[p.level];
    var kbo_price = p.price;

    var up  = p.strength + p.dexterity + p.success + p.endurance + p.intelligence + p.wisdom;

    $("#" + people + "_kbo").html((p.level == 0 || p.bot > 0) ? "100%" : parseInt(kbo_price / kbo * 100) + "%");
    $("#" + people + "_level").attr("value", p.level).attr('disabled',p.bot > 0 ? true : false);
    $("#" + people + "_level_span").text(String(p.level));
    if (p.bot > 0) {
		$("#" + people + "_level").hide();
		$("#" + people + "_level_span").show();
	}
	else {
		$("#" + people + "_level").show();
		$("#" + people + "_level_span").hide();
	}
    $("#" + people + "_strength").attr("value", p.strength || 3).attr('disabled',p.bot > 0 ? true : false);
    $("#" + people + "_dexterity").attr("value", p.dexterity || 3).attr('disabled',p.bot > 0 ? true : false);
    $("#" + people + "_success").attr("value", p.success || 3).attr('disabled',p.bot > 0 ? true : false);
    $("#" + people + "_endurance").attr("value", p.endurance || 3).attr('disabled',p.bot > 0 ? true : false);
    $("#" + people + "_intelligence").attr("value", p.intelligence).attr('disabled',p.bot > 0 ? true : false);
    $("#" + people + "_wisdom").attr("value", p.wisdom).attr('disabled',p.bot > 0 ? true : false);
    $("#" + people + "_up").html(up);

    // Weight
    if (p.weight > p.maxweight) {
        $("#" + people + "_weight").addClass("red");
    } else {
        $("#" + people + "_weight").removeClass();
    }

    // Fight
    $("#draws").html("0");
    $("#left_fight").html("0");
    $("#right_fight").html("0");

	if (p.bot > 0) {
		$("#partner_type").val(1).hide();
		$("#partner_type_title").hide();
		$('.positions').hide();
		$("#" + people + "_items_levels,#" + people + "_items_list,#" + people + "_items,#" +
			people + "_button_custom_item,#" + people + "_button_save,#" + people + "_button_calc_stats"
		).hide();
	}
	else {
		if (!ep.bot > 0) {
			$("#partner_type").show();
			$("#partner_type_title").show();
			if ($("#partner_type").val() == 1) $('.positions').hide();
			else $('.positions').show();
		}
		$("#" + people + "_items_levels,#" + people + "_items_list,#" + people + "_items,#" +
			people + "_button_custom_item,#" + people + "_button_save,#" + people + "_button_calc_stats"
		).show();
		updateItemsLevels(people, p.level);
		updateItemsList(people, $("#" + people + '_items_list').attr("value"));
	}
	diffPeoples();
}

function loadPeople(people){
	$.post("/fitting_room.php", {
		cmd: "load",
		is_ajax: 1,
		name: $("#" + people + "_name").attr("value"),
		people: people
	}, createPeopleFromXML);

	return false;
}

function loadBot(people){
	$.post("/fitting_room.php", {
		cmd: "load_bot",
		id: $("#" + people + "_bot").val(),
		is_ajax: 1,
		people: people
	}, createPeopleFromBotXML);

	return false;
}

function clone(object){
	if (object === null) {
		return null;
	}
	if (object.constructor == Boolean || object.constructor == String || object.constructor == Number || object.constructor == Function) {
		return object;
	}

	var newObject = new object.constructor();

	for (var objectItem in object) {
		newObject[objectItem] = clone(object[objectItem]);
	}

	return newObject;
}

function clonePeople(people){
	if (people == 'left') {
		rightPeople = clone(leftPeople);
		updatePeople('right');
	} else {
		leftPeople = clone(rightPeople);
		updatePeople('left');
	}
}

function clearPeople(people){
	eval(people + 'People = new createPeople()');

	var p = eval(people + 'People');

	updatePeople(people);
}

function updateAbility(people, ability, value){
	var p = eval(people + 'People');

	// Incorrect value
	if (value < 0 || (ability == 'level' && value > 13)) {
		$("#" + people + "_" + ability).attr("value", p[ability]);
		// Updating ability
	} else {
		p[ability] = new Number(value);
	}

	updatePeople(people);
}

function updateItemsList(people, type, levelmin, levelmax){
	levelmin = typeof levelmin == 'undefined' ? eval(people + 'People.levelmin') : levelmin;
	levelmax = typeof levelmax == 'undefined' ? eval(people + 'People.levelmax') : levelmax;

	var html = "";
	var slot = slotByType[type];
	var items = eval(slot + '[' + type + ']');

	$("#" + people + "_items_levels_all").css("font-weight", "normal");

	for (var i = 0; i <= 13; i++) {
		$("#" + people + "_items_levels_" + i).css("font-weight", "normal");
	}

	if (levelmin != levelmax) {
		$("#" + people + "_items_levels_all").css("font-weight", "bold");
	} else {
		$("#" + people + "_items_levels_" + levelmin).css("font-weight", "bold");
	}

	eval(people + 'People.levelmin = ' + levelmin);
	eval(people + 'People.levelmax = ' + levelmax);

	for (var i = 0; i < items.length; i++) {
		var item = items[i];

		if (item.requires.level >= levelmin && item.requires.level <= levelmax) {
			if (type == 13) {
				html += '<div class="nowrap">';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 1);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 1)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 2);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 2)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 3);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 3)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 4);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 4)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '</div>';
			} else if (type == 20) {
				html += '<div class="nowrap">';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 1);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 1)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 2);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 2)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 3);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 3)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 4);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 4)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 5);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 5)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 6);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 6)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 7);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 7)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '</div>';
			} else if (type == 50) {
				html += '<div class="fl m5">';
				html += '<div class="nowrap">';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 1);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 1)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 2);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 2)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 3);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 3)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '</div>';
				html += '<div class="nowrap">';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 4);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 4)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 5);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 5)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\', 6);" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\', 6)" src="http://img.blutbad.ru/i/' + item.image + '" />';
				html += '</div>';
				html += '</div>';
			} else {
				html += '<img onmouseover="showInfo(\'' + people + '\', \'' + slot + '[' + type + '][' + i + ']\');" onmouseout="hideInfo();" onclick="wearItem(\'' + people + '\', \'' + type + '\', \'' + item.num + '\')" src="http://img.blutbad.ru/i/' + item.image + '" />';
			}
		}
	}

	if (!html) {
		html = '<div class="w100 tac" style="margin-top: 150px;">��� ���������<br/>��� ����� ������</div>';
	}

	$("#" + people + "_items").html(html);
}

function updateItemsLevels(people, level) {
    level = new Number(level || eval(people + 'People.level'));
    var html = '' +
		'<a class="tab" onclick="updateItemsList(\'' + people + '\', $(\'#' + people + '_items_list\').attr(\'value\'), 0, ' + level + ');">' +
			'<span id="' + people + '_items_levels_all">���</span>' +
		'</a>';

    for (var i = 0; i <= level; i++) {
        html += '&nbsp;' +
			'<a class="tab" onclick="updateItemsList(\'' + people + '\', $(\'#' + people + '_items_list\').attr(\'value\'), ' + i + ', ' + i + ');">' +
				'<span id="' + people + '_items_levels_' + i + '">' + i + '</span>' +
			'</a>';
    }

    $("#" + people + "_items_levels").html(html);
}

function wearItem(people, type, num, slot0){

	var p = eval(people + 'People');
	if (p.bot > 0) return;
	var slot = slotByType[type];
	var items = eval(slot + '[' + type + ']');

	var last_item = clone(p.slots[slot + (slot0 || '')]);

	for (var i = 0; i < items.length; i++) {
		var item = items[i];

		if (item.num == num) {
			var c = 0;

			if (slot == 'stone') {
				p.slots[slot + (slot0 || '')] = new createItem(0, 0, '', 'char_stone.gif', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

				for (var j = 1; j < 8; j++) {
					var e_num = p.slots[slot + j].num;

					if ((e_num >= 1 && e_num <= 33 && num >= 1 && num <= 33) ||
					(e_num >= 34 && e_num <= 66 && num >= 34 && num <= 66) ||
					(e_num >= 67 && e_num <= 99 && num >= 67 && num <= 99) ||
					(e_num >= 100 && e_num <= 132 && num >= 100 && num <= 132) ||
					(e_num >= 133 && e_num <= 165 && num >= 133 && num <= 165) ||
					(e_num >= 166 && e_num <= 198 && num >= 166 && num <= 198) ||
					(e_num >= 199 && e_num <= 231 && num >= 199 && num <= 231) ||
					(e_num >= 232 && e_num <= 265 && num >= 232 && num <= 265)) {
						c++;
					}
				}
			}

			if (c < 2) {
				p.slots[slot + (slot0 || '')] = item;
			} else {
				p.slots[slot + (slot0 || '')] = last_item;
			}

			break;
		}
	}

	updatePeople(people);
	showInfo(people, people + 'People.slots.' + slot + (slot0 || ''), slot0);
}

function unwearItem(people, slot, slot0){
	var p = eval(people + 'People');

	var type = 0;
	for (var i in slotByType) {
		if (slotByType[i] == slot) {
			type = i;
			break;
		}
	}


	$("#" + people + "_items_list").val(type);

	p.slots[slot + (slot0 || '')] = new createItem(0, 0, '', 'char_' + slot + '.gif', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

	updatePeople(people);
	hideInfo();
}

function showInfo(people, item, slot0){
	var p = eval(people + 'People');

	var item_selected = eval(item);
	var item_equiped = p.slots[slotByType[item_selected.type] + (slot0 || '')] || item_selected;

	// Selected item
	if (item_selected.num > 0) {
		var info = "";

		$("#info_name").html(item_selected.name);

		// Description
		info = '<img class="tal fl" src="http://img.blutbad.ru/i/' + item_selected.image + '" />';
		info += '<p style="padding-left: 80px;" class="vat">';
		info += '<strong>�����:</strong>&nbsp;' + item_selected.properties.weight + '<br/>';
		info += '<strong>����:</strong>&nbsp;' + item_selected.properties.price + '<br/>';
		info += '<strong>�������������:</strong>&nbsp;' + item_selected.durability;
		info += '</p>';

		// Requires
		info += '<p class="clear popup_info b vat">����������:</p>';
		info += '<ul class="popup_info_list vat">';
		if (item_selected.requires.level > 0) {
			info += '<li' + ((item_selected.requires.level > p.level) ? ' class="red"' : '') + '>�������:&nbsp;' + item_selected.requires.level + '</li>'
		};
		if (item_selected.requires.str > 0) {
			info += '<li' + ((item_selected.requires.str > p.str) ? ' class="red"' : '') + '>����:&nbsp;' + item_selected.requires.str + '</li>'
		};
		if (item_selected.requires.dex > 0) {
			info += '<li' + ((item_selected.requires.dex > p.dex) ? ' class="red"' : '') + '>��������:&nbsp;' + item_selected.requires.dex + '</li>'
		};
		if (item_selected.requires.suc > 0) {
			info += '<li' + ((item_selected.requires.suc > p.suc) ? ' class="red"' : '') + '>��������:&nbsp;' + item_selected.requires.suc + '</li>'
		};
		if (item_selected.requires.end > 0) {
			info += '<li' + ((item_selected.requires.end > p.end) ? ' class="red"' : '') + '>����������������:&nbsp;' + item_selected.requires.end + '</li>'
		};
		if (item_selected.requires.intel > 0) {
			info += '<li' + ((item_selected.requires.intel > p.intel) ? ' class="red"' : '') + '>���������:&nbsp;' + item_selected.requires.intel + '</li>'
		};
		if (item_selected.requires.wis > 0) {
			info += '<li' + ((item_selected.requires.wis > p.wis) ? ' class="red"' : '') + '>��������:&nbsp;' + item_selected.requires.wis + '</li>'
		};
		info += '</ul>';

		// Properties
		info += '<p class="clear popup_info b vat">��������:</p>';
		info += '<ul class="popup_info_list vat">';
		if (item_selected.properties.str > 0) {
			info += '<li>����:&nbsp;' + item_selected.properties.str + '</li>'
		};
		if (item_selected.properties.dex > 0) {
			info += '<li>��������:&nbsp;' + item_selected.properties.dex + '</li>'
		};
		if (item_selected.properties.suc > 0) {
			info += '<li>��������:&nbsp;' + item_selected.properties.suc + '</li>'
		};
		if (item_selected.properties.intel > 0) {
			info += '<li>���������:&nbsp;' + item_selected.properties.intel + '</li>'
		};
		if (item_selected.properties.hp > 0) {
			info += '<li>������� �����:&nbsp;' + item_selected.properties.hp + '</li>'
		};
		if (item_selected.properties.pw > 0) {
			info += '<li>������������:&nbsp;' + item_selected.properties.pw + '</li>'
		};
		if (item_selected.properties.uronmin > 0) {
			info += '<li>���.����:&nbsp;' + item_selected.properties.uronmin + '</li>'
		};
		if (item_selected.properties.uronmax > 0) {
			info += '<li>����.����:&nbsp;' + item_selected.properties.uronmax + '</li>'
		};
		if (item_selected.properties.krit > 0) {
			info += '<li>����������� ����:&nbsp;' + item_selected.properties.krit + '</li>'
		};
		if (item_selected.properties.ukrit > 0) {
			info += '<li>������ ������������ �����:&nbsp;' + item_selected.properties.ukrit + '</li>'
		};
		if (item_selected.properties.uvor > 0) {
			info += '<li>�����������:&nbsp;' + item_selected.properties.uvor + '</li>'
		};
		if (item_selected.properties.uuvor > 0) {
			info += '<li>������ �����������:&nbsp;' + item_selected.properties.uuvor + '</li>'
		};
		if (item_selected.properties.b1 > 0) {
			info += '<li>����� ������:&nbsp;' + item_selected.properties.b1 + '</li>'
		};
		if (item_selected.properties.b2 > 0) {
			info += '<li>����� �������:&nbsp;' + item_selected.properties.b2 + '</li>'
		};
		if (item_selected.properties.b3 > 0) {
			info += '<li>����� �����:&nbsp;' + item_selected.properties.b3 + '</li>'
		};
		if (item_selected.properties.b4 > 0) {
			info += '<li>����� ���:&nbsp;' + item_selected.properties.b4 + '</li>'
		};
		info += '</ul>';
		info += '<div class="clear"></div>';

		$("#stats_info").html(info);

		// Equiped item
		if (item_selected == item_equiped) {
			$("#equiped_stats").hide();
		} else if (item_equiped.num > 0) {
			$("#info_name_equiped").html(item_equiped.name);

			// Description
			info = '<img class="tal fl" src="http://img.blutbad.ru/i/' + item_equiped.image + '" />';
			info += '<p style="padding-left: 80px;">';
			info += '<strong>�����:</strong>&nbsp;' + item_equiped.properties.weight + '<br/>';
			info += '<strong>����:</strong>&nbsp;' + item_equiped.properties.price + '<br/>';
			info += '<strong>�������������:</strong>&nbsp;' + item_equiped.durability;
			info += '</p>';

			// Requires
			info += '<p class="clear popup_info b">����������:</p>';
			info += '<ul class="popup_info_list">';
			if (item_equiped.requires.level > 0) {
				info += '<li' + ((item_equiped.requires.level > p.level) ? ' class="red"' : '') + '>�������:&nbsp;' + item_equiped.requires.level + '</li>'
			};
			if (item_equiped.requires.str > 0) {
				info += '<li' + ((item_equiped.requires.str > p.str) ? ' class="red"' : '') + '>����:&nbsp;' + item_equiped.requires.str + '</li>'
			};
			if (item_equiped.requires.dex > 0) {
				info += '<li' + ((item_equiped.requires.dex > p.dex) ? ' class="red"' : '') + '>��������:&nbsp;' + item_equiped.requires.dex + '</li>'
			};
			if (item_equiped.requires.suc > 0) {
				info += '<li' + ((item_equiped.requires.suc > p.suc) ? ' class="red"' : '') + '>��������:&nbsp;' + item_equiped.requires.suc + '</li>'
			};
			if (item_equiped.requires.end > 0) {
				info += '<li' + ((item_equiped.requires.end > p.end) ? ' class="red"' : '') + '>����������������:&nbsp;' + item_equiped.requires.end + '</li>'
			};
			if (item_equiped.requires.intel > 0) {
				info += '<li' + ((item_equiped.requires.intel > p.intel) ? ' class="red"' : '') + '>���������:&nbsp;' + item_equiped.requires.intel + '</li>'
			};
			if (item_equiped.requires.wis > 0) {
				info += '<li' + ((item_equiped.requires.wis > p.wis) ? ' class="red"' : '') + '>��������:&nbsp;' + item_equiped.requires.wis + '</li>'
			};
			info += '</ul>';

			// Properties
			info += '<p class="clear popup_info b">��������:</p>';
			info += '<ul class="popup_info_list">';
			if (item_equiped.properties.str > 0) {
				info += '<li>����:&nbsp;' + item_equiped.properties.str + '</li>'
			};
			if (item_equiped.properties.dex > 0) {
				info += '<li>��������:&nbsp;' + item_equiped.properties.dex + '</li>'
			};
			if (item_equiped.properties.suc > 0) {
				info += '<li>��������:&nbsp;' + item_equiped.properties.suc + '</li>'
			};
			if (item_equiped.properties.intel > 0) {
				info += '<li>���������:&nbsp;' + item_equiped.properties.intel + '</li>'
			};
			if (item_equiped.properties.hp > 0) {
				info += '<li>������� �����:&nbsp;' + item_equiped.properties.hp + '</li>'
			};
			if (item_equiped.properties.pw > 0) {
				info += '<li>������������:&nbsp;' + item_equiped.properties.pw + '</li>'
			};
			if (item_equiped.properties.uronmin > 0) {
				info += '<li>���.����:&nbsp;' + item_equiped.properties.uronmin + '</li>'
			};
			if (item_equiped.properties.uronmax > 0) {
				info += '<li>����.����:&nbsp;' + item_equiped.properties.uronmax + '</li>'
			};
			if (item_equiped.properties.krit > 0) {
				info += '<li>����������� ����:&nbsp;' + item_equiped.properties.krit + '</li>'
			};
			if (item_equiped.properties.ukrit > 0) {
				info += '<li>������ ������������ �����:&nbsp;' + item_equiped.properties.ukrit + '</li>'
			};
			if (item_equiped.properties.uvor > 0) {
				info += '<li>�����������:&nbsp;' + item_equiped.properties.uvor + '</li>'
			};
			if (item_equiped.properties.uuvor > 0) {
				info += '<li>������ �����������:&nbsp;' + item_equiped.properties.uuvor + '</li>'
			};
			if (item_equiped.properties.b1 > 0) {
				info += '<li>����� ������:&nbsp;' + item_equiped.properties.b1 + '</li>'
			};
			if (item_equiped.properties.b2 > 0) {
				info += '<li>����� �������:&nbsp;' + item_equiped.properties.b2 + '</li>'
			};
			if (item_equiped.properties.b3 > 0) {
				info += '<li>����� �����:&nbsp;' + item_equiped.properties.b3 + '</li>'
			};
			if (item_equiped.properties.b4 > 0) {
				info += '<li>����� ���:&nbsp;' + item_equiped.properties.b4 + '</li>'
			};
			info += '</ul>';
			info += '<div class="clear"></div>';

			$("#stats_info_equiped").html(info);
			$("#equiped_stats").show();
		} else {
			$("#equiped_stats").hide();
		}

		$(".popup_stats").css("top", ($(window).scrollTop() + 20) + "px").show();

		if ((people == 'right') && ($("#equiped_stats").css("display") != "none")) {
			$(".popup_stats").addClass("left_margin");
		} else {
			$(".popup_stats").removeClass("left_margin");
		}

		if ($.browser.msie) {
			$(".popup_stats_frame").show();
			$(".popup_stats_frame").css("top", $(".popup_stats").css("top"));
			$(".popup_stats_frame").css("margin-left", $(".popup_stats").css("margin-left"));
			$(".popup_stats_frame").css("left", $(".popup_stats").css("left"));

			if ($("#equiped_stats").css("display") == "none") {
				wid1 = 0;
			} else {
				wid1 = 246;
			}
			if ($("#current_stats").css("display") == "none") {
				wid2 = 0;
			} else {
				wid2 = 246;
			}

			h1 = 0;
			h2 = 0;
			w1 = 0;
			w2 = 0;

			w1 = document.getElementById("equiped_stats").offsetWidth;
			h1 = document.getElementById("equiped_stats").offsetHeight;
			w2 = document.getElementById("current_stats").offsetWidth;
			h2 = document.getElementById("current_stats").offsetHeight;

			if ((h1 == 0) || (h2 == 0)) {
				if (h1 == 0) {
					h = h2;
				}
				if (h2 == 0) {
					h = h1;
				}
			} else {
				if (h2 < h1) {
					h = h2;
				} else {
					h = h1;
				}
			}
			w = w1 + w2;

			$(".popup_stats_frame").css("height", h + "px");
			$(".popup_stats_frame").css("width", w + "px");
		}
	}
}

function hideInfo(){
	$(".popup_stats").hide();
	$(".popup_stats_frame").hide();
}

function changeSex(people){
	var p = eval(people + 'People');

	if (p.sex == "M") {
		p.sex = "F";
		p.obraz = FEMALE_IMG;

		$("#" + people + "_obraz").css("background", "url(http://img.blutbad.ru/i/" + FEMALE_IMG + ")");
	} else if (p.sex == "F") {
		p.sex = "M";
		p.obraz = MALE_IMG;

		$("#" + people + "_obraz").css("background", "url(http://img.blutbad.ru/i/" + MALE_IMG + ")");
	}
}

function diffPeoples(){
	$("#left_str").removeClass();
	$("#right_str").removeClass();
	$("#left_dex").removeClass();
	$("#right_dex").removeClass();
	$("#left_suc").removeClass();
	$("#right_suc").removeClass();
	$("#left_end").removeClass();
	$("#right_end").removeClass();
	$("#left_intel").removeClass();
	$("#right_intel").removeClass();
	$("#left_wis").removeClass();
	$("#right_wis").removeClass();

	$("#left_price").removeClass();
	$("#right_price").removeClass();
	$("#left_hp").removeClass();
	$("#right_hp").removeClass();
	$("#left_pw").removeClass();
	$("#right_pw").removeClass();
	$("#left_uronmin").removeClass();
	$("#right_uronmin").removeClass();
	$("#left_uronmax").removeClass();
	$("#right_uronmax").removeClass();
	$("#left_krit").removeClass();
	$("#right_krit").removeClass();
	$("#left_ukrit").removeClass();
	$("#right_ukrit").removeClass();
	$("#left_uvor").removeClass();
	$("#right_uvor").removeClass();
	$("#left_uuvor").removeClass();
	$("#right_uuvor").removeClass();

	$("#left_b1").removeClass();
	$("#right_b1").removeClass();
	$("#left_b2").removeClass();
	$("#right_b2").removeClass();
	$("#left_b3").removeClass();
	$("#right_b3").removeClass();
	$("#left_b4").removeClass();
	$("#right_b4").removeClass();

	if (leftPeople.str > rightPeople.str) {
		$("#left_str").addClass("green");
		$("#right_str").addClass("red");
	} else if (leftPeople.str < rightPeople.str) {
		$("#left_str").addClass("red");
		$("#right_str").addClass("green");
	}
	if (leftPeople.dex > rightPeople.dex) {
		$("#left_dex").addClass("green");
		$("#right_dex").addClass("red");
	} else if (leftPeople.dex < rightPeople.dex) {
		$("#left_dex").addClass("red");
		$("#right_dex").addClass("green");
	}
	if (leftPeople.suc > rightPeople.suc) {
		$("#left_suc").addClass("green");
		$("#right_suc").addClass("red");
	} else if (leftPeople.suc < rightPeople.suc) {
		$("#left_suc").addClass("red");
		$("#right_suc").addClass("green");
	}
	if (leftPeople.end > rightPeople.end) {
		$("#left_end").addClass("green");
		$("#right_end").addClass("red");
	} else if (leftPeople.end < rightPeople.end) {
		$("#left_end").addClass("red");
		$("#right_end").addClass("green");
	}
	if (leftPeople.intel > rightPeople.intel) {
		$("#left_intel").addClass("green");
		$("#right_intel").addClass("red");
	} else if (leftPeople.intel < rightPeople.intel) {
		$("#left_intel").addClass("red");
		$("#right_intel").addClass("green");
	}
	if (leftPeople.wis > rightPeople.wis) {
		$("#left_wis").addClass("green");
		$("#right_wis").addClass("red");
	} else if (leftPeople.wis < rightPeople.wis) {
		$("#left_wis").addClass("red");
		$("#right_wis").addClass("green");
	}

	if (leftPeople.price > rightPeople.price) {
		$("#left_price").addClass("red");
		$("#right_price").addClass("green");
	} else if (leftPeople.price < rightPeople.price) {
		$("#left_price").addClass("green");
		$("#right_price").addClass("red");
	}
	if (leftPeople.hp > rightPeople.hp) {
		$("#left_hp").addClass("green");
		$("#right_hp").addClass("red");
	} else if (leftPeople.hp < rightPeople.hp) {
		$("#left_hp").addClass("red");
		$("#right_hp").addClass("green");
	}
	if (leftPeople.pw > rightPeople.pw) {
		$("#left_pw").addClass("green");
		$("#right_pw").addClass("red");
	} else if (leftPeople.pw < rightPeople.pw) {
		$("#left_pw").addClass("red");
		$("#right_pw").addClass("green");
	}
	if (leftPeople.uronmin > rightPeople.uronmin) {
		$("#left_uronmin").addClass("green");
		$("#right_uronmin").addClass("red");
	} else if (leftPeople.uronmin < rightPeople.uronmin) {
		$("#left_uronmin").addClass("red");
		$("#right_uronmin").addClass("green");
	}
	if (leftPeople.uronmax > rightPeople.uronmax) {
		$("#left_uronmax").addClass("green");
		$("#right_uronmax").addClass("red");
	} else if (leftPeople.uronmax < rightPeople.uronmax) {
		$("#left_uronmax").addClass("red");
		$("#right_uronmax").addClass("green");
	}
	if (leftPeople.krit > rightPeople.krit) {
		$("#left_krit").addClass("green");
		$("#right_krit").addClass("red");
	} else if (leftPeople.krit < rightPeople.krit) {
		$("#left_krit").addClass("red");
		$("#right_krit").addClass("green");
	}
	if (leftPeople.ukrit > rightPeople.ukrit) {
		$("#left_ukrit").addClass("green");
		$("#right_ukrit").addClass("red");
	} else if (leftPeople.ukrit < rightPeople.ukrit) {
		$("#left_ukrit").addClass("red");
		$("#right_ukrit").addClass("green");
	}
	if (leftPeople.uvor > rightPeople.uvor) {
		$("#left_uvor").addClass("green");
		$("#right_uvor").addClass("red");
	} else if (leftPeople.uvor < rightPeople.uvor) {
		$("#left_uvor").addClass("red");
		$("#right_uvor").addClass("green");
	}
	if (leftPeople.uuvor > rightPeople.uuvor) {
		$("#left_uuvor").addClass("green");
		$("#right_uuvor").addClass("red");
	} else if (leftPeople.uuvor < rightPeople.uuvor) {
		$("#left_uuvor").addClass("red");
		$("#right_uuvor").addClass("green");
	}

	if (leftPeople.b1 > rightPeople.b1) {
		$("#left_b1").addClass("green");
		$("#right_b1").addClass("red");
	} else if (leftPeople.b1 < rightPeople.b1) {
		$("#left_b1").addClass("red");
		$("#right_b1").addClass("green");
	}
	if (leftPeople.b2 > rightPeople.b2) {
		$("#left_b2").addClass("green");
		$("#right_b2").addClass("red");
	} else if (leftPeople.b2 < rightPeople.b2) {
		$("#left_b2").addClass("red");
		$("#right_b2").addClass("green");
	}
	if (leftPeople.b3 > rightPeople.b3) {
		$("#left_b3").addClass("green");
		$("#right_b3").addClass("red");
	} else if (leftPeople.b3 < rightPeople.b3) {
		$("#left_b3").addClass("red");
		$("#right_b3").addClass("green");
	}
	if (leftPeople.b4 > rightPeople.b4) {
		$("#left_b4").addClass("green");
		$("#right_b4").addClass("red");
	} else if (leftPeople.b4 < rightPeople.b4) {
		$("#left_b4").addClass("red");
		$("#right_b4").addClass("green");
	}
}

function saveComplect(people){
	var p = eval(people + 'People');
	if (p.bot > 0) return;

	var stats = new Array();
	var slots = new Array();

	for (var i = 0; i < STATS.length; i++) {
		var stat = STATS[i];
		var value = p[stat];

		stats.push(SHORT_STATS[i] + "_" + value);
	}

	for (var i = 0; i < SLOTS.length; i++) {
		var slot = SLOTS[i];

		var item = p.slots[slot];
		var req = item.requires;
		var prop = item.properties;
		var type = item.type;
		var num = item.num;

		if (type > 0) {
			var slot_str = SHORT_SLOTS[i] + "_" + type + "_" + num;
			if (num == CUSTOM_ITEM_NUM) {
				slot_str += '_' + item.name + '_' + item.image + '_' + prop.weight + '_' + item.durability + '_' + prop.price;
				slot_str += '_' + req.level + '_' + req.str + '_' + req.dex + '_' + req.suc + '_' + req.end + '_' + req.intel + '_' + req.wis;
				slot_str += '_' + prop.str + '_' + prop.dex + '_' + prop.suc + '_' + prop.intel;
				slot_str += '_' + prop.hp + '_' + prop.pw + '_' + prop.uronmin + '_' + prop.uronmax + '_' + prop.krit;
				slot_str += '_' + prop.ukrit + '_' + prop.uvor + '_' + prop.uuvor + '_' + prop.b1 + '_' + prop.b2 + '_' + prop.b3 + '_' + prop.b4;
			}
			slots.push(slot_str);
		}
	}

	var string = stats.join(":") + "=" + slots.join(":");

	if (string != "") {
		prompt("��������� ��� ������ � ��������� ���������:", string);
	} else {
		alert("... � �� �������� ��� ���������, � ��������� ��� �� ���, ���� �����, �������� �� ����. (�) ����� ������ ������");
	}
}

function loadstrComplect(people, entry) {
   // var entry = prompt("������� ������ � ��������� ���������:", "");

	if (entry) {
		var load = entry.replace(/\s+/g, '').replace(/\n/g, '').split('=');

		var p = eval(people + 'People');
		if (p.bot > 0) {
			delete p.bot;
			delete p.id;
			delete p.name;
			p.sex = "M";
			p.obraz = MALE_IMG;
		}
		var stats = load[0].split(":");
		var complect = load[1].split(":");

		p.slots = new peopleSlots();

		for (var i = 0; i < stats.length; i++) {
			var s = stats[i].split("_") || [];
			if (s.length == 2) {
				var stat = String(s[0]) || "";
				for (var j = 0; j < SHORT_STATS.length; j++) {
					if (SHORT_STATS[j] == stat) {
						stat = STATS[j]
					}
				}
				var value = Number(s[1]) || 0;

				if (stat != "") {
					p[stat] = value;
				}
			}
		}

		for (var i = 0; i < complect.length; i++) {
			var c = complect[i].split("_") || [];

			if (c.length >= 3) {
				var slot = String(c[0]) || "";
				for (var j = 0; j < SHORT_SLOTS.length; j++) {
					if (SHORT_SLOTS[j] == slot) {
						slot = SLOTS[j]
					}
				}
				var type = Number(c[1]) || 0;
				var num = Number(c[2]) || 0;

				if (type > 0) {
					if (c.length == 3) {
						var items = eval(slotByType[type] + '[' + type + ']');

						for (var j = 0; j < items.length; j++) {
							var item = items[j];

							if (item.num == num) {
								p.slots[slot] = item;
								break;
							}
						}
					}
					else {
						var item = new createItem(type,num,c[3],c[4],c[5],c[6],c[7],c[8],c[9],c[10],c[11],c[12],c[13],c[14],c[15],c[16],c[17],c[18],
							c[19],c[20],c[21],c[22],c[23],c[24],c[25],c[26],c[27],c[28],c[29],c[30]
						);
						p.slots[slot] = item;
					}
				}
			}
		}

		updatePeople(people);
	}
}

function loadComplect(people) {
    var entry = prompt("������� ������ � ��������� ���������:", "");

	if (entry) {
		var load = entry.replace(/\s+/g, '').replace(/\n/g, '').split('=');

		var p = eval(people + 'People');
		if (p.bot > 0) {
			delete p.bot;
			delete p.id;
			delete p.name;
			p.sex = "M";
			p.obraz = MALE_IMG;
		}
		var stats = load[0].split(":");
		var complect = load[1].split(":");

		p.slots = new peopleSlots();

		for (var i = 0; i < stats.length; i++) {
			var s = stats[i].split("_") || [];
			if (s.length == 2) {
				var stat = String(s[0]) || "";
				for (var j = 0; j < SHORT_STATS.length; j++) {
					if (SHORT_STATS[j] == stat) {
						stat = STATS[j]
					}
				}
				var value = Number(s[1]) || 0;

				if (stat != "") {
					p[stat] = value;
				}
			}
		}

		for (var i = 0; i < complect.length; i++) {
			var c = complect[i].split("_") || [];

			if (c.length >= 3) {
				var slot = String(c[0]) || "";
				for (var j = 0; j < SHORT_SLOTS.length; j++) {
					if (SHORT_SLOTS[j] == slot) {
						slot = SLOTS[j]
					}
				}
				var type = Number(c[1]) || 0;
				var num = Number(c[2]) || 0;

				if (type > 0) {
					if (c.length == 3) {
						var items = eval(slotByType[type] + '[' + type + ']');

						for (var j = 0; j < items.length; j++) {
							var item = items[j];

							if (item.num == num) {
								p.slots[slot] = item;
								break;
							}
						}
					}
					else {
						var item = new createItem(type,num,c[3],c[4],c[5],c[6],c[7],c[8],c[9],c[10],c[11],c[12],c[13],c[14],c[15],c[16],c[17],c[18],
							c[19],c[20],c[21],c[22],c[23],c[24],c[25],c[26],c[27],c[28],c[29],c[30]
						);
						p.slots[slot] = item;
					}
				}
			}
		}

		updatePeople(people);
	}
}

function checkReqs(people){
	var p = eval(people + 'People');
	var modified = 0;

	for (var slot in p.slots) {
		var item = p.slots[slot];

		for (var req in item.requires) {
			if (p[req] < item.requires[req]) {
				var image = slot;

				if (slot == 'ring1' || slot == 'ring2' || slot == 'ring3' || slot == 'ring4') {
					image = 'ring';
				} else if (slot == 'rune1' || slot == 'rune2' || slot == 'rune3' || slot == 'rune4' || slot == 'rune5' || slot == 'rune6') {
					image = 'rune';
				} else if (slot == 'stone1' || slot == 'stone2' || slot == 'stone3' || slot == 'stone4' || slot == 'stone5' || slot == 'stone6' || slot == 'stone7') {
					image = 'stone';
				}

				// ������� �������
				p.slots[slot] = new createItem(0, 0, '', 'char_' + image + '.gif', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
				updatePeople(people);

				modified = 1;

				break;
			}
		}
	}

	return modified;
}

function doFight(){
	var l = leftPeople;
	var r = rightPeople;

	// Requirements
	if ($('#left_fight_reqs').attr('checked') == true) {
		checkReqs('left');
	}

	if ($('#right_fight_reqs').attr('checked') == true) {
		checkReqs('right');
	}

	// Runes
	var l_hp_runes = new Array();
	var l_pw_runes = new Array();
	var r_hp_runes = new Array();
	var r_pw_runes = new Array();

	/*for (var i = 1; i <= 9; i++) {
		var slot = 'rune' + i;

		if (l.slots[slot].properties.hp > 0) {
			for (var j = 1; j <= l.slots[slot].durability; j++) {
				l_hp_runes.push(l.slots[slot].properties.hp);
			}
		}
		if (l.slots[slot].properties.pw > 0) {
			for (var j = 1; j <= l.slots[slot].durability; j++) {
				l_pw_runes.push(l.slots[slot].properties.pw);
			}
		}
		if (r.slots[slot].properties.hp > 0) {
			for (var j = 1; j <= r.slots[slot].durability; j++) {
				r_hp_runes.push(r.slots[slot].properties.hp);
			}
		}
		if (r.slots[slot].properties.pw > 0) {
			for (var j = 1; j <= r.slots[slot].durability; j++) {
				r_pw_runes.push(r.slots[slot].properties.pw);
			}
		}
	}*/

	$("#draws").html('<img src="http://img.blutbad.ru/i/loader.gif" />');
	$("#left_wins").html('<img src="http://img.blutbad.ru/i/loader.gif" />');
	$("#right_wins").html('<img src="http://img.blutbad.ru/i/loader.gif" />');

	$("#fight_img").attr("src", "http://img.blutbad.ru/i/fitting_tool_button_none.gif");
	f_i = 0;

	$.post("/fitting_room.php", {
		cmd: "fight",
		is_ajax: 1,
		partner_type : $('#partner_type').val(),
		left: l.bot + ":" + l.id + ":" + l.str + ":" + l.dex + ":" + l.suc + ":" + l.hp + ":" + l.pw + ":" + l.uronmin + ":" + l.uronmax + ":" + l.krit + ":" + l.ukrit + ":" + l.uvor + ":" + l.uuvor + ":" + l.b1 + ":" + l.b2 + ":" + l.b3 + ":" + l.b4 + ":" + l_hp_runes.join("_") + ":" + l_pw_runes.join("_"),
		right: r.bot + ":" + r.id + ":" + r.str + ":" + r.dex + ":" + r.suc + ":" + r.hp + ":" + r.pw + ":" + r.uronmin + ":" + r.uronmax + ":" + r.krit + ":" + r.ukrit + ":" + r.uvor + ":" + r.uuvor + ":" + r.b1 + ":" + r.b2 + ":" + r.b3 + ":" + r.b4 + ":" + r_hp_runes.join("_") + ":" + r_pw_runes.join("_"),
		pos_left: $('input[name="left_fight_position"]:checked').val(),
		pos_right: $('input[name="right_fight_position"]:checked').val(),
		ext: $('[name="ext"]:checked').val(),
		counts: $("#fights_counts").val()
	}, function(xml){
		var draws = $("draws", xml).text() || 0;
		var left = $("left", xml).text() || 0;
		var right = $("right", xml).text() || 0;
		var error = $("error", xml).text() || "";
		var log = $("log", xml).text() || "";

		$("#draws").html(draws);
		$("#left_wins").html(left);
		$("#right_wins").html(right);

		var sl = $("sleft", xml);
		var sr = $("sright", xml);
		if (sl && sr) {
			$("#left_total_dmg").html(sl.attr('total_dmg'));
			$("#left_total_dmgt").html(sl.attr('total_dmgt'));
			$("#left_total_kick").html(sl.attr('total_kick'));
			$("#left_suc_kick").html(sl.attr('suc_kick'));
			$("#left_suc_uvor").html(sl.attr('suc_uvor'));
			$("#left_suc_krit").html(sl.attr('suc_krit'));
			$("#left_suc_kritt").html(sl.attr('suc_kritt'));
			$("#left_suc_kub").html(sl.attr('suc_kub'));
			$("#left_suc_hp").html(sl.attr('suc_hp'));
			$("#left_suc_pw").html(sl.attr('suc_pw'));

			$("#right_total_dmg").html(sr.attr('total_dmg'));
			$("#right_total_dmgt").html(sr.attr('total_dmgt'));
			$("#right_total_kick").html(sr.attr('total_kick'));
			$("#right_suc_kick").html(sr.attr('suc_kick'));
			$("#right_suc_uvor").html(sr.attr('suc_uvor'));
			$("#right_suc_krit").html(sr.attr('suc_krit'));
			$("#right_suc_kritt").html(sr.attr('suc_kritt'));
			$("#right_suc_kub").html(sr.attr('suc_kub'));
			$("#right_suc_hp").html(sr.attr('suc_hp'));
			$("#right_suc_pw").html(sr.attr('suc_pw'));

		}
			$("#log").html(log.replace(/2br/g, "<br>").replace(/uleft/g, "<b class='side-1'>�����</b>").replace(/uright/g, "<b class='side-2'>������</b>"));

		  //	$("#log").html(log.replace(/be/g, "</b>"));

		$("#fight_img").attr("src", "http://img.blutbad.ru/i/fitting_tool_button_primary.gif");
		f_i = 1;


		if (error) {
			alert(error);
		} else {
			var user_money = $("user_money", xml).text() || 0;

			if (draws == 0 && left == 0 && right == 0 && user_money == 0) {
				alert("�� ��������� ������������ ����� �������� � ������.\n��� IP �������� ������������!");
			} else {
				$("#user_money").html(user_money);
			}
		}
	}, 'xml');
}

function calcStats(people){
	var p = eval(people + 'People');
	if (p.bot > 0) return;

	for (var i in p.slots) {
		var item = p.slots[i];

		for (var stat in item.requires) {
			if (p[stat] < item.requires[stat]) {
				var stat_base = REQS[stat];
				var stat_delta = p[stat] - p[stat_base];
				var delta = item.requires[stat] - p[stat];

				p[stat_base] = p[stat_base] + delta;
				p[stat] = p[stat_base] + stat_delta;
			}
		}
	}

	updatePeople(people);
}

function wearCustomItemDialog(people) {
	var p = eval(people + 'People');
	if (p.bot > 0) return;
	var dialog = getDialog();

	dialog.dialog('option', 'title', '������ ������������ ����');

	var value = '<div>';

	value += '<div style="text-align: left;">';

	value += '<table class="custom-params">';
	value += '<tr><td>��������:</td><td colspan="5"><input type="text" class="text" size="30" value="��������� �������" id="custom_name" /></td></tr>';

	value += '<td>�����:</td><td><select id="custom_type">';
	value += '<optgroup label="������">';
	for (var i=0;i<typesWeapons.length;i++) {
		value += '<option value="'+typesWeapons[i].type+'">'+typesWeapons[i].name+'</option>';
	}
	value += '</optgroup>';
	value += '<optgroup label="�����">';
	for (var i=0;i<typesArmors.length;i++) {
		value += '<option value="'+typesArmors[i].type+'">'+typesArmors[i].name+'</option>';
	}
	value += '</optgroup>';
	value += '<optgroup label="���������">';
	for (var i=0;i<typesMagics.length;i++) {
		if (typesMagics[i].type == 20 || typesMagics[i].type == 50) continue;
		value += '<option value="'+typesMagics[i].type+'">'+typesMagics[i].name+'</option>';
	}
	value += '</optgroup>';
	value += '</select></td><td>������� ������������:</td><td><input type="text" class="text" size="3" value="0" id="custom_ws" /></td><td>����.����:</td><td><input type="text" class="text" size="3" value="0" id="custom_uronmax" /></td></tr>';

	value += '<tr><td>����:</td><td><input type="text" class="text" size="3" value="0" id="custom_strength" /></td><td>����������� ����:</td><td><input type="text" class="text" size="3" value="0" id="custom_krit" /></td><td>����� ������:</td><td><input type="text" class="text" size="3" value="0" id="custom_b1" /></td></tr>';

	value += '<tr><td>��������:</td><td><input type="text" class="text" size="3" value="0" id="custom_dexterity" /></td><td>������ ������������ �����:</td><td><input type="text" class="text" size="3" value="0" id="custom_ukrit" /></td><td>����� �������:</td><td><input type="text" class="text" size="3" value="0" id="custom_b2" /></td></tr>';

	value += '<tr><td>��������:</td><td><input type="text" class="text" size="3" value="0" id="custom_success" /></td><td>�����������:</td><td><input type="text" class="text" size="3" value="0" id="custom_uvor" /></td><td>����� �����:</td><td><input type="text" class="text" size="3" value="0" id="custom_b3" /></td></tr>';

	value += '<tr><td>���������:</td><td><input type="text" class="text" size="3" value="0" id="custom_intelligence" /></td><td>������ �����������:</td><td><input type="text" class="text" size="3" value="0" id="custom_uuvor" /></td><td>����� ���:</td><td><input type="text" class="text" size="3" value="0" id="custom_b4" /></td></tr>';

	value += '<tr><td>������� �����:</td><td><input type="text" class="text" size="3" value="0" id="custom_hp" /></td><td>���.����:</td><td><input type="text" class="text" size="3" value="0" id="custom_uronmin" /></td></tr>';

	value += '</table>';

	value += '<input type="button" class="xbbutton" value="��" id="custom_ok" />';
	value += '</div>';

	value += '</div>';

	dialog.html(value);

	dialog.dialog('option', 'width', 640);
	dialog.dialog('open');

	jQuery("#custom_ok").click(function() {
		var type = jQuery("#custom_type").val();
		var num = CUSTOM_ITEM_NUM;
		var slot = slotByType[type];
		var slot0 = 0;
		if (!slot) return;

		var name = jQuery("#custom_name").val() || "��������� �������";

        if (jQuery("#custom_name").val().match(/</)) {
          alert("� �������� ���������� ������������ �������"); return;
        }

		var strength = Number(jQuery("#custom_strength").val()) || 0;
		var dexterity = Number(jQuery("#custom_dexterity").val()) || 0;
		var success = Number(jQuery("#custom_success").val()) || 0;
		var intelligence = Number(jQuery("#custom_intelligence").val()) || 0;
		var hp = Number(jQuery("#custom_hp").val()) || 0;
		var ws = Number(jQuery("#custom_ws").val()) || 0;
		var krit = Number(jQuery("#custom_krit").val()) || 0;
		var ukrit = Number(jQuery("#custom_ukrit").val()) || 0;
		var uvor = Number(jQuery("#custom_uvor").val()) || 0;
		var uuvor = Number(jQuery("#custom_uuvor").val()) || 0;
		var uronmin = Number(jQuery("#custom_uronmin").val()) || 0;
		var uronmax = Number(jQuery("#custom_uronmax").val()) || 0;
		var b1 = Number(jQuery("#custom_b1").val()) || 0;
		var b2 = Number(jQuery("#custom_b2").val()) || 0;
		var b3 = Number(jQuery("#custom_b3").val()) || 0;
		var b4 = Number(jQuery("#custom_b4").val()) || 0;

		var item = new createItem(
			type,num,name,TYPE_CODES[type]+"/"+num+".gif",0,1,0,0,0,0,0,0,0,0,strength,dexterity,success,intelligence,
			hp,ws,uronmin,uronmax,krit,ukrit,uvor,uuvor,b1,b2,b3,b4
		);

		var found = 0;
		for (var slot0 = 0;slot0 <= 7; slot0++) {
			var old_item = p.slots[slot+(slot0 ? slot0 : '')];
			if (old_item && old_item.image == 'char_' + slot + '.gif') {
				found = 1;
				break;
			}
		}
		if (!found) {
			if (p.slots[slot]) slot0 = 0;
			else slot0 = 1;
			unwearItem(people,slot,slot0);
		}
		p.slots[slot+(slot0 ? slot0 : '')] = item;

		updatePeople(people);
		showInfo(people, people + 'People.slots.' + slot + (slot0 || ''), slot0);
		dialog.dialog('close');
	});
}

/* Events */

$(function(){
	$("#fight_img").hover(
		function(){ // Over
			if (f_i == 1) {
				this.src = 'http://img.blutbad.ru/i/fitting_tool_button_hovered.gif';
			} else {
				this.src = 'http://img.blutbad.ru/i/fitting_tool_button_none.gif';
			}
		},
		function(){ // Out
			if (f_i == 1) {
				this.src = 'http://img.blutbad.ru/i/fitting_tool_button_primary.gif';
			} else {
				this.src = 'http://img.blutbad.ru/i/fitting_tool_button_none.gif';
			}
		}
	).click(function(){
		var msg = '�� ������������� ������ �������� �������� ��������?';

		if ($('#left_fight_reqs').attr('checked') == true || $('#right_fight_reqs').attr('checked') == true) {
			msg = msg + '\n\n��������! ��� ��������, �� ��������������� �����������, ����� ����� � ���������.';
		}

		//if (confirm(msg)) {}
			doFight();

	});

	$('.inv-items li').mouseout(function(){
		hideInfo();
	});

	$("#left_obraz").click(function(){
		changeSex('left');
	});
	$("#right_obraz").click(function(){
		changeSex('right');
	});

	$("#left_rune1").mouseover(function(){
		showInfo('left', 'leftPeople.slots.rune1', 1);
	}).click(function(){
		unwearItem('left', 'rune', 1);
	});
	$("#left_rune2").mouseover(function(){
		showInfo('left', 'leftPeople.slots.rune2', 2);
	}).click(function(){
		unwearItem('left', 'rune', 2);
	});
	$("#left_rune3").mouseover(function(){
		showInfo('left', 'leftPeople.slots.rune3', 3);
	}).click(function(){
		unwearItem('left', 'rune', 3);
	});
	$("#left_rune4").mouseover(function(){
		showInfo('left', 'leftPeople.slots.rune4', 4);
	}).click(function(){
		unwearItem('left', 'rune', 4);
	});
	$("#left_rune5").mouseover(function(){
		showInfo('left', 'leftPeople.slots.rune5', 5);
	}).click(function(){
		unwearItem('left', 'rune', 5);
	});
	$("#left_rune6").mouseover(function(){
		showInfo('left', 'leftPeople.slots.rune6', 6);
	}).click(function(){
		unwearItem('left', 'rune', 6);
	});
	$("#left_rune7").mouseover(function(){
		showInfo('left', 'leftPeople.slots.rune7', 7);
	}).click(function(){
		unwearItem('left', 'rune', 7);
	});
	$("#left_rune8").mouseover(function(){
		showInfo('left', 'leftPeople.slots.rune8', 8);
	}).click(function(){
		unwearItem('left', 'rune', 8);
	});
	$("#left_rune9").mouseover(function(){
		showInfo('left', 'leftPeople.slots.rune9', 9);
	}).click(function(){
		unwearItem('left', 'rune', 9);
	});

	$("#left_helmet").mouseover(function(){
		showInfo('left', 'leftPeople.slots.helmet');
	}).click(function(){
		unwearItem('left', 'helmet');
	});
	$("#left_armor").mouseover(function(){
		showInfo('left', 'leftPeople.slots.armor');
	}).click(function(){
		unwearItem('left', 'armor');
	});
	$("#left_belt").mouseover(function(){
		showInfo('left', 'leftPeople.slots.belt');
	}).click(function(){
		unwearItem('left', 'belt');
	});
	$("#left_shoes").mouseover(function(){
		showInfo('left', 'leftPeople.slots.shoes');
	}).click(function(){
		unwearItem('left', 'shoes');
	});

	$("#left_bracelet").mouseover(function(){
		showInfo('left', 'leftPeople.slots.bracelet');
	}).click(function(){
		unwearItem('left', 'bracelet');
	});
	$("#left_gloves").mouseover(function(){
		showInfo('left', 'leftPeople.slots.gloves');
	}).click(function(){
		unwearItem('left', 'gloves');
	});
	$("#left_weapon").mouseover(function(){
		showInfo('left', 'leftPeople.slots.weapon');
	}).click(function(){
		unwearItem('left', 'weapon');
	});
	$("#left_shield").mouseover(function(){
		showInfo('left', 'leftPeople.slots.shield');
	}).click(function(){
		unwearItem('left', 'shield');
	});
	$("#left_earring").mouseover(function(){
		showInfo('left', 'leftPeople.slots.earring');
	}).click(function(){
		unwearItem('left', 'earring');
	});
	$("#left_necklace").mouseover(function(){
		showInfo('left', 'leftPeople.slots.necklace');
	}).click(function(){
		unwearItem('left', 'necklace');
	});

	$("#left_pants").mouseover(function(){
		showInfo('left', 'leftPeople.slots.pants');
	}).click(function(){
		unwearItem('left', 'pants');
	});

	/*$("#left_pocket1").mouseover(function(){
		showInfo('left', 'leftPeople.slots.pocket1', 1);
	}).click(function(){
		unwearItem('left', 'pocket', 1);
	});
	$("#left_pocket2").mouseover(function(){
		showInfo('left', 'leftPeople.slots.pocket2', 2);
	}).click(function(){
		unwearItem('left', 'pocket', 2);
	});*/

	$("#left_ring1").mouseover(function(){
		showInfo('left', 'leftPeople.slots.ring1', 1);
	}).click(function(){
		unwearItem('left', 'ring', 1);
	});
	$("#left_ring2").mouseover(function(){
		showInfo('left', 'leftPeople.slots.ring2', 2);
	}).click(function(){
		unwearItem('left', 'ring', 2);
	});
	$("#left_ring3").mouseover(function(){
		showInfo('left', 'leftPeople.slots.ring3', 3);
	}).click(function(){
		unwearItem('left', 'ring', 3);
	});
	$("#left_ring4").mouseover(function(){
		showInfo('left', 'leftPeople.slots.ring4', 4);
	}).click(function(){
		unwearItem('left', 'ring', 4);
	});

   /*	$("#left_shoulder").mouseover(function(){
		showInfo('left', 'leftPeople.slots.shoulder');
	}).click(function(){
		unwearItem('left', 'shoulder');
	});*/

	$("#left_stone1").mouseover(function(){
		showInfo('left', 'leftPeople.slots.stone1', 1);
	}).click(function(){
		unwearItem('left', 'stone', 1);
	});
	$("#left_stone2").mouseover(function(){
		showInfo('left', 'leftPeople.slots.stone2', 2);
	}).click(function(){
		unwearItem('left', 'stone', 2);
	});
	$("#left_stone3").mouseover(function(){
		showInfo('left', 'leftPeople.slots.stone3', 3);
	}).click(function(){
		unwearItem('left', 'stone', 3);
	});
	$("#left_stone4").mouseover(function(){
		showInfo('left', 'leftPeople.slots.stone4', 4);
	}).click(function(){
		unwearItem('left', 'stone', 4);
	});
	$("#left_stone5").mouseover(function(){
		showInfo('left', 'leftPeople.slots.stone5', 5);
	}).click(function(){
		unwearItem('left', 'stone', 5);
	});
	$("#left_stone6").mouseover(function(){
		showInfo('left', 'leftPeople.slots.stone6', 6);
	}).click(function(){
		unwearItem('left', 'stone', 6);
	});
	$("#left_stone7").mouseover(function(){
		showInfo('left', 'leftPeople.slots.stone7', 7);
	}).click(function(){
		unwearItem('left', 'stone', 7);
	});

	$("#left_items_list").change(function(){
		updateItemsList('left', this.value);
	});
	$("#right_items_list").change(function(){
		updateItemsList('right', this.value);
	});
	$("#left_level").change(function(){
		updateAbility('left', 'level', this.value);
	});
	$("#right_level").change(function(){
		updateAbility('right', 'level', this.value);
	});

	$("#left_strength").change(function(){
		updateAbility('left', 'strength', this.value);
	});
	$("#left_dexterity").change(function(){
		updateAbility('left', 'dexterity', this.value);
	});
	$("#left_success").change(function(){
		updateAbility('left', 'success', this.value);
	});
	$("#left_endurance").change(function(){
		updateAbility('left', 'endurance', this.value);
	});
	$("#left_intelligence").change(function(){
		updateAbility('left', 'intelligence', this.value);
	});
	$("#left_wisdom").change(function(){
		updateAbility('left', 'wisdom', this.value);
	});

	$("#right_strength").change(function(){
		updateAbility('right', 'strength', this.value);
	});
	$("#right_dexterity").change(function(){
		updateAbility('right', 'dexterity', this.value);
	});
	$("#right_success").change(function(){
		updateAbility('right', 'success', this.value);
	});
	$("#right_endurance").change(function(){
		updateAbility('right', 'endurance', this.value);
	});
	$("#right_intelligence").change(function(){
		updateAbility('right', 'intelligence', this.value);
	});
	$("#right_wisdom").change(function(){
		updateAbility('right', 'wisdom', this.value);
	});

	$("#right_rune1").mouseover(function(){
		showInfo('right', 'rightPeople.slots.rune1', 1);
	}).click(function(){
		unwearItem('right', 'rune', 1);
	});
	$("#right_rune2").mouseover(function(){
		showInfo('right', 'rightPeople.slots.rune2', 2);
	}).click(function(){
		unwearItem('right', 'rune', 2);
	});
	$("#right_rune3").mouseover(function(){
		showInfo('right', 'rightPeople.slots.rune3', 3);
	}).click(function(){
		unwearItem('right', 'rune', 3);
	});
	$("#right_rune4").mouseover(function(){
		showInfo('right', 'rightPeople.slots.rune4', 4);
	}).click(function(){
		unwearItem('right', 'rune', 4);
	});
	$("#right_rune5").mouseover(function(){
		showInfo('right', 'rightPeople.slots.rune5', 5);
	}).click(function(){
		unwearItem('right', 'rune', 5);
	});
	$("#right_rune6").mouseover(function(){
		showInfo('right', 'rightPeople.slots.rune6', 6);
	}).click(function(){
		unwearItem('right', 'rune', 6);
	});

	$("#right_helmet").mouseover(function(){
		showInfo('right', 'rightPeople.slots.helmet');
	}).click(function(){
		unwearItem('right', 'helmet');
	});
	$("#right_armor").mouseover(function(){
		showInfo('right', 'rightPeople.slots.armor');
	}).click(function(){
		unwearItem('right', 'armor');
	});
	$("#right_belt").mouseover(function(){
		showInfo('right', 'rightPeople.slots.belt');
	}).click(function(){
		unwearItem('right', 'belt');
	});
	$("#right_shoes").mouseover(function(){
		showInfo('right', 'rightPeople.slots.shoes');
	}).click(function(){
		unwearItem('right', 'shoes');
	});

	$("#right_bracelet").mouseover(function(){
		showInfo('right', 'rightPeople.slots.bracelet');
	}).click(function(){
		unwearItem('right', 'bracelet');
	});
	$("#right_gloves").mouseover(function(){
		showInfo('right', 'rightPeople.slots.gloves');
	}).click(function(){
		unwearItem('right', 'gloves');
	});
	$("#right_weapon").mouseover(function(){
		showInfo('right', 'rightPeople.slots.weapon');
	}).click(function(){
		unwearItem('right', 'weapon');
	});
	$("#right_shield").mouseover(function(){
		showInfo('right', 'rightPeople.slots.shield');
	}).click(function(){
		unwearItem('right', 'shield');
	});
	$("#right_earring").mouseover(function(){
		showInfo('right', 'rightPeople.slots.earring');
	}).click(function(){
		unwearItem('right', 'earring');
	});
	$("#right_necklace").mouseover(function(){
		showInfo('right', 'rightPeople.slots.necklace');
	}).click(function(){
		unwearItem('right', 'necklace');
	});

/*	$("#right_pants").mouseover(function(){
		showInfo('right', 'rightPeople.slots.pants');
	}).click(function(){
		unwearItem('right', 'pants');
	});*/

	/*$("#right_pocket1").mouseover(function(){
		showInfo('right', 'rightPeople.slots.pocket1', 1);
	}).click(function(){
		unwearItem('right', 'pocket', 1);
	});
	$("#right_pocket2").mouseover(function(){
		showInfo('right', 'rightPeople.slots.pocket2', 2);
	}).click(function(){
		unwearItem('right', 'pocket', 2);
	});*/

	$("#right_ring1").mouseover(function(){
		showInfo('right', 'rightPeople.slots.ring1', 1);
	}).click(function(){
		unwearItem('right', 'ring', 1);
	});
	$("#right_ring2").mouseover(function(){
		showInfo('right', 'rightPeople.slots.ring2', 2);
	}).click(function(){
		unwearItem('right', 'ring', 2);
	});
	$("#right_ring3").mouseover(function(){
		showInfo('right', 'rightPeople.slots.ring3', 3);
	}).click(function(){
		unwearItem('right', 'ring', 3);
	});
	$("#right_ring4").mouseover(function(){
		showInfo('right', 'rightPeople.slots.ring4', 4);
	}).click(function(){
		unwearItem('right', 'ring', 4);
	});

   /*	$("#right_shoulder").mouseover(function(){
		showInfo('right', 'rightPeople.slots.shoulder');
	}).click(function(){
		unwearItem('right', 'shoulder');
	});*/

	$("#right_stone1").mouseover(function(){
		showInfo('right', 'rightPeople.slots.stone1', 1);
	}).click(function(){
		unwearItem('right', 'stone', 1);
	});
	$("#right_stone2").mouseover(function(){
		showInfo('right', 'rightPeople.slots.stone2', 2);
	}).click(function(){
		unwearItem('right', 'stone', 2);
	});
	$("#right_stone3").mouseover(function(){
		showInfo('right', 'rightPeople.slots.stone3', 3);
	}).click(function(){
		unwearItem('right', 'stone', 3);
	});
	$("#right_stone4").mouseover(function(){
		showInfo('right', 'rightPeople.slots.stone4', 4);
	}).click(function(){
		unwearItem('right', 'stone', 4);
	});
	$("#right_stone5").mouseover(function(){
		showInfo('right', 'rightPeople.slots.stone5', 5);
	}).click(function(){
		unwearItem('right', 'stone', 5);
	});
	$("#right_stone6").mouseover(function(){
		showInfo('right', 'rightPeople.slots.stone6', 6);
	}).click(function(){
		unwearItem('right', 'stone', 6);
	});
	$("#right_stone7").mouseover(function(){
		showInfo('right', 'rightPeople.slots.stone7', 7);
	}).click(function(){
		unwearItem('right', 'stone', 7);
	});

	$(".stats_input").attr("value", "0");
});

$(function(){
	updatePeople('left');
	updatePeople('right');
});

/* Arrays */

armor = new Array();
armor[10] = new Array();
belt = new Array();
belt[12] = new Array();
bracelet = new Array();
bracelet[17] = new Array();
earring = new Array();
earring[15] = new Array();
gloves = new Array();
gloves[11] = new Array();
helmet = new Array();
helmet[8] = new Array();
necklace = new Array();
necklace[14] = new Array();
ring = new Array();
ring[13] = new Array();
shield = new Array();
shield[16] = new Array();
shoes = new Array();
shoes[9] = new Array();
stone = new Array();
stone[20] = new Array();
weapon = new Array();
weapon[1] = new Array();
weapon[2] = new Array();
weapon[3] = new Array();
weapon[4] = new Array();
weapon[5] = new Array();
weapon[6] = new Array();
weapon[7] = new Array();
weapon[18] = new Array();
rune = new Array();
pants = new Array();
pants[19] = new Array();
rune[50] = new Array();
//pocket = new Array();
//pocket[91] = new Array();
//shoulder = new Array();
//shoulder[23] = new Array();

leftPeople  = new createPeople();
rightPeople = new createPeople();
