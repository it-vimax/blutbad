function translate2(){
	var s = new Array();
	s = document.F1.txt.value.split(' ');
	for (var i = 0; i < s.length; i++) {
		if (s[i].indexOf("http://") < 0 && s[i].indexOf("@") < 0 && s[i].indexOf("www.") < 0 && !(s[i].charAt(0) == ":" && s[i].charAt(s[i].length - 1) == ":")) {
			s[i] = transliterate(s[i]);
		}
	}
	document.F1.txt.value = s.join(' ');
}

function translateComplaint(){
	var s = new Array();
	s = document.F2.txt.value.split(' ');
	for (var i = 0; i < s.length; i++) {
		if (s[i].indexOf("http://") < 0 && s[i].indexOf("@") < 0 && s[i].indexOf("www.") < 0 && !(s[i].charAt(0) == ":" && s[i].charAt(s[i].length - 1) == ":")) {
			s[i] = transliterate(s[i]);
		}
	}
	document.F2.txt.value = s.join(' ');
}

wins = new Array();

function wopen(wurl, trgt, h, w){
	win = wins[trgt];
	if (!win || win.closed) {
		wins[trgt] = window.open(wurl, trgt, 'height=' + h + ',width=' + w + ',resizable=no,scrollbars=yes,menubar=no,status=no,toolbar=no');
		win = wins[trgt];
	}
	win.focus();
	return false;
}

function WinRefresh(){
	self.focus();
	var location = document.location.href;
	var new_location = "/forum.php?";
	var t_start_pos = location.indexOf("t=");
	if (t_start_pos != -1) {
		var t_end_pos = location.indexOf("&", t_start_pos);
		if (t_end_pos != -1)
			new_location += location.substring(t_start_pos, t_end_pos) + "&";
		else
			new_location += location.substring(t_start_pos) + "&";
	}
	var subt_start_pos = location.indexOf("subt=");
	if (subt_start_pos != -1) {
		var subt_end_pos = location.indexOf("&", subt_start_pos);
		if (subt_end_pos != -1)
			new_location += location.substring(subt_start_pos, subt_end_pos) + "&";
		else
			new_location += location.substring(subt_start_pos) + "&";
	}
	var m_start_pos = location.indexOf("m=");
	if (m_start_pos != -1) {
		var m_end_pos = location.indexOf("&", m_start_pos);
		if (m_end_pos != -1)
			new_location += location.substring(m_start_pos, m_end_pos) + "&";
		else
			new_location += location.substring(m_start_pos) + "&";
	}
	new_location = new_location.substring(0, new_location.length - 1);

	document.location.href = new_location;
}

function SelfClose(stay){
	if (!stay && window.opener) {
		setTimeout(function(){
			if (!window.opener.closed) {
				window.opener.WinRefresh();
			}

			self.close();
		}, 2000);
	} else {
		setTimeout(function(){
			self.close();
		}, 2000);
	}
}

function translatestring(str){
	var s = new Array();
	s = str.split(' ');
	for (var i = 0; i < s.length; i++) {
		if (s[i].indexOf("http://") < 0 && s[i].indexOf("@") < 0 && s[i].indexOf("www.") < 0 && !(s[i].charAt(0) == ":" && s[i].charAt(s[i].length - 1) == ":")) {
			s[i] = transliterate(s[i]);
		}
	}
	return s.join(' ');
}

function translate(){
	document.F1.title.value = translatestring(document.F1.title.value);
	document.F1.txt.value = translatestring(document.F1.txt.value);
}

function processHighlight(){
	removeHighlight();

	var timestamp = '';
	var day = $('#highlight-date').val();
	var reg = new RegExp(/^(\d{2})\-(\d{2})\-(\d{4})$/);

	if (reg.test(day)) {
		timestamp += RegExp.$3 + RegExp.$2 + RegExp.$1;
	} else {
		alert('����������� ������� ����');

		return false;
	}

	var time = $('#highlight-time').val();
	if (!time) {
		time = '00:00';
	}

	var reg = new RegExp(/^(\d{2})\:(\d{2})$/);

	if (reg.test(time)) {
		timestamp += RegExp.$1 + RegExp.$2;
	} else {
		alert('����������� ������� �����');

		return false;
	}

	timestamp += '00';

	$('.topic .reply, .topic .content').each(function() {
		if(timestamp <= this.id) {
			$(this).addClass('highlight');
		}
	});

	if($('#highlight-leave:checked').length) {
		var arr = document.domain.split('.');
		var domain = '.' + arr[arr.length - 2] + '.' + arr[arr.length - 1];

		$.cookie('highlight-leave', day + ' ' + time, {
			domain: domain,
			expires: 365
		});
	}
}

function removeHighlight(){
	$('.topic .content, .topic .reply').removeClass('highlight');
	var arr = document.domain.split('.');
	var domain = '.' + arr[arr.length - 2] + '.' + arr[arr.length - 1];
	$.cookie('highlight-leave', null, {
		domain: domain
	});
}

$(function(){
	var dates = $('#search_date1, #search_date2').datepicker({
		maxDate: new Date(),
		minDate: new Date(2004, 1-1, 1),
		onSelect: function(selectedDate){
			var option = this.id == "search_date1" ? "minDate" : "maxDate";
			var instance = $(this).data("datepicker");
			var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	});

	if ($.mask) {
		$.mask.definitions['2'] = '[0-2]';
		$.mask.definitions['5'] = '[0-5]';
		$('#from-time, #till-time').mask('29:59:59',{
			completed: function(){
				var arr = this.val().split(':');
				if(Number(arr[0]) > 23 || Number(arr[1]) > 59 || Number(arr[2]) > 59) {
					alert('����������� ������� �����');
				}
			}
		});

		$('#highlight-time').mask('29:59',{
			completed: function(){
				var arr = this.val().split(':');
				if(Number(arr[0]) > 23 || Number(arr[1]) > 59) {
					alert('����������� ������� �����');
				}
			}
		});
	}

	$('#extended-search-toggle').click(function(){
		var link = $(this);
		if($('#extended-search:visible').length) {
			link.text('������� � ������������ ������');
			$('#search-extended').val(0);
		} else {
			link.text('������� � �������� ������ ');
			$('#search-extended').val(1);
		}
		$('#extended-search').slideToggle('fast');

		return false;
	});

	/*
	 * Topics
	 */
	$('#topics .topic-col').click(function(event) {
		if (!$(event.target).is('a')) {
			location.href = $(this).children('a')[0].href;
		}
	}).hover(
		function() {
			$(this).children('a').addClass('hover');
		},
		function() {
			$(this).children('a').removeClass('hover');
		}
	).children('a').removeClass('hover');
	/* end: Topics */

	/*
	 * New topic
	 */
	// ��������� ���������� ����� ����
	$('#new-topic').submit(function() {
		var errors = '';

		var title = $('#new-topic-title');
		if(!title.val()) {
			errors += '<li>����������� ���� ���������</li>';
		}

		var content = $('#new-topic-content');
		if(!content.val()) {
			errors += '<li>������ ���������</li>';
		}

		if(errors) {
			var notify = $.pnotify({
				pnotify_title: '��������!',
				pnotify_text: '<ul class="attention">' + errors + '</ul>',
				pnotify_width: '250px'
			});
			notify.appendTo('#notifies').click(notify.pnotify_remove);

			return false;
		} else {
			return true;
		}
	});

	$('#new-topic-translit').click(function() {
		$('#new-topic-title, #new-topic-content').each(function() {
			this.value = transliterate(this.value);
		});
	});
	/* end: New topic */

	/*
	 * Topic
	 */
	$('#highlight-process').click(processHighlight);
	$('#highlight-reset').click(removeHighlight);

	$('#highlight-date').datepicker({
		minDate: '-5y',
		maxDate: '+0'
	});

	if($('#highlight-block').length && $.cookie('highlight-leave')) {
		var arr = $.cookie('highlight-leave').split(' ');
		$('#highlight-date').val(arr[0]);
		$('#highlight-time').val(arr[1]);
		$('#highlight-leave')[0].checked = true;
		processHighlight();
	}

	$('.topic .message-link').click(function() {
		prompt("������ �� ���������", this.href);
		return false;
	});

	/*function initForm() {
		var dialog = $(this);

		dialog.find('form').ajaxForm({
			beforeSubmit: function() {
				var submit = dialog.find('input[type="submit"]')[0];
				submit.disabled = true;
			},
			data: {
				is_ajax: 1
			},
			success: initForm,
			target: dialog
		});
		dialog.dialog('open');
	}*/

	var popupWindows = {};

	$('.topic .actions .answer').click(function() {
		var url = $(this).attr('data');

		if (!popupWindows.reply || popupWindows.reply.closed) {
			var left = Math.floor(screen.width / 2 - 620 / 2);
			var top = Math.floor(screen.height / 2 - 420 / 2);

			popupWindows.reply = window.open(url, 'reply', 'height=420, left=' + left + ' menubar=no, resizable=no, scrollbars=yes, status=no, top=' + top + ' toolbar=no, width=620');
		} else {
			alert('���� ������ �� ��������� ��� �������! �������� ��� � ���������� ��� ���.');
			popupWindows.reply.focus();
		}

		return false;
	});

	$('.topic .actions .edit').click(function() {
		var url = $(this).attr('data');

		if (!popupWindows.edit || popupWindows.edit.closed) {
			var left = Math.floor(screen.width / 2 - 620 / 2);
			var top = Math.floor(screen.height / 2 - 420 / 2);

			popupWindows.edit = window.open(url, 'edit', 'height=420, left=' + left + ' menubar=no, resizable=no, scrollbars=yes, status=no, top=' + top + ' toolbar=no, width=620');
		} else {
			alert('���� ��������� ��������� ��� �������! �������� ��� � ���������� ��� ���.');
			popupWindows.edit.focus();
		}

		return false;
	});

	$('.topic .actions .complain').click(function() {
		var url = $(this).attr('data') + '&domain=' + document.domain;

		if (!popupWindows.complaint || popupWindows.complaint.closed) {
			var left = Math.floor(screen.width / 2 - 620 / 2);
			var top = Math.floor(screen.height / 2 - 440 / 2);

			popupWindows.complaint = window.open(url, 'complaint', 'height=440, left=' + left + ' menubar=no, resizable=no, scrollbars=yes, status=no, top=' + top + ' toolbar=no, width=620');
		} else {
			alert('���� ������ ��� �������! �������� ��� � ���������� ��� ���.');
			popupWindows.complaint.focus();
		}

		return false;
	});

	$('#reply-translit').click(function() {
		$('#reply-text').val(transliterate($('#reply-text').val()));
	});
	/* end: Topic */
});