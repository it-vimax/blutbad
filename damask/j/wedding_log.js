function getAlignName(align){
	if (align == 10) return '���� ����';
	if (align == 20) return '���� �����';
	if (align == 30) return '���� �����';
	if (align == 40) return '���� �������';
	if (align == 15) return '������ ����';
	if (align == 25) return '������ �����';
	if (align == 35) return '������ �����';
	if (align == 45) return '������ �������';
	if (align == 19) return '������� ����';
	if (align == 29) return '������� �����';
	if (align == 39) return '������� �����';
	if (align == 49) return '������� �������';

	return '';
}

function getKlan(klan, klanname) {
	if (klan.length > 0){
		var klanLink = 'http://lib.carnage.ru/clans/' + klan + '/';
		klan = '<a class="clan" href="' + klanLink + '" target="_blank">' +
				   '<img src="http://img.carnage.ru/i/klan/' + klan
             + '.gif" width="25" height="16" alt="' + klanname + '" /></a>';
	}

	return klan;
}


function w2(id, name, level, align, klan, klanname, guild, guildname, sex){

	var alignname = getAlignName(align);
	klan = getKlan(klan, klanname);

	if (guild.length > 0) {
		guild = '<img alt="' + guildname + '" class="guild" height="16" src="http://img.carnage.ru/i/guild/'
              + guild + '.gif" title="' + guildname + '" width="25" />';
	}

	if (align != 0) {
		align = '<img alt="' + alignname + '" class="align" height="16" src="http://img.carnage.ru/i/align'
              + align + '.gif" title="' + alignname + '" width="25" />';
	} else {
		align = "";
	}

    fitName = name;

    if (fitName.length > 9)
        fitName = fitName.substring(0, 9) + '...';

    fullStr = align + klan + guild + '<span class="short-nick">' + fitName + '</span><span class="long-nick">' + name + '</span> ' +
        '<img alt="" class="empty" src="http://img.carnage.ru/i/null.gif" height="16" width="1" />' +
		'<span class="level">[' + level + ']</span> ' +
		'<a class="info" href="inf.pl?user=' + name + '" target="_blank">' +
		'<img alt="���������� � ���������" height="12" src="http://img.carnage.ru/i/inf' +
        sex + '.gif" title="���������� � ���������" width="12" />' + '</a>';

    var nick = $('<div class="nickname">' + fullStr + '</div>').hover(
        function() {
            var defaultWidth = $(this).width();
            $(this).addClass('nickname-hover');
            var tmp = $(this).clone();
            tmp.appendTo('body').hide();
            var width = tmp.width() < defaultWidth ? defaultWidth : tmp.outerWidth();

            if($(this).closest('#married').length) {
                var marginLeft = -(width / 2 - 65);
            } else {
                var marginLeft = -(width / 2 - 68);
                if($.browser.msie) {
                    marginLeft -= 17;
                }
            }

            $(this).width(width).css('margin-left', marginLeft);

            var rightBorder = $(this).offset().left + width - $(window).width() - $(window).scrollLeft() - 7;
            if(rightBorder > 0) {
                $(this).css('margin-left', marginLeft - rightBorder - 19);
            } else {
                leftBorder = $(this).offset().left - 7;
                if(leftBorder < 0) {
                    $(this).css('margin-left', marginLeft - leftBorder);
                }

            }
        },
        function() {
            $(this).width('').removeClass('nickname-hover').css('margin-left', '');
        }
    );
    $('#user-' + id).prepend(nick);
}


function expand(el, fullStr){
    el.innerHTML = fullStr;
    el.style.zIndex = el.style.zIndex + 1;
    el.style.width = 270;

}

function collapse(el, fitName){
    el.innerHTML = fitName;
    el.style.zIndex = el.style.zIndex - 1;
    el.style.width = 100;
}
