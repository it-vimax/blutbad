$(function() {
	$( '.roll-tabs .roll-tabs' ).tabs( 'option', 'cookie', { expires: 1 / 24 / 4 } );

	$('.timeout').each(function(){
		var $this = $(this);
		var until = Number($this.attr('data'));

		if (until > 0) {
			$(this).countdown({
				format: 'yodhms',
				layout: '{y<}{yn} {yl} {y>}{o<}{on} {ol} {o>}{d<}{dn} {dl} {d>}{h<}{hn} {hl} {h>}{m<}{mn} {ml} {m>}{sn} {sl}',
				onTick: function(periods){
					var seconds = periods[0] * 31557600 + periods[1] * 2629800 + periods[2] * 604800 + periods[3] * 86400 + periods[4] * 3600 + periods[5] * 60 + periods[6];
					if (seconds < 3600) {
						$this.addClass('attention').css('font-weight', 'bold');
					}
				},
				until: until
			});
		}
	});

	$( '.action img' ).click( function() {
		$( '.inventory-table .auction-form form' ).remove();

		var oForm = $.buildForm( {
			'fields': [
				{
					'name': 'cmd',
					'type': 'hidden',
					'value': 'items.sell'
				}, {
					'name': 'nd',
					'type': 'hidden',
					'value': nd
				}, {
					'name': 'madein_entry',
					'type': 'hidden',
					'value': $( this ).closest( 'tr' )[ 0 ].id
				}, {
					'label': '��������� ����',
					'group': [
						{
							'type': 'text',
							'name': 'price',
							'size': 8
						}, {
							'type': 'message',
							'value': '��.'
						}
					]
				}, {
					'label': '�������� ����',
					'group': [
						{
							'type': 'text',
							'name': 'buyout',
							'size': 8
						}, {
							'type': 'message',
							'value': '��.'
						}
					]
				}, {
					'label': '����� ������',
					'type': 'message',
					'value': $( this ).data( 'deposit' )
				}, {
					'label': '������������',
					'name': 'duration',
					'type': 'radio',
					'list': [
						{
							'value':0,
							'label':'12 �����',
							'checked':true
						}, {
							'value':1,
							'label':'24 ����'
						}, {
							'value':2,
							'label':'48 �����'
						}
					]
				}, {
					'colspan': true,
					'group': [
						{
							'type': 'submit',
							'value': '���������'
						}, {
							'type': 'button',
							'value': '�������',
							'click': function() {
								$( this ).closest( 'form' ).remove();
							}
						}
					]
				}
			],
			'url': '/auction.php'
		} ).submit( function() {
			this.price.value = this.price.value.replace( /,/g, '.' );
			this.price.value = this.price.value.replace( /\s/g, '' );
			this.buyout.value = this.buyout.value.replace( /,/g, '.' );
			this.buyout.value = this.buyout.value.replace( /\s/g, '' );

			var price = parseFloat( this.price.value );

			if ( isNaN( price ) || price < 0.01 ) {
				alert( '������������ ��������� ����' );
				this.price.focus();

				return false;

			} else {
				this.price.value = price;
			}

			if ( this.buyout.value != '' ) {
				var buyout = parseFloat( this.buyout.value );

				if ( isNaN( buyout ) || buyout <= 0 ) {
					alert( '������������ �������� ����' );
					this.buyout.focus();

					return false;

				} else {
					this.buyout.value = buyout;
				}

				if ( buyout < price ) {
					alert( '�������� ���� ������ ���� �� ������ ���������' );
					this.buyout.focus();

					return false;

				}

			}

			return true;

		} );

		$( this ).closest( 'td' ).next().append( oForm );
		oForm[ 0 ].price.focus();

	});

});
