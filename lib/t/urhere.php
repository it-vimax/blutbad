<?php

/*
Copyright (c) 2000-2002, ActiveIntra.net
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided 
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the 
following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution. 

Neither the name of the ActiveIntra.net nor the names of its contributors may be used to endorse or 
promote products derived from this software without specific prior written permission.

THIS SOFTWARE 
IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/
 
 class URHere {
     var $sitename = "My URHere Demo"; //Changeme
     var $seperator = "&gt;"; // "&gt;" is ">", "&lt;" is "<", ":", ,"::", "|", "*""
     var $text = "";
     var $link = "";

  function Text($location) {
 	$path = explode("/", $location);
 	$c = 1;
 	while(list($key, $val) = each($path)) { 
 		if ($c > 1) {
 			$this->text .= " " . $this->seperator ." ";
            $val = str_replace("_"," ",$val); //Strip underscore; Thanks to Matt Williams: matt_AT_yewlands_DOT_com
            $val = str_replace("-"," ",$val); //Strip hyphen; Thanks to Matt Williams: matt_AT_yewlands_DOT_com
 		    $this->text .= ucwords(ereg_replace("\..*$","",$val)); //Strip ANY extension; Thanks to Matt Williams: matt_AT_yewlands_DOT_com
        }else {
            $this->text= $this->sitename;
        }
 		$c++;
 	}
 	return $this->text;
  }


  function Link($location) {
 	$path = explode("/", $location);
 	$c = 1;
 	while(list($key, $val) = each($path)) { 
 		if ($c > 1) {
 			$this->link .= " " . $this->seperator ." ";
			$link .= "/$val";
            $val = str_replace("_"," ",$val); //Strip underscore; Thanks to Matt Williams: matt_AT_yewlands_DOT_com
            $val = str_replace("-"," ",$val); //Strip hyphen; Thanks to Matt Williams: matt_AT_yewlands_DOT_com
			$this->link .= '<a href="'.$link.'">'. ucwords(ereg_replace("\..*$","",$val)) .'</a>'; //Strip ANY extension; Thanks to Matt Williams: matt_AT_yewlands_DOT_com
 		} else {
            $this->link = '<a href="/">' . $this->sitename . '</a>';
        }
		$c++;
 	}
 	return $this->link;
  }

 }
 
 ?>
