<?php
// -----------------------------------------------------------------------------------------------
// menu.php
// -----------------------------------------------------------------------------------------------
// Project:   Cascading menu with image buttons using on-the-fly image creation.
// Author:    Copyright (c) Urs <admin@circle.ch>
// Version:   1.0.0
// Update:    20-7-2000
// Licence:   ?
// PHP:       php-4.0.0-win32
//
// Reference: - "menu class" originally written by:      <zakj@i.am>,
//            - modified "menu class" for image use by:  <admin@circle.ch>
//
// Enjoy!
// -----------------------------------------------------------------------------------------------

class menu {

    var $image;
    var $items;
    var $open;
    var $closed;
    var $indent;
    var $name;

    function menu($image,
                  $name,
                  $open = '',
                  $closed = '',
                  $indent = ''
                 )
    {
        $this->items  = array();
        $this->image   = $image;
        $this->name = $name;
        $this->open   = $open;
        $this->closed = $closed;
        $this->indent = $indent;
    }

    function add($image, $href = "", $target = "") {
        $n = count($this->items);

        if (is_object($image)) {
            $this->items[$n] = $image;
        } else {
            $this->items[$n]['image'] = $image;
            $this->items[$n]['href'] = $href;
            $this->items[$n]['target'] = $target;
        }
    }

    function show($nest = 0) {
        $urlimage = strtr ($this->name, ' ', '_');
        $indent = '';
        global $$urlimage;
        global $PHP_SELF;
        global $QUERY_STRING;

        if ($nest) { // > php4
            if(function_exists("str_repeat")){
                $indent = str_repeat($this->indent, $nest);
            } else { // php3
                for($i=0; $i<$nest; $i++){
                    $indent = $indent . $this->indent;
                }
            }
        }

        if (isset($$urlimage)) {
            printf('%s<a href="%s?%s">%s</a><br>',
                   $indent . $this->open,
                   basename ($PHP_SELF),
                   ereg_replace("{$urlimage}=&", '', $QUERY_STRING),
                   $this->image);
            echo "\n";

            while (list(,$item) = each($this->items)) {
                if (is_object($item)) {
                    $item->show($nest + 1);
                } else {
                    printf('%s<a href="%s"%s>%s</a><br>',
                           $indent . $this->indent,
                           $item['href'],
                           (!empty($item['target']) ? ' target="' . $item['target'] . '"' : ''), $item['image']);
                    echo "\n";
                }
            }
        } else {
            printf('%s<a href="%s?%s=&%s">%s</a><br>',
                   $indent . $this->closed,
                   basename ($PHP_SELF),
                   $urlimage, $QUERY_STRING,
                   $this->image);
            echo "\n";
        }
    }
}
?>