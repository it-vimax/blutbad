<?php
// -----------------------------------------------------------------------------------------------
// button.php
// -----------------------------------------------------------------------------------------------
// Project:   Cascading menu with image buttons using on-the-fly image creation.
// Author:    Copyright (c) Urs <admin@circle.ch>
// Version:   1.0.0
// Update:    20-7-2000
// Licence:   ?
// PHP:       php-4.0.0-win32
//
// Source:    http://www.circle.ch/scripts/code/button_menu.zip
// Reference: "menu class" written by:                  <zakj@i.am>,
//            modified "menu class" for image use by:   <admin@circle.ch>
// Syntax:    for testing:
//               http://localhost/nested_menu.php
//            for inclusion (see also nested_menu.php):
//               <img src="button.php?fg=990000&bg=ffffff&txt=button one" border="0" alt="">
// Settings:  $bg = background color , hexadecimal
//            $fg = foreground color , hexadecimal
//            hexadecimal order : RGB (each 2byte)
//
// Enjoy!
// -----------------------------------------------------------------------------------------------
// Be aware of the patented GIF format! Adapt the routines to PNG. <http://www.libpng.org/pub/png>
// -----------------------------------------------------------------------------------------------

  define("TextFONT", "3");

  function ConvertColor($hexVal){
    $ColorVal = array(3);
    for($i = 0; $i < 3; $i++)
      $ColorVal[$i] = HexDec(substr($hexVal, $i * 2, 2));
    return $ColorVal;
  }

  //$width = strlen($txt) * ImageFontWidth(TextFONT);
  $width = 110;
  $offset = 2;
  $imgFRAME = ImageCreate($width, ImageFontHeight(TextFONT)+$offset);

  list($red, $green, $blue) = ConvertColor($bg);
  $bgCOLOR = ImageColorAllocate($imgFRAME, $red, $green, $blue);

  list($red, $green, $blue) = ConvertColor($fg);
  $fgCOLOR = ImageColorAllocate($imgFRAME, $red, $green, $blue);

  ImageFill($imgFRAME, 1, 1, $bgCOLOR);
  ImageString($imgFRAME, TextFONT, 1, 1, $txt, $fgCOLOR);

  header("Content-type: image/png");
  ImagePNG($imgFRAME);
  ImageDestroy($imgFRAME);
?>