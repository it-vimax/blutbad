<?
include("urhere.php");
$URHere = new URHere;
$text = $URHere->text($PHP_SELF);
$link = $URHere->link($PHP_SELF);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title><? echo $text; ?></title>
<style type="text/css">
a {text-decoration: none}
</style>
</head>

<body>

<? echo $link; ?>

<p>This Demonstrates the URHere Class.  The page title displays the text version, and the little bar above this paragraph contains the link function.  Please use and abuse this code.</p>

<p>If you find any way of making this code better, please give me a heads up: flinn_AT_activeintra_DOT_net</p>

<p>Copyright (c) 2000-2002, ActiveIntra.net<br />
All rights reserved.</p>

<p>Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:</p>

<ul>

<li>Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.</li>

<li>Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.</li>

<li>Neither the name of the ActiveIntra.net nor the names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.</li>

</ul>

<p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>


</body>

</html>
