<?php
// -----------------------------------------------------------------------------------------------
// nested_menu.php
// -----------------------------------------------------------------------------------------------
// Project:   Cascading menu with image buttons using on-the-fly image creation.
// Author:    Copyright (c) Urs <admin@circle.ch>
// Version:   1.0.0
// Update:    20-7-2000
// Licence:   ?
// PHP:       php-4.0.0-win32
//
// Source:    http://www.circle.ch/scripts/code/button_menu.zip
// Reference: "menu class" written by:                  <zakj@i.am>,
//            modified "menu class" for image use by:   <admin@circle.ch>
// Syntax:    for testing:
//               http://localhost/nested_menu.php
//            for inclusion (see also nested_menu.php):
//               <img src="button.php?fg=990000&bg=ffffff&txt=button one" border="0" alt="">
// Settings:  $bg = background color , hexadecimal
//            $fg = foreground color , hexadecimal
//            hexadecimal order : RGB (each 2byte)
//
// Enjoy!
// -----------------------------------------------------------------------------------------------
// Be aware of the patented GIF format! Adapt the routines to PNG. <http://www.libpng.org/pub/png>
// -----------------------------------------------------------------------------------------------

  require_once('menu.php');
  require_once('mymenu.php');
  ?>
  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
  <HTML>
  <HEAD>
  <TITLE>Title</TITLE>
  </HEAD>
  <BODY BGCOLOR="#DDDDDD">

  <table cesspadding=10 cellspacing=10>
    <tr><td><?php  $main->show(); $main2->show(); ?></td><td valign=top>Here comes your content page 5</td></tr>
  </table>

  </BODY>
  </HTML>