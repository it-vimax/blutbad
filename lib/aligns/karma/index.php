<?
    $title = ' - ���������� - �����';
    include($_SERVER["DOCUMENT_ROOT"]."/main.top.php");
?>
<table width="100%" cellpadding="4"><tbody><tr><td>&nbsp;</td><td bgcolor1="#BDB198" align="center" width="100%"><span class="nav_href">�&nbsp;
<a href="http://lib.blutbad.ru" class="nav_href">�������</a>
&nbsp;/&nbsp;
<a href="http://lib.blutbad.ru/aligns" class="nav_href">����������</a>
&nbsp;/&nbsp;
<a href="http://lib.blutbad.ru/aligns/light" class="nav_href">�����</a>
&nbsp;�</span></td><td>&nbsp;</td></tr></tbody></table>
<hr>
<p align="center" class="header"><b>�����</b></p>
<p align="justify" class="text"></p>
<p align="justify">� ������ � ���������� ����� ������� ������� <b><b><u>�����</u></b></b>: </p>
<p align="justify">��� ���������� � ���������� ������� ����� ������������� ��������� �������� �<b>-1</b>� (<img alt="���� �����" src="http://img.blutbad.ru/i/align_0.97.gif" width="25" height="16" style=" vertical-align: middle; "><b><a href="http://lib.blutbad.ru/aligns/haos/">����</a></b> ��� <img alt="���� ����" src="http://img.blutbad.ru/i/align_0.98.gif" width="25" height="16" style=" vertical-align: middle; "><b><a href="http://lib.blutbad.ru/aligns/dark/">����</a></b>) ��� �<b>+1</b>� (<img alt="���� �����" src="http://img.blutbad.ru/i/align_0.99.gif" width="25" height="16" style=" vertical-align: middle; "><b><a href="http://lib.blutbad.ru/aligns/light/">����</a></b> ��� <img alt="���� �������" src="http://img.blutbad.ru/i/align_0.96.gif" width="25" height="16"><b>�������</b>) </p>
<p align="justify"><b><u>������� ��������� �����</u></b></p>
<ul>
<li>��� ������ �� ����-���� (������ � ����, ��� ��������� ����� 2-� �������);
</li><li>��� ������ � ������������ ��������� ��� ������ <b>����� </b> ��� <b>�������</b>;
</li><li>������ � ��� <a href="http://lib.blutbad.ru/aligns/hran/" target="_blank">����������</a> �� <b>����</b> ��� <b>����</b> ������ <b>�����</b> ��� <b>�������</b>;
</li><li>������������� �������� ������� <b>�����</b> ��� <b>������</b>;
</li><!--<li>������� � ����� <img alt="������� ����" src="http://img.blutbad.ru/i/align19.gif" width="25" height="16"><b>�����</b> ��� <img alt="������� �����" src="http://img.blutbad.ru/i/align39.gif" width="25" height="16"><b>�����</b> (�������� ������ ��� ������������� �����). </li>--></ul>
<p align="justify"><b><u>������� ��������� �����</u></b></p>
<ul>
<li>��� ������ �� ����-���� (������ � ����, ��� ��������� ����� 2-� �������);&nbsp;
</li><li>������ � ����������� ��������� ���<b> </b>������ <b>����� </b>��� <strong>����</strong>;
</li><li>������ � ��� <a href="http://lib.blutbad.ru/aligns/hran/" target="_blank">����������</a> �� <b>���� </b>��� <b>������� </b>������ <b>����� </b>��� <strong>����</strong>;
</li><!--<li>������� � ����� <img alt="������� �����" src="http://img.blutbad.ru/i/align29.gif" width="25" height="16"><b>������</b> ��� <img alt="������� �������" src="http://img.blutbad.ru/i/align49.gif" width="25" height="16"><b>����</b> (������ ��� ������������� �����).</li>--></ul>
<p align="justify">���� ���� ����� ���������� ��������������� ����������� ���������� �� ������ ��������� �� ��� �� ������� ��������������� ������! </p>
<p align="justify"><b><u>������� �����</u> </b></p>
<p align="justify">������� ����� - �������� ������������ � ������� �� ������������ ����� � ����������. ������������ �����, �������� �� 2, - ��� ������� 10-�� �������. ��� �� 2 - 9-�� ������� � �.�. �������������, ���� ������������ ����� ����� �, �� ������� n-� ������� ����� �/(2(11-n)).</p>
<p align="justify"><b><u>������ �� �����</u></b></p>
<p align="justify">����� � ������������ � ��������� ���������� ����� � ���� ���������:<br><br></p>
<center>
<table style="border: 1px solid;" border="0" cellspacing="0" bordercolor="#995419" cellpadding="0" width="432">
<tbody>
<tr>
<td style="border: 1px solid;" rowspan="2" width="74">
<p align="center"><b>�������</b></p></td>
</tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="166" align="middle"><img alt="���� �����" src="http://img.blutbad.ru/i/align_0.97.gif" width="25" height="16" style=" vertical-align: middle; "><b>����</b>&nbsp;/&nbsp;<img alt="���� ����" src="http://img.blutbad.ru/i/align_0.98.gif" width="25" height="16" style=" vertical-align: middle; "><b>����</b></td>
<td style="border: 1px solid; vertical-align: middle;" width="184" align="middle"><img alt="���� �����" src="http://img.blutbad.ru/i/align_0.99.gif" width="25" height="16" style=" vertical-align: middle; "><b>����</b>&nbsp;/&nbsp;<img alt="���� �������" src="http://img.blutbad.ru/i/align_0.96.gif" width="25" height="16"><b>�������</b></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>1</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">�������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">����������</p></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>2</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">���������</p></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>3</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">�������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">���������</p></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>4</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">��������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">��������</p></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>5</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">���������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">������������</p></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>6</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">�������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">����������</p></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>7</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">�����������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">�������������</p></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>8</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">��������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">����������</p></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>9</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">�������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">��������������</p></td></tr>
<tr valign="top">
<td style="border: 1px solid; vertical-align: middle;" width="74" align="middle"><b>10</b></td>
<td style="border: 1px solid;" width="166">
<p align="center">��������</p></td>
<td style="border: 1px solid;" width="184">
<p align="center">����������</p></td></tr></tbody></table></center>


<p></p>
<hr>
<? include($_SERVER["DOCUMENT_ROOT"]."/main.but.php");?>