<?
    $title = ' - ������� ��� - ��� � ������';
    include($_SERVER["DOCUMENT_ROOT"]."/main.top.php");
?>
<table width="100%" cellpadding="4"><tbody><tr><td>&nbsp;</td><td bgcolor1="#BDB198" align="center" width="100%"><span class="nav_href">�&nbsp;
<a href="http://lib.blutbad.ru" class="nav_href">�������</a>
&nbsp;/&nbsp;
<a href="http://lib.blutbad.ru/world" class="nav_href">������� ���</a>
&nbsp;/&nbsp;
<a href="http://lib.blutbad.ru/world/events" class="nav_href">������� � ������</a>
&nbsp;/&nbsp;
<a href="http://lib.blutbad.ru/world/events/quests" class="nav_href">������ �������</a>
&nbsp;�</span></td><td>&nbsp;</td></tr></tbody></table>
<hr>
<p align="center" class="header"><b>������ �������</b></p>
<link href="http://damask.blutbad.ru/css/main.css" rel="stylesheet" type="text/css">
<link href="http://damask.blutbad.ru/c/quest.css" rel="stylesheet" type="text/css">
<link href="http://damask.blutbad.ru/c/battle.css" rel="stylesheet" type="text/css">
<style type="text/css">.text {color: #395058;font-size: 8pt;}</style>
<script src="http://damask.blutbad.ru/j/jquery/jquery-1.5.2.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
    /*<![CDATA[*/


            function g_qu(q_body, q_name, q_need, q_award, q_conditions, q_description){

              $(function() {
                    $('.q_view').hide();
                    $('#' + q_body).toggle();
                    var sad = '';

                    sad += '<div id="quest-col" class="quest-col">';
                    	sad += '<h3>�����: "' + q_name + '"</h3>';
                        sad += '<div class="quest-aims text qblock">';
                         sad += '<h5>���� ������:</h5>';
                          sad += '<div id="goal_quest" style=": none;">';
                    		sad += '<ol class="aims-list">';

                                for (var i = 0; i < q_need.length; i++) {
                                  sad += '<li style="display: block;">' + q_need[i] + '</li>';
                                }

                            sad += '</ol>';
                          sad += '</div>';
                    	sad += '</div>';

                    	sad += '<div class="quest-description qblock">';
                    		sad += '<h5>��������:</h5>';
                    		sad += '<div id="quest_description" style="display: block;">' + q_description + '</div>';
                    	sad += '</div>';
                        	sad += '<div class="quest-rewards qblock">';
                        		sad += '<h5>�������:</h5>';
                             sad += '<div id="quest_reward" style="display: block;">';
                              sad += '<ul class="rewards-list">';

                                for (var i = 0; i < q_award.length; i++) {
                                  sad += '<li style="display: block;">' + q_award[i] + '</li>';
                                }

                              sad += '</ul>';
                        	 sad += '</div>';
                            sad += '</div>';

                            if (q_conditions != '') {
                              sad += '<div class="quest-conditions qblock">';
                        	      sad += '<h5>�������:</h5>';
                                sad += '<div id="quest_terms" style="display: block;">';
                        		   sad += '<ul class="conditions-list">';
                                    	sad += '<li>' + q_conditions + '</li>';
                        	     	sad += '</ul>';
                                sad += '</div>';
                              sad += '</div><br>';
                            }

                     sad += '</div>';

                    $('#' + q_body).html(sad);
              });

            }

        $(function() {

        	jQuery('.btn_q').live('click', function() {

              $('.btn_q').css('text-decoration', 'none');
              $(this).css('text-decoration', 'underline');

              $('.q_view').hide();

              $('#'+$(this).attr('qu')).toggle();
            });

        });

/*]]>*/
</script>
<span class="context npc"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_bot_m_mentor_s.jpg">&nbsp;<a href="#"></a><b>������</b> - ����� � ������������� ����</span>

<hr><span class="context npc"><a href="http://damask.blutbad.ru/inf=���������%20�������" target="_blank"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_bot_m_sage_s.jpg">&nbsp;<a href="#"></a><b>���������</b></a> - ��������� � ����� ���� �������</span>

<ul style="padding-left: 45px;">
  <li><a href="#q_1_1" class="btn_q" qu="q_1_1" onclick="g_qu('q_1_1', '������ � ��������', ['������� <b>���� ��������</b>', '������� <b>����� �����</b>'], ['�������� ����������'], '', '��� �������� ����� ������, � ������� �����������.<br><br>��� ����� ������ ��������� �� ��������� �� �������� ���� ���-�� �� ����� �� ������������. ����� ���-�� ���������? ����� ��� � ������� ��� ������� ��������.')">�����: ������ � ��������</a> <i>(�� 1 ������)</i></li>
  <li><a href="#q_1_2" class="btn_q" qu="q_1_2" onclick="g_qu('q_1_2', '������� ���������', ['����� <b>������� ���������</b>'], ['(80%) �������� �����', '<b>+10</b> ��.'], '������� �������� ���� ��� � �����', '��� �������� ����� ������, � ������� �����������.<br><br>��� ����� ������ ��������� �� ��������� �� �������� ���� ���-�� �� ����� �� ������������. ����� ���-�� ���������? ����� ��� � ������� ��� ������� ��������.')">�����: ������� ���������</a> <i>(�� 3 ������)</i></li>
  <li><a href="#q_1_3" class="btn_q" qu="q_1_3" onclick="g_qu('q_1_3', '������ ������', ['����� <b>������ ������</b>'], ['�� ������ ������ 1 �����'], '', '������ ������ ����� ������ ������� �� ��� ����� ����� ��� ������������� �������� ����� ��� �� ������ ����������� � ���������� ')">�����: ������ ������</a> <i>(�� 5 �� �� 7 ��)</i></li>
</ul>
<div id="q_1_1" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_1_2" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_1_3" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>


<hr><span class="context npc"><a href="http://damask.blutbad.ru/inf=��������%20�������" target="_blank"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_bot_m_rogue_s.jpg">&nbsp;<a href="#"></a><b>��������</b></a> - ������ �� ���� ��������</span>

<ul style="padding-left: 45px;">
  <li><a href="#q_2_1" class="btn_q" qu="q_2_1" onclick="g_qu('q_2_1', '������ ���������', ['�������� <b>������� ������</b>', '�������� <b>��������� � ��������</b>'], ['����� ����', '���� ����'], '', '����� ������� � <b>������� ������������ �����</b> � ������ ��� ������� �������<br><br>�� ������ �������  � <b>�������</b> ������� ��� ������������ ��� ���� ���� � �� ���� � ������.')">�����: ������ ���������</a> <i>(�� 1 ������)</i></li>
  <li><a href="#q_2_2" class="btn_q" qu="q_2_2" onclick="g_qu('q_2_2', '���� ��������', ['<b>������� � �������� ���������</b>'], ['������� ���������'], '������� �������� ���� ��� � �����', '����� �������� � ������ ��� ���������')">�����: ���� ��������</a> <i>(�� 3 ������)</i></li>
</ul>
<div id="q_2_1" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_2_2" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>


<hr><span class="context npc"><a href="http://damask.blutbad.ru/inf=���������%20�������" target="_blank"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_bot_m_vicar_s.jpg">&nbsp;<a href="#"></a><b>���������</b></a> - ����� � ������� - �������� ����� � � ������� - ������ �������� �����</span>

<ul style="padding-left: 45px;">
  <li><a href="#q_3_1" class="btn_q" qu="q_3_1" onclick="g_qu('q_3_1', '��������� ������', ['�������� <b>��������� ������</b>'], ['������� �� �������'], '������� �������� ���� ��� � �����', '���������� � �����������, �������� ����� � ������� � ���� ���������� ����.')">�����: ��������� ������</a> <i>(�� 1 �� �� 7 ��)</i></li>
  <li><a href="#q_3_2" class="btn_q" qu="q_3_2" onclick="g_qu('q_3_2', '��������� ���� �����', ['� ��������� ������ �������� <b>�������</b> (0/1)'], ['���� �����'], '', '���, ��� ���� ��������� �������, ��� ��������� � ��������� ������ ������� ������� � ��������� ���� �������� - �������� - �������� ������ �� �������� �������.')">�����: ��������� ���� �����</a> <i>(�� 10 ������)</i></li>
  <li><a href="#q_3_3" class="btn_q" qu="q_3_3" onclick="g_qu('q_3_3', '���� ������', ['�������� ���� � ����� �� ����� �� 10 ���.'], ['������� �����', '������ ����'], '', '���� ����� ��������������� �������� ����� ���� <a href=&quot;http://bank.blutbad.ru/&quot; target=&quot;_blank&quot; style=&quot;color: blue; font-weight: bold;&quot;><b>bank.blutbad.ru</b></a>: ��������� ���� � ����� �� ����� �� 10 ���. � ������ ������� ���� �������.')">�����: ���� ������</a> <i>(�� 1 �� �� 7 ��)</i></li>
  <li><a href="#q_3_4" class="btn_q" qu="q_3_4" onclick="g_qu('q_3_4', '��������� ������ (�����������)', ['�������� <b>������</b> (0/1)', '�������� <b>������</b> (0/1)', '�������� <b>������</b> (0/1)'], ['300% �������� �����'], '������� �������� ���� ��� � �����', '�� �������� ������ ��� �������� ������� � ��� � �������� ������, ��������� �� ��� ����� ���� ��������� �����.<br><br>��� �������� ������ ��� <b>������</b> ��������� � ������� ���� � &quot;������ �������&quot; ��� ��������� ���������� ���� ��������� ��������.')">�����: ��������� ������ (�����������)</a> <i>(�� 5 ������)</i></li>
</ul>
<div id="q_3_1" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_3_2" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_3_3" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_3_4" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>

<hr><span class="context npc"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_bot_m_sir_knight_s.jpg">&nbsp;<a href="#"></a><b>��� ������</b> - ����� � ������� ����������� �������</span>
<ul style="padding-left: 45px;">
  <li><a href="#q_4_1" class="btn_q" qu="q_4_1" onclick="g_qu('q_4_1', '����� ������', ['�������� <b>����� �� 3-� ������� ������</b> (0/1)', '�������� <b>��������� �����</b> (0/1)', '�������� <b>��������� +50 (���.)</b> (0/1)', '�������� <b>������� &quot;�����&quot; 0.01 �����</b> (0/1)'], ['(300%) �������� �����', '��������� � ������ ��������'], '', '��� ���� ��� ���� ������ ��� ����������� ����� ������ ���� ����� � ������:<br><br><b>������ ������</b> - ����� ����� � ����� �������� ������.<br><b>��������� �����</b> - ����� ������� � �������� �������.<br><b>��������� +50 (���.)</b> - ����� ���������� � ������� ����� &quot;������� ������������ �����&quot;.<br><b>������� &quot;�����&quot; 0.01 �����</b> - ����� ������ � ����� �� �������� ������.<br><br>��� ������ �������� ��� ����������� ������� ������ �������.')">�����: ����� ������</a> <i>(�� 5 ������)</i></li>
  <li><a href="#q_4_2" class="btn_q" qu="q_4_2" onclick="g_qu('q_4_2', '��������� ���� (�����������)', ['�������� � <b>��������� ����</b> (0/10)'], ['������������ ���������� �����'], '', '�������� ������ � ��������� ���� 10 ���. ����� ���������� - ������� �� ��������.<br>������� ������ ���� ��������� �� ����� �������� ��� ����� � �� ���� ��������� ���� ������. ')">�����: ��������� ���� (�����������)</a> <i>(�� 5 ������)</i></li>
  <li><a href="#q_4_3" class="btn_q" qu="q_4_3" onclick="g_qu('q_4_3', '������� �������', ['�������� � <b>��������� ����</b> (0/400)', '�������� � <b>��������� ���� ���� ���������</b> (0/25)', '�������� <b>�������</b> (0/35)', '�������� � ���������� <b>������ �����</b> (0/25)', '�������� ������� <b>������� ������</b> (0/50)', '�������� ������� <b>�����</b> (0/30) �����'], ['���� �������'], '������� ����� ��������� �� 30 ������', '����� �������� ���� ����������� � ���������� �������� &quot;���� �������&quot;')">�����: ������� �������</a> <i>(�� 7 ������)</i></li>
  <li><a href="#q_4_4" class="btn_q" qu="q_4_4" onclick="g_qu('q_4_4', '������������� ������', ['�������� <b>��������� � ��������</b> (0/1)', '�������� <b>��������� � ��������</b> (0/1)', '�������� <b>��������� ����������</b> (0/1)'], ['(50% �������� �����)', '+50 �����'], '', '����������� � ������� ������� ������� - � ��� � ��� �������� �������, � �������������� ������������� ������ � ������ ������� ��������.<br>���� � ���� ��� ����������� � �����������. ����������.<br><br>��������� ���������� ������ � ��� � �����')">�����: ������������� ������</a> <i>(�� 1 ������)</i></li>
</ul>
<div id="q_4_1" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_4_2" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_4_3" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_4_4" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>

<hr><span class="context npc"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_quest_m_cityman_s.jpg">&nbsp;<a href="#"></a><b>�����</b> - ����� � ������� - ����� �������� ���� � � ������� - �������� �������</span>
<ul style="padding-left: 45px;">
  <li><a href="#q_5_1" class="btn_q" qu="q_5_1" onclick="g_qu('q_5_1', '�����������', ['�������� <b>������</b> (5 ��.)'], ['������', '��������� ������� ������� ��������� +5', '20 ��. �����'], '', '��� ������� ���� ����������� ������, ������� � ������.<br>��� - ������ ��� ������ � ���� ������������� ��������. <br>������ ���������� ����� �� - ���������� �� ����. �� ������ ���������� ��������� �� ������������ �� ��� ������� ����� �� ������� - ������� ������� ���������.<br>���� ������ - ������� 5 �����. � ���� � ������� ���������. ������ � �������� � ����� �������� ����� ��������� �������.<br><br>������!')">�����: �����������</a> <i>(�� 1 ������)</i></li>
  <li><a href="#q_5_2" class="btn_q" qu="q_5_2" onclick="g_qu('q_5_2', '������ ������� ��������� (�����������)', ['�������� <b>�������</b> (0/3)'], ['������ �������', '��������� ������� ������� ��������� +5'], '', '���-�� ���������� ����� � �������� � ��������� �����. ������ � �������, ��������� � �������...<br><br>��� � � ����� ������� ��������� ������� �����������. � ����� � ������ ��������� ������-�� �������� ������� - �������. �������� ������, �� � ���� ������� ��� ���������.<br><br>������ ������ ���� - ������ ��� ���������� �� �������. �� ������ ��� ����������� �������, � ���� ������ ���� 10 ������� �� ����� �������. �� � ������, ������� ������ ��� �� ��������, ����� � ���� ��� ������� � �����.')">�����: ������ ������� ��������� (�����������)</a> <i>(�� 1 ������)</i></li>
</ul>
<div id="q_5_1" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_5_2" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>

<hr><span class="context npc"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_bot_m_marauder_s.jpg">&nbsp;<a href="#"></a><b>�������</b> - ����� � ������� - ����� �������� ���� � � ������� - �������� �������</span>
<ul style="padding-left: 45px;">
  <li><a href="#q_6_1" class="btn_q" qu="q_6_1" onclick="g_qu('q_6_1', '�������� ��� ��� �������� (�����������)', ['�������� <b>��������� ����������</b> (0/2)'], ['��� ���� �����', '+20 �����'], '������� �������� ���� ��� � �����', '��� ����� ������������ ����� � ����� � ����� ��� ��� ��������� ����������� ������ �������� �� � ����')">�����: �������� ��� ��� �������� (�����������)</a> <i>(�� 1 �� �� 7 ��)</i></li>
  <li><a href="#q_6_2" class="btn_q" qu="q_6_2" onclick="g_qu('q_6_2', '��������� �������� (�����������)', ['�������� � <b>��������� ���������</b> (0/3)'], ['��������� ���������������'], '������� ����� ��������� � ���� ������.', '��� � ����� � ������ � ��������� ����')">�����: ��������� �������� (�����������)</a> <i>(�� 5 ������)</i></li>
</ul>
<div id="q_6_1" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>
<div id="q_6_2" class="q_view" style="margin-top: 10px;margin-bottom: 10px;display: none;padding: 10px;background-color: #d7d9d9;border: 1px solid;border-color: #aebdc9 #aebdc9 #aebdc9 #aebdc9;"></div>

<hr><span class="context npc"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_bot_merchantsaid_s.jpg">&nbsp;<a href="#"></a><b>��������</b> - ����� � ��������</span>
<hr><span class="context npc"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_bot_m_tavernkeeper_s.jpg">&nbsp;<a href="#"></a><b>������ �������</b> - ����� � �������</span>
<hr><span class="context npc"><img alt="" class="avatar tooltip-next" style="vertical-align: middle;" src="http://img.blutbad.ru/i/obraz/1_bot_m_miner_s.jpg">&nbsp;<a href="#"></a><b>�������</b> - ����� �� ������ � �����</span>



<hr>
<? include($_SERVER["DOCUMENT_ROOT"]."/main.but.php");?>