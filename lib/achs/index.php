<?
    $title = ' - ����������';
    include($_SERVER["DOCUMENT_ROOT"]."/main.top.php");



/***------------------------------------------
 * ����������
 **/

 $achievements = array(

          # ----------------------------
          # ���
          # ----------------------------

                       # ----------------------------
                       # �������
                         'winner_10' => array(

                                              # ��������
                                                'name' => '�������',

                                              # ��������
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',

                                              # ����
                                                'goals' => array(                         # ������� ������ �� ��������
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 10, 'listgoals' => array()),
                                                                ),

                                              # �������
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),

                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",

                                              # �������� ��� �������� ����������� ����������
                                                'done_achiev' => "",
                                                'next_achiev' => "winner_50"

                                              ),
                       # ----------------------------
                       # ������
                         'winner_50' => array(
                                                'name' => '������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 50, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_10",
                                                'next_achiev' => "winner_100"
                                              ),
                       # ----------------------------
                       # �������
                         'winner_100' => array(
                                                'name' => '�������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '25', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_50",
                                                'next_achiev' => "winner_500"
                                              ),
                       # ----------------------------
                       # ������
                         'winner_500' => array(
                                                'name' => '������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_100",
                                                'next_achiev' => "winner_1000"
                                              ),
                       # ----------------------------
                       # ������
                         'winner_1000' => array(
                                                'name' => '������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_500",
                                                'next_achiev' => "winner_2500"
                                              ),
                       # ----------------------------
                       # ���������
                         'winner_2500' => array(
                                                'name' => '���������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 2500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '150', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_1000",
                                                'next_achiev' => "winner_5000"
                                              ),
                       # ----------------------------
                       # ������
                         'winner_5000' => array(
                                                'name' => '������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 5000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_2500",
                                                'next_achiev' => "winner_10000"
                                              ),
                       # ----------------------------
                       # �������
                         'winner_10000' => array(
                                                'name' => '�������',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 10000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_5000",
                                                'next_achiev' => "winner_25000"
                                              ),
                       # ----------------------------
                       # �����
                         'winner_25000' => array(
                                                'name' => '�����',
                                                'description' => '�������� ������ � %s% ����� ����, ����� ������������� ���� � ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������ � %s/s% ����', 'name' => '', 'val' => 0, 'valmax' => 25000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '425', 'description' => '����� ����������',  'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������� �������"',    'description' => '', 'ex' => 1, 'code' => 'item'),
                                                                 ),
                                                'code_groupe' => "winner_total",
                                                'code'        => "winner_total",
                                                'done_achiev' => "winner_10000",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ������ �� ������ - ������ �������
                         'fight_1' => array(
                                                'name' => '������ �� ������ - ������ �������',
                                                'description' => '������� ������ � ������ �������!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "������ �������"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_1",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ������ �� ������ - ������ ������������
                         'fight_2' => array(
                                                'name' => '������ �� ������ - ������ ������������',
                                                'description' => '������� ������ � ������ ������������!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "������ ������������"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_2",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ������ �� ������ - ��������� �����
                         'fight_3' => array(
                                                'name' => '������ �� ������ - ��������� �����',
                                                'description' => '������� ������ � ��������� �����!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "��������� �����"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '25', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_3",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ������ �� ������ - �������������� ��������
                         'fight_4' => array(
                                                'name' => '������ �� ������ - �������������� ��������',
                                                'description' => '������� ������ � �������������� ��������!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "�������������� ��������"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_4",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ������ �� ������ - ������������ ��������
                         'fight_5' => array(
                                                'name' => '������ �� ������ - ������������ ��������',
                                                'description' => '������� ������ � ������������ ��������!',
                                                'goals' => array(
                                                                 1 => array('description' => '������ �������� ����� � ����� "������������ ��������"', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "statused_5",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������� ����
                         'fight_6' => array(
                                                'name' => '����������� ����',
                                                'description' => '������� ������ �� ���� ��������� ��������� ����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ����������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������ �� ������ - ������ �������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '������ �� ������ - ������ ������������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ �� ������ - ��������� �����', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '������ �� ������ - �������������� ��������', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '������ �� ������ - ������������ ��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '1000', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '����� "����������� ����"', 'description' => '', 'ex' => 0, 'code' => 'obraz '),
                                                                 ),
                                                'code_groupe' => "statused_6",
                                                'code'        => "statused",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # �������
          # ----------------------------

                       # ----------------------------
                       # ������
                         'tavern_food' => array(
                                                'name' => '������',
                                                'description' => '���������� �� ���� "���" � ������� ������������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '��������� � ��������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� � ��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "affects_1",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������
                         'tavern_nonalc' => array(
                                                'name' => '����������',
                                                'description' => '���������� �� ���� "��������������� �������" � ������� ������������� ����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�������� ���������������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '�������� ����������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ������������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '�������� ������', 'done' => 0)
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "affects_2",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # �������� ����
                         'tavern_alc' => array(
                                                'name' => '�������� ����',
                                                'description' => '���������� �� ���� "��������" � ������� ������������� �������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '���', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����� ����', 'done' => 0)
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "affects_3",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # �������� ���
                         'tavern_fish' => array(
                                                'name' => '�������� ���',
                                                'description' => '������� ��������� ���� ��� � ����� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '��������� ���', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '�������� ���', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '���������� ���', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������� ���', 'done' => 0),
                                                                                                                                                                                6 => array('name' => '���������� ���', 'done' => 0),
                                                                                                                                                                                7 => array('name' => '���������� ���', 'done' => 0),
                                                                                                                                                                                8 => array('name' => '������� ���', 'done' => 0),
                                                                                                                                                                                9 => array('name' => '��� ��-�������', 'done' => 0),
                                                                                                                                                                                10 => array('name' => '��� ��-���������', 'done' => 0),
                                                                                                                                                                                11 => array('name' => '��� ��-����������', 'done' => 0),
                                                                                                                                                                                12 => array('name' => '��� ��-c���������', 'done' => 0),
                                                                                                                                                                                13 => array('name' => '����������� ���', 'done' => 0),
                                                                                                                                                                                14 => array('name' => '����-���', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '25', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "affects_4",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ������� ������
                         'tavern_all' => array(
                                                'name' => '������� ������',
                                                'description' => '�������� ��������� ����������, ��������� � ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ����', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '�������� ���', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 20% ��� ������ ����� ���� � �������������� �������� � ��������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "affects_5",
                                                'code'        => "affects",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # �����������
          # ----------------------------

                       # ----------------------------
                       # ����������� � ������
                         'city_damask' => array(
                                                'name' => '����������� � ������',
                                                'description' => '��������� ��������������������� ������ ������',
                                                'goals' => array(
                                                                 1 => array('description' => '����������� ����� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����������� �������', 'done' => 0),          # 120
                                                                                                                                                                                2 => array('name' => '����� �������� ����', 'done' => 0),          # 150
                                                                                                                                                                                3 => array('name' => '�������� �����', 'done' => 0),               # 130
                                                                                                                                                                                4 => array('name' => '��������� �����', 'done' => 0),              # 140
                                                                                                                                                                                5 => array('name' => '������', 'done' => 0),                       # 100
                                                                                                                                                                                6 => array('name' => '����������� ���', 'done' => 0),              # 101
                                                                                                                                                                                7 => array('name' => '������� "����������� ������"', 'done' => 0), # 126
                                                                                                                                                                                8 => array('name' => '��������� ����', 'done' => 0),               # 152
                                                                                                                                                                                9 => array('name' => '������', 'done' => 0)                        # 136
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "loc_visit_1",
                                                'code'        => "loc_visit",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������� � ������
                         'city_alamut' => array(
                                                'name' => '����������� � ������',
                                                'description' => '��������� ��������������������� ������ ������',
                                                'goals' => array(
                                                                 1 => array('description' => '����������� ����� ������:', 'name' => '', 'val' => 0, 'valmax' => 0, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����������� �������', 'done' => 0),   # 120
                                                                                                                                                                                2 => array('name' => '�������� �������', 'done' => 0),      # 150
                                                                                                                                                                                3 => array('name' => '������ �������� �����', 'done' => 0), # 130
                                                                                                                                                                                4 => array('name' => '�������� ����', 'done' => 0),         # 140
                                                                                                                                                                                5 => array('name' => '�����', 'done' => 0),                 # 100
                                                                                                                                                                                6 => array('name' => '����������� ���', 'done' => 0),       # 101
                                                                                                                                                                                7 => array('name' => '������� "������� ����"', 'done' => 0),# 126
                                                                                                                                                                                8 => array('name' => '��������� ����', 'done' => 0),        # 152
                                                                                                                                                                                9 => array('name' => '������', 'done' => 0)                 # 156
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "loc_visit_2",
                                                'code'        => "loc_visit",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # ���������
          # ----------------------------

                       # ----------------------------
                       # ��������� �������
                         'naemnik_10' => array(
                                                'name' => '��������� �������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 10, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "",
                                                'next_achiev' => "naemnik_50"
                                              ),
                       # ----------------------------
                       # ������ ������
                         'naemnik_50' => array(
                                                'name' => '������ ������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 50, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "naemnik_10",
                                                'next_achiev' => "naemnik_100"
                                              ),
                       # ----------------------------
                       # �����������
                         'naemnik_100' => array(
                                                'name' => '�����������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "naemnik_50",
                                                'next_achiev' => "naemnik_250"
                                              ),
                       # ----------------------------
                       # ������
                         'naemnik_250' => array(
                                                'name' => '������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "naemnik_100",
                                                'next_achiev' => "naemnik_500"
                                              ),

                       # ----------------------------
                       # ����������
                         'naemnik_500' => array(
                                                'name' => '����������',
                                                'description' => '���������� � �������� �������� � %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������ ��������� � ��� � �������� �������� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% ��� ������� ��� "�������������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "naemnik",
                                                'code'        => "naemnik",
                                                'done_achiev' => "naemnik_250",
                                                'next_achiev' => ""
                                              ),



                       # ----------------------------
                       # �����������
                         'mentor_1' => array(
                                                'name' => '�����������',
                                                'description' => '�������� �� 5-��� ������ ������ �������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "",
                                                'next_achiev' => "mentor_5"
                                              ),
                       # ----------------------------
                       # ���������
                         'mentor_5' => array(
                                                'name' => '���������',
                                                'description' => '�������� �� 5-��� ������ ���� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "mentor_1",
                                                'next_achiev' => "mentor_15"
                                              ),
                       # ----------------------------
                       # ��������� ���������
                         'mentor_15' => array(
                                                'name' => '��������� ���������',
                                                'description' => '�������� �� 5-��� ������  ���������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "mentor_5",
                                                'next_achiev' => "mentor_30"
                                              ),
                       # ----------------------------
                       # ������� �������
                         'mentor_30' => array(
                                                'name' => '������� �������',
                                                'description' => '�������� �� 5-��� ������ �������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "mentor_15",
                                                'next_achiev' => "mentor_50"
                                              ),
                       # ----------------------------
                       # ����
                         'mentor_50' => array(
                                                'name' => '����',
                                                'description' => '�������� �� 5-��� ������ ��������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� �� 5-��� ������ %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 50, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"���������� ���������� ���������� ����������� ����� �� ������ �������� �� 50%"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "mentor",
                                                'code'        => "mentor",
                                                'done_achiev' => "mentor_30",
                                                'next_achiev' => ""
                                              ),


          # ----------------------------
          # �������
          # ----------------------------

                       # ----------------------------
                       # �������
                         'waterman_3' => array(
                                                'name' => '�������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "",
                                                'next_achiev' => "waterman_15"
                                              ),
                       # ----------------------------
                       # ���������� �������
                         'waterman_15' => array(
                                                'name' => '���������� �������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '30', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_3",
                                                'next_achiev' => "waterman_45"
                                              ),
                       # ----------------------------
                       # �����
                         'waterman_45' => array(
                                                'name' => '�����',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_15",
                                                'next_achiev' => "waterman_90"
                                              ),
                       # ----------------------------
                       # ��������
                         'waterman_90' => array(
                                                'name' => '��������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_45",
                                                'next_achiev' => "waterman_180"
                                              ),
                       # ----------------------------
                       # ��������
                         'waterman_180' => array(
                                                'name'        => '��������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 180, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_90",
                                                'next_achiev' => "waterman_270"
                                              ),
                       # ----------------------------
                       # ���������
                         'waterman_270' => array(
                                                'name' => '���������',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_90",
                                                'next_achiev' => "waterman_360"
                                              ),


                       # ----------------------------
                       # �����
                         'waterman_360' => array(
                                                'name' => '�����',
                                                'description' => '������ %s% �������',
                                                'goals' => array(
                                                                 1 => array('description' => '����� %s/s% �������', 'name' => '', 'val' => 0, 'valmax' => 360, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"�������������� ����������� "������ �������� ���������" � ���� � ��������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "waterman",
                                                'code'        => "waterman",
                                                'done_achiev' => "waterman_270",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # ����������
          # ----------------------------

                       # ----------------------------
                       # ������ ����
                         'dark_f_3' => array(
                                                'name' => '������ ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "",
                                                'next_achiev' => "dark_f_15"
                                              ),
                       # ----------------------------
                       # ������� ����
                         'dark_f_15' => array(
                                                'name' => '������� ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "dark_f_3",
                                                'next_achiev' => "dark_f_45"
                                              ),
                       # ----------------------------
                       # ��������� ����
                         'dark_f_45' => array(
                                                'name' => '��������� ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "dark_f_15",
                                                'next_achiev' => "dark_f_90"
                                              ),
                       # ----------------------------
                       # ������� ����
                         'dark_f_90' => array(
                                                'name' => '������� ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "dark_f_45",
                                                'next_achiev' => "dark_f_270"
                                              ),
                       # ----------------------------
                       # ������� ����
                         'dark_f_270' => array(
                                                'name' => '������� ����',
                                                'description' => '����������� ������ ���������� "����� �����" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "����� �����" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"���������� ������� ������� ����� ������ ����������� ���������� "����" ��� ����������� �� ������������� ����������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "align_vamp2",
                                                'code'        => "align_vamp2",
                                                'done_achiev' => "dark_f_90",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ����������� ����
                         'light_f_3' => array(
                                                'name' => '����������� ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "",
                                                'next_achiev' => "light_f_15"
                                              ),
                       # ----------------------------
                       # ������������ ����
                         'light_f_15' => array(
                                                'name' => '������������ ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "light_f_3",
                                                'next_achiev' => "light_f_45"
                                              ),
                       # ----------------------------
                       # �������� ����
                         'light_f_45' => array(
                                                'name' => '�������� ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "light_f_15",
                                                'next_achiev' => "light_f_90"
                                              ),
                       # ----------------------------
                       # ������������ ����
                         'light_f_90' => array(
                                                'name' => '������������ ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "light_f_45",
                                                'next_achiev' => "light_f_270"
                                              ),
                       # ----------------------------
                       # ���������� ����
                         'light_f_270' => array(
                                                'name' => '���������� ����',
                                                'description' => '����������� ������ ���������� "�����������������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "�����������������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '',    'description' => '"���������� ������� ������� ����� ������ ����������� ���������� "����" ��� ����������� �� ������������� ����������"', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "align_sacr",
                                                'code'        => "align_sacr",
                                                'done_achiev' => "light_f_90",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ������� ����
                         'chaos_f_3' => array(
                                                'name' => '������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "",
                                                'next_achiev' => "chaos_f_15"
                                              ),
                       # ----------------------------
                       # �������� ����
                         'chaos_f_15' => array(
                                                'name' => '�������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "chaos_f_3",
                                                'next_achiev' => "chaos_f_45"
                                              ),
                       # ----------------------------
                       # ����������� ����
                         'chaos_f_45' => array(
                                                'name' => '����������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "chaos_f_15",
                                                'next_achiev' => "chaos_f_90"
                                              ),
                       # ----------------------------
                       # �������� ����
                         'chaos_f_90' => array(
                                                'name' => '�������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "chaos_f_45",
                                                'next_achiev' => "chaos_f_270"
                                              ),
                       # ----------------------------
                       # �������� ����
                         'chaos_f_270' => array(
                                                'name' => '�������� ����',
                                                'description' => '����������� ������ ���������� "������� �������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "������� �������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '',    'description' => '"���������� ������� ������� ����� ������ ����������� ���������� "����" ��� ����������� �� ������������� ����������"', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "align_ch_side",
                                                'code'        => "align_ch_side",
                                                'done_achiev' => "chaos_f_90",
                                                'next_achiev' => ""
                                              ),

                       # ----------------------------
                       # ������� �������
                         'order_f_3' => array(
                                                'name' => '������� �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "",
                                                'next_achiev' => "order_f_15"
                                              ),
                       # ----------------------------
                       # �������� �������
                         'order_f_15' => array(
                                                'name' => '�������� �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 15, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '40', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "order_f_3",
                                                'next_achiev' => "order_f_45"
                                              ),
                       # ----------------------------
                       # ������ �������
                         'order_f_45' => array(
                                                'name' => '������ �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 45, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "order_f_15",
                                                'next_achiev' => "order_f_90"
                                              ),
                       # ----------------------------
                       # ������������� �������
                         'order_f_90' => array(
                                                'name' => '������������� �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 90, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "order_f_45",
                                                'next_achiev' => "order_f_270"
                                              ),
                       # ----------------------------
                       # �������������� �������
                         'order_f_270' => array(
                                                'name' => '�������������� �������',
                                                'description' => '����������� ������ ���������� "��������� ����������" %s% ����',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ ������ ���������� "��������� ����������" � ��� %s/s% ���', 'name' => '', 'val' => 0, 'valmax' => 270, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"���������� ������� ������� ����� ������ ����������� ���������� "�������" ��� ����������� �� ������������� ����������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "align_subm",
                                                'code'        => "align_subm",
                                                'done_achiev' => "order_f_90",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����
                         'dobla_1' => array(
                                                'name' => '����',
                                                'description' => '�������� ������ ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "",
                                                'next_achiev' => "dobla_2"
                                              ),
                       # ----------------------------
                       # ����
                         'dobla_2' => array(
                                                'name' => '����',
                                                'description' => '�������� ������ ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 2, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '25', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_1",
                                                'next_achiev' => "dobla_3"
                                              ),
                       # ----------------------------
                       # ��� ������
                         'dobla_3' => array(
                                                'name' => '��� ������',
                                                'description' => '�������� ������ ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '45', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_2",
                                                'next_achiev' => "dobla_4"
                                              ),
                       # ----------------------------
                       # ����
                         'dobla_4' => array(
                                                'name' => '����',
                                                'description' => '�������� ��������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 4, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '75', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_3",
                                                'next_achiev' => "dobla_5"
                                              ),
                       # ----------------------------
                       # �����
                         'dobla_5' => array(
                                                'name' => '�����',
                                                'description' => '�������� ����� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '110', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_4",
                                                'next_achiev' => "dobla_6"
                                              ),
                       # ----------------------------
                       # ������
                         'dobla_6' => array(
                                                'name' => '������',
                                                'description' => '�������� ����� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 6, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '150', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_5",
                                                'next_achiev' => "dobla_7"
                                              ),
                       # ----------------------------
                       # ����
                         'dobla_7' => array(
                                                'name' => '����',
                                                'description' => '�������� ������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 7, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '225', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_6",
                                                'next_achiev' => "dobla_8"
                                              ),
                       # ----------------------------
                       # ������
                         'dobla_8' => array(
                                                'name' => '������',
                                                'description' => '�������� ������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 8, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_7",
                                                'next_achiev' => "dobla_9"
                                              ),
                       # ----------------------------
                       # ������
                         'dobla_9' => array(
                                                'name' => '������',
                                                'description' => '�������� ������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 9, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '450', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_8",
                                                'next_achiev' => "dobla_10"
                                              ),
                       # ----------------------------
                       # �����
                         'dobla_10' => array(
                                                'name' => '�����',
                                                'description' => '�������� ������� ������� ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� ��������', 'name' => '', 'val' => 0, 'valmax' => 10, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '450', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"��������� ����"', 'description' => '', 'ex' => 1, 'code' => 'affect'),
                                                                 ),
                                                'code_groupe' => "fame_level",
                                                'code'        => "fame_level",
                                                'done_achiev' => "dobla_9",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ���������
                         'karma_1' => array(
                                                'name' => '���������',
                                                'description' => '�������� ������ ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "",
                                                'next_achiev' => "karma_2"
                                              ),
                       # ----------------------------
                       # ���
                         'karma_2' => array(
                                                'name' => '���',
                                                'description' => '�������� ������ ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 2, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '15', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_1",
                                                'next_achiev' => "karma_3"
                                              ),
                       # ----------------------------
                       # �������
                         'karma_3' => array(
                                                'name' => '�������',
                                                'description' => '�������� ������ ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 3, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '30', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_2",
                                                'next_achiev' => "karma_4"
                                              ),
                       # ----------------------------
                       # �����
                         'karma_4' => array(
                                                'name' => '�����',
                                                'description' => '�������� ��������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 4, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_3",
                                                'next_achiev' => "karma_5"
                                              ),
                       # ----------------------------
                       # ������������
                         'karma_5' => array(
                                                'name' => '������������',
                                                'description' => '�������� ����� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '75', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_4",
                                                'next_achiev' => "karma_6"
                                              ),
                       # ----------------------------
                       # ���������
                         'karma_6' => array(
                                                'name' => '���������',
                                                'description' => '�������� ����� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 6, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_5",
                                                'next_achiev' => "karma_7"
                                              ),
                       # ----------------------------
                       # ����
                         'karma_7' => array(
                                                'name' => '����',
                                                'description' => '�������� ������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 7, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '150', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_6",
                                                'next_achiev' => "karma_8"
                                              ),
                       # ----------------------------
                       # ��������� ���
                         'karma_8' => array(
                                                'name' => '��������� ���',
                                                'description' => '�������� ������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 8, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_7",
                                                'next_achiev' => "karma_9"
                                              ),
                       # ----------------------------
                       # ������
                         'karma_9' => array(
                                                'name' => '������',
                                                'description' => '�������� ������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 9, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_8",
                                                'next_achiev' => "karma_10"
                                              ),
                       # ----------------------------
                       # �������� �����
                         'karma_10' => array(
                                                'name' => '�������� �����',
                                                'description' => '�������� ������� ������� �����',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� %s/s% ������� �����', 'name' => '', 'val' => 0, 'valmax' => 10, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"100%-�� ������������ ����� ������ ����������� ����������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "karma_level",
                                                'code'        => "karma_level",
                                                'done_achiev' => "karma_9",
                                                'next_achiev' => ""
                                              ),


          # ----------------------------
          # ������ � �������
          # ----------------------------

                       # ----------------------------
                       # ���� ����� �����
                         'presents_20' => array(
                                                'name' => '���� ����� �����',
                                                'description' => '�������� ������� %s% ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������� %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 20, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "gift",
                                                'code'        => "gift",
                                                'done_achiev' => "",
                                                'next_achiev' => "presents_100"
                                              ),
                       # ----------------------------
                       # �������� ������ ����
                         'presents_100' => array(
                                                'name' => '�������� ������ ����',
                                                'description' => '�������� ������� %s% ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������� %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "gift",
                                                'code'        => "gift",
                                                'done_achiev' => "presents_20",
                                                'next_achiev' => "presents_500"
                                              ),
                       # ----------------------------
                       # ��� �����
                         'presents_500' => array(
                                                'name' => '��� �����',
                                                'description' => '�������� ������� %s% ��������',
                                                'goals' => array(
                                                                 1 => array('description' => '�������� ������� %s/s% ��������', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 20% ��� ������� ����� ��������� ���������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "gift",
                                                'code'        => "gift",
                                                'done_achiev' => "presents_100",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������
                         'friends_35' => array(
                                                'name' => '����������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 35, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "",
                                                'next_achiev' => "friends_50"
                                              ),
                       # ----------------------------
                       # ������
                         'friends_50' => array(
                                                'name' => '������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 50, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "friends_35",
                                                'next_achiev' => "friends_65"
                                              ),
                       # ----------------------------
                       # ��������
                         'friends_65' => array(
                                                'name' => '��������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 65, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "friends_50",
                                                'next_achiev' => "friends_80"
                                              ),
                       # ----------------------------
                       # ���� ��������
                         'friends_80' => array(
                                                'name' => '���� ��������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 80, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '150', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "friends_65",
                                                'next_achiev' => "friends_100"
                                              ),
                       # ----------------------------
                       # � ��������
                         'friends_100' => array(
                                                'name' => '� ��������',
                                                'description' => '�������� � ���� ������ ��������� %s% ������',
                                                'goals' => array(
                                                                 1 => array('description' => '������� � ���� ������ ��������� %s/s% ������', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '250', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"����������� ���������� ������ ��������� �� ������� ������ �� 120"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "friends",
                                                'code'        => "friends",
                                                'done_achiev' => "friends_80",
                                                'next_achiev' => ""
                                              ),

          # ----------------------------
          # ����
          # ----------------------------

                     # ----------------------------
                     # �����
                     # ----------------------------

                       # ----------------------------
                       # �������
                         'use_rune_hp_25' => array(
                                                'name' => '�������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 25, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_hp_500"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_hp_500' => array(
                                                'name' => '���������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_25",
                                                'next_achiev' => "use_rune_hp_1000"
                                              ),
                       # ----------------------------
                       # �����������
                         'use_rune_hp_1000' => array(
                                                'name' => '�����������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '20', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_500",
                                                'next_achiev' => "use_rune_hp_5000"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_hp_5000' => array(
                                                'name' => '�������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 5000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_1000",
                                                'next_achiev' => "use_rune_hp_10000"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_hp_10000' => array(
                                                'name' => '�������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 10000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_5000",
                                                'next_achiev' => "use_rune_hp_25000"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_hp_25000' => array(
                                                'name' => '�����',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 25000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_10000",
                                                'next_achiev' => "use_rune_hp_50000"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_hp_50000' => array(
                                                'name' => '�������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 50000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_25000",
                                                'next_achiev' => "use_rune_hp_100000"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_hp_100000' => array(
                                                'name' => '�����',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 100000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_50000",
                                                'next_achiev' => "use_rune_hp_250000"
                                              ),
                       # ----------------------------
                       # ��������� ����
                         'use_rune_hp_250000' => array(
                                                'name' => '��������� ����',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 250000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '600', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_100000",
                                                'next_achiev' => "use_rune_hp_500000"
                                              ),
                       # ----------------------------
                       # ������� �����������
                         'use_rune_hp_500000' => array(
                                                'name' => '������� �����������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ����� ������ ���� ��� ������ ����� ��� ���������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ����� ������ ���� ��� ������ ����� ���', 'name' => '', 'val' => 0, 'valmax' => 500000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '1000', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ��������� ���� �������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_hp",
                                                'code'        => "use_rune_hp",
                                                'done_achiev' => "use_rune_hp_250000",
                                                'next_achiev' => ""
                                              ),

                     # ----------------------------
                     # ������������
                     # ----------------------------


                       # ----------------------------
                       # ��������
                         'use_rune_pw_25' => array(
                                                'name' => '��������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 25, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_pw_500"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_pw_500' => array(
                                                'name' => '���������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_25",
                                                'next_achiev' => "use_rune_pw_1000"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_pw_1000' => array(
                                                'name' => '��������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '20', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_500",
                                                'next_achiev' => "use_rune_pw_5000"
                                              ),
                       # ----------------------------
                       # ���� ������
                         'use_rune_pw_5000' => array(
                                                'name' => '���� ������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 5000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_1000",
                                                'next_achiev' => "use_rune_pw_10000"
                                              ),
                       # ----------------------------
                       # ��� � ��
                         'use_rune_pw_10000' => array(
                                                'name' => '��� � ��',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 10000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_5000",
                                                'next_achiev' => "use_rune_pw_25000"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_pw_25000' => array(
                                                'name' => '��������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 25000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_10000",
                                                'next_achiev' => "use_rune_pw_50000"
                                              ),
                       # ----------------------------
                       # ���� ���������
                         'use_rune_pw_50000' => array(
                                                'name' => '���� ���������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 50000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_25000",
                                                'next_achiev' => "use_rune_pw_100000"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_pw_100000' => array(
                                                'name' => '������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 100000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_50000",
                                                'next_achiev' => "use_rune_pw_250000"
                                              ),
                       # ----------------------------
                       # ������� �����
                         'use_rune_pw_250000' => array(
                                                'name' => '������� �����',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 250000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '600', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_100000",
                                                'next_achiev' => "use_rune_pw_500000"
                                              ),
                       # ----------------------------
                       # �������������
                         'use_rune_pw_500000' => array(
                                                'name' => '�������������',
                                                'description' => '������������ � ����� ��������� %s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������',
                                                'goals' => array(
                                                                 1 => array('description' => '������������ � ����� ��������� %s/s% ������� ������ ������������ ������ ���� ��� ������ ����� ��� ��������������', 'name' => '', 'val' => 0, 'valmax' => 500000, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '1000', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ��������� ���� ��������������"', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_pw",
                                                'code'        => "use_rune_pw",
                                                'done_achiev' => "use_rune_pw_250000",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_zah_1' => array(
                                                'name' => '��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_zah_5"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_zah_5' => array(
                                                'name' => '�������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_1",
                                                'next_achiev' => "use_rune_zah_30"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_zah_30' => array(
                                                'name' => '������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_5",
                                                'next_achiev' => "use_rune_zah_100"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_zah_100' => array(
                                                'name' => '�����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_30",
                                                'next_achiev' => "use_rune_zah_250"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_zah_250' => array(
                                                'name' => '�������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_100",
                                                'next_achiev' => "use_rune_zah_500"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_zah_500' => array(
                                                'name' => '�����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_250",
                                                'next_achiev' => "use_rune_zah_1000"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_zah_1000' => array(
                                                'name' => '������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1000 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� ��� ������', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ���������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_1",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_zah_500",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # �����������
                         'use_rune_opl_1' => array(
                                                'name' => '�����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_opl_5"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_opl_5' => array(
                                                'name' => '���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '20', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_1",
                                                'next_achiev' => "use_rune_opl_30"
                                              ),
                       # ----------------------------
                       # �����������
                         'use_rune_opl_30' => array(
                                                'name' => '�����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_5",
                                                'next_achiev' => "use_rune_opl_100"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_opl_100' => array(
                                                'name' => '��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_30",
                                                'next_achiev' => "use_rune_opl_250"
                                              ),
                       # ----------------------------
                       # �������� �����
                         'use_rune_opl_250' => array(
                                                'name' => '�������� �����',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_100",
                                                'next_achiev' => "use_rune_opl_500"
                                              ),
                       # ----------------------------
                       # ����������
                         'use_rune_opl_500' => array(
                                                'name' => '����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_250",
                                                'next_achiev' => "use_rune_opl_1000"
                                              ),
                       # ----------------------------
                       # ������� � �����
                         'use_rune_opl_1000' => array(
                                                'name' => '������� � �����',
                                                'description' => '������� ����������� ����� ���� �� ������ "�����" 1000 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '������������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '���� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '������ ���� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '600', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "�����""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_2",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_opl_500",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_tak_1' => array(
                                                'name' => '�����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_tak_5"
                                              ),
                       # ----------------------------
                       # ����������
                         'use_rune_tak_5' => array(
                                                'name' => '����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '20', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_1",
                                                'next_achiev' => "use_rune_tak_30"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_tak_30' => array(
                                                'name' => '���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_5",
                                                'next_achiev' => "use_rune_tak_100"
                                              ),
                       # ----------------------------
                       # �����
                         'use_rune_tak_100' => array(
                                                'name' => '�����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_30",
                                                'next_achiev' => "use_rune_tak_250"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_tak_250' => array(
                                                'name' => '������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_100",
                                                'next_achiev' => "use_rune_tak_500"
                                              ),
                       # ----------------------------
                       # ����������
                         'use_rune_tak_500' => array(
                                                'name' => '����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '500', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_250",
                                                'next_achiev' => "use_rune_tak_1000"
                                              ),
                       # ----------------------------
                       # ��������� �����
                         'use_rune_tak_1000' => array(
                                                'name' => '��������� �����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1000 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '����� ���� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����� �� ��� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������� ��� (���.)', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '600', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_3",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_tak_500",
                                                'next_achiev' => ""
                                              ),
                       # ----------------------------
                       # ����������
                         'use_rune_pok_1' => array(
                                                'name' => '����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_pok_5"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_pok_5' => array(
                                                'name' => '��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_1",
                                                'next_achiev' => "use_rune_pok_30"
                                              ),
                       # ----------------------------
                       # �����������
                         'use_rune_pok_30' => array(
                                                'name' => '�����������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_5",
                                                'next_achiev' => "use_rune_pok_100"
                                              ),
                       # ----------------------------
                       # ��������� �����
                         'use_rune_pok_100' => array(
                                                'name' => '��������� �����',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_30",
                                                'next_achiev' => "use_rune_pok_250"
                                              ),
                       # ----------------------------
                       # �������
                         'use_rune_pok_250' => array(
                                                'name' => '�������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_100",
                                                'next_achiev' => "use_rune_pok_500"
                                              ),
                       # ----------------------------
                       # ������ ���
                         'use_rune_pok_500' => array(
                                                'name' => '������ ���',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_250",
                                                'next_achiev' => "use_rune_pok_1000"
                                              ),
                       # ----------------------------
                       # �������-���������
                         'use_rune_pok_1000' => array(
                                                'name' => '�������-���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '400', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_4",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_pok_250",
                                                'next_achiev' => "use_rune_pok_1000"
                                              ),


                       # ----------------------------
                       # ���������
                         'use_rune_vozd_1' => array(
                                                'name' => '���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 1 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '���������� (���.)', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '��������� (���.)', 'done' => 0),
                                                                                                                                                                                3 => array('name' => '�������', 'done' => 0),
                                                                                                                                                                                4 => array('name' => '����', 'done' => 0),
                                                                                                                                                                                5 => array('name' => '��������', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '5', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "",
                                                'next_achiev' => "use_rune_vozd_5"
                                              ),
                       # ----------------------------
                       # ���������
                         'use_rune_vozd_5' => array(
                                                'name' => '���������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 5 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 5, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '10', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_1",
                                                'next_achiev' => "use_rune_vozd_30"
                                              ),
                       # ----------------------------
                       # ������ ����
                         'use_rune_vozd_30' => array(
                                                'name' => '������ ����',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 30 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 30, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '30', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_5",
                                                'next_achiev' => "use_rune_vozd_100"
                                              ),
                       # ----------------------------
                       # ��������
                         'use_rune_vozd_100' => array(
                                                'name' => '��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 100 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 100, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_30",
                                                'next_achiev' => "use_rune_vozd_250"
                                              ),
                       # ----------------------------
                       # ���������� ��������
                         'use_rune_vozd_250' => array(
                                                'name' => '���������� ��������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 250 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 250, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '100', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_100",
                                                'next_achiev' => "use_rune_vozd_500"
                                              ),
                       # ----------------------------
                       # ������������ ������
                         'use_rune_vozd_500' => array(
                                                'name' => '������������ ������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 500, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                                2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '200', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_250",
                                                'next_achiev' => "use_rune_vozd_1000"
                                              ),
                       # ----------------------------
                       # ������
                         'use_rune_vozd_1000' => array(
                                                'name' => '������',
                                                'description' => '������� ����������� ����� ���� �� ������ "���������" 500 ���',
                                                'goals' => array(
                                                                 1 => array('description' => '������� ������������ %s/s% ��� ����� ���� �� ������:  ', 'name' => '', 'val' => 0, 'valmax' => 1000, 'listgoals' => array(
                                                                                                                                                                                1 => array('name' => '�����������', 'done' => 0),
                                                                                                                                                                               2 => array('name' => '����������� 15+15', 'done' => 0),
                                                                                                                                                                               )),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '300', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                  2 => array('name' => '"������ 10% �� ��� ���� ��������� "���������""', 'description' => '', 'ex' => 1, 'code' => 'property'),
                                                                 ),
                                                'code_groupe' => "use_rune_list_5",
                                                'code'        => "use_rune_list",
                                                'done_achiev' => "use_rune_vozd_250",
                                                'next_achiev' => ""
                                              ),




          # ----------------------------
          # �����
          # ----------------------------

                       # ----------------------------
                       # ���� �����
                         'klan_1' => array(
                                                'name' => '���� �����',
                                                'description' => '������� ������ ����� %s% ���',
                                                'goals' => array(
                                                                 1 => array('description' => '����� ������ ����� %s/s% ���',  'name' => '', 'val' => 0, 'valmax' => 1, 'listgoals' => array()),
                                                                ),
                                                'honors' => array(
                                                                  1 => array('name' => '50', 'description' => '����� ����������', 'ex' => 0, 'code' => 'achievement_score'),
                                                                 ),
                                                'code_groupe' => "klan",
                                                'code'        => "klan",
                                                'done_achiev' => "",
                                                'next_achiev' => ""
                                              ),


 );

?>
<table width="100%" cellpadding="4"><tbody><tr><td>&nbsp;</td><td bgcolor1="#BDB198" align="center" width="100%"><span class="nav_href">�&nbsp;
<a href="http://lib.blutbad.ru" class="nav_href">�������</a>
&nbsp;/&nbsp;
<a href="http://lib.blutbad.ru/achs" class="nav_href">����������</a>
&nbsp;�</span></td><td>&nbsp;</td></tr></tbody></table>
<hr>
<p align="center" class="header"><b>��������� ����������</b></p>

<table><tbody>
<?
foreach ($achievements as $k => $achiev) {

 if (count($achiev['honors']) > 1) {

?>
 <tr>
  <td style="vertical-align: top;">
      <img alt="<?= $k ?>" height="80" src="http://img.blutbad.ru/i/achievement/<?= $k ?>_f.jpg" width="80"> 
  </td>
  <td>
      <div><b><?= $achiev['name'] ?></b></div>
      <div style="margin-left: 10px; margin-bottom: 10px;">
<?
  $valmax = $achiev['goals'][1]['valmax'];
  $achiev['description'] = str_replace('%s%', ''.$valmax.'', $achiev['description']);
?>
          <div><i><?= $achiev['description'] ?></i></div>

            <div>����:</div>
            <ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">
              <?
               foreach ($achiev['goals'] as $id_goals => $goals) {

                //$goals['description'] = str_replace('%s/s%', '<strong>'.$goals['val'].'/'.$goals['valmax'].'</strong>', $goals['description']);
                 $goals['description'] = str_replace('%s/s%', '<strong>'.$goals['valmax'].'</strong>', $goals['description']);

              ?>
            	<li>
                    <?= $goals['description'] ?>
                <?
                if ($goals['listgoals']) {
                 echo '<ul class="ach_aim_list">';
                 foreach ($goals['listgoals'] as $k_listgoals => $v_listgoals) {
                ?>
            		<li>
            			<?= $v_listgoals['done']?"<b>":"" ?><?= $v_listgoals['name'] ?><?= $v_listgoals['done']?"</b>":"" ?>
            		</li>

                <?
                 }
                 echo '</ul>';
                 }
                ?>
            	</li>
              <?
               }
              ?>
            </ul>
            <div>�������:</div>
            <ul style="list-style-type: disc; padding:0; margin: 0; margin-left: 25px;">
              <?
               foreach ($achiev['honors'] as $id_honors => $honors) {
              ?>
            	<li>
                    <em><?= $honors['name'] ?></em> <?= $honors['description'] ?>
            	</li>
              <?
               }
              ?>
            </ul>
      </div>
  </td>
 </tr>
<?
  }
}
?>
</tbody></table>

 <hr>
<? include($_SERVER["DOCUMENT_ROOT"]."/main.but.php");?>